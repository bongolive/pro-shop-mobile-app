/*
 * The source codes in this project are owned by Bongolive Enterprises. Copying or any kind of usage without Bongolive's approval is against the law.
 * Copyright (c) 2015.
 *
 */

package bongolive.apps.proshop.model;

import android.content.ContentResolver;
import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.DatabaseUtils;
import android.database.sqlite.SQLiteDatabase;
import android.net.Uri;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import bongolive.apps.proshop.controller.AppPreference;
import bongolive.apps.proshop.controller.ReportsItem;
import bongolive.apps.proshop.controller.Validating;
import bongolive.apps.proshop.controller.Constants;
import bongolive.apps.proshop.controller.UrlCon;

public class Expenses {
	public static final String TABLENAME = "expense" ;
	public static final String ID = "_id";
	public static final String REFERENCE = "reference";// required to send system_customer_id
	public static final String AMOUNT = "amount";// required to send system_customer_id
	public static final String CREATEDON = "created_on";
	public static final String PAYMETHOD ="payment_method" ;
	public static final String EXPENSETYPE ="expense_type" ;
	public static final String COMMENT ="note" ;
	public static final String OPERATOR = "operator_name" ;
	public static final String SUPPLIER = "supplier" ;
	public static final String CREATEDBY = "created_by" ;
	public static final String UPDATEDON = "updated_on" ;
	public static final String SYS_ID = "server_id";
	public static final String ATTACHMENT = "attachment";
	public static final String TITLE = "title";
	public static final String ACK = "ack" ;
	public static final String TIMESTAMP = "timestamp" ;

	public static final Uri BASEURI = Uri.parse("content://"+ ContentProviderApi.AUTHORITY+"/"+TABLENAME) ;
	public static final String SPECIFIC_CONTENT_TYPE = "vnd.android.cursor.item/vnd.com.proshop.expense" ;
	public static final String GENERAL_CONTENT_TYPE = "vnd.android.cursor.dir/vnd.com.proshop.expense" ;
	private static final String ARRAYNAME = "expense_list";

	int id,customer,ack,status ;
	double pay,totalsales,totaltax,totaldiscount ;
	String date,paystatus ;
	static UrlCon js = null ;

	public Expenses() {

	}
	public Expenses(int id, int cus, int sy, double paynt, double sales, double tax  , String dt, String status,int st_){
		this.id =id ;
		this.customer = cus ;
		this.ack = sy ;
		this.totalsales = sales ;
		this.totaltax = tax ;
		this.date = dt ;
		this.paystatus = status ;
		this.pay = paynt;
		this.status = st_;
	}

	public static boolean insert(Context context, String[] orders, String source){
		int ordercount = getCount(context) ;
		ContentResolver cr = context.getContentResolver() ;
		ContentValues cv = new ContentValues() ;
		if(source.equals(Constants.REMOTE)) {
			cv.put(REFERENCE, orders[0]);
			cv.put(AMOUNT, orders[1]);
			cv.put(EXPENSETYPE, orders[2]);
			cv.put(PAYMETHOD, orders[3]);
			cv.put(OPERATOR, orders[4]);
			cv.put(SUPPLIER, orders[5]);
			cv.put(ACK, 1);
			cv.put(SYS_ID, orders[6]);
			cv.put(ATTACHMENT, orders[7]);
			cv.put(COMMENT, orders[8]);
			cv.put(TIMESTAMP, orders[9]);
			cv.put(CREATEDBY, orders[10]);
			cv.put(CREATEDON, orders[11]);
			cv.put(TITLE, orders[0]);
			cv.put(UPDATEDON, Constants.getDate());
		} else if(source.equals(Constants.LOCAL)) {
			cv.put(REFERENCE, orders[0]);
			cv.put(AMOUNT, orders[1]);
			cv.put(EXPENSETYPE, orders[2]);
			cv.put(PAYMETHOD, orders[3]);
			cv.put(OPERATOR, orders[4]);
			cv.put(SUPPLIER, orders[5]);
			cv.put(TITLE, orders[0]);
			cv.put(CREATEDBY, orders[6]);
			cv.put(ATTACHMENT, orders[7]);
			cv.put(COMMENT, orders[8]);
			cv.put(TIMESTAMP, orders[9]);
		}
		cr.insert(BASEURI, cv);
		return getCount(context) == ordercount + 1;
	}
	public static boolean update(Context context, String[] orders, String source){
		ContentResolver cr = context.getContentResolver() ;
		ContentValues cv = new ContentValues() ;
		String where = null;
		if(source.equals(Constants.REMOTE)) {
			cv.put(REFERENCE, orders[0]);
			cv.put(AMOUNT, orders[1]);
			cv.put(EXPENSETYPE, orders[2]);
			cv.put(PAYMETHOD, orders[3]);
			cv.put(OPERATOR, orders[4]);
			cv.put(SUPPLIER, orders[5]);
			cv.put(ACK, 1);
			cv.put(ATTACHMENT, orders[7]);
			cv.put(COMMENT, orders[8]);
			cv.put(TIMESTAMP, orders[9]);
			cv.put(CREATEDBY, orders[10]);
			cv.put(CREATEDON, orders[11]);
			cv.put(TITLE, orders[0]);
			cv.put(UPDATEDON, Constants.getDate());
			where = SYS_ID + " = "+ orders[6] ;
		} else if(source.equals(Constants.LOCAL)) {
			cv.put(REFERENCE, orders[0]);
			cv.put(AMOUNT, orders[1]);
			cv.put(EXPENSETYPE, orders[2]);
			cv.put(PAYMETHOD, orders[3]);
			cv.put(OPERATOR, orders[4]);
			cv.put(SUPPLIER, orders[5]);
			cv.put(TITLE, orders[6]);
			cv.put(CREATEDBY, orders[7]);
			cv.put(ATTACHMENT, orders[8]);
			cv.put(COMMENT, orders[9]);
			cv.put(TIMESTAMP, orders[10]);
			where = ID + " = "+ orders[11] ;
		}
		int up = cr.update(BASEURI, cv, where, null);
		if(up > 0 )
			return true;
		return false;
	}

	//gettotaltax,getpayamount
	public static int getCount(Context context){
		ContentResolver cr = context.getContentResolver() ;
		Cursor _c = cr.query(BASEURI, null, null, null, null);
		int count = _c.getCount() ;
		_c.close() ;
		return count;
	}

	/*
         * Fetching sales from server if sent frJsonObjectJsonArrayom this device
         */
	public static JSONObject getFormJson(Context context) throws JSONException
	{
		JSONObject jobs = new JSONObject();
		jobs.put("tag", Constants.TAGSGETXPENSE);
		jobs.put(Constants.IMEI, Constants.getIMEINO(context));
		js = new UrlCon();
		String job =  js.getJson(Constants.SERVER, jobs);
		JSONObject json;
		try {
			json = new JSONObject(job);
			if(json != null)
			{
				return json ;
			}
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null ;
	}

	public static JSONObject sendFormJson(Context context) throws JSONException
	{
		ContentResolver cr = context.getContentResolver() ;
		String where = ACK + " = 0" ;
		Cursor c = cr.query(BASEURI, null, where, null, null);
		if(c.getCount() == 0){
			c.close();
			return null;
		}
		js = new UrlCon();
		try {
			JSONObject jsonObject = new JSONObject();
			jsonObject.put("tag", Constants.TAGSSENDXPENSE);
			jsonObject.put(Constants.IMEI, Constants.getIMEINO(context));
			String array ;
			if(c.moveToFirst())
			{
				JSONArray jarr = new JSONArray();
				do{
					JSONObject j = new JSONObject();
					j.put(REFERENCE, c.getString(c.getColumnIndexOrThrow(REFERENCE)));
					j.put(AMOUNT, c.getString(c.getColumnIndexOrThrow(AMOUNT)));
					j.put(CREATEDON, c.getString(c.getColumnIndexOrThrow(CREATEDON)));
					j.put(EXPENSETYPE, c.getString(c.getColumnIndexOrThrow(EXPENSETYPE)));
					j.put(OPERATOR, c.getString(c.getColumnIndexOrThrow(OPERATOR)));
					j.put(SUPPLIER, c.getString(c.getColumnIndexOrThrow(SUPPLIER)));
					j.put(TITLE, c.getString(c.getColumnIndexOrThrow(TITLE)));
					j.put(ACK, c.getString(c.getColumnIndexOrThrow(ACK)));
					j.put(CREATEDBY, c.getString(c.getColumnIndexOrThrow(CREATEDBY)));
					j.put(UPDATEDON, c.getString(c.getColumnIndexOrThrow(UPDATEDON)));
					j.put(ATTACHMENT, c.getString(c.getColumnIndexOrThrow(ATTACHMENT)));
					j.put(COMMENT, c.getString(c.getColumnIndexOrThrow(COMMENT)));
					j.put(TIMESTAMP, c.getString(c.getColumnIndexOrThrow(TIMESTAMP)));
					j.put(PAYMETHOD, c.getString(c.getColumnIndexOrThrow(PAYMETHOD)));
					j.put(ID, c.getString(c.getColumnIndexOrThrow(ID)));
					j.put(SYS_ID, c.getString(c.getColumnIndexOrThrow(SYS_ID)));
					j.put(SUPPLIER, c.getString(c.getColumnIndexOrThrow(SUPPLIER)));
					j.put(OPERATOR, c.getString(c.getColumnIndexOrThrow(OPERATOR)));
					jarr.put(j);
				} while(c.moveToNext()) ;
				jsonObject.put(ARRAYNAME, jarr);
				System.out.println(jsonObject.toString());
				array = js.getJson(Constants.SERVER, jsonObject) ;
				JSONObject job =  new JSONObject(array);
				return job ;
			}
		}finally {
			if(c != null) {
				c.close();
			}
		}
		return null ;
	}

	public static void processInformation(Context context) throws JSONException{

		JSONObject jsonin = getFormJson(context);
		if(jsonin != null)
			try {
				if(jsonin.has(Constants.SUCCESS)){
					String res = jsonin.getString(Constants.SUCCESS) ;
					if(Integer.parseInt(res) == 1)
					{
						if(jsonin.has(ARRAYNAME)) {
							JSONArray jsarray = jsonin.getJSONArray(ARRAYNAME);
							int success = 0;

							for (int i = 0; i < jsarray.length(); i++) {
								JSONObject j = jsarray.getJSONObject(i);
								String min = j.getString(REFERENCE);
								String mout = j.getString(AMOUNT);
								String tin = j.getString(PAYMETHOD);
								String tout = j.getString(EXPENSETYPE);
								String cin = j.getString(OPERATOR);
								String cout = j.getString(SUPPLIER);
								String cby = "0";
								String sysid = j.getString(SYS_ID);
								String sign = "";
								String note = j.getString(COMMENT);
								String isprint = j.getString(TIMESTAMP);
								String con = Constants.getDate();

								String[] vals = {min,mout,tin,tout,cin,cout,sysid,sign,note,isprint,cby,con};
								String[] chk = {min,mout,tin,tout,cin,cout,sysid,isprint,cby,isprint};
								if (Validating.areSet(chk)) {
									boolean var = isFormPresentFromSystem(context, sysid);

									ArrayList<String> extye = new ArrayList<>();
									AppPreference appPreference = new AppPreference(context);
									extye = appPreference.getExpenseType();
									int k = extye.indexOf(tout);
									if(k == -1)
										appPreference.storeExpenseType(tout);
									if (!var) {
										if (insert(context, vals, Constants.REMOTE)) {
											success += 1;
										}
									} else {
										success += 1;
									}
								}
							}
							if (success == jsarray.length() && jsarray.length() >0)
								sendAckJson(context);
						}
					}
				}
			} catch (NumberFormatException e) {
				e.printStackTrace();
			} catch (JSONException e) {
				e.printStackTrace();
			}


		JSONObject jsonout = sendFormJson(context);
		if(jsonout != null)
			try {
				if(jsonout.has(Constants.SUCCESS)){
					String res = jsonout.getString(Constants.SUCCESS) ;
					if(Integer.parseInt(res) == 1)
					{
						if(jsonout.has(ARRAYNAME)){
							JSONArray jsonArray = jsonout.getJSONArray(ARRAYNAME);
							for(int i = 0; i< jsonArray.length(); i++) {
								JSONObject jsonObject1 = jsonArray.getJSONObject(i);
								String orderid = jsonObject1.getString(TIMESTAMP);
								String saleid = jsonObject1.getString(SYS_ID);
								if(saleid.equals("0") || !Validating.areSet(saleid))
									continue;
								String[] v = {"1",orderid,saleid};
								if(Validating.areSet(v)) {
									boolean OK = updateSync(context, v);
									if (OK) {
										System.out.println("updated form "+saleid);
									}
								}
							}
						}
					}
				}
			} catch (NumberFormatException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (JSONException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

	}

    /* check if the sale id is present in the app
    *
    */

	private static boolean isFormPresentFromSystem(Context context, String custid)
	{
		ContentResolver cr = context.getContentResolver();
		boolean ret = false;
		String where = SYS_ID +" = "+custid;
		Cursor c = cr.query(BASEURI, null, where, null, null);
		try {
			if (c.moveToFirst()) {
				ret = true ;
			}
		} finally {
			if (c != null) {
				c.close();
			}
		}
		return ret;
	}

	public static boolean columnExist(SQLiteDatabase context,String column){
		boolean ret= false;
		String sql = " SELECT * FROM "+TABLENAME  +" LIMIT 1";

		Cursor c = context.rawQuery(sql, null);
		try {
			int indext = c.getColumnIndex(column);
			System.out.println(" index is " +indext);
			if(indext != -1)
				ret = true;
		}finally {
			if(c != null) {
				c.close();
			}
		}
		return ret;
	}

	public static boolean updateSync(Context context, String[] vals)
	{
		ContentResolver cr = context.getContentResolver();
		ContentValues values = new ContentValues() ;
		values.put(ACK, vals[0]);
		values.put(SYS_ID, vals[2]);
		values.put(UPDATEDON, Constants.getDate());
		String where =  TIMESTAMP+ " = "+ DatabaseUtils.sqlEscapeString(vals[1]);
		return cr.update(BASEURI, values, where, null) == 1;
	}

	public static int isFormSynced(Context context, String id)
	{
		ContentResolver cr = context.getContentResolver();
		String where = ID + " = " + id ;

		Cursor c = cr.query(BASEURI, new String[] {ACK}, where,null,null) ;
		try {
			int i= 0 ;
			if(c.moveToFirst())
			{
				i = c.getInt(c.getColumnIndexOrThrow(ACK));
				return i ;
			}
		} finally {
			if(c != null) {
				c.close();
			}
		}
		return 0;
	}

	public static int getId(Context context) {
		ContentResolver cr = context.getContentResolver();
		Cursor c = cr.query(BASEURI, new String[] {ID}, null,null,null) ;
		try {
			int i= 0 ;
			if(c.moveToLast())
			{
				i = c.getInt(c.getColumnIndexOrThrow(ID));
				return i ;
			}
		}finally {
			if(c != null) {
				c.close();
			}
		}
		return 0 ;
	}



	public static void sendAckJson(Context context) throws JSONException {
		JSONObject jobs = new JSONObject();
		jobs.put("tag", Constants.TAGACK);
		jobs.put(Constants.ACKITEM, TABLENAME);
		jobs.put(Constants.IMEI, Constants.getIMEINO(context));
		js = new UrlCon();
		System.out.println(jobs.toString());
		js.getJson(Constants.SERVER,jobs);
	}

	private static ArrayList<ReportsItem> getItems(Context context, long id){
		ArrayList<ReportsItem> lst = new ArrayList<>();
		ContentResolver cr = context.getContentResolver() ;
		String where = ID +" = "+id ;
		Cursor c = cr.query(BASEURI, null, where, null, null);
		try {
			if (c.moveToFirst()) {

			}
		} finally {
			if(c!= null)
				c.close();
		}
		return  lst;
	}

}
