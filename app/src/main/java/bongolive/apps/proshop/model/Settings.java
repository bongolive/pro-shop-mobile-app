/*
 * Copyright (c) 2015.
 * Prosurvey is a property of Bongolive Enterprises Ltd. An authorized
 * copy of ideas, source codes is against the regulations
 */

package bongolive.apps.proshop.model;

import android.content.ContentResolver;
import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.net.Uri;
import android.util.Log;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import bongolive.apps.proshop.controller.AppPreference;
import bongolive.apps.proshop.R;
import bongolive.apps.proshop.controller.Validating;
import bongolive.apps.proshop.controller.Constants;
import bongolive.apps.proshop.controller.UrlCon;

public class Settings {
	public static final String TABLENAME = "settings" ;
	public static final String ID = "_id" ;
	public static final String SYSID = "id" ;
	public static final String VAT = "enable_tax" ;
	public static final String DISCOUNT = "enable_discount" ;
    public static final String TRACKTIME = "track_time_interval";
    public static final String PRINT = "print_receipt";
	public static final String EXPORTDB = "export_db" ;
	public static final String IMPORTDB = "import_db" ;
	public static final String PRODUCT_EDIT = "allow_product_editing" ;
	public static final String CUSTOMER_EDIT = "allow_customer_editing" ;
	public static final String DELIVERY = "allow_delivery" ;
	public static final String STOCK_RECEIVE = "allow_stock_receiving" ;
	public static final String PRODUCT_SHARING = "allow_product_sharing" ;
	public static final String PRODUCT_ADD = "allow_product_adding" ;
	public static final String CUSTOMER_ADD = "allow_customer_adding" ;
	public static final String DISCOUNT_LIMIT = "max_discount_per" ;
	public static final String APPTYPE = "application_type" ;
	public static final String PAYLATER = "allow_post_paid" ;
	public static final String CREATED = "created" ;
	public static final String MODIFIED = "modified" ;
//  we need to handle deliveries... how? am not sure but we just need to
	public static final String USERS = "users" ;
	public static final String ORDERS = "orders" ;
	public static final String PROSHOPLITE = "proshop_lite" ;
	public static final String STATION_FORM = "station_form_module" ;
	public static final String EXPENSES = "expenses_module" ;
	public static final String AUDIT = "audit_module" ;
	public static final String PAYMENT = "payment" ;
	public static final String STOCK = "stock_module" ;
	public static final String SALES = "sales_module" ;
	public static final String REPORTS = "reports_module" ;
	public static final String PRODUCTS = "products_module" ;
	public static final String CUSTOMERS = "customers_module" ;
	public static final String SIGNATURE = "allow_signature" ;
	public static final String GENERAL_CONTENT_TYPE = "vnd.android.cursor.dir/vnd.com.proshop.settings" ;
	public static final String SPECIFIC_CONTENT_TYPE = "vnd.android.cursor.item/vnd.com.proshop.settings" ;
    public static final String ARRAYNAME = "settingsList";
	private static final String TAG = Settings.class.getName();
	public static final Uri BASEURI = Uri.parse("content://" + ContentProviderApi.AUTHORITY + "/" + TABLENAME) ;
    private static  JSONArray settingsarray;
    static UrlCon js;

	int id,tax,discount,track,print,prexport,primport,prodedit,custedit,stockrece,prodsharing,prodadd,custadd,paylt,
            delivery ;

    double disclimit;
    String apptype;

    public Settings(int id, int tax, int discount, int track, int print, int prexport, int primport, int prodedit,
                    int custedit, int stockrece, int prodsharing, int prodadd, int custadd,int pyt, double disclimit,
                    String  apptype, int del) {
        this.id = id;
        this.tax = tax;
        this.discount = discount;
        this.track = track;
        this.print = print;
        this.prexport = prexport;
        this.primport = primport;
        this.prodedit = prodedit;
        this.custedit = custedit;
        this.stockrece = stockrece;
        this.prodsharing = prodsharing;
        this.prodadd = prodadd;
        this.custadd = custadd;
        this.disclimit = disclimit;
        this.apptype = apptype;
        this.paylt = pyt;
        this.delivery = del;
    }

    public static int getSettingsCount(Context context) {
        ContentResolver cr = context.getContentResolver();
        Cursor c = cr.query(BASEURI, null, null, null, null);
        int count = 0;
        try {
            count = c.getCount();
        } finally {
            if (c != null) {
                c.close();
            }
        }
        return count;
    }

	public static boolean insert(Context context,String[] v) {
        int settingsCount = getSettingsCount(context);
        ContentResolver cr = context.getContentResolver();
        ContentValues cv = new ContentValues();
        cv.put(SYSID, v[0]);
        cv.put(VAT, v[1]);
        cv.put(DISCOUNT, v[2]);
        cv.put(TRACKTIME, v[3]);
        cv.put(PRINT, v[4]);
        cv.put(EXPORTDB, v[5]);
        cv.put(IMPORTDB, v[6]);
        cv.put(PRODUCT_EDIT, v[7]);
        cv.put(CUSTOMER_EDIT, v[8]);
        cv.put(STOCK_RECEIVE, v[9]);
        cv.put(PRODUCT_SHARING, v[10]);
        cv.put(PRODUCT_ADD, v[11]);
        cv.put(CUSTOMER_ADD, v[12]);
        cv.put(DISCOUNT_LIMIT, v[13]);
        cv.put(APPTYPE, v[14]);
        cv.put(PAYLATER, v[15]);
        cv.put(USERS, v[16]);
        cv.put(DELIVERY, v[17]);
        cv.put(ORDERS, v[18]);
        cv.put(PROSHOPLITE, v[19]);
        cv.put(STATION_FORM, v[20]);
        cv.put(EXPENSES, v[21]);
        cv.put(AUDIT, v[22]);
        cv.put(PAYMENT, v[23]);
        cv.put(STOCK, v[24]);
        cv.put(SIGNATURE, v[25]);
        cv.put(SALES, v[26]);
        cv.put(REPORTS, v[27]);
        cv.put(PRODUCTS, v[28]);
        cv.put(CUSTOMERS, v[29]);
        cv.put(CREATED, Constants.getDate());
        cr.insert(BASEURI, cv);
        System.out.println("count " + settingsCount);
        if (getSettingsCount(context) == settingsCount + 1) {
            return true;
        } else {
            return false;
        }
    }


	public static boolean update(Context context,String[] v)
	{
		ContentResolver cr = context.getContentResolver() ;
		ContentValues cv = new ContentValues() ;
        cv.put(VAT,v[1]);
        cv.put(DISCOUNT,v[2]);
        cv.put(TRACKTIME,v[3]);
        cv.put(PRINT,v[4]);
        cv.put(EXPORTDB,v[5]);
        cv.put(IMPORTDB,v[6]);
        cv.put(PRODUCT_EDIT,v[7]);
        cv.put(CUSTOMER_EDIT,v[8]);
        cv.put(STOCK_RECEIVE,v[9]);
        cv.put(PRODUCT_SHARING, v[10]);
        cv.put(PRODUCT_ADD,v[11]);
        cv.put(CUSTOMER_ADD,v[12]);
        cv.put(DISCOUNT_LIMIT,v[13]);
        cv.put(APPTYPE, v[14]);
        cv.put(PAYLATER, v[15]);
        cv.put(USERS, v[16]);
        cv.put(DELIVERY, v[17]);
        cv.put(ORDERS, v[18]);
        cv.put(PROSHOPLITE, v[19]);
        cv.put(STATION_FORM, v[20]);
        cv.put(EXPENSES, v[21]);
        cv.put(AUDIT, v[22]);
        cv.put(PAYMENT, v[23]);
        cv.put(STOCK, v[24]);
        cv.put(SIGNATURE, v[25]);
        cv.put(SALES, v[26]);
        cv.put(REPORTS, v[27]);
        cv.put(PRODUCTS, v[28]);
        cv.put(CUSTOMERS, v[29]);
        cv.put(MODIFIED, Constants.getDate());
        String where = SYSID +" = "+v[0];
		if(cr.update(BASEURI, cv, where, null) > 0)
		{
			return true ;
		} else {
			return false ;
		}
	}
	  
	public static void processSettings(Context context) throws JSONException
	{
		JSONObject json = getSettingsJson(context) ;
        if(json != null)
		try { 
			if(json.has(Constants.SUCCESS)) {
                String res = json.getString(Constants.SUCCESS);
                if (Integer.parseInt(res) == 1) {
                    if (json.has(ARRAYNAME)) {

                        JSONObject j = json.getJSONObject(ARRAYNAME);
                        String si = j.getString(SYSID);
                        String tx = j.getString(VAT);
                        String ds = j.getString(DISCOUNT);
                        String tt = j.getString(TRACKTIME);
                        String pt = j.getString(PRINT);
                        String ex = j.getString(EXPORTDB);
                        String im = j.getString(IMPORTDB);
                        String pe = j.getString(PRODUCT_EDIT);
                        String ce = j.getString(CUSTOMER_EDIT);
                        String sr = j.getString(STOCK_RECEIVE);
                        String ps = j.getString(PRODUCT_SHARING);
                        String pa = j.getString(PRODUCT_ADD);
                        String ca = j.getString(CUSTOMER_ADD);
                        String dl = j.getString(DISCOUNT_LIMIT);
                        String ap = j.getString(APPTYPE);
                        String pl = j.getString(PAYLATER);
                        String us = j.getString(USERS);
                        String del =j.getString(DELIVERY);
                        String ord =j.getString(ORDERS);
                        String lt = j.getString(PROSHOPLITE);
                        String f = j.getString(STATION_FORM);
                        String e = j.getString(EXPENSES);
                        String a = j.getString(AUDIT);
                        String p = j.getString(PAYMENT);
                        String s = j.getString(STOCK);
                        String sig = j.getString(SIGNATURE);
                        String sl = j.getString(SALES);
                        String rp = j.getString(REPORTS);
                        String pd = j.getString(PRODUCTS);
                        String ct = j.getString(CUSTOMERS);

                        String[] settings = {si, tx, ds, tt, pt, ex, im, pe, ce, sr, ps, pa, ca, dl, ap,pl,us,del,
                                ord,lt,f,e,a,p,s,sig,sl,rp,pd,ct};
                        if (Validating.areSet(settings)) {
                            if (!isSettingsExist(context, si)) {
                                System.out.println("settings is not existing");
                                if (insert(context, settings))
                                    sendAckJson(context);
                            } else {
                                System.out.println("settings is existing now updating");
                                if (update(context, settings))
                                    sendAckJson(context);
                            }
                        } else {

                        }

                    }
                }
            }
		 } catch (JSONException e) { 
						e.printStackTrace();
	 }
	}




    public static boolean columnExist(SQLiteDatabase context,String column){
        boolean ret= false;
        String sql = " SELECT * FROM "+TABLENAME  +" LIMIT 1";

        Cursor c = context.rawQuery(sql, null);
        try {
            int indext = c.getColumnIndex(column);
            System.out.println(" index is " +indext);
            if(indext != -1)
                ret = true;
        }finally {
            if(c != null) {
                c.close();
            }
        }
        return ret;
    }


    public static void sendAckJson(Context context) throws JSONException {
        JSONObject jobs = new JSONObject();
        jobs.put("tag", Constants.TAGACK);
        jobs.put(Constants.ACKITEM, TABLENAME);
        jobs.put(Constants.IMEI, Constants.getIMEINO(context));
         js = new UrlCon();
        System.out.println(jobs.toString());
        js.getJson(Constants.SERVER,jobs);
    }

    public static void checkSync(Context context) throws JSONException {
        AppPreference ap = new AppPreference(context);
        JSONObject jobs = new JSONObject();
        jobs.put("tag", "check_sync");
        String time = ap.getDefaultSync();
        time  = !time.isEmpty() ? time : context.getResources().getStringArray(R.array.pref_sync_frequency_values)[0];
        jobs.put("nextsync", time);
        jobs.put(Constants.IMEI, Constants.getIMEINO(context));
         js = new UrlCon();
        js.getJson(Constants.SYNCTRACK,  jobs);
    }

	private static JSONObject getSettingsJson(Context context) throws JSONException {
        JSONObject jobs = new JSONObject();
        jobs.put("tag", Constants.TAGSETTINGS);
        jobs.put(Constants.IMEI, Constants.getIMEINO(context));
         js = new UrlCon();
        String job = js.getJson(Constants.SERVER,jobs);
        JSONObject json;
        try {
            json = new JSONObject(job);
            if (json.length() != 0) {
                return json;
            } else {
                Log.v("ERROR JOB", " JSONOBJECT IS NOT VALID OR NULL");
            }
        } catch (JSONException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        return null;

    }

    /* check if the settings id is present in the app
    *
    */

    public static boolean isSettingsExist(Context context, String custid)
    {
        ContentResolver cr = context.getContentResolver();
        boolean ret = false;
        String where = SYSID +" = "+custid;
        Cursor c = cr.query(BASEURI, null, where, null, null);
        try {
            if (c.moveToFirst()) {
                ret = true ;
            }
        } finally {
            if (c != null) {
                c.close();
            }
        }
        return ret;
    }

	/*
	*  register device
	* */
	public static JSONObject registerDevice(Context context) throws JSONException {
		JSONObject jobs = new JSONObject();
		jobs.put("tag", Constants.TAGREGISTRATION);
		jobs.put(Constants.IMEI, Constants.getIMEINO(context));

		 js = new UrlCon();
		String ret = js.getJson(Constants.SERVERAUTHENTICITY, jobs);
        System.out.println(ret);
		JSONObject json = new JSONObject(ret);
		return json;
	}
	/*
	* validate account
	* */
	public static JSONObject validateToken(Context context) throws JSONException {
        JSONObject jobs = new JSONObject();
		AppPreference appPreference = new AppPreference(context);
        jobs.put("tag", Constants.AUTHENTICATETAG);
        jobs.put(Constants.IMEI, Constants.getIMEINO(context));
        jobs.put(Constants.AUTHTOKENSERVER, appPreference.getAuthkey());

		 js = new UrlCon();
		String ret = js.getJson(Constants.SERVERAUTHENTICITY,  jobs);
		JSONObject json = new JSONObject(ret);
		return json;
	}

	public static void perform_account_validation(Context context){
		AppPreference appPreference = new AppPreference(context);
		try {
			JSONObject jsonObject = validateToken(context);
			System.out.println(jsonObject.toString());
			if (jsonObject != null) {
				if (!jsonObject.has(Constants.SUCCESS)) {
					appPreference.clear_authentications();
				} else {
					appPreference.save_last_sync(Constants.getDateOnly2());
				}
			}
            
		} catch (JSONException e) {
			e.printStackTrace();
		}
	}

    public static boolean updateVal(Context context, String[] vals)
    {
        ContentResolver cr = context.getContentResolver();
        ContentValues values = new ContentValues() ;
        values.put(vals[0], vals[1]);
        boolean ret = false;
        int id = getId(context);
        if(id > 0) {
            String where = ID + " = " + getId(context);
            if (cr.update(BASEURI, values, where, null) > 0) {
                System.out.println("updating " + vals[0] + " with value " + vals[1]);
                ret = true;
            }
        } else {
            cr.insert(BASEURI,values);
            System.out.println(" id is "+getId(context));
            if(getSettingsCount(context) > 0)
                ret = true;
        }
        return ret;
    }

    public static int getId(Context context) {
        ContentResolver cr = context.getContentResolver();
        Cursor c = cr.query(BASEURI, new String[]{ID}, null,null,null) ;
        try {
            int i = 0;
            if(c.moveToFirst())
            {
                i = c.getInt(c.getColumnIndexOrThrow(ID));
                return i ;
            }
        }finally {
            if(c != null) {
                c.close();
            }
        }
        return 0 ;
    }
    public static int getTrackTime(Context context) {
        ContentResolver cr = context.getContentResolver();
        Cursor c = cr.query(BASEURI, null, null,null,null) ;
        try {
            int i = 0;
            if(c.moveToFirst())
            {
                i = c.getInt(c.getColumnIndexOrThrow(TRACKTIME));
                return i ;
            }
        }finally {
            if(c != null) {
                c.close();
            }
        }
        return 0 ;
    }


    public static boolean getTaxEnabled(Context context) {
        ContentResolver cr = context.getContentResolver();
        Cursor c = cr.query(BASEURI, null, null, null, null) ;
        boolean ret = false;
        try {
            int i = 0;
            if(c.moveToFirst())
            {
                i = c.getInt(c.getColumnIndexOrThrow(VAT));
                System.out.println("vat status is "+i);
                if(i == 0)
                    ret = false;
                if(i == 1)
                    ret = true;
            }
            System.out.println(ret);
        }finally {
            if(c != null) {
                c.close();
            }
        }
        System.out.println(ret);
        return ret;
    }



    public static boolean getUserEnabled(Context context) {
        ContentResolver cr = context.getContentResolver();
        Cursor c = cr.query(BASEURI, null, null, null, null) ;
        boolean ret = false;
        try {
            int i = 0;
            if(c.moveToFirst())
            {
                i = c.getInt(c.getColumnIndexOrThrow(USERS));
                if(i == 0)
                    ret = false;
                if(i == 1)
                    ret = true;
            }
            System.out.println(ret);
        }finally {
            if(c != null) {
                c.close();
            }
        }
        System.out.println(ret);
        return ret;
    }

    public static boolean getOrderEnabled(Context context) {
        ContentResolver cr = context.getContentResolver();
        Cursor c = cr.query(BASEURI, null, null, null, null) ;
        boolean ret = false;
        try {
            int i = 0;
            if(c.moveToFirst())
            {
                i = c.getInt(c.getColumnIndexOrThrow(ORDERS));
                if(i == 0)
                    ret = false;
                if(i == 1)
                    ret = true;
            }
            System.out.println(ret);
        }finally {
            if(c != null) {
                c.close();
            }
        }
        System.out.println(ret);
        return ret;
    }

    public static boolean getDeliveryEnabled(Context context) {
        ContentResolver cr = context.getContentResolver();
        Cursor c = cr.query(BASEURI, null, null, null, null) ;
        boolean ret = false;
        try {
            int i = 0;
            if(c.moveToFirst())
            {
                i = c.getInt(c.getColumnIndexOrThrow(DELIVERY));
                if(i == 0)
                    ret = false;
                if(i == 1)
                    ret = true;
            }
            System.out.println(ret);
        }finally {
            if(c != null) {
                c.close();
            }
        }
        System.out.println(ret);
        return ret;
    }



    public static boolean getPayLaterEnabled(Context context) {
        ContentResolver cr = context.getContentResolver();
        Cursor c = cr.query(BASEURI, null, null,null,null) ;
        try {
            int i = 0;
            if(c.moveToFirst())
            {
                i = c.getInt(c.getColumnIndexOrThrow(PAYLATER));
                if(i == 0)
                    return false;
                if(i == 1)
                    return true;
            }
        }finally {
            if(c != null) {
                c.close();
            }
        }
        return true ;
    }

    public static boolean getDiscountEnabled(Context context) {
        ContentResolver cr = context.getContentResolver();
        Cursor c = cr.query(BASEURI, null, null,null,null) ;
        try {
            int i = 0;
            if(c.moveToFirst())
            {
                i = c.getInt(c.getColumnIndexOrThrow(DISCOUNT));
                if(i == 0)
                    return false;
                if(i == 1)
                    return true;
            }
        }finally {
            if(c != null) {
                c.close();
            }
        }
        return true ;
    }

    public static boolean getImportEnabled(Context context) {
        ContentResolver cr = context.getContentResolver();
        Cursor c = cr.query(BASEURI, null, null,null,null) ;
        try {
            int i = 0;
            if(c.moveToFirst())
            {
                i = c.getInt(c.getColumnIndexOrThrow(IMPORTDB));
                if(i == 0)
                    return false;
                if(i == 1)
                    return true;
            }
        }finally {
            if(c != null) {
                c.close();
            }
        }
        return true ;
    }

    public static boolean getExportEnabled(Context context) {
        ContentResolver cr = context.getContentResolver();
        Cursor c = cr.query(BASEURI, null, null,null,null) ;
        try {
            int i = 0;
            if(c.moveToFirst())
            {
                i = c.getInt(c.getColumnIndexOrThrow(EXPORTDB));
                if(i == 0)
                    return false;
                if(i == 1)
                    return true;
            }
        }finally {
            if(c != null) {
                c.close();
            }
        }
        return true ;
    }


    public static boolean getProductShareEnabled(Context context) {
        ContentResolver cr = context.getContentResolver();
        Cursor c = cr.query(BASEURI, null, null,null,null) ;
        try {
            int i = 0;
            if(c.moveToFirst())
            {
                i = c.getInt(c.getColumnIndexOrThrow(PRODUCT_SHARING));
                if(i == 0)
                    return false;
                if(i == 1)
                    return true;
            }
        }finally {
            if(c != null) {
                c.close();
            }
        }
        return true ;
    }
    public static boolean getPrintEnabled(Context context) {
        ContentResolver cr = context.getContentResolver();
        Cursor c = cr.query(BASEURI, null, null,null,null) ;
        try {
            int i = 0;
            if(c.moveToFirst())
            {
                i = c.getInt(c.getColumnIndexOrThrow(PRINT));
                if(i == 0)
                    return false;
                if(i == 1)
                    return true;
            }
        }finally {
            if(c != null) {
                c.close();
            }
        }
        return true ;
    }

    public static boolean getAddProdEnabled(Context context) {
        ContentResolver cr = context.getContentResolver();
        Cursor c = cr.query(BASEURI, null, null,null,null) ;
        try {
            int i = 0;
            if(c.moveToFirst())
            {
                i = c.getInt(c.getColumnIndexOrThrow(PRODUCT_ADD));
                if(i == 0)
                    return false;
                if(i == 1)
                    return true;
            }
        }finally {
            if(c != null) {
                c.close();
            }
        }
        return true ;
    }

    public static boolean getEditProdEnabled(Context context) {
        ContentResolver cr = context.getContentResolver();
        Cursor c = cr.query(BASEURI, null, null,null,null) ;
        try {
            int i = 0;
            if(c.moveToFirst())
            {
                i = c.getInt(c.getColumnIndexOrThrow(PRODUCT_EDIT));
                if(i == 0)
                    return false;
                if(i == 1)
                    return true;
            }
        }finally {
            if(c != null) {
                c.close();
            }
        }
        return true ;
    }

    public static boolean getAddCustomerEnabled(Context context) {
        ContentResolver cr = context.getContentResolver();
        Cursor c = cr.query(BASEURI, null, null,null,null) ;
        try {
            int i = 0;
            if(c.moveToFirst())
            {
                i = c.getInt(c.getColumnIndexOrThrow(CUSTOMER_ADD));
                if(i == 0)
                    return false;
                if(i == 1)
                    return true;
            }
        }finally {
            if(c != null) {
                c.close();
            }
        }
        return true ;
    }


    public static boolean getEditCustEnabled(Context context) {
        ContentResolver cr = context.getContentResolver();
        Cursor c = cr.query(BASEURI, null, null,null,null) ;
        try {
            int i = 0;
            if(c.moveToFirst())
            {
                i = c.getInt(c.getColumnIndexOrThrow(CUSTOMER_EDIT));
                if(i == 0)
                    return false;
                if(i == 1)
                    return true;
            }
        }finally {
            if(c != null) {
                c.close();
            }
        }
        return true ;
    }


    public static boolean getPuchItemEnabled(Context context) {
        ContentResolver cr = context.getContentResolver();
        Cursor c = cr.query(BASEURI, null, null,null,null) ;
        try {
            int i = 0;
            if(c.moveToFirst())
            {
                i = c.getInt(c.getColumnIndexOrThrow(STOCK_RECEIVE));
                if(i == 0)
                    return false;
                if(i == 1)
                    return true;
            }
        }finally {
            if(c != null) {
                c.close();
            }
        }
        return true ;
    }



    public static boolean getProshopLite(Context context) {
        ContentResolver cr = context.getContentResolver();
        Cursor c = cr.query(BASEURI, new String[]{PROSHOPLITE}, null,null,null) ;
        try {
            int i = 0;
            if(c.moveToFirst())
            {
                i = c.getInt(c.getColumnIndexOrThrow(PROSHOPLITE));
                if(i == 0)
                    return false;
                if(i == 1)
                    return true;
            }
        }finally {
            if(c != null) {
                c.close();
            }
        }
        return true ;
    }
    public static boolean getFormEnabled(Context context) {
        ContentResolver cr = context.getContentResolver();
        Cursor c = cr.query(BASEURI, new String[]{STATION_FORM}, null,null,null) ;
        try {
            int i = 0;
            if(c.moveToFirst())
            {
                i = c.getInt(c.getColumnIndexOrThrow(STATION_FORM));
                if(i == 0)
                    return false;
                if(i == 1)
                    return true;
            }
        }finally {
            if(c != null) {
                c.close();
            }
        }
        return true ;
    }
    public static boolean getExEnabled(Context context) {
        ContentResolver cr = context.getContentResolver();
        Cursor c = cr.query(BASEURI, new String[]{EXPENSES}, null,null,null) ;
        try {
            int i = 0;
            if(c.moveToFirst())
            {
                i = c.getInt(c.getColumnIndexOrThrow(EXPENSES));
                if(i == 0)
                    return false;
                if(i == 1)
                    return true;
            }
        }finally {
            if(c != null) {
                c.close();
            }
        }
        return true ;
    }
    public static boolean getAuditEnabled(Context context) {
        ContentResolver cr = context.getContentResolver();
        Cursor c = cr.query(BASEURI, new String[]{AUDIT}, null,null,null) ;
        try {
            int i = 0;
            if(c.moveToFirst())
            {
                i = c.getInt(c.getColumnIndexOrThrow(AUDIT));
                if(i == 0)
                    return false;
                if(i == 1)
                    return true;
            }
        }finally {
            if(c != null) {
                c.close();
            }
        }
        return true ;
    }
    public static boolean getPaymentEnabled(Context context) {
        ContentResolver cr = context.getContentResolver();
        Cursor c = cr.query(BASEURI, new String[]{PAYMENT}, null,null,null) ;
        try {
            int i = 0;
            if(c.moveToFirst())
            {
                i = c.getInt(c.getColumnIndexOrThrow(PAYMENT));
                if(i == 0)
                    return false;
                if(i == 1)
                    return true;
            }
        }finally {
            if(c != null) {
                c.close();
            }
        }
        return true ;
    }
    public static boolean getSignEnabled(Context context) {
        ContentResolver cr = context.getContentResolver();
        Cursor c = cr.query(BASEURI, new String[]{SIGNATURE}, null,null,null) ;
        try {
            int i = 0;
            if(c.moveToFirst())
            {
                i = c.getInt(c.getColumnIndexOrThrow(SIGNATURE));
                if(i == 0)
                    return false;
                if(i == 1)
                    return true;
            }
        }finally {
            if(c != null) {
                c.close();
            }
        }
        return true ;
    }
    public static boolean getStockEnabled(Context context) {
        ContentResolver cr = context.getContentResolver();
        Cursor c = cr.query(BASEURI, new String[]{STOCK}, null,null,null) ;
        try {
            int i = 0;
            if(c.moveToFirst())
            {
                i = c.getInt(c.getColumnIndexOrThrow(STOCK));
                if(i == 0)
                    return false;
                if(i == 1)
                    return true;
            }
        }finally {
            if(c != null) {
                c.close();
            }
        }
        return true ;
    }


    public static boolean getSalesEnabled(Context context) {
        ContentResolver cr = context.getContentResolver();
        Cursor c = cr.query(BASEURI, new String[]{SALES}, null,null,null) ;
        try {
            int i = 0;
            if(c != null && c.moveToFirst())
            {
                i = c.getInt(c.getColumnIndexOrThrow(SALES));
                if(i == 0)
                    return false;
                if(i == 1)
                    return true;
            }
        }finally {
            if(c != null) {
                c.close();
            }
        }
        return true ;
    }


    public static boolean getReportEnabled(Context context) {
        ContentResolver cr = context.getContentResolver();
        Cursor c = cr.query(BASEURI, new String[]{REPORTS}, null,null,null) ;
        try {
            int i = 0;
            if(c != null && c.moveToFirst())
            {
                i = c.getInt(c.getColumnIndexOrThrow(REPORTS));
                if(i == 0)
                    return false;
                if(i == 1)
                    return true;
            }
        }finally {
            if(c != null) {
                c.close();
            }
        }
        return true ;
    }


    public static boolean getProductsEnabled(Context context) {
        ContentResolver cr = context.getContentResolver();
        Cursor c = cr.query(BASEURI, new String[]{PRODUCTS}, null,null,null) ;
        try {
            int i = 0;
            if(c != null && c.moveToFirst())
            {
                i = c.getInt(c.getColumnIndexOrThrow(PRODUCTS));
                if(i == 0)
                    return false;
                if(i == 1)
                    return true;
            }
        }finally {
            if(c != null) {
                c.close();
            }
        }
        return true ;
    }



    public static boolean getCustomersEnabled(Context context) {
        ContentResolver cr = context.getContentResolver();
        Cursor c = cr.query(BASEURI, new String[]{CUSTOMERS}, null,null,null) ;
        try {
            int i = 0;
            if(c != null && c.moveToFirst())
            {
                i = c.getInt(c.getColumnIndexOrThrow(CUSTOMERS));
                if(i == 0)
                    return false;
                if(i == 1)
                    return true;
            }
        }finally {
            if(c != null) {
                c.close();
            }
        }
        return true ;
    }


    public static String getApptypeEnabled(Context context) {
        ContentResolver cr = context.getContentResolver();
        Cursor c = cr.query(BASEURI, null, null,null,null) ;
        try {
            String i = "";
            if(c.moveToFirst())
            {
                i = c.getString(c.getColumnIndexOrThrow(APPTYPE));
                    return i;
            }
        }finally {
            if(c != null) {
                c.close();
            }
        }
        return Constants.APPTYPE_SHOP ;
    }

    public static double getDiscountLimit(Context context) {
        ContentResolver cr = context.getContentResolver();
        Cursor c = cr.query(BASEURI, null, null,null,null) ;
        try {
            double i = 0;
            if(c.moveToFirst())
            {
                i = c.getDouble(c.getColumnIndexOrThrow(DISCOUNT_LIMIT));
                System.out.println(i);
                return i;
            }
        }finally {
            if(c != null) {
                c.close();
            }
        }
        return 0 ;
    }



    public String getApptype() {
        return apptype;
    }

    public double getDisclimit() {
        return disclimit;
    }

    public int getCustadd() {
        return custadd;
    }

    public int getProdadd() {
        return prodadd;
    }

    public int getProdsharing() {
        return prodsharing;
    }

    public int getStockrece() {
        return stockrece;
    }

    public int getCustedit() {
        return custedit;
    }

    public int getProdedit() {
        return prodedit;
    }

    public int getPrimport() {
        return primport;
    }

    public int getPrexport() {
        return prexport;
    }

    public int getPrint() {
        return print;
    }

    public int getTrack() {
        return track;
    }

    public int getDiscount() {
        return discount;
    }

    public int getTax() {
        return tax;
    }

    public int getId() {
        return id;
    }


}
