/*
 * Copyright (c) 2015.
 * Prosurvey is a property of Bongolive Enterprises Ltd. An authorized
 * copy of ideas, source codes is against the regulations
 */

package bongolive.apps.proshop.model;

import android.content.ContentResolver;
import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.net.Uri;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.math.BigDecimal;
import java.util.ArrayList;

import bongolive.apps.proshop.controller.Constants;
import bongolive.apps.proshop.controller.UrlCon;
import bongolive.apps.proshop.controller.Validating;

public class PurchaseItems
{
	public static final String TABLENAME = "purchase_items";
	public static final String ID = "_id";
	public static final String PRODUCT = "product";//local
    public static final String BATCH = "batch";
	public static final String WAREHOUSE = "warehouse";
	public static final String QUANTITY = "product_qty" ;//current_stock";
	public static final String EXPIRY = "expiry";
	public static final String TIMESTAMP = "timestamp";
	public static final String UNITPRICE = "unit_purch_price" ;//unit purchase price
	public static final String UPDATE = "last_update" ;
	public static final String SYS_PRODID = "prod_id" ;//"system_prod_id" ; //must exist before accepting this
	public static final String SYS_PURCHASID = "sys_purchase_id" ;//"Sys_stock_id" ;//0 if null
    public static final String ACK = "ack" ;//"Sys_stock_id" ;
	public static final Uri BASEURI = Uri.parse("content://"+ ContentProviderApi.AUTHORITY+"/"+TABLENAME) ;
	public static final String SPECIFIC_CONTENT_TYPE = "vnd.android.cursor.item/vnd.com.proshop.purchase_items" ;
	public static final String GENERAL_CONTENT_TYPE = "vnd.android.cursor.dir/vnd.com.proshop.purchase_items" ;
	private static final String TAG = PurchaseItems.class.getName();
	static UrlCon js ;
	public static String ARRAYNAME = "purch_items_list" ;
 
	private static JSONArray stockarray ;
	public PurchaseItems() {
		/*
		rules

		the purchase item can not be saved if it has an unknown warehouse or product
		a success save should return to the server the batch no and the purchase id
		* */
	}


    public static void sendAckJson(Context context) throws JSONException {
        JSONObject jobs = new JSONObject();
        jobs.put("tag", Constants.TAGACK);
        jobs.put(Constants.ACKITEM, TABLENAME);
        jobs.put(Constants.IMEI, Constants.getIMEINO(context));
        js = new UrlCon();
        System.out.println(jobs.toString());
        js.getJson(Constants.SERVER, jobs) ;
    }
	 
	 private static boolean isStockPresentFromSystem(Context context, String custid)
	    {
            ContentResolver cr = context.getContentResolver();
            boolean ret = false;
            String where = SYS_PURCHASID +" = "+custid;
            Cursor c = cr.query(BASEURI, null, where, null, null);
            try {
                if (c.moveToFirst()) {
                  ret = true ;
                }
            } finally {
                if (c != null) {
                    c.close();
                }
            }
            return ret;
	    }


    public static boolean updateWh(Context context,String wh)
    {
        ContentResolver cr = context.getContentResolver();
        ContentValues values = new ContentValues() ;
        values.put(WAREHOUSE, wh);
        return cr.update(BASEURI, values, null, null) > 0;
    }



    public static int getPurchaseItems(Context context, String stockid) {
		ContentResolver cr = context.getContentResolver();
		String where = PRODUCT + " = " + stockid ;
    	Cursor c = cr.query(BASEURI, new String[]{QUANTITY}, where, null, null) ;
    	try {
    	int i= 0 ;
    	if(c.moveToFirst())
    	{
    		do {
    			i = c.getInt(c.getColumnIndexOrThrow(QUANTITY));
    			 return i ;
    		}while(c.moveToNext());
    	} 
    	}finally {
			 if(c != null) {
		            c.close();
		        }
		 }
		return 0 ;
	}

    public static String getUnitprice(Context context, String prodid) {
		ContentResolver cr = context.getContentResolver();
		String where = PRODUCT + " = " + prodid ;
        String i= "0" ;
    	Cursor c = cr.query(BASEURI, new String[]{UNITPRICE}, where, null, null) ;
    	try {
    	if(c != null && c.moveToFirst()) {
            i = c.getString(c.getColumnIndexOrThrow(UNITPRICE));
        } else {
            i = Products.getUnitPriceBasedSysid(context, prodid);
        }
    	}finally {
			 if(c != null) {
		            c.close();
		        }
		 }
		return i ;
	}
	/*
	 *  function to update in the stock table to those products that are existing
	 */
	public static boolean updateProduct(Context context, String[] string, String source)
	{
		ContentResolver cr = context.getContentResolver(); 
		ContentValues values = new ContentValues() ;
        String where = null;
        if(source.equals(Constants.REMOTE)) {
            values.put(PRODUCT, string[0]);
            values.put(WAREHOUSE, string[2]);
            values.put(QUANTITY, string[3]);
            values.put(EXPIRY, string[4]);
            values.put(UNITPRICE, Double.parseDouble(string[5]));
            values.put(SYS_PRODID, string[6]);
            values.put(SYS_PURCHASID, string[7]);
            where = SYS_PURCHASID + " = "+string[7];
        } else if (source.equals(Constants.LOCAL)){
            values.put(PRODUCT, string[0]);
            values.put(WAREHOUSE, string[2]);
            values.put(QUANTITY, string[3]);
            values.put(EXPIRY, string[4]);
            values.put(UNITPRICE, Double.parseDouble(string[5]));
            where = ID + " = " +string[6];
        }
		if (cr.update(BASEURI, values, where, null) > 0) {
                return true;
            }

		return false;
	}/*
	 *  function to update in the stock table to those products that are existing
	 */
	public static boolean updateSysId(Context context, String[] string)
	{
		ContentResolver cr = context.getContentResolver();
		ContentValues values = new ContentValues() ;
        String where = SYS_PRODID +" = "+string[1];
            values.put(SYS_PURCHASID, string[0]);
            values.put(ACK, 1);
		if (cr.update(BASEURI, values, where, null) > 0) {
            System.out.println(" updated "+string[1]);
                return true;
            }

		return false;
	}


	/*
	 * Save a new product in the stock table
	 */
	public static boolean insert(Context context, String[] string, String source)
	{
		int productcount = getCount(context) ;
		ContentResolver cr = context.getContentResolver();
        ContentValues values = new ContentValues();
        if(source.equals(Constants.REMOTE)) {
            values.put(PRODUCT, string[0]);
            values.put(BATCH, string[1]);

            if(string[2].equals("0"))
                values.put(WAREHOUSE, Warehouse.getDefaultId(context));
            else
                values.put(WAREHOUSE, string[2]);

            values.put(QUANTITY, string[3]);
            values.put(EXPIRY, string[4]);
            values.put(UNITPRICE, Double.parseDouble(string[5]));
            values.put(SYS_PRODID, string[6]);
            values.put(SYS_PURCHASID, string[7]);
            values.put(ACK, 1);
            values.put(TIMESTAMP, string[8]);
        } else if (source.equals(Constants.LOCAL)){
            values.put(PRODUCT, string[0]);
            values.put(BATCH, string[1]);
            values.put(WAREHOUSE, string[2]);
            values.put(QUANTITY, string[3]);
            values.put(EXPIRY, string[4]);
            values.put(UNITPRICE, string[5]);
            values.put(TIMESTAMP, System.currentTimeMillis());
        }
		cr.insert(BASEURI, values);  
		if(getCount(context) == productcount+1){
			return true ;
		} else {
			return false ;
		}  
	}
	/*
	 * How many items are in the table ?
	 */
	public static int getCount(Context context){
		ContentResolver cr = context.getContentResolver() ;
		Cursor _c = cr.query(BASEURI, null, null, null, null);
		int count = _c.getCount() ;
		_c.close() ; 
		return count;
	}
	
	/*
	 * Fetching stock for a new product giving out imei of the device
	 */
	private static JSONObject getStockJson(Context context) throws JSONException
	{
        JSONObject jobs = new JSONObject();
        jobs.put("tag", Constants.TAGSGETPURCHASES);
        jobs.put(Constants.IMEI, Constants.getIMEINO(context));
        js = new UrlCon();
        String job =  js.getJson(Constants.SERVER, jobs) ;
        JSONObject json;
        try {
            json = new JSONObject(job);
            if(json.length() != 0)
            {
                return json ;
            } else {
            }
        } catch (JSONException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        return null ;
	}

	/*
	 * Acknowledgement to the server to tick item as a go
	 */
	public static void sendAcks(Context context, String[] requiredid) throws JSONException
    {
        JSONObject jobs = new JSONObject();
        jobs.put("tag", Constants.TAGACK);
        jobs.put(Constants.ACK, Constants.ACKVALUE);
        jobs.put(Constants.IMEI, Constants.getIMEINO(context));
        jobs.put(Constants.ACKREFERENCE, requiredid[0]);
        jobs.put(Constants.ACKKEY, SYS_PRODID);
        jobs.put(SYS_PURCHASID, requiredid[1]);
        jobs.put(BATCH, requiredid[2]);
        js = new UrlCon();
        js.getJson(Constants.SERVER, jobs) ;
	}

    private static ArrayList<String> getBatches(Context context){
        ArrayList<String> lst = new ArrayList<>();
        ContentResolver cr = context.getContentResolver() ;
        String where = ACK +" != 1";
        Cursor c = cr.query(BASEURI, new String[]{"DISTINCT "+BATCH}, where, null, null);
        try {
            if (c.moveToFirst()) {
                do {
                    lst.add(c.getString(c.getColumnIndexOrThrow(BATCH)));
                } while (c.moveToNext());
                return  lst;
            }
        } finally {
            if(c!= null)
                c.close();
        }
        return  lst;
    }

    public static ArrayList<String> getIds(Context context){
        ArrayList<String> lst = new ArrayList<>();
        ContentResolver cr = context.getContentResolver() ;
        String where = SYS_PRODID +" = 0" ;
        Cursor c = cr.query(BASEURI, new String[]{" DISTINCT "+PRODUCT}, where, null, null);
        try {
            if (c.moveToFirst()) {
                do {
                    lst.add(c.getString(c.getColumnIndexOrThrow(PRODUCT)));
                } while (c.moveToNext());
                return lst;
            }
        } finally {
            if(c!= null)
                c.close();
        }
        return  null;
    }

    public static void updatePurchaseItems(Context context){
        ArrayList<String> lst = getIds(context);
        ContentResolver cr = context.getContentResolver();

        if(lst == null || lst.size() == 0)
            return;
        for(int i = 0; i < lst.size(); i++ ) {
            ContentValues cv = new ContentValues();
            String prod = Products.getSysProdidBasedOnLocalId(context,lst.get(i));
            cv.put(SYS_PRODID, prod);
            String where = PRODUCT + " = " + lst.get(i)+" AND "+SYS_PRODID+" = 0";

            System.out.println( "updating "+cr.update(BASEURI, cv, where, null));
                System.out.println("updated "+lst.get(i));
        }
    }

    public static JSONObject sendItemsJson(Context context) throws JSONException
    {
        updatePurchaseItems(context);
        ArrayList<String> batch = getBatches(context);

        js = new UrlCon();

        ContentResolver cr = context.getContentResolver() ;
        JSONArray jarr = new JSONArray();
        JSONObject jsonObject = new JSONObject();
        jsonObject.put("tag", Constants.TAGSSENDPURCHASES);
        jsonObject.put(Constants.IMEI, Constants.getIMEINO(context));
        for(int i = 0; i< batch.size(); i++) {
            String where =  BATCH + " = " +batch.get(i) +" AND "+ACK + " != 1";

            Cursor c = cr.query(BASEURI, null, where, null, null);

            if (c != null && c.getCount() == 0) {
                c.close();
                return null;
            }
            try {

                if (c != null && c.moveToFirst()) {
                    do {
                        JSONObject j = new JSONObject();
                        String prod = c.getString(c.getColumnIndexOrThrow(SYS_PRODID));
                        String locid = c.getString(c.getColumnIndexOrThrow(PRODUCT));
                        if (prod.equals("0"))
                        {
                            continue;
                        }

                        j.put(ID, c.getString(c.getColumnIndexOrThrow(ID)));
                        j.put(PRODUCT, locid);
                        j.put(BATCH, c.getString(c.getColumnIndexOrThrow(BATCH)));
                        j.put(WAREHOUSE, c.getString(c.getColumnIndexOrThrow(WAREHOUSE)));
                        j.put(QUANTITY, c.getString(c.getColumnIndexOrThrow(QUANTITY)));
                        j.put(EXPIRY, c.getString(c.getColumnIndexOrThrow(EXPIRY)));
                        j.put(UNITPRICE, c.getString(c.getColumnIndexOrThrow(UNITPRICE)));
                        j.put(SYS_PRODID, prod);
                        j.put(TIMESTAMP, c.getString(c.getColumnIndexOrThrow(TIMESTAMP)));
                        jarr.put(j);
                    } while (c.moveToNext());
                    jsonObject.put(ARRAYNAME, (Object) jarr);
                }
            } finally {
                if (c != null) {
                    c.close();
                }
            }
        }
        if(jsonObject != null) {
            System.out.println(jsonObject.toString());
            String array = js.getJson(Constants.SERVER, jsonObject) ;
            JSONObject job = new JSONObject(array);
            return job;
        }
        return null;
    }

	public static void saveStock(Context context) throws JSONException
	{
        boolean whexcp  = false,prodexc = false;
		JSONObject json = getStockJson(context) ;
        if(json != null)
		try { 
			if(json.has(Constants.SUCCESS)){
			 String res = json.getString(Constants.SUCCESS) ;
			 if(Integer.parseInt(res) == 1)
			   {
                   if(json.has(ARRAYNAME)) {
                       stockarray = json.getJSONArray(ARRAYNAME) ;

                       String batch = Constants.generateBatch();
                       int success = 0;
                       for (int i = 0; i < stockarray.length(); i++) {
                           JSONObject js = stockarray.getJSONObject(i);
                           String ref = js.getString(SYS_PRODID);
                           String lc = Products.getLocalProdidBasedOnSystemId(context, ref);
                           int lcid = Integer.parseInt(lc);

                           if(lcid == 0) {
                               System.out.println("product not exist "+ref);
                               prodexc = true;
                               continue;
                           }
                           String wrh = js.getString(WAREHOUSE);
                           boolean checkwh = Warehouse.isWhPresent(context, wrh);
                           System.out.println(" check where "+ checkwh);

                           if(!checkwh) {
                               whexcp = true;
                               continue;
                           }

                           String stq = js.getString(QUANTITY);
                           int qty = new BigDecimal(stq).intValue();
                           stq = new BigDecimal(qty).toString();
                           String exp = js.getString(EXPIRY);
                           String upc = js.getString(UNITPRICE);
                           String stk = js.getString(SYS_PURCHASID);
                           String time = js.getString(TIMESTAMP);


                           int newqty = new BigDecimal(stq).intValue();
                           String[] stockvalue = new String[]{lc,batch,wrh,stq,exp,upc,ref,stk,time};
                           if (Validating.areSet(stockvalue)) {

                               String[] reqired = new String[]{ref, stk, batch};
                               int currpurchaseqty = getPurchaseItemQty(context,reqired,Constants.REMOTE);
                               System.out.println("current "+currpurchaseqty+" new "+newqty);
                               boolean var = isStockPresentFromSystem(context,stk);
                               System.out.println("exists? "+var);
                               int itemsonhand = Products.getProductQuantity(context, lcid);
                               int purchqty = new BigDecimal(newqty).subtract(new BigDecimal(itemsonhand))
                                       .intValue();
                               stk = String.valueOf(purchqty);
                               String[] vals = new String[]{lc,batch,wrh,stq,exp,upc,ref,stk,time};
                               if(!var) {
                                   if (insert(context, vals, Constants.REMOTE)) {
                                       if(Products.updateQtyPurchaseItems(context,new int[]{lcid,newqty})) {
                                           success += 1;
                                       }
                                   }
                               } else {
                                   int qtyitemsbalance = new BigDecimal(newqty).subtract(new BigDecimal
                                           (currpurchaseqty)).intValue();
                                   int newprodbalance = new BigDecimal(itemsonhand).subtract(new BigDecimal
                                           (qtyitemsbalance)).intValue();
                                   System.out.println("current stock balance "+qtyitemsbalance+" new stock "+newqty+"" +
                                           " current product qty "+itemsonhand+" quantity balance "+newprodbalance);
                                   if (updateProduct(context, vals, Constants.REMOTE)) {
                                       if(Products.updateQtyPurchaseItems(context, new int[]{lcid, newprodbalance})) {
                                           success += 1;
                                       }
                                   }
                               }
                           }
                       }
                       if(success == stockarray.length())
                           sendAckJson(context);
                       if(prodexc)
                           Products.sendErrorJson(context, new String[]{Products.TABLENAME, "13"});
                       if(whexcp)
                           Products.sendErrorJson(context, new String[]{Warehouse.TABLENAME, "13"});
                   }
		       }
			 }
				 
		 } catch (JSONException e) { 
						e.printStackTrace();
	 }


        JSONObject jsonout = sendItemsJson(context) ;
        if(jsonout != null)
        try {
            if(jsonout.has(Constants.SUCCESS)){
                String res = jsonout.getString(Constants.SUCCESS) ;
                if(Integer.parseInt(res) == 1)
                {
                    if(jsonout.has(ARRAYNAME)) {
                        stockarray = jsonout.getJSONArray(ARRAYNAME) ;

                        String batch = Constants.generateBatch();
                        for (int i = 0; i < stockarray.length(); i++) {
                            JSONObject js = stockarray.getJSONObject(i);
                            String ref = js.getString(SYS_PRODID);
                            String stk = js.getString(SYS_PURCHASID);
                            String[] stockvalue = new String[]{stk,ref};
                            if (Validating.areSet(stockvalue)) {
                                updateSysId(context,stockvalue);
                            }
                        }
                    }
                }
            }

        } catch (JSONException e) {
            e.printStackTrace();
        }
	}

    public static int getPurchaseItemQty(Context context, String[] vls, String source)
    {
        ContentResolver cr = context.getContentResolver();
        String where = null;
        if(source.equals(Constants.REMOTE))
         where = SYS_PURCHASID + " = " + vls[1] + " AND "+SYS_PRODID + " = " +vls[0]  ;
        if(source.equals(Constants.LOCAL))
         where = ID + " = " + vls[0] + " AND "+PRODUCT + " = " +vls[1]  ;

        Cursor cursor = cr.query(BASEURI, new String[]{QUANTITY}, where, null, null) ;
        try{
            if(cursor.moveToFirst())
            {
                do{
                    int quantity = cursor.getInt(cursor.getColumnIndexOrThrow(QUANTITY));
                    return quantity;
                } while (cursor.moveToNext());
            }
        }finally {
            if(cursor != null) {
                cursor.close();
            }
        }
        return 0;
    }

	public static String[] getAllStock(Context context)
    {
        ContentResolver cr = context.getContentResolver();
        Cursor cursor = cr.query(BASEURI, null, null, null, null) ;
        if(cursor.getCount() > 0)
        {
            String[] str = new String[cursor.getCount()];
            int i = 0;
 
            while (cursor.moveToNext())
            {
                 str[i] = cursor.getString(cursor.getColumnIndex(BATCH));
                 i++;
             } 
            cursor.close();
            return str; 
        }
        else
        {  
            cursor.close();
            return new String[] {};
        }
    }
	
	public static int getAllStockQuantity(Context context, String productname)
    {
        ContentResolver cr = context.getContentResolver();
        String where = PRODUCT + " LIKE \'%" + productname + "%\'"  ;
        Cursor cursor = cr.query(BASEURI, new String[] {QUANTITY}, where, null, null) ;
        try{
        if(cursor.moveToFirst())
        {
        	do{
        		int quantity = cursor.getInt(cursor.getColumnIndexOrThrow(QUANTITY));
        		return quantity;
        	} while (cursor.moveToNext());
        } 
        }finally {
			 if(cursor != null) {
			        cursor.close();
		        }
		 }
        return 0;
    }

}