/*
 * The source codes in this project are owned by Bongolive Enterprises. Copying or any kind of usage without Bongolive's approval is against the law.
 * Copyright (c) 2015.
 *
 */

package bongolive.apps.proshop.model;

import android.content.ContentResolver;
import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.DatabaseUtils;
import android.database.sqlite.SQLiteDatabase;
import android.net.Uri;
import android.util.Log;

import com.jjoe64.graphview.series.DataPoint;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.ArrayList;

import bongolive.apps.proshop.controller.Constants;
import bongolive.apps.proshop.controller.UrlCon;
import bongolive.apps.proshop.controller.Validating;


public class StockIssue {
    public static final String TABLENAME = "stock_issue" ;
    public static final String ID = "_id";
    public static final String CUSTOMERID = "customer_id";// required to send system_customer_id
    public static final String LOCALCUSTOMER = "loc_customer_id";// required to send system_customer_id
    public static final String ORDERDATE = "sales_date";
    public static final String PAYMENTSTATUS ="payment_status" ;
    public static final String PAYMENTAMOUNT ="paid" ;
    public static final String COMMENT ="note" ;
    public static final String TOTALSALES = "total_sales" ;
    public static final String GRANDTOTAL = "grand_total" ;
    public static final String TOTALDISCOUNT = "total_discount" ;
    public static final String ITEMS = "total_items" ;
    public static final String SYS_SALE = "sy_sale_id";
    public static final String SALESTATUS = "sale_status";
    public static final String SALE_REFERENCE = "sale_refid";//SALE/2015/06/0008
    public static final String TOTALTAX = "total_tax";
    public static final String ACK = "ack" ;
    public static final String ISPRINTED = "is_printed" ;
    public static final String LAT = "lat";
    public static final String LONGTUDE = "lng";
    public static final String SALE_TYPE = "sale_type" ;
    public static final String TAXSETTINGS = "tax_setting";

    public static final Uri BASEURI = Uri.parse("content://"+ ContentProviderApi.AUTHORITY+"/"+TABLENAME) ;
    public static final String SPECIFIC_CONTENT_TYPE = "vnd.android.cursor.item/vnd.com.proshop.stock_issue" ;
    public static final String GENERAL_CONTENT_TYPE = "vnd.android.cursor.dir/vnd.com.proshop.stock_issue" ;
    private static final String ARRAYNAME = "sales_list";

    long id;
    int customer,ack,status ;
    double pay,totalsales,totaltax,totaldiscount ;
    String date,paystatus,saleref,custlocal ;
    static UrlCon js = null ;

    public StockIssue() {
    }

    public static boolean insert(Context context, String[] orders, String source){
        int ordercount = getCount(context) ;
        ContentResolver cr = context.getContentResolver() ;
        ContentValues cv = new ContentValues() ;
        if(source.equals(Constants.REMOTE)) {
            cv.put(CUSTOMERID, orders[0]);
            cv.put(ORDERDATE, orders[1]);
            cv.put(PAYMENTAMOUNT, orders[2]);
            cv.put(TOTALSALES, orders[3]);
            cv.put(TOTALTAX, orders[4]);
            cv.put(ACK, 1);
            cv.put(TOTALDISCOUNT, orders[5]);
            cv.put(ITEMS, orders[6]);
            cv.put(SYS_SALE, orders[7]);
            cv.put(SALE_REFERENCE, orders[8]);
            cv.put(LAT, orders[9]);
            cv.put(LONGTUDE, orders[10]);//comm,taxsett,isprint,paystat,loca
            cv.put(COMMENT, orders[11]);
            cv.put(TAXSETTINGS, orders[12]);
            cv.put(ISPRINTED, orders[13]);
            cv.put(PAYMENTSTATUS, orders[14]);
            cv.put(LOCALCUSTOMER, orders[15]);
            cv.put(GRANDTOTAL, orders[16]);
            cv.put(SALESTATUS, 1);
            cv.put(SALESTATUS, orders[17]);
            cv.put(SALE_TYPE, orders[18]);
            System.out.println("sale status " + orders[17]);
        } else if(source.equals(Constants.LOCAL)) {
            cv.put(CUSTOMERID, orders[0]);
            cv.put(ORDERDATE, Constants.getDate());
            cv.put(PAYMENTAMOUNT, orders[1]);
            double ts = new BigDecimal(orders[2]).subtract(new BigDecimal(orders[3])).setScale(2, RoundingMode
                    .HALF_UP).doubleValue();
            cv.put(TOTALSALES, ts);
            cv.put(TOTALTAX, orders[3]);
            cv.put(TOTALDISCOUNT, orders[4]);
            cv.put(ITEMS, orders[5]);
            cv.put(SYS_SALE, 0);
            cv.put(SALE_REFERENCE, orders[6]);
            cv.put(LAT, orders[12]);
            cv.put(LONGTUDE, orders[13]);
            cv.put(COMMENT, orders[7]);
            cv.put(TAXSETTINGS, orders[8]);
            cv.put(ISPRINTED, orders[9]);
            cv.put(PAYMENTSTATUS, orders[10]);
            cv.put(LOCALCUSTOMER, orders[11]);
            cv.put(GRANDTOTAL, orders[2]);
            cv.put(SALESTATUS, orders[14]);
            cv.put(SALE_TYPE, orders[15]);
            System.out.println(" sale type is " + orders[15]);
        }
        cr.insert(BASEURI, cv);
        return getCount(context) == ordercount + 1;
    }
    //gettotaltax,getpayamount
    public static int getCount(Context context){
        ContentResolver cr = context.getContentResolver() ;
        Cursor _c = cr.query(BASEURI, null, null, null, null);
        int count = _c.getCount() ;
        _c.close() ;
        return count;
    }

    private static ArrayList<String> getIds(Context context){
        ArrayList<String> lst = new ArrayList<>();
        ContentResolver cr = context.getContentResolver() ;
        String where = CUSTOMERID +" = 0" ;
        Cursor c = cr.query(BASEURI, new String[]{" DISTINCT "+LOCALCUSTOMER}, where, null, null);
        try {
            if (c.moveToFirst()) {
                do {
                    lst.add(c.getString(c.getColumnIndexOrThrow(LOCALCUSTOMER)));
                } while (c.moveToNext());
                return lst;
            }
        } finally {
            if(c!= null)
                c.close();
        }
        return  null;
    }


    public static String getLocalSaleId(Context context, String str) {
        ContentResolver cr = context.getContentResolver();
        String where = SALE_REFERENCE + " = " + DatabaseUtils.sqlEscapeString(str);

        Cursor c = cr.query(BASEURI, new String[]{ID}, where, null, null);
        try {
            String cu = null;
            if (c.moveToFirst()) {
                cu = c.getString(c.getColumnIndexOrThrow(ID));
                return cu;
            }
        } finally {
            if (c != null) {
                c.close();
            }
        }
        return "0";
    }

    public static double getDebt(Context context, String str) {
        ContentResolver cr = context.getContentResolver();
        String where = ID + " = " + str;
        double debt = 0;
        Cursor c = cr.query(BASEURI, null, where, null, null);
        try {
            if(c != null && c.moveToFirst()) {
                double cu = 0, gt = 0;
                cu = c.getDouble(c.getColumnIndex(PAYMENTAMOUNT));
                gt = c.getDouble(c.getColumnIndex(GRANDTOTAL));
                Log.e("DEBT"," total "+gt+" paid "+cu);
                debt = new BigDecimal(gt).subtract(new BigDecimal(cu)).setScale(2, RoundingMode.HALF_UP).doubleValue();
            }
        } finally {
            if (c != null) {
                c.close();
            }
        }
        return debt;
    }

    public static double getPaid(Context context, String str) {
        ContentResolver cr = context.getContentResolver();
        String where = ID + " = " + str;
        double debt = 0;
        Cursor c = cr.query(BASEURI, new String[]{PAYMENTAMOUNT}, where, null, null);
        try {
            if (c != null && c.moveToFirst()) {
                debt = c.getDouble(c.getColumnIndexOrThrow(PAYMENTAMOUNT));
            }
        } finally {
            if (c != null) {
                c.close();
            }
        }
        return debt;
    }

    public static String getLocalSale(Context context, String str) {
        ContentResolver cr = context.getContentResolver();
        String where = SYS_SALE +" = "+str;

        Cursor c = cr.query(BASEURI, new String[]{ID}, where, null, null);
        try {
            String cu = null;
            if (c.moveToFirst()) {
                cu = c.getString(c.getColumnIndexOrThrow(ID));
                return cu;
            }
        } finally {
            if (c != null) {
                c.close();
            }
        }
        return "0";
    }

    private static void updateSaleItems(Context context){
        ArrayList<String> lst = getIds(context);
        ContentResolver cr = context.getContentResolver();

        if(lst == null || lst.size() == 0)
            return;
        for(int i = 0; i < lst.size(); i++ ) {
            ContentValues cv = new ContentValues();
            String prod = Customers.getSysCustidBasedOnLocalId(context, lst.get(i));
            cv.put(CUSTOMERID, prod);
            String where = LOCALCUSTOMER + " = " + lst.get(i)+" AND "+CUSTOMERID+" = 0";

            System.out.println( "updating "+cr.update(BASEURI, cv, where, null));
            System.out.println("updated "+lst.get(i));
        }
    }
    /*
         * Fetching sales from server if sent frJsonObjectJsonArrayom this device
         */
    public static JSONObject getSaleJson(Context context) throws JSONException
    {
        JSONObject jobs = new JSONObject();
        jobs.put("tag", Constants.TAGSRECEIVESALES);
        jobs.put(Constants.IMEI, Constants.getIMEINO(context));
         js = new UrlCon();
        String job =  js.getJson(Constants.SERVER,jobs);
        JSONObject json;
        try {
            json = new JSONObject(job);
            if(json != null)
            {
                return json ;
            }
        } catch (JSONException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        return null ;
    }


    public static boolean updateSaleStatus(Context context, long id)
    {
        ContentResolver cr = context.getContentResolver();
        boolean ret = false;
        String  where = ID +" = "+id;
        ContentValues cv = new ContentValues(2);
        cv.put(SALESTATUS, Constants.SALE_STATUS_CMPLT);
        cv.put(ACK, 2);
        if(cr.update(BASEURI,cv,where,null) > 0)
            ret = true;

        return ret;
    }

    public static boolean updatePaymentStatus(Context context, double gt, String id)
    {
        ContentResolver cr = context.getContentResolver();
        boolean ret = false;
        String  where = ID +" = "+id;
        ContentValues cv = new ContentValues(2);
        cv.put(PAYMENTAMOUNT, gt);
        cv.put(ACK, 0);
        if(cr.update(BASEURI,cv,where,null) > 0) {
//            Log.e("PAYMENTUPDATE","Payment is updated new payment "+gt);
            ret = true;
        }

        return ret;
    }


    public static boolean isSalePending(Context context, long id)
    {
        ContentResolver cr = context.getContentResolver();
        boolean ret = false;
        String where =  ID +" = "+id+" AND "+SALESTATUS+" = "+DatabaseUtils.sqlEscapeString(Constants
                    .SALE_STATUS_PNDG);

        Cursor c = cr.query(BASEURI, null, where, null, null);
        try {
            if (c.moveToFirst()) {
                ret = true ;
            }
        } finally {
            if (c != null) {
                c.close();
            }
        }
        return ret;
    }

    public static JSONObject sendJson(Context context) throws JSONException
    {
        updateSaleItems(context);
        ContentResolver cr = context.getContentResolver() ;
        String where = ACK + " = 0" ;
        Cursor c = cr.query(BASEURI, null, where, null, null);
        if(c.getCount() == 0){
            c.close();
            return null;
        }
         js = new UrlCon();
        try {
            JSONObject jsonObject = new JSONObject();
            jsonObject.put("tag", Constants.TAGSSENDSTOCKISUED);
            jsonObject.put(Constants.IMEI, Constants.getIMEINO(context));
            String array ;
            if(c.moveToFirst())
            {
                JSONArray jarr = new JSONArray();
                do{
                    JSONObject j = new JSONObject();
                    String custmerid = c.getString(c.getColumnIndexOrThrow(CUSTOMERID));
                    if(custmerid.equals("0"))
                        continue;
                    j.put(CUSTOMERID, custmerid);
                    j.put(ORDERDATE, c.getString(c.getColumnIndexOrThrow(ORDERDATE)));
                    j.put(PAYMENTAMOUNT, c.getString(c.getColumnIndexOrThrow(PAYMENTAMOUNT)));
                    j.put(TOTALSALES, c.getString(c.getColumnIndexOrThrow(TOTALSALES)));
                    j.put(GRANDTOTAL, c.getString(c.getColumnIndexOrThrow(GRANDTOTAL)));
                    j.put(TOTALTAX, c.getString(c.getColumnIndexOrThrow(TOTALTAX)));
                    j.put(ACK, c.getString(c.getColumnIndexOrThrow(ACK)));
                    j.put(TOTALDISCOUNT, c.getString(c.getColumnIndexOrThrow(TOTALDISCOUNT)));
                    j.put(ITEMS, c.getString(c.getColumnIndexOrThrow(ITEMS)));
                    j.put(SALE_REFERENCE, c.getString(c.getColumnIndexOrThrow(SALE_REFERENCE)));
                    j.put(LAT, c.getString(c.getColumnIndexOrThrow(LAT)));
                    j.put(LONGTUDE, c.getString(c.getColumnIndexOrThrow(LONGTUDE)));
                    j.put(COMMENT, c.getString(c.getColumnIndexOrThrow(COMMENT)));
                    j.put(TAXSETTINGS, c.getString(c.getColumnIndexOrThrow(TAXSETTINGS)));
                    j.put(ISPRINTED, c.getString(c.getColumnIndexOrThrow(ISPRINTED)));
                    j.put(PAYMENTSTATUS, c.getString(c.getColumnIndexOrThrow(PAYMENTSTATUS)));
                    j.put(ID, c.getString(c.getColumnIndexOrThrow(ID)));
                    j.put(SYS_SALE, c.getString(c.getColumnIndexOrThrow(SYS_SALE)));
                    j.put(SALESTATUS, c.getString(c.getColumnIndexOrThrow(SALESTATUS)));
                    j.put(SALE_TYPE, c.getString(c.getColumnIndexOrThrow(SALE_TYPE)));
                    jarr.put(j);
                } while(c.moveToNext()) ;
                jsonObject.put(ARRAYNAME, jarr);
                System.out.println(jsonObject.toString());
                array = js.getJson(Constants.SERVER, jsonObject) ;
                JSONObject job =  new JSONObject(array);
                return job ;
            }
        }finally {
            if(c != null) {
                c.close();
            }
        }
        return null ;
    }
    /* check if the sale id is present in the app
    *
    */

    private static boolean isSalePresentFromSystem(Context context, String custid)
    {
        ContentResolver cr = context.getContentResolver();
        boolean ret = false;
        String where = SYS_SALE +" = "+custid;
        Cursor c = cr.query(BASEURI, null, where, null, null);
        try {
            if (c.moveToFirst()) {
                ret = true ;
            }
        } finally {
            if (c != null) {
                c.close();
            }
        }
        return ret;
    }


    public static String getCustomerLocal(Context context, String ordid) {
        String where = ID +" = " +ordid;
        ContentResolver cr = context.getContentResolver();
        Cursor c = cr.query(BASEURI, new String[] {LOCALCUSTOMER}, where,null,null) ;
        try {
            String i= "0" ;
            if(c.moveToFirst())
            {
                i = c.getString(c.getColumnIndexOrThrow(LOCALCUSTOMER));
                return i ;
            }
        }finally {
            if(c != null) {
                c.close();
            }
        }
        return "0" ;
    }

    public static String getSaleRef(Context context, String ordid) {
        String where = ID +" = " +ordid;
        ContentResolver cr = context.getContentResolver();
        Cursor c = cr.query(BASEURI, new String[]{SALE_REFERENCE}, where, null, null) ;
        try {
            String i= "0" ;
            if(c.moveToFirst())
            {
                i = c.getString(c.getColumnIndexOrThrow(SALE_REFERENCE));
                return i ;
            }
        }finally {
            if(c != null) {
                c.close();
            }
        }
        return "0" ;
    }

    public static boolean columnExist(SQLiteDatabase context,String column){
        boolean ret= false;
        String sql = " SELECT * FROM "+TABLENAME  +" LIMIT 1";

        Cursor c = context.rawQuery(sql, null);
        try {
                int indext = c.getColumnIndex(column);
                System.out.println(" index is " +indext);
                if(indext != -1)
                    ret = true;
        }finally {
            if(c != null) {
                c.close();
            }
        }
        return ret;
    }
    /*
    * Acknowledgement to the server to tick item as a go
    */
    public static void sendAck(Context context, String[] requiredid) throws JSONException
    {
        JSONObject jobs = new JSONObject();
        jobs.put("tag", Constants.TAGACK);
        jobs.put(Constants.ACK, Constants.ACKVALUE);
        jobs.put(Constants.IMEI, Constants.getIMEINO(context));
        jobs.put(Constants.ACKREFERENCE, requiredid[0]);
        jobs.put(Constants.ACKKEY, SYS_SALE);
         js = new UrlCon();
        js.getJson(Constants.SERVER,jobs);
    }

    public static boolean updateSync(Context context, String[] vals)
    {
        ContentResolver cr = context.getContentResolver();
        ContentValues values = new ContentValues() ;
        values.put(ACK, vals[0]);
        values.put(SYS_SALE, vals[2]);
        // read more on contenturis and update
        String where =  ID+ " = "+ vals[1] ;
        return cr.update(BASEURI, values, where, null) == 1;
    }

    public static int isOrderSynced(Context context, String businessnm)
    {
        ContentResolver cr = context.getContentResolver();
        String where = ID + " = " + businessnm ;

        Cursor c = cr.query(BASEURI, new String[] {ACK}, where,null,null) ;
        try {
            int i= 0 ;
            if(c.moveToFirst())
            {
                i = c.getInt(c.getColumnIndexOrThrow(ACK));
                return i ;
            }
        } finally {
            if(c != null) {
                c.close();
            }
        }
        return 0;
    }

    public static void process(Context context){
        JSONObject jsonissue = null;
        try {
            jsonissue = sendJson(context);
        if(jsonissue != null)
            try {
                if(jsonissue.has(Constants.SUCCESS)){
                    String res = jsonissue.getString(Constants.SUCCESS) ;
                    if(Integer.parseInt(res) == 1)
                    {
                        if(jsonissue.has(ARRAYNAME)){
                            JSONArray jsonArray = jsonissue.getJSONArray(ARRAYNAME);
                            for(int i = 0; i< jsonArray.length(); i++) {
                                JSONObject jsonObject1 = jsonArray.getJSONObject(i);
                                String orderid = jsonObject1.getString(ID);
                                String saleid = jsonObject1.getString(SYS_SALE);
                                if(saleid.equals("0") || !Validating.areSet(saleid))
                                    continue;
                                String[] v = {"1",orderid,saleid};
                                if(Validating.areSet(v)) {
                                    boolean OK = StockIssue.updateSync(context, v);
                                    if (OK) {
                                        System.out.println("updated sale "+saleid);
                                    }
                                }
                            }
                        }
                    }
                }
            } catch (NumberFormatException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            } catch (JSONException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }


        JSONObject jsonitemissue = null;
        try {
            jsonitemissue = StockIssueItems.sendJson(context);

        if(jsonitemissue != null)
            try {
                if(jsonitemissue.has(Constants.SUCCESS)){
                    String res = jsonitemissue.getString(Constants.SUCCESS) ;
                    if(Integer.parseInt(res) == 1)
                    {
                        if(jsonitemissue.has(StockIssueItems.ARRAYNAME)){
                            JSONArray jsonArray = jsonitemissue.getJSONArray(StockIssueItems.ARRAYNAME);
                            for(int i = 0; i< jsonArray.length(); i++) {
                                JSONObject jsonObject1 = jsonArray.getJSONObject(i);
                                String ref = String.valueOf(jsonObject1.get(StockIssueItems.ORDERID));

                                String sysref = String.valueOf(jsonObject1.get("sys_sales_item_id"));
                                System.out.println(sysref);
                                if(sysref.equals("0") || ref.equals("0")){
                                    Products.sendErrorJson(context,new String[]{StockIssueItems.TABLENAME,"0"});
                                    StockIssueItems.updateSaleItemsOnError(context);
                                    continue;
                                }
                                boolean OK = StockIssueItems.updateSync(context, Integer.parseInt(ref), 1, sysref);
                                if (OK) {
                                    System.out.println("updated "+ref);
                                }
                            }
                        }

                    }
                }
            } catch (NumberFormatException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            } catch (JSONException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }
    public static int getId(Context context) {
        ContentResolver cr = context.getContentResolver();
        String where = SYS_SALE + " = 0";
        Cursor c = cr.query(BASEURI, new String[] {ID}, where,null,null) ;
        try {
            int i= 0 ;
            if(c != null && c.moveToLast())
            {
                i = c.getInt(c.getColumnIndexOrThrow(ID));
                return i ;
            }
        }finally {
            if(c != null) {
                c.close();
            }
        }
        return 0 ;
    }

    public static void sendAckJson(Context context) throws JSONException {
        JSONObject jobs = new JSONObject();
        jobs.put("tag", Constants.TAGACK);
        jobs.put(Constants.ACKITEM, TABLENAME);
        jobs.put(Constants.IMEI, Constants.getIMEINO(context));
         js = new UrlCon();
        System.out.println(jobs.toString());
        js.getJson(Constants.SERVER,jobs);
    }

    public static String getSaleDate(Context context, String ordid) {
        String where = ID +" = " +ordid;
        ContentResolver cr = context.getContentResolver();
        Cursor c = cr.query(BASEURI, null, where,null,null) ;
        try {
            String i= "0" ;
            if(c != null && c.moveToFirst())
            {
                i = c.getString(c.getColumnIndexOrThrow(ORDERDATE));

                return Constants.getFormatedDate(i) ;
            }
        }finally {
            if(c != null) {
                c.close();
            }
        }
        return "0" ;
    }

    public static String getPayerableAmount(Context context, String ordid) {
        String where = ID +" = " +ordid;
        ContentResolver cr = context.getContentResolver();
        Cursor c = cr.query(BASEURI, null, where,null,null) ;
        try {
            String i= "0" ;
            if(c != null && c.moveToFirst())
            {
                i = c.getString(c.getColumnIndexOrThrow(GRANDTOTAL));

                return i ;
            }
        }finally {
            if(c != null) {
                c.close();
            }
        }
        return "0" ;
    }

    public static String getCustomer(Context context, String ordid) {
        String where = ID +" = " +ordid;
        ContentResolver cr = context.getContentResolver();
        Cursor c = cr.query(BASEURI, new String[] {CUSTOMERID}, where,null,null) ;
        try {
            String i= "0" ;
            if(c.moveToFirst())
            {
                i = c.getString(c.getColumnIndexOrThrow(CUSTOMERID));
                return i ;
            }
        }finally {
            if(c != null) {
                c.close();
            }
        }
        return "0" ;
    }

    public static double getDailySummary(Context context, int flag){
        ContentResolver cr = context.getContentResolver() ;
        String where = null ;
        switch(flag)
        {
            case 0:
                where = ORDERDATE + " >=  date('now')";

                break;
            case 1:
                where = ORDERDATE + " >= date('now','-7 day')";
                //date('now','start of month','+0 month','0 day') AND "+ ORDERDATE + " <= date('now','start of month','+0 month','-1 day')";
                break;
            case 2:
                where = ORDERDATE + " >=  date('now','start of month','-1 month') AND "+
                        ORDERDATE + " < date('now','start of month','-1 day')";
                //date('now','start of month','+1 month','-1 day')
                break;
        }
        Cursor c = cr.query(BASEURI, null, where, null, null);
        double total = 0;
        try{
            if(c.moveToFirst()){
                do{
                    total += c.getDouble(c.getColumnIndexOrThrow(PAYMENTAMOUNT));
                } while (c.moveToNext());
                return total ;
            }
        }finally{
            if(c != null) {
                c.close();
            }
        }
        return 0;
    }

    public static DataPoint[] getTotalSalesGraph(Context context){

        DataPoint[] vals = null;
        String query = " select sum(case  cast(strftime('%w',"+ORDERDATE+ ") as integer) when 0 then IFNULL("+GRANDTOTAL+"," +
                " 0)   else 0  end) as sunday ," +
                "sum(case  cast(strftime('%w',"+ORDERDATE+") as integer) when 1 then IFNULL("+GRANDTOTAL+",0)  else 0 end) as" +
                " monday ," +
                "sum(case  cast(strftime('%w',"+ORDERDATE+") as integer) when 2 then IFNULL("+GRANDTOTAL+",0)   else 0 end) as tuesday ," +
                "sum(case  cast(strftime('%w',"+ORDERDATE+") as integer) when 3 then IFNULL("+GRANDTOTAL+",0)   else 0 end) as wednesday ," +
                "sum(case  cast(strftime('%w',"+ORDERDATE+") as integer) when 4 then IFNULL("+GRANDTOTAL+",0)   else 0 end) as thursday ," +
                "sum(case  cast(strftime('%w',"+ORDERDATE+") as integer) when 5 then IFNULL("+GRANDTOTAL+",0)   else 0 end) as friday ," +
                "sum(case  cast(strftime('%w',"+ORDERDATE+") as integer) when 6 then IFNULL("+GRANDTOTAL+",0)   else 0 end) as saturday " +
                "from "+TABLENAME+" where  strftime('%W',"+ORDERDATE+") = strftime('%W','now') group by strftime('%W','now') ";
        final Cursor c1 = DatabaseHandler.getInstance(context).getReadableDatabase().rawQuery(query, null);
        if (c1 != null) {
            vals = new DataPoint[c1.getCount()];
            try {
                if(c1.moveToFirst()) {
                    double mon = 0, tues = 0, wed = 0, thur = 0, frd = 0, sat = 0, sun = 0;
                    mon = c1.getDouble(c1.getColumnIndex("monday"));
                    tues = c1.getDouble(c1.getColumnIndex("tuesday"));
                    wed = c1.getDouble(c1.getColumnIndex("wednesday"));
                    thur = c1.getDouble(c1.getColumnIndex("thursday"));
                    frd = c1.getDouble(c1.getColumnIndex("friday"));
                    sat = c1.getDouble(c1.getColumnIndex("saturday"));
                    sun = c1.getDouble(c1.getColumnIndex("sunday"));
                    vals = new DataPoint[]{new DataPoint(0, sun), new DataPoint(1, mon), new DataPoint(2, tues),
                            new DataPoint(3, wed), new DataPoint(4, thur), new DataPoint(5, frd), new DataPoint(6, sat)};
                }
            } finally {
                c1.close();
            }
        }
        return vals;
    }

    public static DataPoint[] getTotalOutstandingGraph(Context context){

        DataPoint[] vals = null;
        String query = " select sum(case  cast(strftime('%w',"+ORDERDATE+ ") as integer) when 0 then IFNULL("+PAYMENTAMOUNT+"," +
                " 0)   else 0  end) as sunday ," +
                "sum(case  cast(strftime('%w',"+ORDERDATE+") as integer) when 1 then IFNULL("+PAYMENTAMOUNT+",0)  else 0 end) as" +
                " monday ," +
                "sum(case  cast(strftime('%w',"+ORDERDATE+") as integer) when 2 then IFNULL("+PAYMENTAMOUNT+",0)   else 0 end) as tuesday ," +
                "sum(case  cast(strftime('%w',"+ORDERDATE+") as integer) when 3 then IFNULL("+PAYMENTAMOUNT+",0)   else 0 end) as wednesday ," +
                "sum(case  cast(strftime('%w',"+ORDERDATE+") as integer) when 4 then IFNULL("+PAYMENTAMOUNT+",0)   else 0 end) as thursday ," +
                "sum(case  cast(strftime('%w',"+ORDERDATE+") as integer) when 5 then IFNULL("+PAYMENTAMOUNT+",0)   else 0 end) as friday ," +
                "sum(case  cast(strftime('%w',"+ORDERDATE+") as integer) when 6 then IFNULL("+PAYMENTAMOUNT+",0)   else 0 end) as saturday " +
                "from "+TABLENAME+" where  strftime('%W',"+ORDERDATE+") = strftime('%W','now') group by strftime('%W','now') ";
        final Cursor c1 = DatabaseHandler.getInstance(context).getReadableDatabase().rawQuery(query, null);
        if (c1 != null) {
            vals = new DataPoint[c1.getCount()];
            try {
                if(c1.moveToFirst()) {
                    double mon = 0, tues = 0, wed = 0, thur = 0, frd = 0, sat = 0, sun = 0;
                    mon = c1.getDouble(c1.getColumnIndex("monday"));
                    tues = c1.getDouble(c1.getColumnIndex("tuesday"));
                    wed = c1.getDouble(c1.getColumnIndex("wednesday"));
                    thur = c1.getDouble(c1.getColumnIndex("thursday"));
                    frd = c1.getDouble(c1.getColumnIndex("friday"));
                    sat = c1.getDouble(c1.getColumnIndex("saturday"));
                    sun = c1.getDouble(c1.getColumnIndex("sunday"));
                    vals = new DataPoint[]{new DataPoint(0, sun), new DataPoint(1, mon), new DataPoint(2, tues),
                            new DataPoint(3, wed), new DataPoint(4, thur), new DataPoint(5, frd), new DataPoint(6, sat)};
                }
            } finally {
                c1.close();
            }
        }
        return vals;
    }


    public static int putPrintStatus(Context context, int[] data)
    {
        ContentResolver cr = context.getContentResolver();
        ContentValues values = new ContentValues() ;
        values.put(ISPRINTED, data[1]);

        String where =  ID+ " = "+ data[0] ;
        if(cr.update(BASEURI, values, where, null) > 0)
        {
            return 1 ;
        }
        return 0;
    }

    public static double getGrandTotal(Context context, long status) {
        ContentResolver cr = context.getContentResolver();
        String where = ID + " = " + status;
        Cursor c = cr.query(BASEURI, new String[] {TOTALSALES}, where,null,null) ;
        try {
            double i= 0 ;
            if(c.moveToFirst())
            {
                i = c.getDouble(c.getColumnIndexOrThrow(TOTALSALES));
                return i ;
            }
        }finally {
            if(c != null) {
                c.close();
            }
        }
        return 0 ;
    }
    public static double getTaxAmount(Context context, long status) {
        ContentResolver cr = context.getContentResolver();
        String where = ID + " = " + status;
        Cursor c = cr.query(BASEURI, new String[] {TOTALTAX}, where,null,null) ;
        try {
            double i= 0 ;
            if(c.moveToFirst())
            {
                i = c.getDouble(c.getColumnIndexOrThrow(TOTALTAX));
                return i ;
            }
        }finally {
            if(c != null) {
                c.close();
            }
        }
        return 0 ;
    }

    public static int getPrintStatus(Context context, long status) {
        ContentResolver cr = context.getContentResolver();
        String where = ID + " = " + status;
        Cursor c = cr.query(BASEURI, new String[] {ISPRINTED}, where,null,null) ;
        try {
            int i= 0 ;
            if(c.moveToFirst())
            {
                i = c.getInt(c.getColumnIndexOrThrow(ISPRINTED));
                return i ;
            }
        }finally {
            if(c != null) {
                c.close();
            }
        }
        return 0 ;
    }


    public static ArrayList<StockIssue> getItems(Context context){
        ArrayList<StockIssue> list = new ArrayList<>();
        ContentResolver cr = context.getContentResolver();
        Cursor c = cr.query(BASEURI,null,null,null,ID+" DESC");
        try{
            if(c != null && c.getCount() > 0){
                c.moveToFirst();
                do {
                    String sl = c.getString(c.getColumnIndex(SYS_SALE));
                    String lsl = c.getString(c.getColumnIndex(LOCALCUSTOMER));
                    String wh = c.getString(c.getColumnIndex(ORDERDATE));
                    String st = c.getString(c.getColumnIndex(SALE_TYPE));
                    double pd = c.getDouble(c.getColumnIndex(GRANDTOTAL));
                    String md = c.getString(c.getColumnIndex(SALE_REFERENCE));
                    int ack = c.getInt(c.getColumnIndex(ACK));
                    long i = c.getLong(c.getColumnIndex(ID));
                    list.add(new StockIssue(lsl,md,wh,pd,ack,i));
                } while (c.moveToNext());
            }
        } finally {
            if(c != null){
                c.close();
            }
        }
        return list;
    }



    public String getCustlocal() {
        return custlocal;
    }

    public String getSaleref() {
        return saleref;
    }

    public String getPaystatus() {
        return paystatus;
    }

    public String getDate() {
        return date;
    }

    public double getTotaldiscount() {
        return totaldiscount;
    }

    public double getTotaltax() {
        return totaltax;
    }

    public double getTotalsales() {
        return totalsales;
    }

    public double getPay() {
        return pay;
    }

    public int getStatus() {
        return status;
    }

    public int getAck() {
        return ack;
    }

    public int getCustomer() {
        return customer;
    }

    public long getId() {
        return id;
    }

    public StockIssue(String custlocal, String saleref, String date, double totalsales, int ack, long id) {
        this.custlocal = custlocal;
        this.saleref = saleref;
        this.date = date;
        this.totalsales = totalsales;
        this.ack = ack;
        this.id = id;
    }
}
