/*
 * The source codes in this project are owned by Bongolive Enterprises. Copying or any kind of usage without Bongolive's approval is against the law.
 * Copyright (c) 2015.
 *
 */

package bongolive.apps.proshop.model;

import android.content.ContentResolver;
import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.DatabaseUtils;
import android.database.sqlite.SQLiteDatabase;
import android.net.Uri;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import bongolive.apps.proshop.controller.Constants;
import bongolive.apps.proshop.controller.ReportsItem;
import bongolive.apps.proshop.controller.UrlCon;
import bongolive.apps.proshop.controller.Validating;


public class Station {
	public static final String TABLENAME = "station" ;
	public static final String ID = "_id";
	public static final String METERIN = "meter_in";// required to send system_customer_id
	public static final String METEROUT = "meter_out";// required to send system_customer_id
	public static final String CREATEDON = "created_on";
	public static final String TIMEIN ="time_in" ;
	public static final String TIMEOUT ="time_out" ;
	public static final String COMMENT ="comment" ;
	public static final String CASHIN = "cash_in" ;
	public static final String CASHOUT = "cash_out" ;
	public static final String CREATEDBY = "created_by" ;
	public static final String OPERATOR = "operator_name" ;
	public static final String UPDATEDON = "updated_on" ;
	public static final String SYS_ID = "station_id";
	public static final String SOURCE = "source";
	public static final String SIGNATURE = "signature";
	public static final String STATIONNAME = "station_name";
	public static final String ACK = "ack" ;
	public static final String TIMESTAMP = "timestamp" ;

	public static final Uri BASEURI = Uri.parse("content://"+ ContentProviderApi.AUTHORITY+"/"+TABLENAME) ;
	public static final String SPECIFIC_CONTENT_TYPE = "vnd.android.cursor.item/vnd.com.proshop.station" ;
	public static final String GENERAL_CONTENT_TYPE = "vnd.android.cursor.dir/vnd.com.proshop.station" ;
	private static final String ARRAYNAME = "station_list";

	int id,customer,ack,status ;
	double pay,totalsales,totaltax,totaldiscount ;
	String date,paystatus ;
	static UrlCon js = null ;

	public Station() {

	}
	public Station(int id, int cus, int sy, double paynt, double sales, double tax  , String dt, String status,int st_){
		this.id =id ;
		this.customer = cus ;
		this.ack = sy ;
		this.totalsales = sales ;
		this.totaltax = tax ;
		this.date = dt ;
		this.paystatus = status ;
		this.pay = paynt;
		this.status = st_;
	}

	public static boolean insert(Context context, String[] orders, String source){
		int ordercount = getCount(context) ;
		ContentResolver cr = context.getContentResolver() ;
		ContentValues cv = new ContentValues() ;
		if(source.equals(Constants.REMOTE)) {
			cv.put(METERIN, orders[0]);
            cv.put(METEROUT, orders[1]);
			cv.put(TIMEOUT, orders[2]);
            cv.put(TIMEIN, orders[3]);
			cv.put(CASHIN, orders[4]);
            cv.put(CASHOUT, orders[5]);
			cv.put(ACK, 1);
			cv.put(SYS_ID, orders[6]);
			cv.put(SIGNATURE, orders[7]);
			cv.put(COMMENT, orders[8]);
			cv.put(TIMESTAMP, orders[9]);
            cv.put(CREATEDBY, orders[10]);
            cv.put(CREATEDON, orders[11]);
            cv.put(STATIONNAME, orders[12]);
			cv.put(SOURCE, orders[13]) ;
			cv.put(OPERATOR, orders[14]) ;
			cv.put(UPDATEDON, Constants.getDate());
		} else if(source.equals(Constants.LOCAL)) {
            cv.put(METERIN, orders[0]);
            cv.put(METEROUT, orders[1]);
            cv.put(TIMEOUT, orders[2]);
            cv.put(TIMEIN, orders[3]);
            cv.put(CASHIN, orders[4]);
            cv.put(CASHOUT, orders[5]);
            cv.put(STATIONNAME, orders[6]);
            cv.put(CREATEDBY, orders[7]);
            cv.put(SIGNATURE, orders[8]);
            cv.put(COMMENT, orders[9]);
            cv.put(TIMESTAMP, orders[10]);
			cv.put(OPERATOR, orders[11]) ;
		}
		cr.insert(BASEURI, cv);
		return getCount(context) == ordercount + 1;
	}
	public static boolean update(Context context, String[] orders, String source){
		ContentResolver cr = context.getContentResolver() ;
		ContentValues cv = new ContentValues() ;
        String where = null;
		if(source.equals(Constants.REMOTE)) {
            cv.put(METERIN, orders[0]);
            cv.put(METEROUT, orders[1]);
            cv.put(TIMEOUT, orders[2]);
            cv.put(TIMEIN, orders[3]);
            cv.put(CASHIN, orders[4]);
            cv.put(CASHOUT, orders[5]);
            cv.put(ACK, 1);
            cv.put(SIGNATURE, orders[7]);
            cv.put(COMMENT, orders[8]);
            cv.put(TIMESTAMP, orders[9]);
            cv.put(CREATEDBY, orders[10]);
            cv.put(CREATEDON, orders[11]);
            cv.put(STATIONNAME, orders[14]);
            cv.put(SOURCE, orders[13]) ;
			cv.put(OPERATOR, orders[14]) ;
            cv.put(UPDATEDON, Constants.getDate());
            where = SYS_ID + " = "+ orders[6] ;
		} else if(source.equals(Constants.LOCAL)) {
            cv.put(METERIN, orders[0]);
            cv.put(METEROUT, orders[1]);
            cv.put(TIMEOUT, orders[2]);
            cv.put(TIMEIN, orders[3]);
            cv.put(CASHIN, orders[4]);
            cv.put(CASHOUT, orders[5]);
            cv.put(STATIONNAME, orders[6]);
            cv.put(CREATEDBY, orders[7]);
            cv.put(SIGNATURE, orders[8]);
            cv.put(COMMENT, orders[9]);
            cv.put(TIMESTAMP, orders[10]);
			cv.put(OPERATOR, orders[12]) ;
            where = ID + " = "+ orders[11] ;
		}
		int up = cr.update(BASEURI, cv, where, null);
		if(up > 0 )
            return true;
        return false;
	}
	//gettotaltax,getpayamount
	public static int getCount(Context context){
		ContentResolver cr = context.getContentResolver() ;
		Cursor _c = cr.query(BASEURI, null, null, null, null);
		int count = _c.getCount() ;
		_c.close() ;
		return count;
	}

	/*
         * Fetching sales from server if sent frJsonObjectJsonArrayom this device
         */
	public static JSONObject getFormJson(Context context) throws JSONException
	{
		JSONObject jobs = new JSONObject();
		jobs.put("tag", Constants.TAGSGETDFORM);
		jobs.put(Constants.IMEI, Constants.getIMEINO(context));
		js = new UrlCon();
		String job =  js.getJson(Constants.SERVER, jobs);
		JSONObject json;
		try {
			json = new JSONObject(job);
			if(json != null)
			{
				return json ;
			}
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null ;
	}

	public static JSONObject sendFormJson(Context context) throws JSONException
	{
		ContentResolver cr = context.getContentResolver() ;
		String where = ACK + " = 0" ;
		Cursor c = cr.query(BASEURI, null, where, null, null);
		if(c== null || c.getCount() == 0){
			c.close();
			return null;
		}
		js = new UrlCon();
		JSONObject jsonObject = new JSONObject();
		JSONObject j = new JSONObject();
		JSONArray jarr = new JSONArray();
		String array ;
		try {
			jsonObject.put("tag", Constants.TAGSSENDFORM);
			jsonObject.put(Constants.IMEI, Constants.getIMEINO(context));
			if(c.moveToFirst())
			{
				do{
					j.put(METERIN, c.getString(c.getColumnIndexOrThrow(METERIN)));
					j.put(METEROUT, c.getString(c.getColumnIndexOrThrow(METEROUT)));
					j.put(CREATEDON, c.getString(c.getColumnIndexOrThrow(CREATEDON)));
					j.put(TIMEOUT, c.getString(c.getColumnIndexOrThrow(TIMEOUT)));
					j.put(CASHIN, c.getString(c.getColumnIndexOrThrow(CASHIN)));
					j.put(CASHOUT, c.getString(c.getColumnIndexOrThrow(CASHOUT)));
					j.put(STATIONNAME, Warehouse.getDefaultId(context));
					j.put(ACK, c.getString(c.getColumnIndexOrThrow(ACK)));
					j.put(CREATEDBY, c.getString(c.getColumnIndexOrThrow(CREATEDBY)));
					j.put(UPDATEDON, c.getString(c.getColumnIndexOrThrow(UPDATEDON)));
					j.put(SIGNATURE, c.getString(c.getColumnIndexOrThrow(SIGNATURE)));
					j.put(COMMENT, c.getString(c.getColumnIndexOrThrow(COMMENT)));
					j.put(TIMESTAMP, c.getString(c.getColumnIndexOrThrow(TIMESTAMP)));
					j.put(TIMEIN, c.getString(c.getColumnIndexOrThrow(TIMEIN)));
					j.put(ID, c.getString(c.getColumnIndexOrThrow(ID)));
					j.put(SYS_ID, c.getString(c.getColumnIndexOrThrow(SYS_ID)));
					j.put(SOURCE, c.getString(c.getColumnIndexOrThrow(SOURCE)));
					j.put(OPERATOR, c.getString(c.getColumnIndexOrThrow(OPERATOR)));
					JSONArray jsonArray = StationPump.getJsonArray(context,c.getString(c.getColumnIndex(ID)));
					j.put(StationPump.ARRAYNAME,jsonArray);
					jarr.put(j);
				} while(c.moveToNext()) ;
			}
		}finally {
			if(c != null) {
				c.close();
			}
		}
		jsonObject.put(ARRAYNAME, jarr);
		System.out.println(jsonObject.toString());
		array = js.getJson(Constants.SERVER, jsonObject) ;
		JSONObject job =  new JSONObject(array);
		return job ;
	}


    public static String getLocalId(Context context, String sysid)
    {
        ContentResolver cr = context.getContentResolver();
        String where = SYS_ID +  " = " + sysid;
        Cursor cursor = cr.query(BASEURI, new String[] {ID}, where, null, null) ;
        try{
            if(cursor != null && cursor.moveToFirst())
            {
                    String quantity = cursor.getString(cursor.getColumnIndexOrThrow(ID));
                    return quantity;
            }
        }finally {
            if(cursor != null) {
                cursor.close();
            }
        }
        return "0";
    }


    public static void processInformation(Context context) throws JSONException{

		JSONObject jsonin = getFormJson(context);
//		JSONObject jsonin = new JSONObject("{\"success\":1,\"station_list\":[{\"station_id\":\"7\",\"station_name\":\"60\",\"wh_name\":\"Nasibu Wh( WH\\/907820222373 )\",\"meter_in\":\"20020\",\"meter_out\":\"19020\",\"time_in\":\"2016-02-29 11:18\",\"time_out\":\"2016-02-29 18:18\",\"cash_in\":\"1200000\",\"cash_out\":\"2300000\",\"source\":\"web\",\"signature\":null,\"comment\":\"No comment kaka\",\"timestamp\":\"1456733914\",\"created_by\":\"66\",\"created_on\":\"2016-02-29 08:18:34\",\"operator_name\":\"Kimoka wa Mbola\",\"station_pump\":[{\\\"id\\\":\\\"8\\\",\\\"name\\\":\\\"Pump1\\\"}]}]}");
		if(jsonin != null)
			try {
				if(jsonin.has(Constants.SUCCESS)){
					String res = jsonin.getString(Constants.SUCCESS) ;
					if(Integer.parseInt(res) == 1)
					{
						if(jsonin.has(ARRAYNAME)) {
							JSONArray jsarray = jsonin.getJSONArray(ARRAYNAME);
							int success = 0;

							for (int i = 0; i < jsarray.length(); i++) {
								JSONObject j = jsarray.getJSONObject(i);
								String min = j.getString(METERIN);
								String mout = j.getString(METEROUT);
								String tin = j.getString(TIMEIN);
								String tout = j.getString(TIMEOUT);
                                String cin = j.getString(CASHIN);
                                String cout = j.getString(CASHOUT);
								String con = j.getString(CREATEDON);
								String sn = j.getString(STATIONNAME);
								String cby = j.getString(CREATEDBY);
								String sysid = j.getString(SYS_ID);
								String sign = j.getString(SIGNATURE);
								String note = j.getString(COMMENT);
								String isprint = j.getString(TIMESTAMP);
								String src = j.getString(SOURCE);
								String opr = j.getString(OPERATOR);

								String[] vals = {min,mout,tin,tout,cin,cout,sysid,sign,note,isprint,cby,con,sn,
                                        src,opr};
								String[] chk = {min,mout,tin,tout,cin,cout,sysid,isprint,cby,con,isprint};
								if (Validating.areSet(chk)) {
									boolean var = isFormPresentFromSystem(context, sysid);
									System.out.println("present? " + var);
									if (!var) {
										if (insert(context, vals, Constants.REMOTE)) {
                                            String locid = Station.getLocalId(context,sysid);
                                            JSONArray jsonArray = j.getJSONArray(StationPump.ARRAYNAME);
                                            for(int k = 0; k< jsonArray.length(); k++){
                                                JSONObject jsonObject = jsonArray.getJSONObject(k);
                                                String nm = jsonObject.getString(StationPump.NAME);
                                                String syid = jsonObject.getString(StationPump.SYS_ID);
                                                String[] v = {syid,nm,locid};
                                                StationPump.insert(context,v,Constants.REMOTE);
                                            }
											success += 1;
										}
									} else {
                                        if (update(context, vals, Constants.REMOTE)) {
                                            String locid = Station.getLocalId(context,sysid);
                                            JSONArray jsonArray = j.getJSONArray(StationPump.ARRAYNAME);
                                            for(int k = 0; k< jsonArray.length(); k++){
                                                JSONObject jsonObject = jsonArray.getJSONObject(k);
                                                String nm = jsonObject.getString(StationPump.NAME);
                                                String syid = jsonObject.getString(StationPump.SYS_ID);
                                                String[] v = {syid,nm,locid};
                                                StationPump.insert(context,v,Constants.REMOTE);
                                            }
                                            success += 1;
                                        }
									}
								}
							}
							if (success == jsarray.length() && jsarray.length() >0)
								sendAckJson(context);
						}
					}
				}
			} catch (NumberFormatException e) {
				e.printStackTrace();
			} catch (JSONException e) {
				e.printStackTrace();
			}




		JSONObject jsonout = sendFormJson(context);
		if(jsonout != null)
			try {
				if(jsonout.has(Constants.SUCCESS)){
					String res = jsonout.getString(Constants.SUCCESS) ;
					if(Integer.parseInt(res) == 1)
					{
						if(jsonout.has(ARRAYNAME)){
							JSONArray jsonArray = jsonout.getJSONArray(ARRAYNAME);
							for(int i = 0; i< jsonArray.length(); i++) {
								JSONObject jsonObject1 = jsonArray.getJSONObject(i);
								String orderid = jsonObject1.getString(TIMESTAMP);
								String saleid = jsonObject1.getString(SYS_ID);
								if(saleid.equals("0") || !Validating.areSet(saleid))
									continue;
								String[] v = {"1",orderid,saleid};
								if(Validating.areSet(v)) {
									boolean OK = updateSync(context, v);
									if (OK) {
										System.out.println("updated form "+saleid);
									}
								}
							}
						}
					}
				}
			} catch (NumberFormatException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (JSONException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

	}

    /* check if the sale id is present in the app
    *
    */

	private static boolean isFormPresentFromSystem(Context context, String custid)
	{
		ContentResolver cr = context.getContentResolver();
		boolean ret = false;
		String where = SYS_ID +" = "+custid;
		Cursor c = cr.query(BASEURI, null, where, null, null);
		try {
			if (c.moveToFirst()) {
				ret = true ;
			}
		} finally {
			if (c != null) {
				c.close();
			}
		}
		return ret;
	}

	public static boolean columnExist(SQLiteDatabase context,String column){
		boolean ret= false;
		String sql = " SELECT * FROM "+TABLENAME  +" LIMIT 1";

		Cursor c = context.rawQuery(sql, null);
		try {
			int indext = c.getColumnIndex(column);
			System.out.println(" index is " +indext);
			if(indext != -1)
				ret = true;
		}finally {
			if(c != null) {
				c.close();
			}
		}
		return ret;
	}

	public static boolean updateSync(Context context, String[] vals)
	{
		ContentResolver cr = context.getContentResolver();
		ContentValues values = new ContentValues() ;
		values.put(ACK, vals[0]);
		values.put(SYS_ID, vals[2]);
		values.put(UPDATEDON, Constants.getDate());
		String where =  TIMESTAMP+ " = "+ DatabaseUtils.sqlEscapeString(vals[1]);
		return cr.update(BASEURI, values, where, null) == 1;
	}

	public static int isFormSynced(Context context, String id)
	{
		ContentResolver cr = context.getContentResolver();
		String where = ID + " = " + id ;

		Cursor c = cr.query(BASEURI, new String[] {ACK}, where,null,null) ;
		try {
			int i= 0 ;
			if(c.moveToFirst())
			{
				i = c.getInt(c.getColumnIndexOrThrow(ACK));
				return i ;
			}
		} finally {
			if(c != null) {
				c.close();
			}
		}
		return 0;
	}

	public static int getId(Context context) {
		ContentResolver cr = context.getContentResolver();
		Cursor c = cr.query(BASEURI, new String[] {ID}, null,null,null) ;
		try {
			int i= 0 ;
			if(c != null && c.moveToLast())
			{
				i = c.getInt(c.getColumnIndexOrThrow(ID));
				return i ;
			}
		}finally {
			if(c != null) {
				c.close();
			}
		}
		return 0 ;
	}



	public static void sendAckJson(Context context) throws JSONException {
		JSONObject jobs = new JSONObject();
		jobs.put("tag", Constants.TAGACK);
		jobs.put(Constants.ACKITEM, TABLENAME);
		jobs.put(Constants.IMEI, Constants.getIMEINO(context));
		js = new UrlCon();
		System.out.println(jobs.toString());
		js.getJson(Constants.SERVER,jobs);
	}

	private static ArrayList<ReportsItem> getItems(Context context, long id){
		ArrayList<ReportsItem> lst = new ArrayList<>();
		ContentResolver cr = context.getContentResolver() ;
		String where = ID +" = "+id ;
		Cursor c = cr.query(BASEURI, null, where, null, null);
		try {
			if (c.moveToFirst()) {

			}
		} finally {
			if(c!= null)
				c.close();
		}
		return  lst;
	}

}
