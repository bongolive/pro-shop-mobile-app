/*
 * The source codes in this project are owned by Bongolive Enterprises. Copying or any kind of usage without Bongolive's approval is against the law.
 * Copyright (c) 2015.
 *
 */

package bongolive.apps.proshop.model;

import android.content.ContentResolver;
import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.net.Uri;
import android.util.Log;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import bongolive.apps.proshop.controller.Constants;
import bongolive.apps.proshop.controller.UrlCon;
import bongolive.apps.proshop.controller.Validating;

public class AuditResponses {
    public static final String TABLENAME = "audit_response";
    public static final String ID = "_id";
    public static final String PRODUCTNO = "product_id" ;
    public static final String AUDITID = "audit_id" ;
    public static final String AUDITOPTIONID = "audit_option_id" ;
    public static final String LOCALPRODUCTNO = "local_prod" ;
    public static final String RESPONSE = "option_value" ;
    public static final String CREATED = "created_on" ;
    public static final String UPDATED = "updated_on" ;
    public static final String TIMESTAMP = "timestamp" ;
    public static final String CUSTOMER = "customer_id" ;
    public static final String CUSTOMERLOCAL = "customer_local" ;
    public static final String ACK = "ack" ;
    /*
    *
    * */
    public static final Uri BASEURI = Uri.parse("content://"+ ContentProviderApi.AUTHORITY+"/"+TABLENAME) ;
    public static final String SPECIFIC_CONTENT_TYPE = "vnd.android.cursor.item/vnd.com.proshop.audit_response" ;
    public static final String GENERAL_CONTENT_TYPE = "vnd.android.cursor.dir/vnd.com.proshop.audit_response" ;
    public static String ARRAYNAME= "audit_list" ;
    static UrlCon js;

    String prod,prodl,optid,cust,custl,resp,ack;
    long id, auid;
    public AuditResponses(){

    }

    public static int getCount(Context context){
        ContentResolver cr = context.getContentResolver() ;
        Cursor _c = cr.query(BASEURI, null, null, null, null);
        int count = _c.getCount() ;
        _c.close() ;
        return count;
    }
    public static boolean insert(Context context, String[] items){
        int itemcount = getCount(context);

        ContentResolver cr = context.getContentResolver() ;
        ContentValues cv = new ContentValues() ;
        cv.put(PRODUCTNO, items[0]);
        cv.put(LOCALPRODUCTNO, items[1]);
        cv.put(AUDITOPTIONID, items[2]);
        cv.put(AUDITID, items[3]);
        cv.put(CUSTOMER, items[4]);
        cv.put(CUSTOMERLOCAL, items[5]);
        cv.put(RESPONSE, items[6]);
        cr.insert(BASEURI, cv);
        if(getCount(context) == itemcount + 1) {
            Log.e("AURDITRESPONSE","Inserted");
            return true;
        }
        return false;
    }


    public static boolean getCustResponse(Context context, long[] items)
    {
        ContentResolver cr = context.getContentResolver();
        boolean ret = false;
        String where = AUDITID +" = "+ items[0] + " AND "+LOCALPRODUCTNO + " = "+items[1] + " AND "+CUSTOMERLOCAL +
                " = "+items[2];
//        System.out.print("where "+where);
        Cursor c = cr.query(BASEURI, null, where, null, null);
        try {
            if (c != null && c.moveToFirst()) {
                ret = true ;
            }
        }  finally {
            if (c != null) {
                c.close();
            }
        }
        return ret;
    }

    public static boolean update(Context context, String[] items){
        ContentResolver cr = context.getContentResolver() ;
        ContentValues cv = new ContentValues() ;
        cv.put(PRODUCTNO, items[0]);
        cv.put(LOCALPRODUCTNO, items[1]);
        cv.put(AUDITOPTIONID, items[2]);
        cv.put(AUDITID, items[3]);
        cv.put(CUSTOMER, items[4]);
        cv.put(CUSTOMERLOCAL, items[5]);
        cv.put(RESPONSE, items[6]);
        cv.put(UPDATED, Constants.getDate());
        String where = AUDITID +" = "+ items[3] + " AND "+LOCALPRODUCTNO + " = "+items[1] + " AND "+AUDITOPTIONID + " = " +
                items[2];
        int up = cr.update(BASEURI, cv, where, null);
        if(up > 0) {
            Log.e("AURDITRESPONSE","Updated");
            return true;
        }

        return false ;
    }


    public static boolean isResponseExisting(Context context, String[] items)
    {
        ContentResolver cr = context.getContentResolver();
        boolean ret = false;
        String where = AUDITID +" = "+ items[3] + " AND "+LOCALPRODUCTNO + " = "+items[1] + " AND "+CUSTOMERLOCAL +
                " = "+items[5]  + " AND "+AUDITOPTIONID + " = "+items[2];
        Cursor c = cr.query(BASEURI, null, where, null, null);
        try {
            if (c != null && c.moveToFirst()) {
                ret = true ;
            }
        } finally {
            if (c != null) {
                c.close();
            }
        }
        return ret;
    }
    public static boolean columnExist(SQLiteDatabase context,String column){
        boolean ret= false;
        String sql = " SELECT * FROM "+TABLENAME  +" LIMIT 1";

        Cursor c = context.rawQuery(sql, null);
        try {
            int indext = c.getColumnIndex(column);
            System.out.println(" index is " +indext);
            if(indext != -1)
                ret = true;
        }finally {
            if(c != null) {
                c.close();
            }
        }
        return ret;
    }

    /* get all product id with system product id 0*/
    private static ArrayList<String> getIdsOnError(Context context){
        ArrayList<String> lst = new ArrayList<>();
        ContentResolver cr = context.getContentResolver() ;
        Cursor c = cr.query(BASEURI, new String[]{" DISTINCT " + LOCALPRODUCTNO}, null, null, null);
        try {
            if (c.moveToFirst()) {
                do {
                    lst.add(c.getString(c.getColumnIndexOrThrow(LOCALPRODUCTNO)));
                } while (c.moveToNext());
                return lst;
            }
        } finally {
            if(c!= null)
                c.close();
        }
        return  null;
    }

    private static void updateAuditOnError(Context context){
        ArrayList<String> lst = getIdsOnError(context);
        ContentResolver cr = context.getContentResolver();

        if(lst == null || lst.size() == 0)
            return;
        for(int i = 0; i < lst.size(); i++ ) {
            ContentValues cv = new ContentValues();
            String prod = Products.getSysProdidBasedOnLocalId(context,lst.get(i));
            cv.put(PRODUCTNO, prod);
            String where = LOCALPRODUCTNO + " = " + lst.get(i);

            System.out.println( "updating "+cr.update(BASEURI, cv, where, null));
            System.out.println("updated "+lst.get(i));
        }
    }


    public static JSONObject sendAuditJson(Context context) throws JSONException
    {
        ContentResolver cr = context.getContentResolver() ;
        String where = ACK + " = 0" ;
        Cursor c = cr.query(BASEURI, null, where, null, null);
        if(c.getCount() == 0){
            c.close();
            return null;
        }
         js = new UrlCon();
        try {
            JSONObject jsonObject = new JSONObject();
            jsonObject.put("tag", Constants.TAGSSENDAUDIT);
            jsonObject.put(Constants.IMEI, Constants.getIMEINO(context));
            String array ;
            if(c.moveToFirst())
            {
                JSONArray jarr = new JSONArray();
                do{
                    JSONObject j = new JSONObject();
                    int prod = c.getInt(c.getColumnIndexOrThrow(PRODUCTNO));
                    int cust = c.getInt(c.getColumnIndexOrThrow(CUSTOMER));
                    if(prod == 0)
                        continue;
                    if(cust == 0)
                        continue;
                    j.put(PRODUCTNO, String.valueOf(prod));
                    j.put(ID, c.getString(c.getColumnIndexOrThrow(ID)));
                    j.put(LOCALPRODUCTNO, c.getString(c.getColumnIndexOrThrow(LOCALPRODUCTNO)));
                    j.put(RESPONSE, c.getString(c.getColumnIndexOrThrow(RESPONSE))) ;
                    j.put(CREATED, c.getString(c.getColumnIndexOrThrow(CREATED))) ;
                    j.put(AUDITOPTIONID, c.getString(c.getColumnIndexOrThrow(AUDITOPTIONID))) ;
                    j.put(AUDITID, c.getString(c.getColumnIndexOrThrow(AUDITID))) ;
                    j.put(CUSTOMER, String.valueOf(cust)) ;
                    j.put(CUSTOMERLOCAL, c.getString(c.getColumnIndexOrThrow(CUSTOMERLOCAL))) ;
                    j.put(TIMESTAMP, c.getString(c.getColumnIndexOrThrow(TIMESTAMP))) ;
                    jarr.put(j);
                } while(c.moveToNext()) ;
                if(jarr.length() == c.getCount()) {
                    jsonObject.put(ARRAYNAME, jarr);
                    System.out.println(jsonObject.toString());
                    array = js.getJson(Constants.SERVER, jsonObject);
                    JSONObject job = new JSONObject(array);
                    return job;
                }
            }
        }finally {
            if(c != null) {
                c.close();
            }
        }
        return null ;
    }

    public static void processInformation(Context context) throws JSONException{

        JSONObject jsonout = sendAuditJson(context);
        if(jsonout != null)
            try {
                if(jsonout.has(Constants.SUCCESS)){
                    String res = jsonout.getString(Constants.SUCCESS) ;
                    if(Integer.parseInt(res) == 1)
                    {
                        if(jsonout.has(ARRAYNAME)){
                            JSONArray jsonArray = jsonout.getJSONArray(ARRAYNAME);
                            for(int i = 0; i< jsonArray.length(); i++) {
                                JSONObject jsonObject1 = jsonArray.getJSONObject(i);
                                String aud = String.valueOf(jsonObject1.get(AUDITID));
                                String cus = String.valueOf(jsonObject1.get(CUSTOMER));
                                String opt = String.valueOf(jsonObject1.get(AUDITOPTIONID));
                                String prod = String.valueOf(jsonObject1.get(PRODUCTNO));
                                String[] v = {aud,opt,cus,prod};
                                boolean OK = updateSync(context, v,1);
                                if (OK) {
                                    System.out.println("updated "+aud);
                                }
                            }
                        }

                    }
                }
            } catch (NumberFormatException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            } catch (JSONException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
    }

    public static boolean insertResponse(Context context, String[] vals)
    {
        ContentResolver cr = context.getContentResolver();
        ContentValues values = new ContentValues() ;
        values.put(RESPONSE, vals[1]);
        values.put(UPDATED, Constants.getDate());
        String where = ID + " = "+ vals[0] ;
        boolean boo = false;
        boo =  cr.update(BASEURI, values, where, null) > 0;
        if(boo){
            System.out.println("response updated "+vals[1]+" id "+vals[0]);
        }
        return boo;
    }
    public static boolean insertAggrateResponse(Context context, String[] vals)
    {
        ContentResolver cr = context.getContentResolver();
        ContentValues values = new ContentValues() ;
        values.put(CUSTOMER, vals[1]);
        values.put(UPDATED, Constants.getDate());
        values.put(ACK, 2);
        String where = AUDITID +" = "+ vals[0] ; ;
        boolean boo = false;
        boo =  cr.update(BASEURI, values, where, null) > 0;
        if(boo){
            System.out.println("response updated "+vals[1]+" id "+vals[0]);
        }
        return boo;
    }

    public static boolean insertTimestamp(Context context, String[] vals)
    {
        ContentResolver cr = context.getContentResolver();
        ContentValues values = new ContentValues() ;
        values.put(TIMESTAMP, vals[2]);
        String where = AUDITID +" = "+ vals[1] + " AND "+LOCALPRODUCTNO + " = "+vals[0] ;
        boolean boo = false;
        boo =  cr.update(BASEURI, values, where, null) > 0;
        if(boo){
            System.out.println("response updated "+vals[1]+" id "+vals[0]);
        }
        return boo;
    }

    public static boolean purgeProgress(Context context, long audit)
    {
        ContentResolver cr = context.getContentResolver();
        ContentValues values = new ContentValues() ;
        values.put(TIMESTAMP, "");
        values.put(ACK, 0);
        values.put(RESPONSE, "");
        values.put(CUSTOMER, "");
        values.put(CUSTOMERLOCAL, "");
        values.put(UPDATED, "");
        String where = AUDITID +" = "+ audit ;
        boolean boo = false;
        boo =  cr.update(BASEURI, values, where, null) > 0;
        if(boo){
            System.out.println("response purged ");
        }
        return boo;
    }

    public static boolean purgeProgress(Context context, long audit,long prodl)
    {
        ContentResolver cr = context.getContentResolver();
        ContentValues values = new ContentValues() ;
        values.put(TIMESTAMP, "");
        values.put(ACK, 0);
        values.put(RESPONSE, "");
        values.put(CUSTOMER, "");
        values.put(CUSTOMERLOCAL, "");
        values.put(UPDATED, "");
        String where = AUDITID +" = "+ audit+" AND "+LOCALPRODUCTNO+" = "+prodl ;
        boolean boo = false;
        boo =  cr.update(BASEURI, values, where, null) > 0;
        if(boo){
            System.out.println("response purged ");
        }
        return boo;
    }


    public static void sendErrorJson(Context context,String[] cd) throws JSONException {
        JSONObject jobs = new JSONObject();
        jobs.put("tag", Constants.TAGACK);
        jobs.put(Constants.ACKITEM, cd[0]);
        jobs.put(Constants.ERRORCODE, cd[1]);
        jobs.put(Constants.IMEI, Constants.getIMEINO(context));
        js = new UrlCon();
        System.out.println(jobs.toString());
        js.getJson(Constants.SERVER, jobs) ;
    }


    public static boolean isTimestampExist(Context context, String[] items)
    {
        ContentResolver cr = context.getContentResolver();
        boolean ret = false;
        String where = AUDITID +" = "+ items[1] + " AND "+LOCALPRODUCTNO + " = "+items[0] ;
        Cursor c = cr.query(BASEURI, new String[]{TIMESTAMP}, where, null, null);
        try {
            if (c.moveToFirst()) {
                String tt = c.getString(c.getColumnIndex(TIMESTAMP));
                if(Validating.areSet(tt))
                {
                    ret = true;
                }
            }
        } finally {
            if (c != null) {
                c.close();
            }
        }
        return ret;
    }

    public static ArrayList<AuditResponses> getAuditResponse(Context context, long[] where){
        ArrayList<AuditResponses> list = new ArrayList<>();
        String q = AUDITID +" = "+where[0] +" AND "+CUSTOMERLOCAL+" = "+where[1] + " AND "+LOCALPRODUCTNO
                +" = "+where[2];
        ContentResolver cr = context.getContentResolver();
        Cursor c = cr.query(BASEURI,null,q,null,null);
        try{
            if(c != null && c.moveToFirst()){
                do {
                    long aud = c.getLong(c.getColumnIndex(AUDITID));
                    long id = c.getLong(c.getColumnIndex(ID));
                    String res = c.getString(c.getColumnIndex(RESPONSE));
                    String custl = c.getString(c.getColumnIndex(CUSTOMERLOCAL));
                    String cust = c.getString(c.getColumnIndex(CUSTOMER));
                    String prodl = c.getString(c.getColumnIndex(LOCALPRODUCTNO));
                    String prod = c.getString(c.getColumnIndex(PRODUCTNO));
                    String optid = c.getString(c.getColumnIndex(AUDITOPTIONID));
                    String ack = c.getString(c.getColumnIndex(ACK));
                    list.add(new AuditResponses(prod,prodl,optid,cust,custl,res,ack,aud,id));
                } while (c.moveToNext());
            }
        } finally {
            if(c != null){
                c.close();
            }
        }
        return list;
    }

    public static boolean updateSync(Context context, String[] v, int flag)
    {
        ContentResolver cr = context.getContentResolver();
        ContentValues values = new ContentValues() ;
        values.put(ACK, flag);
        String where = AUDITID + " = "+ v[0]+" AND "+AUDITOPTIONID +" = "+v[1] +" AND "+CUSTOMER+" = "
                +v[2] + " AND " +PRODUCTNO +" = "+v[3];
        return cr.update(BASEURI, values, where, null) > 0;
    }

    public static boolean updateprod(Context context, String[] prod)
    {
        ContentResolver cr = context.getContentResolver();
        ContentValues values = new ContentValues() ;
        values.put(PRODUCTNO, prod[0]);
        String where = LOCALPRODUCTNO + " = "+ prod[1] ;
        return cr.update(BASEURI, values, where, null) > 0;
    }

    public AuditResponses(String prod, String prodl, String optid, String cust, String custl, String resp, String ack, long auid) {
        this.prod = prod;
        this.prodl = prodl;
        this.optid = optid;
        this.cust = cust;
        this.custl = custl;
        this.resp = resp;
        this.ack = ack;
        this.auid = auid;
    }

    public AuditResponses(String prod, String prodl, String optid, String cust, String custl, String resp, String ack, long auid,long id) {
        this.prod = prod;
        this.prodl = prodl;
        this.optid = optid;
        this.cust = cust;
        this.custl = custl;
        this.resp = resp;
        this.ack = ack;
        this.auid = auid;
        this.id = id;
    }

    public long getId() {
        return id;
    }

    public long getAuid() {
        return auid;
    }

    public String getProd() {
        return prod;
    }

    public String getProdl() {
        return prodl;
    }

    public String getOptid() {
        return optid;
    }

    public String getCust() {
        return cust;
    }

    public String getCustl() {
        return custl;
    }

    public String getResp() {
        return resp;
    }

    public String getAck() {
        return ack;
    }
}
