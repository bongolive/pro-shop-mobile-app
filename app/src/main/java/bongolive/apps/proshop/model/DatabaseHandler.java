/*
 * The source codes in this project are owned by Bongolive Enterprises. Copying or any kind of usage without Bongolive's approval is against the law.
 * Copyright (c) 2015.
 *
 */

package bongolive.apps.proshop.model;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import bongolive.apps.proshop.controller.Constants;

@SuppressWarnings("unused")
public class DatabaseHandler extends SQLiteOpenHelper
{
	public static final String DBNAME = "proshop" ;//proshop
	private static final int DBVERSION =  8 ;
	private static DatabaseHandler mInstance = null;
	Context _context ;
	
	public static DatabaseHandler getInstance(Context ctx) { 
	    if (mInstance == null) {
	      mInstance = new DatabaseHandler(ctx.getApplicationContext());
	    }
	    return mInstance;
	  }
	
	private DatabaseHandler(Context context) {  
		super(context, DBNAME, null, DBVERSION);
	}

	@Override
	public void onCreate(SQLiteDatabase db) {

	 String CUSTOMERS = "CREATE TABLE IF NOT EXISTS "+Customers.TABLENAME + " ("+
			 Customers.ID + " INTEGER PRIMARY KEY AUTOINCREMENT ,"+ 
			 Customers.CUSTOMERNAME + " VARCHAR(50) ," +
			 Customers.BUZINESSNAME + " VARCHAR(50) ," +
			 Customers.REFERENCE + " INTEGER ," + 
			 Customers.AREA + " VARCHAR(50) ," +
			 Customers.CITY + " VARCHAR(50) ,"+
			 Customers.MOBILE +" VARCHAR(20) ,"+
			 Customers.EMAIL + " VARCHAR(50) ,"+
			 Customers.VRN + " VARCHAR(50) ,"+
			 Customers.TIN + " VARCHAR(50) ,"+
			 Customers.LAT + " DOUBLE ,"+
			 Customers.LONGTUDE + " DOUBLE ,"+
			 Customers.SYNCSTATUS + " INTEGER DEFAULT 0,"+
			 Customers.STREET + " VARCHAR(50) )"; 

     String PURCHASEITEMS = "CREATE TABLE IF NOT EXISTS  "+ PurchaseItems.TABLENAME + " ("+
			 PurchaseItems.ID + " INTEGER PRIMARY KEY AUTOINCREMENT ,"+
    		 PurchaseItems.PRODUCT + " INTEGER ,"+
			 PurchaseItems.BATCH + " VARCHAR(100) ,"+
    		 PurchaseItems.QUANTITY + " DOUBLE ,"+
			 PurchaseItems.EXPIRY + " VARCHAR(15) ,"+
			 PurchaseItems.WAREHOUSE + " INTEGER ,"+
    		 PurchaseItems.UNITPRICE + " DOUBLE ," +
    		 PurchaseItems.UPDATE + " DATETIME DEFAULT CURRENT_TIMESTAMP ,"+
			 PurchaseItems.TIMESTAMP + " TEXT NOT NULL ," +
			 PurchaseItems.ACK + " INTEGER DEFAULT 0, " +
			 PurchaseItems.SYS_PRODID + " INTEGER DEFAULT 0, " +
             PurchaseItems.SYS_PURCHASID +" INTEGER DEFAULT 0 )";

     String TRITM = "CREATE TABLE IF NOT EXISTS  "+ TransferItems.TABLENAME + " ("+
			 TransferItems.ID + " INTEGER PRIMARY KEY AUTOINCREMENT ,"+
    		 TransferItems.STATUS +  " INTEGER DEFAULT 0, " +
			 TransferItems.TRANSIDL + " INTEGER DEFAULT 0, " +
			 TransferItems.SYS_TRANS_ITEMID + " INTEGER DEFAULT 0, " +
    		 TransferItems.QUANTITY + " TEXT ,"+
    		 TransferItems.QTYVERIFIED + " TEXT ,"+
			 TransferItems.EXPIRY + " TEXT ,"+
    		 TransferItems.UNITPRICE + " TEXT ," +
    		 TransferItems.UPDATE + " DATETIME DEFAULT CURRENT_TIMESTAMP ,"+
			 TransferItems.TIMESTAMP + " TEXT NOT NULL ," +
			 TransferItems.ACK + " INTEGER DEFAULT 0, " +
			 TransferItems.PRODUCT + " INTEGER DEFAULT 0, " +
             TransferItems.TRANSID +" INTEGER DEFAULT 0 )";

     String TRF = "CREATE TABLE IF NOT EXISTS  "+ Transfer.TABLENAME + " ("+
			 Transfer.ID + " INTEGER PRIMARY KEY AUTOINCREMENT ,"+
			 Transfer.BATCH + " TEXT ,"+
    		 Transfer.NOTE + " TEXT ,"+
    		 Transfer.TOTAL + " TEXT ,"+
			 Transfer.GRANDTOTAL + " TEXT ,"+
			 Transfer.TOTALTAX + " TEXT ,"+
    		 Transfer.WAREHOUSE +  " INTEGER ," +
    		 Transfer.WAREHOUSEFROM +  " TEXT ," +
    		 Transfer.MEDIA +  " TEXT ," +
    		 Transfer.CREATED + " DATETIME DEFAULT CURRENT_TIMESTAMP ,"+
    		 Transfer.UPDATE + " DATETIME ,"+
			 Transfer.SYSTRANSID + " INTEGER DEFAULT 0, " +
			 Transfer.RECEIVED + " INTEGER DEFAULT 2, " +
			 Transfer.ACK + " INTEGER DEFAULT 0, " +
			 Transfer.TRANSTYPE + " VARCHAR(40) NOT NULL DEFAULT "+Constants.TRANSTYPE_TRANSFER+" )";

     String _PRODUCT = "CREATE TABLE IF NOT EXISTS  "+Products.TABLENAME + " ("+
			 Products.ID + " INTEGER PRIMARY KEY AUTOINCREMENT ,"+
    		 Products.PRODUCTNAME + " VARCHAR(50) NOT NULL ,"+
    		 Products.PRODUCTCODE + " VARCHAR(50) NOT NULL UNIQUE ,"+
    		 Products.BARCODESYMBOL + " VARCHAR(50) NOT NULL DEFAULT code128 ,"+
    		 Products.IMAGE + " VARCHAR(50)  ,"+
    		 Products.PACKAGETYPE + " VARCHAR(50) NULL ,"+
			 Products.PACKAGEUNITS + " INTEGER NULL, "+
			 Products.CONTINER + " VARCHAR(30) NULL ,"+
			 Products.TAXMETHOD + " INTEGER(11) NOT NULL DEFAULT 0,"+
			 Products.ITEMS_ON_HAND + " INTEGER(11) NOT NULL DEFAULT 0,"+
			 Products.REODER + " DOUBLE NOT NULL DEFAULT 0,"+
			 Products.PRODUCT_TYPE + " VARCHAR,"+
    		 Products.UNITPRICE + " DOUBLE NOT NULL ," +
			 Products.TAXRATE + " DOUBLE NOT NULL DEFAULT 18 , " +
    		 Products.UPDATE + " DATETIME NULL ,"+
    		 Products.CREATED + " DATETIME NOT NULL ,"+
			 Products.REFERENCE + " INTEGER(11) NOT NULL DEFAULT 0  ,"+
             Products.CATEGORY +" VARCHAR(50) , "+
			 Products.SYNCSTATUS + " INTEGER DEFAULT 0,"+
             Products.DESCRIPTION +" VARCHAR(50) , "+
             Products.SKU +" VARCHAR(50) UNIQUE NOT NULL DEFAULT CURRENT_TIMESTAMP , "+
             Products.UNITMEASURE +" VARCHAR(50) , "+
             Products.UNITSIZE + " DOUBLE, "+
			 Products.PRODSTATUS + " INTEGER NOT NULL DEFAULT 1 )";
     
     String ORDERS = "CREATE TABLE IF NOT EXISTS  "+ Sales.TABLENAME+" ("+
		     Sales.ID + " INTEGER PRIMARY KEY AUTOINCREMENT ," +
    		 Sales.LOCALCUSTOMER + " INTEGER ," +
		     Sales.ORDERDATE + " DATETIME DEFAULT CURRENT_TIMESTAMP ," +
    		 Sales.PAYMENTSTATUS + " VARCHAR(20) ,"+
		     Sales.COMMENT + " VARCHAR(100) ," +
    		 Sales.PAYMENTAMOUNT + " DOUBLE ," +
		     Sales.TOTALSALES + " DOUBLE ," +
		     Sales.GRANDTOTAL + " DOUBLE ," +
    		 Sales.TOTALTAX + " DOUBLE ," +
		     Sales.TOTALDISCOUNT + " DOUBLE ," +
			 Sales.CUSTOMERID + " INTEGER DEFAULT 0," +//0 means don't calculate tax
			 Sales.TAXSETTINGS + " INTEGER DEFAULT 0," +//0 means don't calculate tax
			 Sales.SALESTATUS + " VARCHAR(20) NOT NULL," +//0 means don't calculate tax
             Sales.LAT + " DOUBLE ,"+
             Sales.LONGTUDE + " DOUBLE ,"+
             Sales.ISPRINTED + " INTEGER DEFAULT 0 ," +
             Sales.ITEMS + " DOUBLE ," +
             Sales.SYS_SALE + " INTEGER ," +
             Sales.SALE_REFERENCE + " VARCHAR(100) ," +
             Sales.SALE_TYPE + " VARCHAR(10) NOT NULL DEFAULT '"+Constants.SALETYPE_SALE+"'," +
    		 Sales.ACK + " INTEGER DEFAULT 0 )";


     String STKISSUE = "CREATE TABLE IF NOT EXISTS  "+ StockIssue.TABLENAME+" ("+
		     StockIssue.ID + " INTEGER PRIMARY KEY AUTOINCREMENT ," +
    		 StockIssue.LOCALCUSTOMER + " INTEGER ," +
		     StockIssue.ORDERDATE + " DATETIME DEFAULT CURRENT_TIMESTAMP ," +
    		 StockIssue.PAYMENTSTATUS + " VARCHAR(20) ,"+
		     StockIssue.COMMENT + " VARCHAR(100) ," +
    		 StockIssue.PAYMENTAMOUNT + " DOUBLE ," +
		     StockIssue.TOTALSALES + " DOUBLE ," +
		     StockIssue.GRANDTOTAL + " DOUBLE ," +
    		 StockIssue.TOTALTAX + " DOUBLE ," +
		     StockIssue.TOTALDISCOUNT + " DOUBLE ," +
			 StockIssue.CUSTOMERID + " INTEGER DEFAULT 0," +//0 means don't calculate tax
			 StockIssue.TAXSETTINGS + " INTEGER DEFAULT 0," +//0 means don't calculate tax
			 StockIssue.SALESTATUS + " VARCHAR(20) NOT NULL," +//0 means don't calculate tax
             StockIssue.LAT + " DOUBLE ,"+
             StockIssue.LONGTUDE + " DOUBLE ,"+
             StockIssue.ISPRINTED + " INTEGER DEFAULT 0 ," +
             StockIssue.ITEMS + " DOUBLE ," +
             StockIssue.SYS_SALE + " INTEGER ," +
             StockIssue.SALE_REFERENCE + " VARCHAR(100) ," +
             StockIssue.SALE_TYPE + " VARCHAR(10) NOT NULL DEFAULT '"+Constants.SALETYPE_SALE+"'," +
    		 StockIssue.ACK + " INTEGER DEFAULT 0 )";


     String PMT = "CREATE TABLE IF NOT EXISTS  "+ Payments.TABLENAME+" ("+
		     Payments.ID + " INTEGER PRIMARY KEY AUTOINCREMENT ," +
		     Payments.CREATED + " DATETIME DEFAULT CURRENT_TIMESTAMP ," +
		     Payments.UPDATEDDATE + " DATETIME ," +
    		 Payments.PAYDATE + " DATETIME ,"+
		     Payments.COMMENT + " TEXT ," +
    		 Payments.AMOUNT + " TEXT ," +
    		 Payments.POSTBALANCE + " TEXT ," +
		     Payments.PAYID + " INTEGER ," +
    		 Payments.POSTPAID + " TEXT ," +
			 Payments.NODETYPE + " INTEGER NOT NULL DEFAULT 0," +
		     Payments.PAIDBY + " TEXT ," +
			 Payments.ATTACHEMENT + " VARCHAR(80) NOT NULL," +//0 means don't calculate tax
             Payments.LOCALSALE + " INTEGER ," +
             Payments.SYS_SALE + " INTEGER ," +
             Payments.REFRENCENO + " VARCHAR(100) ," +
             Payments.RECEIVEDBY + " VARCHAR(100) ," +
             Payments.TYPE + " VARCHAR(10) NOT NULL DEFAULT '"+Constants.PAYTYPE_RECEIVED+"'," +
    		 Payments.ACK + " INTEGER DEFAULT 0 )";

     String ORDERITEM = "CREATE TABLE IF NOT EXISTS  "+ SalesItems.TABLENAME  + " ("+
			SalesItems.ID + " INTEGER PRIMARY KEY AUTOINCREMENT ,"+
			SalesItems.LOCALPRODUCTNO + " INTEGER ," +
			SalesItems.PRODUCTNO + " INTEGER NOT NULL DEFAULT 0," +
			SalesItems.PRODUCTQTY + " DOUBLE ," +
			SalesItems.PRODUCTQTYDEL + " DOUBLE ," +
			SalesItems.CUSTOMERID + " INTEGER ," +
			SalesItems.SALEITEMID + " INTEGER NOT NULL DEFAULT 0 ," +
			SalesItems.SALEPRICE + " DOUBLE ," +
			SalesItems.NETSALEPRICE + " DOUBLE ," +
			SalesItems.DISCOUNTPRICE + " DOUBLE DEFAULT 0," +
			SalesItems.TAX_AMOUNT + " DOUBLE ," +
			SalesItems.ITEM_TAX + " DOUBLE ," +
			SalesItems.SUBTOTAL + " DOUBLE ," +
			SalesItems.SALE_REFERENCE + " VARCHAR(100) ," +
			SalesItems.TAXRATE + " DOUBLE ," +
			SalesItems.ORDERID + " INTEGER ," +
			SalesItems.DELSTATUS + " VARCHAR(20) NOT NULL," +
			SalesItems.WH + " INTEGER ," +
            SalesItems.SALE_TYPE + " VARCHAR(100) NOT NULL DEFAULT '"+Constants.SALETYPE_SALE+"'," +
			SalesItems.TIMESTAMP + " TEXT NOT NULL ," +
			SalesItems.ACK + " INTEGER DEFAULT 0 )";

     String STKISSUEIT = "CREATE TABLE IF NOT EXISTS  "+ StockIssueItems.TABLENAME  + " ("+
			StockIssueItems.ID + " INTEGER PRIMARY KEY AUTOINCREMENT ,"+
			StockIssueItems.LOCALPRODUCTNO + " INTEGER ," +
			StockIssueItems.PRODUCTNO + " INTEGER NOT NULL DEFAULT 0," +
			StockIssueItems.PRODUCTQTY + " DOUBLE ," +
			StockIssueItems.PRODUCTQTYDEL + " DOUBLE ," +
			StockIssueItems.CUSTOMERID + " INTEGER ," +
			StockIssueItems.SALEITEMID + " INTEGER NOT NULL DEFAULT 0 ," +
			StockIssueItems.SALEPRICE + " DOUBLE ," +
			StockIssueItems.NETSALEPRICE + " DOUBLE ," +
			StockIssueItems.DISCOUNTPRICE + " DOUBLE DEFAULT 0," +
			StockIssueItems.TAX_AMOUNT + " DOUBLE ," +
			StockIssueItems.ITEM_TAX + " DOUBLE ," +
			StockIssueItems.SUBTOTAL + " DOUBLE ," +
			StockIssueItems.SALE_REFERENCE + " VARCHAR(100) ," +
			StockIssueItems.TAXRATE + " DOUBLE ," +
			StockIssueItems.ORDERID + " INTEGER ," +
			StockIssueItems.DELSTATUS + " VARCHAR(20) NOT NULL," +
			StockIssueItems.WH + " INTEGER ," +
            StockIssueItems.SALE_TYPE + " VARCHAR(100) NOT NULL DEFAULT '"+Constants.SALETYPE_SALE+"'," +
			StockIssueItems.TIMESTAMP + " TEXT NOT NULL ," +
			StockIssueItems.ACK + " INTEGER DEFAULT 0 )";

     String PENDINGS = "CREATE TABLE IF NOT EXISTS  "+ Pending_Items.TABLENAME  + " ("+
			Pending_Items.ID + " INTEGER PRIMARY KEY AUTOINCREMENT ,"+
			Pending_Items.LOCALPRODUCTNO + " INTEGER ," +
			Pending_Items.PRODUCTNO + " INTEGER NOT NULL DEFAULT 0," +
			Pending_Items.PRODUCTQTY + " DOUBLE ," +
			Pending_Items.PRODUCTQTYDEL + " DOUBLE ," +
			Pending_Items.CUSTOMERID + " INTEGER ," +
			Pending_Items.SALEITEMID + " INTEGER NOT NULL DEFAULT 0 ," +
			Pending_Items.SALEPRICE + " DOUBLE ," +
			Pending_Items.NETSALEPRICE + " DOUBLE ," +
			Pending_Items.DISCOUNTPRICE + " DOUBLE DEFAULT 0," +
			Pending_Items.TAX_AMOUNT + " DOUBLE ," +
			Pending_Items.ITEM_TAX + " DOUBLE ," +
			Pending_Items.SUBTOTAL + " DOUBLE ," +
			Pending_Items.SALE_REFERENCE + " VARCHAR(100) ," +
			Pending_Items.TAXRATE + " DOUBLE ," +
			Pending_Items.ORDERID + " INTEGER ," +
			Pending_Items.DELSTATUS + " VARCHAR(20) NOT NULL," +
			Pending_Items.WH + " INTEGER ," +
            Pending_Items.SALE_TYPE + " VARCHAR(100) NOT NULL DEFAULT '"+Constants.SALETYPE_SALE+"'," +
			Pending_Items.TIMESTAMP + " TEXT NOT NULL ," +
			Pending_Items.ACK + " INTEGER DEFAULT 0 )";

     String PAYMENTMODE = "CREATE TABLE IF NOT EXISTS  "+PaymentMode.TABLENAME + " (" +
			PaymentMode.ID + " INTEGER PRIMARY KEY AUTOINCREMENT ,"+
			PaymentMode.MODETITLE + " VARCHAR ," +
			PaymentMode.SYSTEMMODE + " INTEGER ," +
			PaymentMode.SYMBOL + " VARCHAR )";

     String STPUMP = "CREATE TABLE IF NOT EXISTS  "+StationPump.TABLENAME + " (" +
			 StationPump.ID + " INTEGER PRIMARY KEY AUTOINCREMENT ,"+
			 StationPump.SYS_ID + " INTEGER DEFAULT 0 ," +
			 StationPump.NAME + " VARCHAR(100) ," +
			 StationPump.ACK + " INTEGER DEFAULT 0 ," +
			 StationPump.STATION + " INTEGER DEFAULT 0 )";

     String TRACKING = "CREATE TABLE IF NOT EXISTS  "+Tracking.TABLENAME + " (" +
             Tracking.ID + " INTEGER PRIMARY KEY AUTOINCREMENT ,"+
             Tracking.LAT + " DOUBLE ," +
             Tracking.LNG + " DOUBLE ," +
             Tracking.ISSYNCED + " INTEGER NOT NULL DEFAULT 0 , "+
             Tracking.DATE + " VARCHAR )";

     String USERS = "CREATE TABLE IF NOT EXISTS  "+ User.TABLENAME + " (" +
			 User.ID + " INTEGER PRIMARY KEY AUTOINCREMENT ,"+
			 User.STATUS + " INTEGER NOT NULL DEFAULT 1 , "+
			 User.CREATED + " DATETIME NOT NULL , "+
			 User.USERNAME + " VARCHAR(50) , "+
			 User.UPDATED + " DATETIME , "+
			 User.ROLE + " INTEGER NOT NULL DEFAULT 0 , "+
			 User.PIN + " INTEGER NOT NULL DEFAULT 0 , "+
			 User.PASSWORD + " VARCHAR(70) )";

     String SETTINGS = "CREATE TABLE IF NOT EXISTS  " + Settings.TABLENAME + " (" +
    		  Settings.ID + " INTEGER PRIMARY KEY AUTOINCREMENT ," +
    		  Settings.SYSID + " INTEGER DEFAULT 0 ," +// zero false 1 true
    		  Settings.VAT + " INTEGER DEFAULT 1 ," +
    		  Settings.DISCOUNT + " INTEGER DEFAULT 1 ," +
    		  Settings.TRACKTIME + " INTEGER DEFAULT 15 ," +
    		  Settings.PRINT + " INTEGER DEFAULT 1 ," +
    		  Settings.EXPORTDB + " INTEGER DEFAULT 0 ," +
    		  Settings.IMPORTDB + " INTEGER DEFAULT 0 ," +
    		  Settings.PRODUCT_EDIT + " INTEGER DEFAULT 0 ," +
    		  Settings.CUSTOMER_EDIT + " INTEGER DEFAULT 0 ," +
    		  Settings.STOCK_RECEIVE + " INTEGER DEFAULT 0 ," +
    		  Settings.PRODUCT_SHARING + " INTEGER DEFAULT 1 ," +
    		  Settings.PRODUCT_ADD + " INTEGER DEFAULT 1 ," +
    		  Settings.CUSTOMER_ADD + " INTEGER DEFAULT 1 ," +
    		  Settings.PAYLATER + " INTEGER DEFAULT 0 ," +
    		  Settings.SALES + " INTEGER DEFAULT 0 ," +
    		  Settings.PRODUCTS + " INTEGER DEFAULT 1 ," +
    		  Settings.REPORTS + " INTEGER DEFAULT 1 ," +
    		  Settings.CUSTOMERS + " INTEGER DEFAULT 1 ," +
    		  Settings.ORDERS + " INTEGER DEFAULT 0 ," +
    		  Settings.PROSHOPLITE + " INTEGER DEFAULT 0 ," +
    		  Settings.DELIVERY + " INTEGER DEFAULT 0 ," +
    		  Settings.STATION_FORM + " INTEGER DEFAULT 0 ," +
    		  Settings.EXPENSES + " INTEGER DEFAULT 0 ," +
    		  Settings.AUDIT + " INTEGER DEFAULT 0 ," +
    		  Settings.PAYMENT + " INTEGER DEFAULT 1 ," +
    		  Settings.STOCK + " INTEGER DEFAULT 0 ," +
    		  Settings.SIGNATURE + " INTEGER DEFAULT 1 ," +
    		  Settings.USERS + " INTEGER DEFAULT 0 ," +
    		  Settings.DISCOUNT_LIMIT + " DOUBLE DEFAULT 0 ," +
			  Settings.CREATED + " NOT NULL DEFAULT CURRENT_TIMESTAMP, "+
			  Settings.MODIFIED + " NOT NULL DEFAULT CURRENT_TIMESTAMP, "+
    		  Settings.APPTYPE + " VARCHAR(50) DEFAULT '"+Constants.APPTYPE_SHOP+"' )";

     String MULTM = "CREATE TABLE IF NOT EXISTS  " + MultimediaContents.TABLENAME + " (" +
    		  MultimediaContents.ID + " INTEGER PRIMARY KEY AUTOINCREMENT ," +
    		  MultimediaContents.ISSYNCED + " INTEGER NOT NULL DEFAULT 0 ," +
    		  MultimediaContents.NAME + " VARCHAR(100) NOT NULL ," +
    		  MultimediaContents.SIZE + " VARCHAR(30) NOT NULL ," +
    		  MultimediaContents.TYPE + " VARCHAR(25) NOT NULL ," +
    		  MultimediaContents.CONTENT + " VARCHAR(150) NOT NULL," +
    		  MultimediaContents.FOREIGNKEY + " VARCHAR(100) NOT NULL," +
    		  MultimediaContents.DATE + " DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP  )";

     String WH = "CREATE TABLE IF NOT EXISTS  " + Warehouse.TABLENAME + " (" +
    		  Warehouse.ID + " INTEGER PRIMARY KEY AUTOINCREMENT ," +
    		  Warehouse.ACK + " INTEGER NOT NULL DEFAULT 0 ," +
    		  Warehouse.WH_CODE + " VARCHAR(20) NOT NULL ," +
    		  Warehouse.WH_NAME + " VARCHAR(30) NOT NULL ," +
    		  Warehouse.WH_ID + " INT NOT NULL DEFAULT 0 ," +
    		  Warehouse.WH_ADDRESS + " VARCHAR(150) NOT NULL," +
              Warehouse.WH_STATUS +" TEXT NOT NULL DEFAULT '"+Constants.WH_ACTIVE+"', " +
    		  Warehouse.CREATED_ON + " DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP  )";


     String CAT = "CREATE TABLE IF NOT EXISTS  " + Product_Categories.TABLENAME + " (" +
    		  Product_Categories.ID + " INTEGER PRIMARY KEY AUTOINCREMENT ," +
    		  Product_Categories.ACK + " INTEGER NOT NULL DEFAULT 0 ," +
    		  Product_Categories.CAT_CODE + " VARCHAR(20) NOT NULL ," +
    		  Product_Categories.CAT_NAME + " VARCHAR(30) NOT NULL ," +
    		  Product_Categories.CAT_ID + " INT NOT NULL DEFAULT 0 ," +
    		  Product_Categories.CREATED_ON + " DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP  )";

     String DEL = "CREATE TABLE IF NOT EXISTS  " + Delivery.TABLENAME + " (" +
             Delivery.ID + " INTEGER PRIMARY KEY AUTOINCREMENT ,"+
             Delivery.LOCALCUSTOMER + " INTEGER ," +
             Delivery.LOCALSALE + " INTEGER ," +
             Delivery.SYS_SALE + " INTEGER ," +
             Delivery.CUSTOMERID + " INTEGER ," +
             Delivery.DELID + " INTEGER ," +
             Delivery.DELIVERYSTATUS + " TEXT ," +
             Delivery.DELIVEREDDATE + " TEXT ," +
             Delivery.DELIVERYDATE + " TEXT ," +
             Delivery.DELIVERYMETHOD + " TEXT ," +
             Delivery.REFRENCENO + " TEXT ," +
             Delivery.ADDRESS + " TEXT ," +
             Delivery.LONGTUDE + " DOUBLE ," +
             Delivery.SALE_REFERENCE + " TEXT ," +
             Delivery.LAT + " DOUBLE ," +
             Delivery.CREATED + " DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP  ," +
             Delivery.VEHICLENO + " TEXT ," +
             Delivery.SIGNATURE + " TEXT ," +
             Delivery.UPDATEDDATE + " TEXT  ," +
             Delivery.COMMENT + " TEXT  ," +
             Delivery.ACK + " INTEGER DEFAULT 0 )";


     String ST = "CREATE TABLE IF NOT EXISTS  " + Station.TABLENAME + " (" +
             Station.ID + " INTEGER PRIMARY KEY AUTOINCREMENT ,"+
             Station.SYS_ID + " INTEGER DEFAULT 0," +
             Station.METERIN + " DOUBLE DEFAULT 0.0," +
             Station.METEROUT + " DOUBLE DEFAULT 0.0," +
             Station.TIMEIN + " VARCHAR(20) NULL DEFAULT '0000-00-00 00:00:00'," +
             Station.TIMEOUT + " VARCHAR(20) NULL DEFAULT '0000-00-00 00:00:00'," +
             Station.UPDATEDON +" VARCHAR(20) NULL DEFAULT '0000-00-00 00:00:00'," +
             Station.CREATEDON + " DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP  ," +
             Station.CASHIN + " DOUBLE DEFAULT 0.0," +
             Station.CASHOUT + " DOUBLE DEFAULT 0.0," +
             Station.CREATEDBY + " INTEGER DEFAULT 0," +
             Station.SOURCE + " VARCHAR(20) DEFAULT '"+Constants.SOURCE_APP+"', " +
             Station.STATIONNAME + " VARCHAR(100) ," +
             Station.OPERATOR + " VARCHAR(100) ," +
             Station.SIGNATURE + " VARCHAR(100) ," +
             Station.TIMESTAMP + " TEXT  ," +
             Station.COMMENT + " TEXT  ," +
             Station.ACK + " INTEGER DEFAULT 0 )";


     String EX = "CREATE TABLE IF NOT EXISTS  " + Expenses.TABLENAME + " (" +
             Expenses.ID + " INTEGER PRIMARY KEY AUTOINCREMENT ,"+
             Expenses.SYS_ID + " INTEGER DEFAULT 0," +
             Expenses.REFERENCE + " VARCHAR(20) NOT NULL," +
             Expenses.AMOUNT + " DOUBLE DEFAULT 0.0," +
             Expenses.EXPENSETYPE + " VARCHAR(100) , " +
             Expenses.PAYMETHOD + " VARCHAR(100) , " +
             Expenses.UPDATEDON +" VARCHAR(20) NULL DEFAULT '0000-00-00 00:00:00'," +
             Expenses.CREATEDON + " DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP  ," +
             Expenses.CREATEDBY + " INTEGER DEFAULT 0," +
             Expenses.TITLE + " VARCHAR(100) ," +
             Expenses.OPERATOR + " VARCHAR(100) ," +
             Expenses.SUPPLIER + " VARCHAR(100) ," +
             Expenses.ATTACHMENT + " VARCHAR(100) ," +
             Expenses.TIMESTAMP + " TEXT  ," +
             Expenses.COMMENT + " TEXT  ," +
             Expenses.ACK + " INTEGER DEFAULT 0 )";

     String AU = "CREATE TABLE IF NOT EXISTS  " + Audit.TABLENAME + " (" +
             Audit.ID + " INTEGER PRIMARY KEY AUTOINCREMENT ,"+
             Audit.AUDITID + " INTEGER ," +
			 Audit.CREATED + " DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP  ," +
             Audit.UPDATED +" VARCHAR(20) NULL DEFAULT '0000-00-00 00:00:00'," +
             Audit.GPS + " INTEGER DEFAULT 0," +
             Audit.AUDITOPTIONID + " INTEGER ," +
             Audit.INPUTTYPE + " INTEGER DEFAULT 1," +
             Audit.OPTIONTYPE + " INTEGER DEFAULT 1," +
             Audit.CAPTION + " VARCHAR(100)  ," +
             Audit.TIMESTAMP + " TEXT  ," +
             Audit.CUSTOMERS + " TEXT ," +
             Audit.PRODUCTS + " TEXT ," +
             Audit.DESCRIPTION + " TEXT ," +
             Audit.NAME + " VARCHAR(100)  ," +
             Audit.MIN + " VARCHAR(100)  ," +
             Audit.MAX + " VARCHAR(100)  ," +
			 Audit.COMPULS + " INTEGER DEFAULT 0 ," +
             Audit.ACK + " INTEGER DEFAULT 0 )";

     String AUDRES = "CREATE TABLE IF NOT EXISTS  " + AuditResponses.TABLENAME + " (" +
             AuditResponses.ID + " INTEGER PRIMARY KEY AUTOINCREMENT ,"+
             AuditResponses.AUDITID + " INTEGER ," +
             AuditResponses.PRODUCTNO + " INTEGER DEFAULT 0," +
			 AuditResponses.CREATED + " DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP  ," +
             AuditResponses.UPDATED +" VARCHAR(20) NULL DEFAULT '0000-00-00 00:00:00'," +
             AuditResponses.LOCALPRODUCTNO + " INTEGER DEFAULT 0," +
             AuditResponses.CUSTOMER + " INTEGER DEFAULT 0," +
             AuditResponses.CUSTOMERLOCAL + " INTEGER DEFAULT 0," +
             AuditResponses.AUDITOPTIONID + " INTEGER ," +
             AuditResponses.TIMESTAMP + " TEXT  ," +
             AuditResponses.RESPONSE + " VARCHAR(100)  ," +
             AuditResponses.ACK + " INTEGER DEFAULT 0 )";

         db.execSQL(CUSTOMERS);
         db.execSQL(ORDERS);
         db.execSQL(ORDERITEM);
         db.execSQL(PURCHASEITEMS);
         db.execSQL(PAYMENTMODE);
         db.execSQL(SETTINGS);
         db.execSQL(TRACKING);
         db.execSQL(MULTM);
         db.execSQL(_PRODUCT);
         db.execSQL(USERS);
         db.execSQL(WH);
         db.execSQL(CAT);
         db.execSQL(DEL);
         db.execSQL(PENDINGS);
         db.execSQL(ST);
         db.execSQL(AU);
         db.execSQL(EX);
         db.execSQL(PMT);
         db.execSQL(AUDRES);
         db.execSQL(STPUMP);
         db.execSQL(TRF);
         db.execSQL(TRITM);
         db.execSQL(STKISSUE);
         db.execSQL(STKISSUEIT);
         System.out.println("database created ");
	}

	@Override
	public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
		Log.w("LOG_TAG", "upgrading database from version "
				+ oldVersion + " to version " + newVersion + " all data will be dropped") ;

        String SALES = " ALTER TABLE " + Sales.TABLENAME +" ADD COLUMN "+ Sales.SALESTATUS +  " VARCHAR(20) NOT " +
					"NULL DEFAULT "+ Constants.SALE_STATUS_CMPLT;

		String SALEITEMS = " ALTER TABLE " + SalesItems.TABLENAME +" ADD COLUMN "+ SalesItems.DELSTATUS +  " VARCHAR" +
				"(20) NOT NULL DEFAULT "+ Constants.SALE_STATUS_CMPLT;

		String SALEITEMS2 = " ALTER TABLE " + SalesItems.TABLENAME +" ADD COLUMN "+ SalesItems.PRODUCTQTYDEL +  " " +
				"DOUBLE NOT NULL DEFAULT 0";

        String SALEITEMS3 = " ALTER TABLE " + SalesItems.TABLENAME +" ADD COLUMN "+ SalesItems.SALEITEMID +  " " +
				"INT NOT NULL DEFAULT 0";

        String CUST = " ALTER TABLE " + Customers.TABLENAME +" ADD COLUMN "+ Customers.VRN + " VARCHAR(50) ";

        String CUST1 = " ALTER TABLE " + Customers.TABLENAME +" ADD COLUMN "+ Customers.TIN + " VARCHAR(50) " ;

        String ROEGINKEY = " ALTER TABLE " + MultimediaContents.TABLENAME +" ADD COLUMN "+ MultimediaContents
				.FOREIGNKEY +" VARCHAR(100) " ;

		String WH = " ALTER TABLE " + Warehouse.TABLENAME +" ADD COLUMN "+ Warehouse
				.WH_STATUS +" TEXT NOT NULL DEFAULT '"+Constants.WH_ACTIVE+"' " ;

		String SETTING = " ALTER TABLE " + Settings.TABLENAME +" ADD COLUMN "+ Settings.DELIVERY +  " " +
				"INT NOT NULL DEFAULT 0";

		String ORDERS_SETTINGS = " ALTER TABLE " + Settings.TABLENAME +" ADD COLUMN "+ Settings.ORDERS  +
				" INT NOT NULL DEFAULT 0";

		String LITE_SETTINGS = " ALTER TABLE " + Settings.TABLENAME +" ADD COLUMN "+ Settings.PROSHOPLITE + " INTEGER" +
				" DEFAULT 0 " ;
		String FORM = " ALTER TABLE " + Settings.TABLENAME +" ADD COLUMN "+ Settings.STATION_FORM + " INTEGER" +
				" DEFAULT 0 " ;
		String AUDIT = " ALTER TABLE " + Settings.TABLENAME +" ADD COLUMN "+ Settings.AUDIT + " INTEGER" +
				" DEFAULT 0 " ;
		String EXPENSE = " ALTER TABLE " + Settings.TABLENAME +" ADD COLUMN "+ Settings.EXPENSES + " INTEGER" +
				" DEFAULT 0 " ;
		String PAYM = " ALTER TABLE " + Settings.TABLENAME +" ADD COLUMN "+ Settings.PAYMENT + " INTEGER" +
				" DEFAULT 0 " ;
		String STOCK = " ALTER TABLE " + Settings.TABLENAME +" ADD COLUMN "+ Settings.STOCK + " INTEGER" +
				" DEFAULT 0 " ;
		String SIGN = " ALTER TABLE " + Settings.TABLENAME +" ADD COLUMN "+ Settings.SIGNATURE + " INTEGER" +
				" DEFAULT 0 " ;

		String SALETYPE = " ALTER TABLE "+Sales.TABLENAME+ " ADD COLUMN " + Sales.SALE_TYPE + " VARCHAR(10) NOT NULL " +
                "DEFAULT " + "'"+Constants.SALETYPE_SALE+"'";

        String SALEITEMTYPE = " ALTER TABLE "+SalesItems.TABLENAME+ " ADD COLUMN " + SalesItems.SALE_TYPE + " VARCHAR(10)" +
                " NOT NULL DEFAULT '"+Constants.SALETYPE_SALE+"'";

		String AUD = " ALTER TABLE "+Audit.TABLENAME+" ADD COLUMN "+Audit.GPS+" INT NOT NULL DEFAULT 0";

        if(!Sales.columnExist(db,Sales.SALESTATUS))
			db.execSQL(SALES);

        if(!SalesItems.columnExist(db,SalesItems.DELSTATUS))
			db.execSQL(SALEITEMS);

        if(!SalesItems.columnExist(db,SalesItems.PRODUCTQTYDEL))
			db.execSQL(SALEITEMS2);

        if(!SalesItems.columnExist(db,SalesItems.SALEITEMID))
			db.execSQL(SALEITEMS3);

        if(!SalesItems.columnExist(db,SalesItems.SALE_TYPE))
			db.execSQL(SALEITEMTYPE);

        if(!Sales.columnExist(db,Sales.SALE_TYPE))
			db.execSQL(SALETYPE);

        if(!Customers.columnExist(db,Customers.VRN))
			db.execSQL(CUST);

        if(!Customers.columnExist(db,Customers.TIN))
			db.execSQL(CUST1);

        if(!MultimediaContents.columnExist(db,MultimediaContents.FOREIGNKEY))
			db.execSQL(ROEGINKEY);

        if(!Warehouse.columnExist(db,Warehouse.WH_STATUS))
			db.execSQL(WH);

        if(!Settings.columnExist(db,Settings.DELIVERY))
			db.execSQL(SETTING);

        if(!Settings.columnExist(db,Settings.ORDERS))
			db.execSQL(ORDERS_SETTINGS);

        if(!Settings.columnExist(db,Settings.PROSHOPLITE))
			db.execSQL(LITE_SETTINGS);

        if(!Settings.columnExist(db,Settings.STATION_FORM))
			db.execSQL(FORM);

        if(!Settings.columnExist(db,Settings.EXPENSES))
			db.execSQL(EXPENSE);

        if(!Settings.columnExist(db,Settings.AUDIT))
			db.execSQL(AUDIT);

        if(!Settings.columnExist(db,Settings.PAYMENT))
			db.execSQL(PAYM);

        if(!Settings.columnExist(db,Settings.STOCK))
			db.execSQL(STOCK);

        if(!Settings.columnExist(db,Settings.SIGNATURE))
			db.execSQL(SIGN);

        if(!Audit.columnExist(db,Audit.GPS))
			db.execSQL(AUD);


		if(newVersion == 7) {
			String aud = "DROP TABLE IF EXISTS " + Audit.TABLENAME;
			db.execSQL(aud);
		}
            System.out.println("ALTERED ");
			onCreate(db);
	}

}
