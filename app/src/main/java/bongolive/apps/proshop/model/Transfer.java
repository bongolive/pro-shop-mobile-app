/*
 * Copyright (c) 2015.
 * Prosurvey is a property of Bongolive Enterprises Ltd. An authorized
 * copy of ideas, source codes is against the regulations
 */

package bongolive.apps.proshop.model;

import android.content.ContentResolver;
import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.DatabaseUtils;
import android.net.Uri;
import android.util.Log;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.math.BigDecimal;
import java.util.ArrayList;

import bongolive.apps.proshop.controller.Constants;
import bongolive.apps.proshop.controller.UrlCon;
import bongolive.apps.proshop.controller.Validating;

public class Transfer
{
	public static final String TABLENAME = "tranfer";
	public static final String ID = "_id";
	public static final String NOTE = "note";
	public static final String TOTALTAX = "total_tax" ;//current_stock";
	public static final String TOTAL = "total" ;//current_stock";
	public static final String TRANSTYPE = "request_type" ;//request_type";
	public static final String MEDIA = "media" ;//current_stock";
	public static final String GRANDTOTAL = "grand_total" ;//current_stock";
	public static final String WAREHOUSE = "warehouse" ;//current_stock";
	public static final String WAREHOUSEFROM = "from_warehouse" ;//current_stock";
	public static final String BATCH = "transfer_no";
	public static final String CREATED = "create_on" ;
	public static final String UPDATE = "last_update" ;
	public static final String SYSTRANSID = "sys_transfer_id" ;//"Sys_stock_id" ;
	public static final String RECEIVED = "approval" ;// 1 not approved(show btn) | 2 not approved
	public static final String ACK = "ack" ;//"//0 not syncd if 1 //send it for approval// 2 then done

	public static final Uri BASEURI = Uri.parse("content://"+ ContentProviderApi.AUTHORITY+"/"+TABLENAME) ;
	public static final String SPECIFIC_CONTENT_TYPE = "vnd.android.cursor.item/vnd.com.proshop.tranfer" ;
	public static final String GENERAL_CONTENT_TYPE = "vnd.android.cursor.dir/vnd.com.proshop.tranfer" ;
	private static final String TAG = Transfer.class.getName();
	static UrlCon js ;
	public static String ARRAYNAME = "trans_list" ;
    String gtotal,status,warehousefrom,warehouse,batch,transid,date,media;
    long id;
    int approval,ack ;

	private static JSONArray stockarray ;

	public Transfer() {
		
	}

	public static int deleteStock(Context context, String string)
	{
		ContentResolver cr = context.getContentResolver();
		ContentValues values = new ContentValues() ;
		values.put(ACK, "3") ;
		values.put(UPDATE, Constants.getDate());
		String where =  ID + " = " + string ;
		if( cr.update(BASEURI, values, where, null) > 0) {
			Log.v("updated","updated");
			return 1 ;
		}
		return 0;
	}

	private static boolean isStockPresent(Context context, String[] custid)
	    {
            String where = custid[0] +" = "+ DatabaseUtils.sqlEscapeString(custid[1]);
	    	Cursor c = context.getContentResolver().query(BASEURI, null, where, null, null) ;
            boolean ret = false;
	    	 try {
	    	if(c != null && c.moveToFirst())
	    	{
	    			ret = true;
	    	} 
		 } finally {
			 if(c != null) {
		            c.close();
		        }
		 }
	    	return ret ;
	    }

	 private static int isStockPresentLocal(Context context, String custid)
	    {
            String where = ID + " = "+custid;
	    	Cursor c = context.getContentResolver().query(BASEURI, null, where, null, null) ;
	    	 try {

	    	if(c != null && c.moveToFirst())
	    	{
	    		return 1;
	    	}
		 } finally {
			 if(c != null) {
		            c.close();
		        }
		 }
	    	return 0 ;
	    }

	public static int getQuantity(Context context, int stockid) {
		ContentResolver cr = context.getContentResolver();
		String where = ID + " = " + stockid ;
    	Cursor c = cr.query(BASEURI, new String[]{TOTAL}, where, null, null) ;
    	try {
    	int i= 0 ;
    	if(c.moveToFirst())
    	{
    		do {
    			i = c.getInt(c.getColumnIndexOrThrow(TOTAL));
    			 return i ;
    		}while(c.moveToNext());
    	} 
    	}finally {
			 if(c != null) {
		            c.close();
		        }
		 }
		return 0 ;
	}
/*
	 *  function to update in the stock table to those products that are existing
	 */
	public static int updateProductLocal(Context context, String[] string)
	{
		ContentResolver cr = context.getContentResolver();
		ContentValues values = new ContentValues() ;

		values.put(TOTAL, string[0]);
		values.put(GRANDTOTAL, string[2]) ;
		values.put(TOTALTAX, string[1]) ;
		values.put(UPDATE, Constants.getDate());

		String where = ID + " = "+ string[3] ;
		if(cr.update(BASEURI, values, where, null) > 0) {
			int itemonhand = Products.getItemOnHand(context, string[3]);
			int newitenhand = Integer.parseInt(string[0]);
			int updatingqty =  new BigDecimal(newitenhand).subtract(new BigDecimal(itemonhand))
					.intValue();
			Log.v("itemonhandupdating"," itemonhand "+itemonhand + " newitemonhand "+newitenhand+
					" updatingitemonhand "+updatingqty);
			//prodid,qty,action(0 add 1 deduct

				int[] vals = {Integer.parseInt(string[3]),updatingqty,0};
				Products.updateProductQty(context,vals);

			return 1 ;
		}
		return 0;
	}
	
	/*
	 * Save a new product in the stock table
	 */
	public static boolean insert(Context context, String[] string)
	{
		int productcount = getCount(context) ;
        boolean ret = false;
		ContentResolver cr = context.getContentResolver();
		ContentValues values = new ContentValues() ;
        values.put(NOTE, string[0]);
        values.put(TOTAL, string[1]);
        values.put(TOTALTAX, string[2]) ;
        values.put(TRANSTYPE, string[3]);
		values.put(GRANDTOTAL, string[4]);
		values.put(WAREHOUSE, string[5]);
		values.put(BATCH, string[6]);
		values.put(SYSTRANSID, string[7]);
		values.put(ACK, string[8]);
		values.put(WAREHOUSEFROM, string[9]);
		values.put(MEDIA, string[10]);
		values.put(RECEIVED, string[11]);
		cr.insert(BASEURI, values);
		if(getCount(context) == productcount+1)
			ret = true ;
        return ret;
	}


    public static String getId(Context context) {
        ContentResolver cr = context.getContentResolver();
        Cursor c = cr.query(BASEURI, new String[] {ID}, null,null,null) ;
        String  i = "0";
        try {
            if(c != null && c.moveToLast())
            {
                i = c.getString(c.getColumnIndexOrThrow(ID));
                return i ;
            }
        }finally {
            if(c != null) {
                c.close();
            }
        }
        return i ;
    }


    /*
     *  function to update in the stock table to those products that are existing
     */
    public static boolean update(Context context, String[] string)
    {
        boolean ret = false;
        ContentResolver cr = context.getContentResolver();
        ContentValues values = new ContentValues() ;
        values.put(NOTE, string[0]);
        values.put(TOTAL, string[1]);
        values.put(TOTALTAX, string[2]) ;
        values.put(TRANSTYPE, string[3]);
        values.put(GRANDTOTAL, string[4]);
        values.put(WAREHOUSE, string[5]);
        values.put(BATCH, string[6]);
        values.put(SYSTRANSID, string[7]);
        values.put(ACK, string[8]);
        values.put(WAREHOUSEFROM, string[9]);
        values.put(MEDIA, string[10]);

        String where = string[9] + " = "+ string[10] ;
        if(cr.update(BASEURI, values, where, null) > 0) {
            ret = true ;
        }
        return ret;
    }

    /*
     *  function to update in the stock table to those products that are existing
     */
    public static boolean updateSyncStatus(Context context, String[] string)
    {
        boolean ret = false;
        ContentResolver cr = context.getContentResolver();
        ContentValues values = new ContentValues() ;
        values.put(SYSTRANSID, string[2]);
        values.put(ACK, string[3]);
        values.put(RECEIVED, string[4]);

        String where = string[0] + " = "+ DatabaseUtils.sqlEscapeString(string[1]) ;
        Log.e("UPDATING", "UPDATED THE TRANSFER BATCH " + string[1]);
        if(cr.update(BASEURI, values, where, null) > 0) {
            ret = true ;
        }
        return ret;
    }

	/*
	 * How many items are in the table ?
	 */
	public static int getCount(Context context){
		ContentResolver cr = context.getContentResolver();
//		String where = ACK + " != ";
		Cursor _c = cr.query(BASEURI, null, null, null, null);
		int count = _c.getCount() ;
		_c.close() ; 
		return count;
	}

	/*
	 * Acknowledgement to the server to tick item as a go
	 */

    public static void sendAckJson(Context context) throws JSONException {
        JSONObject jobs = new JSONObject();
        jobs.put("tag", Constants.TAGACK);
        jobs.put(Constants.ACKITEM, "stock");
        jobs.put(Constants.IMEI, Constants.getIMEINO(context));
        js = new UrlCon();
        System.out.println(jobs.toString());
        js.getJson(Constants.SERVER, jobs) ;
    }

    public static ArrayList<Transfer> getItems(Context context,String action){
        ArrayList<Transfer> list = new ArrayList<>();
        ContentResolver cr = context.getContentResolver();
        String where =  TRANSTYPE+" = "+ DatabaseUtils.sqlEscapeString(action);
        Cursor c = cr.query(BASEURI,null,where,null,ID+" DESC");
        try{
            if(c != null && c.getCount() > 0){
                c.moveToFirst();
                do {
                    String sl = c.getString(c.getColumnIndex(SYSTRANSID));
                    String lsl = c.getString(c.getColumnIndex(WAREHOUSEFROM));
                    String wh = c.getString(c.getColumnIndex(WAREHOUSE));
                    String st = c.getString(c.getColumnIndex(TRANSTYPE));
                    String pd = c.getString(c.getColumnIndex(GRANDTOTAL));
                    String bl = c.getString(c.getColumnIndex(CREATED));
                    String dt = c.getString(c.getColumnIndex(BATCH));
                    String md = c.getString(c.getColumnIndex(MEDIA));
                    int ack = c.getInt(c.getColumnIndex(ACK));
                    int ap = c.getInt(c.getColumnIndex(RECEIVED));
                    long i = c.getLong(c.getColumnIndex(ID));
                    list.add(new Transfer(pd,st,lsl,wh,dt,sl,bl,md,ack,ap,i));
                } while (c.moveToNext());
            }
        } finally {
            if(c != null){
                c.close();
            }
        }
        return list;
    }

    public static JSONObject sendTransferJson(Context context) throws JSONException {
        js = new UrlCon();

        ContentResolver cr = context.getContentResolver();
        JSONArray jarr = new JSONArray();
        JSONObject jsonObject = null;

        String where = ACK + " != 1 AND "+RECEIVED + " != 1" ;

        Cursor c = cr.query(BASEURI, null, where, null, null);

        if (c != null && c.getCount() == 0) {
            c.close();
            return null;
        }
        try {

            if (c != null && c.moveToFirst()) {
                jsonObject = new JSONObject();
                jsonObject.put("tag", Constants.TAGSSENDTRANSFER);
                jsonObject.put(Constants.IMEI, Constants.getIMEINO(context));
                do {
                    JSONObject j = new JSONObject();
                    String trid = c.getString(c.getColumnIndex(ID));
                    int sytrid = c.getInt(c.getColumnIndex(SYSTRANSID));
                    j.put(ID, trid);
                    j.put(NOTE, c.getString(c.getColumnIndex(NOTE)));
                    j.put(TOTALTAX, c.getString(c.getColumnIndexOrThrow(TOTALTAX)));
                    j.put(TOTAL, c.getString(c.getColumnIndexOrThrow(TOTAL)));
                    j.put(TRANSTYPE, c.getString(c.getColumnIndexOrThrow(TRANSTYPE)));
                    j.put(MEDIA, c.getString(c.getColumnIndexOrThrow(MEDIA)));
                    j.put(GRANDTOTAL, c.getString(c.getColumnIndexOrThrow(GRANDTOTAL)));
                    j.put(WAREHOUSE, c.getString(c.getColumnIndexOrThrow(WAREHOUSE)));
                    j.put(RECEIVED, c.getString(c.getColumnIndexOrThrow(RECEIVED)));
                    j.put(WAREHOUSEFROM, c.getString(c.getColumnIndexOrThrow(WAREHOUSEFROM)));
                    j.put(BATCH, c.getString(c.getColumnIndexOrThrow(BATCH)));
                    j.put(SYSTRANSID, sytrid);
                    j.put(ACK, c.getString(c.getColumnIndexOrThrow(ACK)));
                    JSONArray transferarray = TransferItems.getJsonArray(context,trid);
                    j.put(TransferItems.ARRAYNAME,(Object)transferarray);
                    jarr.put(j);
                } while (c.moveToNext());
                jsonObject.put(ARRAYNAME, (Object) jarr);
            }
        } finally {
            if (c != null) {
                c.close();
            }
        }
        JSONObject job = null;
        if (jsonObject != null) {
            System.out.println(jsonObject.toString());
            String array = js.getJson(Constants.SERVER, jsonObject);
            job = new JSONObject(array);
        }
        return job;
    }

    /*
 * Fetching stock for a new product giving out imei of the device
 */
    private static JSONObject getTransferJson(Context context) throws JSONException
    {
        JSONObject jobs = new JSONObject();
        jobs.put("tag", Constants.TAGSGETTRANSFER);
        jobs.put(Constants.IMEI, Constants.getIMEINO(context));
        js = new UrlCon();
        String job =  js.getJson(Constants.SERVER, jobs) ;
        JSONObject json;
        try {
            json = new JSONObject(job);
            if(json.length() != 0)
                return json ;
        } catch (JSONException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        return null ;
    }

    public static void updateall(Context context,String vals){
        ContentResolver cr = context.getContentResolver();
//        String where = ID + " = "+vals;
        Log.e("UPDATING", String.valueOf(cr.delete(BASEURI, null, null)));
    }
    public static void process(Context context) throws JSONException {
        JSONObject json = sendTransferJson(context);
        if (json != null)
            try {
                if (json.has(Constants.SUCCESS)) {
                    String res = json.getString(Constants.SUCCESS);
                    if (Integer.parseInt(res) == 1) {
                        if (json.has(ARRAYNAME)) {
                            stockarray = json.getJSONArray(ARRAYNAME);
                            for (int i = 0; i < stockarray.length(); i++) {
                                JSONObject js = stockarray.getJSONObject(i);
                                String put = js.getString(BATCH);
                                String trid = js.getString(SYSTRANSID);
                                String appr = js.getString(RECEIVED);

                                String[] stockvalue = new String[]{BATCH, put, trid, "1",appr};
                                int no = 0;
                                if (Validating.areSet(stockvalue)) {
                                    if (isStockPresent(context, new String[]{BATCH, put})) {
                                        if (js.has(TransferItems.ARRAYNAME)) {
                                            JSONArray jsonArray = js.getJSONArray(TransferItems.ARRAYNAME);
                                            for (int a = 0; a < jsonArray.length(); a++) {
                                                JSONObject object = jsonArray.getJSONObject(a);
                                                String times = object.getString(TransferItems.TIMESTAMP);
                                                String itemid = object.getString(TransferItems.SYS_TRANS_ITEMID);
                                                appr = object.getString(TransferItems.STATUS);
                                                if(itemid.equals("false"))
                                                    continue;
                                                String[] v = {TransferItems.TIMESTAMP, times, itemid, trid, "1",appr};
                                                if (TransferItems.updateSyncStatus(context, v))
                                                    no += 1;
                                            }
                                            if (no == jsonArray.length())
                                                updateSyncStatus(context, stockvalue);
                                        }
                                    }
                                }
                            }
                        }
                    }
                }

            } catch (JSONException e) {
                e.printStackTrace();
            }
        stockarray = null;
        int success = 0;
        JSONObject job = getTransferJson(context);
        if (job != null)
            try {
                if (job.has(Constants.SUCCESS)) {
                    String res = job.getString(Constants.SUCCESS);
                    if (Integer.parseInt(res) == 1) {
                        if(job.has(ARRAYNAME)) {
                            stockarray = job.getJSONArray(ARRAYNAME);

                            if(stockarray.length() > 0){
                                for( int i = 0; i < stockarray.length(); i++){
                                    JSONObject js = stockarray.getJSONObject(i);
                                    String transid = js.getString(SYSTRANSID);
                                    String status = js.getString(TRANSTYPE);
                                    String wh = js.getString(WAREHOUSEFROM);
                                    String rec = js.getString(RECEIVED);
                                    String[] v = {SYSTRANSID,transid,status,wh,rec};
                                    if(isStockPresent(context, new String[]{SYSTRANSID, transid})){
                                        Log.e("TRANSFEREXIST","YES");
                                        if(js.has(TransferItems.ARRAYNAME)){
                                            Log.e("TRANSFERITEMARRAY","YES");
                                            JSONArray jsonarr = js.getJSONArray(TransferItems.ARRAYNAME);
                                            if(jsonarr.length()> 0){
                                                for(int a = 0; a< jsonarr.length(); a++){
                                                    JSONObject obj = jsonarr.getJSONObject(a);
                                                    String sysid = obj.getString(TransferItems.SYS_TRANS_ITEMID);
                                                    String sts = obj.getString(TransferItems.STATUS);
                                                    String[] vv = {TransferItems.SYS_TRANS_ITEMID,sysid,sts};
                                                    if(TransferItems.isItemExist(context,vv)){
                                                        Log.e("ITEM","YES");
                                                        if(TransferItems.approved(context,vv))
                                                            success += 1;
                                                    }
                                                }
                                                if(jsonarr.length() == success)
                                                {
                                                    if(approved(context,v))
                                                        sendAckJson(context);
                                                }
                                            }
                                        }
                                    } else {
                                        Log.e("TRANSFEREXIST", "NO");
                                        success = 0;
                                        String note = "N/A";
                                        String total = js.getString(TOTAL);;
                                        String ttax = js.getString(TOTALTAX);
                                        String status_ = js.getString(TRANSTYPE);
                                        String whto = js.getString(WAREHOUSE);
                                        String batch = js.getString(BATCH);
                                        String systrnaid = transid;
                                        String rcv = js.getString(RECEIVED);
                                        String ack_ = "2";
                                        if(rcv.equals("1"))
                                            ack_ = "2";
                                        else if(rcv.equals("2"))
                                            ack_ = "1";
                                        else if(rcv.equals("3"))
                                            ack_ = "1";
                                        String whf = wh;
                                        String media = " ";
                                        String transidl = "0";
                                        if (js.has(TransferItems.ARRAYNAME)) {
                                            Log.e("TRANSFERITEMARRAY", "YES");
                                            JSONArray jsonArray = js.getJSONArray(TransferItems.ARRAYNAME);
                                            if(jsonArray.length() > 0) {
                                                String[] transfer = {note, total, ttax, status_, total, whto, batch, systrnaid, ack_, whf, media,rec};
                                                if (Validating.areSet(transfer)) {
                                                    if (Transfer.insert(context, transfer)) {
                                                        transidl = Transfer.getId(context);
                                                        Log.e("TRANSFERISNERTED", "YES ID "+transidl);
                                                        if(Integer.parseInt(transidl) > 0)
                                                            for (int a = 0; a < jsonArray.length(); a++) {
                                                                JSONObject obj = jsonArray.getJSONObject(a);
                                                                String transitemid = obj.getString(TransferItems.SYS_TRANS_ITEMID);
                                                                String quantity = obj.getString(TransferItems.QUANTITY);
                                                                String quantityv = obj.getString(TransferItems.QUANTITY);
                                                                quantity = String.valueOf(new BigDecimal(quantity).intValue());
                                                                quantityv = String.valueOf(new BigDecimal(quantityv).intValue());
                                                                String expirydate = obj.getString(TransferItems.EXPIRY);
                                                                String sprice = obj.getString(TransferItems.UNITPRICE);
                                                                String productid = obj.getString(TransferItems.PRODUCT);
                                                                String timestamp = obj.getString(TransferItems.TIMESTAMP);
                                                                String st = obj.getString(TransferItems.STATUS);
                                                                String[] transferitem = new String[]{status, transitemid, transidl, quantity, expirydate
                                                                        , sprice, productid, transid, timestamp,ack_,quantityv,st};
                                                                if(Validating.areSet(transferitem)) {
                                                                    if (TransferItems.insert(context, transferitem)) {
                                                                        success += 1;
                                                                    }
                                                                }
                                                            }
                                                    }
                                                }
                                                if(success == jsonArray.length())
                                                    sendAckJson(context);
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }

            } catch (JSONException e) {
                e.printStackTrace();
            }
    }

    public static boolean updateDelivery(Context context, String[] vals)
    {
        ContentResolver cr = context.getContentResolver();
        ContentValues values = new ContentValues() ;
        values.put(ACK, vals[1]);
        values.put(RECEIVED, vals[2]);
        values.put(UPDATE, Constants.getDate());

        String where =  ID + " = "+ vals[0] ;
        return cr.update(BASEURI, values, where, null) > 0;
    }


    public static String getTransIdSy(Context context, long productname)
    {
        ContentResolver cr = context.getContentResolver();
        String where = ID +  " = " + productname;
        Cursor cursor = cr.query(BASEURI, new String[]{SYSTRANSID}, where, null, null) ;
        try{
        if(cursor != null && cursor.moveToFirst())
        {
        		String quantity = cursor.getString(cursor.getColumnIndexOrThrow(SYSTRANSID));
        		return quantity;
        }
    }
        finally {
		 if(cursor != null) {
		        cursor.close();
	        }
	 }
        return "0";
    }


    public static String getRefNo(Context context, String ordid) {
        String where = ID +" = " +ordid;
        ContentResolver cr = context.getContentResolver();
        Cursor c = cr.query(BASEURI, new String[]{BATCH}, where, null, null) ;
        try {
            String i= "0" ;
            if(c != null && c.moveToFirst())
            {
                i = c.getString(c.getColumnIndexOrThrow(BATCH));
                return i ;
            }
        }finally {
            if(c != null) {
                c.close();
            }
        }
        return "0" ;
    }


    public static int getRecStatus(Context context, long prodid)
    {
        ContentResolver cr = context.getContentResolver();
        String where = ID +  " = " + prodid;
        Cursor cursor = cr.query(BASEURI, new String[]{RECEIVED}, where, null, null) ;
        try{
        if(cursor != null && cursor.moveToFirst())
        {
        		return  cursor.getInt(cursor.getColumnIndexOrThrow(RECEIVED));
        }
    }
        finally {
		 if(cursor != null) {
		        cursor.close();
	        }
	 }
        return 0;
    }

    /*
 *  function to update in the stock table to those products that are existing
 */
    public static boolean approved(Context context, String[] string)
    {
        boolean ret = false;
        ContentResolver cr = context.getContentResolver();
        ContentValues values = new ContentValues() ;
        values.put(TRANSTYPE, string[2]);
        values.put(RECEIVED, string[4]);
        values.put(WAREHOUSEFROM, string[3]);

        String where = string[0] + " = "+ DatabaseUtils.sqlEscapeString(string[1]) ;
        Log.e("UPDATING", "UPDATE THE APPROVED TRANSFER ");
        if(cr.update(BASEURI, values, where, null) > 0) {
            ret = true ;
        }
        return ret;
    }


    public static int getStockLocalid(Context context, String productname)
    {
        ContentResolver cr = context.getContentResolver();
        String where = NOTE +  " = " + productname;
        Cursor cursor = cr.query(BASEURI, new String[] {ID}, where, null, null) ;
        try{
        if(cursor.moveToFirst())
        {
        	do{
        		int quantity = cursor.getInt(cursor.getColumnIndexOrThrow(ID));
        		return quantity;
        	} while (cursor.moveToNext());
        }
    }finally {
		 if(cursor != null) {
		        cursor.close();
	        }
	 }
        return 0;
    }

    public long getId() {
        return id;
    }

    public String getTransid() {
        return transid;
    }

    public String getBatch() {
        return batch;
    }

    public String getWarehouse() {
        return warehouse;
    }

    public String getWarehousefrom() {
        return warehousefrom;
    }

    public String getStatus() {
        return status;
    }

    public String getGtotal() {
        return gtotal;
    }
    public String getDate() {
        return date;
    }

    public String getMedia() {
        return media;
    }

    public int getAck() {
        return ack;
    }

    public int getApproval() {
        return approval;
    }

    public Transfer(String gtotal, String status, String warehousefrom, String warehouse, String batch,
                    String transid, String date,String media, int ack,int approval, long id) {
        this.gtotal = gtotal;
        this.status = status;
        this.warehousefrom = warehousefrom;
        this.warehouse = warehouse;
        this.batch = batch;
        this.transid = transid;
        this.date = date;
        this.id = id;
        this.ack = ack;
        this.approval = approval;
        this.media = media;
    }
}