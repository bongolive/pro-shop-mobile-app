/*
 * The source codes in this project are owned by Bongolive Enterprises. Copying or any kind of usage without Bongolive's approval is against the law.
 * Copyright (c) 2015.
 *
 */

package bongolive.apps.proshop.model;

import android.content.ContentResolver;
import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.DatabaseUtils;
import android.net.Uri;

import com.jjoe64.graphview.series.DataPoint;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import bongolive.apps.proshop.controller.Constants;
import bongolive.apps.proshop.controller.UrlCon;
import bongolive.apps.proshop.controller.Validating;


public class Payments implements Serializable{
    public static final String TABLENAME = "payment" ;
    public static final String ID = "_id";
    public static final String POSTPAID = "post_paid";
    public static final String POSTBALANCE ="post_balance" ;
    public static final String PAYID ="id" ;
    public static final String COMMENT ="note" ;
    public static final String PAYDATE = "date" ;
    public static final String UPDATEDDATE = "date_update" ;
    public static final String AMOUNT = "amount" ;
    public static final String LOCALSALE = "loc_sale_id" ;
    public static final String SYS_SALE = "sale_id";
    public static final String PAIDBY = "paid_by";
    public static final String RECEIVEDBY = "received_by";
    public static final String ATTACHEMENT = "attachement";
    public static final String TYPE = "type";
    public static final String NODETYPE = "node_type";
    public static final String ACK = "ack" ;
    public static final String CREATED = "created_at";
    public static final String REFRENCENO = "reference_no";

    private static UrlCon js ;

    String postpaid,postbalance,payid,comment,paydate,updated,amount,localsale,syssale,paidby,refer,
            attachment,ack,type,recby;
    long id;
    int nodetype;
    public List<Payments> invisibleChildren;

    public static final Uri BASEURI = Uri.parse("content://"+ ContentProviderApi.AUTHORITY+"/"+TABLENAME) ;
    public static final String SPECIFIC_CONTENT_TYPE = "vnd.android.cursor.item/vnd.com.proshop.payment" ;
    public static final String GENERAL_CONTENT_TYPE = "vnd.android.cursor.dir/vnd.com.proshop.payment" ;
    private static final String ARRAYNAME = "payment_list";

    public static boolean insert(Context context, String[] orders){
        int ordercount = getCount(context) ;
        ContentResolver cr = context.getContentResolver() ;
        ContentValues cv = new ContentValues() ;
        cv.put(POSTPAID, orders[0]);
        cv.put(POSTBALANCE, orders[1]);
        cv.put(PAYID, 0);
        cv.put(COMMENT, orders[2]);
        cv.put(PAYDATE, orders[3]);
        cv.put(UPDATEDDATE, Constants.getDate());
        cv.put(AMOUNT, orders[4]);
        cv.put(LOCALSALE, orders[5]);
        cv.put(SYS_SALE, orders[6]);
        cv.put(PAIDBY, orders[7]);
        cv.put(ATTACHEMENT, orders[8]);
        cv.put(TYPE, orders[9]);
        cv.put(ACK, orders[10]);
        cv.put(REFRENCENO, orders[11]);
        cv.put(RECEIVEDBY, orders[12]);
        cv.put(NODETYPE, orders[13]);
        cv.put(CREATED, Constants.getDate());
        System.out.println(" refno "+orders[11]);

        cr.insert(BASEURI, cv);
        return getCount(context) == ordercount + 1;
    }
    public static boolean update(Context context, String[] orders){
        boolean ret = false;

        ContentResolver cr = context.getContentResolver() ;
        ContentValues cv = new ContentValues() ;
        cv.put(POSTPAID, orders[1]);
        cv.put(POSTBALANCE, orders[2]);
        cv.put(UPDATEDDATE, Constants.getDate());
        cv.put(PAIDBY, orders[3]);
        cv.put(ATTACHEMENT, orders[4]);
        cv.put(RECEIVEDBY, orders[5]);
        cv.put(ACK, 0);
        System.out.println(" refno " + orders[11]);

        String where = ID + " = "+orders[0];
        ret = cr.update(BASEURI,cv,where,null)> 0;
        return  ret;
    }
    //gettotaltax,getpayamount
    public static int getCount(Context context){
        ContentResolver cr = context.getContentResolver() ;
        Cursor _c = cr.query(BASEURI, null, null, null, null);
        int count = _c.getCount() ;
        _c.close() ;
        return count;
    }

    private static ArrayList<String> getIds(Context context){
        ArrayList<String> lst = new ArrayList<>();
        ContentResolver cr = context.getContentResolver() ;

        Cursor c = cr.query(BASEURI, null, null, null, null);
        try {
            if (c.moveToFirst()) {
                do {

                } while (c.moveToNext());
                return lst;
            }
        } finally {
            if(c!= null)
                c.close();
        }
        return  null;
    }


    public static String getLocalSaleId(Context context, String str) {
        ContentResolver cr = context.getContentResolver();
        String where = POSTBALANCE + " = " + DatabaseUtils.sqlEscapeString(str);

        Cursor c = cr.query(BASEURI, new String[]{ID}, where, null, null);
        try {
            String cu = null;
            if (c.moveToFirst()) {
                cu = c.getString(c.getColumnIndexOrThrow(ID));
                return cu;
            }
        } finally {
            if (c != null) {
                c.close();
            }
        }
        return "0";
    }

    public static boolean anyChild(Context context, String str) {
        ContentResolver cr = context.getContentResolver();
        String where = LOCALSALE + " = " + str + " AND "+NODETYPE+" = 1";
        boolean cu = false;
        Cursor c = cr.query(BASEURI,null, where, null, null);
        try {

            if (c != null && c.moveToFirst()) {
                cu = true;
            }
        } finally {
            if (c != null) {
                c.close();
            }
        }
        return cu;
    }

    /*
         * Fetching sales from server if sent from this device
         */
    public static JSONObject getDeliveryJson(Context context) throws JSONException
    {
        JSONObject jobs = new JSONObject();
        jobs.put("tag", Constants.TAGSRECEIVEDELIVERY);
        jobs.put(Constants.IMEI, Constants.getIMEINO(context));
        js = new UrlCon();
        String job =  js.getJson(Constants.SERVER, jobs) ;
        JSONObject json;
        try {
            json = new JSONObject(job);
            if(json != null)
            {
                return json ;
            }
        } catch (JSONException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        return null ;
    }


    public static ArrayList<Payments> getPaymentItems(Context context){
        ArrayList<Payments> list = new ArrayList<>();
        ContentResolver cr = context.getContentResolver();
//        String where = Products.REODER +" = 1";
        Cursor c = cr.query(BASEURI,null,null,null,LOCALSALE+" ASC");
        try{
            if(c != null && c.getCount() > 0){
                c.moveToFirst();
                do {
                    String sl = c.getString(c.getColumnIndex(SYS_SALE));
                    String lsl = c.getString(c.getColumnIndex(LOCALSALE));
                    String pd = c.getString(c.getColumnIndex(AMOUNT));
                    String bl = c.getString(c.getColumnIndex(POSTBALANCE));
                    String dt = c.getString(c.getColumnIndex(PAYDATE));
                    long i = c.getLong(c.getColumnIndex(ID));
                    int nt = c.getInt(c.getColumnIndex(NODETYPE));
                    list.add(new Payments(bl,dt,pd,lsl,sl,i,nt));
                } while (c.moveToNext());
            }
        } finally {
            if(c != null){
                c.close();
            }
        }
        return list;
    }


    public static ArrayList<Payments> getPaymentSummary(Context context,long id){
        ArrayList<Payments> list = new ArrayList<>();
        ContentResolver cr = context.getContentResolver();
        String where = ID +" = "+id;
        Cursor c = cr.query(BASEURI,null,where,null,PAYDATE+" DESC");
        try{
            if(c != null && c.getCount() > 0){
                c.moveToFirst();
                do {
                    String sl = c.getString(c.getColumnIndex(SYS_SALE));
                    String lsl = c.getString(c.getColumnIndex(LOCALSALE));
                    String pd = c.getString(c.getColumnIndex(AMOUNT));
                    String bl = c.getString(c.getColumnIndex(POSTBALANCE));
                    String ac = c.getString(c.getColumnIndex(ACK));
                    String dt = c.getString(c.getColumnIndex(PAYDATE));
                    String pp = c.getString(c.getColumnIndex(POSTPAID));
                    String ty = c.getString(c.getColumnIndex(TYPE));
                    String ref = c.getString(c.getColumnIndex(REFRENCENO));
                    String com = c.getString(c.getColumnIndex(COMMENT));
                    String att = c.getString(c.getColumnIndex(ATTACHEMENT));
                    String pby = c.getString(c.getColumnIndex(PAIDBY));
                    String upd = c.getString(c.getColumnIndex(UPDATEDDATE));
                    String recby = c.getString(c.getColumnIndex(RECEIVEDBY));
                    long i = c.getLong(c.getColumnIndex(ID));
                    list.add(new Payments(pp,bl,com,dt,pd,lsl,sl,pby,ref,att,ac,ty,id,upd,recby));
                } while (c.moveToNext());
            }
        } finally {
            if(c != null){
                c.close();
            }
        }
        return list;
    }

    public static JSONObject sendJson(Context context) throws JSONException
    {

        ContentResolver cr = context.getContentResolver() ;

        String where = ACK + " = 0 AND "+SYS_SALE +" != 0" ;
        Cursor c = cr.query(BASEURI, null, where, null, null);

        js = new UrlCon();
        try {
            JSONObject jsonObject = new JSONObject();
            jsonObject.put("tag", Constants.TAGSENDPAYMENT);
            jsonObject.put(Constants.IMEI, Constants.getIMEINO(context));
            String array ;
            if(c != null && c.moveToFirst())
            {
                JSONArray jarr = new JSONArray();
                do{
                    JSONObject j = new JSONObject();
                    j.put(ATTACHEMENT, c.getString(c.getColumnIndexOrThrow(ATTACHEMENT)));
                    j.put(PAYID, c.getString(c.getColumnIndexOrThrow(PAYID)));
                    j.put(PAYDATE, c.getString(c.getColumnIndexOrThrow(PAYDATE)));
                    j.put(UPDATEDDATE, c.getString(c.getColumnIndexOrThrow(UPDATEDDATE)));
                    j.put(TYPE, c.getString(c.getColumnIndexOrThrow(TYPE)));
                    j.put(ACK, c.getString(c.getColumnIndexOrThrow(ACK)));
                    j.put(AMOUNT, c.getString(c.getColumnIndexOrThrow(AMOUNT)));
                    j.put(LOCALSALE, c.getString(c.getColumnIndexOrThrow(LOCALSALE)));
                    j.put(POSTBALANCE, c.getString(c.getColumnIndexOrThrow(POSTBALANCE)));
                    j.put(COMMENT, c.getString(c.getColumnIndexOrThrow(COMMENT)));
                    j.put(ID, c.getString(c.getColumnIndexOrThrow(ID)));
                    j.put(PAIDBY, c.getString(c.getColumnIndexOrThrow(PAIDBY)));
                    j.put(REFRENCENO, c.getString(c.getColumnIndexOrThrow(REFRENCENO)));
                    j.put(SYS_SALE, c.getString(c.getColumnIndexOrThrow(SYS_SALE)));
                    j.put(RECEIVEDBY, c.getString(c.getColumnIndexOrThrow(RECEIVEDBY)));
                    jarr.put(j);
                } while(c.moveToNext()) ;
                jsonObject.put(ARRAYNAME, jarr);
                System.out.println(jsonObject.toString());
                array = js.getJson(Constants.SERVER, jsonObject) ;
                JSONObject job =  new JSONObject(array);
                return job ;
            }
        }finally {
            if(c != null) {
                c.close();
            }
        }
        return null ;
    }


    public static void processInformation(Context context) throws JSONException{

        JSONObject jsonin = sendJson(context);
        if(jsonin != null)
            try {
                if(jsonin.has(Constants.SUCCESS)){
                    String res = jsonin.getString(Constants.SUCCESS) ;
                    if(Integer.parseInt(res) == 1)
                    {
                        if(jsonin.has(ARRAYNAME)) {
                            JSONArray jsonArray = jsonin.getJSONArray(ARRAYNAME);
                            for (int i = 0; i < jsonArray.length(); i++) {
                                JSONObject job = jsonArray.getJSONObject(i);

                                String saleid = job.getString(PAYID);
                                String id = job.getString(ID);
                                String[] chek = {saleid,id};

                                if (Validating.areSet(chek)) {
                                    updateSync(context,chek);
                                }
                            }
                        }
                    }
                }
            } catch (NumberFormatException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            } catch (JSONException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
    }


    /* check if the sale id is present in the app
    *
    */

    private static boolean isSalePresentFromSystem(Context context, String custid)
    {
        ContentResolver cr = context.getContentResolver();
        boolean ret = false;
        String where = SYS_SALE +" = "+custid;
        Cursor c = cr.query(BASEURI, null, where, null, null);
        try {
            if (c.moveToFirst()) {
                ret = true ;
            }
        } finally {
            if (c != null) {
                c.close();
            }
        }
        return ret;
    }
    private static boolean isSalePresentFromSystem(Context context, String[] custid)
    {
        ContentResolver cr = context.getContentResolver();
        boolean ret = false;
        String where = PAIDBY +" = "+custid[0]+" AND "+SYS_SALE +" = "+custid[1];
        Cursor c = cr.query(BASEURI, null, where, null, null);
        try {
            if (c.moveToFirst()) {
                ret = true ;
            }
        } finally {
            if (c != null) {
                c.close();
            }
        }
        return ret;
    }
    public static String getSaleReference(Context context, long custid)
    {
        ContentResolver cr = context.getContentResolver();
        String ref = null;
        String where = LOCALSALE +" = "+custid;
        System.out.println(where + " where ");
        Cursor c = cr.query(BASEURI, null, where, null, null);
        try {
            if (c.moveToFirst()) {
                ref = c.getString(c.getColumnIndex(REFRENCENO)) ;
                System.out.println(ref);
            }
        } finally {
            if (c != null) {
                c.close();
            }
        }
        return ref;
    }
    /*
    * Acknowledgement to the server to tick item as a go
    */
    public static void sendAck(Context context, String[] requiredid) throws JSONException
    {
        JSONObject jobs = new JSONObject();
        jobs.put("tag", Constants.TAGACK);
        jobs.put(Constants.ACK, Constants.ACKVALUE);
        jobs.put(Constants.IMEI, Constants.getIMEINO(context));
        jobs.put(Constants.ACKREFERENCE, requiredid[0]);
        jobs.put(Constants.ACKKEY, SYS_SALE);
        js = new UrlCon();
        js.getJson(Constants.SERVER, jobs) ;
    }

    public static boolean setSaleId(Context context, String[] vals)
    {
        ContentResolver cr = context.getContentResolver();
        ContentValues values = new ContentValues() ;
        values.put(SYS_SALE, vals[1]);
        String where =  LOCALSALE + " = " +vals[0] ;
        return cr.update(BASEURI, values, where, null) > 0;
    }

    public static boolean updateSync(Context context, String[] vals)
    {
        ContentResolver cr = context.getContentResolver();
        ContentValues values = new ContentValues() ;
        values.put(ACK, 1);
        values.put(PAYID, vals[0]);
        String where =  ID + " = " +vals[1] ;
        return cr.update(BASEURI, values, where, null) == 1;
    }

    public static boolean updateDelivery(Context context, String[] vals)
    {
        ContentResolver cr = context.getContentResolver();
        ContentValues values = new ContentValues() ;
        values.put(UPDATEDDATE, Constants.getDate());
        values.put(PAYID, Constants.getDate());
        values.put(ACK, 2);
        String where =  REFRENCENO+ " = "+ DatabaseUtils.sqlEscapeString(vals[0]) ;
        return cr.update(BASEURI, values, where, null) > 0;
    }

    public static int isOrderSynced(Context context, String businessnm)
    {
        ContentResolver cr = context.getContentResolver();
        String where = ID + " = " + businessnm ;

        Cursor c = cr.query(BASEURI, new String[] {ACK}, where,null,null) ;
        try {
            int i= 0 ;
            if(c.moveToFirst())
            {
                i = c.getInt(c.getColumnIndexOrThrow(ACK));
                return i ;
            }
        } finally {
            if(c != null) {
                c.close();
            }
        }
        return 0;
    }

    public static int getId(Context context) {
        ContentResolver cr = context.getContentResolver();
        Cursor c = cr.query(BASEURI, new String[] {ID}, null,null,null) ;
        try {
            int i= 0 ;
            if(c.moveToLast())
            {
                i = c.getInt(c.getColumnIndexOrThrow(ID));
                return i ;
            }
        }finally {
            if(c != null) {
                c.close();
            }
        }
        return 0 ;
    }



    public static void sendAckJson(Context context) throws JSONException {
        JSONObject jobs = new JSONObject();
        jobs.put("tag", Constants.TAGACK);
        jobs.put(Constants.ACKITEM, TABLENAME);
        jobs.put(Constants.IMEI, Constants.getIMEINO(context));
        js = new UrlCon();
        System.out.println(jobs.toString());
        js.getJson(Constants.SERVER, jobs) ;
    }


    public static double getDailySummary(Context context, int flag){
        ContentResolver cr = context.getContentResolver() ;
        String where = null ;
        switch(flag)
        {
            case 0:
                where = ATTACHEMENT + " >=  date('now')";

                break;
            case 1:
                where = ATTACHEMENT + " >= date('now','-7 day')";
                //date('now','start of month','+0 month','0 day') AND "+ ATTACHEMENT + " <= date('now','start of month','+0 month','-1 day')";
                break;
            case 2:
                where = ATTACHEMENT + " >=  date('now','start of month','-1 month') AND "+
                        ATTACHEMENT + " < date('now','start of month','-1 day')";
                //date('now','start of month','+1 month','-1 day')
                break;
        }
        Cursor c = cr.query(BASEURI, null, where, null, null);
        double total = 0;
        try{
            if(c.moveToFirst()){
                do{
                    total += c.getDouble(c.getColumnIndexOrThrow(PAYID));
                } while (c.moveToNext());
                return total ;
            }
        }finally{
            if(c != null) {
                c.close();
            }
        }
        return 0;
    }

    public static DataPoint[] getTotalSalesGraph(Context context){

        DataPoint[] vals = null;
        String query = " select sum(case  cast(strftime('%w',"+ATTACHEMENT+ ") as integer) when 0 then IFNULL("+UPDATEDDATE+"," +
                " 0)   else 0  end) as sunday ," +
                "sum(case  cast(strftime('%w',"+ATTACHEMENT+") as integer) when 1 then IFNULL("+UPDATEDDATE+",0)  else 0 end) as" +
                " monday ," +
                "sum(case  cast(strftime('%w',"+ATTACHEMENT+") as integer) when 2 then IFNULL("+UPDATEDDATE+",0)   else 0 end) as tuesday ," +
                "sum(case  cast(strftime('%w',"+ATTACHEMENT+") as integer) when 3 then IFNULL("+UPDATEDDATE+",0)   else 0 end) as wednesday ," +
                "sum(case  cast(strftime('%w',"+ATTACHEMENT+") as integer) when 4 then IFNULL("+UPDATEDDATE+",0)   else 0 end) as thursday ," +
                "sum(case  cast(strftime('%w',"+ATTACHEMENT+") as integer) when 5 then IFNULL("+UPDATEDDATE+",0)   else 0 end) as friday ," +
                "sum(case  cast(strftime('%w',"+ATTACHEMENT+") as integer) when 6 then IFNULL("+UPDATEDDATE+",0)   else 0 end) as saturday " +
                "from "+TABLENAME+" where  strftime('%W',"+ATTACHEMENT+") = strftime('%W','now') group by strftime('%W','now') ";
        final Cursor c1 = DatabaseHandler.getInstance(context).getReadableDatabase().rawQuery(query, null);
        if (c1 != null) {
            vals = new DataPoint[c1.getCount()];
            try {
                if(c1.moveToFirst()) {
                    double mon = 0, tues = 0, wed = 0, thur = 0, frd = 0, sat = 0, sun = 0;
                    mon = c1.getDouble(c1.getColumnIndex("monday"));
                    tues = c1.getDouble(c1.getColumnIndex("tuesday"));
                    wed = c1.getDouble(c1.getColumnIndex("wednesday"));
                    thur = c1.getDouble(c1.getColumnIndex("thursday"));
                    frd = c1.getDouble(c1.getColumnIndex("friday"));
                    sat = c1.getDouble(c1.getColumnIndex("saturday"));
                    sun = c1.getDouble(c1.getColumnIndex("sunday"));
                    vals = new DataPoint[]{new DataPoint(0, sun), new DataPoint(1, mon), new DataPoint(2, tues),
                            new DataPoint(3, wed), new DataPoint(4, thur), new DataPoint(5, frd), new DataPoint(6, sat)};
                }
            } finally {
                c1.close();
            }
        }
        return vals;
    }

    public static DataPoint[] getTotalOutstandingGraph(Context context){

        DataPoint[] vals = null;
        String query = " select sum(case  cast(strftime('%w',"+ATTACHEMENT+ ") as integer) when 0 then IFNULL("+PAYID+"," +
                " 0)   else 0  end) as sunday ," +
                "sum(case  cast(strftime('%w',"+ATTACHEMENT+") as integer) when 1 then IFNULL("+PAYID+",0)  else 0 end) as" +
                " monday ," +
                "sum(case  cast(strftime('%w',"+ATTACHEMENT+") as integer) when 2 then IFNULL("+PAYID+",0)   else 0 end) as tuesday ," +
                "sum(case  cast(strftime('%w',"+ATTACHEMENT+") as integer) when 3 then IFNULL("+PAYID+",0)   else 0 end) as wednesday ," +
                "sum(case  cast(strftime('%w',"+ATTACHEMENT+") as integer) when 4 then IFNULL("+PAYID+",0)   else 0 end) as thursday ," +
                "sum(case  cast(strftime('%w',"+ATTACHEMENT+") as integer) when 5 then IFNULL("+PAYID+",0)   else 0 end) as friday ," +
                "sum(case  cast(strftime('%w',"+ATTACHEMENT+") as integer) when 6 then IFNULL("+PAYID+",0)   else 0 end) as saturday " +
                "from "+TABLENAME+" where  strftime('%W',"+ATTACHEMENT+") = strftime('%W','now') group by strftime('%W','now') ";
        final Cursor c1 = DatabaseHandler.getInstance(context).getReadableDatabase().rawQuery(query, null);
        if (c1 != null) {
            vals = new DataPoint[c1.getCount()];
            try {
                if(c1.moveToFirst()) {
                    double mon = 0, tues = 0, wed = 0, thur = 0, frd = 0, sat = 0, sun = 0;
                    mon = c1.getDouble(c1.getColumnIndex("monday"));
                    tues = c1.getDouble(c1.getColumnIndex("tuesday"));
                    wed = c1.getDouble(c1.getColumnIndex("wednesday"));
                    thur = c1.getDouble(c1.getColumnIndex("thursday"));
                    frd = c1.getDouble(c1.getColumnIndex("friday"));
                    sat = c1.getDouble(c1.getColumnIndex("saturday"));
                    sun = c1.getDouble(c1.getColumnIndex("sunday"));
                    vals = new DataPoint[]{new DataPoint(0, sun), new DataPoint(1, mon), new DataPoint(2, tues),
                            new DataPoint(3, wed), new DataPoint(4, thur), new DataPoint(5, frd), new DataPoint(6, sat)};
                }
            } finally {
                c1.close();
            }
        }
        return vals;
    }


    public static double getGrandTotal(Context context, long status) {
        ContentResolver cr = context.getContentResolver();
        String where = ID + " = " + status;
        Cursor c = cr.query(BASEURI, new String[]{PAYDATE}, where, null, null) ;
        try {
            double i= 0 ;
            if(c.moveToFirst())
            {
                i = c.getDouble(c.getColumnIndexOrThrow(PAYDATE));
                return i ;
            }
        }finally {
            if(c != null) {
                c.close();
            }
        }
        return 0 ;
    }

    public static ArrayList<String> deliveryDetails(Context context, long id){
        ArrayList<String> list = new ArrayList<>();
        ContentResolver cr = context.getContentResolver();
        String where = LOCALSALE + " = " + id;
        Cursor c = cr.query(BASEURI, null, where,null,null) ;
        try {
            double i= 0 ;
            if(c.moveToFirst())
            {
                list.add(1,c.getString(c.getColumnIndex(ATTACHEMENT)));
                list.add(2,c.getString(c.getColumnIndex(POSTBALANCE)));
                list.add(3,c.getString(c.getColumnIndex(REFRENCENO)));
                list.add(6,c.getString(c.getColumnIndex(TYPE)));
                list.add(7,c.getString(c.getColumnIndex(COMMENT)));
            }
        }finally {
            if(c != null) {
                c.close();
            }
        }
        return list;
    }
    public static ArrayList<String> getprintdetails(Context context, long id){
        ArrayList<String> list = new ArrayList<>();
        ContentResolver cr = context.getContentResolver();
        String where = LOCALSALE + " = " + id;
        Cursor c = cr.query(BASEURI, null, where,null,null) ;
        try {
            double i= 0 ;
            if(c.moveToFirst())
            {
                list.add(Constants.getDate());
                list.add(c.getString(c.getColumnIndex(REFRENCENO)));
                list.add(c.getString(c.getColumnIndex(POSTBALANCE)));
                list.add(c.getString(c.getColumnIndex(TYPE)));
                list.add(c.getString(c.getColumnIndex(COMMENT)));
            }
        }finally {
            if(c != null) {
                c.close();
            }
        }
        return list;
    }

    public Payments(String postpaid, String postbalance, String comment, String paydate,
                    String amount, String localsale, String syssale, String paidby, String refer,
                    String attachment, String ack, String type, long id, String updated, String recby) {
        this.postpaid = postpaid;
        this.postbalance = postbalance;
        this.comment = comment;
        this.paydate = paydate;
        this.amount = amount;
        this.localsale = localsale;
        this.syssale = syssale;
        this.paidby = paidby;
        this.refer = refer;
        this.attachment = attachment;
        this.ack = ack;
        this.type = type;
        this.id = id;
        this.updated = updated;
        this.recby = recby;
    }

    public Payments(String postbalance, String paydate,
                    String amount, String localsale, String syssale, long id,int nodetype) {
        this.postbalance = postbalance;
        this.paydate = paydate;
        this.amount = amount;
        this.localsale = localsale;
        this.syssale = syssale;
        this.id = id;
        this.nodetype = nodetype;
    }

    public Payments(String postbalance, String paydate,
                    String amount, String localsale, String syssale, long id) {
        this.postbalance = postbalance;
        this.paydate = paydate;
        this.amount = amount;
        this.localsale = localsale;
        this.syssale = syssale;
        this.id = id;
        this.nodetype = nodetype;
    }

    public long getId() {
        return id;
    }

    public String getType() {
        return type;
    }

    public String getAck() {
        return ack;
    }

    public String getAttachment() {
        return attachment;
    }

    public String getRefer() {
        return refer;
    }

    public String getPaidby() {
        return paidby;
    }

    public String getSyssale() {
        return syssale;
    }

    public String getLocalsale() {
        return localsale;
    }

    public String getAmount() {
        return amount;
    }

    public String getUpdated() {
        return updated;
    }

    public String getPaydate() {
        return paydate;
    }

    public String getComment() {
        return comment;
    }

    public String getPayid() {
        return payid;
    }

    public String getPostbalance() {
        return postbalance;
    }

    public String getPostpaid() {
        return postpaid;
    }

    public String getRecby() {
        return recby;
    }

    public int getNodetype() {
        return nodetype;
    }
}
