package bongolive.apps.proshop.model;

import android.content.Context;
import android.os.AsyncTask;
import android.os.Environment;
import android.preference.CheckBoxPreference;
import android.util.Log;
import android.widget.Toast;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.channels.FileChannel;

import bongolive.apps.proshop.view.AlertDialogManager;
import bongolive.apps.proshop.controller.Constants;

/**
 * Created by nasznjoka on 2/22/2015.
 */
public class ExportDatabase extends AsyncTask<String, Void, Boolean> {
	Context ctx;
	CheckBoxPreference chk;
	public ExportDatabase(Context context, CheckBoxPreference checkBoxPreference){
		this.ctx = context;
		this.chk = checkBoxPreference;
	}
//	private final ProgressDialog dialog = new ProgressDialog(ctx);

	// can use UI thread here
	protected void onPreExecute() {/*
		this.dialog.setMessage("Exporting database...");
		this.dialog.show();*/
	}

	// automatically done on worker thread (separate from UI thread)
	protected Boolean doInBackground(final String... args) {

		File dbFile =new File(Environment.getDataDirectory() + "/data/bongolive.apps.proshop/databases/proshop");
        Log.v("dbpath","path is "+ctx.getDatabasePath(DatabaseHandler.DBNAME).toString());

		File sdcard = Environment.getExternalStorageDirectory() ;

		File folder = new File(sdcard.getAbsoluteFile(), Constants.MASTER_DIR);

		if (!folder.exists()) {
			folder.mkdirs();
		}

		String str = Constants.getIMEINO(this.ctx);
		File file = new File(folder.getAbsoluteFile(), str) ;

		try {
			try {

				file.createNewFile();
			} catch (IOException e) {
				e.printStackTrace();
			}
			this.copyFile(dbFile, file);
			return true;
		} catch (IOException e) {
			Log.e("mypck", e.getMessage(), e);
			return false;
		}
	}

	// can use UI thread here
	protected void onPostExecute(final Boolean success) {
		/*if (this.dialog.isShowing()) {
			this.dialog.dismiss();
		}*/
		if (success) {
			AlertDialogManager dialog = new AlertDialogManager();
			String str = Constants.getIMEINO(ctx);
			dialog.showAlertDialog(ctx, "DATABASE EXPORT", "Export successful! \n " +
					"please locate the back up in the memory card within a folder called pro_shop  and a file called " +
                    "" +
                    str, Boolean.valueOf(false));
			this.chk.setChecked(false);
			chk.setChecked(false);
		} else {
			Toast.makeText(ctx, "Export failed", Toast.LENGTH_SHORT).show();
			chk.setChecked(false);
		}
	}

	void copyFile(File src, File dst) throws IOException {
		FileChannel inChannel = new FileInputStream(src).getChannel();
		FileChannel outChannel = new FileOutputStream(dst).getChannel();
		try {
			inChannel.transferTo(0, inChannel.size(), outChannel);
		} finally {
			if (inChannel != null)
				inChannel.close();
			if (outChannel != null)
				outChannel.close();
		}
	}

    /*
    *


Guys here is answer that I have implemented successfully

//new async task for file export to csv
private class ExportDatabaseCSVTask extends AsyncTask<String, String, Boolean> {
    private final ProgressDialog dialog = new ProgressDialog(SearchResultActivity.this);
    boolean memoryErr = false;

    // to show Loading dialog box
    @Override
    protected void onPreExecute() {
        this.dialog.setMessage("Exporting database...");
        this.dialog.show();
    }

    // to write process
    protected Boolean doInBackground(final String... args) {

        boolean success = false;

        String currentDateString = new SimpleDateFormat(Constants.SimpleDtFrmt_ddMMyyyy).format(new Date());

        File dbFile = getDatabasePath("HLPL_FRETE.db");
        Log.v(TAG, "Db path is: " + dbFile); // get the path of db
        File exportDir = new File(Environment.getExternalStorageDirectory() + File.separator + Constants.FileNm.FILE_DIR_NM, "");

        long freeBytesInternal = new File(getApplicationContext().getFilesDir().getAbsoluteFile().toString()).getFreeSpace();
        long megAvailable = freeBytesInternal / 1048576;

        if (megAvailable < 0.1) {
            System.out.println("Please check"+megAvailable);
            memoryErr = true;
        }else {
            exportDirStr = exportDir.toString();// to show in dialogbox
            Log.v(TAG, "exportDir path::" + exportDir);
            if (!exportDir.exists()) {
                exportDir.mkdirs();
            }
            try {
                List<SalesActivity> listdata = salesLst;
                SalesActivity sa = null;
                String lob = null;
                for (int index = 0; index < listdata.size();) {
                    sa = listdata.get(index);
                    lob = sa.getLob();
                    break;
                }
                if (Constants.Common.OCEAN_LOB.equals(lob)) {

                    file = new File(exportDir, Constants.FileNm.FILE_OFS + currentDateString + ".csv");
                } else {
                    file = new File(exportDir, Constants.FileNm.FILE_AFS + currentDateString + ".csv");
                }
                file.createNewFile();
                CSVWriter csvWrite = new CSVWriter(new FileWriter(file));


                // this is the Column of the table and same for Header of CSV
                // file
                if (Constants.Common.OCEAN_LOB.equals(lob)) {
                    csvWrite.writeNext(Constants.FileNm.CSV_O_HEADER);
                }else{
                    csvWrite.writeNext(Constants.FileNm.CSV_A_HEADER);
                }
                String arrStr1[] = { "SR.No", "CUTSOMER NAME", "PROSPECT", "PORT OF LOAD", "PORT OF DISCHARGE" };
                csvWrite.writeNext(arrStr1);

                if (listdata.size() > 0) {
                    for (int index = 0; index < listdata.size(); index++) {
                        sa = listdata.get(index);
                        String pol;
                        String pod;
                        if (Constants.Common.OCEAN_LOB.equals(sa.getLob())) {
                            pol = sa.getPortOfLoadingOENm();
                            pod = sa.getPortOfDischargeOENm();
                        } else {
                            pol = sa.getAirportOfLoadNm();
                            pod = sa.getAirportOfDischargeNm();
                        }
                        int srNo = index;
                        String arrStr[] = { String.valueOf(srNo + 1), sa.getCustomerNm(), sa.getProspectNm(), pol, pod };
                        csvWrite.writeNext(arrStr);
                    }
                    success = true;
                }
                csvWrite.close();

            } catch (IOException e) {
                Log.e("SearchResultActivity", e.getMessage(), e);
                return success;
            }
        }
        return success;
    }

    // close dialog and give msg
    protected void onPostExecute(Boolean success) {
        if (this.dialog.isShowing()) {
            this.dialog.dismiss();
        }
        if (success) {
            dialogBox(Constants.Flag.FLAG_EXPRT_S);
        } else {
            if (memoryErr==true) {
                dialogBox(Constants.Flag.FLAG_MEMORY_ERR);
            } else {
                dialogBox(Constants.Flag.FLAG_EXPRT_F);
            }
        }
    }
}

*/
}
