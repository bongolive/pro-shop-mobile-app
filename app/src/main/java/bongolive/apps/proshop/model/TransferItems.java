/*
 * Copyright (c) 2015.
 * Prosurvey is a property of Bongolive Enterprises Ltd. An authorized
 * copy of ideas, source codes is against the regulations
 */

package bongolive.apps.proshop.model;

import android.content.ContentResolver;
import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.DatabaseUtils;
import android.net.Uri;
import android.util.Log;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import bongolive.apps.proshop.controller.Constants;
import bongolive.apps.proshop.controller.UrlCon;

public class TransferItems
{
	public static final String TABLENAME = "transfer_items";
	public static final String ID = "_id";
    public static final String SYS_TRANS_ITEMID = "sys_transfer_item_id";
	public static final String TRANSIDL = "trans_id_local";
	public static final String QUANTITY = "product_qty" ;//current_stock";
	public static final String QTYVERIFIED = "received_qty" ;//current_stock";
	public static final String EXPIRY = "expiry";
	public static final String TIMESTAMP = "timestamp";
	public static final String UNITPRICE = "unit_purch_price" ;//unit tranfer price
	public static final String UPDATE = "last_update" ;
	public static final String PRODUCT = "prod_id" ;
	public static final String STATUS = "approval" ;//0 not approved,1 approved,2 rejected/not brought
	public static final String TRANSID = "sys_transfer_id" ;
    public static final String ACK = "ack" ;
	public static final Uri BASEURI = Uri.parse("content://"+ ContentProviderApi.AUTHORITY+"/"+TABLENAME) ;
	public static final String SPECIFIC_CONTENT_TYPE = "vnd.android.cursor.item/vnd.com.proshop.tranfer_items" ;
	public static final String GENERAL_CONTENT_TYPE = "vnd.android.cursor.dir/vnd.com.proshop.tranfer_items" ;
	private static final String TAG = TransferItems.class.getName();
	static UrlCon js ;
	public static String ARRAYNAME = "trans_items_list" ;

	private static JSONArray stockarray ;
    String date,product,qty,qtyverified,unit,status,transidl,timestamp;
    int ack;
    long id,transid,transitemid;


	public TransferItems() {
		/*
		rules

		the tranfer item can not be saved if it has an unknown warehouse or product
		a success save should return to the server the batch no and the tranfer id
		* */
	}


    public static void sendAckJson(Context context) throws JSONException {
        JSONObject jobs = new JSONObject();
        jobs.put("tag", Constants.TAGACK);
        jobs.put(Constants.ACKITEM, TABLENAME);
        jobs.put(Constants.IMEI, Constants.getIMEINO(context));
        js = new UrlCon();
        System.out.println(jobs.toString());
        js.getJson(Constants.SERVER, jobs) ;
    }
	 
	 private static boolean isStockPresentFromSystem(Context context, String custid)
	    {
            ContentResolver cr = context.getContentResolver();
            boolean ret = false;
            String where = TRANSID +" = "+custid;
            Cursor c = cr.query(BASEURI, null, where, null, null);
            try {
                if (c.moveToFirst()) {
                  ret = true ;
                }
            } finally {
                if (c != null) {
                    c.close();
                }
            }
            return ret;
	    }


    public static boolean updateWh(Context context,String wh)
    {
        ContentResolver cr = context.getContentResolver();
        ContentValues values = new ContentValues() ;
        values.put(TRANSIDL, wh);
        return cr.update(BASEURI, values, null, null) > 0;
    }



    public static int getPurchaseItems(Context context, String stockid) {
		ContentResolver cr = context.getContentResolver();
		String where = STATUS + " = " + stockid ;
    	Cursor c = cr.query(BASEURI, new String[]{QUANTITY}, where, null, null) ;
    	try {
    	int i= 0 ;
    	if(c.moveToFirst())
    	{
    		do {
    			i = c.getInt(c.getColumnIndexOrThrow(QUANTITY));
    			 return i ;
    		}while(c.moveToNext());
    	} 
    	}finally {
			 if(c != null) {
		            c.close();
		        }
		 }
		return 0 ;
	}
	/*
	 *  function to update in the stock table to those products that are existing
	 */
	public static boolean update(Context context, String[] string)
	{
        boolean ret = false;
        ContentResolver cr = context.getContentResolver();
        ContentValues values = new ContentValues();

        values.put(STATUS, string[0]);
        values.put(SYS_TRANS_ITEMID, string[1]);
        values.put(TRANSIDL, string[2]);
        values.put(QUANTITY, string[3]);
        values.put(EXPIRY, string[4]);
        values.put(UNITPRICE, Double.parseDouble(string[5]));
        values.put(PRODUCT, string[6]);
        values.put(TRANSID, string[7]);
        values.put(TIMESTAMP, string[8]);
        values.put(ACK, string[9]);
        values.put(QTYVERIFIED, string[10]);
        String where = TRANSID + " = " +string[7] + " AND "+ SYS_TRANS_ITEMID +" = "+string[1]+" AND "
                + TIMESTAMP + " = "+string[8];
        if (cr.update(BASEURI, values, where, null) > 0)
                ret = true;

		return ret;
	}


	/*
	 *  function to update in the stock table to those products that are existing
	 */
	public static boolean updateSysId(Context context, String[] string)
	{
		ContentResolver cr = context.getContentResolver();
		ContentValues values = new ContentValues() ;
        String where = PRODUCT +" = "+string[1];
            values.put(TRANSID, string[0]);
            values.put(ACK, 1);
		if (cr.update(BASEURI, values, where, null) > 0) {
            System.out.println(" updated "+string[1]);
                return true;
            }

		return false;
	}


	/*
	 * Save a new product in the stock table
	 */
	public static boolean insert(Context context, String[] string) {
        int productcount = getCount(context);
        boolean ret = false;
        ContentResolver cr = context.getContentResolver();
        ContentValues values = new ContentValues();

        values.put(STATUS, string[0]);
        values.put(SYS_TRANS_ITEMID, string[1]);
        values.put(TRANSIDL, string[2]);
        values.put(QUANTITY, string[3]);
        values.put(EXPIRY, string[4]);
        values.put(UNITPRICE, Double.parseDouble(string[5]));
        values.put(PRODUCT, string[6]);
        values.put(TRANSID, string[7]);
        values.put(TIMESTAMP, string[8]);
        values.put(ACK, string[9]);
        values.put(QTYVERIFIED, string[10]);
        values.put(STATUS, string[11]);
        cr.insert(BASEURI, values);
        Log.e("tranferitem","inserted");
        if (getCount(context) == productcount + 1)
            ret = true;

        return ret;
    }
	/*
	 * How many items are in the table ?
	 */
	public static int getCount(Context context){
		ContentResolver cr = context.getContentResolver() ;
		Cursor _c = cr.query(BASEURI, null, null, null, null);
		int count = _c.getCount() ;
		_c.close() ; 
		return count;
	}


    public static boolean updateItem(Context context, String[] vals)
    {
        ContentResolver cr = context.getContentResolver();
        ContentValues values = new ContentValues() ;
        values.put(ACK, 2);
        values.put(STATUS, vals[2]);
        values.put(QTYVERIFIED, vals[1]);
        String where = ID + " = "+ vals[0] ;
        return cr.update(BASEURI, values, where, null) > 0;
    }

    public static boolean updateAllForReturn(Context context, long ordrid)
    {
        ContentResolver cr = context.getContentResolver();
        ContentValues values = new ContentValues() ;
        values.put(ACK, 2);
        values.put(STATUS, 2);
        String where = TRANSIDL + " = "+ ordrid ;
        return cr.update(BASEURI, values, where, null) > 0;
    }
    public static boolean updateItemRollBack(Context context, long vals) {
        ArrayList<String> qtys = getallqty(context, vals);
        boolean ret = false;
        int status = 0;
        for (int i = 0; i < qtys.size(); i++) {
            ContentResolver cr = context.getContentResolver();
            ContentValues values = new ContentValues();
            values.put(ACK, "1");
            values.put(STATUS, "1");
            values.put(QTYVERIFIED, qtys.get(i));
            String where = TRANSIDL + " = " + vals;
            System.out.println("query " + values.toString());
            if( cr.update(BASEURI, values, where, null) > 0)
                status += 1;
        }
        if(status == qtys.size())
            ret = true;

        return ret;
    }
    public static boolean rollQtyBack(Context context, String[] vals) {

        boolean ret = false;

        ContentResolver cr = context.getContentResolver();
        ContentValues values = new ContentValues();
        values.put(ACK, "1");
        values.put(QTYVERIFIED, vals[1]);
        String where = ID+" = "+vals[0] ;
        System.out.println("query " + values.toString());
        if( cr.update(BASEURI, values, where, null) > 0)
            ret = true;

        return ret;
    }

    private static ArrayList<String> getallqty(Context context, long id){
        ArrayList<String> ret = new ArrayList<>();
        ContentResolver cr = context.getContentResolver();
        String where = TRANSIDL +" = "+id;
        Cursor c = cr.query(BASEURI,new String[]{QUANTITY},where,null,ID+" ASC");
        try{
            if(c != null && c.moveToFirst()){
                do{
                    String qty = c.getString(c.getColumnIndex(QUANTITY));
                    ret.add(qty);
                } while (c.moveToNext());
            }
        } finally {
            if(c != null){
                c.close();
            }
        }
        return ret;
    }


    public static boolean checkReject(Context context, long id, int action)
    {
        ContentResolver cr = context.getContentResolver();
        boolean ret = false;
        String where = null;
        if(action ==0)
            where = TRANSIDL +" = "+id+" AND "+ACK+" = "+"1";
        if(action ==1)
            where = TRANSIDL +" = "+id+" AND "+ACK+" = "+"1";

        Cursor c = cr.query(BASEURI, null, where, null, null);
        try {
            if (c.moveToFirst()) {
                ret = true ;
            }
        } finally {
            if (c != null) {
                c.close();
            }
        }
        return ret;
    }


    public static boolean isItemExist(Context context, String[] custid)
    {
        String where = custid[0] +" = "+ DatabaseUtils.sqlEscapeString(custid[1]);
        Cursor c = context.getContentResolver().query(BASEURI, null, where, null, null) ;
        boolean ret = false;
        try {
            if(c != null && c.moveToFirst())
            {
                ret = true;
            }
        } finally {
            if(c != null) {
                c.close();
            }
        }
        return ret ;
    }


    /*
     *  function to update in the stock table to those products that are existing
     */
    public static boolean updateSyncStatus(Context context, String[] string)
    {
        boolean ret = false;
        ContentResolver cr = context.getContentResolver();
        ContentValues values = new ContentValues() ;
        values.put(SYS_TRANS_ITEMID, string[2]);
        values.put(TRANSID, string[3]);
        values.put(ACK, string[4]);
        values.put(STATUS, string[5]);

        String where = string[0] + " = "+ DatabaseUtils.sqlEscapeString(string[1]) ;
        Log.e("UPDATING", "UPDATED THE TRANSFER ITEM TIMESTAMP " + string[1]);
        if(cr.update(BASEURI, values, where, null) > 0) {
            ret = true ;
        }
        return ret;
    }
    /*
     *  function to update in the stock table to those products that are existing
     */
    public static boolean approved(Context context, String[] string)
    {
        boolean ret = false;
        ContentResolver cr = context.getContentResolver();
        ContentValues values = new ContentValues() ;
        values.put(STATUS, string[2]);

        String where = string[0] + " = "+ DatabaseUtils.sqlEscapeString(string[1]) ;
        Log.e("UPDATING", "UPDATE THE APPROVED ITEM ");
        if(cr.update(BASEURI, values, where, null) > 0) {
            ret = true ;
        }
        return ret;
    }


    public static ArrayList<TransferItems> getItem(Context context, long tr){
        ArrayList<TransferItems> list = new ArrayList<>();
        ContentResolver cr = context.getContentResolver();
        String where =  ID+" = "+ tr;
        Cursor c = cr.query(BASEURI,null,where,null,ID+" ASC");
        try{
            if(c != null && c.getCount() > 0){
                c.moveToFirst();
                do {
                    String dt = c.getString(c.getColumnIndex(UPDATE));
                    String qty = c.getString(c.getColumnIndex(QUANTITY));
                    String qtyv = c.getString(c.getColumnIndex(QTYVERIFIED));
                    String prd = c.getString(c.getColumnIndex(PRODUCT));
                    String unt = c.getString(c.getColumnIndex(UNITPRICE));
                    long trid = c.getLong(c.getColumnIndex(TRANSID));
                    long tritid = c.getLong(c.getColumnIndex(SYS_TRANS_ITEMID));
                    String st = c.getString(c.getColumnIndex(STATUS));
                    String tridl = c.getString(c.getColumnIndex(TRANSIDL));
                    int a = c.getInt(c.getColumnIndex(ACK));
                    long i = c.getLong(c.getColumnIndex(ID));
                    String tmp = c.getString(c.getColumnIndex(TIMESTAMP));
                    list.add(new TransferItems(dt,prd,qty,unt,trid,tritid,st,tridl,qtyv,tmp,a, i));
                } while (c.moveToNext());
            }
        } finally {
            if(c != null){
                c.close();
            }
        }
        return list;
    }




    public static ArrayList<TransferItems> getItems(Context context, long tr){
        ArrayList<TransferItems> list = new ArrayList<>();
        ContentResolver cr = context.getContentResolver();
        String where =  TRANSIDL+" = "+ tr;
        Cursor c = cr.query(BASEURI,null,where,null,ID+" ASC");
        try{
            if(c != null && c.getCount() > 0){
                c.moveToFirst();
                do {
                    String dt = c.getString(c.getColumnIndex(UPDATE));
                    String qty = c.getString(c.getColumnIndex(QUANTITY));
                    String qtyv = c.getString(c.getColumnIndex(QTYVERIFIED));
                    String prd = c.getString(c.getColumnIndex(PRODUCT));
                    String unt = c.getString(c.getColumnIndex(UNITPRICE));
                    long trid = c.getLong(c.getColumnIndex(TRANSID));
                    long tritid = c.getLong(c.getColumnIndex(SYS_TRANS_ITEMID));
                    String st = c.getString(c.getColumnIndex(STATUS));
                    String tridl = c.getString(c.getColumnIndex(TRANSIDL));
                    String tmp = c.getString(c.getColumnIndex(TIMESTAMP));
                    long i = c.getLong(c.getColumnIndex(ID));
                    int a = c.getInt(c.getColumnIndex(ACK));
                    list.add(new TransferItems(dt,prd,qty,unt,trid,tritid,st,tridl,qtyv,tmp,a,i));
                } while (c.moveToNext());
            }
        } finally {
            if(c != null){
                c.close();
            }
        }
        return list;
    }

    public static ArrayList<TransferItems> getItemsApproved(Context context, long tr){
        ArrayList<TransferItems> list = new ArrayList<>();
        ContentResolver cr = context.getContentResolver();
        String where =  TRANSIDL+" = "+ tr + " AND "+STATUS + " = 1";
        Cursor c = cr.query(BASEURI,null,where,null,ID+" ASC");
        try{
            if(c != null && c.getCount() > 0){
                c.moveToFirst();
                do {
                    String dt = c.getString(c.getColumnIndex(UPDATE));
                    String qty = c.getString(c.getColumnIndex(QUANTITY));
                    String qtyv = c.getString(c.getColumnIndex(QTYVERIFIED));
                    String prd = c.getString(c.getColumnIndex(PRODUCT));
                    String unt = c.getString(c.getColumnIndex(UNITPRICE));
                    long trid = c.getLong(c.getColumnIndex(TRANSID));
                    long tritid = c.getLong(c.getColumnIndex(SYS_TRANS_ITEMID));
                    String st = c.getString(c.getColumnIndex(STATUS));
                    String tridl = c.getString(c.getColumnIndex(TRANSIDL));
                    String tmp = c.getString(c.getColumnIndex(TIMESTAMP));
                    long i = c.getLong(c.getColumnIndex(ID));
                    int a = c.getInt(c.getColumnIndex(ACK));
                    list.add(new TransferItems(dt,prd,qty,unt,trid,tritid,st,tridl,qtyv,tmp,a,i));
                } while (c.moveToNext());
            }
        } finally {
            if(c != null){
                c.close();
            }
        }
        return list;
    }


    public static JSONArray getJsonArray(Context context, String tr){
        ArrayList<TransferItems> list = new ArrayList<>();
        ContentResolver cr = context.getContentResolver();
        String where =  TRANSIDL+" = "+ tr;
        Cursor c = cr.query(BASEURI,null,where,null,ID+" ASC");
        JSONArray jsonArray = new JSONArray();

        try{
            if(c != null && c.getCount() > 0){
                c.moveToFirst();
                do {
                    JSONObject obj = new JSONObject();
                    obj.put(SYS_TRANS_ITEMID, c.getString(c.getColumnIndex(SYS_TRANS_ITEMID)));
                    obj.put(TRANSIDL, c.getString(c.getColumnIndex(TRANSIDL)));
                    obj.put(QUANTITY, c.getString(c.getColumnIndex(QUANTITY)));
                    obj.put(QTYVERIFIED, c.getString(c.getColumnIndex(QTYVERIFIED)));
                    obj.put(EXPIRY, c.getString(c.getColumnIndex(EXPIRY)));
                    obj.put(TIMESTAMP, c.getString(c.getColumnIndex(TIMESTAMP)));
                    obj.put(UNITPRICE, c.getString(c.getColumnIndex(UNITPRICE)));
                    obj.put(PRODUCT, c.getString(c.getColumnIndex(PRODUCT)));
                    obj.put(STATUS, c.getString(c.getColumnIndex(STATUS)));
                    obj.put(TRANSID, c.getString(c.getColumnIndex(TRANSID)));
                    jsonArray.put(obj);
                } while (c.moveToNext());
            }
        } catch (JSONException e) {
            e.printStackTrace();
        } finally {
            if(c != null){
                c.close();
            }
        }
        return jsonArray;
    }


    public long getId() {
        return id;
    }

    public String getTransidl() {
        return transidl;
    }

    public String getStatus() {
        return status;
    }

    public long getTransitemid() {
        return transitemid;
    }

    public long getTransid() {
        return transid;
    }

    public String getUnit() {
        return unit;
    }

    public String getQty() {
        return qty;
    }

    public String getProduct() {
        return product;
    }

    public String getTimestamp() {
        return timestamp;
    }

    public int getAck() {
        return ack;
    }

    public String getDate() {
        return date;
    }

    public String getQtyverified() {
        return qtyverified;
    }

    public TransferItems(String date, String product, String qty, String unit, long transid,
                         long transitemid, String status, String transidl, String qtyverified, String timestamp, int ack, long id) {
        this.date = date;
        this.product = product;
        this.qty = qty;
        this.unit = unit;
        this.transid = transid;
        this.transitemid = transitemid;
        this.status = status;
        this.transidl = transidl;
        this.qtyverified = qtyverified;
        this.timestamp = timestamp;
        this.ack = ack;
        this.id = id;
    }
}