/*
 * The source codes in this project are owned by Bongolive Enterprises. Copying or any kind of usage without Bongolive's approval is against the law.
 * Copyright (c) 2015.
 *
 */

package bongolive.apps.proshop.model;

import android.content.ContentResolver;
import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.DatabaseUtils;
import android.database.sqlite.SQLiteDatabase;
import android.net.Uri;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import bongolive.apps.proshop.R;
import bongolive.apps.proshop.controller.Validating;
import bongolive.apps.proshop.controller.Constants;
import bongolive.apps.proshop.controller.UrlCon;

public class Warehouse {
    public static final String TABLENAME = "warehouse";
    public static final Uri BASEURI = Uri.parse("content://" + ContentProviderApi.AUTHORITY + "/" + TABLENAME);
    public static final String ID = "_id";
    public static final String WH_CODE = "wh_code";
    public static final String WH_NAME = "wh_name";
    public static final String WH_STATUS = "wh_status";
    public static final String WH_ID = "wh_id";
    public static final String WH_ADDRESS = "wh_address";
    public static final String CREATED_ON = "created_on";
    public static final String ACK = "ack";//0 new //1 synced //2 update //3 delete
    public static final String SPECIFIC_CONTENT_TYPE = "vnd.android.cursor.item/vnd.com.proshop.wh";
    public static final String GENERAL_CONTENT_TYPE = "vnd.android.cursor.dir/vnd.com.proshop.wh";
    private static UrlCon js ;
    public static final String GETARRAYNAME = "wh_list";
    public static final String SENDARRAYNAME = "wh";
    public static final String SENDARRAYNAME2 = "wh_sent_list";
    private static final String TAG = Warehouse.class.getName();

    int id, ack , sysid;
    String code,name,address;

    public Warehouse() {

    }

    public static boolean columnExist(SQLiteDatabase context,String column){
        boolean ret= false;
        String sql = " SELECT * FROM "+TABLENAME  +" LIMIT 1";

        Cursor c = context.rawQuery(sql, null);
        try {
            int indext = c.getColumnIndex(column);
            System.out.println(" index is " +indext);
            if(indext != -1)
                ret = true;
        }finally {
            if(c != null) {
                c.close();
            }
        }
        return ret;
    }

    public static int getCount(Context context){
        ContentResolver cr = context.getContentResolver();
        Cursor c = cr.query(BASEURI, null, null, null, null);
        int count = 0;
        try {
            count = c.getCount();
        } finally {
            if (c != null) {
                c.close();
            }
        }
        return count;
    }

    public static boolean isWhPresent(Context context, String whid) {
        ContentResolver cr = context.getContentResolver();

        String where = null;
        if(getCount(context) ==1) {
            if (isDemo(context))
                where = WH_STATUS + " = " + DatabaseUtils.sqlEscapeString(Constants.WH_DEMO);
            else
                where = WH_ID + " = " + whid;
        }
        else
            where = WH_ID +" = "+whid;

        boolean bool = false;
        Cursor c = cr.query(BASEURI, new String[]{WH_ID}, where, null, null);
        try {
            if (c.moveToFirst()) {
                        bool = true;
            }
        } finally {
            if (c != null) {
                c.close();
            }
        }
        return bool;
    }

    public static boolean isDemo(Context context) {
        ContentResolver cr = context.getContentResolver();
        boolean bool = false;
        String where = WH_STATUS +" = "+ DatabaseUtils.sqlEscapeString(Constants.WH_DEMO);
        Cursor c = cr.query(BASEURI, new String[]{WH_STATUS}, where, null, null);
        try {
            if (c.moveToFirst()) {
                bool = true;
            }
        } finally {
            if (c != null) {
                c.close();
            }
        }
        return bool;
    }

    public static ArrayList<String> getAll(Context context) {
        ContentResolver cr = context.getContentResolver();
        Cursor c = cr.query(BASEURI, new String[]{ WH_NAME}, null, null, null) ;
        ArrayList<String> custdetails = new ArrayList<>();
        custdetails.add(context.getString(R.string.strselectwarehouse));
        try {
            if(c.moveToFirst()) {
                do {
                    String n = c.getString(c.getColumnIndexOrThrow(WH_NAME));
                    custdetails.add(n);
                } while (c.moveToNext());
            }
            return custdetails;
        } finally {
            if(c != null) {
                c.close();
            }
        }

    }


    public static ArrayList<String> getAllIds(Context context) {
        ContentResolver cr = context.getContentResolver();
        Cursor c = cr.query(BASEURI, new String[]{ WH_ID}, null, null, ID +" ASC") ;
        ArrayList<String> custdetails = new ArrayList<>();
        custdetails.add(context.getString(R.string.strselectwarehouse));
        try {
            if(c.moveToFirst()) {
                do {
                    String n = c.getString(c.getColumnIndexOrThrow(WH_ID));
                    custdetails.add(n);
                } while (c.moveToNext());
            }
            return custdetails;
        } finally {
            if(c != null) {
                c.close();
            }
        }

    }


    public static String getDefault(Context context) {
        String bz = "";
        ContentResolver cr = context.getContentResolver();
        String where = ID + " = 1";
        Cursor c = cr.query(BASEURI, null, where, null, null);
        try {
            if (c.moveToFirst()) {
                bz = c.getString(c.getColumnIndexOrThrow(WH_NAME));
            }
        } finally {
            if (c != null) {
                c.close();
            }
        }
        return bz;
    }

    public static String getDefaultId(Context context) {
        String bz = "";
        ContentResolver cr = context.getContentResolver();
        Cursor c = cr.query(BASEURI, null, null, null, null);
        try {
            if (c.moveToFirst()) {
                bz = c.getString(c.getColumnIndexOrThrow(WH_ID));
            }
        } finally {
            if (c != null) {
                c.close();
            }
        }
        return bz;
    }

    public static String[] getWhCodeAndName(Context context, String custid) {
        String[] bz = null;
        ContentResolver cr = context.getContentResolver();
        String where = WH_ID + " = " + custid;
        Cursor c = cr.query(BASEURI, null, where, null, null);
        try {
            if (c.moveToFirst()) {
                String cd = c.getString(c.getColumnIndexOrThrow(WH_CODE));
                String nm = c.getString(c.getColumnIndexOrThrow(WH_NAME));
                bz = new String[]{cd,nm};
            }
        } finally {
            if (c != null) {
                c.close();
            }
        }
        return bz;
    }

    public static int storeWh(Context context, String[] string) {
        int whcount = getCount(context);
        ContentResolver cr = context.getContentResolver();
        ContentValues values = new ContentValues();
        values.put(WH_ID, string[0]);
        values.put(WH_CODE, string[1]);
        values.put(WH_NAME, string[2]);
        values.put(WH_ADDRESS, string[3]);
        values.put(WH_STATUS, string[4]);
        cr.insert(BASEURI, values);
        if (getCount(context) == whcount + 1) {
            return 1;
        }
        return whcount;
    }



    public static void sendAckJson(Context context) throws JSONException {
        JSONObject jobs = new JSONObject();
        jobs.put("tag", Constants.TAGACK);
        jobs.put(Constants.ACKITEM, TABLENAME);
        jobs.put(Constants.IMEI, Constants.getIMEINO(context));
        js = new UrlCon();
        System.out.println(jobs.toString());
        js.getJson(Constants.SERVER, jobs) ;
    }

    public static int updateWh(Context context, String[] string) {
        ContentResolver cr = context.getContentResolver();
        ContentValues values = new ContentValues();
        values.put(WH_ID, string[0]);
        values.put(WH_CODE, string[1]);
        values.put(WH_NAME, string[2]);
        values.put(WH_ADDRESS, string[3]);
        values.put(WH_STATUS, string[4]);
        String where = null;

        if(getCount(context) == 1) {
            boolean isdemo = isDemo(context);
            if(isdemo)
                where = WH_STATUS +" = "+DatabaseUtils.sqlEscapeString(Constants.WH_DEMO);
            else
                where = WH_ID + " = "+string[0];
        } else {
            where = WH_ID + " = "+string[0];
        }
        int upd = cr.update(BASEURI,values,where,null);
        if (upd > 0) {
            System.out.println("WH updated ");
            return 1;
        }
        return 0;
    }

    public static JSONObject getWarehouseJson(Context context) throws JSONException {
        JSONObject jobs = new JSONObject();
        jobs.put("tag", Constants.TAGGETWH);
        jobs.put(Constants.IMEI, Constants.getIMEINO(context));
        js = new UrlCon();
        String job = js.getJson(Constants.SERVER, jobs) ;
        JSONObject json;
        try {
            json = new JSONObject(job);
            if (json.length() != 0) {
                return json;
            }
        } catch (JSONException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        return null;
    }

    public static void getWhfromServer(Context context) throws JSONException {
        JSONObject json = getWarehouseJson(context);
        if(json != null)
        try {
            if (json.has(Constants.SUCCESS)) {
                String res = json.getString(Constants.SUCCESS);
                if (Integer.parseInt(res) == 1) {

                    if (json.has(GETARRAYNAME)) {
                        JSONArray custarray = json.getJSONArray(GETARRAYNAME);
                        int success = 0;
                        for (int i = 0; i < custarray.length(); i++) {
                            JSONObject js = custarray.getJSONObject(i);
                            String whid = js.getString(WH_ID);
                            String cd = js.getString(WH_CODE);
                            String nm = js.getString(WH_NAME);
                            String ad = js.getString(WH_ADDRESS);
                            String status = Constants.WH_ACTIVE;
                            String[] vals = {whid,cd,nm,ad,status};
                            String[] chk = {whid,cd,nm};
                            if(Validating.areSet(chk)) {
                                if(!isWhPresent(context, whid)) {
                                    if(storeWh(context,vals) == 1)
                                        success += 1;
                                } else {
                                    if(updateWh(context, vals) == 1)
                                    success +=1;
                                }
                            }
                        }
                        if(success == custarray.length() && custarray.length()>0)
                            sendAckJson(context);
                    }
                }
            }
        } catch (JSONException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }

    public Warehouse(int wid,int wack,String wcode,String wname){
        id = wid;
        ack = wack;
        code = wcode;
        name = wname;
    }

    public int getId() {
        return id;
    }

    public int getAck() {
        return ack;
    }

    public int getSysid() {
        return sysid;
    }

    public String getCode() {
        return code;
    }

    public String getName() {
        return name;
    }

    public String getAddress() {
        return address;
    }
}
