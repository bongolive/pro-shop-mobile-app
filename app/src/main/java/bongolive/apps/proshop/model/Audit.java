/*
 * The source codes in this project are owned by Bongolive Enterprises. Copying or any kind of usage without Bongolive's approval is against the law.
 * Copyright (c) 2015.
 *
 */

package bongolive.apps.proshop.model;

import android.content.ContentResolver;
import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.net.Uri;
import android.text.TextUtils;

import org.apache.commons.lang3.StringUtils;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Arrays;

import bongolive.apps.proshop.controller.Constants;
import bongolive.apps.proshop.controller.ItemList;
import bongolive.apps.proshop.controller.UrlCon;
import bongolive.apps.proshop.controller.Validating;

public class Audit {
    public static final String TABLENAME = "audit";
    public static final String ID = "_id";
    public static final String PRODUCTS = "products" ;
    public static final String AUDITID = "audit_id" ;
    public static final String AUDITOPTIONID = "audit_option_id" ;
    public static final String CAPTION = "option_caption" ;
    public static final String OPTIONTYPE = "option_type" ;//1:select;2:input;3:image
    public static final String INPUTTYPE = "input_type" ;//1:varchar; 2:integer; 3:double/float; 4:date
    public static final String NAME = "audit_name" ;
    public static final String DESCRIPTION = "description" ;
    public static final String CREATED = "created_on" ;
    public static final String UPDATED = "updated_on" ;
    public static final String TIMESTAMP = "timestamp" ;
    public static final String MIN = "min" ;
    public static final String MAX = "max" ;
    public static final String COMPULS = "compulsory" ;
    public static final String GPS = "enable_gps" ;
    public static final String ACK = "ack" ;
    public static final String CUSTOMERS = "customers" ;
    /*
    *
    * */
    public static final Uri BASEURI = Uri.parse("content://"+ ContentProviderApi.AUTHORITY+"/"+TABLENAME) ;
    public static final String SPECIFIC_CONTENT_TYPE = "vnd.android.cursor.item/vnd.com.proshop.audit" ;
    public static final String GENERAL_CONTENT_TYPE = "vnd.android.cursor.dir/vnd.com.proshop.audit" ;
    public static String ARRAYNAME= "audit_list" ;
    static UrlCon js;

    String name,date,desc;
    long id, auid;

    String optionid,caption;
    int optiontype,gps,inputtype;
    public Audit(){

    }

    public static int getCount(Context context){
        ContentResolver cr = context.getContentResolver() ;
        Cursor _c = cr.query(BASEURI, null, null, null, null);
        int count = _c.getCount() ;
        _c.close() ;
        return count;
    }
    public static boolean insert(Context context, String[] items){
        int itemcount = getCount(context);

        ContentResolver cr = context.getContentResolver() ;
        ContentValues cv = new ContentValues() ;
        cv.put(PRODUCTS, items[0]);
        cv.put(CAPTION, items[1]);
        cv.put(OPTIONTYPE, items[2]);
        cv.put(INPUTTYPE, items[3]);
        cv.put(AUDITOPTIONID, items[4]);
        cv.put(AUDITID, items[5]);
        cv.put(NAME, items[6]);
        cv.put(DESCRIPTION, items[7]);
        cv.put(CUSTOMERS, items[8]);
        cv.put(GPS, items[9]);
        cr.insert(BASEURI, cv);
        if(getCount(context) == itemcount + 1)
        {
            System.out.println(" updated "+items[0]);
            return true ;
        } else {
            return false ;
        }
    }

    public static boolean update(Context context, String[] items){
        ContentResolver cr = context.getContentResolver() ;
        ContentValues cv = new ContentValues() ;

        cv.put(PRODUCTS, items[0]);
        cv.put(CAPTION, items[1]);
        cv.put(OPTIONTYPE, items[2]);
        cv.put(INPUTTYPE, items[3]);
        cv.put(AUDITOPTIONID, items[4]);
        cv.put(AUDITID, items[5]);
        cv.put(NAME, items[6]);
        cv.put(DESCRIPTION, items[7]);
        cv.put(CUSTOMERS, items[8]);
        cv.put(GPS, items[9]);
        String where = AUDITID +" = "+ items[5] + " AND "+AUDITOPTIONID + " = " +
                items[4];
        int up = cr.update(BASEURI, cv,where,null);
        if(up > 0)
        {
            System.out.println(" updated "+items[0]);
            return true ;
        } else {
            return false ;
        }
    }

    public static void sendAckJson(Context context) throws JSONException {
        JSONObject jobs = new JSONObject();
        jobs.put("tag", Constants.TAGACK);
        jobs.put(Constants.ACKITEM, TABLENAME);
        jobs.put(Constants.IMEI, Constants.getIMEINO(context));
        js = new UrlCon();
        System.out.println(jobs.toString());
        js.getJson(Constants.SERVER, jobs) ;
    }


    public static boolean columnExist(SQLiteDatabase context,String column){
        boolean ret= false;
        String sql = " SELECT * FROM "+TABLENAME  +" LIMIT 1";

        Cursor c = context.rawQuery(sql, null);
        try {
            int indext = c.getColumnIndex(column);
            System.out.println(" index is " +indext);
            if(indext != -1)
                ret = true;
        }finally {
            if(c != null) {
                c.close();
            }
        }
        return ret;
    }

    /*
	 * Fetching stock for a new product giving out imei of the device
	 */
    public static JSONObject getAuditJson(Context context) throws JSONException
    {
        JSONObject jobs = new JSONObject();
        jobs.put("tag", Constants.TAGSGETAUDIT);
        jobs.put(Constants.IMEI, Constants.getIMEINO(context));
         js = new UrlCon();
        String job =  js.getJson(Constants.SERVER, jobs);
        JSONObject json;
        try {
            json = new JSONObject(job);
            if(json.length() != 0)
            {
                return json ;
            }
        } catch (JSONException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        return null ;
    }

    public static void processInformation(Context context) throws JSONException{

        JSONObject jsonin = getAuditJson(context);
        if(jsonin != null)
        try {
            if(jsonin.has(Constants.SUCCESS)){
                String res = jsonin.getString(Constants.SUCCESS) ;
                if(Integer.parseInt(res) == 1)
                {
                    if(jsonin.has(ARRAYNAME)) {
                        JSONArray jsonArray = jsonin.getJSONArray(ARRAYNAME);
                        int success = 0;
                        for (int i = 0; i < jsonArray.length(); i++) {
                            JSONObject job = jsonArray.getJSONObject(i);
                            String prdn = "job.getString(PRODUCTS);";
                            String cust = "job.getString(CUSTOMERS);";
                            String cpn = job.getString(CAPTION);
                            String optype = job.getString(OPTIONTYPE);
                            String intype = job.getString(INPUTTYPE);
                            String auctid = job.getString(AUDITOPTIONID);
                            String aud = job.getString(AUDITID);
                            String nm = job.getString(NAME);
                            String desc = job.getString(DESCRIPTION);
                            String gps = job.getString(GPS);

                            String[] chek = {prdn, cpn,optype,intype,auctid,aud,nm,cust};

                            if (Validating.areSet(chek)) {

                                String[] vals = {prdn, cpn,optype,intype,auctid,aud,nm,desc,cust,gps};
                                if(!isAuditEisting(context, vals)) {
                                    if (insert(context, vals)) {
                                        success += 1;
                                    }
                                } else {
                                    if(update(context,vals))
                                        success += 1;
                                }
                            }
                        }
                        if(success == jsonArray.length() && jsonArray.length() >0) {
                            sendAckJson(context);
                        } else {
                            System.out.println("send json error");
                            sendErrorJson(context, new String[]{Products.TABLENAME,"16"});
                        }
                    }
                }
            }
        } catch (NumberFormatException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } catch (JSONException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }

    public static void sendErrorJson(Context context,String[] cd) throws JSONException {
        JSONObject jobs = new JSONObject();
        jobs.put("tag", Constants.TAGACK);
        jobs.put(Constants.ACKITEM, cd[0]);
        jobs.put(Constants.ERRORCODE, cd[1]);
        jobs.put(Constants.IMEI, Constants.getIMEINO(context));
        js = new UrlCon();
        System.out.println(jobs.toString());
        js.getJson(Constants.SERVER, jobs) ;
    }


    private static boolean isAuditEisting(Context context, String[] items)
    {
        ContentResolver cr = context.getContentResolver();
        boolean ret = false;
        String where = AUDITID +" = "+ items[5] +  " AND "+AUDITOPTIONID +
                " = "+items[4];
        Cursor c = cr.query(BASEURI, null, where, null, null);
        try {
            if (c.moveToFirst()) {
                ret = true ;
            }
        } finally {
            if (c != null) {
                c.close();
            }
        }
        return ret;
    }

    public static boolean isTimestampExist(Context context, String[] items)
    {
        ContentResolver cr = context.getContentResolver();
        boolean ret = false;
        String where = AUDITID +" = "+ items[1]  ;
        Cursor c = cr.query(BASEURI, new String[]{TIMESTAMP}, where, null, null);
        try {
            if (c.moveToFirst()) {
                String tt = c.getString(c.getColumnIndex(TIMESTAMP));
                if(Validating.areSet(tt))
                {
                    ret = true;
                }
            }
        } finally {
            if (c != null) {
                c.close();
            }
        }
        return ret;
    }

    public static ArrayList<Audit> getAudits(Context context){
        ArrayList<Audit> list = new ArrayList<>();
        String q = "SELECT * FROM "+TABLENAME+" GROUP BY "+AUDITID;
        Cursor c = DatabaseHandler.getInstance(context). getReadableDatabase().rawQuery(q, null);
        try{
            if(c.moveToFirst()){
                do {
                    long aud = c.getLong(c.getColumnIndex(AUDITID));
                    long id = c.getLong(c.getColumnIndex(ID));
                    String title = c.getString(c.getColumnIndex(NAME));
                    String dt = c.getString(c.getColumnIndex(CREATED));
                    String description = c.getString(c.getColumnIndex(DESCRIPTION));
                    list.add(new Audit(title,dt,description,id,aud));
                } while (c.moveToNext());
            }
        } finally {
            if(c != null){
                c.close();
            }
        }
        return list;
    }

    public static ArrayList<Audit> getAuditList(Context context, long auid){
        ArrayList<Audit> list = new ArrayList<>();
        String where = AUDITID + " = "+ auid;
        ContentResolver cr = context.getContentResolver();
        Cursor c = cr.query(BASEURI,null,where,null,null);
        try{
            if(c != null && c.moveToFirst()){
                do {
                    long aud = c.getLong(c.getColumnIndex(AUDITID));
                    long id = c.getLong(c.getColumnIndex(ID));
                    String title = c.getString(c.getColumnIndex(CAPTION));
                    String optid = c.getString(c.getColumnIndex(AUDITOPTIONID));
                    int inputtype = c.getInt(c.getColumnIndex(INPUTTYPE));
                    int optionty = c.getInt(c.getColumnIndex(OPTIONTYPE));
                    int gps = c.getInt(c.getColumnIndex(GPS));

                    list.add(new Audit(id,auid,optid,title,optionty,gps,inputtype));
                } while (c.moveToNext());
            }
        } finally {
            if(c != null){
                c.close();
            }
        }
        return list;
    }


    public static ArrayList<ItemList> getAuditCustmerList(Context context){
        ArrayList<ItemList> list = new ArrayList<>();
        String q = "SELECT * FROM "+TABLENAME+" GROUP BY "+AUDITID;
        Cursor c = DatabaseHandler.getInstance(context). getReadableDatabase().rawQuery(q, null);
        ArrayList<String> custlst = new ArrayList<>();
        try{
            if(c.moveToFirst()){
                do {
                    String cust = c.getString(c.getColumnIndex(CUSTOMERS));
                    custlst.add(cust);
                } while (c.moveToNext());
                String ct = StringUtils.join(custlst, ",");
                String[] lst = TextUtils.split(ct, ",");
                ArrayList<String> xt = new ArrayList<>(Arrays.asList(lst));
                for(int i = 0; i < xt.size(); i++){
                    String id = xt.get(i);
                    String bz = Customers.getCustomerBusinessName(context, id);
                    if(Validating.areSet(bz))
                        list.add(new ItemList(Long.parseLong(id),bz));
                }
            }
        } finally {
            if(c != null){
                c.close();
            }
        }
        return list;
    }


    public static boolean updateSync(Context context, String orderid, int flag)
    {
        ContentResolver cr = context.getContentResolver();
        ContentValues values = new ContentValues() ;
        values.put(ACK, flag);
        String where = AUDITID + " = "+ orderid ;
        return cr.update(BASEURI, values, where, null) > 0;
    }


    public Audit(String name, String date, String desc, long id, long auid) {
        this.name = name;
        this.date = date;
        this.desc = desc;
        this.id = id;
        this.auid = auid;
    }

    public Audit(long id, long auid, String optionid, String caption, int optiontype, int gps, int inputtype) {
        this.id = id;
        this.auid = auid;
        this.optionid = optionid;
        this.caption = caption;
        this.optiontype = optiontype;
        this.gps = gps;
        this.inputtype = inputtype;
    }

    public String getName() {
        return name;
    }

    public String getDate() {
        return date;
    }

    public String getDesc() {
        return desc;
    }

    public long getId() {
        return id;
    }

    public long getAuid() {
        return auid;
    }

    public int getInputtype() {
        return inputtype;
    }

    public int getGps() {
        return gps;
    }

    public int getOptiontype() {
        return optiontype;
    }

    public String getCaption() {
        return caption;
    }

    public String getOptionid() {
        return optionid;
    }
}
