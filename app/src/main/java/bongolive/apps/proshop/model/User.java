/*
 * The source codes in this project are owned by Bongolive Enterprises. Copying or any kind of usage without Bongolive's approval is against the law.
 * Copyright (c) 2015.
 *
 */

/**
 * 
 */
package bongolive.apps.proshop.model;

import android.content.ContentResolver;
import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.DatabaseUtils;
import android.net.Uri;

import bongolive.apps.proshop.R;
import bongolive.apps.proshop.controller.Validating;
import bongolive.apps.proshop.controller.Constants;
import bongolive.apps.proshop.controller.UrlCon;

/**
 *    @author nasznjoka
 *
 *    
 *    Nov 15, 2014
 */
public class User {
 public static final String TABLENAME = "user" ;
    
 public static final Uri BASEURI = Uri.parse("content://" + ContentProviderApi.AUTHORITY + "/" + TABLENAME) ;
 public static final String SPECIFIC_CONTENT_TYPE = "vnd.android.cursor.item/vnd.com.proshop.user" ;
 public static final String GENERAL_CONTENT_TYPE = "vnd.android.cursor.dir/vnd.com.proshop.user" ;

 public static final String ID = "uid";
 public static final String USERNAME = "username";
 public static final String PASSWORD = "password";
 public static final String ROLE = "role";
 public static final String STATUS = "status";
 public static final String CREATED = "created";
 public static final String UPDATED = "updated";
 public static final String PIN = "pin";
 public static final String ARRAYNAME = "user_data_array";

private static final String TAG = User.class.getName();
	String username,password,role,status,pin,update;
    long id;

 public static UrlCon js ;
 
 public static int login(Context context,String[] v)
 { 
	 ContentResolver cr = context.getContentResolver();
	 String where = USERNAME+" = "+DatabaseUtils.sqlEscapeString(v[0]) +" AND "+
			 PASSWORD +" = "+DatabaseUtils.sqlEscapeString(v[1]);
     System.out.println("where is "+where);
	 Cursor c = cr.query(BASEURI, new String[]{ID}, where, null, null);
	 try {
	 if(c.moveToFirst()) {
         int id = c.getInt(c.getColumnIndexOrThrow(ID));
         String wh = ID +" = "+id;
         ContentValues cv = new ContentValues();
         cv.put(UPDATED, Constants.getDate());
         cr.update(BASEURI,cv,wh,null);
         return id;
     }
      }finally {
	    if(c != null) {
            c.close();
         }
      }  
	 return -1 ;
 }
 
 public static int getCount(Context context){
	    ContentResolver cr = context.getContentResolver() ;
		Cursor c = cr.query(BASEURI, null, null, null, null);
		int count = 0 ;
		try {
			if(c != null)
				count = c.getCount() ;
		} finally {
 		if(c != null) {
	            c.close();
	        }
 	}  
		return count; 
	}
	public static int changePass(Context context, String[] args){
		ContentResolver cr = context.getContentResolver();
		ContentValues contentValues = new ContentValues(1);
		contentValues.put(PASSWORD, args[1]);
		String where = ID + " = " + args[0];
		int psw = cr.update(BASEURI, contentValues, where, null);
		if(psw == 1)
			return 1;
		return 0;
	}
 public static int getUserlRole(Context context, int id){
	    ContentResolver cr = context.getContentResolver() ;
	    String where = ID + " = " + id ;
		Cursor c = cr.query(BASEURI, null, where, null, null);
		try {
			if(c.moveToFirst()) { 
		 return c.getInt(c.getColumnIndexOrThrow(ROLE));
			}
		} finally {
		if(c != null) {
	            c.close();
	        }
	}
		return 9;   
	}


	public static boolean userExist(Context context, String id){
	    ContentResolver cr = context.getContentResolver() ;
	    String where = USERNAME + " = " + DatabaseUtils.sqlEscapeString(id);
		Cursor c = cr.query(BASEURI, null, where, null, null);
		try {
			if(c.moveToFirst()) {
		 return true;
			}
		} finally {
		if(c != null) {
	            c.close();
	        }
	}
		return false;
	}


    public static boolean delete(Context context, String string) {
        ContentResolver cr = context.getContentResolver();
        String where = ID + " = " + string;
        if (cr.delete(BASEURI, where, null) > 0) {
            return true;
        }
        return false;
    }


    public static boolean insert(Context context, String[] v)
 {
	 int value = getCount(context);
	 ContentValues cv = new ContentValues();
	 cv.put(USERNAME, v[0]);
	 cv.put(PASSWORD, v[1]);
	 cv.put(ROLE, v[2]);
	 cv.put(STATUS, 1);
	 cv.put(CREATED, Constants.getDate());
	 ContentResolver cr = context.getContentResolver() ;
	 if(!userExist(context,v[0]))
	 cr.insert(BASEURI, cv);
	 if(getCount(context) == value + 1)
         return true ;

	 return false ;
 }


 public static boolean update(Context context, String[] v)
 {
	 ContentValues cv = new ContentValues();
	 cv.put(USERNAME, v[0]);
	 cv.put(PASSWORD, v[1]);
	 cv.put(ROLE, v[2]);
	 cv.put(CREATED, Constants.getDate());
	 ContentResolver cr = context.getContentResolver() ;
     String where =  ID + " = " + v[3] ;
     if( cr.update(BASEURI, cv, where, null) > 0) {
         return true ;
     }
	 return false ;
 }



 public static boolean getAdminCount(Context context)
 {
	 String where = ROLE + " = 1 " ;
 	Cursor c = context.getContentResolver().query(BASEURI, null, where,null,null) ;
 	 try {
 	if(c.getCount() > 0)
 	{
 				return true ;
 	}
	 } finally {
		 if(c != null) {
	            c.close();
	        }
	 }
 	return false ;
 }


	public static String[] getUser(Context context, long id) {
		ContentResolver cr = context.getContentResolver();
		String where = ID + " = " + id;

		Cursor c = cr.query(BASEURI, null, where, null, null);
		String[] custdetails;
		try {
			if (c.moveToFirst()) {
				String n = c.getString(c.getColumnIndexOrThrow(USERNAME));
				String b = c.getString(c.getColumnIndexOrThrow(UPDATED));
				String e = c.getString(c.getColumnIndexOrThrow(ROLE));
				if(!Validating.areSet(b))
					b = context.getString(R.string.strnever);
				custdetails = new String[]{n, b, e};
				return custdetails;
			}
		} finally {
			if (c != null) {
				c.close();
			}
		}
		return null;
	}


	public static String getUser(Context context, String id) {
		ContentResolver cr = context.getContentResolver();
		String where = ID + " = " + id;

		Cursor c = cr.query(BASEURI, null, where, null, null);
		try {
			if (c.moveToFirst()) {
				String n = c.getString(c.getColumnIndexOrThrow(USERNAME));
				return n;
			}
		} finally {
			if (c != null) {
				c.close();
			}
		}
		return null;
	}




	public String getUsername() {
        return username;
    }

    public String getPassword() {
        return password;
    }

    public String getRole() {
        return role;
    }

    public String getStatus() {
        return status;
    }

    public long getId() {
        return id;
    }

    public String getPin() {
        return pin;
    }

    public String getUpdated() {
        return pin;
    }

    public User(String username, String password, String role, String status, String pin, String updt,long id) {
        this.username = username;
        this.password = password;
        this.role = role;
        this.status = status;
        this.pin = pin;
        this.id = id;
        this.update = updt;
    }
}
