/*
 * The source codes in this project are owned by Bongolive Enterprises. Copying or any kind of usage without Bongolive's approval is against the law.
 * Copyright (c) 2015.
 *
 */

package bongolive.apps.proshop.model;

import android.content.ContentResolver;
import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.DatabaseUtils;
import android.net.Uri;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.ArrayList;

import bongolive.apps.proshop.controller.UrlCon;

public class Pending_Items {
    public static final String TABLENAME = "pending_sale_items";
    public static final String ID = "_id";
    public static final String PRODUCTNO = "prod_system_id" ;
    public static final String CUSTOMERID = "cust_id" ;
    public static final String LOCALPRODUCTNO = "local_system_id" ;
    public static final String PRODUCTQTY = "prod_order_qty" ;
    public static final String PRODUCTQTYDEL = "prod_order_qty_delivered" ;
    public static final String SALEPRICE = "unit_price" ;
    public static final String NETSALEPRICE = "net_unit_price" ;
    public static final String TAX_AMOUNT = "tax_amount" ;
    public static final String ITEM_TAX = "item_tax" ;
    public static final String SUBTOTAL = "subtotal" ;
    public static final String WH = "wh" ;
    public static final String DISCOUNTPRICE = "discounted_price" ;
    public static final String TAXRATE = "tax_rate";
    public static final String ORDERID = "order_id";
    public static final String SALEITEMID = "sale_item_id";
    public static final String SALE_REFERENCE = "sale_refid";
    public static final String DELSTATUS = "delivery_status";
    public static final String ACK = "ack" ;
    public static final String TIMESTAMP = "timestamp" ;
    public static final String SALE_TYPE = "sale_type" ;
    public static final Uri BASEURI = Uri.parse("content://"+ ContentProviderApi.AUTHORITY+"/"+TABLENAME) ;
    public static final String SPECIFIC_CONTENT_TYPE = "vnd.android.cursor.item/vnd.com.proshop.pending_items" ;
    public static final String GENERAL_CONTENT_TYPE = "vnd.android.cursor.dir/vnd.com.proshop.pending_items" ;

    public static String ARRAYNAME= "sales_itemlist" ;
    static UrlCon js;
    long id,orderid,prodloc;
    String prod,cust,wh,saletype;
    double netprice,price,taxmt,sbtotal,taxrt,discount,taxitem;
    int qty,qtydel,pos;

    public Pending_Items(){

    }

    public Pending_Items(long prdlocal, String whs, long i, int qt, double ntprice, double slprice, double taxamnt, double
            subtotal, double taxrate, double prdiscount, String pro,String saletype, int p){
        prodloc = prdlocal;
        wh = whs;
        id = i;
        netprice = ntprice;
        price = slprice;
        taxmt = taxamnt;
        sbtotal = subtotal;
        taxrt = taxrate;
        discount = prdiscount;
        qty = qt;
        prod = pro;
        this.saletype = saletype;
        this.pos = p;
    }

    public static int getCount(Context context){
        ContentResolver cr = context.getContentResolver() ;
        Cursor _c = cr.query(BASEURI, null, null, null, null);
        int count = _c.getCount() ;
        _c.close() ;
        return count;
    }


    public static boolean isProdIdExist(Context context, String[] custid) {
        ContentResolver cr = context.getContentResolver();
        String where = LOCALPRODUCTNO +" = "+custid[0] + " AND "+SALE_TYPE+" = "+DatabaseUtils.sqlEscapeString
                (custid[1]);
        Cursor c = cr.query(BASEURI, null, where, null, null);
        try {
            if (c.moveToFirst()) {
                return true;
            }
        } finally {
            if (c != null) {
                c.close();
            }
        }
        return false;
    }



    public static boolean insertItems(Context context, String[] items){
        int itemcount = getCount(context) ;
        boolean ret = false;
        ContentResolver cr = context.getContentResolver() ;
        ContentValues cv = new ContentValues() ;
        cv.put(PRODUCTNO, items[0]);
        cv.put(LOCALPRODUCTNO, items[1]);
        cv.put(PRODUCTQTY, items[2]);
        cv.put(NETSALEPRICE, items[3]);
        cv.put(SALEPRICE, items[4]);
        cv.put(TAX_AMOUNT, items[5]);
        cv.put(SUBTOTAL, items[6]);
        if(items[7].equals("0"))
            cv.put(WH, Warehouse.getDefaultId(context));
        else
            cv.put(WH,items[7]);
        cv.put(TIMESTAMP, System.currentTimeMillis());
        cv.put(DISCOUNTPRICE, items[8]);
        cv.put(TAXRATE, items[9]);
        cv.put(SALE_TYPE, items[10]);
        cv.put(DELSTATUS, items[10]);
        cv.put(ACK, items[11]);
        double itemtax = 0;
        double dis = new BigDecimal(items[8]).doubleValue();
        if(dis < 1) {
            itemtax = new BigDecimal(items[4]).subtract(new BigDecimal(items[3])).setScale(2, RoundingMode
                    .HALF_UP).doubleValue();
            System.out.println(" no discount  ts ("+items[4]+") - ns ("+items[3]+") =  itemstax ==>"+itemtax);
        } else {
            itemtax = new BigDecimal(items[8]).subtract(new BigDecimal(items[3])).setScale(2, RoundingMode
                    .HALF_UP).doubleValue();
            System.out.println(" thereis discount  ts ("+items[8]+") - ns ("+items[3]+") =  itemstax ==>"+itemtax);
        }
        cv.put(ITEM_TAX, itemtax);
        cr.insert(BASEURI, cv);
        if(getCount(context) == itemcount + 1)
            ret = true;

        return ret;
    }



    public static boolean updateItems(Context context, String[] items){

        boolean ret = false;
        ContentResolver cr = context.getContentResolver() ;
        ContentValues cv = new ContentValues() ;
        cv.put(PRODUCTNO, items[0]);
        cv.put(PRODUCTQTY, items[2]);
        cv.put(NETSALEPRICE, items[3]);
        cv.put(SALEPRICE, items[4]);
        cv.put(TAX_AMOUNT, items[5]);
        cv.put(SUBTOTAL, items[6]);
        if(items[7].equals("0"))
            cv.put(WH, Warehouse.getDefaultId(context));
        else
            cv.put(WH,items[7]);
        cv.put(TIMESTAMP, System.currentTimeMillis());
        cv.put(DISCOUNTPRICE, items[8]);
        cv.put(TAXRATE, items[9]);
        cv.put(SALE_TYPE, items[10]);
        cv.put(DELSTATUS, items[10]);
        cv.put(ACK, items[11]);
        double itemtax = 0;
        double dis = new BigDecimal(items[8]).doubleValue();
        if(dis < 1) {
            itemtax = new BigDecimal(items[4]).subtract(new BigDecimal(items[3])).setScale(2, RoundingMode
                    .HALF_UP).doubleValue();
            System.out.println(" no discount  ts ("+items[4]+") - ns ("+items[3]+") =  itemstax ==>"+itemtax);
        } else {
            itemtax = new BigDecimal(items[8]).subtract(new BigDecimal(items[3])).setScale(2, RoundingMode
                    .HALF_UP).doubleValue();
            System.out.println(" thereis discount  ts ("+items[8]+") - ns ("+items[3]+") =  itemstax ==>"+itemtax);
        }
        cv.put(ITEM_TAX, itemtax);

        String where = LOCALPRODUCTNO + " = "+items[1];
        int up = cr.update(BASEURI, cv,where,null);
        if(up > 0)
            ret = true;

        return ret;
    }


    public static boolean purgeItems(Context context, String src){
        ContentResolver cv = context.getContentResolver();
        String where = SALE_TYPE +" = "+DatabaseUtils.sqlEscapeString(src);
        int del = cv.delete(BASEURI,where,null);
        if(del > 0)
            return true;
        return false;
    }
    public static ArrayList<Pending_Items> getItems(Context context,String souce){
        ContentResolver cr = context.getContentResolver();
        String where = SALE_TYPE + " = " + DatabaseUtils.sqlEscapeString(souce);

        Cursor c = cr.query(BASEURI, null, where, null, null) ;
        ArrayList<Pending_Items> custdetails = new ArrayList<>();
        try {
            if(c.moveToFirst()) {
                do {
                    long prodlo = c.getLong(c.getColumnIndexOrThrow(LOCALPRODUCTNO));
                    int q = c.getInt(c.getColumnIndexOrThrow(PRODUCTQTY));
                    int ps = c.getInt(c.getColumnIndexOrThrow(ACK));
                    String w = c.getString(c.getColumnIndexOrThrow(WH));
                    double netpr = c.getDouble(c.getColumnIndexOrThrow(NETSALEPRICE));
                    double slpr = c.getDouble(c.getColumnIndexOrThrow(SALEPRICE));
                    double edisc = c.getDouble(c.getColumnIndexOrThrow(DISCOUNTPRICE));
                    double p = c.getDouble(c.getColumnIndexOrThrow(TAXRATE));
                    String ref = c.getString(c.getColumnIndexOrThrow(PRODUCTNO));
                    long id = c.getLong(c.getColumnIndexOrThrow(ID));
                    double subtotal = c.getDouble(c.getColumnIndexOrThrow(SUBTOTAL));
                    double totatx = c.getDouble(c.getColumnIndexOrThrow(TAX_AMOUNT));
                    String stype = c.getString(c.getColumnIndexOrThrow(SALE_TYPE));
                    custdetails.add(new Pending_Items(prodlo,w,id,q,netpr,slpr,totatx,subtotal,p,edisc,ref,stype,ps));
                } while (c.moveToNext());
            }
        } finally {
            if(c != null) {
                c.close();
            }
        }
        return custdetails;
    }
    public static ArrayList<Pending_Items> getItems(Context context,long prodid,String souce){
        ContentResolver cr = context.getContentResolver();
        String where = SALE_TYPE + " = " + DatabaseUtils.sqlEscapeString(souce) +" AND "+LOCALPRODUCTNO+" = "+prodid;

        Cursor c = cr.query(BASEURI, null, where, null, null) ;
        ArrayList<Pending_Items> custdetails = new ArrayList<>();
        try {
            if(c.moveToFirst()) {
                    long prodlo = c.getLong(c.getColumnIndexOrThrow(LOCALPRODUCTNO));
                    int q = c.getInt(c.getColumnIndexOrThrow(PRODUCTQTY));
                int ps = c.getInt(c.getColumnIndexOrThrow(ACK));
                String w = c.getString(c.getColumnIndexOrThrow(WH));
                    double netpr = c.getDouble(c.getColumnIndexOrThrow(NETSALEPRICE));
                    double slpr = c.getDouble(c.getColumnIndexOrThrow(SALEPRICE));
                    double edisc = c.getDouble(c.getColumnIndexOrThrow(DISCOUNTPRICE));
                double p = c.getDouble(c.getColumnIndexOrThrow(TAXRATE));
                String ref = c.getString(c.getColumnIndexOrThrow(PRODUCTNO));
                long id = c.getLong(c.getColumnIndexOrThrow(ID));
                    double subtotal = c.getDouble(c.getColumnIndexOrThrow(SUBTOTAL));
                double totatx = c.getDouble(c.getColumnIndexOrThrow(TAX_AMOUNT));
                String stype = c.getString(c.getColumnIndexOrThrow(SALE_TYPE));
                    custdetails.add(new Pending_Items(prodlo,w,id,q,netpr,slpr,totatx,subtotal,p,edisc,ref,stype,ps));
            }
        } finally {
            if(c != null) {
                c.close();
            }
        }
        return custdetails;
    }

    public static double getTaxAmount(Context context, String status) {
        ContentResolver cr = context.getContentResolver();
        String where = SALE_TYPE + " = " + DatabaseUtils.sqlEscapeString(status);
        Cursor c = cr.query(BASEURI, new String[] {TAX_AMOUNT}, where,null,null) ;
        try {
            double i= 0 ;
            if(c.moveToFirst())
            {
                i = c.getDouble(c.getColumnIndexOrThrow(TAX_AMOUNT));
                return i ;
            }
        }finally {
            if(c != null) {
                c.close();
            }
        }
        return 0 ;
    }

    public long getId() {
        return id;
    }

    public long getOrderid() {
        return orderid;
    }

    public String getProd() {
        return prod;
    }

    public long getProdloc() {
        return prodloc;
    }

    public String getCust() {
        return cust;
    }

    public String getWh() {
        return wh;
    }

    public double getNetprice() {
        return netprice;
    }

    public double getPrice() {
        return price;
    }

    public double getTaxmt() {
        return taxmt;
    }

    public double getSbtotal() {
        return sbtotal;
    }

    public double getTaxrt() {
        return taxrt;
    }

    public double getDiscount() {
        return discount;
    }

    public double getTaxitem() {
        return taxitem;
    }

    public int getQty() {
        return qty;
    }

    public int getQtydel() {
        return qtydel;
    }

    public String getSaletype() {
        return saletype;
    }

}
