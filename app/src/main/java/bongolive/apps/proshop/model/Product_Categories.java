/*
 * The source codes in this project are owned by Bongolive Enterprises. Copying or any kind of usage without Bongolive's approval is against the law.
 * Copyright (c) 2015.
 *
 */

package bongolive.apps.proshop.model;

import android.content.ContentResolver;
import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.DatabaseUtils;
import android.net.Uri;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import bongolive.apps.proshop.R;
import bongolive.apps.proshop.controller.Validating;
import bongolive.apps.proshop.controller.Constants;
import bongolive.apps.proshop.controller.UrlCon;

public class Product_Categories {
    public static final String TABLENAME = "product_categories";
    public static final Uri BASEURI = Uri.parse("content://" + ContentProviderApi.AUTHORITY + "/" + TABLENAME);
    public static final String ID = "_id";
    public static final String CAT_CODE = "cat_code";
    public static final String CAT_NAME = "cat_name";
    public static final String CAT_ID = "cat_id";
    public static final String CREATED_ON = "created_on";
    public static final String ACK = "ack";//0 new //1 synced //2 update //3 delete
    public static final String SPECIFIC_CONTENT_TYPE = "vnd.android.cursor.item/vnd.com.proshop.cat";
    public static final String GENERAL_CONTENT_TYPE = "vnd.android.cursor.dir/vnd.com.proshop.cat";
    private static UrlCon js ;
    public static final String GETARRAYNAME = "cat_list";
    private static final String TAG = Product_Categories.class.getName();

    int id, ack , sysid;
    String code,name;

    public Product_Categories() {

    }


    public static int getCount(Context context){
        ContentResolver cr = context.getContentResolver();
        Cursor c = cr.query(BASEURI, null, null, null, null);
        int count = 0;
        try {
            count = c.getCount();
        } finally {
            if (c != null) {
                c.close();
            }
        }
        return count;
    }

    public static boolean isCatPresent(Context context, String whid, String source) {
        ContentResolver cr = context.getContentResolver();
        String where = null;
        if(source.equals(Constants.LOCAL)) {
            String prodcd  = whid.toUpperCase();
            prodcd = prodcd.replace(" ","");
            where = CAT_CODE + " = " + DatabaseUtils.sqlEscapeString(prodcd);
        }
        if(source.equals(Constants.REMOTE))
            where = CAT_ID +" = "+whid;
        boolean bool = false;
        Cursor c = cr.query(BASEURI, null, where, null, null);
        try {
            if (c.moveToFirst()) {
                        bool = true;
            }
        } finally {
            if (c != null) {
                c.close();
            }
        }
        return bool;
    }

    public static ArrayList<String> getProductCats(Context context) {
        ContentResolver cr = context.getContentResolver();
        Cursor c = cr.query(BASEURI, null, null, null, ID +" ASC") ;
        ArrayList<String> custdetails = new ArrayList<>();
        custdetails.add(context.getString(R.string.strselectcategory));
        try {
            if(c.moveToFirst()) {
                do {
                    String n = c.getString(c.getColumnIndexOrThrow(CAT_NAME));
                    custdetails.add(n);
                } while (c.moveToNext());
            }
            custdetails.add(context.getString(R.string.straddcat));
            return custdetails;
        } finally {
            if(c != null) {
                c.close();
            }
        }

    }

    public static ArrayList<String> getProductCatsId(Context context) {
        ContentResolver cr = context.getContentResolver();
        Cursor c = cr.query(BASEURI, null, null, null, ID +" ASC") ;
        ArrayList<String> custdetails = new ArrayList<>();
        custdetails.add(context.getString(R.string.strselectcategory));
        try {
            if(c.moveToFirst()) {
                do {
                    String n = c.getString(c.getColumnIndexOrThrow(ID));
                    custdetails.add(n);
                } while (c.moveToNext());
            }
            custdetails.add(context.getString(R.string.straddcat));
            return custdetails;
        } finally {
            if(c != null) {
                c.close();
            }
        }

    }


    public static String[] getCatCodeAndName(Context context, String custid) {
        String[] bz = null;
        ContentResolver cr = context.getContentResolver();
        String where = ID + " = " + custid;
        Cursor c = cr.query(BASEURI, null, where, null, null);
        try {
            if (c.moveToFirst()) {
                String cd = c.getString(c.getColumnIndexOrThrow(CAT_CODE));
                String nm = c.getString(c.getColumnIndexOrThrow(CAT_NAME));
                bz = new String[]{cd,nm};
            }
        } finally {
            if (c != null) {
                c.close();
            }
        }
        return bz;
    }

    public static void sendErrorJson(Context context,String cd) throws JSONException {
        JSONObject jobs = new JSONObject();
        jobs.put("tag", Constants.TAGACK);
        jobs.put(Constants.ACKITEM, TABLENAME);
        jobs.put(Constants.ERRORCODE, cd);
        jobs.put(Constants.IMEI, Constants.getIMEINO(context));
        js = new UrlCon();
        System.out.println(jobs.toString());
        js.getJson(Constants.SERVER, jobs) ;
    }

    public static int storeWCat(Context context, String[] string, String src) {
        int whcount = getCount(context);
        ContentResolver cr = context.getContentResolver();
        ContentValues values = new ContentValues();
        values.put(CAT_ID, string[0]);
        if(src.equals(Constants.LOCAL)) {
            values.put(CAT_NAME, string[1].toUpperCase());
            String prodct  = string[1].toUpperCase();
            prodct = prodct.replace(" ","");
            values.put(CAT_CODE, prodct);
        }
        if(src.equals(Constants.REMOTE)) {
            values.put(CAT_CODE, string[1]);
            values.put(CAT_NAME, string[2]);
            values.put(ACK, 1);
        }
        cr.insert(BASEURI, values);
        if (getCount(context) == whcount + 1) {
            return 1;
        }
        return whcount;
    }


    public static String getId(Context context) {
        ContentResolver cr = context.getContentResolver();
        Cursor c = cr.query(BASEURI, new String[]{ID}, null, null, null) ;
        String id = "0";
        try {
            if(c.moveToLast())
            {
                id = c.getString(c.getColumnIndexOrThrow(ID));
            }
        }finally {
            if(c != null) {
                c.close();
            }
        }
        return id ;
    }


    public static String getIdLite(Context context) {
        ContentResolver cr = context.getContentResolver();
        String where = CAT_CODE +" = "+DatabaseUtils.sqlEscapeString("GENERAL");
        Cursor c = cr.query(BASEURI, new String[]{ID}, where, null, null) ;
        String id = "0";
        try {
            if(c.moveToFirst())
            {
                id = c.getString(c.getColumnIndexOrThrow(ID));
            }
        }finally {
            if(c != null) {
                c.close();
            }
        }
        return id ;
    }


    public static void sendAckJson(Context context) throws JSONException {
        JSONObject jobs = new JSONObject();
        jobs.put("tag", Constants.TAGACK);
        jobs.put(Constants.ACKITEM, TABLENAME);
        jobs.put(Constants.IMEI, Constants.getIMEINO(context));
        js = new UrlCon();
        System.out.println(jobs.toString());
        js.getJson(Constants.SERVER, jobs) ;
    }


    public static boolean createDefault(Context context){
        String catname = "GENERAL";
        String catcode = "GENERAL";

        ContentResolver cr = context.getContentResolver();
        ContentValues values = new ContentValues();
        values.put(CAT_NAME, catname);
        values.put(CAT_CODE, catcode);
        cr.insert(BASEURI,values);
        if(getCount(context) > 0)
            return true;
        return false;
    }
    public static String getSysId(Context context, String catid) {
        ContentResolver cr = context.getContentResolver();
        String where = ID +" = "+catid;
        Cursor c = cr.query(BASEURI, new String[]{CAT_ID}, where, null, null) ;
        String id = "0";
        try {
            if(c.moveToFirst())
            {
                id = c.getString(c.getColumnIndexOrThrow(CAT_ID));
            }
        }finally {
            if(c != null) {
                c.close();
            }
        }
        return id ;
    }
    public static String getLocId(Context context, String catid) {
        ContentResolver cr = context.getContentResolver();
        String prodcd  = catid.toUpperCase().trim();
        prodcd = prodcd.replace(" ","");
        String where = CAT_CODE + " = " + DatabaseUtils.sqlEscapeString(prodcd);
        Cursor c = cr.query(BASEURI, new String[]{ID}, where, null, null) ;
        String id = "0";
        try {
            if(c.moveToFirst())
            {
                id = c.getString(c.getColumnIndexOrThrow(ID));
            }
        }finally {
            if(c != null) {
                c.close();
            }
        }
        return id ;
    }

    public static String getLocIdBasedOnSys(Context context, String catid) {
        ContentResolver cr = context.getContentResolver();
        String prodcd  = catid.toUpperCase().trim();
        prodcd = prodcd.replace(" ","");
        String where = CAT_ID + " = " + prodcd;
        Cursor c = cr.query(BASEURI, new String[]{ID}, where, null, null) ;
        String id = "0";
        try {
            if(c.moveToFirst())
            {
                id = c.getString(c.getColumnIndexOrThrow(ID));
            }
        }finally {
            if(c != null) {
                c.close();
            }
        }
        return id ;
    }


    public static int updateCat(Context context, String[] string) {
        ContentResolver cr = context.getContentResolver();
        ContentValues values = new ContentValues();
        values.put(CAT_CODE, string[1]);
        values.put(CAT_NAME, string[2]);
        values.put(ACK, "1");
        String where = CAT_ID +" = "+string[0];
        int upd = cr.update(BASEURI,values,where,null);
        if (upd > 0) {
            return 1;
        }
        return 0;
    }
    public static int updateCat(Context context,String string) {
        ContentResolver cr = context.getContentResolver();
        ContentValues values = new ContentValues();
        values.put(ACK, 0);
        values.put(CAT_ID, 0);
        String where = ID +" = "+string;
        int upd = cr.update(BASEURI,values,where,null);
        if (upd > 0) {
            return 1;
        }
        return 0;
    }

    public static boolean updateCatId(Context context, String[] string) {
        ContentResolver cr = context.getContentResolver();
        ContentValues values = new ContentValues();
        values.put(CAT_ID, string[1]);
        values.put(ACK, "1");
        String where = ID +" = "+string[0];
        if(cr.update(BASEURI,values,where,null)>0) {
            System.out.println("updating "+string[0] +" id "+string[1]);
            return true;
        }
        return false;
    }

    public static JSONObject getCatsJson(Context context) throws JSONException {
        JSONObject jobs = new JSONObject();
        jobs.put("tag", Constants.TAGGETCAT);
        jobs.put(Constants.IMEI, Constants.getIMEINO(context));
        js = new UrlCon();
        String job = js.getJson(Constants.SERVER, jobs) ;
        JSONObject json;
        try {
            json = new JSONObject(job);
            if (json.length() != 0) {
                return json;
            } else {
            }
        } catch (JSONException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        return null;
    }


    public static JSONObject sendCatsJson(Context context) throws JSONException
    {
        ContentResolver cr = context.getContentResolver() ;
        String where =  ACK + " != 1" ;
        Cursor c = cr.query(BASEURI, null, where, null, null);
        if(c.getCount() == 0){
            if(c != null)
                c.close();
            return null;
        }
        js = new UrlCon();
        try {
            JSONObject jsonObject = new JSONObject();
            jsonObject.put("tag", Constants.TAGSENDCAT);
            jsonObject.put(Constants.IMEI, Constants.getIMEINO(context));
            String array ;
            if(c.moveToFirst())
            {
                JSONArray jarr = new JSONArray();
                do{
                    JSONObject j = new JSONObject();
                    j.put(ID, c.getString(c.getColumnIndexOrThrow(ID)));
                    j.put(CAT_NAME, c.getString(c.getColumnIndexOrThrow(CAT_NAME)));
                    j.put(CAT_CODE, c.getString(c.getColumnIndexOrThrow(CAT_CODE)));
                    j.put(ACK, c.getString(c.getColumnIndexOrThrow(ACK)));
                    j.put(CAT_ID, c.getString(c.getColumnIndexOrThrow(CAT_ID)));
                    jarr.put(j);
                } while(c.moveToNext()) ;
                jsonObject.put(GETARRAYNAME,(Object)jarr);
                System.out.println("json " + jsonObject.toString() + " json");
                array = js.getJson(Constants.SERVER, jsonObject) ;
                JSONObject job = new JSONObject(array);
                return job;
            }
        } finally {
            if(c != null) {
                c.close();
            }
        }
        return null ;
    }


    public static void getCatfromServer(Context context) throws JSONException {
        JSONObject json = getCatsJson(context);
        if(json != null)
        try {
            if (json.has(Constants.SUCCESS)) {
                String res = json.getString(Constants.SUCCESS);
                if (Integer.parseInt(res) == 1) {

                    if (json.has(GETARRAYNAME)) {
                        JSONArray custarray = json.getJSONArray(GETARRAYNAME);
                        if(custarray.length() == 0)
                        {
                            System.out.println("product cats arraylengh is 0" );
                            return;
                        }
                        int success = 0;
                        for (int i = 0; i < custarray.length(); i++) {
                            JSONObject js = custarray.getJSONObject(i);
                            String whid = js.getString(CAT_ID);
                            String cd = js.getString(CAT_CODE);
                            String nm = js.getString(CAT_NAME);
                            if(whid.equals("0"))
                                continue;
                            String[] vals = {whid,cd,nm};
                            if(Validating.areSet(vals)) {
                                if(!isCatPresent(context, whid, Constants.REMOTE)) {
                                    if(storeWCat(context,vals, Constants.REMOTE) == 1)
                                        success += 1;
                                } else {
                                    if(updateCat(context, vals) == 1)
                                        success += 1;
                                }
                            }
                        }
                        if(success == custarray.length() && success >0)
                            sendAckJson(context);
                    }
                }
            }
        } catch (JSONException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

        JSONObject jsonout = sendCatsJson(context);
        if(jsonout != null)
            try {
                if (jsonout.has(Constants.SUCCESS)) {
                    String res = jsonout.getString(Constants.SUCCESS);
                    if (Integer.parseInt(res) == 1) {

                        if (jsonout.has(GETARRAYNAME)) {
                            JSONArray custarray = jsonout.getJSONArray(GETARRAYNAME);
                            if(custarray.length() == 0)
                            {
                                System.out.println("product cats arraylengh is 0" );
                                return;
                            }
                            for (int i = 0; i < custarray.length(); i++) {
                                JSONObject js = custarray.getJSONObject(i);
                                String whid = js.getString(CAT_ID);
                                String locid = js.getString(ID);
                                if(whid.equals("0"))
                                    continue;

                                System.out.println("updating "+whid +" id "+locid);
                                String[] vals = {locid,whid};
                                if(Validating.areSet(vals)) {
                                    if(whid.equals("0")) {
                                        updateCat(context,locid);
                                        continue;
                                    }
                                    System.out.println("updating "+whid +" id "+locid);
                                    updateCatId(context, vals);
                                }
                            }
                        }
                    }
                }
            } catch (JSONException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
    }

    public static void sendAck(Context context, String requiredid) throws JSONException {
        JSONObject jobs = new JSONObject();
        jobs.put("tag", Constants.TAGACK);
        jobs.put(Constants.ACK, Constants.ACKVALUE);
        jobs.put(Constants.IMEI, Constants.getIMEINO(context));
        jobs.put(Constants.ACKKEY, CAT_ID);
        jobs.put(CAT_ID, requiredid);
        js = new UrlCon();
        js.getJson(Constants.SERVER, jobs) ;
    }

    public Product_Categories(int wid, int wack, String wcode, String wname){
        id = wid;
        ack = wack;
        code = wcode;
        name = wname;
    }

    public int getId() {
        return id;
    }

    public int getAck() {
        return ack;
    }

    public int getSysid() {
        return sysid;
    }

    public String getCode() {
        return code;
    }

    public String getName() {
        return name;
    }

}
