/*
 * Copyright (c) 2015.
 * Prosurvey is a property of Bongolive Enterprises Ltd. An authorized
 * copy of ideas, source codes is against the regulations
 */

package bongolive.apps.proshop.model;

import android.content.ContentResolver;
import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.DatabaseUtils;
import android.net.Uri;
import android.util.Log;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.math.BigDecimal;
import java.util.ArrayList;

import bongolive.apps.proshop.R;
import bongolive.apps.proshop.controller.Constants;
import bongolive.apps.proshop.controller.UrlCon;
import bongolive.apps.proshop.controller.Validating;

public class Products
{
//product_id, product_name, description, items_on_hand,  category, product_status,
// re_order_value, container_type, unit_size, unit_measure, package_type, package_units,
// unit_sale_price, sku_product_no, tax_rate, created_on, created_by, account_id,  updated_on,
// updated_by
	public static final String TABLENAME = "products";
	public static final String ID = "_id";
	public static final String PRODUCTCODE = "product_uniq_name";
	public static final String PRODUCTNAME = "product_name";
	public static final String PACKAGETYPE = "package_type";
	public static final String TAXMETHOD = "tax_method";
	public static final String BARCODESYMBOL = "barcode_symbol";
	public static final String PACKAGEUNITS = "package_units";
	public static final String CONTINER = "container_type";
	public static final String ITEMS_ON_HAND = "items_on_hand" ;
	public static final String REODER = "re_order_value" ;
	public static final String PRODUCT_TYPE = "product_type" ; //this is for those products who
	// don't run out of stock 0 for normal 1 for the one that never end
	public static final String UNITPRICE = "unit_sale_price" ;
	public static final String CATEGORY = "category";
	public static final String IMAGE = "image";
	public static final String PRODSTATUS = "product_status";
	public static final String TAXRATE = "tax_rate";
	public static final String UPDATE = "updated_on" ;
	public static final String REFERENCE = "prod_system_id" ;//"system_prod_id" ;
	public static final String DESCRIPTION = "description" ;//"system_prod_id" ;
    public static final String SKU = "product_code" ;//"Sys_stock_id" ;
    public static final String UNITSIZE = "unit_size" ;//"Sys_stock_id" ;
    public static final String CREATED = "created_on" ;//"Sys_stock_id" ;
    public static final String SYNCSTATUS = "is_synced" ;
    public static final String UNITMEASURE = "unit_measure" ;//"Sys_stock_id" ;
	public static final Uri BASEURI = Uri.parse("content://"+ ContentProviderApi.AUTHORITY+"/"+TABLENAME) ;
	public static final String SPECIFIC_CONTENT_TYPE = "vnd.android.cursor.item/vnd.com.proshop.products" ;
	public static final String GENERAL_CONTENT_TYPE = "vnd.android.cursor.dir/vnd.com.proshop.products" ;
	private static final String TAG = Products.class.getName();
	static UrlCon js ;
	public static String ARRAYNAME = "productsList" ;

	private static JSONArray productsarray ;

	public Products() {

	}

	 public static int isProductPresent(Context context, String[] productname)
	    {
            String where = null;
            if(productname.length == 2)
            where = REFERENCE  + " = " + productname[1]  ;
            if(productname.length == 3)
                where = SKU  + " = " + DatabaseUtils.sqlEscapeString(productname[2])  ;
            if(productname.length == 1) {
                String pr = productname[0].replace(" ","").trim().toUpperCase();
                where = PRODUCTCODE + " = " + DatabaseUtils.sqlEscapeString(pr) + " OR "+SKU+" = "+DatabaseUtils
                        .sqlEscapeString(pr);
            }
            Cursor c = context.getContentResolver().query(BASEURI, null, where, null, null);
	    	 try {
	    	if(c.moveToFirst())
	    	{
                return 1;//product exists
	    	}
		 } finally {
			 if(c != null) {
		            c.close();
		        }
		 }
	    	return 0 ;
	    }

    private static boolean isProdIdExist(Context context, String custid) {
        ContentResolver cr = context.getContentResolver();
        String where = REFERENCE +" = "+custid;
        Cursor c = cr.query(BASEURI, null, where, null, null);
        try {
            if (c.moveToFirst()) {
                return true;
            }
        } finally {
            if (c != null) {
                c.close();
            }
        }
        return false;
    }
    public static boolean checkQty(Context context, long proid, int qty) {
        ContentResolver cr = context.getContentResolver();
        String where = ID +" = "+proid + " AND "+ITEMS_ON_HAND +" >= "+qty + " AND "+ qty +" > 0";
        Cursor c = cr.query(BASEURI, null, where, null, null);
        try {
            if (c.moveToFirst()) {
                return true;
            }
        } finally {
            if (c != null) {
                c.close();
            }
        }
        return false;
    }


    public static int isProductPresentUpdating(Context context, String productname)
	    {
            String pr = productname.replace(" ","").trim().toUpperCase();
            String where = PRODUCTCODE  + " = "+DatabaseUtils.sqlEscapeString(pr)  ;
            Cursor c = context.getContentResolver().query(BASEURI, null, where, null, null);
	    	 try {
	    	if(c.moveToFirst())
	    	{
                return c.getInt(c.getColumnIndex(ID));//product exists
	    	}
		 } finally {
			 if(c != null) {
		            c.close();
		        }
		 }
	    	return 0 ;
	    }
	public static int getItemOnHand(Context context, String stockid) {
		ContentResolver cr = context.getContentResolver();
		String where = ID + " = " + stockid ;
    	Cursor c = cr.query(BASEURI, new String[]{ITEMS_ON_HAND}, where, null, null) ;
    	try {
    	int i= 0 ;
    	if(c.moveToFirst())
    	{
    			i = c.getInt(c.getColumnIndexOrThrow(ITEMS_ON_HAND));
    			 return i ;
    	}
    	}finally {
			 if(c != null) {
		            c.close();
		        }
		 }
		return 0 ;
	}
	public static int checkItemOnHand(Context context) {
		ContentResolver cr = context.getContentResolver();

        int qty= 0 ;
        String where = SYNCSTATUS + " != 3";
    	Cursor c = cr.query(BASEURI, new String[]{ITEMS_ON_HAND}, where, null, null) ;
    	try {

    	if(c.moveToFirst())
    	{
    		do {
                int q;
    			q = c.getInt(c.getColumnIndexOrThrow(ITEMS_ON_HAND));
    			 if(q > 0)
                     qty += 1;
    		}while(c.moveToNext());
    	}
    	}finally {
			 if(c != null) {
		            c.close();
		        }
		 }
		return qty ;
	}
	/*
	 *  function to update in the stock table to those products that are existing
	 */
	public static boolean updateProduct(Context context, String[] string,String source)
	{
		ContentResolver cr = context.getContentResolver();
		ContentValues values = new ContentValues() ;
        String where = null;
        if(source.equals(Constants.LOCAL)) {
            values.put(PRODUCTNAME, string[0].toUpperCase());//prodname,reorder,price,tax,desc,sku
            String prodcd  = string[0].toUpperCase();
            prodcd = prodcd.replace(" ","");
            values.put(PRODUCTCODE, prodcd);
            values.put(PACKAGETYPE, context.getString(R.string.strno));
            values.put(PACKAGEUNITS, 0);
            values.put(CONTINER, context.getString(R.string.strno));
            values.put(REODER, string[1]);
            values.put(UNITPRICE, Double.parseDouble(string[2]));
            values.put(CATEGORY, string[7]);
            values.put(TAXRATE, string[3]);
            values.put(DESCRIPTION, string[5]);
            values.put(SKU, string[4]);
            values.put(PRODUCT_TYPE, string[8]);
            values.put(BARCODESYMBOL, string[9]);
            values.put(TAXMETHOD, string[10]);
            values.put(IMAGE, string[11]);
            values.put(UNITSIZE, 0);
            values.put(SYNCSTATUS, 2);
            values.put(UNITMEASURE, context.getString(R.string.strno));
            values.put(UPDATE, Constants.getDate());
            where = ID + " = " + string[6];
        } else if (source.equals(Constants.REMOTE)){
            values.put(PRODUCTNAME, string[0]);//prodname,reorder,price,tax,desc,sku
            values.put(PRODUCTCODE, string[1]);
            values.put(PACKAGETYPE, context.getString(R.string.strno));
            values.put(PACKAGEUNITS, 0);
            values.put(CONTINER, context.getString(R.string.strno));
            values.put(UNITPRICE, string[2]);
            values.put(CATEGORY, string[3]);
            values.put(TAXRATE, string[4]);
            values.put(DESCRIPTION, string[5]);
            values.put(SKU, string[6]);
            values.put(UNITSIZE, 0);
            values.put(SYNCSTATUS, 1);
            values.put(PRODUCT_TYPE, string[7]);
            values.put(BARCODESYMBOL, string[8]);
            values.put(TAXMETHOD, string[9]);
            values.put(REFERENCE, string[10]);
//            values.put(IMAGE, string[11]);
            values.put(UPDATE, Constants.getDate());
            values.put(UNITMEASURE, context.getString(R.string.strno));
            where = REFERENCE +" = "+ string[10];
        }
		if(cr.update(BASEURI, values, where, null) > 0) {
			return true ;
		}
		return false;
	}
	/*
	 * Save a new product in the stock table
	 */
	public static int insertProduct(Context context, String[] string, String source)
	{
		int productcount = getCount(context) ;
		ContentResolver cr = context.getContentResolver();
		ContentValues values = new ContentValues() ;
        if(source.equals(Constants.LOCAL)) {

            values.put(PRODUCTNAME, string[0].toUpperCase());//prodname,reorder,price,tax,desc,sku
            String prodcd  = string[0].toUpperCase();
            prodcd = prodcd.replace(" ","");
            values.put(PRODUCTCODE, prodcd);
            values.put(PACKAGETYPE, context.getString(R.string.strno));
            values.put(PACKAGEUNITS, 0);
            values.put(CONTINER, context.getString(R.string.strno));
            values.put(REODER, string[1]);
            values.put(UNITPRICE, string[2]);
            values.put(CATEGORY, string[6]);
            values.put(TAXRATE, string[3]);
            values.put(DESCRIPTION, string[5]);
            values.put(SKU, string[4]);
            values.put(UNITSIZE, 0);
            values.put(PRODUCT_TYPE, string[7]);
            values.put(BARCODESYMBOL, string[8]);
            values.put(TAXMETHOD, string[9]);
            values.put(IMAGE, string[10]);
            values.put(CREATED, Constants.getDate());
            values.put(UNITMEASURE, context.getString(R.string.strno));
        } else if(source.equals(Constants.REMOTE)){
            values.put(PRODUCTNAME, string[0]);//prodname,reorder,price,tax,desc,sku
            values.put(PRODUCTCODE, string[1]);
            values.put(PACKAGETYPE, context.getString(R.string.strno));
            values.put(PACKAGEUNITS, 0);
            values.put(CONTINER, context.getString(R.string.strno));
            values.put(REODER, 0);
            values.put(SYNCSTATUS, 1);
            values.put(UNITPRICE, string[2]);
            values.put(CATEGORY, string[3]);
            values.put(TAXRATE, string[4]);
            values.put(DESCRIPTION, string[5]);
            values.put(SKU, string[6]);
            values.put(UNITSIZE, 0);
            values.put(PRODUCT_TYPE, string[7]);
            values.put(BARCODESYMBOL, string[8]);
            values.put(TAXMETHOD, string[9]);
            values.put(REFERENCE, string[10]);
            String img = Constants.convertBase64toBitmap(context,string[11]);
            values.put(IMAGE, img);
            values.put(CREATED, Constants.getDate());
            values.put(UNITMEASURE, context.getString(R.string.strno));
        }
		cr.insert(BASEURI, values);
		if(getCount(context) == productcount+1){
			return 1 ;
		} else {
			return 0 ;
		}
	}
	/*
	 * How many items are in the table ?
	 */
	public static int getCount(Context context){
		ContentResolver cr = context.getContentResolver() ;
        String where = SYNCSTATUS + " != 3";
		Cursor _c = cr.query(BASEURI, null, where, null, null);
        int count = _c.getCount() ;
		_c.close() ;
		return count;
	}


    public static int deleteProduct(Context context, String string,int acctype)
    {
        ContentResolver cr = context.getContentResolver();
        ContentValues values = new ContentValues() ;
        values.put(SYNCSTATUS, "3") ;
        values.put(UPDATE, Constants.getDate());
        String where =  ID + " = " + string ;
        if(acctype == 2){
            if( cr.delete(BASEURI, where,null) > 0) {
                Log.v("updated", "updated");
                return 1;
            }
        } else
                if (cr.update(BASEURI, values, where, null) > 0) {
                    Log.v("updated", "updated");
                    return 1;
                }

        return 0;
    }

    public static int updateProductQty(Context context, int[] string)
    {
        ContentResolver cr = context.getContentResolver();
        ContentValues values = new ContentValues() ;
        int itemsonhand = getProductQuantity(context, string[0]);
        Log.v("itemsonhand"," are "+itemsonhand);

        int newitemonhand = 0;

        if(string[2] == 0)// add stock
        newitemonhand = new BigDecimal(itemsonhand).add(new BigDecimal(string[1])).intValue();

        if(string[2] == 1)//deduct products sold
            newitemonhand = new BigDecimal(itemsonhand).subtract(new BigDecimal(string[1])).intValue();
        Log.v("newitemsonhand"," are "+newitemonhand);
        values.put(ITEMS_ON_HAND, newitemonhand);
        values.put(UPDATE, Constants.getDate());

        String where = ID + " = "+ string[0] ;
        if(cr.update(BASEURI, values, where, null) > 0) {
            return 1 ;
        }
        return 0;
    }
    public static boolean updateQtyPurchaseItems(Context context, int[] string)
    {//0 id, 1 new qty
        ContentResolver cr = context.getContentResolver();
        ContentValues values = new ContentValues() ;
            values.put(ITEMS_ON_HAND, string[1]);
            values.put(UPDATE, Constants.getDate());

        String where = ID + " = "+ string[0] ;
        if(cr.update(BASEURI, values, where, null) > 0) {
            return true ;
        }
        return false;
    }

    public static boolean updateProductQtyItems(Context context, int[] string) {
        ContentResolver cr = context.getContentResolver();
        ContentValues values = new ContentValues();
        int itemsonhand = getProductQuantity(context, string[2]);
        Log.v("itemsonhand", " are " + itemsonhand);

        int newitemonhand = 0;
        int qty = new BigDecimal(itemsonhand).subtract(new BigDecimal(string[0])).intValue();
        newitemonhand = new BigDecimal(qty).add(new BigDecimal(string[1])).intValue();
        Log.v("newitemsonhand", " are " + newitemonhand + " after taking " + itemsonhand + " - " + string[0] + " plus new qty " +
                string[1]);
        values.put(ITEMS_ON_HAND, newitemonhand);
        values.put(UPDATE, Constants.getDate());

        String where = ID + " = " + string[2];
        if (cr.update(BASEURI, values, where, null) > 0) {
            return true;
        }
        return false;
    }

  public static boolean updateSalesProduct(Context context, String[] string)
    {
        ContentResolver cr = context.getContentResolver();
        ContentValues values = new ContentValues() ;
        values.put(SalesItems.PRODUCTNO, string[0]);
        String where = SalesItems.LOCALPRODUCTNO + " = "+ string[1] ;
        if(cr.update(SalesItems.BASEURI, values, where, null) > 0)
            return true;

        return false;
    }


    public static int getProductQuantity(Context context, int stockid) {
        ContentResolver cr = context.getContentResolver();
        String where = ID + " = " + stockid ;
        Cursor c = cr.query(BASEURI, new String[]{ITEMS_ON_HAND}, where, null, null) ;
        try {
            int i= 0 ;
            if(c.moveToFirst())
            {
                do {
                    i = c.getInt(c.getColumnIndexOrThrow(ITEMS_ON_HAND));
                    return i ;
                }while(c.moveToNext());
            }
        }finally {
            if(c != null) {
                c.close();
            }
        }
        return 0 ;
    }

    public static int confirmdeleteProduct(Context context, String string)
    {
        ContentResolver cr = context.getContentResolver();
        String where =  REFERENCE + " = " + string ;
        if( cr.delete(BASEURI, where, null) > 0)
            return 1;
        return 0;
    }

	/*
	 * Fetching stock for a new product giving out imei of the device
	 */
	public static JSONObject getProductJson(Context context) throws JSONException
	{
        JSONObject jobs = new JSONObject();
        jobs.put("tag", Constants.TAGSRECEIVEPRODUCT);
        jobs.put(Constants.IMEI, Constants.getIMEINO(context));
        System.out.println(jobs.toString());
        js = new UrlCon();
        String job =  js.getJson(Constants.SERVER, jobs) ;
        JSONObject json;
        try {
            json = new JSONObject(job);
            if(json.length() != 0)
            {
                return json ;
            } else {
            }
        } catch (JSONException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        return null ;
	}

	/*
	 * Fetching stock for a new product giving out imei of the device
	 */
    public static JSONObject sendProductsJson(Context context) throws JSONException
    {
        ContentResolver cr = context.getContentResolver() ;
        String where = SYNCSTATUS + " != 1" ;
        Cursor c = cr.query(BASEURI, null, where, null, null);
        if(c.getCount() == 0){
            c.close();
            return null;
        }
        js = new UrlCon();
        try {
            JSONObject jsonObject = new JSONObject();
            jsonObject.put("tag", Constants.TAGSSENDPRODUCT);
            jsonObject.put(Constants.IMEI, Constants.getIMEINO(context));
            String array ;
            if(c.moveToFirst())
            {
                JSONArray jarr = new JSONArray();
                do{
                    JSONObject j = new JSONObject();
                    String catid = Product_Categories.getSysId(context, c.getString(c.getColumnIndexOrThrow(CATEGORY)));
                    if(catid.equals("0"))
                        continue;

                    j.put(ID, c.getString(c.getColumnIndexOrThrow(ID)));
                    j.put(PRODUCTNAME, c.getString(c.getColumnIndexOrThrow(PRODUCTNAME)));
                    j.put(PRODUCTCODE, c.getString(c.getColumnIndexOrThrow(PRODUCTCODE)));
                    j.put(REODER, c.getString(c.getColumnIndexOrThrow(REODER)));
                    j.put(UNITPRICE, c.getString(c.getColumnIndexOrThrow(UNITPRICE)));
                    j.put(CATEGORY, catid);
                    j.put(TAXRATE, c.getString(c.getColumnIndexOrThrow(TAXRATE)));
                    j.put(REFERENCE, c.getString(c.getColumnIndexOrThrow(REFERENCE)));
                    j.put(DESCRIPTION, c.getString(c.getColumnIndexOrThrow(DESCRIPTION)));
                    j.put(SKU, c.getString(c.getColumnIndexOrThrow(SKU)));
                    j.put(SYNCSTATUS,c.getString(c.getColumnIndexOrThrow(SYNCSTATUS)));
                    j.put(PRODUCT_TYPE, c.getString(c.getColumnIndexOrThrow(PRODUCT_TYPE)));
                    j.put(BARCODESYMBOL, c.getString(c.getColumnIndexOrThrow(BARCODESYMBOL)));
                    j.put(TAXMETHOD, c.getString(c.getColumnIndexOrThrow(TAXMETHOD)));
                    j.put(IMAGE, c.getString(c.getColumnIndexOrThrow(IMAGE)));
                    jarr.put(j);
                } while(c.moveToNext()) ;
                jsonObject.put(ARRAYNAME,(Object)jarr);
                System.out.println("json " + jsonObject.toString() + " json");
                array = js.getJson(Constants.SERVER, jsonObject) ;
                JSONObject job = new JSONObject(array);
                return job;
            }
        } finally {
            if(c != null) {
                c.close();
            }
        }
        return null ;
    }


    public static boolean updateSalesPurchaseProducts(Context context, String[] string)
    {
        ContentResolver cr = context.getContentResolver();
        ContentValues values = new ContentValues() ;
        values.put(PurchaseItems.SYS_PRODID, string[0]);
        String where = PurchaseItems.PRODUCT + " = "+ string[1] ;
        ContentValues cv = new ContentValues();
        cv.put(SalesItems.PRODUCTNO,string[0]);
        String wherer1 = SalesItems.LOCALPRODUCTNO+" = "+string[1];
        if(cr.update(PurchaseItems.BASEURI, values, where, null) > 0 || cr.update(SalesItems.BASEURI,cv,wherer1,null)>0)
            return true;
        return false;
    }
    public static void processInformation(Context context) throws JSONException{
        JSONObject jsonout = sendProductsJson(context);
        if(jsonout != null)
        try {
            if(jsonout.has(Constants.SUCCESS)){
                String res = jsonout.getString(Constants.SUCCESS) ;
                if(Integer.parseInt(res) == 1)
                {
                        if(jsonout.has(ARRAYNAME)){
                            JSONArray jsonArray = jsonout.getJSONArray(ARRAYNAME);
                            if(jsonArray.length() == 0)
                            {
                                System.out.println("product arraylengh is 0" );
                                return;
                            }
                            for(int i = 0; i< jsonArray.length(); i++) {
                                JSONObject job = jsonArray.getJSONObject(i);
                                String ref = job.getString(REFERENCE);
                                String localid = job.getString(ID);

                                if(job.has(Constants.ERRORCODE)){
                                    System.out.println(" cat is "+job.getString(Constants.ERRORCODE));
                                    String errorcode = job.getString(Constants.ERRORCODE);
                                    if(errorcode.equals("10")){
                                        String cat = getCategory(context,localid);
                                        Product_Categories.updateCat(context,cat);
                                        System.out.println(" cat is "+cat);
                                    }
                                            continue;
                                }
                                if(ref.equals("0"))
                                    continue;
                                String[] req = {ref,localid};
                                if(Validating.areSet(req)) {
                                    if(updateSync(context,req)) {
                                        System.out.println("product updated" + ref);
                                        updateSalesPurchaseProducts(context,req);
                                    }
                                }
                            }
                        }
//                    }
                }
            }
        } catch (NumberFormatException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } catch (JSONException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

        JSONObject jsonin = getProductJson(context);

        if(jsonin != null)
        try {
            if(jsonin.has(Constants.SUCCESS)){
                String res = jsonin.getString(Constants.SUCCESS) ;
                if(Integer.parseInt(res) == 1)
                {
                    if(jsonin.has(ARRAYNAME)){
                            JSONArray jsonArray = jsonin.getJSONArray(ARRAYNAME);
                        if(jsonArray.length() == 0)
                        {
                            System.out.println("product arraylengh is 0" );
                            return;
                        }
                            int success = 0;
                            for(int i = 0; i< jsonArray.length(); i++) {
                                JSONObject job = jsonArray.getJSONObject(i);
                                String prn = job.getString(PRODUCTNAME);
                                String prc = job.getString(PRODUCTCODE);
                                String unitprice = job.getString(UNITPRICE);
                                String cat = job.getString(CATEGORY);
                                String txr = job.getString(TAXRATE);
                                String desc = job.getString(DESCRIPTION);
                                String sku = job.getString(SKU);
                                String prdtype = job.getString(PRODUCT_TYPE);
                                String barcode = job.getString(BARCODESYMBOL);
                                String taxmeth = job.getString(TAXMETHOD);
                                String ref = job.getString(REFERENCE);
                                String img = job.getString(IMAGE);

                                if(ref.equals("0"))
                                    continue;
                                String loccat = "0";
                                String[] vals = {prn,prc,unitprice,cat,txr,desc,sku,prdtype,barcode,taxmeth,ref,img};
                                String[] check = {prn,prc,unitprice,cat,txr,sku,prdtype,barcode,taxmeth,ref};
                                if(Validating.areSet(check)){
                                    String[] newval = null;
                                    if(!Product_Categories.isCatPresent(context,cat,Constants.REMOTE)) {
                                        System.out.println("product category does not exist" + cat);
                                        continue;
                                    }

                                    loccat = Product_Categories.getLocIdBasedOnSys(context,cat);
                                    newval = new String[]{prn,prc,unitprice,loccat,txr,desc,sku,prdtype,barcode,
                                            taxmeth,  ref,img};
                                    if(isProductPresent(context,new String[]{prc,ref}) == 0) {
                                        System.out.println("product does not exist" + prc+ " local catid "+loccat);
                                        if(!loccat.equals("0"))
                                        if (insertProduct(context, newval, Constants.REMOTE) == 1) {
                                            System.out.println("product inserted " + prc);
                                            success += 1;
                                        }
                                    } else {
                                        System.out.println("product does exist" + prc);
                                        //check if this id exist
                                        //if not update its id,
                                        //update all entities in this system with this updated id
                                        if(isProdIdExist(context,ref)){
                                            if (updateProduct(context, newval, Constants.REMOTE)) {
                                                System.out.println("product updated" + prc);
                                                success += 1;
                                            }
                                        }

                                    }

                                }

                            }

                        if(success == jsonArray.length() && jsonArray.length() >0) {
                            sendAckJson(context);
                        } else {
                            System.out.println("send json error");
                            sendErrorJson(context, new String[]{Product_Categories.TABLENAME,"16"});
                        }
                    }
                }
            }
        } catch (NumberFormatException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } catch (JSONException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }

    /*
     * Acknowledgement to the server to tick item as a go
     */
	public static void sendAcks(Context context, String[] requiredid) throws JSONException
    {
        JSONObject jobs = new JSONObject();
        jobs.put("tag", Constants.TAGACK);
        jobs.put(Constants.ACK, Constants.ACKVALUE);
        jobs.put(Constants.IMEI, Constants.getIMEINO(context));
        jobs.put(Constants.ACKREFERENCE, requiredid[0]);
        jobs.put(Constants.ACKKEY, REFERENCE);
         js = new UrlCon();
        js.getJson(Constants.SERVER, jobs) ;
	}



    public static boolean updateSync(Context context, String[] vals)
    {
        ContentResolver cr = context.getContentResolver();
        ContentValues values = new ContentValues() ;
        values.put(SYNCSTATUS, 1);
        values.put(REFERENCE, vals[0]);
        String where =  ID+ " = "+ vals[1] ;
        if(cr.update(BASEURI, values, where, null) >0)
        {
            System.out.println("product sync updated to 1 and ref is " + vals[0]);
            return true ;
        }   else {
            return false ;
        }
    }


    public static void sendAckJson(Context context) throws JSONException {
        JSONObject jobs = new JSONObject();
        jobs.put("tag", Constants.TAGACK);
        jobs.put(Constants.ACKITEM, TABLENAME);
        jobs.put(Constants.IMEI, Constants.getIMEINO(context));
        js = new UrlCon();
        System.out.println(jobs.toString());
        js.getJson(Constants.SERVER, jobs) ;
    }


    public static void sendErrorJson(Context context,String[] cd) throws JSONException {
        JSONObject jobs = new JSONObject();
        jobs.put("tag", Constants.TAGACK);
        jobs.put(Constants.ACKITEM, cd[0]);
        jobs.put(Constants.ERRORCODE, cd[1]);
        jobs.put(Constants.IMEI, Constants.getIMEINO(context));
        js = new UrlCon();
        System.out.println(jobs.toString());
        js.getJson(Constants.SERVER, jobs) ;
    }

	public static ArrayList<String> getAllProducts(Context context)
    {
        ContentResolver cr = context.getContentResolver();
        String where = SYNCSTATUS + " != 3";
        Cursor cursor = cr.query(BASEURI, null, where, null, null) ;
        ArrayList<String> str = new ArrayList<>(cursor.getCount());
        if(cursor.getCount() > 0)
        {

            int i = 0;
            str.add(context.getString(R.string.strchoseprod));
            while (cursor.moveToNext())
            {
                 str.add(cursor.getString(cursor.getColumnIndex(PRODUCTNAME)));
                 i++;
             }
            cursor.close();
        }
        else
        {
            cursor.close();;
        }
        return str;
    }

	public static ArrayList<String> getAllProductsSyncd(Context context)
    {
        ContentResolver cr = context.getContentResolver();
        String where = REFERENCE + " > 0";
        Cursor cursor = cr.query(BASEURI, null, where, null, null) ;
        ArrayList<String> str = new ArrayList<>(cursor.getCount());
        if(cursor.getCount() > 0)
        {

            int i = 0;
            str.add(context.getString(R.string.strchoseprod));
            while (cursor.moveToNext())
            {
                 str.add(cursor.getString(cursor.getColumnIndex(PRODUCTNAME)));
                 i++;
             }
            cursor.close();
        }
        else
        {
            cursor.close();;
        }
        return str;
    }

	public static ArrayList<String> getAllProductsCode(Context context)
    {
        ContentResolver cr = context.getContentResolver();
        String where = SYNCSTATUS + " != 3";
        Cursor cursor = cr.query(BASEURI, null, where, null, null) ;
        ArrayList<String> str = new ArrayList<>(cursor.getCount());
        if(cursor.getCount() > 0)
        {

            int i = 0;
            str.add(context.getString(R.string.strchoseprod));
            while (cursor.moveToNext())
            {
                str.add(cursor.getString(cursor.getColumnIndex(PRODUCTCODE)));
                i++;
            }
            cursor.close();
        }
        else
        {
            cursor.close();;
        }
        return str;
    }

	public static ArrayList<String> getAllProductsCodeSyncd(Context context)
    {
        ContentResolver cr = context.getContentResolver();
        String where = REFERENCE + " > 0";
        Cursor cursor = cr.query(BASEURI, null, where, null, null) ;
        ArrayList<String> str = new ArrayList<>(cursor.getCount());
        if(cursor.getCount() > 0)
        {

            int i = 0;
            str.add(context.getString(R.string.strchoseprod));
            while (cursor.moveToNext())
            {
                str.add(cursor.getString(cursor.getColumnIndex(PRODUCTCODE)));
                i++;
            }
            cursor.close();
        }
        else
        {
            cursor.close();;
        }
        return str;
    }
	public static ArrayList<Long> getAllProductIds(Context context)
    {
        ContentResolver cr = context.getContentResolver();
        Cursor cursor = cr.query(BASEURI, null, null, null, null) ;
        ArrayList<Long> str = new ArrayList<>(cursor.getCount());
        if(cursor.getCount() > 0)
        {

            int i = 0;
            long id = 0;
            str.add(id);
            while (cursor.moveToNext())
            {
                str.add(cursor.getLong(cursor.getColumnIndex(ID)));
                i++;
            }
            cursor.close();
        }
        else
        {
            cursor.close();;
        }
        return str;
    }

	public static int getAllProductQuantity(Context context, String productname)
    {
        ContentResolver cr = context.getContentResolver();
//        String where = PRODUCTNAME + " LIKE \'%" + productname + "%\'"  ;
        String where = PRODUCTCODE + " = " + DatabaseUtils.sqlEscapeString(productname)  ;
        Cursor cursor = cr.query(BASEURI, new String[] {ITEMS_ON_HAND}, where, null, null) ;
        try{
        if(cursor.moveToFirst())
        {
        	do{
        		int quantity = cursor.getInt(cursor.getColumnIndexOrThrow(ITEMS_ON_HAND));
        		return quantity;
        	} while (cursor.moveToNext());
        }
        }finally {
			 if(cursor != null) {
			        cursor.close();
		        }
		 }
        return 0;
    }


	public static int getTax(Context context, String productname)
    {
        ContentResolver cr = context.getContentResolver();
        String where = PRODUCTCODE +  " = " + DatabaseUtils.sqlEscapeString(productname);
        Cursor cursor = cr.query(BASEURI, new String[] {TAXRATE}, where, null, null) ;
        try{
        if(cursor.moveToFirst())
        {
        		int quantity = cursor.getInt(cursor.getColumnIndexOrThrow(TAXRATE));
        		return quantity;
        }
        }finally {
			 if(cursor != null) {
			        cursor.close();
		        }
		 }
        return 0;
    }
    public static int getTax(Context context, long productname)
    {
        ContentResolver cr = context.getContentResolver();
        String where = ID +  " = " + productname;
        Cursor cursor = cr.query(BASEURI, new String[] {TAXRATE}, where, null, null) ;
        try{
        if(cursor.moveToFirst())
        {
        		int quantity = cursor.getInt(cursor.getColumnIndexOrThrow(TAXRATE));
        		return quantity;
        }
        }finally {
			 if(cursor != null) {
			        cursor.close();
		        }
		 }
        return 0;
    }

    public static int getUnitPrice(Context context, long productname)
    {
        ContentResolver cr = context.getContentResolver();
        String where = ID +  " = " + productname;
        Cursor cursor = cr.query(BASEURI, new String[] {UNITPRICE}, where, null, null) ;
        try{
        if(cursor.moveToFirst())
        {
        		int quantity = cursor.getInt(cursor.getColumnIndexOrThrow(UNITPRICE));
        		return quantity;
        }
        }finally {
			 if(cursor != null) {
			        cursor.close();
		        }
		 }
        return 0;
    }


    public static String getUnitPriceBasedSysid(Context context, String prid)
    {
        ContentResolver cr = context.getContentResolver();
        String where = ID +  " = " + prid;
        Cursor cursor = cr.query(BASEURI, new String[] {UNITPRICE}, where, null, null) ;
        String quantity = "0";
        try{
        if(cursor != null && cursor.moveToFirst())
        {
        		quantity = cursor.getString(cursor.getColumnIndexOrThrow(UNITPRICE));
        }
        }finally {
			 if(cursor != null) {
			        cursor.close();
		        }
		 }
        return quantity;
    }

	public static int getProdid(Context context, String productname)
    {
        ContentResolver cr = context.getContentResolver();
        String where = PRODUCTCODE +  " = " + DatabaseUtils.sqlEscapeString(productname);
        Cursor cursor = cr.query(BASEURI, new String[] {ID}, where, null, null) ;
        try{
        if(cursor.moveToFirst())
        {
        	do{
        		int quantity = cursor.getInt(cursor.getColumnIndexOrThrow(ID));
        		return quantity;
        	} while (cursor.moveToNext());
        }
    }finally {
		 if(cursor != null) {
		        cursor.close();
	        }
	 }
        return 0;
    }

	public static String getProdid(Context context, long productname)
    {
        ContentResolver cr = context.getContentResolver();
        String where = ID +  " = " + productname;
        Cursor cursor = cr.query(BASEURI, new String[] {REFERENCE}, where, null, null) ;
        try{
        if(cursor.moveToFirst())
        {
        	do{
        		String quantity = cursor.getString(cursor.getColumnIndexOrThrow(REFERENCE));
        		return quantity;
        	} while (cursor.moveToNext());
        }
    }finally {
		 if(cursor != null) {
		        cursor.close();
	        }
	 }
        return "0";
    }

	public static int getLocalProdid(Context context, String productname)
    {
        ContentResolver cr = context.getContentResolver();
        String where = PRODUCTCODE +  " = " + DatabaseUtils.sqlEscapeString(productname);
        Cursor cursor = cr.query(BASEURI, new String[] {ID}, where, null, null) ;
        try{
        if(cursor.moveToFirst())
        {
        	do{
        		int quantity = cursor.getInt(cursor.getColumnIndexOrThrow(ID));
        		return quantity;
        	} while (cursor.moveToNext());
        }
    }finally {
		 if(cursor != null) {
		        cursor.close();
	        }
	 }
        return 0;
    }

	public static String getLocalProdidBasedOnSystemId(Context context, String ref)
    {
        ContentResolver cr = context.getContentResolver();
        String where = REFERENCE +  " = " + ref;
        Cursor cursor = cr.query(BASEURI, new String[] {ID}, where, null, null) ;
        try{
        if(cursor.moveToFirst())
        {
        	do{
        		String quantity = cursor.getString(cursor.getColumnIndexOrThrow(ID));
        		return quantity;
        	} while (cursor.moveToNext());
        }
    }finally {
		 if(cursor != null) {
		        cursor.close();
	        }
	 }
        return "0";
    }
	public static String getSysProdidBasedOnLocalId(Context context, String ref)
    {
        ContentResolver cr = context.getContentResolver();
        String where = ID +  " = " + ref;
        Cursor cursor = cr.query(BASEURI, new String[] {REFERENCE}, where, null, null) ;
        try{
        if(cursor.moveToFirst())
        {
        		String quantity = cursor.getString(cursor.getColumnIndexOrThrow(REFERENCE));
        		return quantity;
        }
    }finally {
		 if(cursor != null) {
		        cursor.close();
	        }
	 }
        return "0";
    }
	public static String getSystemId(Context context, String ref)
    {
        ContentResolver cr = context.getContentResolver();
        String where = PRODUCTCODE +  " = " + DatabaseUtils.sqlEscapeString(ref);
        Cursor cursor = cr.query(BASEURI, new String[] {REFERENCE}, where, null, null) ;
        try{
        if(cursor.moveToFirst())
        {
        		String quantity = cursor.getString(cursor.getColumnIndexOrThrow(REFERENCE));
        		return quantity;
        }
    }finally {
		 if(cursor != null) {
		        cursor.close();
	        }
	 }
        return "0";
    }

	public static int getUnitPrice(Context context, String productname)
    {
        ContentResolver cr = context.getContentResolver();
//        String where = PRODUCTNAME + " LIKE \'%" + productname + "%\'"  ;
        String where = PRODUCTCODE +  " = " + DatabaseUtils.sqlEscapeString(productname);
        Cursor cursor = cr.query(BASEURI, null, where, null, null) ;
        try{
        if(cursor.moveToFirst())
        {
        		int quantity = cursor.getInt(cursor.getColumnIndexOrThrow(UNITPRICE));
        		return quantity;
        }
        }finally {
			 if(cursor != null) {
			        cursor.close();
		        }
		 }
        return 0;
    }

    public static String getSku(Context context, String productname)
    {
        ContentResolver cr = context.getContentResolver();
        String where = PRODUCTCODE +  " = " + DatabaseUtils.sqlEscapeString(productname);
        Cursor cursor = cr.query(BASEURI, null, where, null, null) ;
        try{
        if(cursor.moveToFirst())
        {
            String quantity = cursor.getString(cursor.getColumnIndexOrThrow(SKU));
        		return quantity;
        }
        }finally {
			 if(cursor != null) {
			        cursor.close();
		        }
		 }
        return "";
    }

	public static String[] getSoldOut(Context context)
    {
        ContentResolver cr = context.getContentResolver();

        String where = SYNCSTATUS + " != 3";
        where = ITEMS_ON_HAND +" < 1" ;
        Cursor cursor = cr.query(BASEURI, null, where, null, null) ;
        if(cursor.getCount() > 0)
        {
            String[] str = new String[cursor.getCount()];
            int i = 0;

            while (cursor.moveToNext())
            {
                 str[i] = cursor.getString(cursor.getColumnIndex(PRODUCTNAME));

                 i++;
             }
            cursor.close();
            return str;
        }
        else
        {
            cursor.close();
            return new String[] {};
        }
    }
    public static String getProdName(Context context, String prodid)
    {
        ContentResolver cr = context.getContentResolver();
        String where = ID + " = " + prodid ;
        Cursor cursor = cr.query(BASEURI, new String[] {PRODUCTNAME}, where, null, null) ;
        try{
            if(cursor.moveToFirst())
            {
                    String quantity = cursor.getString(cursor.getColumnIndexOrThrow(PRODUCTNAME));
                    return quantity;
            }
        }finally {
            if(cursor != null) {
                cursor.close();
            }
        }
        return "";
    }

    public static String getProdNameBasedSyid(Context context, String prodid)
    {
        ContentResolver cr = context.getContentResolver();
        String where = REFERENCE + " = " + prodid ;
        Cursor cursor = cr.query(BASEURI, new String[] {PRODUCTNAME}, where, null, null) ;
        try{
            if(cursor != null && cursor.moveToFirst())
            {
                    String quantity = cursor.getString(cursor.getColumnIndexOrThrow(PRODUCTNAME));
                    return quantity;
            }
        }finally {
            if(cursor != null) {
                cursor.close();
            }
        }
        return "";
    }

    public static String getProductType(Context context, String prodid)
    {
        ContentResolver cr = context.getContentResolver();
        String where = ID + " = " + prodid ;
        Cursor cursor = cr.query(BASEURI, null, where, null, null) ;
        try{
            if(cursor.moveToFirst())
            {
                    String quantity = cursor.getString(cursor.getColumnIndexOrThrow(PRODUCT_TYPE));
                    return quantity;
            }
        }finally {
            if(cursor != null) {
                cursor.close();
            }
        }
        return "";
    }

    public static String getProductIdUsingSku(Context context, String sku)
    {
        ContentResolver cr = context.getContentResolver();
        String where = SKU + " = " + DatabaseUtils.sqlEscapeString(sku) +" OR "+PRODUCTNAME+" = "+DatabaseUtils
                .sqlEscapeString(sku);
        Cursor cursor = cr.query(BASEURI, null, where, null, null) ;
        try{
            if(cursor.moveToFirst())
            {
                    String quantity = cursor.getString(cursor.getColumnIndexOrThrow(PRODUCTCODE));
                    return quantity;
            }
        }finally {
            if(cursor != null) {
                cursor.close();
            }
        }
        return "";
    }
    public static int getTaxmethod(Context context, String prodid)
    {
        ContentResolver cr = context.getContentResolver();
        String where = PRODUCTCODE + " = " + DatabaseUtils.sqlEscapeString(prodid);
        Cursor cursor = cr.query(BASEURI, null, where, null, null) ;
        try{
            if(cursor.moveToFirst())
            {
                    int quantity = cursor.getInt(cursor.getColumnIndexOrThrow(TAXMETHOD));
                    return quantity;
            }
        }finally {
            if(cursor != null) {
                cursor.close();
            }
        }
        return 0;
    }
    public static int getTaxmethod(Context context, long prodid)
    {
        ContentResolver cr = context.getContentResolver();
        String where = ID + " = " + prodid;
        Cursor cursor = cr.query(BASEURI, null, where, null, null) ;
        try{
            if(cursor.moveToFirst())
            {
                    int quantity = cursor.getInt(cursor.getColumnIndexOrThrow(TAXMETHOD));
                    return quantity;
            }
        }finally {
            if(cursor != null) {
                cursor.close();
            }
        }
        return 0;
    }

    public static String getCategory(Context context, String prodid)
    {
        ContentResolver cr = context.getContentResolver();
        String where = ID + " = " + prodid;
        Cursor cursor = cr.query(BASEURI, null, where, null, null) ;
        try{
            if(cursor.moveToFirst())
            {
                    String quantity = cursor.getString(cursor.getColumnIndexOrThrow(CATEGORY));
                    return quantity;
            }
        }finally {
            if(cursor != null) {
                cursor.close();
            }
        }
        return "0";
    }

    public static String[] getProducts(Context context, long i) {
        ContentResolver cr = context.getContentResolver();
        String where = ID + " = " + i ;

        Cursor c = cr.query(BASEURI, null, where,null,null) ;
        String[] custdetails ;
        try {
            if(c.moveToFirst())
            {//name,qty,reorder,unitprice,tax,lastupdate,
                // description,
                String n = c.getString(c.getColumnIndexOrThrow(PRODUCTNAME));
                String cd = c.getString(c.getColumnIndexOrThrow(PRODUCTCODE));
                String b = c.getString(c.getColumnIndexOrThrow(UNITPRICE));
                String e = c.getString(c.getColumnIndexOrThrow(UPDATE));
                String p = c.getString(c.getColumnIndexOrThrow(ITEMS_ON_HAND));
                String a = c.getString(c.getColumnIndexOrThrow(TAXRATE));
                String ar = c.getString(c.getColumnIndexOrThrow(REODER));
                String sk = c.getString(c.getColumnIndexOrThrow(SKU));
                String dc = c.getString(c.getColumnIndexOrThrow(DESCRIPTION));
                String ct = c.getString(c.getColumnIndexOrThrow(CATEGORY));
                String sv = c.getString(c.getColumnIndexOrThrow(PRODUCT_TYPE));
                String tm = c.getString(c.getColumnIndexOrThrow(TAXMETHOD));
                String ph = c.getString(c.getColumnIndexOrThrow(IMAGE));
                if(!Validating.areSet(e))
                    e = "never";
                custdetails = new String[]{n,b,e,p,a,ar,dc,sk,ct,sv,cd,tm,ph};
                return custdetails ;
            }
        } finally {
            if(c != null) {
                c.close();
            }
        }
        return null;
    }

    public static ArrayList<String> getProductCats(Context context) {
        ContentResolver cr = context.getContentResolver();
        Cursor c = cr.query(BASEURI, new String[]{" DISTINCT " + CATEGORY}, null, null, null) ;
        ArrayList<String> custdetails = new ArrayList<>();
        custdetails.add(context.getString(R.string.strselectcategory));
        try {
            if(c.moveToFirst()) {
                do {
                    String n = c.getString(c.getColumnIndexOrThrow(CATEGORY));
                    custdetails.add(n);
                } while (c.moveToNext());
            }
            custdetails.add(context.getString(R.string.straddcat));
            return custdetails;
        } finally {
            if(c != null) {
                c.close();
            }
        }

    }
}