/*
 * Copyright (c) 2015.
 * Prosurvey is a property of Bongolive Enterprises Ltd. An authorized
 * copy of ideas, source codes is against the regulations
 */

package bongolive.apps.proshop.model;

import android.content.ContentResolver;
import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.DatabaseUtils;
import android.database.sqlite.SQLiteDatabase;
import android.net.Uri;
import android.util.Log;

import org.json.JSONException;
import org.json.JSONObject;

import bongolive.apps.proshop.controller.Constants;
import bongolive.apps.proshop.controller.UrlCon;

/**
 * Created by nasznjoka on 3/26/15.
 */
public class MultimediaContents {
    public static final String TABLENAME = "multimedia";
    public static final String ID = "id";
    public static final String CONTENT = "content";
    public static final String SIZE = "size";
    public static final String TYPE = "type";
    public static final String NAME = "name";
    public static final String DATE = "date";
    public static final String FOREIGNKEY = "foreginkeyid";
    public static final String ISSYNCED = "issynced";

    public static final Uri BASEURI = Uri.parse("content://"+ ContentProviderApi.AUTHORITY+"/"+TABLENAME) ;
    public static final String SPECIFIC_CONTENT_TYPE = "vnd.android.cursor.item/vnd.com.proshop.multimedia" ;
    public static final String GENERAL_CONTENT_TYPE = "vnd.android.cursor.dir/vnd.com.proshop.multimedia" ;
    private static final String TAG = MultimediaContents.class.getName();
    public static final String  OPERATIONTYPEIS = "issue";
    public static final String  OPERATIONTYPESL = "sale";
    public static final String  OPERATIONTYPED = "delivery";
    public static final String  OPERATIONTYPET = "transfer";
    public static final String  OPERATIONTYPES = "station";
    public static final String  OPERATIONTYPEA = "audit";
    public static final String  OPERATIONTYPEE = "expense";
    public static final String  OPERATIONTYPEP = "product";
    public static final String OPERATIONTYPEPY = "payment";
    static UrlCon js ;
    public static String ARRAYNAME = "multimedia_array" ;

    public static int getCount(Context context){
        ContentResolver cr = context.getContentResolver() ;
        Cursor _c = cr.query(BASEURI, null, null, null, null);
        int count = _c.getCount() ;
        _c.close() ;
        return count;
    }

    public static boolean storeMultimedia(Context context,String[] vals_){
        int loc = getCount(context);
        ContentResolver cr = context.getContentResolver();
        ContentValues cv = new ContentValues();
        cv.put(CONTENT,vals_[0]);
        cv.put(NAME, vals_[1]);
        cv.put(TYPE, vals_[2]);
        cv.put(FOREIGNKEY, vals_[3]);
        cv.put(SIZE, 0);
        cv.put(DATE, Constants.getDate());
       cr.insert(BASEURI, cv);
        if(loc + 1 == getCount(context)){
            Log.v("MULTIMEDIA", "DATA IS STORED");
            purgeData(context);
            return true;
        }
      return false;
    }


    public static boolean isMediaStored(Context context, String custid)
    {
        ContentResolver cr = context.getContentResolver();
        boolean ret = false;
        String where = FOREIGNKEY +" = "+DatabaseUtils.sqlEscapeString(custid);
        Cursor c = cr.query(BASEURI, null, where, null, null);
        try {
            if (c != null && c.moveToFirst()) {
                ret = true ;
            }
        } finally {
            if (c != null) {
                c.close();
            }
        }
        return ret;
    }

    public static JSONObject sendMultimedia(Context context) throws JSONException
    {
        ContentResolver cr = context.getContentResolver() ;
        String where = ISSYNCED + " = 0" ;
        Cursor c = cr.query(BASEURI, null, where, null, DATE +" ASC LIMIT 1");
        js = new UrlCon();
        JSONObject jsonObject = new JSONObject();
        String array = null;
        try {
            jsonObject.put("tag", Constants.TAGMULTIMEDIA);
            jsonObject.put(Constants.IMEI, Constants.getIMEINO(context));
            if(c != null && c.moveToFirst())
            {
                JSONObject j = new JSONObject();
                j.put(NAME, c.getString(c.getColumnIndexOrThrow(NAME)));
                j.put(ID, c.getString(c.getColumnIndexOrThrow(ID)));
                j.put(TYPE, c.getString(c.getColumnIndex(TYPE)));
                j.put(CONTENT, Constants.getbase64photo(c.getString(c.getColumnIndexOrThrow(CONTENT))));
                j.put(FOREIGNKEY, (c.getString(c.getColumnIndexOrThrow(FOREIGNKEY))));
                jsonObject.put(ARRAYNAME, j);
                array = js.getJson(Constants.SERVER, jsonObject) ;
            }
        } finally {
            if(c != null) {
                c.close();
            }
        }

        if(array != null) {
            JSONObject job = new JSONObject(array);

            return job;
        }
        return null;
    }


    public static JSONObject getMultimedia(Context context) throws JSONException{
        JSONObject jsonObject = new JSONObject() ;
        js = new UrlCon();
        jsonObject.put("tag", Constants.TAGMULTIMEDIA_GET);
        jsonObject.put(Constants.IMEI, Constants.getIMEINO(context));
        String str = js.getJson(Constants.SERVER, jsonObject);
        JSONObject object = new JSONObject(str);
        return object;
    }

    public static boolean columnExist(SQLiteDatabase context,String column){
        boolean ret= false;
        String sql = " SELECT * FROM "+TABLENAME  +" LIMIT 1";
        Cursor c = context.rawQuery(sql, null);
        try {
            int indext = c.getColumnIndex(column);
            System.out.println(" index is " +indext);
            if(indext != -1)
                ret = true;
        }finally {
            if(c != null) {
                c.close();
            }
        }
        return ret;
    }


    public static void processInformation(Context context) throws JSONException{
        JSONObject json = sendMultimedia(context);
        if(json != null)
        try {
            if(json.has(Constants.SUCCESS)){
                String res = json.getString(Constants.SUCCESS) ;
                if(Integer.parseInt(res) == 1) {

                        String locid = json.getString(FOREIGNKEY);
                        boolean OK = updateSync(context, locid, 1);
                        if (OK == true) {
                            Log.v("updated", "the media is synced " + locid);
                        }

                }
            }
        } catch (NumberFormatException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } catch (JSONException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }
    public static boolean updateSync(Context context, String id, int flag)
    {
        ContentResolver cr = context.getContentResolver();
        ContentValues values = new ContentValues() ;
        values.put(ISSYNCED, flag);
        String where =  FOREIGNKEY+ " = "+ DatabaseUtils.sqlEscapeString(id);
        if(cr.update(BASEURI, values, where, null) == 1)
        {
            return true ;
        }   else {
            return false ;
        }
    }

    public static int purgeData(Context context){
        ContentResolver cr = context.getContentResolver();
        int data = getCount(context);
        if(data > 50){
            Cursor c = cr.query(BASEURI, new String[]{ID},null,null,null);
            if(c.moveToLast()){
                    String locid = c.getString(c.getColumnIndexOrThrow(ID));
                    deleteContent(context,locid);
            }
        }
        return 0;
    }

    public static int deleteContent(Context context, String data){
        ContentResolver cr = context.getContentResolver();
        String where = ID +" = "+ data +
                " AND " + ISSYNCED + " = 1";
        if(cr.delete(BASEURI, where, null) > 0){
            Log.v("DELETING DATA", "LOC  ID "+ data);
            return 1;
        }
        return 0;
    }

    public static String getLastImage(Context context)
    {
        String bz = null;
        ContentResolver cr = context.getContentResolver();
        String where = TYPE + " = "+ DatabaseUtils.sqlEscapeString(Constants.MULT_IMAGE);
        Cursor c = cr.query(BASEURI, null, where,null,ID + " DESC LIMIT 1") ;
        try {
            if(c.moveToFirst())
            {
                bz = c.getString(c.getColumnIndexOrThrow(CONTENT));
            }
        } finally {
            if(c != null) {
                c.close();
            }
        }
        return bz ;
    }

    public static String getImage(Context context, String time)
    {
        String where = FOREIGNKEY + " = "+time;
        ContentResolver cr = context.getContentResolver();
        Cursor c = cr.query(BASEURI, null, where,null,null) ;
        String bz = null;
        try {
            if(c != null && c.moveToFirst())
            {
                bz = c.getString(c.getColumnIndexOrThrow(CONTENT));
            }
        } finally {
            if(c != null) {
                c.close();
            }
        }
        return bz ;
    }

    public static String getLastClip(Context context)
    {
        String bz = null;
        ContentResolver cr = context.getContentResolver();
        String where = TYPE + " = "+ DatabaseUtils.sqlEscapeString(Constants.MULT_VIDEO);
        Cursor c = cr.query(BASEURI, null, where,null,ID + " DESC LIMIT 1") ;
        try {
            if(c.moveToFirst())
            {
                bz = c.getString(c.getColumnIndexOrThrow(CONTENT));
            }
        } finally {
            if(c != null) {
                c.close();
            }
        }
        return bz ;
    }

}
