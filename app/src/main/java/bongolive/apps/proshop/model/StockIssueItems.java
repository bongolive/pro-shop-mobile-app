/*
 * The source codes in this project are owned by Bongolive Enterprises. Copying or any kind of usage without Bongolive's approval is against the law.
 * Copyright (c) 2015.
 *
 */

package bongolive.apps.proshop.model;

import android.content.ContentResolver;
import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.DatabaseUtils;
import android.database.sqlite.SQLiteDatabase;
import android.net.Uri;

import com.jjoe64.graphview.series.DataPoint;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.ArrayList;
import java.util.List;

import bongolive.apps.proshop.controller.Constants;
import bongolive.apps.proshop.controller.OrderItemList;
import bongolive.apps.proshop.controller.UrlCon;

public class StockIssueItems {
    public static final String TABLENAME = "stock_issue_items";
    public static final String ID = "_id";
    public static final String PRODUCTNO = "prod_system_id" ;
    public static final String CUSTOMERID = "cust_id" ;
    public static final String LOCALPRODUCTNO = "local_system_id" ;
    public static final String PRODUCTQTY = "prod_order_qty" ;
    public static final String PRODUCTQTYDEL = "prod_order_qty_delivered" ;
    public static final String SALEPRICE = "unit_price" ;
    public static final String NETSALEPRICE = "net_unit_price" ;
    public static final String TAX_AMOUNT = "tax_amount" ;
    public static final String ITEM_TAX = "item_tax" ;
    public static final String SUBTOTAL = "subtotal" ;
    public static final String WH = "wh" ;
    public static final String DISCOUNTPRICE = "discounted_price" ;
    public static final String TAXRATE = "tax_rate";
    public static final String ORDERID = "order_id";
    public static final String SALEITEMID = "sale_item_id";
    public static final String SALE_REFERENCE = "sale_refid";
    public static final String DELSTATUS = "delivery_status";
    public static final String ACK = "ack" ;
    public static final String TIMESTAMP = "timestamp" ;
    public static final String SALE_TYPE = "sale_type" ;
    public static final Uri BASEURI = Uri.parse("content://"+ ContentProviderApi.AUTHORITY+"/"+TABLENAME) ;
    public static final String SPECIFIC_CONTENT_TYPE = "vnd.android.cursor.item/vnd.com.proshop.stock_issue_items" ;
    public static final String GENERAL_CONTENT_TYPE = "vnd.android.cursor.dir/vnd.com.proshop.stock_issue_items" ;

    public static String ARRAYNAME= "sales_itemlist" ;
    static UrlCon js;
    long id,orderid,prodloc;
    String prod,cust,wh;
    double netprice,price,taxmt,sbtotal,taxrt,discount,taxitem;
    int qty,ack,qtydel;

    public StockIssueItems(){

    }

    public StockIssueItems(long prdlocal, String custm, String whs, long i, long ord, int qt, double ntprice,
                           double slprice, double taxamnt, double subtotal, double taxrate, double prdiscount){
        prodloc = prdlocal;
        cust = custm;
        wh = whs;
        id = i;
        orderid = ord;
        netprice = ntprice;
        price = slprice;
        taxmt = taxamnt;
        sbtotal = subtotal;
        taxrt = taxrate;
        discount = prdiscount;
        qty = qt;
    }
    public StockIssueItems(long prdlocal, String whs, long i, int qt, double ntprice, double slprice, double taxamnt, double
            subtotal, double taxrate, double prdiscount, String pro){
        prodloc = prdlocal;
        wh = whs;
        id = i;
        netprice = ntprice;
        price = slprice;
        taxmt = taxamnt;
        sbtotal = subtotal;
        taxrt = taxrate;
        discount = prdiscount;
        qty = qt;
        prod = pro;
    }
    public StockIssueItems(long prdlocal, String whs, long i, int qt, double ntprice, double slprice, double taxamnt, double
            subtotal, double taxrate, double prdiscount, String pro, int ac){
        prodloc = prdlocal;
        wh = whs;
        id = i;
        netprice = ntprice;
        price = slprice;
        taxmt = taxamnt;
        sbtotal = subtotal;
        taxrt = taxrate;
        discount = prdiscount;
        qty = qt;
        prod = pro;
        ack = ac;
    }

    public StockIssueItems(long prdlocal, String whs, long i, int qt, double ntprice, double slprice, double taxamnt, double
            subtotal, double taxrate, double prdiscount, double itmtx, String pro, int ac, int qtyd){
        prodloc = prdlocal;
        wh = whs;
        id = i;
        netprice = ntprice;
        price = slprice;
        taxmt = taxamnt;
        sbtotal = subtotal;
        taxrt = taxrate;
        discount = prdiscount;
        qty = qt;
        prod = pro;
        ack = ac;
        taxitem = itmtx;
        qtydel = qtyd;
    }

    public StockIssueItems(long ref, int qd) {
        this.prodloc = ref;
        this.qtydel = qd;
    }


    public static int getCount(Context context){
        ContentResolver cr = context.getContentResolver() ;
        Cursor _c = cr.query(BASEURI, null, null, null, null);
        int count = _c.getCount() ;
        _c.close() ;
        return count;
    }
    public static int insertItems(Context context, String[] items, String source){
        int itemcount = getCount(context) ;

        ContentResolver cr = context.getContentResolver() ;
        ContentValues cv = new ContentValues() ;
        if(source.equals(Constants.REMOTE)) {
            cv.put(PRODUCTNO, items[0]);
            cv.put(LOCALPRODUCTNO, items[1]);
            cv.put(PRODUCTQTY, items[2]);
            cv.put(PRODUCTQTYDEL, items[2]);
            cv.put(NETSALEPRICE, items[3]);
            cv.put(SALEPRICE, items[4]);
            cv.put(TAX_AMOUNT, items[5]);
            cv.put(SUBTOTAL, items[6]);

            if(items[7].equals("0"))
                cv.put(WH, Warehouse.getDefaultId(context));
            else
                cv.put(WH, items[7]);

            cv.put(ACK, 1);
            cv.put(DISCOUNTPRICE, items[8]);
            cv.put(TAXRATE, items[9]);
            cv.put(ORDERID, items[10]);
            cv.put(SALE_REFERENCE, items[11]);
            cv.put(TIMESTAMP, items[12]);
            cv.put(ITEM_TAX, items[13]);
            cv.put(DELSTATUS, items[14]);
            cv.put(SALEITEMID, items[15]);
            cv.put(SALE_TYPE, items[16]);
        } else if (source.equals(Constants.LOCAL)){
            System.out.println("inserted " + items[11]);
            cv.put(PRODUCTNO, items[0]);
            cv.put(LOCALPRODUCTNO, items[1]);
            cv.put(PRODUCTQTY, items[2]);
            cv.put(PRODUCTQTYDEL, items[2]);
            cv.put(NETSALEPRICE, items[3]);
            cv.put(SALEPRICE, items[4]);
            cv.put(TAX_AMOUNT, items[5]);
            cv.put(SUBTOTAL, items[6]);

            if(items[7].equals("0"))
                cv.put(WH, Warehouse.getDefaultId(context));
            else
                cv.put(WH, items[7]);

            cv.put(ACK, 0);
            cv.put(DISCOUNTPRICE, items[8]);
            cv.put(TAXRATE, items[9]);
            cv.put(ORDERID, items[10]);
            cv.put(SALE_REFERENCE, items[11]);
            cv.put(TIMESTAMP, System.currentTimeMillis());
            cv.put(DELSTATUS, items[12]);
            cv.put(SALE_TYPE, items[13]);
            System.out.println(" saleitem type is "+items[13]);
            double itemtax = 0;
            double dis = new BigDecimal(items[8]).doubleValue();
            if(dis < 1) {
                itemtax = new BigDecimal(items[4]).subtract(new BigDecimal(items[3])).setScale(2, RoundingMode
                        .HALF_UP).doubleValue();
                System.out.println(" no discount  ts ("+items[4]+") - ns ("+items[3]+") =  itemstax ==>"+itemtax);
            } else {
                itemtax = new BigDecimal(items[8]).subtract(new BigDecimal(items[3])).setScale(2, RoundingMode
                        .HALF_UP).doubleValue();
                System.out.println(" thereis discount  ts ("+items[8]+") - ns ("+items[3]+") =  itemstax ==>"+itemtax);
            }
            cv.put(ITEM_TAX, itemtax);
        }
        cr.insert(BASEURI, cv);
        if(getCount(context) == itemcount + 1)
        {
            return 1 ;
        } else {
            return 0 ;
        }
    }

    public static void sendAckJson(Context context) throws JSONException {
        JSONObject jobs = new JSONObject();
        jobs.put("tag", Constants.TAGACK);
        jobs.put(Constants.ACKITEM, TABLENAME);
        jobs.put(Constants.IMEI, Constants.getIMEINO(context));
         js = new UrlCon();
        System.out.println(jobs.toString());
        js.getJson(Constants.SERVER, jobs);
    }

    public static String[] getOrderItems(Context context, long idd)
    {
        ContentResolver cr = context.getContentResolver();
        String where = ID + " = " + idd ;

        Cursor c = cr.query(BASEURI, null, where, null, null) ;
        String[] custdetails ;
        try {
            if(c.moveToFirst())
            {
                String n = c.getString(c.getColumnIndexOrThrow(LOCALPRODUCTNO));
                String b = c.getString(c.getColumnIndexOrThrow(PRODUCTQTY));
                String e = c.getString(c.getColumnIndexOrThrow(SALEPRICE));
                String edisc = c.getString(c.getColumnIndexOrThrow(DISCOUNTPRICE));
                String p = c.getString(c.getColumnIndexOrThrow(TAXRATE));
                String ar = c.getString(c.getColumnIndexOrThrow(ACK));
                String id = c.getString(c.getColumnIndexOrThrow(ID));
                String subtotal = c.getString(c.getColumnIndexOrThrow(SUBTOTAL));
                String totatx = c.getString(c.getColumnIndexOrThrow(TAX_AMOUNT));
                custdetails = new String[]{n,b,e,p,ar,id,edisc};
                //prodno/prodqty/saleprice/taxrate/syncstatus
                return custdetails ;
            }
        } finally {
            if(c != null) {
                c.close();
            }
        }
        return null;
    }



    public static List<StockIssueItems> getStockItems(Context context,long i){
        ContentResolver cr = context.getContentResolver();
        String where = ORDERID + " = " + i ;

        Cursor c = cr.query(BASEURI, null, where, null, null) ;
        ArrayList<StockIssueItems> custdetails = new ArrayList<>();
        try {
            if(c.moveToFirst()) {
                do {
                    long prodlo = c.getLong(c.getColumnIndexOrThrow(LOCALPRODUCTNO));
                    int q = c.getInt(c.getColumnIndexOrThrow(PRODUCTQTY));
                    int qd = c.getInt(c.getColumnIndexOrThrow(PRODUCTQTYDEL));
                    String w = c.getString(c.getColumnIndexOrThrow(WH));
                    double netpr = c.getDouble(c.getColumnIndexOrThrow(NETSALEPRICE));
                    double slpr = c.getDouble(c.getColumnIndexOrThrow(SALEPRICE));
                    double edisc = c.getDouble(c.getColumnIndexOrThrow(DISCOUNTPRICE));
                    double p = c.getDouble(c.getColumnIndexOrThrow(TAXRATE));
                    int ack = c.getInt(c.getColumnIndexOrThrow(ACK));
                    String ref = c.getString(c.getColumnIndexOrThrow(PRODUCTNO));
                    long id = c.getLong(c.getColumnIndexOrThrow(ID));
                    double subtotal = c.getDouble(c.getColumnIndexOrThrow(SUBTOTAL));
                    double totatx = c.getDouble(c.getColumnIndexOrThrow(TAX_AMOUNT));
                    double itax = c.getDouble(c.getColumnIndexOrThrow(ITEM_TAX));
                    custdetails.add(new StockIssueItems(prodlo, w, id, q, netpr, slpr, totatx, subtotal,itax, p, edisc, ref,
                            ack,qd));
                } while (c.moveToNext());
            }
        } finally {
            if(c != null) {
                c.close();
            }
        }
        return custdetails;
    }


    public static ArrayList<StockIssueItems> getItemsarraylist(Context context,long i){
        ContentResolver cr = context.getContentResolver();
        String where = ORDERID + " = " + i ;

        Cursor c = cr.query(BASEURI, null, where, null, null) ;
        ArrayList<StockIssueItems> custdetails = new ArrayList<>();
        try {
            if(c.moveToFirst()) {
                do {

                    int qd = c.getInt(c.getColumnIndexOrThrow(PRODUCTQTYDEL));
                    long ref = c.getLong(c.getColumnIndexOrThrow(LOCALPRODUCTNO));
                    custdetails.add(new StockIssueItems(ref,qd));
                } while (c.moveToNext());
            }
        } finally {
            if(c != null) {
                c.close();
            }
        }
        return custdetails;
    }

    public static DataPoint[] getProductsCount(Context context, String limit){

        DataPoint[] vals = null;
        String query = " select sum("+TABLENAME+"."+PRODUCTQTY+") as qty, "+ Products.TABLENAME+"."+Products
                .PRODUCTNAME  +" from" +TABLENAME +" left join products on order_items.local_system_id = products._id    group by local_system_id order by  " +
                "prod_order_qty desc limit "+limit;
        final Cursor c1 = DatabaseHandler.getInstance(context).getReadableDatabase().rawQuery(query, null);
        if (c1 != null) {
            System.out.println(" totalrows "+c1.getCount());
            vals = new DataPoint[c1.getCount()];
            try {
                if(c1.moveToFirst()) {
                    double mon = 0, tues = 0, wed = 0, thur = 0, frd = 0, sat = 0, sun = 0;
                    mon = c1.getDouble(c1.getColumnIndex("monday"));
                    tues = c1.getDouble(c1.getColumnIndex("tuesday"));
                    wed = c1.getDouble(c1.getColumnIndex("wednesday"));
                    thur = c1.getDouble(c1.getColumnIndex("thursday"));
                    frd = c1.getDouble(c1.getColumnIndex("friday"));
                    sat = c1.getDouble(c1.getColumnIndex("saturday"));
                    sun = c1.getDouble(c1.getColumnIndex("sunday"));
                    vals = new DataPoint[]{new DataPoint(0, sun), new DataPoint(1, mon), new DataPoint(2, tues),
                            new DataPoint(3, wed), new DataPoint(4, thur), new DataPoint(5, frd), new DataPoint(6, sat)};
                }
            } finally {
                c1.close();
            }
        }
        return vals;
    }


    public static boolean columnExist(SQLiteDatabase context,String column){
        boolean ret= false;
        String sql = " SELECT * FROM "+TABLENAME  +" LIMIT 1";

        Cursor c = context.rawQuery(sql, null);
        try {
            int indext = c.getColumnIndex(column);
            System.out.println(" index is " +indext);
            if(indext != -1)
                ret = true;
        }finally {
            if(c != null) {
                c.close();
            }
        }
        return ret;
    }

    public static ArrayList<OrderItemList> getItemsArrayList(Context context, long idd)
    {
        ContentResolver cr = context.getContentResolver();
        String where = ORDERID + " = " + idd ;

        Cursor c = cr.query(BASEURI, null, where, null, null) ;
        ArrayList<OrderItemList> custdetails = new ArrayList<OrderItemList>();
        try {
            if(c.moveToFirst())
            {
                int count = 1;
                do {
                    count +=1 ;
                    int productid = c.getInt(c.getColumnIndexOrThrow(LOCALPRODUCTNO));
                    int quantity = c.getInt(c.getColumnIndexOrThrow(PRODUCTQTY));
                    double sprice = c.getDouble(c.getColumnIndexOrThrow(SALEPRICE));
                    double taxrate = c.getDouble(c.getColumnIndexOrThrow(TAXRATE));
                    String prodname = Products.getProdName(context, String.valueOf(productid));
                    System.out.println("prod is "+prodname+ " productid is "+productid);
                    double grandtotal = Sales.getGrandTotal(context, idd);
                    double taxamount = Sales.getTaxAmount(context, idd);
                    double grandtotalwithnotax = new BigDecimal(grandtotal).add(new BigDecimal(taxamount))
                            .doubleValue();
                    double spricediscount = c.getDouble(c.getColumnIndexOrThrow(DISCOUNTPRICE));

                    custdetails.add(new OrderItemList(prodname,quantity,grandtotal,
                            String.valueOf(count),sprice,productid,grandtotalwithnotax,taxrate,taxamount,spricediscount));

                } while (c.moveToNext());
                return custdetails;
            }
        } finally {
            if(c != null) {
                c.close();
            }
        }
        return null;
    }

    public static ArrayList<StockIssueItems> getSaleItems(Context context, long idd)
    {
        ContentResolver cr = context.getContentResolver();
        String where = ORDERID + " = " + idd ;

        Cursor c = cr.query(BASEURI, null, where, null, null) ;
        ArrayList<StockIssueItems> custdetails = new ArrayList<StockIssueItems>();
        try {
            if(c.moveToFirst())
            {
                int count = 1;
                do {
                    count +=1 ;
                    long productid = c.getLong(c.getColumnIndexOrThrow(LOCALPRODUCTNO));
                    int quantity = c.getInt(c.getColumnIndexOrThrow(PRODUCTQTY));
                    double sprice = c.getDouble(c.getColumnIndexOrThrow(SALEPRICE));
                    double subtotal = c.getDouble(c.getColumnIndexOrThrow(SUBTOTAL));
                    double taxrate = c.getDouble(c.getColumnIndexOrThrow(TAXRATE));
                    double netsaleprice = c.getDouble(c.getColumnIndexOrThrow(NETSALEPRICE));
                    String whid = c.getString(c.getColumnIndex(WH));
                    String proid = c.getString(c.getColumnIndex(PRODUCTNO));

                    double taxamount = Sales.getTaxAmount(context, idd);

                    double spricediscount = c.getDouble(c.getColumnIndexOrThrow(DISCOUNTPRICE));
                    custdetails.add(new StockIssueItems(productid,whid,0,quantity,netsaleprice,sprice,taxamount,subtotal,
                            taxrate,spricediscount,proid));
                } while (c.moveToNext());
                return custdetails;
            }
        } finally {
            if(c != null) {
                c.close();
            }
        }
        return null;
    }

    public static void deleteall(Context context){
        ContentResolver cr = context.getContentResolver();
        String where = ORDERID + " > 0";
        cr.delete(BASEURI, where, null);
        cr.delete(Sales.BASEURI, where, null);
    }


    /* get all product id with system product id 0*/
    private static ArrayList<String> getIds(Context context){
        ArrayList<String> lst = new ArrayList<>();
        ContentResolver cr = context.getContentResolver() ;
        String where = PRODUCTNO +" = 0" ;
        Cursor c = cr.query(BASEURI, new String[]{" DISTINCT "+LOCALPRODUCTNO}, where, null, null);
        try {
            if (c != null && c.moveToFirst()) {
                do {
                    lst.add(c.getString(c.getColumnIndexOrThrow(LOCALPRODUCTNO)));
                } while (c.moveToNext());
                return lst;
            }
        } finally {
            if(c!= null)
                c.close();
        }
        return  null;
    }

    /* get all product id with system product id 0*/
    private static ArrayList<String> getIdsOnError(Context context){
        ArrayList<String> lst = new ArrayList<>();
        ContentResolver cr = context.getContentResolver() ;
        Cursor c = cr.query(BASEURI, new String[]{" DISTINCT "+LOCALPRODUCTNO}, null, null, null);
        try {
            if (c.moveToFirst()) {
                do {
                    lst.add(c.getString(c.getColumnIndexOrThrow(LOCALPRODUCTNO)));
                } while (c.moveToNext());
                return lst;
            }
        } finally {
            if(c!= null)
                c.close();
        }
        return  null;
    }

    public static void updateSaleItems(Context context){
        ArrayList<String> lst = getIds(context);
        ContentResolver cr = context.getContentResolver();

        if(lst == null || lst.size() == 0)
            return;
        for(int i = 0; i < lst.size(); i++ ) {
            ContentValues cv = new ContentValues();
            String prod = Products.getSysProdidBasedOnLocalId(context,lst.get(i));
            cv.put(PRODUCTNO, prod);
            String where = LOCALPRODUCTNO + " = " + lst.get(i)+" AND "+PRODUCTNO+" = 0";

            System.out.println( "updating "+cr.update(BASEURI, cv, where, null));
            System.out.println("updated "+lst.get(i));
        }
    }

    public static void updateSaleItemsOnError(Context context){
        ArrayList<String> lst = getIdsOnError(context);
        ContentResolver cr = context.getContentResolver();

        if(lst == null || lst.size() == 0)
            return;
        for(int i = 0; i < lst.size(); i++ ) {
            ContentValues cv = new ContentValues();
            String prod = Products.getSysProdidBasedOnLocalId(context,lst.get(i));
            cv.put(PRODUCTNO, prod);
            String where = LOCALPRODUCTNO + " = " + lst.get(i);

            System.out.println( "updating "+cr.update(BASEURI, cv, where, null));
            System.out.println("updated "+lst.get(i));
        }
    }

    /* get all order_Id */
    private static ArrayList<String> getSales(Context context){
        ArrayList<String> lst = new ArrayList<>();
        ContentResolver cr = context.getContentResolver() ;
        String where = ACK +" = 1";
        Cursor c = cr.query(BASEURI, new String[]{"DISTINCT "+ORDERID}, where, null, null);
        try {
            if (c.moveToFirst()) {
                do {
                    lst.add(c.getString(c.getColumnIndexOrThrow(ORDERID)));
                } while (c.moveToNext());
                return  lst;
            }
        } finally {
            if(c!= null)
                c.close();
        }
        return  lst;
    }


    public static JSONObject sendJson(Context context) throws JSONException
    {
        ContentResolver cr = context.getContentResolver() ;
        String where = ACK + " = 0" ;
        Cursor c = cr.query(BASEURI, null, where, null, null);
        if(c.getCount() == 0){
            c.close();
            return null;
        }
         js = new UrlCon();
        try {
            JSONObject jsonObject = new JSONObject();
            jsonObject.put("tag", Constants.TAGSSENDSTOCKITEMSISSUED);
            jsonObject.put(Constants.IMEI, Constants.getIMEINO(context));
            String array ;
            if(c.moveToFirst())
            {
                JSONArray jarr = new JSONArray();
                do{
                    JSONObject j = new JSONObject();
                    String ref = c.getString(c.getColumnIndexOrThrow(PRODUCTNO));
                    if(ref.equals("0"))
                        continue;
                    String ord = c.getString(c.getColumnIndexOrThrow(ORDERID)) ;
                    String cusid = StockIssue.getCustomer(context,ord);
                    if(cusid.equals("0"))
                        continue;

                    j.put(PRODUCTNO, ref);
                    j.put(ORDERID, ord);
                    j.put(ID, c.getString(c.getColumnIndexOrThrow(ID)));
                    j.put(LOCALPRODUCTNO, c.getString(c.getColumnIndexOrThrow(LOCALPRODUCTNO)));
                    j.put(PRODUCTQTY, c.getString(c.getColumnIndexOrThrow(PRODUCTQTY)));
                    j.put(SALEPRICE, c.getString(c.getColumnIndexOrThrow(SALEPRICE)));
                    j.put(TAXRATE, c.getString(c.getColumnIndexOrThrow(TAXRATE)));
                    j.put(NETSALEPRICE, c.getString(c.getColumnIndexOrThrow(NETSALEPRICE))) ;
                    j.put(TAX_AMOUNT, c.getString(c.getColumnIndexOrThrow(TAX_AMOUNT))) ;
                    j.put(WH, c.getString(c.getColumnIndexOrThrow(WH))) ;
                    j.put(SUBTOTAL, c.getString(c.getColumnIndexOrThrow(SUBTOTAL))) ;
                    j.put(SALE_REFERENCE, c.getString(c.getColumnIndexOrThrow(SALE_REFERENCE))) ;
                    j.put(DISCOUNTPRICE, c.getString(c.getColumnIndexOrThrow(DISCOUNTPRICE))) ;
                    j.put(TIMESTAMP, c.getString(c.getColumnIndexOrThrow(TIMESTAMP))) ;
                    j.put(ITEM_TAX, c.getString(c.getColumnIndexOrThrow(ITEM_TAX))) ;
                    j.put(DELSTATUS, c.getString(c.getColumnIndexOrThrow(DELSTATUS))) ;
                    j.put(SALE_TYPE, c.getString(c.getColumnIndexOrThrow(SALE_TYPE))) ;
                    jarr.put(j);
                } while(c.moveToNext()) ;
                if(jarr.length() == c.getCount()) {
                    jsonObject.put(ARRAYNAME, jarr);
                    System.out.println(jsonObject.toString());
                    array = js.getJson(Constants.SERVER, jsonObject);
                    JSONObject job = new JSONObject(array);
                    return job;
                }
            }
        }finally {
            if(c != null) {
                c.close();
            }
        }
        return null ;
    }


    public static JSONObject sendReturnitemsJson(Context context) throws JSONException
    {
        updateSaleItems(context);
        ContentResolver cr = context.getContentResolver() ;
        String where = ACK + " = 2" ;
        Cursor c = cr.query(BASEURI, null, where, null, null);
        if(c.getCount() == 0){
            c.close();
            return null;
        }
         js = new UrlCon();
        try {
            JSONObject jsonObject = new JSONObject();
            jsonObject.put("tag", Constants.TAGSSENDRETURNITEM);
            jsonObject.put(Constants.IMEI, Constants.getIMEINO(context));
            String array ;
            if(c.moveToFirst())
            {
                JSONArray jarr = new JSONArray();
                do{
                    JSONObject j = new JSONObject();
                    String ref = c.getString(c.getColumnIndexOrThrow(PRODUCTNO));
                    if(ref.equals("0"))
                        continue;
                    String ord = c.getString(c.getColumnIndexOrThrow(ORDERID)) ;
                    String cusid = Sales.getCustomer(context,ord);
                    if(cusid.equals("0"))
                        continue;

                    j.put(PRODUCTNO, ref);
                    j.put(ORDERID, ord);
                    j.put(ID, c.getString(c.getColumnIndexOrThrow(ID)));
                    j.put(LOCALPRODUCTNO, c.getString(c.getColumnIndexOrThrow(LOCALPRODUCTNO)));
                    j.put(PRODUCTQTY, c.getString(c.getColumnIndexOrThrow(PRODUCTQTY)));
                    j.put(SALEPRICE, c.getString(c.getColumnIndexOrThrow(SALEPRICE)));
                    j.put(TAXRATE, c.getString(c.getColumnIndexOrThrow(TAXRATE)));
                    j.put(NETSALEPRICE, c.getString(c.getColumnIndexOrThrow(NETSALEPRICE))) ;
                    j.put(TAX_AMOUNT, c.getString(c.getColumnIndexOrThrow(TAX_AMOUNT))) ;
                    j.put(WH, c.getString(c.getColumnIndexOrThrow(WH))) ;
                    j.put(SUBTOTAL, c.getString(c.getColumnIndexOrThrow(SUBTOTAL))) ;
                    j.put(SALE_REFERENCE, c.getString(c.getColumnIndexOrThrow(SALE_REFERENCE))) ;
                    j.put(DISCOUNTPRICE, c.getString(c.getColumnIndexOrThrow(DISCOUNTPRICE))) ;
                    j.put(TIMESTAMP, c.getString(c.getColumnIndexOrThrow(TIMESTAMP))) ;
                    j.put(ITEM_TAX, c.getString(c.getColumnIndexOrThrow(ITEM_TAX))) ;
                    j.put(DELSTATUS, c.getString(c.getColumnIndexOrThrow(DELSTATUS))) ;
                    j.put(SALEITEMID, c.getString(c.getColumnIndexOrThrow(SALEITEMID))) ;
                    j.put(PRODUCTQTYDEL, c.getString(c.getColumnIndexOrThrow(PRODUCTQTYDEL))) ;
                    jarr.put(j);
                } while(c.moveToNext()) ;
                if(jarr.length() == c.getCount()) {
                    jsonObject.put(ARRAYNAME, jarr);
                    System.out.println(jsonObject.toString());
                    array = js.getJson(Constants.SERVER, jsonObject);
                    JSONObject job = new JSONObject(array);
                    return job;
                }
            }
        }finally {
            if(c != null) {
                c.close();
            }
        }
        return null ;
    }

    /*
	 * Fetching stock for a new product giving out imei of the device
	 */
    public static JSONObject getSalesItemJson(Context context) throws JSONException
    {
        JSONObject jobs = new JSONObject();
        jobs.put("tag", Constants.TAGSGETSALEITEM);
        jobs.put(Constants.IMEI, Constants.getIMEINO(context));
         js = new UrlCon();
        String job =  js.getJson(Constants.SERVER, jobs);
        JSONObject json;
        try {
            json = new JSONObject(job);
            if(json.length() != 0)
            {
                return json ;
            }
        } catch (JSONException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        return null ;
    }

    public static boolean updateItem(Context context, String[] vals)
    {
        ContentResolver cr = context.getContentResolver();
        ContentValues values = new ContentValues() ;
        values.put(ACK, 2);
        values.put(DELSTATUS, vals[2]);
        values.put(PRODUCTQTYDEL, vals[1]);
        String where = ID + " = "+ vals[0] ;
        return cr.update(BASEURI, values, where, null) > 0;
    }

    public static boolean updateAllForReturn(Context context, long ordrid)
    {
        ContentResolver cr = context.getContentResolver();
        ContentValues values = new ContentValues() ;
        values.put(ACK, 2);
        String where = ORDERID + " = "+ ordrid ;
        return cr.update(BASEURI, values, where, null) > 0;
    }
    public static boolean updateItemRollBack(Context context, long vals) {
        ArrayList<String> qtys = getallqty(context, vals);
        boolean ret = false;
        int status = 0;
        for (int i = 0; i < qtys.size(); i++) {
            ContentResolver cr = context.getContentResolver();
            ContentValues values = new ContentValues();
            values.put(DELSTATUS, Constants.SALE_STATUS_PNDG);
            values.put(PRODUCTQTYDEL, qtys.get(i));
            String where = ORDERID + " = " + vals;
            System.out.println("query " + values.toString());
            if( cr.update(BASEURI, values, where, null) > 0)
                status += 1;
        }
        if(status == qtys.size())
            ret = true;

        return ret;
    }
    public static boolean rollQtyBack(Context context, String[] vals) {

        boolean ret = false;

        ContentResolver cr = context.getContentResolver();
        ContentValues values = new ContentValues();
        values.put(DELSTATUS, Constants.SALE_STATUS_PNDG);
        values.put(PRODUCTQTYDEL, vals[1]);
        String where = ID+" = "+vals[0] ;
        System.out.println("query " + values.toString());
        if( cr.update(BASEURI, values, where, null) > 0)
            ret = true;

         return ret;
    }

    private static ArrayList<String> getallqty(Context context, long id){
        ArrayList<String> ret = new ArrayList<>();
        ContentResolver cr = context.getContentResolver();
        String where = ORDERID +" = "+id;
        Cursor c = cr.query(BASEURI,new String[]{PRODUCTQTY},where,null,ID+" ASC");
        try{
            if(c.moveToFirst()){
                do{
                    String qty = c.getString(c.getColumnIndex(PRODUCTQTY));
                    ret.add(qty);
                } while (c.moveToNext());
            }
        } finally {
            if(c != null){
                c.close();
            }
        }
        return ret;
    }


    public static boolean checkReject(Context context, long id, int action)
    {
        ContentResolver cr = context.getContentResolver();
        boolean ret = false;
        String where = null;
        if(action ==0)
            where = ORDERID +" = "+id+" AND "+DELSTATUS+" = "+DatabaseUtils.sqlEscapeString(Constants
                .SALE_STATUS_REJT);
        if(action ==1)
            where = ORDERID +" = "+id+" AND "+DELSTATUS+" = "+DatabaseUtils.sqlEscapeString(Constants
                .SALE_STATUS_PNDG);

        Cursor c = cr.query(BASEURI, null, where, null, null);
        try {
            if (c.moveToFirst()) {
                ret = true ;
            }
        } finally {
            if (c != null) {
                c.close();
            }
        }
        return ret;
    }

    private static boolean isSaleItemPresent(Context context, String custid)
    {
        ContentResolver cr = context.getContentResolver();
        boolean ret = false;
        String where = TIMESTAMP +" = "+ DatabaseUtils.sqlEscapeString(custid);
        Cursor c = cr.query(BASEURI, null, where, null, null);
        try {
            if (c.moveToFirst()) {
                ret = true ;
            }
        } finally {
            if (c != null) {
                c.close();
            }
        }
        return ret;
    }


    public static void sendAck(Context context, String[] requiredid) throws JSONException
    {
        JSONObject jobs = new JSONObject();
        jobs.put("tag", Constants.TAGACK);
        jobs.put(Constants.ACK, Constants.ACKVALUE);
        jobs.put(Constants.IMEI, Constants.getIMEINO(context));
        jobs.put(Constants.ACKREFERENCE, requiredid[0]);
        jobs.put(Constants.ACKKEY, SALE_REFERENCE);
         js = new UrlCon();
        js.getJson(Constants.SERVER, jobs);
    }

    public static boolean updateSync(Context context, int orderid, int flag,String sysref)
    {
        ContentResolver cr = context.getContentResolver();
        ContentValues values = new ContentValues() ;
        values.put(ACK, flag);
        values.put(SALEITEMID, sysref);
        String where = ORDERID + " = "+ orderid ;
        return cr.update(BASEURI, values, where, null) > 0;
    }


    public static boolean updateWh(Context context,String wh)
    {
        ContentResolver cr = context.getContentResolver();
        ContentValues values = new ContentValues() ;
        values.put(WH, wh);
        return cr.update(BASEURI, values, null, null) > 0;
    }


    public static boolean updateprod(Context context, String[] prod)
    {
        ContentResolver cr = context.getContentResolver();
        ContentValues values = new ContentValues() ;
        values.put(PRODUCTNO, prod[0]);
        String where = LOCALPRODUCTNO + " = "+ prod[1] ;
        return cr.update(BASEURI, values, where, null) > 0;
    }

    public static ArrayList<StockIssueItems> getItems(Context context, long tr){
        ArrayList<StockIssueItems> list = new ArrayList<>();
        ContentResolver cr = context.getContentResolver();
        String where =  ORDERID+" = "+ tr;
        Cursor c = cr.query(BASEURI,null,where,null,ID+" ASC");
        try{
            if(c != null && c.getCount() > 0){
                c.moveToFirst();
                do {
                    long dt = c.getLong(c.getColumnIndex(LOCALPRODUCTNO));
                    String wh = c.getString(c.getColumnIndex(WH));
                    int qty = c.getInt(c.getColumnIndex(PRODUCTQTY));
                    long order = tr;
                    long i = c.getLong(c.getColumnIndex(ID));
                    int a = c.getInt(c.getColumnIndex(ACK));
                    list.add(new StockIssueItems(qty,wh,dt,order,a,i));
                } while (c.moveToNext());
            }
        } finally {
            if(c != null){
                c.close();
            }
        }
        return list;
    }

    public StockIssueItems(int qty, String wh, long prodloc, long orderid, int ack, long id) {
        this.qty = qty;
        this.wh = wh;
        this.prodloc = prodloc;
        this.orderid = orderid;
        this.ack = ack;
        this.id = id;
    }

    public int getQtydel() {
        return qtydel;
    }

    public int getAck() {
        return ack;
    }

    public int getQty() {
        return qty;
    }

    public double getTaxitem() {
        return taxitem;
    }

    public double getDiscount() {
        return discount;
    }

    public double getTaxrt() {
        return taxrt;
    }

    public double getSbtotal() {
        return sbtotal;
    }

    public double getTaxmt() {
        return taxmt;
    }

    public double getPrice() {
        return price;
    }

    public double getNetprice() {
        return netprice;
    }

    public String getWh() {
        return wh;
    }

    public String getCust() {
        return cust;
    }

    public String getProd() {
        return prod;
    }

    public long getProdloc() {
        return prodloc;
    }

    public long getOrderid() {
        return orderid;
    }

    public long getId() {
        return id;
    }
}
