/*
 * The source codes in this project are owned by Bongolive Enterprises. Copying or any kind of usage without Bongolive's approval is against the law.
 * Copyright (c) 2015.
 *
 */

package bongolive.apps.proshop.model;

import android.content.ContentResolver;
import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.DatabaseUtils;
import android.net.Uri;

import com.jjoe64.graphview.series.DataPoint;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.ArrayList;

import bongolive.apps.proshop.controller.Validating;
import bongolive.apps.proshop.controller.Constants;
import bongolive.apps.proshop.controller.UrlCon;


public class Delivery {
    public static final String TABLENAME = "delivery" ;
    public static final String ID = "_id";
    public static final String CUSTOMERID = "customer_id";// required to send system_customer_id
    public static final String LOCALCUSTOMER = "loc_customer_id";// required to send system_customer_id
    public static final String DELIVERYDATE = "date_to_deliver";
    public static final String DELIVERYSTATUS ="status" ;
    public static final String DELIVEREDDATE ="date_delivered" ;
    public static final String COMMENT ="note" ;
    public static final String VEHICLENO = "vehicle_no" ;
    public static final String UPDATEDDATE = "date_update" ;
    public static final String DELIVERYMETHOD = "delivery_method" ;
    public static final String LOCALSALE = "loc_sale_id" ;
    public static final String SYS_SALE = "sale_id";
    public static final String SALE_REFERENCE = "sale_reference_no";
    public static final String ADDRESS = "address";
    public static final String ACK = "ack" ;
    public static final String LAT = "lat";
    public static final String LONGTUDE = "lng";
    public static final String CREATED = "created_at";
    public static final String REFRENCENO = "do_reference_no";
    public static final String DELID = "id";
    public static final String SIGNATURE = "signature";
    private static UrlCon js ;

    String customer,customerlocal,deldate,delstatus,delivereddt,note,vhno,delmethod,localsale,syssale,saleref,
            address,ack,delref,delid,sign;
    long id;
    double lat,lng;
    public static final Uri BASEURI = Uri.parse("content://"+ ContentProviderApi.AUTHORITY+"/"+TABLENAME) ;
    public static final String SPECIFIC_CONTENT_TYPE = "vnd.android.cursor.item/vnd.com.proshop.delivery" ;
    public static final String GENERAL_CONTENT_TYPE = "vnd.android.cursor.dir/vnd.com.proshop.delivery" ;
    private static final String ARRAYNAME = "deliveries";

    public static boolean insert(Context context, String[] orders, String source){
        int ordercount = getCount(context) ;
        ContentResolver cr = context.getContentResolver() ;
        ContentValues cv = new ContentValues() ;
        if(source.equals(Constants.REMOTE)) {
            cv.put(CUSTOMERID, orders[0]);
            cv.put(DELIVERYDATE, orders[1]);
            cv.put(DELIVEREDDATE, orders[2]);
            cv.put(VEHICLENO, orders[3]);
            cv.put(ADDRESS, orders[4]);
            cv.put(ACK, 1);
            cv.put(DELIVERYMETHOD, orders[5]);
            cv.put(LOCALSALE, orders[6]);
            cv.put(SYS_SALE, orders[7]);
            cv.put(SALE_REFERENCE, orders[8]);
            cv.put(LAT, orders[9]);
            cv.put(LONGTUDE, orders[10]);//comm,taxsett,isprint,paystat,loca
            cv.put(COMMENT, orders[11]);
            cv.put(DELID, orders[12]);
            cv.put(REFRENCENO, orders[13]);
            cv.put(DELIVERYSTATUS, orders[14]);
            cv.put(LOCALCUSTOMER, orders[15]);
            cv.put(UPDATEDDATE, orders[16]);
        } else if(source.equals(Constants.LOCAL)) {
            cv.put(CUSTOMERID, orders[0]);
            cv.put(DELIVERYDATE, Constants.getDate());
            cv.put(DELIVEREDDATE, orders[1]);
            double ts = new BigDecimal(orders[2]).subtract(new BigDecimal(orders[3])).setScale(2, RoundingMode
                    .HALF_UP).doubleValue();
            cv.put(VEHICLENO, ts);
            cv.put(ADDRESS, orders[3]);
            cv.put(DELIVERYMETHOD, orders[4]);
            cv.put(LOCALSALE, orders[5]);
            cv.put(SYS_SALE, 0);
            cv.put(SALE_REFERENCE, orders[6]);
            cv.put(LAT, orders[12]);
            System.out.println(orders[12] + " ," + orders[13]);
            cv.put(LONGTUDE, orders[13]);
            cv.put(COMMENT, orders[7]);
            cv.put(DELID, orders[8]);
            cv.put(REFRENCENO, orders[9]);
            cv.put(DELIVERYSTATUS, orders[10]);
            cv.put(LOCALCUSTOMER, orders[11]);
            cv.put(UPDATEDDATE, orders[2]);
            System.out.println("sale status " + orders[14]);
        }
        cr.insert(BASEURI, cv);
        return getCount(context) == ordercount + 1;
    }
    //gettotaltax,getpayamount
    public static int getCount(Context context){
        ContentResolver cr = context.getContentResolver() ;
        Cursor _c = cr.query(BASEURI, null, null, null, null);
        int count = _c.getCount() ;
        _c.close() ;
        return count;
    }

    private static ArrayList<String> getIds(Context context){
        ArrayList<String> lst = new ArrayList<>();
        ContentResolver cr = context.getContentResolver() ;
        String where = CUSTOMERID +" = 0" ;
        Cursor c = cr.query(BASEURI, new String[]{" DISTINCT "+LOCALCUSTOMER}, where, null, null);
        try {
            if (c.moveToFirst()) {
                do {
                    lst.add(c.getString(c.getColumnIndexOrThrow(LOCALCUSTOMER)));
                } while (c.moveToNext());
                return lst;
            }
        } finally {
            if(c!= null)
                c.close();
        }
        return  null;
    }


    public static String getLocalSaleId(Context context, String str) {
        ContentResolver cr = context.getContentResolver();
        String where = SALE_REFERENCE + " = " + DatabaseUtils.sqlEscapeString(str);

        Cursor c = cr.query(BASEURI, new String[]{ID}, where, null, null);
        try {
            String cu = null;
            if (c.moveToFirst()) {
                cu = c.getString(c.getColumnIndexOrThrow(ID));
                return cu;
            }
        } finally {
            if (c != null) {
                c.close();
            }
        }
        return "0";
    }

    private static void updateSaleItems(Context context){
        ArrayList<String> lst = getIds(context);
        ContentResolver cr = context.getContentResolver();

        if(lst == null || lst.size() == 0)
            return;
        for(int i = 0; i < lst.size(); i++ ) {
            ContentValues cv = new ContentValues();
            String prod = Customers.getSysCustidBasedOnLocalId(context, lst.get(i));
            cv.put(CUSTOMERID, prod);
            String where = LOCALCUSTOMER + " = " + lst.get(i)+" AND "+CUSTOMERID+" = 0";

            System.out.println( "updating "+cr.update(BASEURI, cv, where, null));
            System.out.println("updated "+lst.get(i));
        }
    }
    /*
         * Fetching sales from server if sent from this device
         */
    public static JSONObject getDeliveryJson(Context context) throws JSONException
    {
        JSONObject jobs = new JSONObject();
        jobs.put("tag", Constants.TAGSRECEIVEDELIVERY);
        jobs.put(Constants.IMEI, Constants.getIMEINO(context));
        js = new UrlCon();
        String job =  js.getJson(Constants.SERVER, jobs) ;
        JSONObject json;
        try {
            json = new JSONObject(job);
            if(json != null)
            {
                return json ;
            }
        } catch (JSONException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        return null ;
    }


    public static JSONObject sendDeliveryJson(Context context) throws JSONException
    {
        updateSaleItems(context);
        ContentResolver cr = context.getContentResolver() ;
        String where = ACK + " = 2 AND "+DELIVERYSTATUS +" != "+DatabaseUtils.sqlEscapeString(Constants
                .SALE_STATUS_PNDG) ;
        Cursor c = cr.query(BASEURI, null, where, null, null);
        if(c.getCount() == 0){
            c.close();
            return null;
        }
        js = new UrlCon();
        try {
            JSONObject jsonObject = new JSONObject();
            jsonObject.put("tag", Constants.TAGSSENDDELIVERY);
            jsonObject.put(Constants.IMEI, Constants.getIMEINO(context));
            String array ;
            if(c.moveToFirst())
            {
                JSONArray jarr = new JSONArray();
                do{
                    JSONObject j = new JSONObject();
                    String custmerid = c.getString(c.getColumnIndexOrThrow(CUSTOMERID));
                    if(custmerid.equals("0"))
                        continue;
                    j.put(CUSTOMERID, custmerid);
                    j.put(DELIVERYDATE, c.getString(c.getColumnIndexOrThrow(DELIVERYDATE)));
                    j.put(DELIVEREDDATE, c.getString(c.getColumnIndexOrThrow(DELIVEREDDATE)));
                    j.put(VEHICLENO, c.getString(c.getColumnIndexOrThrow(VEHICLENO)));
                    j.put(UPDATEDDATE, c.getString(c.getColumnIndexOrThrow(UPDATEDDATE)));
                    j.put(ADDRESS, "c.getString(c.getColumnIndexOrThrow(ADDRESS))");
                    j.put(ACK, c.getString(c.getColumnIndexOrThrow(ACK)));
                    j.put(DELIVERYMETHOD, c.getString(c.getColumnIndexOrThrow(DELIVERYMETHOD)));
                    j.put(LOCALSALE, c.getString(c.getColumnIndexOrThrow(LOCALSALE)));
                    j.put(SALE_REFERENCE, c.getString(c.getColumnIndexOrThrow(SALE_REFERENCE)));
                    j.put(LAT, c.getString(c.getColumnIndexOrThrow(LAT)));
                    j.put(LONGTUDE, c.getString(c.getColumnIndexOrThrow(LONGTUDE)));
                    j.put(COMMENT, "c.getString(c.getColumnIndexOrThrow(COMMENT))");
                    j.put(DELIVERYSTATUS, c.getString(c.getColumnIndexOrThrow(DELIVERYSTATUS)));
                    j.put(ID, c.getString(c.getColumnIndexOrThrow(ID)));
                    j.put(DELID, c.getString(c.getColumnIndexOrThrow(DELID)));
                    j.put(REFRENCENO, c.getString(c.getColumnIndexOrThrow(REFRENCENO)));
                    j.put(SYS_SALE, c.getString(c.getColumnIndexOrThrow(SYS_SALE)));
                    j.put(SIGNATURE, c.getString(c.getColumnIndexOrThrow(SIGNATURE)));
                    jarr.put(j);
                } while(c.moveToNext()) ;
                jsonObject.put(ARRAYNAME, jarr);
                System.out.println(jsonObject.toString());
                array = js.getJson(Constants.SERVER, jsonObject) ;
                JSONObject job =  new JSONObject(array);
                return job ;
            }
        }finally {
            if(c != null) {
                c.close();
            }
        }
        return null ;
    }


    public static JSONObject sendDeliveryJson2(Context context) throws JSONException
    {
        updateSaleItems(context);
        ContentResolver cr = context.getContentResolver() ;
        String where = ACK + " = 2 AND "+DELIVERYSTATUS +" != "+DatabaseUtils.sqlEscapeString(Constants
                .SALE_STATUS_PNDG) ;
        Cursor c = cr.query(BASEURI, null, where, null, null);
        if(c.getCount() == 0){
            c.close();
            return null;
        }
        js = new UrlCon();
        try {
            JSONObject jsonObject = new JSONObject();
            jsonObject.put("tag", Constants.TAGSSENDDELIVERY);
            jsonObject.put(Constants.IMEI, Constants.getIMEINO(context));
            String array ;
            if(c.moveToFirst())
            {
                JSONArray jarr = new JSONArray();
                do{
                    JSONObject j = new JSONObject();
                    String custmerid = c.getString(c.getColumnIndexOrThrow(CUSTOMERID));
                    if(custmerid.equals("0"))
                        continue;
                    j.put(CUSTOMERID, custmerid);
                    j.put(DELIVERYDATE, c.getString(c.getColumnIndexOrThrow(DELIVERYDATE)));
                    j.put(DELIVEREDDATE, c.getString(c.getColumnIndexOrThrow(DELIVEREDDATE)));
                    j.put(VEHICLENO, c.getString(c.getColumnIndexOrThrow(VEHICLENO)));
                    j.put(UPDATEDDATE, c.getString(c.getColumnIndexOrThrow(UPDATEDDATE)));
                    j.put(ADDRESS, c.getString(c.getColumnIndexOrThrow(ADDRESS)));
                    j.put(ACK, c.getString(c.getColumnIndexOrThrow(ACK)));
                    j.put(DELIVERYMETHOD, c.getString(c.getColumnIndexOrThrow(DELIVERYMETHOD)));
                    j.put(LOCALSALE, c.getString(c.getColumnIndexOrThrow(LOCALSALE)));
                    j.put(SALE_REFERENCE, c.getString(c.getColumnIndexOrThrow(SALE_REFERENCE)));
                    j.put(LAT, c.getString(c.getColumnIndexOrThrow(LAT)));
                    j.put(LONGTUDE, c.getString(c.getColumnIndexOrThrow(LONGTUDE)));
                    j.put(COMMENT, c.getString(c.getColumnIndexOrThrow(COMMENT)));
                    j.put(DELIVERYSTATUS, c.getString(c.getColumnIndexOrThrow(DELIVERYSTATUS)));
                    j.put(ID, c.getString(c.getColumnIndexOrThrow(ID)));
                    j.put(DELID, c.getString(c.getColumnIndexOrThrow(DELID)));
                    j.put(REFRENCENO, c.getString(c.getColumnIndexOrThrow(REFRENCENO)));
                    j.put(SYS_SALE, c.getString(c.getColumnIndexOrThrow(SYS_SALE)));
                    j.put(SIGNATURE, c.getString(c.getColumnIndexOrThrow(SIGNATURE)));
                    jarr.put(j);
                } while(c.moveToNext()) ;
                jsonObject.put(ARRAYNAME, jarr);
                System.out.println(jsonObject.toString());
                array = js.getJson(Constants.SERVER, jsonObject) ;
                JSONObject job =  new JSONObject(array);
                return job ;
            }
        }finally {
            if(c != null) {
                c.close();
            }
        }
        return null ;
    }

    public static void processInformation(Context context) throws JSONException{

        JSONObject jsonin = getDeliveryJson(context);
        if(jsonin != null)
        try {
            if(jsonin.has(Constants.SUCCESS)){
                String res = jsonin.getString(Constants.SUCCESS) ;
                if(Integer.parseInt(res) == 1)
                {
                    if(jsonin.has(ARRAYNAME)) {
                        JSONArray jsarray = jsonin.getJSONArray(ARRAYNAME);
                        int success = 0;
                        for (int i = 0; i < jsarray.length(); i++) {
                            JSONObject j = jsarray.getJSONObject(i);
                            String custid = j.getString(CUSTOMERID);
                            String deldate = j.getString(DELIVERYDATE);
                            String delvddate = j.getString(DELIVEREDDATE);
                            String vehicleno = j.getString(VEHICLENO);
                            String updatedate = j.getString(UPDATEDDATE);
                            String address = j.getString(ADDRESS);
                            String delmethod = j.getString(DELIVERYMETHOD);
                            String sysid = j.getString(SYS_SALE);
                            if(sysid.equals("0"))
                                continue;

                            String locsaleid = Sales.getLocalSale(context, sysid);
                            if(locsaleid.equals("0")) {
                                System.out.println("there is no this sale on device");
                                continue;
                            }

                            String saleref = j.getString(SALE_REFERENCE);
                            String lat = j.getString(LAT);
                            if(!Validating.areSet(lat))
                                lat = "0";
                            String lng = j.getString(LONGTUDE);
                            if(!Validating.areSet(lng))
                                lng = "0";
                            String note = j.getString(COMMENT);
                            String delstatus = j.getString(DELIVERYSTATUS);
                            String ref = j.getString(REFRENCENO);
                            String delid = j.getString(DELID);
                            String localcus = "0";

                            if (Validating.areSet(custid)) {
                                System.out.println("custid " + custid);
                                boolean cus = Customers.isCustomerExisting(context, custid);
                                if (cus) {
                                    localcus = Customers.getCustomerlocalByCustomerRemot(context, custid);
                                }
                            }
                            System.out.println("local cus " + localcus);
                            if (localcus.equals("0"))
                                continue;
                            String[] vals = {custid, deldate, delvddate, vehicleno, address, delmethod, locsaleid, sysid,
                                    saleref,
                                    lat,  lng, note,delid,ref, delstatus, localcus,updatedate};
                            String[] check = {custid, deldate, locsaleid, sysid, saleref,delid,ref, delstatus, localcus};
                            if (Validating.areSet(check)) {
                                boolean var = isSalePresentFromSystem(context, new String[]{delid,sysid});
                                System.out.println("present? " + var);

                                if (!var) {
                                    if (insert(context, vals, Constants.REMOTE)) {
                                        success += 1;
                                    }
                                } else {
                                    success += 1;
                                }
                            }
                        }
                        if (success == jsarray.length() && jsarray.length() >0)
                            sendAckJson(context);
                    }
                }
            }
        } catch (NumberFormatException e) {
            e.printStackTrace();
        } catch (JSONException e) {
            e.printStackTrace();
        }


        JSONObject jsonout = sendDeliveryJson(context);
        if(jsonout != null)
            try {
                if(jsonout.has(Constants.SUCCESS)){
                    String res = jsonout.getString(Constants.SUCCESS) ;
                    if(Integer.parseInt(res) == 1)
                    {
                        if(jsonout.has(ARRAYNAME)){
                            JSONArray jsonArray = jsonout.getJSONArray(ARRAYNAME);
                            for(int i = 0; i< jsonArray.length(); i++) {
                                JSONObject jsonObject1 = jsonArray.getJSONObject(i);
                                String refno = jsonObject1.getString(REFRENCENO);
                                String[] v = {"1",refno};
                                if(Validating.areSet(v)) {
                                    boolean OK = updateSync(context, v);
                                    if (OK) {
                                        System.out.println("updated delivery "+refno);
                                    }
                                }
                            }
                        }
                    }
                }
            } catch (NumberFormatException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            } catch (JSONException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }

    }

    /* check if the sale id is present in the app
    *
    */

    private static boolean isSalePresentFromSystem(Context context, String custid)
    {
        ContentResolver cr = context.getContentResolver();
        boolean ret = false;
        String where = SYS_SALE +" = "+custid;
        Cursor c = cr.query(BASEURI, null, where, null, null);
        try {
            if (c.moveToFirst()) {
                ret = true ;
            }
        } finally {
            if (c != null) {
                c.close();
            }
        }
        return ret;
    }
    private static boolean isSalePresentFromSystem(Context context, String[] custid)
    {
        ContentResolver cr = context.getContentResolver();
        boolean ret = false;
        String where = DELID +" = "+custid[0]+" AND "+SYS_SALE +" = "+custid[1];
        Cursor c = cr.query(BASEURI, null, where, null, null);
        try {
            if (c.moveToFirst()) {
                ret = true ;
            }
        } finally {
            if (c != null) {
                c.close();
            }
        }
        return ret;
    }
    public static String getSaleReference(Context context, long custid)
    {
        ContentResolver cr = context.getContentResolver();
        String ref = null;
        String where = LOCALSALE +" = "+custid;
        System.out.println(where+" where ");
        Cursor c = cr.query(BASEURI, null, where, null, null);
        try {
            if (c.moveToFirst()) {
                ref = c.getString(c.getColumnIndex(REFRENCENO)) ;
                System.out.println(ref);
            }
        } finally {
            if (c != null) {
                c.close();
            }
        }
        return ref;
    }
    /*
    * Acknowledgement to the server to tick item as a go
    */
    public static void sendAck(Context context, String[] requiredid) throws JSONException
    {
        JSONObject jobs = new JSONObject();
        jobs.put("tag", Constants.TAGACK);
        jobs.put(Constants.ACK, Constants.ACKVALUE);
        jobs.put(Constants.IMEI, Constants.getIMEINO(context));
        jobs.put(Constants.ACKREFERENCE, requiredid[0]);
        jobs.put(Constants.ACKKEY, SYS_SALE);
        js = new UrlCon();
        js.getJson(Constants.SERVER, jobs) ;
    }

    public static boolean updateSync(Context context, String[] vals)
    {
        ContentResolver cr = context.getContentResolver();
        ContentValues values = new ContentValues() ;
        values.put(ACK, vals[0]);
        String where =  REFRENCENO+ " = "+ DatabaseUtils.sqlEscapeString(vals[1]) ;
        return cr.update(BASEURI, values, where, null) == 1;
    }

    public static boolean updateDelivery(Context context, String[] vals)
    {
        ContentResolver cr = context.getContentResolver();
        ContentValues values = new ContentValues() ;
        values.put(DELIVERYSTATUS, vals[1]);
        values.put(UPDATEDDATE, Constants.getDate());
        values.put(DELIVEREDDATE, Constants.getDate());
        values.put(ACK, 2);
        String where =  REFRENCENO+ " = "+ DatabaseUtils.sqlEscapeString(vals[0]) ;
        return cr.update(BASEURI, values, where, null) > 0;
    }

    public static int isOrderSynced(Context context, String businessnm)
    {
        ContentResolver cr = context.getContentResolver();
        String where = ID + " = " + businessnm ;

        Cursor c = cr.query(BASEURI, new String[] {ACK}, where,null,null) ;
        try {
            int i= 0 ;
            if(c.moveToFirst())
            {
                i = c.getInt(c.getColumnIndexOrThrow(ACK));
                return i ;
            }
        } finally {
            if(c != null) {
                c.close();
            }
        }
        return 0;
    }

    public static int getId(Context context) {
        ContentResolver cr = context.getContentResolver();
        Cursor c = cr.query(BASEURI, new String[] {ID}, null,null,null) ;
        try {
            int i= 0 ;
            if(c.moveToLast())
            {
                i = c.getInt(c.getColumnIndexOrThrow(ID));
                return i ;
            }
        }finally {
            if(c != null) {
                c.close();
            }
        }
        return 0 ;
    }



    public static void sendAckJson(Context context) throws JSONException {
        JSONObject jobs = new JSONObject();
        jobs.put("tag", Constants.TAGACK);
        jobs.put(Constants.ACKITEM, TABLENAME);
        jobs.put(Constants.IMEI, Constants.getIMEINO(context));
        js = new UrlCon();
        System.out.println(jobs.toString());
        js.getJson(Constants.SERVER, jobs) ;
    }

    public static String getCustomer(Context context, String ordid) {
        String where = ID +" = " +ordid;
        ContentResolver cr = context.getContentResolver();
        Cursor c = cr.query(BASEURI, new String[] {CUSTOMERID}, where,null,null) ;
        try {
            String i= "0" ;
            if(c.moveToFirst())
            {
                i = c.getString(c.getColumnIndexOrThrow(CUSTOMERID));
                return i ;
            }
        }finally {
            if(c != null) {
                c.close();
            }
        }
        return "0" ;
    }

    public static double getDailySummary(Context context, int flag){
        ContentResolver cr = context.getContentResolver() ;
        String where = null ;
        switch(flag)
        {
            case 0:
                where = DELIVERYDATE + " >=  date('now')";

                break;
            case 1:
                where = DELIVERYDATE + " >= date('now','-7 day')";
                //date('now','start of month','+0 month','0 day') AND "+ DELIVERYDATE + " <= date('now','start of month','+0 month','-1 day')";
                break;
            case 2:
                where = DELIVERYDATE + " >=  date('now','start of month','-1 month') AND "+
                        DELIVERYDATE + " < date('now','start of month','-1 day')";
                //date('now','start of month','+1 month','-1 day')
                break;
        }
        Cursor c = cr.query(BASEURI, null, where, null, null);
        double total = 0;
        try{
            if(c.moveToFirst()){
                do{
                    total += c.getDouble(c.getColumnIndexOrThrow(DELIVEREDDATE));
                } while (c.moveToNext());
                return total ;
            }
        }finally{
            if(c != null) {
                c.close();
            }
        }
        return 0;
    }

    public static DataPoint[] getTotalSalesGraph(Context context){

        DataPoint[] vals = null;
        String query = " select sum(case  cast(strftime('%w',"+DELIVERYDATE+ ") as integer) when 0 then IFNULL("+UPDATEDDATE+"," +
                " 0)   else 0  end) as sunday ," +
                "sum(case  cast(strftime('%w',"+DELIVERYDATE+") as integer) when 1 then IFNULL("+UPDATEDDATE+",0)  else 0 end) as" +
                " monday ," +
                "sum(case  cast(strftime('%w',"+DELIVERYDATE+") as integer) when 2 then IFNULL("+UPDATEDDATE+",0)   else 0 end) as tuesday ," +
                "sum(case  cast(strftime('%w',"+DELIVERYDATE+") as integer) when 3 then IFNULL("+UPDATEDDATE+",0)   else 0 end) as wednesday ," +
                "sum(case  cast(strftime('%w',"+DELIVERYDATE+") as integer) when 4 then IFNULL("+UPDATEDDATE+",0)   else 0 end) as thursday ," +
                "sum(case  cast(strftime('%w',"+DELIVERYDATE+") as integer) when 5 then IFNULL("+UPDATEDDATE+",0)   else 0 end) as friday ," +
                "sum(case  cast(strftime('%w',"+DELIVERYDATE+") as integer) when 6 then IFNULL("+UPDATEDDATE+",0)   else 0 end) as saturday " +
                "from "+TABLENAME+" where  strftime('%W',"+DELIVERYDATE+") = strftime('%W','now') group by strftime('%W','now') ";
        final Cursor c1 = DatabaseHandler.getInstance(context).getReadableDatabase().rawQuery(query, null);
        if (c1 != null) {
            vals = new DataPoint[c1.getCount()];
            try {
                if(c1.moveToFirst()) {
                    double mon = 0, tues = 0, wed = 0, thur = 0, frd = 0, sat = 0, sun = 0;
                    mon = c1.getDouble(c1.getColumnIndex("monday"));
                    tues = c1.getDouble(c1.getColumnIndex("tuesday"));
                    wed = c1.getDouble(c1.getColumnIndex("wednesday"));
                    thur = c1.getDouble(c1.getColumnIndex("thursday"));
                    frd = c1.getDouble(c1.getColumnIndex("friday"));
                    sat = c1.getDouble(c1.getColumnIndex("saturday"));
                    sun = c1.getDouble(c1.getColumnIndex("sunday"));
                    vals = new DataPoint[]{new DataPoint(0, sun), new DataPoint(1, mon), new DataPoint(2, tues),
                            new DataPoint(3, wed), new DataPoint(4, thur), new DataPoint(5, frd), new DataPoint(6, sat)};
                }
            } finally {
                c1.close();
            }
        }
        return vals;
    }

    public static DataPoint[] getTotalOutstandingGraph(Context context){

        DataPoint[] vals = null;
        String query = " select sum(case  cast(strftime('%w',"+DELIVERYDATE+ ") as integer) when 0 then IFNULL("+DELIVEREDDATE+"," +
                " 0)   else 0  end) as sunday ," +
                "sum(case  cast(strftime('%w',"+DELIVERYDATE+") as integer) when 1 then IFNULL("+DELIVEREDDATE+",0)  else 0 end) as" +
                " monday ," +
                "sum(case  cast(strftime('%w',"+DELIVERYDATE+") as integer) when 2 then IFNULL("+DELIVEREDDATE+",0)   else 0 end) as tuesday ," +
                "sum(case  cast(strftime('%w',"+DELIVERYDATE+") as integer) when 3 then IFNULL("+DELIVEREDDATE+",0)   else 0 end) as wednesday ," +
                "sum(case  cast(strftime('%w',"+DELIVERYDATE+") as integer) when 4 then IFNULL("+DELIVEREDDATE+",0)   else 0 end) as thursday ," +
                "sum(case  cast(strftime('%w',"+DELIVERYDATE+") as integer) when 5 then IFNULL("+DELIVEREDDATE+",0)   else 0 end) as friday ," +
                "sum(case  cast(strftime('%w',"+DELIVERYDATE+") as integer) when 6 then IFNULL("+DELIVEREDDATE+",0)   else 0 end) as saturday " +
                "from "+TABLENAME+" where  strftime('%W',"+DELIVERYDATE+") = strftime('%W','now') group by strftime('%W','now') ";
        final Cursor c1 = DatabaseHandler.getInstance(context).getReadableDatabase().rawQuery(query, null);
        if (c1 != null) {
            vals = new DataPoint[c1.getCount()];
            try {
                if(c1.moveToFirst()) {
                    double mon = 0, tues = 0, wed = 0, thur = 0, frd = 0, sat = 0, sun = 0;
                    mon = c1.getDouble(c1.getColumnIndex("monday"));
                    tues = c1.getDouble(c1.getColumnIndex("tuesday"));
                    wed = c1.getDouble(c1.getColumnIndex("wednesday"));
                    thur = c1.getDouble(c1.getColumnIndex("thursday"));
                    frd = c1.getDouble(c1.getColumnIndex("friday"));
                    sat = c1.getDouble(c1.getColumnIndex("saturday"));
                    sun = c1.getDouble(c1.getColumnIndex("sunday"));
                    vals = new DataPoint[]{new DataPoint(0, sun), new DataPoint(1, mon), new DataPoint(2, tues),
                            new DataPoint(3, wed), new DataPoint(4, thur), new DataPoint(5, frd), new DataPoint(6, sat)};
                }
            } finally {
                c1.close();
            }
        }
        return vals;
    }


    public static int putDeliveryStatus(Context context, int[] data)
    {
        ContentResolver cr = context.getContentResolver();
        ContentValues values = new ContentValues() ;
        values.put(DELIVERYSTATUS, data[1]);

        String where =  ID+ " = "+ data[0] ;
        if(cr.update(BASEURI, values, where, null) > 0)
        {
            return 1 ;
        }
        return 0;
    }

    public static double getGrandTotal(Context context, long status) {
        ContentResolver cr = context.getContentResolver();
        String where = ID + " = " + status;
        Cursor c = cr.query(BASEURI, new String[] {VEHICLENO}, where,null,null) ;
        try {
            double i= 0 ;
            if(c.moveToFirst())
            {
                i = c.getDouble(c.getColumnIndexOrThrow(VEHICLENO));
                return i ;
            }
        }finally {
            if(c != null) {
                c.close();
            }
        }
        return 0 ;
    }

    public static ArrayList<String> deliveryDetails(Context context, long id){
        ArrayList<String> list = new ArrayList<>();
        ContentResolver cr = context.getContentResolver();
        String where = LOCALSALE + " = " + id;
        Cursor c = cr.query(BASEURI, null, where,null,null) ;
        try {
            double i= 0 ;
            if(c.moveToFirst())
            {
                list.add(0,c.getString(c.getColumnIndex(CUSTOMERID)));
                list.add(1,c.getString(c.getColumnIndex(DELIVERYDATE)));
                list.add(2,c.getString(c.getColumnIndex(SALE_REFERENCE)));
                list.add(3,c.getString(c.getColumnIndex(REFRENCENO)));
                list.add(4,c.getString(c.getColumnIndex(LAT)));
                list.add(5,c.getString(c.getColumnIndex(LONGTUDE)));
                list.add(6,c.getString(c.getColumnIndex(ADDRESS)));
                list.add(7,c.getString(c.getColumnIndex(COMMENT)));
            }
        }finally {
            if(c != null) {
                c.close();
            }
        }
        return list;
    }
    public static ArrayList<String> getprintdetails(Context context, long id){
        ArrayList<String> list = new ArrayList<>();
        ContentResolver cr = context.getContentResolver();
        String where = LOCALSALE + " = " + id;
        Cursor c = cr.query(BASEURI, null, where,null,null) ;
        try {
            double i= 0 ;
            if(c.moveToFirst())
            {
                list.add(Constants.getDate());
                list.add(c.getString(c.getColumnIndex(REFRENCENO)));
                list.add(c.getString(c.getColumnIndex(SALE_REFERENCE)));
                String custu = c.getString(c.getColumnIndex(CUSTOMERID));
                String cust = Customers.getCustomerBusinessName(context, custu);
                list.add(cust);
                list.add(c.getString(c.getColumnIndex(ADDRESS)));
                list.add(c.getString(c.getColumnIndex(COMMENT)));
            }
        }finally {
            if(c != null) {
                c.close();
            }
        }
        return list;
    }

    public Delivery(String customer, String customerlocal, String deldate, String delstatus, String delivereddt,
                    String note, String vhno, String delmethod, String localsale, String syssale, String saleref,
                    String address, String ack, String delref, String delid, String sign, long id, double lat, double lng) {
        this.customer = customer;
        this.customerlocal = customerlocal;
        this.deldate = deldate;
        this.delstatus = delstatus;
        this.delivereddt = delivereddt;
        this.note = note;
        this.vhno = vhno;
        this.delmethod = delmethod;
        this.localsale = localsale;
        this.syssale = syssale;
        this.saleref = saleref;
        this.address = address;
        this.ack = ack;
        this.delref = delref;
        this.delid = delid;
        this.sign = sign;
        this.id = id;
        this.lat = lat;
        this.lng = lng;
    }

    public String getCustomer() {
        return customer;
    }

    public String getCustomerlocal() {
        return customerlocal;
    }

    public String getDeldate() {
        return deldate;
    }

    public String getDelstatus() {
        return delstatus;
    }

    public String getDelivereddt() {
        return delivereddt;
    }

    public String getNote() {
        return note;
    }

    public String getVhno() {
        return vhno;
    }

    public String getDelmethod() {
        return delmethod;
    }

    public String getLocalsale() {
        return localsale;
    }

    public String getSyssale() {
        return syssale;
    }

    public String getSaleref() {
        return saleref;
    }

    public String getAddress() {
        return address;
    }

    public String getAck() {
        return ack;
    }

    public String getDelref() {
        return delref;
    }

    public String getDelid() {
        return delid;
    }

    public String getSign() {
        return sign;
    }

    public long getId() {
        return id;
    }

    public double getLat() {
        return lat;
    }

    public double getLng() {
        return lng;
    }
}
