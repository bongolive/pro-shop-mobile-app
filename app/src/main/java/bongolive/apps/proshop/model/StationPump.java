/*
 * The source codes in this project are owned by Bongolive Enterprises. Copying or any kind of usage without Bongolive's approval is against the law.
 * Copyright (c) 2015.
 *
 */

package bongolive.apps.proshop.model;

import android.content.ContentResolver;
import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.DatabaseUtils;
import android.database.sqlite.SQLiteDatabase;
import android.net.Uri;
import android.util.Log;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import bongolive.apps.proshop.R;
import bongolive.apps.proshop.controller.Constants;
import bongolive.apps.proshop.controller.UrlCon;


public class StationPump {
    public static final String TABLENAME = "station_pump";
    public static final String ID = "_id";
    public static final String SYS_ID = "id";
    ;
    public static final String NAME = "name";
    public static final String STATION = "station_local";
    public static final String ACK = "ack";

    public static final Uri BASEURI = Uri.parse("content://" + ContentProviderApi.AUTHORITY + "/" + TABLENAME);
    public static final String SPECIFIC_CONTENT_TYPE = "vnd.android.cursor.item/vnd.com.proshop.station_pump";
    public static final String GENERAL_CONTENT_TYPE = "vnd.android.cursor.dir/vnd.com.proshop.station_pump";
    public static final String ARRAYNAME = "station_pump";

    int id, customer, ack, status;
    double pay, totalsales, totaltax, totaldiscount;
    String date, paystatus;
    static UrlCon js = null;

    public StationPump() {

    }


    public static boolean insert(Context context, String[] orders, String source) {
        int ordercount = getCount(context);
        ContentResolver cr = context.getContentResolver();
        ContentValues cv = new ContentValues();
        if (source.equals(Constants.REMOTE)) {
            cv.put(ACK, 1);
            cv.put(SYS_ID, orders[0]);
            cv.put(NAME, orders[1]);
            cv.put(STATION, orders[2]);
        } else if (source.equals(Constants.LOCAL)) {
            cv.put(ACK, 1);
            cv.put(SYS_ID, orders[0]);
            cv.put(NAME, orders[1]);
            cv.put(STATION, orders[2]);
        }
        boolean ret = false;
        if(isFormPresent(context, orders[1]))
           if(update(context,orders,source))
               ret = true;
        else
            cr.insert(BASEURI, cv);

        if (getCount(context) == ordercount + 1)
        {
            Log.e("INSERTED","inserted pump");
            ret = true;
        }
        return ret;
    }

    public static boolean update(Context context, String[] orders, String source) {
        ContentResolver cr = context.getContentResolver();
        ContentValues cv = new ContentValues();
        String where = null;
        if (source.equals(Constants.REMOTE)) {
            cv.put(ACK, 1);
            cv.put(NAME, orders[1]);
            cv.put(STATION, orders[2]);
            where = SYS_ID + " = " + orders[0];
        } else if (source.equals(Constants.LOCAL)) {
            cv.put(ACK, 1);
            cv.put(SYS_ID, orders[0]);
            cv.put(NAME, orders[1]);
            cv.put(STATION, orders[2]);
            where = ID + " = " + orders[11];
        }
        int up = cr.update(BASEURI, cv, where, null);
        if (up > 0) {
            Log.e("UPDATED","Updated pump");
            return true;
        }
        return false;
    }

    //gettotaltax,getpayamount
    public static int getCount(Context context) {
        ContentResolver cr = context.getContentResolver();
        Cursor _c = cr.query(BASEURI, null, null, null, null);
        int count = _c.getCount();
        _c.close();
        return count;
    }

	/*
         * Fetching sales from server if sent frJsonObjectJsonArrayom this device
         */

    /* check if the sale id is present in the app
    *
    */

    private static boolean isFormPresent(Context context, String custid) {
        ContentResolver cr = context.getContentResolver();
        boolean ret = false;
        String where = NAME+ " = "+ DatabaseUtils.sqlEscapeString(custid);
        Cursor c = cr.query(BASEURI, null, where, null, null);
        try {
            if (c != null && c.moveToFirst()) {
                ret = true;
            }
        } finally {
            if (c != null) {
                c.close();
            }
        }
        Log.e("exist","exist? "+ret);
        return ret;
    }

    public static boolean columnExist(SQLiteDatabase context, String column) {
        boolean ret = false;
        String sql = " SELECT * FROM " + TABLENAME + " LIMIT 1";

        Cursor c = context.rawQuery(sql, null);
        try {
            int indext = c.getColumnIndex(column);
            System.out.println(" index is " + indext);
            if (indext != -1)
                ret = true;
        } finally {
            if (c != null) {
                c.close();
            }
        }
        return ret;
    }


    public static int getId(Context context) {
        ContentResolver cr = context.getContentResolver();
        Cursor c = cr.query(BASEURI, new String[]{ID}, null, null, null);
        try {
            int i = 0;
            if (c.moveToLast()) {
                i = c.getInt(c.getColumnIndexOrThrow(ID));
                return i;
            }
        } finally {
            if (c != null) {
                c.close();
            }
        }
        return 0;
    }


    public static ArrayList<String> getItems(Context context) {
        ArrayList<String> lst = new ArrayList<>();
        ContentResolver cr = context.getContentResolver();
        Cursor c = cr.query(BASEURI, null, null, null, null);
        try {
            lst.add(context.getString(R.string.strselectpump));
            if (c != null && c.moveToFirst()) {
                do {
                    String n = c.getString(c.getColumnIndex(NAME));
                    if(lst.indexOf(n) == -1)
                        lst.add(n);
                } while (c.moveToNext());
            }
            lst.add(context.getString(R.string.straddpump));
        } finally {
            if (c != null)
                c.close();
        }
        return lst;
    }

    public static JSONArray getJsonArray(Context context, String string) {
        ContentResolver cr = context.getContentResolver();
        String where = STATION + " = " + string;
        Cursor c = cr.query(BASEURI, null, where, null, null);
        if (c != null && c.getCount() == 0) {
            c.close();
            return null;
        }
        JSONArray jarr = new JSONArray();
        try {
            if (c != null && c.moveToFirst()) {
                do {
                    JSONObject j = new JSONObject();
                    j.put(ID, c.getString(c.getColumnIndexOrThrow(ID)));
                    j.put(SYS_ID, c.getString(c.getColumnIndexOrThrow(SYS_ID)));
                    j.put(NAME, c.getString(c.getColumnIndexOrThrow(NAME)));
                    jarr.put(j);
                } while (c.moveToNext());
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
        System.out.println(jarr.toString());
        return jarr;
    }

    public static String getPump(Context context, long id_) {
        String where = STATION +" = " +id_;
        ContentResolver cr = context.getContentResolver();
        Cursor c = cr.query(BASEURI, null, where,null,null) ;
        try {
            String i= "0" ;
            if(c != null && c.moveToFirst())
            {
                i = c.getString(c.getColumnIndexOrThrow(NAME));

                return i ;
            }
        }finally {
            if(c != null) {
                c.close();
            }
        }
        return "0" ;
    }
}