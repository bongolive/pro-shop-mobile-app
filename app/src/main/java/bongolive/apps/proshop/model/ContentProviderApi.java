/*
 * Copyright (c) 2015.
 * Prosurvey is a property of Bongolive Enterprises Ltd. An authorized
 * copy of ideas, source codes is against the regulations
 */

package bongolive.apps.proshop.model;

import android.content.ContentProvider;
import android.content.ContentUris;
import android.content.ContentValues;
import android.content.Context;
import android.content.UriMatcher;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteException;
import android.database.sqlite.SQLiteQueryBuilder;
import android.net.Uri;

public class ContentProviderApi extends ContentProvider
{
	public static final String AUTHORITY = "bongolive.apps.proshop.model.ContentProviderApi" ;
	Context _context;
	//static final HashMap<String, String> hashmap ;
	private static UriMatcher _uriMatcher  ;

	private static final int SPECIFIC_PAYMENT = 1;
	private static final int GENERAL_PAYMENT = 2;
	private static final int GENERAL_ORDERS = 3;
	private static final int SPECIFIC_ORDERS = 4 ;
	private static final int GENERAL_ORDERITEMS = 5 ;
	private static final int SPECIFIC_ORDERITEMS = 6 ;
	private static final int GENERAL_CUSTOMER = 7 ;
	private static final int SPECIFIC_CUSTOMER = 8 ;
	private static final int GENERAL_PAYMODE = 9 ;
	private static final int SPECIFIC_PAYMODE = 10;
	private static final int GENERAL_SETTINGS = 11 ;
	private static final int SPECIFIC_SETTINGS = 12 ;
	private static final int GENERAL_PURCHASEITEMS = 13;
	private static final int SPECIFIC_PURCHASEITEMS = 14 ;
    private static final int GENERAL_LOC = 15;
    private static final int SPECIFIC_LOC = 16;
    private static final int GENERAL_MULT = 17;
    private static final int SPECIFIC_MULT = 18;
    private static final int GENERAL_PRODUCT = 19;
    private static final int SPECIFIC_PRODUCT = 20;
    private static final int GENERAL_TRANSFER = 21;
    private static final int SPECIFIC_TRANSFER = 22;
    private static final int GENERAL_WH = 23;
    private static final int SPECIFIC_WH = 24;;
    private static final int GENERAL_CAT = 25;
    private static final int SPECIFIC_CAT = 26;
    private static final int SPECIFIC_USER = 27;
    private static final int GENERAL_USER = 28;
    private static final int SPECIFIC_DELIVERY = 29;
    private static final int GENERAL_DELIVERY = 30;
    private static final int SPECIFIC_PENDING = 31;
    private static final int GENERAL_PENDING = 32;
    private static final int SPECIFIC_STATION = 33;
    private static final int GENERAL_STATION = 34;
    private static final int SPECIFIC_AUDIT = 35;
    private static final int GENERAL_AUDIT = 36;
    private static final int SPECIFIC_XPENSE = 37;
    private static final int GENERAL_XPENSE = 38;
    private static final int SPECIFIC_AUDITRESPONSE = 39;
    private static final int GENERAL_AUDITRESPONSE = 40;
    private static final int SPECIFIC_PUMP = 41;
    private static final int GENERAL_PUMP = 42;
    private static final int SPECIFIC_TRANSITEMS = 43;
    private static final int GENERAL_TRANSITEMS = 44;
    private static final int SPECIFIC_STKITEMS = 45;
    private static final int GENERAL_STKITEMS = 46;
    private static final int SPECIFIC_STK = 47;
    private static final int GENERAL_STK = 48;
	static {
	    _uriMatcher  = new UriMatcher(UriMatcher.NO_MATCH) ;
		_uriMatcher.addURI(AUTHORITY, Sales.TABLENAME, GENERAL_ORDERS) ;
		_uriMatcher.addURI(AUTHORITY, Sales.TABLENAME+ "/#", SPECIFIC_ORDERS) ;
		_uriMatcher.addURI(AUTHORITY, Customers.TABLENAME, GENERAL_CUSTOMER) ;
		_uriMatcher.addURI(AUTHORITY, Customers.TABLENAME + "/#", SPECIFIC_CUSTOMER) ;
		_uriMatcher.addURI(AUTHORITY, SalesItems.TABLENAME, GENERAL_ORDERITEMS) ;
		_uriMatcher.addURI(AUTHORITY, SalesItems.TABLENAME + "/#", SPECIFIC_ORDERITEMS) ;
		_uriMatcher.addURI(AUTHORITY, PaymentMode.TABLENAME, GENERAL_PAYMODE) ;
		_uriMatcher.addURI(AUTHORITY, PaymentMode.TABLENAME + "/#", SPECIFIC_PAYMODE);
		_uriMatcher.addURI(AUTHORITY, Settings.TABLENAME, GENERAL_SETTINGS) ;
		_uriMatcher.addURI(AUTHORITY, Settings.TABLENAME + "/#", SPECIFIC_SETTINGS) ;
		_uriMatcher.addURI(AUTHORITY, PurchaseItems.TABLENAME, GENERAL_PURCHASEITEMS) ;
		_uriMatcher.addURI(AUTHORITY, PurchaseItems.TABLENAME + "/#", SPECIFIC_PURCHASEITEMS) ;
        _uriMatcher.addURI(AUTHORITY, Tracking.TABLENAME, GENERAL_LOC) ;
        _uriMatcher.addURI(AUTHORITY, Tracking.TABLENAME + "/#", SPECIFIC_LOC) ;
        _uriMatcher.addURI(AUTHORITY, MultimediaContents.TABLENAME, GENERAL_MULT) ;
        _uriMatcher.addURI(AUTHORITY, MultimediaContents.TABLENAME + "/#", SPECIFIC_MULT) ;
        _uriMatcher.addURI(AUTHORITY, Products.TABLENAME, GENERAL_PRODUCT) ;
        _uriMatcher.addURI(AUTHORITY, Products.TABLENAME + "/#", SPECIFIC_PRODUCT) ;
        _uriMatcher.addURI(AUTHORITY, Transfer.TABLENAME, GENERAL_TRANSFER) ;
        _uriMatcher.addURI(AUTHORITY, Transfer.TABLENAME + "/#", SPECIFIC_TRANSFER) ;
        _uriMatcher.addURI(AUTHORITY, Warehouse.TABLENAME, GENERAL_WH) ;
        _uriMatcher.addURI(AUTHORITY, Warehouse.TABLENAME + "/#", SPECIFIC_WH) ;
        _uriMatcher.addURI(AUTHORITY, Product_Categories.TABLENAME, GENERAL_CAT) ;
        _uriMatcher.addURI(AUTHORITY, Product_Categories.TABLENAME + "/#", SPECIFIC_CAT) ;
        _uriMatcher.addURI(AUTHORITY, User.TABLENAME, GENERAL_USER) ;
        _uriMatcher.addURI(AUTHORITY, User.TABLENAME + "/#", SPECIFIC_USER) ;
        _uriMatcher.addURI(AUTHORITY, Delivery.TABLENAME, GENERAL_DELIVERY) ;
        _uriMatcher.addURI(AUTHORITY, Delivery.TABLENAME + "/#", SPECIFIC_DELIVERY) ;
        _uriMatcher.addURI(AUTHORITY, Pending_Items.TABLENAME, GENERAL_PENDING) ;
        _uriMatcher.addURI(AUTHORITY, Pending_Items.TABLENAME + "/#", SPECIFIC_PENDING) ;
        _uriMatcher.addURI(AUTHORITY, Station.TABLENAME, GENERAL_STATION) ;
        _uriMatcher.addURI(AUTHORITY, Station.TABLENAME + "/#", SPECIFIC_STATION) ;
        _uriMatcher.addURI(AUTHORITY, Audit.TABLENAME, GENERAL_AUDIT) ;
        _uriMatcher.addURI(AUTHORITY, Audit.TABLENAME + "/#", SPECIFIC_AUDIT) ;
        _uriMatcher.addURI(AUTHORITY, Expenses.TABLENAME, GENERAL_XPENSE) ;
        _uriMatcher.addURI(AUTHORITY, Expenses.TABLENAME + "/#", SPECIFIC_XPENSE) ;
        _uriMatcher.addURI(AUTHORITY, Payments.TABLENAME, GENERAL_PAYMENT) ;
        _uriMatcher.addURI(AUTHORITY, Payments.TABLENAME + "/#", SPECIFIC_PAYMENT) ;
        _uriMatcher.addURI(AUTHORITY, AuditResponses.TABLENAME, GENERAL_AUDITRESPONSE) ;
        _uriMatcher.addURI(AUTHORITY, AuditResponses.TABLENAME + "/#", SPECIFIC_AUDITRESPONSE) ;
        _uriMatcher.addURI(AUTHORITY, StationPump.TABLENAME, GENERAL_PUMP) ;
        _uriMatcher.addURI(AUTHORITY, StationPump.TABLENAME + "/#", SPECIFIC_PUMP) ;
        _uriMatcher.addURI(AUTHORITY, TransferItems.TABLENAME, GENERAL_TRANSITEMS) ;
        _uriMatcher.addURI(AUTHORITY, TransferItems.TABLENAME + "/#", SPECIFIC_TRANSITEMS) ;
        _uriMatcher.addURI(AUTHORITY, StockIssue.TABLENAME, GENERAL_STK) ;
        _uriMatcher.addURI(AUTHORITY, StockIssue.TABLENAME + "/#", SPECIFIC_STK) ;
        _uriMatcher.addURI(AUTHORITY, StockIssueItems.TABLENAME, GENERAL_STKITEMS) ;
        _uriMatcher.addURI(AUTHORITY, StockIssueItems.TABLENAME + "/#", SPECIFIC_STKITEMS) ;
	}

	@Override
	public boolean onCreate() {
		try{
		DatabaseHandler.getInstance(getContext());
		} catch (SQLiteException ex)
		{
			throw new Error(ex.toString());
		}
		return true ;
	}

	@Override
	public Cursor query(Uri uri, String[] projection, String selection, String[] selectionArgs, String sortOrder) {
		SQLiteDatabase db = DatabaseHandler.getInstance(getContext()).getReadableDatabase() ;
		SQLiteQueryBuilder qb = new SQLiteQueryBuilder();
		switch(_uriMatcher.match(uri)){
		case GENERAL_CUSTOMER:
			qb.setTables(Customers.TABLENAME) ;
			break ;
		case SPECIFIC_CUSTOMER:
			qb.setTables(Customers.TABLENAME);
			qb.appendWhere(Customers.ID + " =  " + uri.getLastPathSegment()) ;
			break ;
		case GENERAL_ORDERITEMS:
			qb.setTables(SalesItems.TABLENAME) ;
			break ;
		case SPECIFIC_ORDERITEMS:
			qb.setTables(SalesItems.TABLENAME);
			qb.appendWhere(SalesItems.ID + " =  " + uri.getLastPathSegment()) ;
			break ;
		case GENERAL_ORDERS:
			qb.setTables(Sales.TABLENAME);
			break ;
		case SPECIFIC_ORDERS:
			qb.setTables(Sales.TABLENAME);
			qb.appendWhere(Sales.ID + " =  " + uri.getLastPathSegment()) ;
			break ;
		case GENERAL_PAYMODE:
			qb.setTables(PaymentMode.TABLENAME);
			break ;
		case SPECIFIC_PAYMODE:
			qb.setTables(PaymentMode.TABLENAME);
			qb.appendWhere(PaymentMode.ID + " =  " + uri.getLastPathSegment()) ;
			break ;
		case GENERAL_SETTINGS:
			qb.setTables(Settings.TABLENAME);
			break ;
		case SPECIFIC_SETTINGS:
			qb.setTables(Settings.TABLENAME);
			qb.appendWhere(Settings.ID + " =  " + uri.getLastPathSegment()) ;
		case GENERAL_PURCHASEITEMS:
			qb.setTables(PurchaseItems.TABLENAME);
			break ;
		case SPECIFIC_PURCHASEITEMS:
			qb.setTables(PurchaseItems.TABLENAME);
			qb.appendWhere(PurchaseItems.ID + " =  " + uri.getLastPathSegment()) ;
			break ;
		case GENERAL_LOC:
			qb.setTables(Tracking.TABLENAME);
			break ;
		case SPECIFIC_LOC:
			qb.setTables(Tracking.TABLENAME);
			qb.appendWhere(Tracking.ID + " =  " + uri.getLastPathSegment()) ;
			break ;
		case GENERAL_MULT:
			qb.setTables(MultimediaContents.TABLENAME);
			break ;
		case SPECIFIC_MULT:
			qb.setTables(MultimediaContents.TABLENAME);
			qb.appendWhere(MultimediaContents.ID + " =  " + uri.getLastPathSegment()) ;
			break ;
		case GENERAL_PRODUCT:
			qb.setTables(Products.TABLENAME);
			break ;
		case SPECIFIC_PRODUCT:
			qb.setTables(Products.TABLENAME);
			qb.appendWhere(Products.ID + " =  " + uri.getLastPathSegment()) ;
			break ;
		case GENERAL_TRANSFER:
			qb.setTables(Transfer.TABLENAME);
			break ;
		case SPECIFIC_TRANSFER:
			qb.setTables(Transfer.TABLENAME);
			qb.appendWhere(Transfer.ID + " =  " + uri.getLastPathSegment()) ;
			break ;
		case GENERAL_WH:
			qb.setTables(Warehouse.TABLENAME);
			break ;
		case SPECIFIC_WH:
			qb.setTables(Warehouse.TABLENAME);
			qb.appendWhere(Warehouse.ID + " =  " + uri.getLastPathSegment()) ;
			break ;
		case GENERAL_CAT:
			qb.setTables(Product_Categories.TABLENAME);
			break ;
		case SPECIFIC_CAT:
			qb.setTables(Product_Categories.TABLENAME);
			qb.appendWhere(Product_Categories.ID + " =  " + uri.getLastPathSegment()) ;
			break ;
		case GENERAL_USER:
			qb.setTables(User.TABLENAME);
			break ;
		case SPECIFIC_USER:
			qb.setTables(User.TABLENAME);
			qb.appendWhere(User.ID + " =  " + uri.getLastPathSegment()) ;
			break ;
		case GENERAL_DELIVERY:
			qb.setTables(Delivery.TABLENAME);
			break ;
		case SPECIFIC_DELIVERY:
			qb.setTables(Delivery.TABLENAME);
			qb.appendWhere(Delivery.ID + " =  " + uri.getLastPathSegment()) ;
			break ;
		case GENERAL_PENDING:
			qb.setTables(Pending_Items.TABLENAME);
			break ;
		case SPECIFIC_PENDING:
			qb.setTables(Pending_Items.TABLENAME);
			qb.appendWhere(Pending_Items.ID + " =  " + uri.getLastPathSegment()) ;
			break ;
		case GENERAL_STATION:
			qb.setTables(Station.TABLENAME);
			break ;
		case SPECIFIC_STATION:
			qb.setTables(Station.TABLENAME);
			qb.appendWhere(Station.ID + " =  " + uri.getLastPathSegment()) ;
			break ;
		case GENERAL_AUDIT:
			qb.setTables(Audit.TABLENAME);
			break ;
		case SPECIFIC_AUDIT:
			qb.setTables(Audit.TABLENAME);
			qb.appendWhere(Audit.ID + " =  " + uri.getLastPathSegment()) ;
			break ;
		case GENERAL_XPENSE:
			qb.setTables(Expenses.TABLENAME);
			break ;
		case SPECIFIC_XPENSE:
			qb.setTables(Expenses.TABLENAME);
			qb.appendWhere(Expenses.ID + " =  " + uri.getLastPathSegment()) ;
			break ;
		case GENERAL_PAYMENT:
			qb.setTables(Payments.TABLENAME);
			break ;
		case SPECIFIC_PAYMENT:
			qb.setTables(Payments.TABLENAME);
			qb.appendWhere(Payments.ID + " =  " + uri.getLastPathSegment()) ;
			break ;
		case GENERAL_AUDITRESPONSE:
			qb.setTables(AuditResponses.TABLENAME);
			break ;
		case SPECIFIC_AUDITRESPONSE:
			qb.setTables(AuditResponses.TABLENAME);
			qb.appendWhere(AuditResponses.ID + " =  " + uri.getLastPathSegment()) ;
			break ;
		case GENERAL_PUMP:
			qb.setTables(StationPump.TABLENAME);
			break ;
		case SPECIFIC_PUMP:
			qb.setTables(StationPump.TABLENAME);
			qb.appendWhere(StationPump.ID + " =  " + uri.getLastPathSegment()) ;
			break ;
		case GENERAL_TRANSITEMS:
			qb.setTables(TransferItems.TABLENAME);
			break ;
		case SPECIFIC_TRANSITEMS:
			qb.setTables(TransferItems.TABLENAME);
			qb.appendWhere(TransferItems.ID + " =  " + uri.getLastPathSegment()) ;
			break ;
		case GENERAL_STK:
			qb.setTables(StockIssue.TABLENAME);
			break ;
		case SPECIFIC_STK:
			qb.setTables(StockIssue.TABLENAME);
			qb.appendWhere(StockIssue.ID + " =  " + uri.getLastPathSegment()) ;
			break ;
		case GENERAL_STKITEMS:
			qb.setTables(StockIssueItems.TABLENAME);
			break ;
		case SPECIFIC_STKITEMS:
			qb.setTables(StockIssueItems.TABLENAME);
			qb.appendWhere(StockIssueItems.ID + " =  " + uri.getLastPathSegment()) ;
			break ;
		default:
			throw new IllegalArgumentException(" UNKOWN URI :" + uri);
		}

		Cursor c = qb.query(db, projection, selection, selectionArgs, null, null, sortOrder) ;
		c.setNotificationUri(getContext().getContentResolver(), uri);
		return c ;
	}

	@Override
	public String getType(Uri uri) {
		switch(_uriMatcher.match(uri)){
		case GENERAL_CUSTOMER:
			return Customers.GENERAL_CONTENT_TYPE ;
		case SPECIFIC_CUSTOMER:
			return Customers.SPECIFIC_CONTENT_TYPE ;
		case GENERAL_ORDERITEMS:
			return SalesItems.GENERAL_CONTENT_TYPE ;
		case SPECIFIC_ORDERITEMS:
			return SalesItems.SPECIFIC_CONTENT_TYPE ;
		case GENERAL_ORDERS:
			return Sales.GENERAL_CONTENT_TYPE;
		case SPECIFIC_ORDERS:
			return Sales.SPECIFIC_CONTENT_TYPE ;
		case GENERAL_PAYMODE:
			return PaymentMode.GENERAL_CONTENT_TYPE;
		case SPECIFIC_PAYMODE:
			return PaymentMode.SPECIFIC_CONTENT_TYPE;
		case GENERAL_SETTINGS:
			return Settings.GENERAL_CONTENT_TYPE;
		case SPECIFIC_SETTINGS:
			return Settings.SPECIFIC_CONTENT_TYPE ;
		case GENERAL_PURCHASEITEMS:
			return PurchaseItems.GENERAL_CONTENT_TYPE;
		case SPECIFIC_PURCHASEITEMS:
			return PurchaseItems.SPECIFIC_CONTENT_TYPE ;
		case GENERAL_LOC:
			return Tracking.GENERAL_CONTENT_TYPE;
		case SPECIFIC_LOC:
			return Tracking.SPECIFIC_CONTENT_TYPE ;
		case GENERAL_MULT:
			return MultimediaContents.GENERAL_CONTENT_TYPE;
		case SPECIFIC_MULT:
			return MultimediaContents.SPECIFIC_CONTENT_TYPE ;
		case GENERAL_PRODUCT:
			return Products.GENERAL_CONTENT_TYPE;
		case SPECIFIC_PRODUCT:
			return Products.SPECIFIC_CONTENT_TYPE ;
		case GENERAL_TRANSFER:
			return Transfer.GENERAL_CONTENT_TYPE;
		case SPECIFIC_TRANSFER:
			return Transfer.SPECIFIC_CONTENT_TYPE ;
		case GENERAL_WH:
			return Warehouse.GENERAL_CONTENT_TYPE;
		case SPECIFIC_WH:
			return Warehouse.SPECIFIC_CONTENT_TYPE ;
		case GENERAL_CAT:
			return Product_Categories.GENERAL_CONTENT_TYPE;
		case SPECIFIC_CAT:
			return Product_Categories.SPECIFIC_CONTENT_TYPE ;
		case GENERAL_USER:
			return User.GENERAL_CONTENT_TYPE;
		case SPECIFIC_USER:
			return User.SPECIFIC_CONTENT_TYPE ;
		case GENERAL_DELIVERY:
			return Delivery.GENERAL_CONTENT_TYPE;
		case SPECIFIC_DELIVERY:
			return Delivery.SPECIFIC_CONTENT_TYPE ;
		case GENERAL_PENDING:
			return Pending_Items.GENERAL_CONTENT_TYPE;
		case SPECIFIC_PENDING:
			return Pending_Items.SPECIFIC_CONTENT_TYPE ;
		case GENERAL_STATION:
			return Station.GENERAL_CONTENT_TYPE;
		case SPECIFIC_STATION:
			return Station.SPECIFIC_CONTENT_TYPE ;
		case GENERAL_AUDIT:
			return Audit.GENERAL_CONTENT_TYPE;
		case SPECIFIC_AUDIT:
			return Audit.SPECIFIC_CONTENT_TYPE ;
		case GENERAL_XPENSE:
			return Expenses.GENERAL_CONTENT_TYPE;
		case SPECIFIC_XPENSE:
			return Expenses.SPECIFIC_CONTENT_TYPE ;
		case GENERAL_PAYMENT:
			return Payments.GENERAL_CONTENT_TYPE;
		case SPECIFIC_PAYMENT:
			return Payments.SPECIFIC_CONTENT_TYPE ;
		case GENERAL_AUDITRESPONSE:
			return AuditResponses.GENERAL_CONTENT_TYPE;
		case SPECIFIC_AUDITRESPONSE:
			return AuditResponses.SPECIFIC_CONTENT_TYPE ;
		case GENERAL_PUMP:
			return StationPump.GENERAL_CONTENT_TYPE;
		case SPECIFIC_PUMP:
			return StationPump.SPECIFIC_CONTENT_TYPE ;
		case GENERAL_TRANSITEMS:
			return TransferItems.GENERAL_CONTENT_TYPE;
		case SPECIFIC_TRANSITEMS:
			return TransferItems.SPECIFIC_CONTENT_TYPE ;
		case GENERAL_STK:
			return StockIssue.GENERAL_CONTENT_TYPE;
		case SPECIFIC_STK:
			return StockIssue.SPECIFIC_CONTENT_TYPE ;
		case GENERAL_STKITEMS:
			return StockIssueItems.GENERAL_CONTENT_TYPE;
		case SPECIFIC_STKITEMS:
			return StockIssueItems.SPECIFIC_CONTENT_TYPE ;
		default:
			throw new IllegalArgumentException(" Unkown TYPE " + uri) ;
		}
	}

	@Override
	public Uri insert(Uri uri, ContentValues values) {
		SQLiteDatabase db = DatabaseHandler.getInstance(getContext()).getWritableDatabase() ;
		long id ;
		switch(_uriMatcher.match(uri))
		{
		case GENERAL_CUSTOMER:
			id = db.insertOrThrow(Customers.TABLENAME, null, values);
			break;
		case GENERAL_ORDERITEMS:
			id = db.insertOrThrow(SalesItems.TABLENAME, null, values);
			break;
		case GENERAL_ORDERS:
			id = db.insertOrThrow(Sales.TABLENAME, null, values);
			break;
		case GENERAL_PAYMODE:
			id = db.insertOrThrow(PaymentMode.TABLENAME, null, values);
			break;
		case GENERAL_SETTINGS:
			id = db.insertOrThrow(Settings.TABLENAME, null, values) ;
			break;
		case GENERAL_PURCHASEITEMS:
			id = db.insertOrThrow(PurchaseItems.TABLENAME, null, values) ;
            break;
		case GENERAL_LOC:
			id = db.insertOrThrow(Tracking.TABLENAME, null, values) ;
			break;
		case GENERAL_MULT:
			id = db.insertOrThrow(MultimediaContents.TABLENAME, null, values) ;
			break;
		case GENERAL_PRODUCT:
			id = db.insertOrThrow(Products.TABLENAME, null, values) ;
			break;
		case GENERAL_TRANSFER:
			id = db.insertOrThrow(Transfer.TABLENAME, null, values) ;
			break;
		case GENERAL_WH:
			id = db.insertOrThrow(Warehouse.TABLENAME, null, values) ;
			break;
		case GENERAL_CAT:
			id = db.insertOrThrow(Product_Categories.TABLENAME, null, values) ;
			break;
		case GENERAL_USER:
			id = db.insertOrThrow(User.TABLENAME, null, values) ;
			break;
		case GENERAL_DELIVERY:
			id = db.insertOrThrow(Delivery.TABLENAME, null, values) ;
			break;
		case GENERAL_PENDING:
			id = db.insertOrThrow(Pending_Items.TABLENAME, null, values) ;
			break;
		case GENERAL_STATION:
			id = db.insertOrThrow(Station.TABLENAME, null, values) ;
			break;
		case GENERAL_AUDIT:
			id = db.insertOrThrow(Audit.TABLENAME, null, values) ;
			break;
		case GENERAL_XPENSE:
			id = db.insertOrThrow(Expenses.TABLENAME, null, values) ;
			break;
		case GENERAL_PAYMENT:
			id = db.insertOrThrow(Payments.TABLENAME, null, values) ;
			break;
		case GENERAL_AUDITRESPONSE:
			id = db.insertOrThrow(AuditResponses.TABLENAME, null, values) ;
			break;
		case GENERAL_PUMP:
			id = db.insertOrThrow(StationPump.TABLENAME, null, values) ;
			break;
		case GENERAL_TRANSITEMS:
			id = db.insertOrThrow(TransferItems.TABLENAME, null, values) ;
			break;
		case GENERAL_STK:
			id = db.insertOrThrow(StockIssue.TABLENAME, null, values) ;
			break;
		case GENERAL_STKITEMS:
			id = db.insertOrThrow(StockIssueItems.TABLENAME, null, values) ;
			break;
		default:
			throw new IllegalArgumentException(" error invalid uri " + uri);
		}
		Uri inserturi = ContentUris.withAppendedId(uri, id);
		getContext().getContentResolver().notifyChange(inserturi, null);
		return inserturi ;
	}

	@Override
	public int delete(Uri uri, String selection, String[] selectionArgs) {
		SQLiteDatabase db = DatabaseHandler.getInstance(getContext()).getWritableDatabase();
		int count = 0 ;
		switch(_uriMatcher.match(uri)){
		case GENERAL_CUSTOMER:
			count = db.delete(Customers.TABLENAME, selection, selectionArgs);
			break;
		case SPECIFIC_CUSTOMER:
			count = db.delete(Customers.TABLENAME, Customers.ID + " = ?", new String[]{uri.getLastPathSegment()});
			break;
		case GENERAL_ORDERITEMS:
			count = db.delete(SalesItems.TABLENAME, selection,selectionArgs);
			break ;
		case SPECIFIC_ORDERITEMS:
			count = db.delete(SalesItems.TABLENAME, SalesItems.ID + " = ?", new String[]{uri.getLastPathSegment()});
			break;
		case GENERAL_ORDERS:
			count = db.delete(Sales.TABLENAME, selection,selectionArgs);
			break ;
		case SPECIFIC_ORDERS:
			count = db.delete(Sales.TABLENAME, Sales.ID + " = ?", new String[]{uri.getLastPathSegment()});
			break;
		case GENERAL_PAYMODE:
			count = db.delete(PaymentMode.TABLENAME, selection,selectionArgs);
			break ;
		case SPECIFIC_PAYMODE:
			count = db.delete(PaymentMode.TABLENAME,PaymentMode.ID + " = ?", new String[]{uri.getLastPathSegment()});
			break;
		case GENERAL_SETTINGS:
			count = db.delete(Settings.TABLENAME, selection,selectionArgs);
			break ;
		case SPECIFIC_SETTINGS:
			count = db.delete(Settings.TABLENAME,  Settings.ID + " = ?", new String[]{uri.getLastPathSegment()});
			break;
		case GENERAL_PURCHASEITEMS:
			count = db.delete(PurchaseItems.TABLENAME, selection,selectionArgs);
			break ;
		case SPECIFIC_PURCHASEITEMS:
			count = db.delete(PurchaseItems.TABLENAME,  PurchaseItems.ID + " = ?", new String[]{uri.getLastPathSegment()});
			break;
		case GENERAL_LOC:
			count = db.delete(Tracking.TABLENAME, selection,selectionArgs);
			break ;
		case SPECIFIC_LOC:
			count = db.delete(Tracking.TABLENAME,  Tracking.ID + " = ?", new String[]{uri.getLastPathSegment()});
			break;
		case GENERAL_MULT:
			count = db.delete(MultimediaContents.TABLENAME, selection,selectionArgs);
			break ;
		case SPECIFIC_MULT:
			count = db.delete(MultimediaContents.TABLENAME,  MultimediaContents.ID + " = ?", new String[]{uri.getLastPathSegment()});
			break;
		case GENERAL_PRODUCT:
			count = db.delete(Products.TABLENAME, selection,selectionArgs);
			break ;
		case SPECIFIC_PRODUCT:
			count = db.delete(Products.TABLENAME,  Products.ID + " = ?", new String[]{uri.getLastPathSegment()});
			break;
		case GENERAL_TRANSFER:
			count = db.delete(Transfer.TABLENAME, selection,selectionArgs);
			break ;
		case SPECIFIC_TRANSFER:
			count = db.delete(Transfer.TABLENAME,  Transfer.ID + " = ?", new String[]{uri.getLastPathSegment()});
			break;
		case GENERAL_WH:
			count = db.delete(Warehouse.TABLENAME, selection,selectionArgs);
			break ;
		case SPECIFIC_WH:
			count = db.delete(Warehouse.TABLENAME,  Warehouse.ID + " = ?", new String[]{uri.getLastPathSegment()});
			break;
		case GENERAL_CAT:
			count = db.delete(Product_Categories.TABLENAME, selection,selectionArgs);
			break ;
		case SPECIFIC_CAT:
			count = db.delete(Product_Categories.TABLENAME,  Product_Categories.ID + " = ?", new String[]{uri.getLastPathSegment()});
			break;
		case GENERAL_USER:
			count = db.delete(User.TABLENAME, selection,selectionArgs);
			break ;
		case SPECIFIC_USER:
			count = db.delete(User.TABLENAME,  User.ID + " = ?", new String[]{uri.getLastPathSegment()});
			break;
		case GENERAL_DELIVERY:
			count = db.delete(Delivery.TABLENAME, selection,selectionArgs);
			break ;
		case SPECIFIC_DELIVERY:
			count = db.delete(Delivery.TABLENAME,  Delivery.ID + " = ?", new String[]{uri.getLastPathSegment()});
			break;
		case GENERAL_PENDING:
			count = db.delete(Pending_Items.TABLENAME, selection,selectionArgs);
			break ;
		case SPECIFIC_PENDING:
			count = db.delete(Pending_Items.TABLENAME,  Pending_Items.ID + " = ?", new String[]{uri.getLastPathSegment()});
			break;
		case GENERAL_STATION:
			count = db.delete(Station.TABLENAME, selection,selectionArgs);
			break ;
		case SPECIFIC_STATION:
			count = db.delete(Station.TABLENAME,  Station.ID + " = ?", new String[]{uri.getLastPathSegment()});
			break;
		case GENERAL_AUDIT:
			count = db.delete(Audit.TABLENAME, selection,selectionArgs);
			break ;
		case SPECIFIC_AUDIT:
			count = db.delete(Audit.TABLENAME,  Audit.ID + " = ?", new String[]{uri.getLastPathSegment()});
			break;
		case GENERAL_XPENSE:
			count = db.delete(Expenses.TABLENAME, selection,selectionArgs);
			break ;
		case SPECIFIC_XPENSE:
			count = db.delete(Expenses.TABLENAME,  Expenses.ID + " = ?", new String[]{uri.getLastPathSegment()});
			break;
		case GENERAL_PAYMENT:
			count = db.delete(Payments.TABLENAME, selection,selectionArgs);
			break ;
		case SPECIFIC_PAYMENT:
			count = db.delete(Payments.TABLENAME,  Payments.ID + " = ?", new String[]{uri.getLastPathSegment()});
			break;
		case GENERAL_AUDITRESPONSE:
			count = db.delete(AuditResponses.TABLENAME, selection,selectionArgs);
			break ;
		case SPECIFIC_AUDITRESPONSE:
			count = db.delete(AuditResponses.TABLENAME,  AuditResponses.ID + " = ?", new String[]{uri.getLastPathSegment()});
			break;
		case GENERAL_PUMP:
			count = db.delete(StationPump.TABLENAME, selection,selectionArgs);
			break ;
		case SPECIFIC_PUMP:
			count = db.delete(StationPump.TABLENAME,  StationPump.ID + " = ?", new String[]{uri.getLastPathSegment()});
			break;
		case GENERAL_TRANSITEMS:
			count = db.delete(TransferItems.TABLENAME, selection,selectionArgs);
			break ;
		case SPECIFIC_TRANSITEMS:
			count = db.delete(TransferItems.TABLENAME,  TransferItems.ID + " = ?", new String[]{uri.getLastPathSegment()});
			break;
		case GENERAL_STK:
			count = db.delete(StockIssue.TABLENAME, selection,selectionArgs);
			break ;
		case SPECIFIC_STK:
			count = db.delete(StockIssue.TABLENAME,  StockIssue.ID + " = ?", new String[]{uri.getLastPathSegment()});
			break;
		case GENERAL_STKITEMS:
			count = db.delete(StockIssueItems.TABLENAME, selection,selectionArgs);
			break ;
		case SPECIFIC_STKITEMS:
			count = db.delete(StockIssueItems.TABLENAME,  StockIssueItems.ID + " = ?", new String[]{uri.getLastPathSegment()});
			break;
			default:
				throw new IllegalArgumentException(" invalid uri "+ uri);
		}
			getContext().getContentResolver().notifyChange(uri, null);
			return count ;
	}

	@Override
	public int update(Uri uri, ContentValues values, String selection, String[] selectionArgs) {
		SQLiteDatabase db = DatabaseHandler.getInstance(getContext()).getWritableDatabase() ;
		int count ;
		switch(_uriMatcher.match(uri)){
		case GENERAL_CUSTOMER:
			count = db.update(Customers.TABLENAME, values, selection, selectionArgs);
			break;
		case SPECIFIC_CUSTOMER:
			count = db.update(Customers.TABLENAME, values, Customers.ID + " = ?", new String[]{uri.getLastPathSegment()});
			break;
		case GENERAL_ORDERITEMS:
			count = db.update(SalesItems.TABLENAME, values, selection, selectionArgs);
			break ;
		case SPECIFIC_ORDERITEMS:
			count = db.update(SalesItems.TABLENAME, values, SalesItems.ID + " = ?", new String[]{uri.getLastPathSegment()});
			break;
		case GENERAL_ORDERS:
			count = db.update(Sales.TABLENAME, values, selection, selectionArgs);
			break ;
		case SPECIFIC_ORDERS:
			count = db.update(Sales.TABLENAME, values, Sales.ID + " = ?", new String[]{uri.getLastPathSegment()});
			break;
		case GENERAL_PAYMODE:
			count = db.update(PaymentMode.TABLENAME, values, selection, selectionArgs);
			break ;
		case SPECIFIC_PAYMODE:
			count = db.update(PaymentMode.TABLENAME, values, PaymentMode.ID + " = ?", new String[]{uri.getLastPathSegment()});
			break;
		case GENERAL_SETTINGS:
			count = db.update(Settings.TABLENAME, values, selection, selectionArgs);
			break ;
		case SPECIFIC_SETTINGS:
			count = db.update(Settings.TABLENAME, values, Settings.ID + " = ?", new String[]{uri.getLastPathSegment()});
			break;
		case GENERAL_PURCHASEITEMS:
			count = db.update(PurchaseItems.TABLENAME, values, selection, selectionArgs);
			break ;
		case SPECIFIC_PURCHASEITEMS:
			count = db.update(PurchaseItems.TABLENAME, values, PurchaseItems.ID + " = ?", new String[]{uri.getLastPathSegment()});
			break;
		case GENERAL_LOC:
			count = db.update(Tracking.TABLENAME, values, selection, selectionArgs);
			break ;
		case SPECIFIC_LOC:
			count = db.update(Tracking.TABLENAME, values, Tracking.ID + " = ?", new String[]{uri.getLastPathSegment()});
			break;
		case GENERAL_MULT:
			count = db.update(MultimediaContents.TABLENAME, values, selection, selectionArgs);
			break ;
		case SPECIFIC_MULT:
			count = db.update(MultimediaContents.TABLENAME, values, MultimediaContents.ID + " = ?", new String[]{uri.getLastPathSegment()});
			break;
		case GENERAL_PRODUCT:
			count = db.update(Products.TABLENAME, values, selection, selectionArgs);
			break ;
		case SPECIFIC_PRODUCT:
			count = db.update(Products.TABLENAME, values, Products.ID + " = ?", new String[]{uri.getLastPathSegment()});
			break;
		case GENERAL_TRANSFER:
			count = db.update(Transfer.TABLENAME, values, selection, selectionArgs);
			break ;
		case SPECIFIC_TRANSFER:
			count = db.update(Transfer.TABLENAME, values, Transfer.ID + " = ?", new String[]{uri.getLastPathSegment()});
			break;
		case GENERAL_WH:
			count = db.update(Warehouse.TABLENAME, values, selection, selectionArgs);
			break ;
		case SPECIFIC_WH:
			count = db.update(Warehouse.TABLENAME, values, Warehouse.ID + " = ?", new String[]{uri.getLastPathSegment()});
			break;
		case GENERAL_CAT:
			count = db.update(Product_Categories.TABLENAME, values, selection, selectionArgs);
			break ;
		case SPECIFIC_CAT:
			count = db.update(Product_Categories.TABLENAME, values, Product_Categories.ID + " = ?", new String[]{uri.getLastPathSegment()});
			break;
		case GENERAL_USER:
			count = db.update(User.TABLENAME, values, selection, selectionArgs);
			break ;
		case SPECIFIC_USER:
			count = db.update(User.TABLENAME, values, User.ID + " = ?", new String[]{uri.getLastPathSegment()});
			break;
		case GENERAL_DELIVERY:
			count = db.update(Delivery.TABLENAME, values, selection, selectionArgs);
			break ;
		case SPECIFIC_DELIVERY:
			count = db.update(Delivery.TABLENAME, values, Delivery.ID + " = ?", new String[]{uri.getLastPathSegment()});
			break;
		case GENERAL_PENDING:
			count = db.update(Pending_Items.TABLENAME, values, selection, selectionArgs);
			break ;
		case SPECIFIC_PENDING:
			count = db.update(Pending_Items.TABLENAME, values, Pending_Items.ID + " = ?", new String[]{uri.getLastPathSegment()});
			break;
		case GENERAL_STATION:
			count = db.update(Station.TABLENAME, values, selection, selectionArgs);
			break ;
		case SPECIFIC_STATION:
			count = db.update(Station.TABLENAME, values, Station.ID + " = ?", new String[]{uri.getLastPathSegment()});
			break;
		case GENERAL_AUDIT:
			count = db.update(Audit.TABLENAME, values, selection, selectionArgs);
			break ;
		case SPECIFIC_AUDIT:
			count = db.update(Audit.TABLENAME, values, Audit.ID + " = ?", new String[]{uri.getLastPathSegment()});
			break;
		case GENERAL_XPENSE:
			count = db.update(Expenses.TABLENAME, values, selection, selectionArgs);
			break ;
		case SPECIFIC_XPENSE:
			count = db.update(Expenses.TABLENAME, values, Expenses.ID + " = ?", new String[]{uri.getLastPathSegment()});
			break;
		case GENERAL_PAYMENT:
			count = db.update(Payments.TABLENAME, values, selection, selectionArgs);
			break ;
		case SPECIFIC_PAYMENT:
			count = db.update(Payments.TABLENAME, values, Payments.ID + " = ?", new String[]{uri.getLastPathSegment()});
			break;
		case GENERAL_AUDITRESPONSE:
			count = db.update(AuditResponses.TABLENAME, values, selection, selectionArgs);
			break ;
		case SPECIFIC_AUDITRESPONSE:
			count = db.update(AuditResponses.TABLENAME, values, AuditResponses.ID + " = ?", new String[]{uri.getLastPathSegment()});
			break;
		case GENERAL_PUMP:
			count = db.update(StationPump.TABLENAME, values, selection, selectionArgs);
			break ;
		case SPECIFIC_PUMP:
			count = db.update(StationPump.TABLENAME, values, StationPump.ID + " = ?", new String[]{uri.getLastPathSegment()});
			break;
		case GENERAL_TRANSITEMS:
			count = db.update(TransferItems.TABLENAME, values, selection, selectionArgs);
			break ;
		case SPECIFIC_TRANSITEMS:
			count = db.update(TransferItems.TABLENAME, values, TransferItems.ID + " = ?", new String[]{uri.getLastPathSegment()});
			break;
		case GENERAL_STK:
			count = db.update(StockIssue.TABLENAME, values, selection, selectionArgs);
			break ;
		case SPECIFIC_STK:
			count = db.update(StockIssue.TABLENAME, values, StockIssue.ID + " = ?", new String[]{uri.getLastPathSegment()});
			break;
		case GENERAL_STKITEMS:
			count = db.update(StockIssueItems.TABLENAME, values, selection, selectionArgs);
			break ;
		case SPECIFIC_STKITEMS:
			count = db.update(StockIssueItems.TABLENAME, values, StockIssueItems.ID + " = ?", new String[]{uri.getLastPathSegment()});
			break;
			default:
				throw new IllegalArgumentException(" invalid uri "+ uri);
		}
		getContext().getContentResolver().notifyChange(uri, null);
		return count ;
	}

}
