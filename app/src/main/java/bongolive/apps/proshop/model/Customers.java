/*
 * The source codes in this project are owned by Bongolive Enterprises. Copying or any kind of usage without Bongolive's approval is against the law.
 * Copyright (c) 2015.
 *
 */

package bongolive.apps.proshop.model;

import android.content.ContentResolver;
import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.DatabaseUtils;
import android.database.sqlite.SQLiteDatabase;
import android.net.Uri;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import bongolive.apps.proshop.R;
import bongolive.apps.proshop.controller.Validating;
import bongolive.apps.proshop.controller.Constants;
import bongolive.apps.proshop.controller.GpsTracker;
import bongolive.apps.proshop.controller.UrlCon;

public class Customers {
    public static final String TABLENAME = "customers";
    public static final Uri BASEURI = Uri.parse("content://" + ContentProviderApi.AUTHORITY + "/" + TABLENAME);
    public static final String ID = "_id";
    public static final String CUSTOMERNAME = "customer_name";
    public static final String BUZINESSNAME = "business_name";
    public static final String REFERENCE = "system_customer_id";
    public static final String AREA = "area";
    public static final String CITY = "city";
    public static final String MOBILE = "mobile";
    public static final String EMAIL = "email";
    public static final String VRN = "vrn";
    public static final String TIN = "tin";
    public static final String LAT = "lat";
    public static final String LONGTUDE = "lng";
    public static final String SYNCSTATUS = "is_synced";//0 new //1 synced //2 update //3 delete
    public static final String STREET = "street";
    private static UrlCon js ;

    public static final String SPECIFIC_CONTENT_TYPE = "vnd.android.cursor.item/vnd.com.proshop.customers";
    public static final String GENERAL_CONTENT_TYPE = "vnd.android.cursor.dir/vnd.com.proshop.customers";

    public static final String GETARRAYNAME = "customerList";
    public static final String SENDARRAYNAME = "customer";
    public static final String SENDARRAYNAME2 = "customerSentList";
    private static final String TAG = Customers.class.getName();

    //id,name,company,address,city,phone,email
    int id, reference, sync;
    String cname, bname, area, city, mob, email, lat, lng, street;

    public Customers(int id, int ref, int sy, String cn, String bn
            , String ar, String ci, String mo, String em, String lt, String lng, String srt) {
        this.id = id;
        this.reference = ref;
        this.sync = sy;
        this.cname = cn;
        this.bname = bn;
        this.area = ar;
        this.city = ci;
        this.mob = mo;
        this.email = em;
        this.lat = lt;
        this.lng = lng;
        this.street = srt;
    }

    public Customers() {

    }

    // for checking if the customer(sy_cust_id) is present in the database locally
    private static int isCustomerPresent(Context context, int custid) {
        ContentResolver cr = context.getContentResolver();
        Cursor c = cr.query(BASEURI, new String[]{REFERENCE}, null, null, null);
        try {
            int i = 0;
            if (c.moveToFirst()) {
                do {
                    i = c.getInt(c.getColumnIndexOrThrow(REFERENCE));
                    if (i == custid) {
                        return 1;
                    }
                } while (c.moveToNext());
            }
        } finally {
            if (c != null) {
                c.close();
            }
        }
        return 0;
    }

    public static String getSysCustidBasedOnLocalId(Context context, String ref)
    {
        ContentResolver cr = context.getContentResolver();
        String where = ID +  " = " + ref;
        Cursor cursor = cr.query(BASEURI, new String[] {REFERENCE}, where, null, null) ;
        try{
            if(cursor != null && cursor.moveToFirst())
            {
                String quantity = cursor.getString(cursor.getColumnIndexOrThrow(REFERENCE));
                return quantity;
            }
        }finally {
            if(cursor != null) {
                cursor.close();
            }
        }
        return "0";
    }

    public static boolean isCustomerValid(Context context, String ref)
    {
        ContentResolver cr = context.getContentResolver();
        String where = BUZINESSNAME +  " = " + DatabaseUtils.sqlEscapeString(ref);
        Cursor cursor = cr.query(BASEURI, null, where, null, null) ;
        try{
            if(cursor.moveToFirst())
            {
                return true;
            }
        }finally {
            if(cursor != null) {
                cursor.close();
            }
        }
        return false;
    }

    public static boolean isCustomerExisting(Context context, String vals) {
        ContentResolver cr = context.getContentResolver();
        String where = REFERENCE + " = " + vals;
        Cursor c = cr.query(BASEURI, null, where, null, null);
        boolean yes = false;
        try {
            if (c.moveToFirst())
                yes = true;
        } finally {
            if (c != null) {
                c.close();
            }
        }
        return yes;
    }

    public static String getCustomerBusinessName(Context context, String custid) {
        String bz = "";
        ContentResolver cr = context.getContentResolver();
        String where = REFERENCE + " = " + custid;
        Cursor c = cr.query(BASEURI, null, where, null, null);
        try {
            if (c.moveToFirst()) {
                bz = c.getString(c.getColumnIndexOrThrow(BUZINESSNAME));
            }
        } finally {
            if (c != null) {
                c.close();
            }
        }
        return bz;
    }

    public static String getCustomerBusinessNameLoca(Context context, String custid) {
        String bz = "";
        ContentResolver cr = context.getContentResolver();
        String where = ID + " = " + custid;
        Cursor c = cr.query(BASEURI, null, where, null, null);
        try {
            if (c.moveToFirst()) {
                bz = c.getString(c.getColumnIndexOrThrow(BUZINESSNAME));
            }
        } finally {
            if (c != null) {
                c.close();
            }
        }
        return bz;
    }

    public static String getVrn(Context context, String custid) {
        String bz = "";
        ContentResolver cr = context.getContentResolver();
        String where = ID + " = " + custid;
        Cursor c = cr.query(BASEURI, null, where, null, null);
        try {
            if (c.moveToFirst()) {
                bz = c.getString(c.getColumnIndexOrThrow(VRN));
            }
        } finally {
            if (c != null) {
                c.close();
            }
        }
        return bz;
    }

    public static String getTin(Context context, String custid) {
        String bz = "";
        ContentResolver cr = context.getContentResolver();
        String where = ID + " = " + custid;
        Cursor c = cr.query(BASEURI, null, where, null, null);
        try {
            if (c.moveToFirst()) {
                bz = c.getString(c.getColumnIndexOrThrow(TIN));
            }
        } finally {
            if (c != null) {
                c.close();
            }
        }
        return bz;
    }

    public static String getDefaultCustomer(Context context) {
        String bz = "";
        ContentResolver cr = context.getContentResolver();
        String where = BUZINESSNAME + " = "+ DatabaseUtils.sqlEscapeString(context.getString(R.string
                .strdefbussname).trim());
        Cursor c = cr.query(BASEURI, null, where, null, null);
        try {
            if (c.moveToFirst()) {
                bz = c.getString(c.getColumnIndexOrThrow(BUZINESSNAME));
            }
        } finally {
            if (c != null) {
                c.close();
            }
        }
        return bz;
    }

    //help to know if the customer is updating or creating
    public static int isCustomerNew(Context context, String buziness, String mobile) {
        ContentResolver cr = context.getContentResolver();
        String where = BUZINESSNAME + " = "  +DatabaseUtils.sqlEscapeString(buziness)+ " AND " + MOBILE + " = " +
                mobile;
//        String where = BUZINESSNAME + " LIKE \'%" + buziness + "%\'" + " AND " + MOBILE + " = " + mobile;

        Cursor c = cr.query(BASEURI, null, where, null, null);
        try {
            if (c.getCount() > 0) {
                return 1;
            }
        } finally {
            if (c != null) {
                c.close();
            }
        }
        return 0;
    }

    public static int insertCustomer(Context context, String[] string) {
        int customercount = getCustomerCount(context);
        ContentResolver cr = context.getContentResolver();
        ContentValues values = new ContentValues();
        values.put(CUSTOMERNAME, string[0]);
        values.put(BUZINESSNAME, string[1]);
        values.put(REFERENCE, string[2]);
        values.put(AREA, string[3]);
        values.put(CITY, string[4]);
        values.put(MOBILE, string[5]);
        values.put(EMAIL, string[6]);
        values.put(LAT, string[7]);
        values.put(LONGTUDE, string[8]);
        values.put(SYNCSTATUS, 1);
        values.put(STREET, string[10]);
        values.put(VRN, string[11]);
        values.put(TIN, string[12]);
        cr.insert(BASEURI, values);
        if (getCustomerCount(context) == customercount + 1) {
            return 1;
        }
        return customercount;
    }

    public static int updateCustomerDetails(Context context, String[] string) {
        ContentResolver cr = context.getContentResolver();
        String where = ID + " = " + string[5];

        ContentValues values = new ContentValues();
        values.put(CUSTOMERNAME, string[0]);
        values.put(BUZINESSNAME, string[1]);
        values.put(AREA, "");
        values.put(CITY, "");
        values.put(MOBILE, string[2]);
        values.put(EMAIL, string[3]);
        values.put(LAT, 0);
        values.put(LONGTUDE, 0);
        values.put(SYNCSTATUS, 2);
        values.put(STREET, string[4]);
        values.put(VRN, string[6]);
        values.put(TIN, string[7]);

        int customercount = cr.update(BASEURI, values, where, null);

        return customercount;
    }

    public static int deleteCustomer(Context context, String string) {
        ContentResolver cr = context.getContentResolver();
        ContentValues values = new ContentValues();
        values.put(SYNCSTATUS, "3");
        String where = ID + " = " + string;
        if (cr.update(BASEURI, values, where, null) > 0) {
            return 1;
        }
        return 0;
    }

    public static int confirmdeleteCustomer(Context context, String string) {
        ContentResolver cr = context.getContentResolver();
        String where = REFERENCE + " = " + string;
        if (cr.delete(BASEURI, where, null) > 0)
            return 1;
        return 0;
    }

    public static int updateCustomer(Context context, String[] string) {
        ContentResolver cr = context.getContentResolver();
        ContentValues values = new ContentValues();
        values.put(REFERENCE, string[0]);
        String where = ID + " = " + string[1];
        if (cr.update(BASEURI, values, where, null) > 0) {
            return 1;
        }
        return 0;
    }

    public static int insertCustomerLocal(Context context, String[] string) {
        GpsTracker gpsTracker = new GpsTracker(context.getApplicationContext());
        int customercount = getCustomerCount(context);
        ContentResolver cr = context.getContentResolver();
        ContentValues values = new ContentValues();
        values.put(CUSTOMERNAME, string[0]);
        values.put(BUZINESSNAME, string[1]);
        values.put(REFERENCE, 0);
        values.put(AREA, "");
        values.put(CITY, "");
        values.put(MOBILE, string[2]);
        values.put(EMAIL, string[3]);
        values.put(SYNCSTATUS, 0);
        values.put(STREET, string[4]);
        values.put(VRN, string[5]);
        values.put(TIN, string[6]);
        values.put(LAT, gpsTracker.getLatitude());
        values.put(LONGTUDE, gpsTracker.getLongitude());
        cr.insert(BASEURI, values);

        if (getCustomerCount(context) == customercount + 1) {
            return 1;
        } else {
            return 0;
        }
    }

    public static boolean columnExist(SQLiteDatabase context,String column){
        boolean ret= false;
        String sql = " SELECT * FROM "+TABLENAME  +" LIMIT 1";
        Cursor c = context.rawQuery(sql, null);
        try {
            int indext = c.getColumnIndex(column);
            System.out.println(" index is " +indext);
            if(indext != -1)
                ret = true;
        }finally {
            if(c != null) {
                c.close();
            }
        }
        return ret;
    }

    public static boolean updateCustomerFromServer(Context context, String[] string, int flag) {

        ContentResolver cr = context.getContentResolver();
        ContentValues values = new ContentValues();
        values.put(CUSTOMERNAME, string[0]);

        if(flag == 0)
            values.put(BUZINESSNAME, string[1]);

        if(flag == 1)
            values.put(REFERENCE, string[2]);

        values.put(AREA, string[3]);
        values.put(CITY, string[4]);
        values.put(MOBILE, string[5]);
        values.put(EMAIL, string[6]);
        values.put(LAT, string[7]);
        values.put(LONGTUDE, string[8]);
        values.put(SYNCSTATUS, 1);
        values.put(STREET, string[10]);
        values.put(VRN, string[11]);
        values.put(TIN, string[12]);

        String where = "";


        if(flag == 0)
            where = REFERENCE +" = "+string[2];

        if(flag == 1)
            where = BUZINESSNAME +" = "+DatabaseUtils.sqlEscapeString(string[1]);

        System.out.println("where "+where);
        cr.update(BASEURI, values, where, null);
        return true;

    }

    public static int getCustomerCount(Context context) {
        ContentResolver cr = context.getContentResolver();
        String where = SYNCSTATUS + " != 3";
        Cursor c = cr.query(BASEURI, null, where, null, null);
        int count = 0;
        try {
            count = c.getCount();
        } finally {
            if (c != null) {
                c.close();
            }
        }
        return count;
    }

    public static JSONObject getCustomerJson(Context context) throws JSONException {
        JSONObject jobs = new JSONObject();
        jobs.put("tag", Constants.TAGGETCUSTOMER);
        jobs.put(Constants.IMEI, Constants.getIMEINO(context));
        js = new UrlCon();
        System.out.println("json "+jobs.toString()+" json");
        String job = js.getJson(Constants.SERVER, jobs) ;
        JSONObject json;
        try {
            json = new JSONObject(job);
            if (json.length() != 0) {
                return json;
            } else {
            }
        } catch (JSONException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        return null;
    }

    public static JSONObject prepareCustomerToSend(Context context) throws JSONException {
        ContentResolver cr = context.getContentResolver();
        String where = SYNCSTATUS + " != 1";
        Cursor c = cr.query(BASEURI, null, where, null, null);
        if(c.getCount()==0 || c == null)
        {
            c.close();
            return null;
        }
        js = new UrlCon();
        try {
            JSONObject jsonObject = new JSONObject();
            jsonObject.put("tag", Constants.TAGSENDCUSTOMER);
            jsonObject.put(Constants.IMEI, Constants.getIMEINO(context));
            String array;
            if (c.moveToFirst()) {
                JSONArray jarr = new JSONArray();
                do {
                    JSONObject j = new JSONObject();
                    j.put(CUSTOMERNAME, c.getString(c.getColumnIndexOrThrow(CUSTOMERNAME)));
                    j.put(BUZINESSNAME, c.getString(c.getColumnIndexOrThrow(BUZINESSNAME)));
                    j.put(AREA, c.getString(c.getColumnIndexOrThrow(AREA)));
                    j.put(CITY, c.getString(c.getColumnIndexOrThrow(CITY)));
                    j.put(MOBILE, c.getString(c.getColumnIndexOrThrow(MOBILE)));
                    j.put(EMAIL, c.getString(c.getColumnIndexOrThrow(EMAIL)));
                    j.put(STREET, c.getString(c.getColumnIndexOrThrow(STREET)));
                    j.put(LAT, c.getString(c.getColumnIndexOrThrow(LAT)));
                    j.put(LONGTUDE, c.getString(c.getColumnIndexOrThrow(LONGTUDE)));
                    j.put(REFERENCE, c.getString(c.getColumnIndexOrThrow(REFERENCE)));
                    j.put(SYNCSTATUS, c.getString(c.getColumnIndexOrThrow(SYNCSTATUS)));
                    j.put(VRN, c.getString(c.getColumnIndexOrThrow(VRN)));
                    j.put(TIN, c.getString(c.getColumnIndexOrThrow(TIN)));
                    j.put(ID, c.getString(c.getColumnIndexOrThrow(ID)));
                    jarr.put(j);
                } while (c.moveToNext());
                jsonObject.put("incommingcustomers", jarr);
                System.out.println(jsonObject.toString());
                array = js.getJson(Constants.SERVER, jsonObject) ;
                JSONObject job = new JSONObject(array);
                return job;
            }
        } finally {
            if (c != null) {
                c.close();
            }
        }
        return null;
    }

    /*
     * Getting A new Customer or Updating an existing one
     */
    public static void getCustomersFromServer(Context context) throws JSONException {
        JSONObject json = getCustomerJson(context);
        try {
            if (json.has(Constants.SUCCESS)) {
                String res = json.getString(Constants.SUCCESS);
                if (Integer.parseInt(res) == 1) {

                    if (json.has(GETARRAYNAME)) {
                        JSONArray custarray = json.getJSONArray(GETARRAYNAME);
                        int success = 0;
                        for (int i = 0; i < custarray.length(); i++) {
                            JSONObject js = custarray.getJSONObject(i);
                            String pty = js.getString(CUSTOMERNAME);
                            String put = js.getString(BUZINESSNAME);
                            String pdn = js.getString(REFERENCE);
                            String ref = js.getString(AREA);
                            String stc = js.getString(CITY);
                            String txr = js.getString(MOBILE);
                            String upc = js.getString(EMAIL);
                            String lud = js.getString(LAT);
                            String lng = js.getString(LONGTUDE);
                            String syn = "0";
                            String str = js.getString(STREET);
                            String vrn = js.getString(VRN);
                            String tin = js.getString(TIN);
                            if (!Validating.areSet(lud))
                                lud = "0";
                            if (!Validating.areSet(lng))
                                lng = "0";
                            if (!Validating.areSet(stc))
                                stc = "Dar es salaam";
                            if (!Validating.areSet(upc))
                                upc = "example@company.com";
                            if (!Validating.areSet(ref))
                                ref = "Posta";


                            if(pdn.equals("0"))
                                continue;
                            String[] stockvalue = new String[]{pty, put, pdn, ref, stc, txr, upc, lud, lng, syn, str,
                                    vrn,tin};
                            String[] check = new String[]{pty,pdn,put,str};
                            if (Validating.areSet(check)) {// is the customer system id present locally?

                                boolean existbyid = isCustomerExisting(context,pdn);
                                boolean existbybuz = isCustomerValid(context, put);
                                if(!isCustomerExisting(context, pdn) && !isCustomerValid(context,put)){
                                    if(insertCustomer(context,stockvalue) == 1)
                                        success += 1;
                                } else {

                                    if(existbyid)
                                        if(updateCustomerFromServer(context,stockvalue,0))
                                            success += 1;
                                        else
                                        if(existbybuz)
                                            if(updateCustomerFromServer(context,stockvalue,1))
                                                success += 1;
                                }
                            }
                        }
                        if(success == custarray.length())
                            sendAckJson(context);
                    }
                }
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    public static void sendCustomersToServer(Context context) throws JSONException {
        JSONObject json = prepareCustomerToSend(context);
        if(json != null)
            try {
                if (json.has(Constants.SUCCESS)) {
                    String res = json.getString(Constants.SUCCESS);
                    if (Integer.parseInt(res) == 1) {
                        if (json.has(SENDARRAYNAME2)) {

                            JSONArray jsonArray = json.getJSONArray(SENDARRAYNAME2);
                            for (int i = 0; i < jsonArray.length(); i++) {
                                JSONObject js = jsonArray.getJSONObject(i);
                                String pdn = js.getString(REFERENCE);
                                if(pdn.equals("0"))
                                    continue;
                                String customerid = js.getString(ID);
                                String[] stockvalue = new String[]{pdn, customerid};
                                int custidpresent = isCustomerPresent(context, Integer.parseInt(pdn));
                                if (Validating.areSet(stockvalue)) {// is the customer system id present locally?
                                    switch (custidpresent) {
                                        case 0:
                                            if (updateCustomer(context, stockvalue) == 1) {
                                                if (updateSync(context, Integer.parseInt(pdn)) == 1)
                                                    updateSalesCustomer(context,stockvalue);
                                            }
                                        case 1:
                                            //the customer system id is present update the values
                                            if (updateCustomer(context, stockvalue) == 1) {
                                                if (updateSync(context, Integer.parseInt(pdn)) == 1)
                                                    updateSalesCustomer(context,stockvalue);
                                            }
                                            break;
                                    }
                                }
                            }

                        }
                    }
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
    }

    public static int updateSync(Context context, int customerid) {
        ContentResolver cr = context.getContentResolver();
        ContentValues values = new ContentValues();
        values.put(SYNCSTATUS, 1);
        String where = REFERENCE + " = " + customerid;
        if (cr.update(BASEURI, values, where, null) == 1) {
            return 1;
        } else {
            return 0;
        }
    }

    public static void sendAck(Context context, String[] requiredid) throws JSONException {
        JSONObject jobs = new JSONObject();
        jobs.put("tag", Constants.TAGACK);
        jobs.put(Constants.ACK, Constants.ACKVALUE);
        jobs.put(Constants.IMEI, Constants.getIMEINO(context));
        jobs.put(Constants.ACKKEY, REFERENCE);
        jobs.put(REFERENCE, requiredid[0]);
        js = new UrlCon();
        js.getJson(Constants.SERVER, jobs) ;
    }

    public static void sendAckJson(Context context) throws JSONException {
        JSONObject jobs = new JSONObject();
        jobs.put("tag", Constants.TAGACK);
        jobs.put(Constants.ACKITEM, TABLENAME);
        jobs.put(Constants.IMEI, Constants.getIMEINO(context));
        js = new UrlCon();
        js.getJson(Constants.SERVER, jobs) ;
    }

    public static String[] getAllCustomers(Context context) {
        ContentResolver cr = context.getContentResolver();
        Cursor cursor = cr.query(BASEURI, null, null, null, null);
        try {
            if (cursor.getCount() > 0) {
                String[] str = new String[cursor.getCount()];
                int i = 0;
                while (cursor.moveToNext()) {
                    str[i] = cursor.getString(cursor.getColumnIndex(BUZINESSNAME));
                    i++;
                }
                cursor.close();
                return str;
            } else {
                cursor.close();
                return new String[]{};
            }
        } finally {
            if (cursor != null) {
                cursor.close();
            }
        }
    }


    public static int getCustomerid(Context context, String businessnm) {
//        businessnm = businessnm.replace(",", "\'\'");
        ContentResolver cr = context.getContentResolver();
        String where = BUZINESSNAME + " = "+ DatabaseUtils.sqlEscapeString(businessnm);

        Cursor c = cr.query(BASEURI, new String[]{REFERENCE}, where, null, null);
        try {
            int i = 0;
            if (c.moveToFirst()) {
                i = c.getInt(c.getColumnIndexOrThrow(REFERENCE));
                return i;
            }
        } finally {
            if (c != null) {
                c.close();
            }
        }

        return 0;
    }

    public static int getCustomerlocalId(Context context, String businessnm) {
        ContentResolver cr = context.getContentResolver();
        String where = BUZINESSNAME + " = " + DatabaseUtils.sqlEscapeString(businessnm);

        Cursor c = cr.query(BASEURI, new String[]{ID}, where, null, null);
        try {
            int i = 0;
            if (c.moveToFirst()) {
                i = c.getInt(c.getColumnIndexOrThrow(ID));
                return i;
            }
        } finally {
            if (c != null) {
                c.close();
            }
        }
        return 0;
    }

    public static String getCustomerlocalByCustomerRemot(Context context, String str) {
        ContentResolver cr = context.getContentResolver();
        String where = REFERENCE + " = " + str;

        Cursor c = cr.query(BASEURI, new String[]{ID}, where, null, null);
        try {
            String cu = null;
            if (c.moveToFirst()) {
                cu = c.getString(c.getColumnIndexOrThrow(ID));
                return cu;
            }
        } finally {
            if (c != null) {
                c.close();
            }
        }
        return "0";
    }

    public static ArrayList<String> getSearchedCustomer(Context context, String businessnm) {
        ArrayList<String> custlist= new ArrayList<>();
        ContentResolver cr = context.getContentResolver();
        String where = BUZINESSNAME + " = " +DatabaseUtils.sqlEscapeString(businessnm)+" OR "+ CUSTOMERNAME +   " = "
                +DatabaseUtils.sqlEscapeString(businessnm);
//        String where = BUZINESSNAME + " LIKE \'%" + businessnm + "%\' OR "+ CUSTOMERNAME + " LIKE \'%" + businessnm + "%'" ;

        Cursor c = cr.query(BASEURI, new String[]{ID}, where, null, null);
        try {
            int i = 0;
            if (c.moveToFirst()) {
                do {
                    custlist.add(c.getString(c.getColumnIndexOrThrow(ID)));
                } while (c.moveToNext());
            }
        } finally {
            if (c != null) {
                c.close();
            }
        }
        return custlist;
    }

    public static ArrayList<Long> getBussId(Context context) {
        ArrayList<Long> custlist= new ArrayList<>();
        ContentResolver cr = context.getContentResolver();

        Cursor c = cr.query(BASEURI, new String[]{ID}, null, null, null);
        try {
            int i = 0;
            long id = 0;
            custlist.add(id);
            if (c.moveToFirst()) {
                do {
                    custlist.add(c.getLong(c.getColumnIndexOrThrow(ID)));
                } while (c.moveToNext());
            }
        } finally {
            if (c != null) {
                c.close();
            }
        }
        return custlist;
    }

    public static ArrayList<String> getBuss(Context context) {
        ArrayList<String> custlist= new ArrayList<>();
        ContentResolver cr = context.getContentResolver();

        Cursor c = cr.query(BASEURI, new String[]{BUZINESSNAME}, null, null, null);
        try {
            int i = 0;

            custlist.add(context.getString(R.string.strchosecust));
            if (c.moveToFirst()) {
                do {
                    custlist.add(c.getString(c.getColumnIndexOrThrow(BUZINESSNAME)));

                } while (c.moveToNext());
            }
        } finally {
            if (c != null) {
                c.close();
            }
        }
        return custlist;
    }

    public static int isCustomerSynced(Context context, String businessnm) {
        ContentResolver cr = context.getContentResolver();
        String where = ID + " = " + businessnm;

        Cursor c = cr.query(BASEURI, new String[]{SYNCSTATUS}, where, null, null);
        try {
            int i = 0;
            if (c.moveToFirst()) {
                i = c.getInt(c.getColumnIndexOrThrow(SYNCSTATUS));
                return i;
            }
        } finally {
            if (c != null) {
                c.close();
            }
        }
        return 0;
    }


    public static boolean updateSalesCustomer(Context context, String[] string)
    {
        ContentResolver cr = context.getContentResolver();
        ContentValues values = new ContentValues() ;
        values.put(Sales.CUSTOMERID, string[0]);
        String where = Sales.LOCALCUSTOMER + " = "+ string[1] ;
        cr.update(Sales.BASEURI, values, where, null);
        cr.update(StockIssue.BASEURI, values, where, null);
        return true;
    }

    public static String[] getCustomer(Context context, long id) {
        ContentResolver cr = context.getContentResolver();
        String where = ID + " = " + id;

        Cursor c = cr.query(BASEURI, null, where, null, null);
        String[] custdetails;
        try {
            if (c.moveToFirst()) {
                String n = c.getString(c.getColumnIndexOrThrow(CUSTOMERNAME));
                String b = c.getString(c.getColumnIndexOrThrow(BUZINESSNAME));
                String e = c.getString(c.getColumnIndexOrThrow(EMAIL));
                String p = c.getString(c.getColumnIndexOrThrow(MOBILE));
                String a = c.getString(c.getColumnIndexOrThrow(STREET));
                String ar = c.getString(c.getColumnIndexOrThrow(AREA));
                String ct = c.getString(c.getColumnIndexOrThrow(CITY));
                String vr = c.getString(c.getColumnIndexOrThrow(VRN));
                String tin = c.getString(c.getColumnIndexOrThrow(TIN));
                String g = a + " " + ar + " " + ct;
                custdetails = new String[]{n, b, e, p, g,vr,tin};
                return custdetails;
            }
        } finally {
            if (c != null) {
                c.close();
            }
        }
        return null;
    }

    public static String[] getCustomers(Context context, long reference) {
        ContentResolver cr = context.getContentResolver();
        String where = ID + " = " + reference;

        Cursor c = cr.query(BASEURI, null, where, null, null);
        String[] custdetails;
        try {
            if (c.moveToFirst()) {
                String n = c.getString(c.getColumnIndexOrThrow(CUSTOMERNAME));
                String b = c.getString(c.getColumnIndexOrThrow(BUZINESSNAME));
                String e = c.getString(c.getColumnIndexOrThrow(EMAIL));
                String p = c.getString(c.getColumnIndexOrThrow(MOBILE));
                String a = c.getString(c.getColumnIndexOrThrow(STREET));
                String ar = c.getString(c.getColumnIndexOrThrow(AREA));
                String ct = c.getString(c.getColumnIndexOrThrow(CITY));
                String g = a + " " + ar + " " + ct;
                custdetails = new String[]{n, b, e, p, g};
                return custdetails;
            }
        } finally {
            if (c != null) {
                c.close();
            }
        }
        return null;
    }

    public static String getCustomerName(Context context, long reference) {
        ContentResolver cr = context.getContentResolver();
        String where = ID + " = " + reference;

        Cursor c = cr.query(BASEURI, null, where, null, null);
        String custdetails = "";
        try {
            if (c.moveToFirst()) {
                String b = c.getString(c.getColumnIndexOrThrow(BUZINESSNAME));
                custdetails = b;
            }
        } finally {
            if (c != null) {
                c.close();
            }
        }
        return custdetails;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getReference() {
        return reference;
    }

    public void setReference(int reference) {
        this.reference = reference;
    }

    public int getSync() {
        return sync;
    }

    public void setSync(int sync) {
        this.sync = sync;
    }

    public String getCname() {
        return cname;
    }

    public void setCname(String cname) {
        this.cname = cname;
    }

    public String getBname() {
        return bname;
    }

    public void setBname(String bname) {
        this.bname = bname;
    }

    public String getArea() {
        return area;
    }

    public void setArea(String area) {
        this.area = area;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getMob() {
        return mob;
    }

    public void setMob(String mob) {
        this.mob = mob;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getLat() {
        return lat;
    }

    public void setLat(String lat) {
        this.lat = lat;
    }

    public String getLng() {
        return lng;
    }

    public void setLng(String lng) {
        this.lng = lng;
    }

    public String getStreet() {
        return street;
    }

    public void setStreet(String street) {
        this.street = street;
    }

    public static void createDefault(Context context) {
        String fname = context.getString(R.string.strdefcustname);
        String bname = context.getString(R.string.strdefbussname);
        String mobile = "0000-000000";
        String area = "Agrey St Kkoo Dar es salaam";
        String email = "default@email.com";
        String vrn = "";
        String tin = "";
        String[] vals = {fname,bname,mobile,email,area,vrn,tin};
        Customers.insertCustomerLocal(context, vals);
    }
}
