/*
 * The source codes in this project are owned by Bongolive Enterprises. Copying or any kind of usage without Bongolive's approval is against the law.
 * Copyright (c) 2015.
 *
 */

package bongolive.apps.proshop.view;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.ContextWrapper;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Path;
import android.graphics.RectF;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.AttributeSet;
import android.util.Log;
import android.view.ContextMenu;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.AbsListView;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.SpinnerAdapter;
import android.widget.TextView;
import android.widget.Toast;

import java.io.File;
import java.io.FileOutputStream;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.ArrayList;
import java.util.Locale;

import bongolive.apps.proshop.R;
import bongolive.apps.proshop.controller.AppPreference;
import bongolive.apps.proshop.controller.Constants;
import bongolive.apps.proshop.controller.SalesItemAdapter;
import bongolive.apps.proshop.controller.SalesSummaryAdapter;
import bongolive.apps.proshop.controller.Validating;
import bongolive.apps.proshop.model.Customers;
import bongolive.apps.proshop.model.MultimediaContents;
import bongolive.apps.proshop.model.Payments;
import bongolive.apps.proshop.model.Products;
import bongolive.apps.proshop.model.Sales;
import bongolive.apps.proshop.model.SalesItems;
import bongolive.apps.proshop.model.Settings;
import bongolive.apps.proshop.model.Warehouse;


public class SalesFrag extends Fragment implements View.OnCreateContextMenuListener, View.OnKeyListener,
        TextWatcher {
    View _rootView;
    private String customeridLocal = "";
    private String customerid = "";/*customerid captures the customer id*/

    private TextView txreportdata;
    private ListView lvitems;
    private AutoCompleteTextView edclientname;
    private AutoCompleteTextView etproduct;
    private EditText etquantity;
    private EditText etprice;
    private EditText etpartial;
    private EditText etdiscount;
    private Dialog dialogmode;
    private AppPreference appPreference;
    private boolean tabletSize = false;

    private ArrayList<String> productlist;
    private ArrayList<String> productcodelist;
    private TextView txtorderitem;
    private int payinfull = 0;
    private boolean discountable = false;

    private String whname = "";
    private String whid = "";
    private ArrayList<String> whlistid;
    private ArrayList<SalesItems> salesItems = new ArrayList<>();
    private SalesItemAdapter adapter;
    private LinearLayout layprod;
    private LinearLayout layqty;
    private LinearLayout layrep;
    private LinearLayout laycust;
    private ImageButton imgaddcust;
    private View topb;
    private View lowb;
    private LinearLayout rdiscount;
    private Toolbar ab;
    private boolean business = false;
    /*
    * new strings
    */
    private String __prodcode = "";
    private String __prodname = "";
    private boolean __taxsettings = false;

    String curr;


    MenuItem menuItem;


    /*
    * end of new strings
    */

    public SalesFrag(){

    }
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        _rootView = inflater.inflate(R.layout.addsales, container, false);
        tabletSize = getResources().getBoolean(R.bool.isTablet);
        return _rootView;
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        appPreference = new AppPreference(getContext());
        appPreference.setShowPrice(true);
        __taxsettings = Settings.getTaxEnabled(getContext());

        createSalesForm();
        curr = appPreference.getDefaultCurrency();
        curr = curr != null ? curr : "Tsh";
        ab = ((MainScreen) this.getActivity()).getmToolbar();
        setHasOptionsMenu(true);
    }

    private void showToast(String txt) {
        Toast.makeText(getContext(), txt, Toast.LENGTH_LONG).show();
    }

    private void createSalesForm() {
        lvitems = (ListView) getActivity().findViewById(R.id.orderlist);
        adapter = new SalesItemAdapter(getContext(), salesItems);
        lvitems.setAdapter(adapter);

        lvitems.setOnScrollListener(new AbsListView.OnScrollListener() {
            private int mLastFirstVisibleItem;

            @Override
            public void onScrollStateChanged(AbsListView view, int scrollState) {

            }

            @Override
            public void onScroll(AbsListView view, int firstVisibleItem,
                                 int visibleItemCount, int totalItemCount) {

                if (mLastFirstVisibleItem < firstVisibleItem) {
                    Log.i("SCROLLING DOWN", "TRUE");
                }
                if (mLastFirstVisibleItem > firstVisibleItem) {
                    Log.i("SCROLLING UP", "TRUE");
                }
                mLastFirstVisibleItem = firstVisibleItem;

            }

        });

        lvitems.setOnCreateContextMenuListener(new View.OnCreateContextMenuListener() {

            @Override
            public void onCreateContextMenu(ContextMenu menu, View view,
                                            ContextMenu.ContextMenuInfo menuInfo) {
                getActivity().getMenuInflater().inflate(R.menu.removesale, menu);
                menu.setHeaderTitle(getResources().getString(R.string.strremovesale));
                menu.setHeaderIcon(R.drawable.ic_done_all_white_24dp);
                AdapterView.AdapterContextMenuInfo info = (AdapterView.AdapterContextMenuInfo) menuInfo;
                int position = info.position;
                SalesItems list = salesItems.get(position);
                Toast.makeText(getActivity(), "ok " + list.getPrice(), Toast.LENGTH_LONG).show();
                double sales = list.getPrice();
                String total = txreportdata.getText().toString();
                if (Validating.areSet(total)) {
                    double remained = Double.parseDouble(total);
                    double rem = remained - sales;
                    txreportdata.setText(String.valueOf(rem));
                }
                salesItems.remove(position);
                adapter.notifyDataSetChanged();
                txtorderitem.setText(getString(R.string.straddeditem) + " (" + salesItems.size() + ")");
            }
        });
        String orderstatus = " (" + salesItems.size() + ")";
        txtorderitem = (TextView) getActivity().findViewById(R.id.txtlabel);
        txtorderitem.append(orderstatus);
        rdiscount = (LinearLayout) getActivity().findViewById(R.id.rddiscount);

        etdiscount = (EditText) getActivity().findViewById(R.id.etdiscount);
        CheckBox chkdiscount = (CheckBox) getActivity().findViewById(R.id.chkdiscount);
        layprod = (LinearLayout) getActivity().findViewById(R.id.layproduct);
        layqty = (LinearLayout) getActivity().findViewById(R.id.lyqty);
        layrep = (LinearLayout) getActivity().findViewById(R.id.layreport);
        laycust = (LinearLayout) getActivity().findViewById(R.id.laycustomer);
        imgaddcust = (ImageButton)getActivity().findViewById(R.id.imgaddcustmer);
        if(!Settings.getAddCustomerEnabled(getContext()))
            imgaddcust.setVisibility(View.GONE);
        imgaddcust.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                addcustomer();
            }
        });
        topb = getActivity().findViewById(R.id.topborder);
        lowb = (View) getActivity().findViewById(R.id.lowerborder);


/*
        if(!Settings.getDiscountEnabled(getContext()))
            chkdiscount.setEnabled(false);*/

        chkdiscount.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    etdiscount.setHintTextColor(Color.GRAY);
                    discountable = true;
                    etdiscount.setVisibility(View.VISIBLE);
                }
                if (!isChecked) {
                    discountable = false;
                    etdiscount.setVisibility(View.INVISIBLE);
                }
            }
        });

        String[] businesslist = Customers.getAllCustomers(getContext());

        productlist = Products.getAllProducts(getContext());
        productcodelist = Products.getAllProductsCode(getContext());

        ArrayAdapter<String> adapters = new ArrayAdapter<>(getContext(), android.R.layout.select_dialog_item, businesslist);
        ArrayAdapter<String> proadapter = new ArrayAdapter<>(getContext(), android.R.layout.select_dialog_item, productlist);

        etproduct = (AutoCompleteTextView) getActivity().findViewById(R.id.autoproduct00);
        txreportdata = (TextView) getActivity().findViewById(R.id.txtreportdata);
        edclientname = (AutoCompleteTextView) getActivity().findViewById(R.id.autotxtsaleclientname);


        if (!business) {
            layprod.setVisibility(View.GONE);
            layqty.setVisibility(View.GONE);
            layrep.setVisibility(View.GONE);
            rdiscount.setVisibility(View.GONE);
            topb.setVisibility(View.GONE);
            lowb.setVisibility(View.GONE);
            lvitems.setVisibility(View.GONE);
            if (tabletSize) {
                System.out.println(" tablet");
                topb.setVisibility(View.VISIBLE);
                lowb.setVisibility(View.VISIBLE);
                lvitems.setVisibility(View.VISIBLE);
                layrep.setVisibility(View.INVISIBLE);
            }
        } else {
            edclientname.setText("");
            laycust.setVisibility(View.GONE);
            layprod.setVisibility(View.VISIBLE);
            layqty.setVisibility(View.VISIBLE);
            layrep.setVisibility(View.VISIBLE);

            if (Settings.getDiscountEnabled(getContext()))
                rdiscount.setVisibility(View.VISIBLE);
            lvitems.setVisibility(View.VISIBLE);
            topb.setVisibility(View.VISIBLE);
            lowb.setVisibility(View.VISIBLE);

            getActivity().getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
        }

        etproduct.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if (!hasFocus) {
                    if (business) {
                        String productname = etproduct.getText().toString();
                        if (TextUtils.isEmpty(productname)) {
                            etproduct.clearFocus();
//                            etquantity.clearFocus();
                            Toast.makeText(getActivity(), getResources().getString(R.string.strfilldata), Toast
                                    .LENGTH_LONG).show();
                        }
                        if (Validating.areSet(productname)) {
                            __prodcode = Products.getProductIdUsingSku(getActivity(), productname);
                            System.out.println(" product code is " + __prodcode);
                            double pr = Products.getUnitPrice(getActivity(), __prodcode);
                            String sku = Products.getSku(getActivity(), __prodcode);
                            etprice.setText(getString(R.string.strprice) + " " + pr);
                            ab.setSubtitle(getString(R.string.strsku2) + " " + sku);
                        }
                    }
//                    start = false;
                }
            }
        });
        etproduct.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                String pro = (String) parent.getItemAtPosition(position);
                etproduct.setText(pro);
                __prodname = pro;
                whid = Warehouse.getDefaultId(getActivity());

                int pos = productlist.indexOf(pro);
                __prodcode = productcodelist.get(pos);
                double pr = Products.getUnitPrice(getActivity(), __prodcode);
                String sku = Products.getSku(getActivity(), __prodcode);
                ab.setSubtitle(getString(R.string.strsku2) + " " + sku);
                etprice.setText(getString(R.string.strprice) + " " + pr);
                etproduct.clearFocus();
                /*
                InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                imm.hideSoftInputFromWindow(getCurrentFocus().getWindowToken(), 0);*/

            }
        });

        etproduct.setAdapter(proadapter);
        etproduct.setThreshold(1);


        Spinner spwarehouse = (Spinner) getActivity().findViewById(R.id.spwarehouse);
        ArrayList<String> whlist = Warehouse.getAll(getContext());
        whlistid = Warehouse.getAllIds(getContext());

        SpinnerAdapter spwhadapter = new ArrayAdapter<>(getContext(), android.R.layout.simple_list_item_1, whlist);
        spwarehouse.setAdapter(spwhadapter);

        if (Warehouse.getCount(getContext()) > 1) {
            String wh = (String) spwhadapter.getItem(1);
            whid = whlistid.get(1);
            System.out.println("wh is " + wh);
            if (!wh.equals(null)) {
                spwarehouse.setSelection(1);
            }
        } else if (Warehouse.getCount(getContext()) > 0) {
            String wh = (String) spwhadapter.getItem(1);
            whid = whlistid.get(1);
            System.out.println("wh is " + wh);
            if (!wh.equals(null)) {
                spwarehouse.setSelection(1);
            }
        }
        spwarehouse.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                whname = "";
                whid = "";
                if (position > 0) {
                    whname = (String) parent.getItemAtPosition(position);
                    whid = whlistid.get(position);
                    System.out.println("whname is " + whname + " whid is " + whid);
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
        spwarehouse.setVisibility(View.GONE);
        etquantity = (EditText) getActivity().findViewById(R.id.autoquantity00);

        etquantity.setOnFocusChangeListener(new View.OnFocusChangeListener() {

            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if (hasFocus) {
                    System.out.println("etquantity has focus");
//                    etprice.setEnabled(true);
//                    etproduct.clearFocus();
                    etprice.setText("");
                    if (!__prodcode.isEmpty()) {
                        double pr = Products.getUnitPrice(getActivity(), __prodcode);
                        String sku = Products.getSku(getActivity(), __prodcode);
                        etprice.setText(getString(R.string.strprice) + " " + pr);
                        ab.setSubtitle(getString(R.string.strsku2) + " " + sku);
                    }

                }
            }
        });
        etquantity.setOnKeyListener(this);

        etprice = (EditText) getActivity().findViewById(R.id.autoprice00);
        etprice.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if (hasFocus) {
                    process_item();
                }
            }
        });

        edclientname.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                String name = (String) parent.getItemAtPosition(position);
                edclientname.setText(name);
                customerid = String.valueOf(Customers.getCustomerid(getActivity(), name));
                customeridLocal = String.valueOf(Customers.getCustomerlocalId(getActivity(), name));
            }
        });

        edclientname.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if(start ==1 ){
                    System.out.println("no customer");
                }
            }

            @Override
            public void afterTextChanged(Editable s) {
                if(s.length() == 0){
//                    Toast.makeText(getActivity(), getString(R.string.straddcustomer)+"?",Toast.LENGTH_SHORT).show();
                }
            }
        });

        if(Customers.getCustomerCount(getContext()) >= 1){
            String name = businesslist[0];

            edclientname.setText(name);
            customerid = String.valueOf(Customers.getCustomerid(getActivity(), name));
            customeridLocal = String.valueOf(Customers.getCustomerlocalId(getActivity(), name));

            edclientname.setAdapter(adapters);
            edclientname.setThreshold(1);
            edclientname.requestFocus();
        }

        edclientname.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (Validating.areSet(edclientname.getText().toString())) {
                    edclientname.setText("");
                }
            }
        });
        etproduct.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (Validating.areSet(etproduct.getText().toString())) {
                    etproduct.setText("");
                    etprice.setText("");
                    etquantity.setText("");
                    __prodcode = "";
                    ab.setSubtitle("");
                }
            }
        });
    }

    private void process_item() {
        System.out.println("whid " + whid);
        if(whid.equals("") || whid.isEmpty() || whid.equals(getString(R.string.strselectwarehouse)) || whid.equals(0))
        {
            whid = Warehouse.getDefaultId(getContext());
        }
        System.out.println("now whid " + whid);
        String qq = etquantity.getText().toString().trim();
        if(!Constants.checkDouble(getContext(), qq, etquantity))
            return;

        String prd = etproduct.getText().toString();
        if(!prd.isEmpty()){
            int prese = Products.isProductPresent(getContext(), new String[]{prd});
            if(prese == 0 ){
                etproduct.setText("");
                __prodcode = "";
                __prodname = "";
                etproduct.setHint(getString(R.string.strinvalidproductwarn));
                etproduct.setHintTextColor(Color.RED);
                etproduct.requestFocus();
                return;
            }
        }

        String p = __prodcode;
        etprice.setText("");
        if (Validating.areSet(p, qq)) {
            etproduct.setHintTextColor(Color.GRAY);
            etproduct.setHint(getString(R.string.strproductname));
            int qa = Products.getAllProductQuantity(getActivity(), p);
            int q =  new BigDecimal(qq).intValue();
            int taxmethod = Products.getTaxmethod(getActivity(), p);
            if (qa >= q && q > 0) {
                double tx = Products.getTax(getActivity(), p);
                double pr = Products.getUnitPrice(getActivity(), p);
                double maxdis = Settings.getDiscountLimit(getContext());
                //proceed now we have quantity and product code
                //check if it is discountable
                double taxamount = 0;
                double netsaleprice = 0;
                double saleprice = 0;
                double subtotal = 0;
                long prodlocal = Products.getProdid(getContext(), p);
                String prod = Products.getProdid(getContext(),prodlocal);
                if(prodlocal == 0)
                    return;
                long i = 0;
                if (!salesItems.isEmpty()) {
                    i = adapter.getCount() + 1;
                } else {
                    i = 1;
                }
                if(discountable){
                    //discountable
                    String discprice = etdiscount.getText().toString().trim();
                    if(!Constants.checkDouble(getContext(),discprice,etdiscount))
                        return;
                    if (Validating.areSet(discprice)) {

                        double pdiscount = new BigDecimal(discprice).setScale(2, BigDecimal
                                .ROUND_HALF_UP).doubleValue();
                        double prdiscount = 0;
                        double checkdis = Constants.isDiscount(new double[]{pr,pdiscount,maxdis});
                        boolean error = false;
                        if(checkdis != 0)
                        {
                            etquantity.setText("");
                            etdiscount.setText("");
                            etdiscount.setHintTextColor(Color.RED);
                            etdiscount.setHint(getString(R.string.strdiscountlimitwarn) + " " + checkdis);
                            etdiscount.requestFocus();
                            error = true;
                            return;
                        }

                        if (pdiscount > 0) {//discounted price should be less than unit price
                            if(error){
                                etdiscount.setHint(getString(R.string.strproduct));
                                etdiscount.setHintTextColor(Color.DKGRAY);
                                error = false;
                            }
                            prdiscount = new BigDecimal(pr).subtract(new BigDecimal(pdiscount)).setScale(2,
                                    RoundingMode.HALF_UP).doubleValue();
                            System.out.println("new price with discount "+prdiscount);
                            double tax = Constants.itemtax(prdiscount,tx,taxmethod,__taxsettings);

                            netsaleprice = Constants.netprice(prdiscount,tx,taxmethod, __taxsettings);
                            saleprice = Constants.saleprice(prdiscount, tax, taxmethod, __taxsettings);

                            subtotal = Constants.calculate_grandtotal(saleprice,q);
                            double netsubtotal = Constants.calculate_grandtotal(netsaleprice,q);
                            taxamount = new BigDecimal(subtotal).subtract(new BigDecimal(netsubtotal)).setScale(2,
                                    RoundingMode.HALF_UP).doubleValue();
                            System.out.println("tax method is " + taxmethod + " tax settings is " +
                                    "" + __taxsettings + " netprice is " + netsaleprice + " saleprice = " +
                                    saleprice + " subtotoal " + subtotal + " netsubtotal " + netsaleprice + " " +
                                    "taxamount " + taxamount);
                            etprice.setText(String.valueOf(subtotal));

                            if(q > 0){
                                salesItems.add(new SalesItems(prodlocal,whid,i,q,netsaleprice,saleprice,taxamount,
                                        subtotal,tx,saleprice,prod));

                                adapter = new SalesItemAdapter(getActivity(), salesItems);
                                adapter.notifyDataSetChanged();
                                txtorderitem.setText(getString(R.string.straddeditem) + " (" + salesItems.size() + ")");
                                lvitems.setAdapter(adapter);

                                double reportdata;
                                String strfinalprice = txreportdata.getText().toString();
                                if (TextUtils.isEmpty(strfinalprice)) {
                                    reportdata = 0;
                                } else {
                                    reportdata = Double.parseDouble(strfinalprice);
                                }
                                double total = subtotal + reportdata;
                                String totals = String.valueOf(total);
                                txreportdata.setText(totals);
                                txreportdata.setText(totals);
                                __prodcode = "";
                                saleprice = 0;
                                netsaleprice = 0;
                                subtotal = 0;
                                q = 0;
                                pdiscount = 0;
                                prdiscount = 0;
                                etprice.setText("");
                                etquantity.setText("");
                                etproduct.setText("");
                                etproduct.requestFocus();
                            }
                        } else {
                            Toast.makeText(getActivity(), getString(R.string.stramountwarn), Toast
                                    .LENGTH_LONG).show();
                            etdiscount.setText("");
                        }
                    } else {
                        Toast.makeText(getActivity(), getString(R.string.strdiscountwarn), Toast
                                .LENGTH_LONG).show();
                        etdiscount.requestFocus();
                    }
                } else {
                    double tax = Constants.itemtax(pr,tx,taxmethod,__taxsettings);
                    netsaleprice = Constants.netprice(pr, tx, taxmethod, __taxsettings);
                    saleprice = Constants.saleprice(pr, tax, taxmethod, __taxsettings);

                    subtotal = Constants.calculate_grandtotal(saleprice,q);
                    double netsubtotal = Constants.calculate_grandtotal(netsaleprice,q);
                    taxamount = new BigDecimal(subtotal).subtract(new BigDecimal(netsubtotal)).setScale(2,
                            RoundingMode.HALF_UP).doubleValue();
                    System.out.println("tax method is "+taxmethod+" tax settings is " +
                            ""+__taxsettings + " netprice is "+netsaleprice + " saleprice = " +
                            saleprice+" subtotoal "+subtotal+" netsubtotal "+netsaleprice+ " " +
                            "taxamount "+taxamount);

                    etprice.setText(String.valueOf(subtotal));
                    if(q > 0){

                        salesItems.add(new SalesItems(prodlocal,whid,i,q,netsaleprice,saleprice,taxamount,
                                subtotal,tx,0,prod));
                        adapter = new SalesItemAdapter(getActivity(), salesItems);
                        adapter.notifyDataSetChanged();
                        txtorderitem.setText(getString(R.string.straddeditem) + " (" + salesItems.size() + ")");
                        lvitems.setAdapter(adapter);


                        double reportdata;
                        String strfinalprice = txreportdata.getText().toString();
                        if (TextUtils.isEmpty(strfinalprice)) {
                            reportdata = 0;
                        } else {
                            reportdata = Double.parseDouble(strfinalprice);
                        }
                        double total = subtotal + reportdata;
                        String totals = String.valueOf(total);
                        txreportdata.setText(totals);
                        __prodcode = "";
                        saleprice = 0;
                        netsaleprice = 0;
                        subtotal = 0;
                        q = 0;
                        etprice.setText("");
                        etquantity.setText("");
                        etproduct.setText("");
                        etproduct.requestFocus();

                    }

                }

            } else {
                Toast.makeText(getActivity(), p + " " + getResources().getString(R.string.strsalesquantityerror1) +
                        " " + qa + " " + getResources().getString(R.string.strsalesquantityerror2), Toast.LENGTH_LONG).show();
                etquantity.setText("");
                etquantity.setHint(qa + " " + getString(R.string.strqtymax));
                etprice.setText("");
                etquantity.requestFocus();
            }
        }

        etdiscount.setText("");
        etquantity.setText("");
        ab.setSubtitle("");
        etprice.setText("");
    }


    public void showStation() {

            DeliverItemsFrag newFragment = new DeliverItemsFrag();
            Bundle args = new Bundle();
//            args.putLong(Transfer.ID, pyid);
            newFragment.setArguments(args);

            FragmentTransaction transaction = getFragmentManager().beginTransaction();
            transaction.replace(R.id.content_frame, newFragment);
//            transaction.addToBackStack(String.valueOf(pyid));
            transaction.commit();
    }

    private void restart(){
        getFragmentManager().popBackStack(null, FragmentManager.POP_BACK_STACK_INCLUSIVE);
        SalesFrag newFragment = new SalesFrag();
        Bundle args = new Bundle();
        newFragment.setArguments(args);
        FragmentTransaction transaction = getFragmentManager().beginTransaction();
        transaction.replace(R.id.content_frame, newFragment, SignHere.class.getName());
        transaction.commit();
    }
    private void save_order(String[] valz, String[] pass) {
        SalesItems list = null;
        System.out.println("commments " + valz[1]);
        if (!customeridLocal.isEmpty() || !customeridLocal.equals("0")) {
            if (!valz[0].isEmpty()) {
                String cust = String.valueOf(customerid);
                String custloc = String.valueOf(customeridLocal);
                String paid = valz[0];
                double totalsales = 0;
                double totaltax = 0;
                double totaldiscount = 0;
                int totalitems = salesItems.size();
                String batch = valz[3];
                String taxset = String.valueOf(__taxsettings);
                String printed = null;
                String paymod = valz[4];
                boolean toprint = false;
                System.out.println("PRINT is "+valz[2]);
                if(valz[2].equals("1")) {
                    printed = "1";
                    toprint = true;
                }
                if(valz[2].equals("0")) {
                    printed = "0";
                    toprint  = false;
                }
                String paystatus = null;

                if (salesItems.size() > 0) {
                    for (int i = 0; i < salesItems.size(); i++) {
                        list = salesItems.get(i);
                        totalsales += list.getSbtotal();
                        totaltax += list.getTaxmt();
                    }
                }
                double _paymnt = new BigDecimal(paid).doubleValue();
                double _tsale = new BigDecimal(totalsales).doubleValue();
                if(_paymnt == _tsale)
                    paystatus = Constants.PAY_STATUS_CMPLT;
                else
                    paystatus = Constants.PAY_STATUS_PNDG;

                String lat = "-6.8147935";
                String lng = "39.2784206";
                float[] lc = appPreference.getLastKnowLocation();
                lat = String.valueOf(lc[0]);
                lng = String.valueOf(lc[1]);

                String[] vals = {cust,paid,String.valueOf(totalsales),String.valueOf(totaltax),String.valueOf
                        (totaldiscount),String.valueOf(totalitems),batch,valz[1],taxset,printed,paystatus,custloc,
                        lat,lng,Constants.SALE_STATUS_CMPLT,Constants.SALETYPE_SALE};
                String[] checkvals = {cust,paid,String.valueOf(totalsales),String.valueOf(totaltax),String.valueOf
                        (totaldiscount),String.valueOf(totalitems),batch,taxset,printed,paystatus,custloc};
                int orderid = 0;
                if(Validating.areSet(checkvals)){
                    if(Sales.insert(getContext(), vals, Constants.LOCAL)){
                        orderid = Sales.getId(getContext());
                        String payref = Constants.generatePayref();
                        double postb = _tsale-_paymnt;
                        String pb = String.format("%.2f", postb);
                        String[] info = appPreference.getAccountInfo();
                        String receiver = !info[1].isEmpty() ? info[1] : "Demo Contact Person";

                        System.out.println("orderid "+orderid);
                        if(pass != null) {
                            String[] pay = {"0",pb,valz[1],Constants.getDate(),paid,String.valueOf(orderid)
                                    ,"0" , paymod, pass[0], Constants.PAYTYPE_RECEIVED,"0",payref,receiver,"0"};

                            if(Validating.areSet(pay)){
                                Payments.insert(getContext(), pay);
                            }
                            String[] med = {pass[0], pass[1], pass[2], batch};

                            if (Validating.areSet(med)) {
                                boolean stored = MultimediaContents.storeMultimedia(getContext(), med);
                                if (stored) {
                                    System.out.println("signature stored ? " + stored);
                                }
                            }
                        } else {
                            String[] pay = {"0",pb,valz[1],Constants.getDate(),paid,String.valueOf(orderid)
                                    ,"0" , paymod, "none", Constants.PAYTYPE_RECEIVED,"0",payref,receiver,"0"};

                            if(Validating.areSet(pay)){
                                Payments.insert(getContext(),pay);
                            }
                        }
                    }
                }
                final int[] insert = new int[]{0};
                if(orderid != 0){
                    for(int i = 0; i < salesItems.size(); i++){
                        list = salesItems.get(i);
                        String prodno = list.getProd();
                        String locprod = String.valueOf(list.getProdloc());
                        String qty = String.valueOf(list.getQty());
                        String netprc = String.valueOf(list.getNetprice());
                        String saleprc = String.valueOf(list.getPrice());
                        String taxam = String.valueOf(list.getTaxmt());
                        String subto = String.valueOf(list.getSbtotal());
                        String wh = list.getWh();
                        String disc = String.valueOf(list.getDiscount());
                        String taxr = String.valueOf(list.getTaxrt());
                        String order = String.valueOf(orderid);
                        String saleref = batch;
                        String[] items = {prodno,locprod,qty,netprc,saleprc,taxam,subto,wh,disc,taxr,order,
                                saleref,Constants.SALE_STATUS_CMPLT,Constants.SALETYPE_SALE};
                        if(Validating.areSet(items)){
                            System.out.println("locprod "+locprod);
                            if(SalesItems.insertItems(getContext(),items,Constants.LOCAL) ==1){
                                String isservice = Products.getProductType(getContext(),locprod);
                                System.out.println("product type is "+isservice);
                                if(isservice.equals(Constants.PRODUCT_STANDARD)){
                                    int prodid = Integer.parseInt(locprod);
                                    Products.updateProductQty(getContext(), new int[]{prodid,list.getQty(), 1});
                                }
                                insert[0] += i ;
                            }
                        }
                    }
                }

                if(toprint){
                    String app = "app.incotex.fpd";
                    if(Constants.isPackageInstalled(app,getContext())) {
                        int send = Constants.writeReceipt(getContext(), salesItems,__taxsettings,customeridLocal);
                        startActivity(getContext().getPackageManager().getLaunchIntentForPackage(app));
                        getActivity().setResult(-1, new Intent());
                        showToast(getString(R.string.strorderadded));
                        if (send == 1) {
                            Sales.putPrintStatus(getContext(), new int[]{orderid, send});
                            customerid = "";
                            customeridLocal = "";
                            salesItems.clear();
                            whid = null;
                            toprint = false;
                            valz = null;
                            adapter.notifyDataSetChanged();
                            lvitems.setAdapter(adapter);
                            txreportdata.setText("");
                            txtorderitem.setText("");
                            business = false;
                        }
                    } else {
                        showToast(getString(R.string.strorderadded));
                        showToast(getString(R.string.strnoprinterapp));
                        customerid = "";
                        customeridLocal = "";
                        salesItems.clear();
                        whid = null;
                        printed = "0";
                        toprint = false;
                        valz = null;
                        adapter.notifyDataSetChanged();
                        lvitems.setAdapter(adapter);
                        txreportdata.setText("");
                        txtorderitem.setText("");
                        business = false;
                    }
                } else {
                    showToast(getString(R.string.strorderadded));
                    customerid = "" ;
                    customeridLocal = "";
                    salesItems.clear();
                    whid = null;
                    printed = "0";
                    toprint = false;
                    valz = null;
                    adapter.notifyDataSetChanged();
                    lvitems.setAdapter(adapter);
                    txreportdata.setText("");
                    txtorderitem.setText("");
                    business = false;
                }
            }

        }

        layprod.setVisibility(View.GONE);
        layqty.setVisibility(View.GONE);
        layrep.setVisibility(View.GONE);
        rdiscount.setVisibility(View.GONE);
        topb.setVisibility(View.GONE);
        lowb.setVisibility(View.GONE);
        if(tabletSize){
            topb.setVisibility(View.VISIBLE);
            lowb.setVisibility(View.VISIBLE);
            lvitems.setVisibility(View.VISIBLE);
            layrep.setVisibility(View.INVISIBLE);
        }
        laycust.setVisibility(View.VISIBLE);
        restart();
    }

    public class ProcessSale extends Fragment{
        final String[] __notes = new String[]{""};
        final String[] __paidamount = new String[]{""};
        final String[] __printable = new String[]{""};
        final String[] __paymode = new String[]{""};
        final String[] __paystatus = new String[]{""};
        Spinner sppaymethods;
        EditText comm ;
        ImageView submit;
        TextView txtfinal;
        final double[] mfinalcost = new double[]{0};
        double totalax = 0;
        double totalsale = 0;
        double subtotal = 0;
        SalesItems list = null;
        ArrayAdapter adapter ;
        Toolbar tbar;


        public ProcessSale(){

        }
        @Override
        public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
        {
            View view = inflater.inflate(R.layout.paymentmethod, container, false);
            return view ;
        }

        @Override
        public void onActivityCreated(Bundle savedInstanceState)
        {
            super.onActivityCreated(savedInstanceState);
            appPreference = new AppPreference(getContext());
            txtfinal = (TextView)getActivity().findViewById(R.id.txttitle);
            if (salesItems.size() > 0) {
                for (int i = 0; i < salesItems.size(); i++) {
                    list = salesItems.get(i);
                    mfinalcost[0] += list.getSbtotal();
                    totalax += list.getTaxmt();
                    subtotal += list.getSbtotal();
                }
            }
            tbar = ((MainScreen)this.getActivity()).getmToolbar();
//            tbar.setNavigationIcon(R.drawable.ic_arrow_back_white_36dp);
            tbar.setNavigationOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    SalesFrag newFragment = new SalesFrag();
                    FragmentTransaction transaction = getFragmentManager().beginTransaction();
                    transaction.replace(R.id.content_frame, newFragment);
                    tbar.setNavigationIcon(null);
                    transaction.commit();
                }
            });
            totalsale = subtotal-totalax;
            sppaymethods = (Spinner)getActivity().findViewById(R.id.sppayoptions);

            final String[] methods = getResources().getStringArray(R.array.strpaymethods_array);
            final String[] methods2 = getResources().getStringArray(R.array.strpaymethods_array_nopost);


            if(Settings.getPayLaterEnabled(getContext()))
                adapter = new ArrayAdapter<>(getContext(), android.R.layout
                        .simple_dropdown_item_1line, methods);
            else
                adapter = new ArrayAdapter<>(getContext(), android.R.layout
                        .simple_dropdown_item_1line, methods2);

            sppaymethods.setAdapter(adapter);
            sppaymethods.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                @Override
                public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                    if (position == adapter.getCount() - 1 && adapter.getCount() == methods.length) {
                        etpartial.setFocusable(true);
                        etpartial.setFocusableInTouchMode(true);
                        etpartial.setText("0");
                        __paymode[0] = (String) adapter.getItem(position);
                        __paystatus[0] = __paymode[0];
                        payinfull = 1;
                        System.out.println(__paymode[0]);
                    } else {
                        __paymode[0] = (String) adapter.getItem(position);
                        __paystatus[0] = __paymode[0];
                        System.out.println(__paymode[0]);
                    }
                }

                @Override
                public void onNothingSelected(AdapterView<?> parent) {

                }
            });

            txtfinal.setText(getContext().getString(R.string.stramount) + " " + mfinalcost[0] + " " + curr);
            submit = (ImageView)getActivity().findViewById(R.id.btnSubmit);

            etpartial = (EditText)getActivity().findViewById(R.id.etpartialpay);
            etpartial.setText("" + subtotal);
            comm = (EditText)getActivity().findViewById(R.id.etcomments);
            getActivity().getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);

            ImageView gobak = (ImageView)getActivity().findViewById(R.id.goback);

            __printable[0] = "1";

            setHasOptionsMenu(true);
        }

        @Override
        public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
            inflater = getActivity().getMenuInflater();
            inflater.inflate(R.menu.add_sales_buttons, menu);
            MenuItem additem, saveitem;
            additem = menu.findItem(R.id.action_add_item);
            saveitem = menu.findItem(R.id.action_save);
            MenuItem syn = menu.findItem(R.id.action_syncnow);
            MenuItem pair = menu.findItem(R.id.action_pair);
            MenuItem sett = menu.findItem(R.id.action_settings);
            syn.setVisible(false);
            pair.setVisible(false);
            sett.setVisible(false);

            additem.setVisible(false);
            saveitem.setVisible(true);
            super.onCreateOptionsMenu(menu, inflater);
        };

        @Override
        public boolean onOptionsItemSelected(MenuItem item) {
            switch (item.getItemId()) {
                case R.id.action_save:
                    String str = etpartial.getText().toString().trim();
                    if (payinfull == 1)
                    {
                        if (!str.isEmpty())
                        {
                            if(!Constants.checkDouble(getContext(),str,etpartial))
                                return false;
                            Double partialpay = new BigDecimal(str).setScale(2, RoundingMode.HALF_UP).doubleValue();
                            Double fullpay =  new BigDecimal(mfinalcost[0]).setScale(2, RoundingMode.HALF_UP).doubleValue();
                            if (partialpay <= fullpay)
                            {
                                __paidamount[0] = String.valueOf(partialpay);
//                            save_order(__paidamount[0],__notes[0],false);
                                String comments = comm.getText().toString().trim();
                                if(!Validating.areSet(comments))
                                    comments = "no comments";
                                String[] vals = new String[]{__paidamount[0],comments, __printable[0],
                                        __paymode[0],txtfinal.getText().toString(),__paystatus[0]};
                                nextstep(vals);
                                return false;
                            } else {
                                showToast(getString(R.string.strwrongamount) + " " +
                                        mfinalcost[0]);
                                return false;
                            }
                        } else {
                            etpartial.requestFocus();
                            showToast(getString(R.string.strfilltheamount));
                            return false;
                        }
                    } else {
                        String comments = comm.getText().toString().trim();
                        if(!Validating.areSet(comments))
                            comments = "no comments";
                        __paidamount[0] = String.valueOf(mfinalcost[0]);
                        String[] vals = new String[]{__paidamount[0],comments, __printable[0],
                                __paymode[0],txtfinal.getText().toString(),__paystatus[0]};
                        nextstep(vals);
                    }
                    break;
            }
            return false;

        }

        private void nextstep(String[] vals){
            LastScreen newFragment = new LastScreen();
            Bundle args = new Bundle();
            args.putStringArray("VALS",vals);
            newFragment.setArguments(args);
            FragmentTransaction transaction = getFragmentManager().beginTransaction();
            transaction.replace(R.id.content_frame, newFragment, LastScreen.class.getName());
            transaction.commit();
        }

    }
    public class LastScreen extends Fragment{
        String[] vals = null;
        TextView txttotal,txtpaid,txtpaystatus,saleref,customer;
        Button btnsaveprint,btnSave;
        ImageView gobak;
        ListView listView;
        Toolbar tbar;
        String batch;

        public LastScreen(){

        }
        @Override
        public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
        {
            View view = inflater.inflate(R.layout.sale_summary, container, false);
            return view ;
        }

        @Override
        public void onActivityCreated(Bundle savedInstanceState)
        {
            super.onActivityCreated(savedInstanceState);
            appPreference = new AppPreference(getContext());
            vals = getArguments().getStringArray("VALS");

            txttotal = (TextView)getActivity().findViewById(R.id.txttotal);
            txtpaid = (TextView)getActivity().findViewById(R.id.txtpaid);
            txtpaystatus = (TextView)getActivity().findViewById(R.id.txtpaymentstatus);
            saleref = (TextView)getActivity().findViewById(R.id.txtsaleref);
            customer = (TextView)getActivity().findViewById(R.id.txtcustomer);
            btnsaveprint = (Button) getActivity().findViewById(R.id.btnSubmitPrint);
            tbar = ((MainScreen)this.getActivity()).getmToolbar();
            tbar.setNavigationIcon(R.drawable.ic_arrow_back_white_36dp);
            tbar.setNavigationOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    ProcessSale newFragment = new ProcessSale();
                    FragmentTransaction transaction = getFragmentManager().beginTransaction();
                    transaction.replace(R.id.content_frame, newFragment, ProcessSale.class.getName());
                    tbar.setNavigationIcon(null);
                    transaction.commit();
                }
            });

            txttotal.append(" " + vals[4]);
            txtpaid.append(" " + vals[0]);
            txtpaystatus.append(" " + vals[5]);
            batch = "SALE"+Constants.generateBatch()+"-"+Constants.getDateOnly2();
            saleref.append(" " + batch);
            customer.append(" "+Customers.getCustomerBusinessNameLoca(getContext(), customeridLocal));

            listView = (ListView)getActivity().findViewById(R.id.summary_list);
            SalesSummaryAdapter adapter = new SalesSummaryAdapter(getContext(),salesItems);
            listView.setAdapter(adapter);

            setHasOptionsMenu(true);
        }

        @Override
        public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
            inflater = getActivity().getMenuInflater();
            inflater.inflate(R.menu.salefrag, menu);

            MenuItem syn = menu.findItem(R.id.action_syncnow);
            MenuItem pair = menu.findItem(R.id.action_pair);
            MenuItem sett = menu.findItem(R.id.action_settings);
            MenuItem print = menu.findItem(R.id.action_printsale);
            if(Settings.getPrintEnabled(getContext()))
                print.setVisible(true);
            syn.setVisible(false);
            pair.setVisible(false);
            sett.setVisible(false);

            super.onCreateOptionsMenu(menu, inflater);
        };

        @Override
        public boolean onOptionsItemSelected(MenuItem item) {
            tbar.setNavigationIcon(null);
            switch (item.getItemId()) {
                case R.id.action_savesale:
                    if (Validating.areSet(vals)) {
                        if(Settings.getSignEnabled(getContext()))
                            nextstep(new String[]{vals[0], vals[1], "0", batch,vals[5]});
                        else
                            save_order(new String[]{vals[0], vals[1], "0", batch,vals[5]}, null);
                    }
                    break;
                case R.id.action_printsale:
                    System.out.println(" click submit and print button ");
                    if (Validating.areSet(vals)) {
                        if (Settings.getSignEnabled(getContext()))
                            nextstep(new String[]{vals[0], vals[1], "1", batch,vals[5]});
                        else
                            save_order(new String[]{vals[0], vals[1], "1", batch,vals[5]}, null);
                    }
                    break;
            }
            return false;

        }
        private void nextstep(String[] vals){
            SignHere newFragment = new SignHere();
            Bundle args = new Bundle();
            args.putStringArray("SIGNATURE",vals);
            newFragment.setArguments(args);
            FragmentTransaction transaction = getFragmentManager().beginTransaction();
            transaction.replace(R.id.content_frame, newFragment, SignHere.class.getName());
            transaction.commit();
        }
    }
    public class SignHere extends Fragment{
        String[] vals = null;
        Toolbar tbar;

        LinearLayout mContent;
        SignHere.signature mSignature;
        Button mClear, mGetSign, mCancel;
        CheckBox chkconsent;
        public String tempDir;
        public int count = 1;
        public String current = null;
        private Bitmap mBitmap;
        View mView;
        File mypath;
        private String uniqueId;

    /*
    * end of new strings
    */

        public SignHere(){

        }
        @Override
        public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
        {
            View view = inflater.inflate(R.layout.signature, container, false);
            return view ;
        }

        @Override
        public void onActivityCreated(Bundle savedInstanceState)
        {
            super.onActivityCreated(savedInstanceState);
            appPreference = new AppPreference(getContext());
            vals = getArguments().getStringArray("SIGNATURE");
            setHasOptionsMenu(false);
            tbar = ((MainScreen)this.getActivity()).getmToolbar();
            tbar.setNavigationIcon(null);

            tempDir = Environment.getExternalStorageDirectory()+"/"+Constants.MASTER_DIR+"/.temp";
            ContextWrapper cw = new ContextWrapper(getContext().getApplicationContext());
            File directory = cw.getDir("tayananjoka", Context.MODE_PRIVATE);

            prepareDirectory();
            uniqueId = System.currentTimeMillis() + "_" + Math.random();
            current = uniqueId + ".png";
            mypath = new File(directory, current);

            mContent = (LinearLayout)getActivity(). findViewById(R.id.linearLayout);
            mSignature = new signature(getContext(), null);
            mSignature.setBackgroundColor(Color.WHITE);
            mContent.addView(mSignature, ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT);
            mClear = (Button) getActivity().findViewById(R.id.clear);
            mGetSign = (Button) getActivity().findViewById(R.id.getsign);
            chkconsent = (CheckBox)getActivity().findViewById(R.id.chkconsent);
            chkconsent.setVisibility(View.GONE);
            mGetSign.setEnabled(false);
            mCancel = (Button) getActivity().findViewById(R.id.cancel);
            mView = mContent;

            mClear.setOnClickListener(new View.OnClickListener() {
                public void onClick(View v) {
                    Log.v("log_tag", "Panel Cleared");
                    mSignature.clear();
                    mGetSign.setEnabled(false);
                }
            });

            mGetSign.setOnClickListener(new View.OnClickListener() {
                public void onClick(View v) {
                    mView.setDrawingCacheEnabled(true);
                    String bitmap = mSignature.save(mView);

                    if (bitmap != null) {
                        Log.v("log_tag", "uri " + bitmap);
                        String[] nm = bitmap.split("/");
                        String[] med = {bitmap,nm[nm.length-1], MultimediaContents.OPERATIONTYPESL};

                        if(Validating.areSet(med)) {
                            save_order(vals, med);
                        }
                    }
                }
            });

            mCancel.setOnClickListener(new View.OnClickListener() {
                public void onClick(View v) {
                    Log.v("log_tag", "Panel Canceled");
                    prevstep(vals);

                }
            });

        }

        @Override
        public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
            inflater = getActivity().getMenuInflater();

            MenuItem syn = menu.findItem(R.id.action_syncnow);
            MenuItem pair = menu.findItem(R.id.action_pair);
            MenuItem sett = menu.findItem(R.id.action_settings);

            syn.setVisible(false);
            pair.setVisible(false);
            sett.setVisible(false);

            super.onCreateOptionsMenu(menu, inflater);
        };

        @Override
        public boolean onOptionsItemSelected(MenuItem item) {
            return false;

        }
        private void prevstep(String[] vals){
            LastScreen newFragment = new LastScreen();
            Bundle args = new Bundle();
            args.putStringArray("VALS", vals);
            newFragment.setArguments(args);
            FragmentTransaction transaction = getFragmentManager().beginTransaction();
            transaction.replace(R.id.content_frame, newFragment, LastScreen.class.getName());
            transaction.commit();
        }


        private boolean prepareDirectory()
        {
            try
            {
                if (makedirs())
                {
                    return true;
                } else {
                    return false;
                }
            } catch (Exception e)
            {
                e.printStackTrace();
                showToast(getString(R.string.strnofilesystem));
                return false;
            }
        }

        private boolean makedirs()
        {
            File tempdir = new File(tempDir);
            if (!tempdir.exists())
                tempdir.mkdirs();

            if (tempdir.isDirectory())
            {
                File[] files = tempdir.listFiles();
                for (File file : files)
                {
                    if (!file.delete())
                    {
                        System.out.println("Failed to delete " + file);
                    }
                }
            }
            return (tempdir.isDirectory());
        }
        public class signature extends View
        {
            private static final float STROKE_WIDTH = 5f;
            private static final float HALF_STROKE_WIDTH = STROKE_WIDTH / 2;
            private Paint paint = new Paint();
            private Path path = new Path();

            private float lastTouchX;
            private float lastTouchY;
            private final RectF dirtyRect = new RectF();

            public signature(Context context, AttributeSet attrs)
            {
                super(context, attrs);
                paint.setAntiAlias(true);
                paint.setColor(Color.BLACK);
                paint.setStyle(Paint.Style.STROKE);
                paint.setStrokeJoin(Paint.Join.ROUND);
                paint.setStrokeWidth(STROKE_WIDTH);
            }

            public String save(View v)
            {
                Uri uri = null;
                Log.v("log_tag", "Width: " + v.getWidth());
                Log.v("log_tag", "Height: " + v.getHeight());
                String bitmap = null;
                if(mBitmap == null)
                {
                    mBitmap =  Bitmap.createBitmap(mContent.getWidth(), mContent.getHeight(), Bitmap.Config.RGB_565);;
                }
                Canvas canvas = new Canvas(mBitmap);
                try
                {
                    FileOutputStream mFileOutStream = new FileOutputStream(mypath);

                    v.draw(canvas);
                    mBitmap.compress(Bitmap.CompressFormat.PNG, 90, mFileOutStream);
                    bitmap = saveImage(mBitmap);
                }
                catch(Exception e)
                {
                    Log.v("log_tag", e.toString());
                }
                return bitmap;
            }

            public void clear()
            {
                path.reset();
                invalidate();
            }

            private String saveImage(Bitmap bitmap) {

                String stored = null;

                File sdcard = Environment.getExternalStorageDirectory() ;

                File folder = new File(sdcard.getAbsoluteFile(), Constants.MASTER_DIR);
                folder.mkdir();
                String filename = String.valueOf(System.currentTimeMillis());
                File file = new File(folder.getAbsoluteFile(), filename + ".jpg") ;
                if (file.exists())
                    return stored ;

                try {
                    FileOutputStream out = new FileOutputStream(file);
                    bitmap.compress(Bitmap.CompressFormat.JPEG, 90, out);
                    out.flush();
                    out.close();
                    stored = file.getAbsolutePath();
                    mGetSign.setEnabled(true);
                } catch (Exception e) {
                    e.printStackTrace();
                }
                return stored;
            }

            @Override
            protected void onDraw(Canvas canvas)
            {
                canvas.drawPath(path, paint);
            }

            @Override
            public boolean onTouchEvent(MotionEvent event)
            {
                float eventX = event.getX();
                float eventY = event.getY();
                mGetSign.setEnabled(true);

                switch (event.getAction())
                {
                    case MotionEvent.ACTION_DOWN:
                        path.moveTo(eventX, eventY);
                        lastTouchX = eventX;
                        lastTouchY = eventY;
                        return true;

                    case MotionEvent.ACTION_MOVE:

                    case MotionEvent.ACTION_UP:

                        resetDirtyRect(eventX, eventY);
                        int historySize = event.getHistorySize();
                        for (int i = 0; i < historySize; i++)
                        {
                            float historicalX = event.getHistoricalX(i);
                            float historicalY = event.getHistoricalY(i);
                            expandDirtyRect(historicalX, historicalY);
                            path.lineTo(historicalX, historicalY);
                        }
                        path.lineTo(eventX, eventY);
                        break;

                    default:
                        debug("Ignored touch event: " + event.toString());
                        return false;
                }

                invalidate((int) (dirtyRect.left - HALF_STROKE_WIDTH),
                        (int) (dirtyRect.top - HALF_STROKE_WIDTH),
                        (int) (dirtyRect.right + HALF_STROKE_WIDTH),
                        (int) (dirtyRect.bottom + HALF_STROKE_WIDTH));

                lastTouchX = eventX;
                lastTouchY = eventY;

                return true;
            }

            private void debug(String string){
            }

            private void expandDirtyRect(float historicalX, float historicalY)
            {
                if (historicalX < dirtyRect.left)
                {
                    dirtyRect.left = historicalX;
                }
                else if (historicalX > dirtyRect.right)
                {
                    dirtyRect.right = historicalX;
                }

                if (historicalY < dirtyRect.top)
                {
                    dirtyRect.top = historicalY;
                }
                else if (historicalY > dirtyRect.bottom)
                {
                    dirtyRect.bottom = historicalY;
                }
            }

            private void resetDirtyRect(float eventX, float eventY)
            {
                dirtyRect.left = Math.min(lastTouchX, eventX);
                dirtyRect.right = Math.max(lastTouchX, eventX);
                dirtyRect.top = Math.min(lastTouchY, eventY);
                dirtyRect.bottom = Math.max(lastTouchY, eventY);
            }
        }
    }
    @Override
    public boolean onKey(View v, int keyCode, KeyEvent event) {
        switch (v.getId()) {
            case R.id.autoquantity00:
                if (event.getAction() == KeyEvent.ACTION_DOWN && event.getKeyCode() == KeyEvent.KEYCODE_ENTER) {
                    Log.i("event", "captured");
                    InputMethodManager imm = (InputMethodManager) getContext().getSystemService(Context.INPUT_METHOD_SERVICE);
                    imm.hideSoftInputFromWindow(getActivity().getCurrentFocus().getWindowToken(), 0);
                    process_item();
                    return false;
                }
                break;
        }
        return false;
    }
/*
    @Override
    public boolean onKeyUp(int keyCode, KeyEvent event) {
        if(keyCode == KeyEvent.KEYCODE_BACK)
        {
            Log.i("event", "captured");
            if(salesItems.size()>0 || !__prodcode.isEmpty()){
                AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
                builder.setMessage(getString(R.string.strsalecancelwarn))
                        .setCancelable(false)
                        .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                salesItems.clear();
                                finish();
                            }
                        })
                        .setNegativeButton("No", new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                dialog.cancel();
                            }
                        });
                AlertDialog alert = builder.create();
                alert.show();
                return true;
            } else {
                return true;
            }
        } else
            return super.onKeyUp(keyCode, event);
    }
*/

    @Override
    public void beforeTextChanged(CharSequence s, int start, int count, int after) {

    }

    @Override
    public void onTextChanged(CharSequence s, int start, int before, int count) {
    }

    @Override
    public void afterTextChanged(Editable s) {
        String productname = etproduct.getText().toString();
        if (TextUtils.isEmpty(productname)) {
//            showToast(getString(R.string.strfilldata));
        }
        if(Validating.areSet(productname)) {
            __prodcode = Products.getProductIdUsingSku(getContext(), productname);
            System.out.println(" product code is " + __prodcode);
            double pr = Products.getUnitPrice(getContext(), __prodcode);
            String sku = Products.getSku(getContext(), __prodcode);
            etprice.setText(getString(R.string.strprice)+" "+pr);
            ab.setSubtitle(getString(R.string.strsku2) + " " + sku);
/*
        etproduct.clearFocus();
        etquantity.requestFocus();*/
            InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(getActivity().getCurrentFocus().getWindowToken(), 0);
        }
    }


    @Override
    public void onResume() {
        super.onResume();

    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater = getActivity().getMenuInflater();
        inflater.inflate(R.menu.add_sales_buttons, menu);
        MenuItem additem, saveitem;
        additem = menu.findItem(R.id.action_add_item);
        saveitem = menu.findItem(R.id.action_save);
        MenuItem syn = menu.findItem(R.id.action_syncnow);
        MenuItem pair = menu.findItem(R.id.action_pair);
        MenuItem sett = menu.findItem(R.id.action_settings);
        syn.setVisible(false);
        pair.setVisible(false);
        sett.setVisible(false);

        additem.setVisible(false);
        saveitem.setVisible(true);
        super.onCreateOptionsMenu(menu, inflater);
    };

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_save:
                next_screen_button();
                break;
        }
        return false;

    }
    private void next_screen_button(){
        if(!business){
            String bznm = edclientname.getText().toString().trim();
            if(Validating.areSet(bznm)){
                if(!Customers.isCustomerValid(getContext(),bznm))
                {
                    edclientname.setText("");
                    edclientname.setHint(getString(R.string.strinvalidbusinesswarn));
                    edclientname.setHintTextColor(Color.RED);
                    edclientname.requestFocus();
                } else {
                    customerid = String.valueOf(Customers.getCustomerid(getContext(), bznm));
                    customeridLocal = String.valueOf(Customers.getCustomerlocalId(getContext(), bznm));
                    business = true;
                    edclientname.setText("");
                    laycust.setVisibility(View.GONE);
                    layprod.setVisibility(View.VISIBLE);
                    etproduct.requestFocus();
                    layqty.setVisibility(View.VISIBLE);
                    layrep.setVisibility(View.VISIBLE);

                    if(Settings.getDiscountEnabled(getContext()))
                        rdiscount.setVisibility(View.VISIBLE);
                    lvitems.setVisibility(View.VISIBLE);
                    topb.setVisibility(View.VISIBLE);
                    lowb.setVisibility(View.VISIBLE);
                }
            }
        } else {
            if( !__prodcode.isEmpty()){
                AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
                builder.setMessage(getString(R.string.strpendingitem))
                        .setCancelable(false)
                        .setPositiveButton("OKAY", new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                dialog.dismiss();
                            }
                        });
                AlertDialog alert = builder.create();
                alert.show();
            } else {
                if (salesItems != null && salesItems.size() > 0) {
                    ProcessSale newFragment = new ProcessSale();
                    Bundle args = new Bundle();
                    newFragment.setArguments(args);
                    FragmentTransaction transaction = getFragmentManager().beginTransaction();
                    transaction.replace(R.id.content_frame, newFragment, ProcessSale.class.getName());
                    transaction.commit();
                }
            }
        }
    }

    public void addcustomer(){

        LayoutInflater infl = (LayoutInflater)getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View view = infl.inflate(R.layout.addcustomer, null);
        final Dialog dialog = new Dialog(getContext(), R.style.CustomDialog);
        dialog.setCancelable(false);
        dialog.setContentView(view);
        dialog.show();
        Button sub = (Button) dialog.findViewById(R.id.btnSubmit);
        TextView txttitle = (TextView) dialog.findViewById(R.id.txttitle);
        String title = getResources().getString(R.string.straddcustomer);
        title.toUpperCase(Locale.getDefault());
        txttitle.setText(title);
        final EditText clname,shname,phone,email,address,vrn,tin ;

        ImageView goback = (ImageView) dialog.findViewById(R.id.goback);
        goback.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });

        clname = (EditText) dialog.findViewById(R.id.etclientname);
        shname = (EditText) dialog.findViewById(R.id.etshopname);
        phone = (EditText) dialog.findViewById(R.id.etclientphone);
        vrn = (EditText) dialog.findViewById(R.id.etvrn);
        tin = (EditText) dialog.findViewById(R.id.ettin);
        email = (EditText) dialog.findViewById(R.id.etclientemail);
        address = (EditText) dialog.findViewById(R.id.etclientshopaddress);


        sub.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String strclname, strshname, strclemail, strclphone, strcladdress, svrn, stin;
                strclname = clname.getText().toString();
                strshname = Validating.stripit(shname.getText().toString());
                strclemail = Validating.stripit(email.getText().toString());
                strclphone = Validating.stripit(phone.getText().toString());
                svrn = vrn.getText().toString();
                stin = tin.getText().toString();
                strcladdress = address.getText().toString();

                if (!strclname.isEmpty())
                    if (!Constants.checkInvalidChars(dialog.getContext(), strclname, clname))
                        return;
                if (!strshname.isEmpty())
                    if (!Constants.checkInvalidChars(dialog.getContext(), strshname, shname))
                        return;
                if (!svrn.isEmpty())
                    if (!Constants.checkInvalidChars(dialog.getContext(), svrn, vrn))
                        return;
                if (!stin.isEmpty())
                    if (!Constants.checkInvalidChars(dialog.getContext(), stin, tin))
                        return;
                if (!strclphone.isEmpty())
                    if (!Constants.checkInvalidChars(dialog.getContext(), strclphone, phone))
                        return;
                if (!strclemail.isEmpty())
                    if (!Constants.checkInvalidChars(dialog.getContext(), strclemail, email))
                        return;

                if (!Validating.areSet(strcladdress))
                    strcladdress = "Dar es salaam";
                if (!Validating.areSet(strclname))
                    strclname = shname.getText().toString();
                EditText[] et = new EditText[]{clname, shname, phone, email, address};
                String[] input = new String[]{strclname, strshname, strclphone, strclemail, strcladdress, svrn, stin};
                String[] inputcheck = new String[]{strclname, strshname, strclphone};

                if (Validating.areSet(inputcheck)) {
                    if (strclemail.isEmpty()) {
                        int insert = Customers.insertCustomerLocal(dialog.getContext(), input);
                        if (insert == 1) {
                            customerid = String.valueOf(Customers.getCustomerid(getContext(), strshname));
                            customeridLocal = String.valueOf(Customers.getCustomerlocalId(getContext(), strshname));
                            business = true;
                            dialog.dismiss();
                            edclientname.setText("");
                            laycust.setVisibility(View.GONE);
                            layprod.setVisibility(View.VISIBLE);
                            etproduct.requestFocus();
                            layqty.setVisibility(View.VISIBLE);
                            layrep.setVisibility(View.VISIBLE);

                            if (Settings.getDiscountEnabled(dialog.getContext()))
                                rdiscount.setVisibility(View.VISIBLE);
                            lvitems.setVisibility(View.VISIBLE);
                            topb.setVisibility(View.VISIBLE);
                            lowb.setVisibility(View.VISIBLE);
                            Toast.makeText(dialog.getContext(), getResources().getString(R.string.strinputsuccesscustomer), Toast.LENGTH_SHORT).show();
                        } else {
                            Toast.makeText(dialog.getContext(), getResources().getString(R.string.strinputfailcustomer), Toast.LENGTH_SHORT).show();
                            dialog.dismiss();
                        }
                    } else if (!strclemail.isEmpty() && Validating.isEmailValid(strclemail)) {

                        int insert = Customers.insertCustomerLocal(dialog.getContext(), input);
                        if (insert == 1) {
                            customerid = String.valueOf(Customers.getCustomerid(getContext(), strshname));
                            customeridLocal = String.valueOf(Customers.getCustomerlocalId(getContext(), strshname));
                            business = true;
                            dialog.dismiss();
                            edclientname.setText("");
                            laycust.setVisibility(View.GONE);
                            layprod.setVisibility(View.VISIBLE);
                            etproduct.requestFocus();
                            layqty.setVisibility(View.VISIBLE);
                            layrep.setVisibility(View.VISIBLE);

                            if (Settings.getDiscountEnabled(dialog.getContext()))
                                rdiscount.setVisibility(View.VISIBLE);
                            lvitems.setVisibility(View.VISIBLE);
                            topb.setVisibility(View.VISIBLE);
                            lowb.setVisibility(View.VISIBLE);
                            Toast.makeText(dialog.getContext(), getResources().getString(R.string.strinputsuccesscustomer), Toast.LENGTH_SHORT).show();
                        } else {
                            Toast.makeText(dialog.getContext(), getResources().getString(R.string.strinputfailcustomer), Toast.LENGTH_SHORT).show();
                            dialog.dismiss();
                        }
                    } else {
                        et[3].setHint(getResources().getString(R.string.strinputerror) +
                                " " + getResources().getString(R.string.strclientemail));
                        et[3].setHintTextColor(Color.RED);
                        et[3].setCompoundDrawablesWithIntrinsicBounds(getResources().getDrawable(R.drawable.ic_timer_auto_black_24dp), null, getResources().getDrawable(R.drawable.ic_error_red_24dp), null);
                    }

                } else {
                    if (!Validating.areSet(strclname)) {
                        et[0].setHint(getResources().getString(R.string.strinputerror) +
                                " " + getResources().getString(R.string.strclientname));
                        et[0].setHintTextColor(Color.RED);
                        et[0].setCompoundDrawablesWithIntrinsicBounds(getResources().getDrawable(R.drawable.ic_timer_auto_black_24dp), null, getResources().getDrawable(R.drawable.ic_error_red_24dp), null);
                    }
                    if (!Validating.areSet(strshname)) {
                        et[1].setHint(getResources().getString(R.string.strinputerror) +
                                " " + getResources().getString(R.string.strclientshopname));
                        et[1].setHintTextColor(Color.RED);

                        et[1].setCompoundDrawablesWithIntrinsicBounds(getResources().getDrawable(R.drawable.ic_account_box_black_24dp), null, getResources().getDrawable(R.drawable.ic_error_red_24dp), null);
                    }
                    if (!Validating.areSet(strclphone)) {
                        et[2].setHint(getResources().getString(R.string.strinputerror) +
                                " " + getResources().getString(R.string.strclientphone));
                        et[2].setHintTextColor(Color.RED);
                        et[2].setCompoundDrawablesWithIntrinsicBounds(getResources().getDrawable(R.drawable.ic_local_phone_black_24dp), null, getResources().getDrawable(R.drawable.ic_error_red_24dp), null);
                    }
                    if (!Validating.areSet(strcladdress)) {
                        et[4].setHint(getResources().getString(R.string.strinputerror) +
                                " " + getResources().getString(R.string.strclientaddress));
                        et[4].setHintTextColor(Color.RED);
                        et[4].setCompoundDrawablesWithIntrinsicBounds(getResources().getDrawable(R.drawable.ic_pin_drop_black_24dp), null, getResources().getDrawable(R.drawable.ic_error_red_24dp), null);
                    }
                    return;
                }
            }
        });
    }
}
