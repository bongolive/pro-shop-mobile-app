/*
 * The source codes in this project are owned by Bongolive Enterprises. Copying or any kind of usage without Bongolive's approval is against the law.
 * Copyright (c) 2015.
 *
 */


package bongolive.apps.proshop.view;


import android.content.ContentResolver;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AutoCompleteTextView;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;

import java.util.ArrayList;

import bongolive.apps.proshop.R;
import bongolive.apps.proshop.controller.Constants;
import bongolive.apps.proshop.controller.TransferAdapter;
import bongolive.apps.proshop.model.ContentProviderApi;
import bongolive.apps.proshop.model.Customers;
import bongolive.apps.proshop.model.StockIssue;
import bongolive.apps.proshop.model.Transfer;

public class Procurement extends AppCompatActivity{

    private boolean tabletSize = false;
    private RecyclerView mRecyclerView;
    private RecyclerView.Adapter mAdapter;
    private RecyclerView.LayoutManager mLayoutManager;
    private ActionBar actionBar;
    private EditText etfilter;
    private ArrayList arrayList;
    private RelativeLayout rl;
    private ImageView imgdone,imgnext;
    private AutoCompleteTextView bzn;
    private ActionBar ab;
    ArrayList<Transfer> mItems = new ArrayList<>();
    ArrayList<StockIssue> mItems_action = new ArrayList<>();
    String action;
    int actionno ;
    FloatingActionButton actionButton;
    /*
    * end of new strings
    */

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        tabletSize = getResources().getBoolean(R.bool.isTablet);
        if (!tabletSize) {
            setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        }
        setContentView(R.layout.selling_screen);

        ab = getSupportActionBar();
        ab.setDisplayUseLogoEnabled(true);
        ab.setDisplayHomeAsUpEnabled(true);

        mRecyclerView = (RecyclerView) findViewById(R.id.prodct_list);
        mRecyclerView.setHasFixedSize(true);
        mRecyclerView.setItemAnimator(new DefaultItemAnimator());

        actionButton = (FloatingActionButton)findViewById(R.id.actionbutton);
        actionButton.setBackgroundColor(getResources().getColor(R.color.actionbar));
        actionButton.setImageResource(R.drawable.ic_action_add);


        Intent intent = getIntent();
        action = intent.getAction();
        switch (action){
            case Constants.TRANSTYPE_REQUEST:
                mItems = Transfer.getItems(this, action);
                actionButton.setVisibility(View.VISIBLE);
                ab.setTitle(getString(R.string.stockrequest));
                actionno = 1;
                mAdapter = new TransferAdapter(this, mItems);
                mRecyclerView.setAdapter(mAdapter);
                break;
            case Constants.TRANSTYPE_ISSUE:
                mItems_action = StockIssue.getItems(this);
                actionButton.setVisibility(View.VISIBLE);
                ab.setTitle(getString(R.string.stockissue));
                actionno = 2;
                mAdapter = new TransferAdapter(this, mItems_action, action);
                mRecyclerView.setAdapter(mAdapter);
                break;
            case Constants.TRANSTYPE_TRANSFER:
                mItems = Transfer.getItems(this,action);
                ab.setTitle(getString(R.string.stocktransfer));
                actionno = 3;
                mAdapter = new TransferAdapter(this, mItems);
                mRecyclerView.setAdapter(mAdapter);
                break;
        }

        mLayoutManager = new LinearLayoutManager(this);
        mRecyclerView.setLayoutManager(mLayoutManager);
        if(mItems.size() < 1 || mItems_action.size() < 1 ){
            Snackbar snackbar = null;
            if(actionno == 3)
                snackbar = Snackbar
                        .make(mRecyclerView, getString(R.string.strnodata), Snackbar.LENGTH_INDEFINITE);
            else
                snackbar = Snackbar
                        .make(mRecyclerView, getString(R.string.strnodata), Snackbar.LENGTH_SHORT);

            snackbar.show();
        }

        actionButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (actionno == 1) {
                    Intent intent = new Intent(Procurement.this, TransferRequest.class);
                    startActivity(intent);
                } else if (actionno == 2) {
                    Intent intent = new Intent(Procurement.this, StockIssueActivity.class);
                    startActivity(intent);
                }
            }
        });


        mRecyclerView.addOnItemTouchListener(
                new RecylerItemClick(this, new RecylerItemClick.OnItemClickListener() {
                    @Override
                    public void onItemClick(View view, int position) {
                        if(actionno != 2) {
                            Transfer py = mItems.get(position);
                            if (mItems.size() > 0) {
                                String wh = py.getWarehousefrom();
                                if (wh.equals("0"))
                                    wh = getString(R.string.strdefaultwh);
                                Intent intent = new Intent(Procurement.this, TransferDetail.class);
                                intent.setAction(action);
                                intent.putExtra(Transfer.ID, py.getId());
                                intent.putExtra(Transfer.ACK, py.getAck());
                                intent.putExtra(Transfer.WAREHOUSEFROM, wh);
                                startActivity(intent);
                            }
                        } else {
                            StockIssue st = mItems_action.get(position);
                            Intent intent = new Intent(Procurement.this, TransferDetail.class);
                            intent.setAction(action);
                            String cu = st.getCustlocal();
                            cu = Customers.getCustomerBusinessNameLoca(Procurement.this, cu);
                            intent.putExtra(Transfer.ID, st.getId());
                            intent.putExtra(Transfer.ACK, st.getAck());
                            intent.putExtra(Transfer.WAREHOUSEFROM, cu);
                            startActivity(intent);
                        }
                    }
                })
        );

    }

    @Override
    protected void onResume() {
        super.onResume();
        if(mAdapter != null)
            mAdapter.notifyDataSetChanged();
    }

    /*
     * running the sync adapter on demand
     */
    public void ondemandsync()
    {
        Bundle settingsBundle = new Bundle() ;
        settingsBundle.putBoolean(ContentResolver.SYNC_EXTRAS_MANUAL, true);
        settingsBundle.putBoolean(ContentResolver.SYNC_EXTRAS_EXPEDITED, true);
        ContentResolver.requestSync(Constants.createSyncAccount(this), ContentProviderApi.AUTHORITY, settingsBundle);
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.sync_menu, menu);
        return super.onCreateOptionsMenu(menu);
    }



    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                return true;
            case R.id.action_syncnow:
                ondemandsync();
                break;
        }
        return super.onOptionsItemSelected(item);
    }

}


