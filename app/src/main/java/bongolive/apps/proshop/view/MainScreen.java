package bongolive.apps.proshop.view;

import android.Manifest;
import android.accounts.Account;
import android.app.AlarmManager;
import android.app.Dialog;
import android.app.PendingIntent;
import android.bluetooth.BluetoothAdapter;
import android.content.ContentResolver;
import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.ActivityInfo;
import android.content.pm.PackageManager;
import android.content.res.Configuration;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.design.widget.NavigationView;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import java.util.Locale;
import java.util.concurrent.TimeUnit;

import bongolive.apps.proshop.R;
import bongolive.apps.proshop.controller.AppPreference;
import bongolive.apps.proshop.controller.Constants;
import bongolive.apps.proshop.controller.LocationTrigger;
import bongolive.apps.proshop.controller.LocationUpdate;
import bongolive.apps.proshop.controller.MCrypt;
import bongolive.apps.proshop.controller.PermissionUtils;
import bongolive.apps.proshop.controller.Validating;
import bongolive.apps.proshop.model.ContentProviderApi;
import bongolive.apps.proshop.model.Customers;
import bongolive.apps.proshop.model.DatabaseHandler;
import bongolive.apps.proshop.model.Product_Categories;
import bongolive.apps.proshop.model.SalesItems;
import bongolive.apps.proshop.model.Settings;
import bongolive.apps.proshop.model.Warehouse;

public class MainScreen extends AppCompatActivity implements NavigationView.OnNavigationItemSelectedListener
        ,ActivityCompat.OnRequestPermissionsResultCallback
        {

    private static final String TAG = MainScreen.class.getName();
    private static final int PHONESTATE_PERMISSION_REQUEST = 1;
    private Toolbar mToolbar;
    private DrawerLayout mDrawerLayout;
    NavigationView mNavigationView;
    FrameLayout mContentFrame;

    public static final int REGISTER_SUBSCRIBER = 5;
    MenuItem mHome,mSales,mSaleslite,mPayment,mAudit,mCustomers,mStock,mReports,mUsers,mAbout,mForm,
            mExpense,mDelivery,mOrder,mStockManageer;
    MenuItem rSalesH,rDelivH,rOrderh,rReorder,rStation,rExpense,rProfit,rSalesByP,rSalesByC,rTax;
    MenuItem iIssue,iTransfer,iRequest;
    String extra = null;
    /* Dashboard variables */
    MenuItem registeritem ;

    Button start;
    ProgressBar progressBar;
    TextView txtsyncsta;
    private boolean firstrun;
    LocationUpdate mLocation;
    BluetoothAdapter mBtAdapter;
    int accounttype;
            boolean permission = false;
    AlertDialogManager alert;
//    Uri customer ;

    private static final int REQUEST_ENABLE_BT = 2;
    public static final String ARG_SECTION_NUMBER = "section_number";
    private static final int REQUEST_PRINT = 1;
    private static final int REQUEST_GPS = 3;
    private static  int CONNECTIONSTATUS = 0;

    /* End of Dashboard variables*/

    boolean tabletSize = false;
    Account mAccount;
    private static final String PREFERENCES_FILE = "menu_settings";
    private static final String PREF_USER_LEARNED_DRAWER = "navigation_drawer_learned";
    private static final String STATE_SELECTED_POSITION = "selected_navigation_drawer_position";

    private boolean mUserLearnedDrawer;
    private boolean mFromSavedInstanceState;
    private int mCurrentSelectedPosition;
    boolean expanded = false, expanded1 = false;
    private AppPreference appPreference;
    TextView txtv,txtn,txtc;
    boolean showusers = false, showdelive = false, showorders = false, lite = false, audit = false, station = false;
    boolean xpense = false, payment = false, stock = false, sales = false, report = false, product = false, customer = false;
    String apptype = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        appPreference = new AppPreference(getApplicationContext());
        String languageToLoad = appPreference.getDefaultLanguage();

        alert = new AlertDialogManager();
        Locale locale = new Locale(languageToLoad);
        Locale.setDefault(locale);
        Configuration config = new Configuration();
        config.locale = locale;

        getBaseContext().getResources().updateConfiguration(config,
                getBaseContext().getResources().getDisplayMetrics());

        tabletSize = getResources().getBoolean(R.bool.isTablet);
        if(!tabletSize){
            setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        }

        setContentView(R.layout.nav_drawer);

        setUpToolbar();
        apptype = Settings.getApptypeEnabled(this);
        appPreference = new AppPreference(this);
        mDrawerLayout = (DrawerLayout) findViewById(R.id.nav_drawer);

        mUserLearnedDrawer = Boolean.valueOf(readSharedSetting(this, PREF_USER_LEARNED_DRAWER, "false"));

        if (savedInstanceState != null) {
            mCurrentSelectedPosition = savedInstanceState.getInt(STATE_SELECTED_POSITION);
            mFromSavedInstanceState = true;
        }


        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.READ_PHONE_STATE) !=
                PackageManager.PERMISSION_GRANTED) {
            permission = true;
            // Permission is given
            Log.e("PERMISSION","Permission is granted");
        } else {
            permission = false;
            Log.e("PERMISSION","Permission is granted");
            PermissionUtils.requestPermission(this, PHONESTATE_PERMISSION_REQUEST,
                    Manifest.permission.READ_PHONE_STATE, true);
        }

        extra = Constants.SEARCHSOURCE_MAIN;

        showusers = Settings.getUserEnabled(this);
        showdelive = Settings.getDeliveryEnabled(this);
        showorders = Settings.getOrderEnabled(this);
        lite = Settings.getProshopLite(this);
        station = Settings.getFormEnabled(this);
        audit = Settings.getAuditEnabled(this);
        xpense = Settings.getExEnabled(this);
        payment = Settings.getPaymentEnabled(this);
        stock = Settings.getStockEnabled(this);
        sales = Settings.getSalesEnabled(this);
        report = Settings.getReportEnabled(this);
        product = Settings.getProductsEnabled(this);
        customer = Settings.getCustomersEnabled(this);

        mLocation = new LocationUpdate(this);

        mAccount = Constants.createSyncAccount(this);
        mBtAdapter = BluetoothAdapter.getDefaultAdapter();

        setUpNavDrawer();


        mNavigationView = (NavigationView) findViewById(R.id.nav_view);

        setNavMenu();

        mContentFrame = (FrameLayout) findViewById(R.id.content_frame);

        FrameLayout frameLayout = (FrameLayout) mNavigationView.getHeaderView(0);

        String[] info = appPreference.getAccountInfo();
        String cmp  = !info[0].isEmpty() ? info[0] : "Demo Company Name";;

        txtv = (TextView)frameLayout.getChildAt(3);
        txtn = (TextView)frameLayout.getChildAt(2);
        txtc = (TextView)frameLayout.getChildAt(1);
        String version = Constants.getVersionName(this);
        String apptype = Constants.ucWord(Settings.getApptypeEnabled(this));
        int dbversion = DatabaseHandler.getInstance(this).getReadableDatabase().getVersion();

        txtn.setText(apptype);
        txtv.setText(getString(R.string.strversion) + " : " + version+" \n "
                +getString(R.string.strdbversion) + " : " + dbversion);
        txtc.setText(cmp);
        mNavigationView.setNavigationItemSelectedListener(this);

//        if(permission) {
            Fragment fragment = new Home();

            FragmentManager fragmentManager = getSupportFragmentManager();
            Bundle bundle = new Bundle();
            bundle.putString(Constants.SEARCHSOURCE, extra);
            fragment.setArguments(bundle);
            fragmentManager.beginTransaction()
//                .addToBackStack(String.valueOf(mNavigationView.getMenu().getItem(0).getItemId()))
                    .addToBackStack(null)
                    .replace(R.id.content_frame, fragment)
                    .commit();
//        }
        accounttype = appPreference.get_accounttype();
        if(accounttype == 1){

/*
            int tracktime = Settings.getTrackTime(this);
            if(tracktime > 0) {
                scheduleAlarm();
            }
*/

            String time = appPreference.getDefaultSync();
            if(!Validating.areSet(time))
            {
                time = getResources().getStringArray(R.array.pref_sync_frequency_values)[0];
            }
            int t = Integer.parseInt(time);

            periodicadapersync(t);
        }
        if(!appPreference.getBluetoothoption()) {
            String address = appPreference.getAddress();
            if (mBtAdapter != null) {
                if (!mBtAdapter.isEnabled()) {
                    getAddress();
                } else {
                    if (address.isEmpty())
                        getAddress();
                }
            } else {
                getAddress();
                Log.v("bluetoothaddress", " address is " + address);
            }
        }
        if(Customers.getCustomerCount(this) == 0){
            Customers.createDefault(this);
        }

        String[] vals = {"1","DEFAULT_WH","SAMPLE WAREHOUSE","NO ADDRESS",Constants.WH_DEMO};
        if(Warehouse.getCount(this) == 0)
            Warehouse.storeWh(this, vals);
        Constants.createsuper(this);
        if(!Product_Categories.isCatPresent(this, "GENERAL", Constants.LOCAL))
            Product_Categories.createDefault(this);

    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions,
                                           @NonNull int[] grantResults) {
        if (requestCode == PHONESTATE_PERMISSION_REQUEST) {
            if (PermissionUtils.isPermissionGranted(permissions, grantResults,
                    Manifest.permission.READ_PHONE_STATE)) {
                permission = true;
//                showToast("permision granted");
            } else {
                permission = false;
                showMissingPermissionError();
//                showToast("permission not granted");
            }
        }
    }

    private void showMissingPermissionError() {
        showToast(getString(R.string.permission_error));
        PermissionUtils.PermissionDeniedDialog
                .newInstance(true).show(getSupportFragmentManager(), "dialog");
    }
    private void check_firstrun() {
        firstrun = appPreference.check_firstrun();
        if (firstrun) {
            validate(Constants.TAGREGISTRATION);
            appPreference.set_firstrun();
            appPreference.save_last_sync(Constants.getDateOnly2());
        } else {
            String auth = appPreference.getAuthkey();
            String authtyping = null;
            if(!auth.isEmpty()){

                String im = Constants.getIMEINO(this);
                im = im.concat("|");
                int acctype = appPreference.get_accounttype();
                if(acctype == 0)
                    authtyping = Constants.ACCOUNT_TYPE_NORMAL;
                if(acctype == 1)
                    authtyping = Constants.ACCOUNT_TYPE_PREMIUM;

                MCrypt mCrypt = new MCrypt();
                try {
                    String crypted = new String(mCrypt.decrypt(auth));
                    if(im.concat(authtyping).equals(crypted)){
                        if(acctype == 1){
                            String time = appPreference.getDefaultSync();
                            if(!Validating.areSet(time))
                            {
                                time = getResources().getStringArray(R.array.pref_sync_frequency_values)[0];
                            }
                            int t = Integer.parseInt(time);
                            if ( Settings.getSettingsCount(this) < 1) {
                                Toast.makeText(this, getString(R.string.strsyncsstartstatus), Toast
                                        .LENGTH_SHORT).show();
                                ondemandsync();
                            } else {
                                periodicadapersync(t);
                            }
                        } else {
                            Toast.makeText(this,
                                    getString(R.string.normalaccountmsg),
                                    Toast.LENGTH_SHORT).show();
                        }
                    } else {
                        validate("validate");
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            } else {
                validate(Constants.TAGREGISTRATION);
            }
        }
    }

    public void getAddress(){
        Intent selectDevice = new Intent(this, UniversalBluetoothInstance.class);
        startActivityForResult(selectDevice, REQUEST_PRINT);
    }


    public void wh(){
        ContentResolver cr = getContentResolver();
        ContentValues cv = new ContentValues();
        cv.put(SalesItems.WH, Warehouse.getDefaultId(this));
        cr.update(SalesItems.BASEURI, cv, null, null);
    }


    @Override
    public boolean onNavigationItemSelected(MenuItem menuItem) {
        Intent intent;
        FragmentManager fragmentManager = getSupportFragmentManager();
        Bundle args = new Bundle() ;
        Fragment fm = null;
        menuItem.setChecked(true);
        int id = menuItem.getItemId();
        switch (id) {
            case R.id.nav_home:
                extra = Constants.SEARCHSOURCE_HOME;
                fm = new Home();
                break;
            case R.id.nav_sales:
                if(!tabletSize) {
                    fm = null;
                    if (lite) {
                        intent = new Intent(this, SalesLiteActivity.class);
                        startActivity(intent);
                    } else {
                        intent = new Intent(this, SalesActivity.class);
                        startActivity(intent);
                    }
                } else {
                    fm = null;
                    if (lite) {
                        intent = new Intent(this, SalesLiteActivity.class);
                        startActivity(intent);
                    } else {
                        fm = new SalesFrag();
                    }
                }
                break;
            case R.id.nav_form:
                extra = Constants.SEARCHSOURCE_STATION;
                fm = new StationFragment();
                break;
            case R.id.nav_del:
                extra = Constants.SEARCHSOURCE_PRODUCTS;
                fm = new DeliveriesFragment();
                break;
            case R.id.nav_customer:
                extra = Constants.SEARCHSOURCE_CUSTOMERS;
                fm = new CustomersFragment();
                break;
            case R.id.nav_stock:
                extra = Constants.SEARCHSOURCE_PRODUCTS;
                fm = new ProductsFragment();
                break;
            case R.id.nav_user:
                fm = new UserManagement();
                break;
            case R.id.nav_about:
                fm = new UserAccount();
                break;
            case R.id.nav_audit:
                fm = null;
                intent = new Intent(this, AuditInit.class);
                startActivity(intent);
                break;
            case R.id.nav_expense:
                fm = new ExpenseFragment();
                break;
            case R.id.nav_paymnt:
                if(!tabletSize) {
                    fm = null;
                    intent = new Intent(this, PaymentScreen.class);
                    startActivity(intent);
                } else
                    fm = new PaymentScreenFrag();
                break;
            case R.id.nav_order:
                fm = null;
                intent = new Intent(this, OrdersActivity.class);
                startActivity(intent);
                break;
            case R.id.nav_report:
                if(!expanded) {
                    mNavigationView.getMenu().setGroupVisible(R.id.grp_rep, true);
                    expanded = true;
                    menuItem.setIcon(R.drawable.circle_minus);
                    updateReportsMenu();
                }
                else {
                    mNavigationView.getMenu().setGroupVisible(R.id.grp_rep, false);
                    expanded = false;
                    menuItem.setIcon(R.drawable.circle_plus);
                }
                break;
            case R.id.nav_inventory:
                    if (!expanded1) {
                        mNavigationView.getMenu().setGroupVisible(R.id.grp_inv, true);
                        expanded1 = true;
                        menuItem.setIcon(R.drawable.circle_minus);
                        updateReportsMenu();
                    } else {
                        mNavigationView.getMenu().setGroupVisible(R.id.grp_inv, false);
                        expanded1 = false;
                        menuItem.setIcon(R.drawable.circle_plus);
                    }
                break;
            case R.id.sb_transfer:
                if(!tabletSize) {
                    intent = new Intent(this, Procurement.class);
                    intent.setAction(Constants.TRANSTYPE_TRANSFER);
                    startActivity(intent);
                    fm = null;
                } else {
                    args.putString(Constants.SOURCE, Constants.TRANSTYPE_TRANSFER);
                    fm = new ProcurementFrag();
                }
                break;
            case R.id.sb_request:
                if(!tabletSize) {
                    intent = new Intent(this, Procurement.class);
                    intent.setAction(Constants.TRANSTYPE_REQUEST);
                    startActivity(intent);
                    fm = null;
                } else {
                    args.putString(Constants.SOURCE, Constants.TRANSTYPE_REQUEST);
                    fm = new ProcurementFrag();
                }
                break;
            case R.id.sb_issue:
                if(!tabletSize) {
                    intent = new Intent(this, Procurement.class);
                    intent.setAction(Constants.TRANSTYPE_ISSUE);
                    startActivity(intent);
                    fm = null;
                } else {
                    args.putString(Constants.SOURCE, Constants.TRANSTYPE_ISSUE);
                    fm = new ProcurementFrag();
                }
                break;
            case R.id.rep_sh:
                fm = new SalesByCustomer();
                args.putString(Constants.REPORTS, Constants.SALEREPORT);
                break;
            case R.id.rep_sbc:
                args.putString(Constants.REPORTS, Constants.SALESBYCUSTOMER);
                fm = new SalesByCustomer();
                break;
            case R.id.rep_sbp:
                args.putString(Constants.REPORTS, Constants.SALESBYPRODUCT);
                fm = new SalesByCustomer();
                break;
            case R.id.rep_dh:
                fm = new SalesByCustomer();
                args.putString(Constants.REPORTS, Constants.DELIVERYHISTORY);
                break;
            case R.id.rep_oh:
                args.putString(Constants.REPORTS, Constants.ORDERREPORT);
                fm = new SalesByCustomer();
                break;
            case R.id.rep_prt:
                args.putString(Constants.REPORTS, Constants.PROFITREPORT);
                fm = new SalesByCustomer();
                break;
            case R.id.rep_tx:
                fm = new SalesByCustomer();
                args.putString(Constants.REPORTS, Constants.TAXBYPRODUCT);
                break;
            case R.id.rep_sr:
                args.putString(Constants.REPORTS, Constants.STATIONHISTORY);
                fm = new SalesByCustomer();
                break;
            case R.id.rep_er:
                args.putString(Constants.REPORTS, Constants.PRODUCTREORDER);
                fm = new SalesByCustomer();
                break;
            default:
                return true;
        }
        setTitle(menuItem.getTitle());
        if(fm != null){
            fm.setArguments(args);
            String itemId = String.valueOf(menuItem.getItemId());
            fragmentManager.beginTransaction().addToBackStack(itemId).
                    replace(R.id.content_frame, fm).
                    commit();
        }

        if (!tabletSize)
            if(id == R.id.nav_report || id == R.id.nav_inventory)
                mDrawerLayout.openDrawer(GravityCompat.START);
            else
                mDrawerLayout.closeDrawer(GravityCompat.START);
        return true;
    }


    private void showToast(String txt) {
        Toast.makeText(this, txt, Toast.LENGTH_LONG).show();
    }


    private void setNavMenu(){
        Menu menu = mNavigationView.getMenu();
        /*main menu */
        mHome = menu.findItem(R.id.nav_home);
        mSales = menu.findItem(R.id.nav_sales);
        mSaleslite = menu.findItem(R.id.nav_sales);
        mCustomers = menu.findItem(R.id.nav_customer);
        mStock = menu.findItem(R.id.nav_stock);
        mUsers = menu.findItem(R.id.nav_user);
        mReports = menu.findItem(R.id.nav_report);
        mAbout = menu.findItem(R.id.nav_about);
        mPayment = menu.findItem(R.id.nav_paymnt);
        mStockManageer = menu.findItem(R.id.nav_inventory);
        mDelivery = menu.findItem(R.id.nav_del);
        mOrder = menu.findItem(R.id.nav_order);
        mForm = menu.findItem(R.id.nav_form);
        mAudit = menu.findItem(R.id.nav_audit);
        mExpense = menu.findItem(R.id.nav_expense);

        /* end of main menu*/

        /*reports*/
        rSalesH = menu.findItem(R.id.rep_sh);
        rSalesByC = menu.findItem(R.id.rep_sbc);
        rSalesByP = menu.findItem(R.id.rep_sbp);
        rDelivH = menu.findItem(R.id.rep_dh);
        rOrderh = menu.findItem(R.id.rep_oh);
        rStation = menu.findItem(R.id.rep_sr);
        rExpense = menu.findItem(R.id.rep_er);
        rExpense.setVisible(false);
        rTax = menu.findItem(R.id.rep_tx);
        rProfit = menu.findItem(R.id.rep_prt);
        rReorder = menu.findItem(R.id.rep_pr);

        iIssue = menu.findItem(R.id.sb_issue);
        iRequest = menu.findItem(R.id.sb_request);
        iTransfer = menu.findItem(R.id.sb_transfer);
        /* end of reports*/
        System.out.println(" stoc "+stock);

        if(showusers)
            mUsers.setVisible(true);
        if(payment)
            mPayment.setVisible(true);
        if(stock) {
            System.out.println(" stock ? "+stock);
            mStockManageer.setVisible(true);
            menu.setGroupVisible(R.id.grp_inv, false);
        } else
            mStockManageer.setVisible(false);
        if(showdelive)
            mDelivery.setVisible(true);
        if(showorders)
            mOrder.setVisible(true);
        if(audit)
            mAudit.setVisible(true);
        if(xpense)
            mExpense.setVisible(true);
        if(!lite)
        if(sales)
            mSales.setVisible(true);
        else
            mSales.setVisible(false);
        if(!lite)
        if(report) {
            mReports.setVisible(true);
        }
        else {
            mReports.setVisible(false);
        }
        if(!lite)
        if(product)
            mStock.setVisible(true);
        else
            mStock.setVisible(false);
        if(!lite)
        if(customer)
            mCustomers.setVisible(true);
        else
            mCustomers.setVisible(false);

        if(station){
            mSaleslite.setVisible(false);
            mStockManageer.setVisible(false);
            mOrder.setVisible(false);
            mCustomers.setVisible(false);
            mDelivery.setVisible(false);
            mStock.setVisible(false);
            mAudit.setVisible(false);
            mForm.setVisible(true);
            mExpense.setVisible(false);
            mPayment.setVisible(false);
            mUsers.setVisible(true);
            rReorder.setVisible(false);
            rSalesByP.setVisible(false);
            rSalesByC.setVisible(false);
            rDelivH.setVisible(false);
            rOrderh.setVisible(false);
            rExpense.setVisible(false);
            rTax.setVisible(false);

            menu.setGroupVisible(R.id.grp_inv,false);
        }
        if(lite){
            mSaleslite.setVisible(true);
            mStockManageer.setVisible(false);
            mOrder.setVisible(false);
            mCustomers.setVisible(false);
            mDelivery.setVisible(false);
            mAudit.setVisible(false);
            mForm.setVisible(false);
            mExpense.setVisible(false);
            mPayment.setVisible(false);
            mUsers.setVisible(false);
            rReorder.setVisible(false);
            rSalesByP.setVisible(false);
            rDelivH.setVisible(false);
            rSalesByC.setVisible(false);
            rOrderh.setVisible(false);
            rExpense.setVisible(false);
            rTax.setVisible(false);
            rStation.setVisible(false);
            mReports.setVisible(true);

            menu.setGroupVisible(R.id.grp_inv, false);
        }

    }

    private void updateReportsMenu(){
        rExpense.setVisible(false);
        if(!xpense)
            rExpense.setVisible(false);
        if(!showdelive)
            rDelivH.setVisible(false);
        if(!showorders)
            rOrderh.setVisible(false);
        if(!station)
            rStation.setVisible(false);
        if(!stock)
            mStockManageer.setVisible(false);
        if(!lite)
        if(sales)
            mSales.setVisible(true);
        else
            mSales.setVisible(false);
        if(!lite)
        if(report) {
            mReports.setVisible(true);
        }
        else {
            mReports.setVisible(false);
        }
        if(!lite)
        if(product)
            mStock.setVisible(true);
        else
            mStock.setVisible(false);
        if(!lite)
        if(customer)
            mCustomers.setVisible(true);
        else
            mCustomers.setVisible(false);

        if(station){
            mSaleslite.setVisible(false);
            mStockManageer.setVisible(false);
            mOrder.setVisible(false);
            mCustomers.setVisible(false);
            mDelivery.setVisible(false);
            mAudit.setVisible(false);
            mForm.setVisible(true);
            mExpense.setVisible(false);
            mPayment.setVisible(false);
            rSalesByC.setVisible(false);
            mUsers.setVisible(true);
            rReorder.setVisible(false);
            rSalesByP.setVisible(false);
            rDelivH.setVisible(false);
            rOrderh.setVisible(false);
            rExpense.setVisible(false);
            rTax.setVisible(false);
            rSalesH.setVisible(false);
            rProfit.setVisible(false);
            rStation.setVisible(true);

        }
        if(lite){
            mSaleslite.setVisible(true);
            mStockManageer.setVisible(false);
            mOrder.setVisible(false);
            mCustomers.setVisible(false);
            mDelivery.setVisible(false);
            mAudit.setVisible(false);
            mForm.setVisible(false);
            mExpense.setVisible(false);
            mPayment.setVisible(false);
            rSalesByC.setVisible(false);
            mUsers.setVisible(false);
            mStock.setVisible(false);
            rReorder.setVisible(false);
            rSalesByP.setVisible(false);
            rDelivH.setVisible(false);
            rOrderh.setVisible(false);
            rExpense.setVisible(false);
            rTax.setVisible(false);
            rStation.setVisible(false);
            mReports.setVisible(true);

            mNavigationView.getMenu().setGroupVisible(R.id.grp_inv, false);
        }
    }
    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putInt(STATE_SELECTED_POSITION, mCurrentSelectedPosition);
    }

    @Override
    protected void onRestoreInstanceState(Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);
        mCurrentSelectedPosition = savedInstanceState.getInt(STATE_SELECTED_POSITION, 0);
        Menu menu = mNavigationView.getMenu();
        menu.getItem(mCurrentSelectedPosition).setChecked(true);
    }

     private void setUpToolbar() {
        mToolbar = (Toolbar) findViewById(R.id.toolbar);
        if (mToolbar != null) {
            setSupportActionBar(mToolbar);
        }
    }
     public Toolbar getmToolbar() {
        if(mToolbar != null)
            return mToolbar;
         return null;
    }

    private void setUpNavDrawer() {
        if (mToolbar != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            if(!tabletSize)
                mToolbar.setNavigationIcon(R.drawable.ic_drawer);
            else
                mToolbar.setNavigationIcon(null);

            mToolbar.setNavigationOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if(!tabletSize)
                        mDrawerLayout.openDrawer(GravityCompat.START);
                }
            });
        }
        mUserLearnedDrawer = true;

        if (!mUserLearnedDrawer) {
            mDrawerLayout.openDrawer(GravityCompat.START);
            mUserLearnedDrawer = true;
            saveSharedSetting(this, PREF_USER_LEARNED_DRAWER, "true");
        }

    }

    public static void saveSharedSetting(Context ctx, String settingName, String settingValue) {
        SharedPreferences sharedPref = ctx.getSharedPreferences(PREFERENCES_FILE, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPref.edit();
        editor.putString(settingName, settingValue);
        editor.apply();
    }

    public static String readSharedSetting(Context ctx, String settingName, String defaultValue) {
        SharedPreferences sharedPref = ctx.getSharedPreferences(PREFERENCES_FILE, Context.MODE_PRIVATE);
        return sharedPref.getString(settingName, defaultValue);
    }

    @Override
    public void onBackPressed() {
        try {
            if (!tabletSize)
                if (mDrawerLayout.isDrawerOpen(GravityCompat.START)) {
                    mDrawerLayout.closeDrawer(GravityCompat.START);
                } else {
                    super.onBackPressed();
                }
            else
                super.onBackPressed();
        } catch (NullPointerException e) {
            e.printStackTrace();
        }

        if (getFragmentManager().getBackStackEntryCount() > 1)
            getFragmentManager().popBackStack();
         else if (getFragmentManager().getBackStackEntryCount() == 1)
            finish();
        else
            super.onBackPressed();

    }

    @Override
    protected void onStart() {
        super.onStart();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {

            getMenuInflater().inflate(R.menu.mainscreen, menu);
            String address = appPreference.getAddress();
            if (address.isEmpty()) {
                menu.findItem(R.id.action_pair).setTitle(getString(R.string.strscanpair));
                CONNECTIONSTATUS = 1;
            } else {
                menu.findItem(R.id.action_pair).setTitle(getString(R.string.strclearpair));
                CONNECTIONSTATUS = 2;
            }
            String auth = appPreference.getAuthkey();
            if(auth.isEmpty()){
                registeritem = menu.findItem(R.id.action_register);
                registeritem.setVisible(true);
                if(extra.equals(Constants.SEARCHSOURCE_MAIN) || extra.equals(Constants.SEARCHSOURCE_HOME)){
                    registeritem.setShowAsAction(MenuItem.SHOW_AS_ACTION_ALWAYS);
                }
            }

        return super.onCreateOptionsMenu(menu);
    }

    /*
         * running the sync adapter periodically
         */
    public void periodicadapersync(int time)
    {
        ContentResolver.addPeriodicSync(
                mAccount,
                ContentProviderApi.AUTHORITY,
                Bundle.EMPTY, time) ;
    }
    /*
     * running the sync adapter on demand
     */
    public void ondemandsync()
    {
        Bundle settingsBundle = new Bundle() ;
        settingsBundle.putBoolean(ContentResolver.SYNC_EXTRAS_MANUAL, true);
        settingsBundle.putBoolean(ContentResolver.SYNC_EXTRAS_EXPEDITED, true);
        ContentResolver.requestSync(mAccount, ContentProviderApi.AUTHORITY, settingsBundle);
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        switch(id){
            case R.id.action_settings:
                Intent intent = new Intent(this, SettingsActivity.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(intent);
                finish();
                return true;
            case R.id.action_syncnow:
                ondemandsync();
                break;
            case R.id.action_pair:
                if(CONNECTIONSTATUS == 2){
                    appPreference.clearAddress();
                    Log.v("ADDRESS"," address cleared now it is "+appPreference.getAddress());
                } else if (CONNECTIONSTATUS == 1) {
                    getAddress();
                }
                break;
            case R.id.action_register:
                check_firstrun();
                break;
        }

        return super.onOptionsItemSelected(item);
    }


    @Override
    public void onResume() {
        super.onResume();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }

    @Override
    protected void onPause() {
        super.onPause();
    }

    @Override
    protected void onStop() {
        super.onStop();
    }


    public void scheduleAlarm() {
        Intent intent = new Intent(getApplicationContext(), LocationTrigger.class);
        final PendingIntent pIntent = PendingIntent.getBroadcast(this, LocationTrigger.REQUEST_CODE,
                intent, PendingIntent.FLAG_UPDATE_CURRENT);
        long firstMillis = System.currentTimeMillis();
        long intervalMillis = 0;
        int minutesintervar = Settings.getTrackTime(this);
        if(minutesintervar == 0){
            intervalMillis = Integer.parseInt(Constants.DEFAULTTRACKINTERVAL);
        } else {
            String interval = String.valueOf(minutesintervar);
            long ii = Long.valueOf(interval);
            intervalMillis = TimeUnit.MINUTES.toMillis(ii);
        }
        if(intervalMillis != 0) {
            AlarmManager alarm = (AlarmManager) this.getSystemService(Context.ALARM_SERVICE);
            alarm.setInexactRepeating(AlarmManager.RTC_WAKEUP, firstMillis, intervalMillis, pIntent);
        }
    }
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        switch (requestCode) {
            case REQUEST_PRINT: {
                if (resultCode == RESULT_OK) {
                    Toast.makeText(this, "Device connectted address "+appPreference.getAddress(),Toast.LENGTH_LONG).show();
                } else {
                    Toast.makeText(this, "Device not connectted",Toast.LENGTH_LONG).show();
                }
                break;
            }
            case REQUEST_ENABLE_BT: {
                if (!mBtAdapter.isEnabled()) {
                    Intent enableBtIntent = new Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE);
                    startActivityForResult(enableBtIntent, REQUEST_ENABLE_BT);
                }
                break;
            }
        }
    }

    private void validate(String source){
        LayoutInflater infl = getLayoutInflater();
        View view = infl.inflate(R.layout.startscreen, null) ;

        final Dialog dialog = new Dialog(this, R.style.CustomDialog	);
        dialog.setCancelable(false);
        dialog.setContentView(view);

        progressBar = (ProgressBar)dialog.findViewById(R.id.progressbar);
        txtsyncsta = (TextView)dialog.findViewById(R.id.txtbody);
        start = (Button)dialog.findViewById(R.id.btnstart);
        progressBar.setVisibility(View.VISIBLE);
        txtsyncsta.setVisibility(View.VISIBLE);

        boolean network = Constants.isNotConnected(this);
        if(!network) {
            Toast.makeText(this, getString(R.string.strinternetwarn), Toast.LENGTH_LONG).show();
            return;
        }
        new DeviceRegistration(dialog.getContext(),progressBar,txtsyncsta,start).execute(source);
        start.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String auth = appPreference.getAuthkey();
                String authtyping = null;
                if (!auth.isEmpty()) {

                    String im = Constants.getIMEINO(dialog.getContext());
                    im = im.concat("|");
                    int acctype = appPreference.get_accounttype();
                    if (acctype == 0)
                        authtyping = Constants.ACCOUNT_TYPE_NORMAL;
                    if (acctype == 1)
                        authtyping = Constants.ACCOUNT_TYPE_PREMIUM;

                    MCrypt mCrypt = new MCrypt();
                    try {
                        String crypted = new String(mCrypt.decrypt(auth));
                        if (im.concat(authtyping).equals(crypted)) {
                            registeritem.setVisible(false);
                            dialog.dismiss();
                        } else {
                            dialog.dismiss();
                            appPreference.clear_authentications();
                            invalidaccount();
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                } else {
                    invalidaccount();
                    appPreference.clear_authentications();
                    dialog.dismiss();
                }
            }
        });
        dialog.show();
    }

    private void invalidaccount(){
        LayoutInflater infl = getLayoutInflater();
        View view = infl.inflate(R.layout.startscreen, null) ;

        final Dialog dialog = new Dialog(this, R.style.CustomDialog	);
        dialog.setCancelable(false);
        dialog.setContentView(view);

        progressBar = (ProgressBar)dialog.findViewById(R.id.progressbar);
        TextView txthead = (TextView)dialog.findViewById(R.id.txtheader);
        start = (Button)dialog.findViewById(R.id.btnstart);
        txthead.setText(getString(R.string.strtokenerror));
        txthead.append("\n\n");
        txthead.append(getString(R.string.strtokenerrormsg));

        Runnable runnable = new Runnable() {
            @Override
            public void run() {
                appPreference.clear_authentications();
                dialog.dismiss();
            }
        };
        Handler handler = new Handler();
        handler.postDelayed(runnable, 15000);

        start.setVisibility(View.VISIBLE);
        start.setText(getString(R.string.strclose));
        start.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                appPreference.clear_authentications();
                dialog.dismiss();
            }
        });
        dialog.show();
    }
    @Override
    public void startActivity(Intent intent) {
        if (Intent.ACTION_SEARCH.equals(intent.getAction())) {
            intent.putExtra(Constants.SEARCHSOURCE, extra);
        }

        super.startActivity(intent);
    }



}