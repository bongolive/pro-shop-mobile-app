/*
 * Copyright (c) 2015.
 * Prosurvey is a property of Bongolive Enterprises Ltd. An authorized
 * copy of ideas, source codes is against the regulations
 */

package bongolive.apps.proshop.view;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

import com.jjoe64.graphview.DefaultLabelFormatter;
import com.jjoe64.graphview.GraphView;
import com.jjoe64.graphview.helper.StaticLabelsFormatter;
import com.jjoe64.graphview.series.BarGraphSeries;
import com.jjoe64.graphview.series.DataPoint;
import com.jjoe64.graphview.series.LineGraphSeries;

import java.util.ArrayList;

import bongolive.apps.proshop.R;
import bongolive.apps.proshop.controller.AppPreference;
import bongolive.apps.proshop.controller.Constants;
import bongolive.apps.proshop.model.Sales;


public class

        Home extends Fragment {

	private static final String ARG_SECTION_NUMBER = "section_number";

	AppPreference appPreference;
	ListView lv ;
	ArrayAdapter<String> adapter ;
    GraphView graph, graph1;
    LineGraphSeries<DataPoint> series = null;
    String imei = "00000";
	ArrayList<String> arraylist ;
    String currency ;
	@Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
    {
       return inflater.inflate(R.layout.home, container, false) ;
    } 
	
	@Override
    public void onActivityCreated(Bundle savedInstanceState)
    { 
        super.onActivityCreated(savedInstanceState);
        appPreference = new AppPreference(getActivity());

        currency = appPreference.getDefaultCurrency();
        currency = currency != null ? currency : "Tsh" ;
        TextView txtinfo = new TextView(getActivity());

        int accounttype = appPreference.get_accounttype();
        LinearLayout view = (LinearLayout)getActivity().findViewById(R.id.main_linear_layout);

        imei = Constants.getIMEINO(getContext());
        if(accounttype == 2) {
            if (view != null)
                view.addView(txtinfo);

            txtinfo.setText(getString(R.string.strdevid) + "\t" + imei.concat("\n\n"));
        }

         graph = (GraphView) getActivity().findViewById(R.id.graph);
         graph1 = (GraphView) getActivity().findViewById(R.id.graph1);

        graph.setTitle(getString(R.string.strtotsales)+" "+getString(R.string.strthisweek));
        BarGraphSeries<DataPoint> series = new BarGraphSeries<>(Sales.getTotalSalesGraph(getActivity()));
        graph.getGridLabelRenderer().setLabelFormatter(new DefaultLabelFormatter() {
            @Override
            public String formatLabel(double value, boolean isValueX) {
                if (isValueX) {
                    // show normal x values
                    return super.formatLabel(value, isValueX);
                } else {
                    // show currency for y values
                    return super.formatLabel(value, isValueX) + currency;
                }
            }
        });

       StaticLabelsFormatter staticLabelsFormatter = new StaticLabelsFormatter(graph);
        staticLabelsFormatter.setHorizontalLabels(getActivity().getResources().getStringArray(R.array.strweekdays));
//        staticLabelsFormatter.setVerticalLabels(new String[]{getString(R.string.strtotsales)});
        graph.getGridLabelRenderer().setLabelFormatter(staticLabelsFormatter);

        graph.addSeries(series);

        graph1.setTitle(getString(R.string.strtotalamountreceived)+" "+getString(R.string.strthisweek));
        BarGraphSeries<DataPoint> series1 = new BarGraphSeries<>(Sales.getTotalOutstandingGraph(getActivity()));
        graph1.getGridLabelRenderer().setLabelFormatter(new DefaultLabelFormatter() {
            @Override
            public String formatLabel(double value, boolean isValueX) {
                if (isValueX) {
                    // show normal x values
                    return super.formatLabel(value, isValueX);
                } else {
                    // show currency for y values
                    return super.formatLabel(value, isValueX) + currency;
                }
            }
        });

       StaticLabelsFormatter staticLabelsFormatter1 = new StaticLabelsFormatter(graph1);
        staticLabelsFormatter1.setHorizontalLabels(getActivity().getResources().getStringArray(R.array.strweekdays));
//        staticLabelsFormatter.setVerticalLabels(new String[]{getString(R.string.strtotsales)});
        graph1.getGridLabelRenderer().setLabelFormatter(staticLabelsFormatter1);

        graph1.addSeries(series1);

    }
}
