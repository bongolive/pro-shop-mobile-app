/*
 * The source codes in this project are owned by Bongolive Enterprises. Copying or any kind of usage without Bongolive's approval is against the law.
 * Copyright (c) 2015.
 *
 */

package bongolive.apps.proshop.view;

import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.ActionBar;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AutoCompleteTextView;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;

import java.util.ArrayList;

import bongolive.apps.proshop.R;
import bongolive.apps.proshop.controller.PaymentSplitAdapter;
import bongolive.apps.proshop.model.Customers;
import bongolive.apps.proshop.model.Payments;
import bongolive.apps.proshop.model.Sales;
import bongolive.apps.proshop.model.Transfer;


public class PaymentScreenFrag extends Fragment
{
    View _rootView;
    private RecyclerView mRecyclerView;
    private RecyclerView.Adapter mAdapter;
    private RecyclerView.LayoutManager mLayoutManager;
    private ActionBar actionBar;
    private EditText etfilter;
    private ArrayList arrayList;
    private RelativeLayout rl;
    private ImageView imgdone,imgnext;
    private AutoCompleteTextView bzn;
    private Toolbar ab;
    long pyid;
    ArrayList<Payments> mItems;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
    {
        _rootView = inflater.inflate(R.layout.selling_screen, container, false);
        return _rootView ;
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState)
    {
        super.onActivityCreated(savedInstanceState);
        setHasOptionsMenu(true);

        ab = ((MainScreen)this.getActivity()).getmToolbar();
        mRecyclerView = (RecyclerView) getActivity().findViewById(R.id.prodct_list);
        mRecyclerView.setHasFixedSize(true);
        mRecyclerView.setItemAnimator(new DefaultItemAnimator());

        mLayoutManager = new LinearLayoutManager(getContext());
        mRecyclerView.setLayoutManager(mLayoutManager);
        mItems = Payments.getPaymentItems(getContext());
        if(mItems.size() < 1){
            Snackbar snackbar = Snackbar
                    .make(mRecyclerView, getString(R.string.strnodata), Snackbar.LENGTH_INDEFINITE);

            snackbar.show();
        }
        mAdapter = new PaymentSplitAdapter(getActivity(), mItems);
        mRecyclerView.setAdapter(mAdapter);

        mRecyclerView.addOnItemTouchListener(
                new RecylerItemClick(getContext(), new RecylerItemClick.OnItemClickListener() {
                    @Override
                    public void onItemClick(View view, int position) {
                        Payments py = mItems.get(position);
                        String localsale = py.getLocalsale();
                        boolean haschild = Payments.anyChild(getContext(), localsale);
                        if((py.getNodetype() == 0 && !haschild) || py.getNodetype() == 1) {
                            pyid = py.getId();
                            String custmerid = Sales.getCustomerLocal(getContext(), localsale);
                            String customer = Customers.getCustomerBusinessNameLoca(getContext(), custmerid);
                            showStation(pyid,customer);
                        }
                    }
                })
        );

    }

    public void showStation(long id, String customer) {

            PaymentDetailFrag newFragment = new PaymentDetailFrag();
            Bundle args = new Bundle();
            args.putLong(Transfer.ID, id);
            args.putString(Sales.CUSTOMERID, customer);
            newFragment.setArguments(args);
            FragmentTransaction transaction = getFragmentManager().beginTransaction();
            transaction.replace(R.id.content_frame, newFragment);
            transaction.addToBackStack(null);
            transaction.commit();
    }


    @Override
    public void onResume() {
        super.onResume();
        if(mAdapter != null)
            mAdapter.notifyDataSetChanged();
    }
}
