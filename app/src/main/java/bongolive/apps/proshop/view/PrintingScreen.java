/*
 * The source codes in this project are owned by Bongolive Enterprises. Copying or any kind of usage without Bongolive's approval is against the law.
 * Copyright (c) 2015.
 *
 */

package bongolive.apps.proshop.view;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.zj.btsdk.BluetoothService;
import com.zj.btsdk.PrintPic;

import java.util.ArrayList;

import bongolive.apps.proshop.R;
import bongolive.apps.proshop.model.Delivery;
import bongolive.apps.proshop.model.Products;
import bongolive.apps.proshop.model.SalesItems;


public class PrintingScreen extends AppCompatActivity {
    MenuItem menupair;

	private static final int REQUEST_ENABLE_BT = 2;
	BluetoothService mService = null;
	BluetoothDevice con_dev = null;
	private static final int REQUEST_CONNECT_DEVICE = 1;  //��ȡ�豸��Ϣ
	ArrayList<SalesItems> arraylist = null;
    ArrayList<String> deliverlist = null;
    String[] header = null;
    ArrayList<String> title = new ArrayList<>();
    TextView txtmsg;
    ActionBar ab;
    FloatingActionButton actionButton;
	
	/** Called when the activity is first created. */
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.printing_screen);
        ab = getSupportActionBar();
		mService = new BluetoothService(this, mHandler);
		//�����������˳�����
		if( mService.isAvailable() == false ){
            Toast.makeText(this, getString(R.string.strnobluetooth), Toast.LENGTH_LONG).show();
            finish();
		}

		Intent intent = getIntent();
		long id = intent.getExtras().getLong("info");
        if(id>0) {
            arraylist = SalesItems.getItemsarraylist(this, id);
            deliverlist = Delivery.getprintdetails(this, id);
        }
        if(arraylist != null && deliverlist != null) {
			System.out.println("size is " + arraylist.size());
			header = new String[]{getString(R.string.strproductname),getString(R.string.strquantity)};
            title.add(getString(R.string.strdate));
            title.add(getString(R.string.strdelref));
            title.add(getString(R.string.strsaleref));
            title.add(getString(R.string.strcustomer));
            title.add(getString(R.string.straddress));
            title.add(getString(R.string.strnotes));
            System.out.println(" title size "+title.size()+" deliv size "+deliverlist.size());
		}
        txtmsg = (TextView)findViewById(R.id.txtmsg);
        String msg = "";
        for(int j = 0; j< deliverlist.size(); j++){
            msg += title.get(j)+" : "+deliverlist.get(j)+"\n";
        }

        /*
        *  TODO ITEMS
        *  update delivery table on status
        *  send deliver on api
        *  store signature in db with the deliver reference
        *  send multimedia on api
        *  format the printing screen
        *  create a report for delivered items
        *  create a report for undelivered but delivered items
        *
        * */


        for(int i = 0; i < arraylist.size(); i++){
            SalesItems salesItems = arraylist.get(i);
            long prod = salesItems.getProdloc();
            int qty = salesItems.getQtydel();
            String prodname = Products.getProdName(this, String.valueOf(prod));
            msg += header[0]+" :  "+prodname+"\t";
            msg += header[1]+" :  "+qty+"\n";
        }
        txtmsg.setText(msg);
        ab.setBackgroundDrawable(getResources().
                getDrawable(R.color.actionbar));
        ab.setDisplayHomeAsUpEnabled(false);

        actionButton = (FloatingActionButton)findViewById(R.id.actionbutton);
        actionButton.setVisibility(View.GONE);
        actionButton.setBackgroundColor(getResources().getColor(R.color.actionbar));
        actionButton.setImageResource(R.drawable.ic_printer);

        actionButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String msg = txtmsg.getText().toString();
                if( msg.length() > 0 ){
                    mService.sendMessage(msg + "\n", "GBK");
                }
            }
        });
	}

    @Override
    public void onStart() {
    	super.onStart();
    	//����δ�򿪣�������
		if( mService.isBTopen() == false)
		{
            Intent enableIntent = new Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE);
            startActivityForResult(enableIntent, REQUEST_ENABLE_BT);
		}
    }
    
	@Override
	protected void onDestroy() {
		super.onDestroy();
		if (mService != null) 
			mService.stop();
		mService = null; 
	}

    /**
     * ����һ��Handlerʵ�������ڽ���BluetoothService�෵�ػ�������Ϣ
     */
    private final Handler mHandler = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            switch (msg.what) {
            case BluetoothService.MESSAGE_STATE_CHANGE:
                switch (msg.arg1) {
                case BluetoothService.STATE_CONNECTED:   //������
                	Toast.makeText(getApplicationContext(), "Connect successful",
							Toast.LENGTH_SHORT).show();
                    actionButton.setVisibility(View.VISIBLE);
                    break;
                case BluetoothService.STATE_CONNECTING:  //��������
                	Log.d("��������", "��������.....");
                    break;
                case BluetoothService.STATE_LISTEN:     //�������ӵĵ���
                case BluetoothService.STATE_NONE:
                	Log.d("��������", "�ȴ�����.....");
                    break;
                }
                break;
            case BluetoothService.MESSAGE_CONNECTION_LOST:    //�����ѶϿ�����
                Toast.makeText(getApplicationContext(), "Device connection was lost",
						Toast.LENGTH_SHORT).show();
                actionButton.setVisibility(View.GONE);
                break;
            case BluetoothService.MESSAGE_UNABLE_CONNECT:     //�޷������豸
            	Toast.makeText(getApplicationContext(), "Unable to connect device",
						Toast.LENGTH_SHORT).show();
            	break;
            }
        }
        
    };
        
    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        switch (requestCode) {
        case REQUEST_ENABLE_BT:      //���������
            if (resultCode == Activity.RESULT_OK) {   //�����Ѿ���
            	Toast.makeText(this, "Bluetooth open successful", Toast.LENGTH_LONG).show();
            } else {                 //�û������������
            	finish();
            }
            break;
        case  REQUEST_CONNECT_DEVICE:     //��������ĳһ�����豸
        	if (resultCode == Activity.RESULT_OK) {   //�ѵ�������б��е�ĳ���豸��
                String address = data.getExtras()
                                     .getString(DeviceListActivity.EXTRA_DEVICE_ADDRESS);  //��ȡ�б������豸��mac��ַ
                con_dev = mService.getDevByMac(address);   
                
                mService.connect(con_dev);
            }
            break;
        }
    } 
    
    //��ӡͼ��
    @SuppressLint("SdCardPath")
	private void printImage() {
    	byte[] sendData = null;
    	PrintPic pg = new PrintPic();
    	pg.initCanvas(384);     
    	pg.initPaint();
    	pg.drawImage(0, 0, "/mnt/sdcard/icon.jpg");
    	sendData = pg.printDraw();
    	mService.write(sendData);   //��ӡbyte������
    }
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
            getMenuInflater().inflate(R.menu.print, menu);
            menupair = menu.findItem(R.id.action_pair);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item)
    {
        switch(item.getItemId())
        {
            case android.R.id.home:
                mService.stop();
                finish();
            case R.id.action_pair:
                Intent serverIntent = new Intent(PrintingScreen.this,DeviceListActivity.class);      //��������һ����Ļ
                startActivityForResult(serverIntent, REQUEST_CONNECT_DEVICE);
                break;
        }

        return false;

    }

}
