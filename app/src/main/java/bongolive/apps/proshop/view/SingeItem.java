/*
 * The source codes in this project are owned by Bongolive Enterprises. Copying or any kind of usage without Bongolive's approval is against the law.
 * Copyright (c) 2015.
 *
 */


package bongolive.apps.proshop.view;


import android.content.Context;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.ArrayList;

import bongolive.apps.proshop.R;
import bongolive.apps.proshop.controller.AppPreference;
import bongolive.apps.proshop.controller.Validating;
import bongolive.apps.proshop.model.Pending_Items;
import bongolive.apps.proshop.model.Products;
import bongolive.apps.proshop.model.Settings;
import bongolive.apps.proshop.model.Warehouse;
import bongolive.apps.proshop.controller.Constants;

public class SingeItem extends AppCompatActivity implements View.OnClickListener{

    private boolean tabletSize = false;
    private ActionBar actionBar;
    private ArrayList<Pending_Items> pitems = new ArrayList<>();
    private long prodid;
    private String source;
    private TextView txttile,txtsubtotal,total,txtunit;
    private EditText etqty,etunit;
    private ImageView imgdone;
    Pending_Items items = null;
    String currency = null;
    final int[] qty = {0};
    final double[] unit = {0} ;
    double subtotal = 0;
    final double[] grandtotal = {0};

    final String[] s_qty = {"0"};
    final String[] g_subtotal = {"0"};
    String s_unit = "0", s_total = "0";
    private int pos = 0;
    AppPreference appPreference;
    /*
    * end of new strings
    */

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        tabletSize = getResources().getBoolean(R.bool.isTablet);
        if (!tabletSize) {
            setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        }
        setContentView(R.layout.single_item);
        actionBar = getSupportActionBar();

        appPreference = new AppPreference(this);
        Intent intent = getIntent();
        prodid = intent.getExtras().getLong(Products.PRODUCTNAME);
        pos = intent.getExtras().getInt(Products.ID);
        source = intent.getExtras().getString(Constants.SOURCE);

        if(prodid == 0)
            finish();
        if(!Validating.areSet(source))
            finish();

        currency = appPreference.getDefaultCurrency();
        currency = currency != null ? currency : "Tsh";
        assert actionBar != null;
        actionBar.setDisplayShowCustomEnabled(true);  actionBar.setDisplayShowTitleEnabled(false);
        actionBar.setDisplayShowHomeEnabled(false);   actionBar.setDisplayHomeAsUpEnabled(false);

        LayoutInflater inflater = (LayoutInflater)this.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View v = inflater.inflate(R.layout.filter_singleitem,null);
        actionBar.setCustomView(v);

        imgdone = (ImageView)v.findViewById(R.id.imgdone);
        boolean dis = Settings.getDiscountEnabled(this);
        if(!dis)
            etunit.setEnabled(false);

        imgdone.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                process_item();
                finish();
            }
        });

        txttile = (TextView)findViewById(R.id.txttitle);
        txtsubtotal = (TextView)findViewById(R.id.txtsubtotal_value);
        total = (TextView)findViewById(R.id.txttotal_value);
        etqty = (EditText)findViewById(R.id.txtqty_value);
        etunit = (EditText)findViewById(R.id.txtunit_valued);
        txtunit = (TextView)findViewById(R.id.txtunit_value);

        double maxdiscount = Settings.getDiscountLimit(SingeItem.this);
        String lmt = getString(R.string.strmaxdiscount) + " " + maxdiscount;
        etunit.setHint(lmt);

        pitems = Pending_Items.getItems(this,prodid,source);

        String product = Products.getProdName(this,String.valueOf(prodid));
        txttile.setText(product);
        if(pitems.size() > 0) {
            items = pitems.get(0);
            qty[0] = new BigDecimal(items.getQty()).intValue();
            unit[0] = items.getPrice();
            subtotal = new BigDecimal(qty[0]).multiply(new BigDecimal(unit[0])).doubleValue();
            grandtotal[0] = items.getSbtotal();
        } else {
            unit[0] = Products.getUnitPrice(this, prodid);
        }

        updateViews(qty[0],grandtotal[0]);

        etunit.setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                if (event.getAction() == KeyEvent.ACTION_DOWN && event.getKeyCode() == KeyEvent.KEYCODE_ENTER) {
                    String prc = etunit.getText().toString();
                    if(Validating.areSet(prc)) {
                        double maxdiscount = Settings.getDiscountLimit(SingeItem.this);
                        if (!Constants.checkDouble(SingeItem.this, prc, etqty))
                            return true;
                        double pr = new BigDecimal(prc).setScale(2, RoundingMode.HALF_UP).doubleValue();
                        if (pr < maxdiscount) {
                            pr = new BigDecimal(pr).divide(new BigDecimal(100)).setScale(2,RoundingMode.HALF_UP).doubleValue();
                            System.out.println(" pr is " + pr);
                            BigDecimal dis = new BigDecimal(unit[0]).multiply(new BigDecimal(pr));
                            unit[0] = new BigDecimal(unit[0]).subtract(dis).setScale(2,RoundingMode.HALF_UP)
                                    .doubleValue();
                            System.out.println(" unit[0] is " + unit[0]);

                            process_item();

                            updateViews(qty[0],grandtotal[0]);
                            InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                            imm.hideSoftInputFromWindow(getCurrentFocus().getWindowToken(), 0);
                        }
                        else {
                            Toast.makeText(SingeItem.this, getString(R.string.stramountwarn), Toast
                                    .LENGTH_LONG).show();
                            etunit.setText("");
                            etunit.requestFocus();
                        }
                    }
                    return false;
                }
                return false;
            }
        });

        etqty.setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                if (event.getAction() == KeyEvent.ACTION_DOWN && event.getKeyCode() == KeyEvent.KEYCODE_ENTER) {
                    String qa = etqty.getText().toString();
                    if(Validating.areSet(qa))
                    {
                        int qaint = new BigDecimal(qa.trim()).intValue();
                        if(!Constants.checkDouble(SingeItem.this, qa, etqty))
                            qaint = 0;

                        if(Products.checkQty(SingeItem.this,prodid,qaint)){
                            qty[0] = qaint;
                            s_qty[0] = String.valueOf(qty[0]);
                            etqty.setText(s_qty[0]);

                            process_item();

                            InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                            imm.hideSoftInputFromWindow(getCurrentFocus().getWindowToken(), 0);
                        } else {
                            Toast.makeText(SingeItem.this,SingeItem.this.getResources().getString(R.string
                                    .strsalesquantityerror1) +" " + Products.getItemOnHand(SingeItem.this,String
                                    .valueOf(prodid)) + " " + SingeItem.this.getResources()
                                    .getString(R.string.strsalesquantityerror2), Toast.LENGTH_LONG).show();
                            etqty.setText(s_qty[0]);
                            InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                            imm.hideSoftInputFromWindow(getCurrentFocus().getWindowToken(), 0);
                        }
                    }
                    return false;
                }
                return false;
            }
        });

    }

    private void updateViews(int qt, double gt){
        double un = unit[0];
        double st = new BigDecimal(qt).multiply(new BigDecimal(un)).doubleValue();
        s_qty[0] = String.valueOf(qt);
        g_subtotal[0] = new BigDecimal(gt).setScale(2, RoundingMode.HALF_UP).toString();
        s_total =  new BigDecimal(st).setScale(2, RoundingMode.HALF_UP).toString();
        s_unit = new BigDecimal(un).setScale(2, RoundingMode.HALF_UP).toString();

        txtsubtotal.setText(s_total);
        etqty.setText(s_qty[0]);
        txtunit.setText(s_unit);
        total.setText(g_subtotal[0]);
    }


    private void process_item() {
        String locprod = String.valueOf(prodid);
        String prodno = Products.getSysProdidBasedOnLocalId(this, locprod);
        String whid = Warehouse.getDefaultId(this);

        int q =  new BigDecimal(qty[0]).intValue();
        int taxmethod = Products.getTaxmethod(this, prodid);
        double tx = Products.getTax(this, prodid);
        double pr = unit[0];
        double taxamount = 0;
        double netsaleprice = 0;
        double saleprice = 0;
        double subtotal = 0;
        long prodlocal = prodid;

        long i = 0;
        boolean __taxsettings = Settings.getTaxEnabled(this);
        double tax = Constants.itemtax(pr, tx, taxmethod, __taxsettings);
        netsaleprice = Constants.netprice(pr, tx, taxmethod, __taxsettings);
        saleprice = Constants.saleprice(pr, tax, taxmethod, __taxsettings);

        subtotal = Constants.calculate_grandtotal(saleprice, q);
        double netsubtotal = Constants.calculate_grandtotal(netsaleprice, q);
        taxamount = new BigDecimal(subtotal).subtract(new BigDecimal(netsubtotal)).setScale(2,
                RoundingMode.HALF_UP).doubleValue();

        this.grandtotal[0] = subtotal;
        g_subtotal[0] = new BigDecimal(this.grandtotal[0]).setScale(2, RoundingMode.HALF_UP).toString();

        total.setText(g_subtotal[0]);

        String spr = String.valueOf(saleprice);
        String nspr = String.valueOf(netsaleprice);
        String txa = String.valueOf(taxamount);
        String stxr = String.valueOf(tx);

        pitems.add(new Pending_Items(prodlocal, whid, i, q, netsaleprice, saleprice, taxamount,
                subtotal, tx, 0, prodno, Constants.SALETYPE_SALE,pos));

        String[] vals = {prodno,locprod,s_qty[0],nspr,spr,txa,g_subtotal[0],whid,"0",stxr,Constants.SALETYPE_SALE,
                String.valueOf(pos)};
        if(Validating.areSet(vals)){
            if(!Pending_Items.isProdIdExist(this,new String[]{vals[1],vals[10]})){
                boolean insert = Pending_Items.insertItems(this,vals);
                pitems = Pending_Items.getItems(this,prodid,source);
                items = pitems.get(0);
                updateViews(qty[0],grandtotal[0]);
            } else {
                boolean insert = Pending_Items.updateItems(this, vals);
                pitems = Pending_Items.getItems(this,prodid,source);
                items = pitems.get(0);
                updateViews(qty[0],grandtotal[0]);
            }
        }
    }
    @Override
    public boolean onKeyUp(int keyCode, KeyEvent event) {
        if(keyCode == KeyEvent.KEYCODE_BACK)
        {
                finish();
                return true;
        } else
            return super.onKeyUp(keyCode, event);
    }

    @Override
    public void onClick(View v) {

    }
}


