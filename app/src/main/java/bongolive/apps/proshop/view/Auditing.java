/*
 * The source codes in this project are owned by Bongolive Enterprises. Copying or any kind of usage without Bongolive's approval is against the law.
 * Copyright (c) 2015.
 *
 */


package bongolive.apps.proshop.view;


import android.app.DatePickerDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.graphics.Bitmap;
import android.hardware.Camera;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.support.design.widget.Snackbar;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.CardView;
import android.text.InputType;
import android.util.Log;
import android.view.ContextThemeWrapper;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ScrollView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Locale;

import bongolive.apps.proshop.R;
import bongolive.apps.proshop.controller.AppPreference;
import bongolive.apps.proshop.controller.Constants;
import bongolive.apps.proshop.controller.CustomerAdapterAudit;
import bongolive.apps.proshop.controller.GpsTracker;
import bongolive.apps.proshop.controller.ProductAdapterAudit;
import bongolive.apps.proshop.controller.Validating;
import bongolive.apps.proshop.model.Audit;
import bongolive.apps.proshop.model.AuditResponses;
import bongolive.apps.proshop.model.Customers;
import bongolive.apps.proshop.model.MultimediaContents;
import bongolive.apps.proshop.model.Products;

public class Auditing extends AppCompatActivity {

    private boolean tabletSize = false;
    private ActionBar actionBar;
    TextView txtphoto;
    GpsTracker gpsTracker;
    AppPreference appPreference;
    /*
    * end of new strings
    */
    long audit, prodl = 0,custlc = 0;
    String prod, cust,image, prodnm, custnm;

    ArrayList<String> products = new ArrayList<>();
    ArrayList<Long> productsids = new ArrayList<>();
    ArrayList<String> customers = new ArrayList<>();
    ArrayList<Long> customerids = new ArrayList<>();
    ArrayList<Audit> auditlist;
    ArrayList<AuditResponses> reslist = new ArrayList<>();
    HashMap<String, AuditResponses> response = new HashMap<>();

    private static final int TAKE_PHOTO = 3;
    private static String product_img;
    private EditText etfilter;
    private ArrayList arrayList;
    private Spinner spp,cuspinner;
    private long customer;
    Snackbar snackbar;
    private ProductAdapterAudit adapter;
    private CustomerAdapterAudit custadapter;
    CardView cd1,cd2,cd3,cd4,cd5,cd6,cd7,cd8,cd9,cd10,cd11,cd12,cd13,cd14,cd15,cd16;
    CardView[] cards ;
    ScrollView scrollView;
    MenuItem menuItem;
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        tabletSize = getResources().getBoolean(R.bool.isTablet);
        if (!tabletSize) {
            setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        }
        setContentView(R.layout.auditing);
        actionBar = getSupportActionBar();

        appPreference = new AppPreference(this);
        Intent intent = getIntent();
        assert actionBar != null;

        gpsTracker = new GpsTracker(this);

        spp = (Spinner)findViewById(R.id.spproducts);
        cuspinner = (Spinner)findViewById(R.id.spcustomers);
        scrollView = (ScrollView)findViewById(R.id.scroll);

        cd1 = (CardView)findViewById(R.id.card1);
        cd2 = (CardView)findViewById(R.id.card2);
        cd3 = (CardView)findViewById(R.id.card3);
        cd4 = (CardView)findViewById(R.id.card4);
        cd5 = (CardView)findViewById(R.id.card5);
        cd6 = (CardView)findViewById(R.id.card6);
        cd7 = (CardView)findViewById(R.id.card7);
        cd8 = (CardView)findViewById(R.id.card8);
        cd9 = (CardView)findViewById(R.id.card9);
        cd10 = (CardView)findViewById(R.id.card10);
        cd11 = (CardView)findViewById(R.id.card11);
        cd12 = (CardView)findViewById(R.id.card12);
        cd13 = (CardView)findViewById(R.id.card13);
        cd14 = (CardView)findViewById(R.id.card14);
        cd15 = (CardView)findViewById(R.id.card15);
        cd16 = (CardView)findViewById(R.id.card16);
        cards = new CardView[]{cd1,cd2,cd3,cd4,cd5,cd6,cd7,cd8,cd9,cd10,cd11,cd12,cd13,cd14,cd15};

        audit = intent.getExtras().getLong(Audit.AUDITID);
        auditlist = Audit.getAuditList(this, audit);

        actionBar.setDisplayUseLogoEnabled(true);
        actionBar.setDisplayHomeAsUpEnabled(true);

        products = Products.getAllProducts(this);
        productsids = Products.getAllProductIds(this);
        customers = Customers.getBuss(this);
        customerids = Customers.getBussId(this);
        cd16.setVisibility(View.VISIBLE);
        if(scrollView.getVisibility() == View.GONE)
            scrollView.setVisibility(View.VISIBLE);

        Snackbar.make(findViewById(R.id.card16), R.string.strchosecust,
                Snackbar.LENGTH_INDEFINITE)
                .show();

        custadapter = new CustomerAdapterAudit(this,customers);

        cuspinner.setAdapter(custadapter);


        cuspinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                if (position > 0) {
                    custnm = customers.get(position);
                    custlc = customerids.get(position);
                    cust = Customers.getSysCustidBasedOnLocalId(Auditing.this, String.valueOf(custlc));
                    actionBar.setTitle(custnm.toUpperCase());
                    cuspinner.setVisibility(View.GONE);
                    spp.setVisibility(View.VISIBLE);
                    adapter = new ProductAdapterAudit(Auditing.this,products,productsids,new long[]{audit,custlc});
                    spp.setAdapter(adapter);
                    Snackbar.make(findViewById(R.id.card16), R.string.strchoseprod,
                            Snackbar.LENGTH_LONG)
                            .show();
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        spp.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                if (custlc > 0) {
                    if (position > 0) {
                        prodnm = products.get(position);
                        prodl = productsids.get(position);
                        prod = Products.getSysProdidBasedOnLocalId(Auditing.this, String.valueOf(prodl));
                        cd16.setVisibility(View.GONE);
                        menuItem.setVisible(true);
                        showthemall();
                    }
                } else {
                    spp.setVisibility(View.GONE);
                    cuspinner.setVisibility(View.VISIBLE);
                    Toast.makeText(Auditing.this, getString(R.string.strchosecust), Toast.LENGTH_LONG).show();
                    spp.setSelection(0);
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

    }

    private void showthemall() {
        long[] where = {audit, custlc,prodl};

        reslist = AuditResponses.getAuditResponse(this, where);

        if(custlc > 0 && prodl > 0) {
            scrollView.setVisibility(View.VISIBLE);
            for (int i = 0; i < auditlist.size(); i++) {
                Audit au = auditlist.get(i);
                final String cp = au.getCaption();
                final String optid = au.getOptionid();
                long id = au.getId();
                final long auid = au.getAuid();
                int gps = au.getGps();
                final int inptype = au.getInputtype();
                int optype = au.getOptiontype();
                cards[i].setVisibility(View.VISIBLE);
                cards[i].removeAllViews();
                switch (optype) {
                    case 1:
                        CheckBox cb = new CheckBox(this);
                        Log.e("input", "input type " + inptype + " option type " + optype);
                        cb.setText(cp);
                        cb.setId(i + 1);
                        cb.setChecked(false);
                        if (reslist.size() > 0) {
                            cb.setChecked(false);
                            for (AuditResponses r : reslist) {
                                if (r.getOptid().equals(optid)) {
                                    String res = r.getResp();
                                    if (res.equals("1"))
                                        cb.setChecked(true);
                                    else
                                        cb.setChecked(false);
                                }
                            }
                        }
                        cards[i].addView(cb, new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT));
                        cb.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                            @Override
                            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                                if (isChecked) {
                                    String ans = "1";
                                    add_response(ans, optid);
                                } else {
                                    String ans = "0";
                                    add_response(ans, optid);
                                }
                            }
                        });
                        break;
                    case 2:
                        final EditText et = new EditText(this);
                        et.setHint(cp);
                        et.setId(i + 1);
                        et.setBackgroundResource(R.drawable.edittext_bg);
                        et.setSingleLine();
                        et.setSelectAllOnFocus(true);

                        if (reslist.size() > 0) {
                            et.setText("");
                            for (AuditResponses r : reslist) {
                                if (r.getOptid().equals(optid)) {
                                    String res = r.getResp();
                                    if (Validating.areSet(res))
                                        et.setText(res);
                                }
                            }
                        }

                        switch (inptype) {
                            case 2:
                                Log.e("INPUTTYPE", "input " + inptype);
                                et.setInputType(InputType.TYPE_NUMBER_FLAG_SIGNED | InputType.TYPE_CLASS_NUMBER);
                                break;
                            case 3:
                                Log.e("INPUTTYPE", "input " + inptype);
                                et.setInputType(InputType.TYPE_NUMBER_FLAG_DECIMAL | InputType.TYPE_CLASS_NUMBER);
                                break;
                            case 4:
                                Log.e("INPUTTYPE", "input " + inptype);
                                et.setFocusable(false);
                                et.setFocusableInTouchMode(false);
                                et.setText(cp);
                                et.setOnClickListener(new View.OnClickListener() {
                                    @Override
                                    public void onClick(View v) {
                                        Constants.setDateTimeField(Auditing.this, et);
                                    }
                                });
                                break;
                        }

                        et.setOnKeyListener(new View.OnKeyListener() {
                            @Override
                            public boolean onKey(View v, int keyCode, KeyEvent event) {
                                if (event.getAction() == KeyEvent.ACTION_DOWN && event.getKeyCode() == KeyEvent.KEYCODE_ENTER) {
                                    Log.i("event", "captured");
                                    InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                                    imm.hideSoftInputFromWindow(getCurrentFocus().getWindowToken(), 0);
                                    String ans = et.getText().toString().trim();
                                    add_response(ans, optid);
                                }
                                return false;
                            }


                        });
                        cards[i].addView(et, new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT));
                        break;
                    case 3:
                        final Button txtimg = new Button(new ContextThemeWrapper(this, R.style.boarderless), null, R.style.boarderless);
                        txtimg.setText(getString(R.string.straddphoto));
                        cards[i].setId(i+1);
                        cards[i].addView(txtimg);
                        Log.e("input", "input type " + inptype + " option type " + optype);
                        final ImageView img = new ImageView(this);
                        cards[i].addView(img);
                        img.setVisibility(View.GONE);

                        if (reslist.size() > 0) {
                            img.setImageBitmap(null);
                            txtimg.setVisibility(View.VISIBLE);
                            img.setVisibility(View.GONE);
                            for (AuditResponses r : reslist) {
                                if (r.getOptid().equals(optid)) {
                                    String res = r.getResp();
                                    if (Validating.areSet(res)) {
                                        String path = MultimediaContents.getImage(this, res);
                                        Bitmap btmp = Constants.decodeImg(path, 300, 300);
                                        if (btmp != null) {
                                            img.setVisibility(View.VISIBLE);
                                            txtimg.setVisibility(View.GONE);
                                            img.setImageBitmap(btmp);
                                        }
                                    }
                                }
                            }
                        }

                        txtimg.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                take_photo(img, txtimg, optid);
                            }
                        });
                        break;
                    case 4:
                        final Button txt =  new Button(new ContextThemeWrapper(this, R.style.boarderless), null, R.style.boarderless);
                        txt.setId(i + 1);
                        cards[i].addView(txt);
                        System.out.println(" res " + reslist.size());
                        if (reslist.size() > 0) {
                            for (AuditResponses r : reslist) {
                                if (r.getOptid().equals(optid)) {
                                    String res = r.getResp();
                                    System.out.println(" res "+res);
                                    if (Validating.areSet(res))
                                        txt.setText(res);
                                }
                            }
                        } else
                            txt.setText(cp);

                        txt.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                Calendar newCalendar = Calendar.getInstance();
                                final SimpleDateFormat dateFormatter;
                                dateFormatter = new SimpleDateFormat("yyyy-MM-dd", Locale.US);
                                DatePickerDialog fromDatePickerDialog = new DatePickerDialog(Auditing.this,
                                        new DatePickerDialog.OnDateSetListener() {

                                    public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                                        Calendar newDate = Calendar.getInstance();
                                        newDate.set(year, monthOfYear, dayOfMonth);
                                        txt.setText(dateFormatter.format(newDate.getTime()));
                                        String ans = txt.getText().toString();
                                        Log.e("ans", "ans is " + ans);
                                        if (!ans.equals(cp))
                                            add_response(ans, optid);

                                    }

                                }, newCalendar.get(Calendar.YEAR), newCalendar.get(Calendar.MONTH), newCalendar.get(Calendar.DAY_OF_MONTH));
                                fromDatePickerDialog.show();

                            }
                        });

                        break;
                    case 5:
                        String lt = String.valueOf(gpsTracker.getLatitude());
                        String ln = String.valueOf(gpsTracker.getLongitude());
                        String ans = lt.concat(",").concat(ln);
                        add_response(ans, optid);
                        break;
                }
            }
        } else {
            scrollView.setVisibility(View.GONE);
        }
        Log.e("array", "array size is " + auditlist.size());
    }

    private void add_response(String ans, String optid){

        response.put(optid, new AuditResponses(prod, String.valueOf(prodl), optid, cust,
                String.valueOf(custlc), ans, "0", audit));
            Log.e("RESPONESIZE", "Response size " + response.size()+ " opt id "+optid);
    }

    private void process_item() {

        for(Audit au: auditlist){
            if(response.size() > 0){
                if(response.containsKey(au.getOptionid())){
                    AuditResponses re = response.get(au.getOptionid());
                    if(au.getOptiontype() == 3){
                        String path = re.getResp();
                        String[] nm = path.split("/");
                        String timestamp = String.valueOf(System.currentTimeMillis());
                        String[] med = {path,nm[nm.length-1], MultimediaContents.OPERATIONTYPEA,timestamp};
                        boolean stored = MultimediaContents.storeMultimedia(this, med);
                        if (stored) {
                            System.out.println("image stored ? " + stored);
                        }

                        String[] v = {re.getProd(), re.getProdl(), re.getOptid(), String.valueOf(re.getAuid()),
                                    re.getCust(), re.getCustl(), timestamp};

                        if (!AuditResponses.isResponseExisting(this, v))
                                AuditResponses.insert(this, v);
                        else
                                AuditResponses.update(this, v);

                    } else {
                        String[] v = {re.getProd(), re.getProdl(), re.getOptid(), String.valueOf(re.getAuid()),
                                re.getCust(), re.getCustl(), re.getResp()};
                        if (!AuditResponses.isResponseExisting(this, v))
                            AuditResponses.insert(this, v);
                        else
                            AuditResponses.update(this, v);
                    }
                    adapter.notifyDataSetChanged();
                }
            }
        }
        Snackbar.make(findViewById(R.id.card16), R.string.strauditdded,
                Snackbar.LENGTH_LONG)
                .show();
    }

    @Override
    public boolean onKeyUp(int keyCode, KeyEvent event) {
        if (keyCode == KeyEvent.KEYCODE_BACK) {
            finish();
            return true;
        } else
            return super.onKeyUp(keyCode, event);
    }

    public void take_photo(final ImageView img, final Button txtimg, final String optid) {

        LayoutInflater infl = (LayoutInflater)getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View view = infl.inflate(R.layout.camerascreen, null);

        final Dialog dg = new Dialog(this, R.style.CustomDialog);
        dg.setCancelable(true);
        dg.setContentView(view);
        dg.show();

        Camera mCamera = null;
        final int MEDIA_TYPE_IMAGE = 1;
        PhotoPreview mCameraView = null;
        try {
            mCamera = Camera.open();//you can use open(int) to use different cameras
        } catch (Exception e) {
            Log.d("ERROR", "Failed to get camera: " + e.getMessage());
            Toast.makeText(this,getString(R.string.strcamerafailed), Toast.LENGTH_LONG).show();
            dg.dismiss();
        }

        if (mCamera != null) {
            mCameraView = new PhotoPreview(this, mCamera);//create a SurfaceView to show camera data
            FrameLayout camera_view = (FrameLayout)dg.findViewById(R.id.camera_view);
            camera_view.addView(mCameraView);//add the SurfaceView to the layout
        }

        //btn to close the application
        ImageButton imgClose = (ImageButton)dg.findViewById(R.id.imgClose);
        ImageButton imgSave = (ImageButton) dg.findViewById(R.id.imgTake);
        imgClose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Log.v("log_tag", "Panel Canceled");
                dg.dismiss();
            }

        });

        final Camera.PictureCallback mPicture = new Camera.PictureCallback() {

            @Override
            public void onPictureTaken(byte[] data, Camera camera) {

                File pictureFile = getOutputMediaFile(MEDIA_TYPE_IMAGE);
                if (pictureFile == null){
                    return;
                }

                try {
                    FileOutputStream fos = new FileOutputStream(pictureFile);
                    fos.write(data);
                    fos.close();
                } catch (FileNotFoundException e) {
                } catch (IOException e) {
                }
                if(pictureFile != null)
                {
                    String path = pictureFile.getAbsolutePath().toString();
                    System.out.println(path);
                    //return/save image here
                    img.setVisibility(View.VISIBLE);
                    txtimg.setVisibility(View.GONE);
                    Bitmap btm = Constants.decodeImg(path, 300, 300);
                    img.setImageBitmap(btm);
                        add_response(path, optid);
                    dg.dismiss();
                }
            }

            private File getOutputMediaFile(int type) {

                    File sdcard = Environment.getExternalStorageDirectory() ;

                    File folder = new File(sdcard.getAbsoluteFile(), Constants.MASTER_DIR);
                    if(!folder.exists())
                        folder.mkdir();
                    String filename = String.valueOf(System.currentTimeMillis());
                    File file = new File(folder.getAbsoluteFile(), filename + ".jpg") ;

                    return file;
            }
        };


        final Camera finalMCamera = mCamera;
        imgSave.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                try {
                    finalMCamera.takePicture(null, null, mPicture);
                } catch (NullPointerException e) {
                    e.printStackTrace();
                }
            }

        });

    }

    @Override
    public void onStart() {
        super.onStart();
        turnGpsOn() ;
    }

    private void startAct(){
        Intent intent = new Intent(this, Dashboard.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        startActivity(intent);
        finish();
    }

    public void turnGpsOn(){
        int accounttype = appPreference.get_accounttype();
        if(accounttype == 1)
            if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
                if(!gpsTracker.isgpson()) {
                    System.out.println(" isgpson?? " + gpsTracker.isgpson());
                    Toast.makeText(this,getString(R.string.strgps),Toast.LENGTH_LONG).show();
                    Intent inte = new Intent(android.provider.Settings.ACTION_LOCATION_SOURCE_SETTINGS);
                    startActivity(inte);
                }
            } else {
                Intent intent = new Intent("android.location.GPS_ENABLED_CHANGE");
                intent.putExtra("enabled", true);
                sendBroadcast(intent);
            }
    }


    @Override
    public void onResume() {
        super.onResume();
        turnGpsOn();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        if(gpsTracker.isgpson())
            gpsTracker.stopUsingGPS();
        turnGpsOf();
    }

    @Override
    public void onPause() {
        super.onPause();
        if(gpsTracker.isgpson())
            gpsTracker.stopUsingGPS();
        turnGpsOf();
    }
    public void turnGpsOf(){
        if(Build.VERSION.SDK_INT < Build.VERSION_CODES.KITKAT) {
            Intent intent = new Intent("android.location.GPS_ENABLED_CHANGE");
            intent.putExtra("enabled", false);
            sendBroadcast(intent);
        }
    }

    @Override
    public void onStop() {
        super.onStop();
        if(gpsTracker.isgpson())
            gpsTracker.stopUsingGPS();
        turnGpsOf();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.audit, menu);
        menuItem = menu.findItem(R.id.action_tick);
        menuItem.setVisible(false);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                if(custlc == 0)
                    finish();
                else {
                    prod = null;
                    prodl = 0;
                    cust = null;
                    custlc = 0;
                    spp.setVisibility(View.GONE);
                    cuspinner.setVisibility(View.VISIBLE);
                    showthemall();
                    menuItem.setVisible(false);
                }
                return true;
            case R.id.action_tick:
                process_item();
                break;
        }
        return super.onOptionsItemSelected(item);
    }
}


