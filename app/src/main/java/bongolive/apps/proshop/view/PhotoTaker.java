/*
 * Copyright (c) 2015. All rights reserved. The source codes are property of bongolive
 */


package bongolive.apps.proshop.view;

import android.app.Activity;
import android.content.Intent;
import android.hardware.Camera;
import android.os.Bundle;
import android.os.Environment;
import android.util.Log;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ImageButton;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;

import bongolive.apps.proshop.R;
import bongolive.apps.proshop.controller.Constants;

public class PhotoTaker extends Activity {

    private Camera mCamera = null;
    private PhotoPreview mCameraView = null;

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.camerascreen);

        try {
            mCamera = Camera.open();//you can use open(int) to use different cameras
        } catch (Exception e) {
            Log.d("ERROR", "Failed to get camera: " + e.getMessage());
        }

        if (mCamera != null) {
            mCameraView = new PhotoPreview(this, mCamera);//create a SurfaceView to show camera data
            FrameLayout camera_view = (FrameLayout) findViewById(R.id.camera_view);
            camera_view.addView(mCameraView);//add the SurfaceView to the layout
        }

        //btn to close the application
        ImageButton imgClose = (ImageButton) findViewById(R.id.imgClose);
        ImageButton imgSave = (ImageButton) findViewById(R.id.imgTake);
        imgClose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Log.v("log_tag", "Panel Canceled");
                Bundle b = new Bundle();
                Intent intent = new Intent();
                intent.putExtras(b);
                setResult(RESULT_CANCELED, intent);
                finish();
            }

        });


        imgSave.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mCamera.takePicture(null,null,mPicture);
            }

        });
    }

    private Camera.PictureCallback mPicture = new Camera.PictureCallback() {

        @Override
        public void onPictureTaken(byte[] data, Camera camera) {

            File pictureFile = getOutputMediaFile(MEDIA_TYPE_IMAGE);
            if (pictureFile == null){
                return;
            }

            try {
                FileOutputStream fos = new FileOutputStream(pictureFile);
                fos.write(data);
                fos.close();
            } catch (FileNotFoundException e) {
            } catch (IOException e) {
            }
            if(pictureFile != null)
            {
                String path = pictureFile.getAbsolutePath().toString();
                System.out.println(path);
                //return/save image here
                Intent intent = new Intent();
                intent.putExtra("data", path);
                if (getParent() == null) {
                    setResult(Activity.RESULT_OK, intent);
                } else {
                    getParent().setResult(Activity.RESULT_OK, intent);
                }
                finish();
            }
        }
    };
    public static final int MEDIA_TYPE_IMAGE = 1;

    private static File getOutputMediaFile(int type){

        File sdcard = Environment.getExternalStorageDirectory() ;

        File folder = new File(sdcard.getAbsoluteFile(), Constants.MASTER_DIR);
        if(!folder.exists())
            folder.mkdir();
        String filename = String.valueOf(System.currentTimeMillis());
        File file = new File(folder.getAbsoluteFile(), filename + ".jpg") ;

        return file;
    }

}
