/*
 * The source codes in this project are owned by Bongolive Enterprises. Copying or any kind of usage without Bongolive's approval is against the law.
 * Copyright (c) 2015.
 *
 */

package bongolive.apps.proshop.view;

import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.ActionBar;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AutoCompleteTextView;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;

import java.util.ArrayList;

import bongolive.apps.proshop.R;
import bongolive.apps.proshop.controller.Constants;
import bongolive.apps.proshop.controller.TransferAdapter;
import bongolive.apps.proshop.model.Customers;
import bongolive.apps.proshop.model.StockIssue;
import bongolive.apps.proshop.model.Transfer;


public class ProcurementFrag extends Fragment
{
    View _rootView;
    private boolean tabletSize;
    private RecyclerView mRecyclerView;
    private RecyclerView.Adapter mAdapter;
    private RecyclerView.LayoutManager mLayoutManager;
    private ActionBar actionBar;
    private EditText etfilter;
    private ArrayList arrayList;
    private RelativeLayout rl;
    private ImageView imgdone,imgnext;
    private AutoCompleteTextView bzn;
    private ActionBar ab;
    ArrayList<Transfer> mItems = new ArrayList<>();
    ArrayList<StockIssue> mItems_action = new ArrayList<>();
    String action;
    int actionno ;
    FloatingActionButton actionButton;
    /*
    * end of new strings
    */

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
    {
        _rootView = inflater.inflate(R.layout.selling_screen, container, false);
        tabletSize = getResources().getBoolean(R.bool.isTablet);
        return _rootView ;
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState)
    {
        super.onActivityCreated(savedInstanceState);

        mRecyclerView = (RecyclerView) getActivity().findViewById(R.id.prodct_list);
        mRecyclerView.setHasFixedSize(true);
        mRecyclerView.setItemAnimator(new DefaultItemAnimator());

        actionButton = (FloatingActionButton) getActivity().findViewById(R.id.actionbutton);
        actionButton.setBackgroundColor(getResources().getColor(R.color.actionbar));
        actionButton.setImageResource(R.drawable.ic_action_add);
        action = getArguments().getString(Constants.SOURCE);
        switch (action){
            case Constants.TRANSTYPE_REQUEST:
                mItems = Transfer.getItems(getContext(), action);
                actionButton.setVisibility(View.VISIBLE);
                actionno = 1;
                mAdapter = new TransferAdapter(getActivity(), mItems);
                mRecyclerView.setAdapter(mAdapter);
                break;
            case Constants.TRANSTYPE_ISSUE:
                mItems_action = StockIssue.getItems(getContext());
                actionButton.setVisibility(View.VISIBLE);
                actionno = 2;
                mAdapter = new TransferAdapter(getActivity(), mItems_action, action);
                mRecyclerView.setAdapter(mAdapter);
                break;
            case Constants.TRANSTYPE_TRANSFER:
                mItems = Transfer.getItems(getContext(),action);
                actionno = 3;
                mAdapter = new TransferAdapter(getActivity(), mItems);
                mRecyclerView.setAdapter(mAdapter);
                break;
        }

        mLayoutManager = new LinearLayoutManager(getContext());
        mRecyclerView.setLayoutManager(mLayoutManager);
        if(mItems.size() < 1 || mItems_action.size() < 1 ){
            Snackbar snackbar = null;
            if(actionno == 3)
                snackbar = Snackbar
                        .make(mRecyclerView, getString(R.string.strnodata), Snackbar.LENGTH_INDEFINITE);
            else
                snackbar = Snackbar
                        .make(mRecyclerView, getString(R.string.strnodata), Snackbar.LENGTH_SHORT);

            snackbar.show();
        }

        actionButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (actionno == 1) {
                    TransferRequestFrag newFragment = new TransferRequestFrag();
                    FragmentTransaction transaction = getFragmentManager().beginTransaction();
                    transaction.replace(R.id.content_frame, newFragment);
                    transaction.addToBackStack(null);
                    transaction.commit();
                } else if (actionno == 2) {
                    StockIssueFrag newFragment = new StockIssueFrag();
                    FragmentTransaction transaction = getFragmentManager().beginTransaction();
                    transaction.replace(R.id.content_frame, newFragment);
                    transaction.addToBackStack(null);
                    transaction.commit();
                }
            }
        });

        mRecyclerView.addOnItemTouchListener(
                new RecylerItemClick(getContext(), new RecylerItemClick.OnItemClickListener() {
                    @Override
                    public void onItemClick(View view, int position) {
                        showStation(position);
                    }
                })
        );
        setHasOptionsMenu(true);

    }

    @Override
    public void onResume() {
        super.onResume();
        if(mAdapter != null)
            mAdapter.notifyDataSetChanged();
    }

    public void showStation(int position) {
        if(actionno != 2) {
            Transfer py = mItems.get(position);
            if (mItems.size() > 0) {
                String wh = py.getWarehousefrom();
                if (wh.equals("0"))
                    wh = getString(R.string.strdefaultwh);
                TransferDetailFrag newFragment = new TransferDetailFrag();
                Bundle args = new Bundle();
                args.putLong(Transfer.ID, py.getId());
                args.putString(Constants.SOURCE, action);
                args.putString(Transfer.WAREHOUSEFROM, wh);
                newFragment.setArguments(args);

                FragmentTransaction transaction = getFragmentManager().beginTransaction();
                transaction.replace(R.id.content_frame, newFragment);
                transaction.addToBackStack(action);
                transaction.commit();
            }
        } else {
            StockIssue st = mItems_action.get(position);
            String cu = st.getCustlocal();
            cu = Customers.getCustomerBusinessNameLoca(getContext(), cu);
            Log.e("CUSTOMER", "customer "+cu+" action "+action);
            TransferDetailFrag newFragment = new TransferDetailFrag();
            Bundle args = new Bundle();
            args.putLong(Transfer.ID, st.getId());
            args.putString(Constants.SOURCE, action);
            args.putString(Transfer.WAREHOUSEFROM, cu);
            args.putInt(Transfer.ACK, st.getAck());
            newFragment.setArguments(args);

            FragmentTransaction transaction = getFragmentManager().beginTransaction();
            transaction.replace(R.id.content_frame, newFragment);
            transaction.addToBackStack(action);
            transaction.commit();
        }
    }
}
