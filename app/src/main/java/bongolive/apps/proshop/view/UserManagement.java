/*
 * The source codes in this project are owned by Bongolive Enterprises. Copying or any kind of usage without Bongolive's approval is against the law.
 * Copyright (c) 2015.
 *
 */

package bongolive.apps.proshop.view;

import android.app.Dialog;
import android.app.SearchManager;
import android.content.ContentResolver;
import android.content.Context;
import android.database.Cursor;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.support.v7.widget.SearchView;
import android.util.Log;
import android.view.ContextMenu;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import bongolive.apps.proshop.R;
import bongolive.apps.proshop.controller.AppPreference;
import bongolive.apps.proshop.controller.Constants;
import bongolive.apps.proshop.controller.ItemList;
import bongolive.apps.proshop.controller.ItemListAdapter;
import bongolive.apps.proshop.controller.Validating;
import bongolive.apps.proshop.model.Settings;
import bongolive.apps.proshop.model.User;


public class UserManagement extends Fragment implements OnItemClickListener,OnClickListener
{
	private static final String ARG_SECTION_NUMBER = "section_number";
	AlertDialogManager alert ;
	View _rootView;
	List<ItemList> list;
    ListView lv ;
    Button sub ;
	Dialog dialog ;
	ItemListAdapter adapter ;
    EditText etuname,etpass,etpass2,etlastlogin ;
    CheckBox chkadmin,chknormal;
	ArrayList<String> deletearray;
    SearchManager searchManager;
    SearchView searchView;
    TextView txtbuz,txtcli,txtadd,txtcont,txtit4;
    boolean tabletsize;
    FloatingActionButton actionbar ;
    AppPreference appPreference;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
    {
		if(_rootView == null)
		{

            tabletsize = getResources().getBoolean(R.bool.isTablet);
			_rootView = inflater.inflate(R.layout.list_redesinged, container, false);
		} else {
//			((ViewGroup)_rootView.getParent()).removeView(_rootView) ;
        }
        return _rootView ;
    }

	@Override
    public void onActivityCreated(Bundle savedInstanceState)
    {
        super.onActivityCreated(savedInstanceState);
//        ((Dashboard)getActivity()).onSectionAttached(getArguments().getInt(ARG_SECTION_NUMBER));
        alert = new AlertDialogManager();

        appPreference = new AppPreference(getActivity());
        if(tabletsize) {

            txtbuz = (TextView) getActivity().findViewById(R.id.txtbusiness);
            txtcli = (TextView) getActivity().findViewById(R.id.txtclient);
            txtadd = (TextView) getActivity().findViewById(R.id.txtadddress);
            txtcont = (TextView) getActivity().findViewById(R.id.txtcontact);
            txtit4 = (TextView) getActivity().findViewById(R.id.txtunkown);
            txtbuz.setText(getString(R.string.strusername));
            txtcli.setText(getString(R.string.strrole));
            txtcont.setText(getString(R.string.strloginstat));
            txtadd.setVisibility(View.GONE);
            txtit4.setVisibility(View.GONE);
        }

        lv = (ListView)getActivity().findViewById(R.id.list);
		lv.setItemsCanFocus(false);

        adapter = new ItemListAdapter(getActivity(), getList(), 100);
        lv.setAdapter(adapter);
        lv.setOnItemClickListener(this);
		registerForContextMenu(lv);
        actionbar = (FloatingActionButton)getActivity().findViewById(R.id.addcustmer);
        if(appPreference.getUserRole() != 0)
            actionbar.setVisibility(View.VISIBLE);
        else
            actionbar.setVisibility(View.GONE);
        actionbar.setBackgroundColor(getResources().getColor(R.color.actionbar));
        actionbar.setImageResource(R.drawable.ic_action_add);

        actionbar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                adduser2();
            }
        });


		setHasOptionsMenu(true);
    }


	/* (non-Javadoc)
	 * @see android.widget.AdapterView.OnItemClickListener#onItemClick(android.widget.AdapterView, android.view.View, int, long)
	 */
	@Override
	public void onItemClick(AdapterView<?> parent, View view, final int position, final long id) {
				ItemList _list = list.get(position);
				String name = _list.getitemname();
				long i = _list.getId();
        if(list.size() > 1)
                showUser(i);
	}
	@Override
	public void onCreateContextMenu(ContextMenu menu, View v, ContextMenu.ContextMenuInfo menuInfo) {
        if(list.size() >1)
		if (v.getId() == R.id.list) {
			ListView lv = (ListView) v;
			AdapterView.AdapterContextMenuInfo acmi = (AdapterView.AdapterContextMenuInfo) menuInfo;
			ItemList obj = (ItemList) lv.getItemAtPosition(acmi.position);

            menu.setHeaderTitle(getString(R.string.actioncenter));
            // Add all the menu options
            menu.add(Menu.NONE, Constants.CONTEXTMENU_EDIT, 0, getString(R.string.stredit));
            menu.add(Menu.NONE, Constants.CONTEXTMENU_DELETE, 1, getString(R.string.strdelete));
        }
	}

    @Override
    public boolean onContextItemSelected(MenuItem item) {
        AdapterView.AdapterContextMenuInfo info = (AdapterView.AdapterContextMenuInfo) item.getMenuInfo();
        ItemList _list = list.get(info.position);
        String name = _list.getitemname();
        long i = _list.getId();
        switch (item.getItemId()) {
            case Constants.CONTEXTMENU_DELETE:

                    User.delete(getActivity(), String.valueOf(i));
                    adapter.notifyDataSetChanged();
                    adapter = new ItemListAdapter(getActivity(), getList(), 100);
                    lv.setAdapter(adapter);

                return true;
            case Constants.CONTEXTMENU_EDIT:

                    User.delete(getActivity(), String.valueOf(i));
                    showUser(i);

                return true;
            default:
                return super.onContextItemSelected(item);
        }
    }

    private void showUser(final long i) {
        LayoutInflater infl = getActivity().getLayoutInflater();
        View view = infl.inflate(R.layout.adduser, null);

        final Dialog dialog = new Dialog(getActivity(), R.style.CustomDialog);
        dialog.setCancelable(true);
        dialog.setContentView(view);

        sub = (Button) dialog.findViewById(R.id.btnSubmit);
        sub.setVisibility(View.GONE);

        TextView txttitle = (TextView) dialog.findViewById(R.id.txttitle);
        String title = getResources().getString(R.string.stradduser);
        title.toUpperCase(Locale.getDefault());
        txttitle.setText(title);

        actionbar = (FloatingActionButton)dialog.findViewById(R.id.adduser);
        if(appPreference.getUserRole() != 0)
            actionbar.setVisibility(View.VISIBLE);
        else
            actionbar.setVisibility(View.GONE);

        actionbar.setBackgroundColor(getResources().getColor(R.color.actionbar));
        actionbar.setImageResource(R.drawable.ic_action_send);


        ImageView goback = (ImageView) dialog.findViewById(R.id.goback);
        goback.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });

        etuname = (EditText) dialog.findViewById(R.id.etusername);
        etpass = (EditText) dialog.findViewById(R.id.etpass);
        etpass2 = (EditText) dialog.findViewById(R.id.etpass2);
        chkadmin = (CheckBox)dialog.findViewById(R.id.chkadmin);
        chknormal = (CheckBox)dialog.findViewById(R.id.chknonadmin);
        etlastlogin = (EditText)dialog.findViewById(R.id.etlastlogin);

        final String[] info = User.getUser(dialog.getContext(),i);
        if(info.length > 0){
            etuname.setText(info[0]);
            etuname.setFocusable(false);
            etuname.setFocusableInTouchMode(false);
            if(info[2].equals("0"))
                chknormal.setChecked(true);
            if(info[2].equals("1"))
                chkadmin.setChecked(true);
            chkadmin.setEnabled(false);
            chknormal.setEnabled(false);
            etlastlogin.setVisibility(View.VISIBLE);
            etlastlogin.setText(info[1]);
            etlastlogin.setFocusable(false);
            etlastlogin.setFocusableInTouchMode(false);

            actionbar.setOnClickListener(new OnClickListener() {
                @Override
                public void onClick(View v) {
                    updateuser(i,info,dialog);
                }
            });
            etpass2.setOnKeyListener(new View.OnKeyListener() {
                @Override
                public boolean onKey(View v, int keyCode, KeyEvent event) {
                    if (event.getAction() == KeyEvent.ACTION_DOWN && event.getKeyCode() == KeyEvent.KEYCODE_ENTER) {
                        Log.i("event", "captured");
                        InputMethodManager imm = (InputMethodManager) dialog.getContext(). getSystemService
                                (Context
                                        .INPUT_METHOD_SERVICE);
                        imm.hideSoftInputFromWindow(dialog.getWindow().getCurrentFocus().getWindowToken(), 0);
                        updateuser(i, info,dialog);

                        return false;
                    }
                    return false;
                }
            });

        }


        dialog.show();

    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {

        inflater.inflate(R.menu.addcustomer, menu);
        MenuItem item = menu.findItem(R.id.action_addcustomer);
        item.setTitle(getString(R.string.stradduser));

        super.onCreateOptionsMenu(menu, inflater);
    }

	public List<ItemList> getList(){
		ContentResolver cr = getActivity().getContentResolver() ;
        String where = null;
        if(appPreference.getUserRole() == 0)
            where = User.ID +" = "+appPreference.getUserId();
        else
            where =  User.STATUS + " = 1 AND "+User.ROLE +" != 3";
        Cursor c = cr.query(User.BASEURI, null, where, null, User.ID + " DESC");
        list = new ArrayList<ItemList>();
        try{
        if(c.getCount() > 0)
        {
        	c.moveToFirst() ;
        	do{
                int colN = c.getColumnIndex(User.USERNAME);
                int colP = c.getColumnIndex(User.UPDATED);
                int colI = c.getColumnIndex(User.ID);
                int colR = c.getColumnIndex(User.ROLE);
                String n = c.getString(colN);
                String p = c.getString(colP);
                String r = c.getString(colR);
                long i = c.getLong(colI) ;
                if(!Validating.areSet(p))
                    p = getString(R.string.strnever);

                if(r.equals(0))
                    r = getString(R.string.strsales);

                if(r.equals(1))
                    r = getString(R.string.stradmin);

                if(r.equals("3"))
                    r = getString(R.string.stradmin);

                if(tabletsize){
                    list.add(new ItemList(n, r, p, i,"null"));
                } else {
                    list.add(new ItemList(getString(R.string.strusername).toUpperCase(Locale.getDefault()) + ": " +
                            n.toUpperCase(Locale.getDefault()),   "null",
                            getString(R.string.strloginstat).toUpperCase(Locale.getDefault()) + ": " + p, i,
                            getString(R.string.strrole).toUpperCase(Locale.getDefault()) + ": " +  r.toUpperCase
                                    (Locale.getDefault())));
                }
        	} while (c.moveToNext()) ;
        } else {
        	String n = getString(R.string.strnodata).toUpperCase(Locale.getDefault()) ;
        	String p = " " ;
        	long i = 0 ;
        	String d = null ;
        	list.add(new ItemList(n,"", p, i,d));
        	lv.setAdapter(adapter);
        }
        }finally{
			if(c != null) {
	            c.close();
	        }
		}
        return list ;
	}
	@Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_addcustomer:
                if(!Settings.getAddCustomerEnabled(getActivity()))
                {
                    Toast.makeText(getActivity(),getString(R.string.strnopermissionwarn),Toast.LENGTH_LONG).show();
                } else {
                   adduser2();
                }
                break;
            case R.id.action_search:
                getActivity().onSearchRequested();
                break;
        }

		return super.onOptionsItemSelected(item);
	}

	/* (non-Javadoc)
	 * @see android.view.View.OnClickListener#onClick(android.view.View)
	 */
	@Override
	public void onClick(View v) {
        switch (v.getId()) {
            case R.id.adduser:
               adduser();
                break;
        }
    }

/* todo
 * add search functionality
 */

    private void adduser2(){
        LayoutInflater infl = getActivity().getLayoutInflater();
        View view = infl.inflate(R.layout.adduser, null);
        dialog = new Dialog(getActivity(), R.style.CustomDialog);
        dialog.setCancelable(false);
        dialog.setContentView(view);
        dialog.show();
        sub = (Button) dialog.findViewById(R.id.btnSubmit);
        sub.setVisibility(View.GONE);

        TextView txttitle = (TextView) dialog.findViewById(R.id.txttitle);
        String title = getResources().getString(R.string.stradduser);
        title.toUpperCase(Locale.getDefault());
        txttitle.setText(title);


        ImageView goback = (ImageView) dialog.findViewById(R.id.goback);
        goback.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });

        etuname = (EditText) dialog.findViewById(R.id.etusername);
        etpass = (EditText) dialog.findViewById(R.id.etpass);
        etpass2 = (EditText) dialog.findViewById(R.id.etpass2);
        chkadmin = (CheckBox)dialog.findViewById(R.id.chkadmin);
        chknormal = (CheckBox)dialog.findViewById(R.id.chknonadmin);


        actionbar = (FloatingActionButton)dialog.findViewById(R.id.adduser);
        actionbar.setVisibility(View.VISIBLE);
        actionbar.setBackgroundColor(getResources().getColor(R.color.actionbar));
        actionbar.setImageResource(R.drawable.ic_action_send);

        int usercount = User.getCount(getActivity());
        System.out.println("user "+usercount);
        if(usercount == 1){
            Toast.makeText(getActivity(),getString(R.string.struserwarn),Toast.LENGTH_LONG).show();
            chknormal.setChecked(false);
            chknormal.setEnabled(false);
            chkadmin.setEnabled(true);
            chkadmin.setChecked(true);
        } else if (usercount > 1){
            chkadmin.setEnabled(false);
            chknormal.setChecked(true);
        }

        if(!User.getAdminCount(getActivity())){
            Toast.makeText(getActivity(),getString(R.string.struserwarn),Toast.LENGTH_LONG).show();
            chknormal.setEnabled(false);
            chkadmin.setChecked(true);
        }

        chkadmin.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if(isChecked){
                    chknormal.setChecked(false);
                }
            }
        });

        chknormal.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if(isChecked){
                    chkadmin.setChecked(false);
                }
            }
        });
        etpass2.setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                if (event.getAction() == KeyEvent.ACTION_DOWN && event.getKeyCode() == KeyEvent.KEYCODE_ENTER) {
                    Log.i("event", "captured");
                    InputMethodManager imm = (InputMethodManager) dialog.getContext(). getSystemService
                            (Context
                                    .INPUT_METHOD_SERVICE);
                    imm.hideSoftInputFromWindow(dialog.getWindow().getCurrentFocus().getWindowToken(), 0);
                    adduser();

                    return false;
                }
                return false;
            }
        });

        actionbar.setOnClickListener(this);
    }

    private void adduser(){
        String strun, stretpass, stretpass2, strrole = "";
        strun = Validating.stripit(etuname.getText().toString());
        stretpass = Validating.stripit(etpass.getText().toString());
        stretpass2 = Validating.stripit(etpass2.getText().toString());

        if(chkadmin.isChecked())
            strrole = "1";
        if(chknormal.isChecked())
            strrole = "0";


        if (!strun.isEmpty())
            if (!Constants.checkInvalidChars(getActivity(), strun, etuname))
                return;

        String[] inputcheck = new String[]{strun, stretpass, stretpass2,strrole};

        if (Validating.areSet(inputcheck)) {
            if(!stretpass.equals(stretpass2)){
                etpass2.setText("");
                etpass.setText("");
                etpass.setHint(getString(R.string.strpassmismatch));
                etpass.requestFocus();
                return;
            } else if(stretpass.length() < 4){
                etpass2.setText("");
                etpass.setText("");
                etpass.setHint(getString(R.string.strpasslengthwarn));
                etpass.requestFocus();
                return;
            }

            try{
                stretpass = Constants.makeSHA1Hash(stretpass);
            } catch (NoSuchAlgorithmException ex){
                ex.printStackTrace();
            }
            String[] input = new String[]{strun, stretpass,strrole};
            boolean insert = User.insert(getActivity(), input);
            adapter.notifyDataSetChanged();
            if (insert) {
                adapter = new ItemListAdapter(getActivity(), getList(), 100);
                adapter.notifyDataSetChanged();
                lv.setAdapter(adapter);
                Toast.makeText(getActivity(), getResources().getString(R.string.struseraddsuccess), Toast
                        .LENGTH_SHORT)
                        .show();
                dialog.dismiss();
            } else {
                Toast.makeText(getActivity(), getResources().getString(R.string.struseraddfailure), Toast
                        .LENGTH_SHORT).show();
                dialog.dismiss();
            }
        }
    }

    private void updateuser(long i, String[] info, Dialog dialog){
        String  stretpass = etpass.getText().toString().trim();
        String stretpass2 = etpass2.getText().toString().trim();

        String[] inputcheck = new String[]{stretpass, stretpass2};

        if (Validating.areSet(inputcheck)) {
            if(!stretpass.equals(stretpass2)){
                etpass2.setText("");
                etpass.setText("");
                etpass.setHint(getString(R.string.strpassmismatch));
                etpass.requestFocus();
                return;
            } else if(stretpass.length() < 4){
                etpass2.setText("");
                etpass.setText("");
                etpass.setHint(getString(R.string.strpasslengthwarn));
                etpass.requestFocus();
                return;
            }

            try{
                stretpass = Constants.makeSHA1Hash(stretpass);
            } catch (NoSuchAlgorithmException ex){
                ex.printStackTrace();
            }
            String[] input = new String[]{info[0], stretpass,info[2],String.valueOf(i)};
            boolean update = User.update(getActivity(), input);
            adapter.notifyDataSetChanged();
            if (update) {
                adapter = new ItemListAdapter(getActivity(), getList(), 100);
                adapter.notifyDataSetChanged();
                lv.setAdapter(adapter);
                Toast.makeText(getActivity(), getResources().getString(R.string.struserupdatesuccess), Toast
                        .LENGTH_SHORT)
                        .show();
                dialog.dismiss();
            } else {
                Toast.makeText(getActivity(), getResources().getString(R.string.struseraddfailure), Toast
                        .LENGTH_SHORT).show();
                dialog.dismiss();
            }
        }
    }

}
