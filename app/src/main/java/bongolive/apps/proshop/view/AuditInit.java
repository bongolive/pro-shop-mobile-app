/*
 * The source codes in this project are owned by Bongolive Enterprises. Copying or any kind of usage without Bongolive's approval is against the law.
 * Copyright (c) 2015.
 *
 */


package bongolive.apps.proshop.view;


import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;

import java.util.ArrayList;

import bongolive.apps.proshop.R;
import bongolive.apps.proshop.controller.AuditAdapter;
import bongolive.apps.proshop.model.Audit;

public class AuditInit extends AppCompatActivity {

    private boolean tabletSize = false;
    private ActionBar ab;
    RecyclerView mRecyclerView;
    LinearLayoutManager mLayoutManager;

    ArrayList<Audit> list = new ArrayList<>();
    private RecyclerView.Adapter mAdapter;
    TextView txtcust,txtdebt,txtpaid,txtdate;
    /*
    * end of new strings
    */

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        tabletSize = getResources().getBoolean(R.bool.isTablet);
        if (!tabletSize) {
            setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        }
        setContentView(R.layout.audit_init);

        ab = getSupportActionBar();
        ab.setDisplayUseLogoEnabled(true);
        ab.setDisplayHomeAsUpEnabled(true);

        if(tabletSize){
            txtcust = (TextView)findViewById(R.id.txtcustomer);
            txtcust.setTextSize(15);
            txtdate = (TextView)findViewById(R.id.txtsaledate);
            txtdate.setVisibility(View.GONE);
            txtpaid = (TextView)findViewById(R.id.txtpaid);
            txtdebt = (TextView)findViewById(R.id.txtunpaid);

            String tt = getString(R.string.straudittitle);
            String due = getString(R.string.strcreated);
            String ds = getString(R.string.strprodesc);

            txtdebt.setText(ds);
            txtcust.setText(tt.toUpperCase());
            txtpaid.setText(due);
        }

        mRecyclerView = (RecyclerView) findViewById(R.id.prodct_list);
        mRecyclerView.setHasFixedSize(true);
        mRecyclerView.setItemAnimator(new DefaultItemAnimator());

        mLayoutManager = new LinearLayoutManager(this);
        mRecyclerView.setLayoutManager(mLayoutManager);
        list = Audit.getAudits(this);
        if(list.size() < 1){
            Snackbar snackbar = Snackbar
                    .make(mRecyclerView, getString(R.string.strnodata), Snackbar.LENGTH_INDEFINITE);

            snackbar.show();
        }
        mAdapter = new AuditAdapter(this, list,tabletSize);
        mRecyclerView.setAdapter(mAdapter);
        mRecyclerView.addOnItemTouchListener(
                new RecylerItemClick(this, new RecylerItemClick.OnItemClickListener() {
                    @Override
                    public void onItemClick(View view, int position) {
                        Audit py = list.get(position);
                        long id = py.getId();
                        long audid = py.getAuid();
                        Intent orderintent = new Intent(AuditInit.this, Auditing.class);
                        orderintent.putExtra(Audit.AUDITID, audid);
                        startActivity(orderintent);
                    }
                })
        );

    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }
}


