/*
 * The source codes in this project are owned by Bongolive Enterprises. Copying or any kind of usage without Bongolive's approval is against the law.
 * Copyright (c) 2015.
 *
 */

/**
 * 
 */
package bongolive.apps.proshop.view;

import android.content.Context;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.content.res.Configuration;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import java.util.Locale;
import java.util.Timer;

import bongolive.apps.proshop.R;
import bongolive.apps.proshop.controller.AppPreference;
import bongolive.apps.proshop.controller.Validating;
import bongolive.apps.proshop.model.Settings;
import bongolive.apps.proshop.model.User;
import bongolive.apps.proshop.controller.Constants;

/**
 *    @author nasznjoka
 *
 *    
 *    Nov 29, 2014
 */
public  class StartScreen extends AppCompatActivity implements OnClickListener,View.OnKeyListener {

	private static final String TAG = StartScreen.class.getName();
	Button login, cancel;
    EditText etuser,etpass;
	TextView txtv;
	AppPreference appPreference;
    AlertDialogManager ma;
    boolean tabletSize = false;
	Timer myTimer;
		@Override
	    public void onCreate(Bundle savedInstanceState)
	    { 
	        super.onCreate(savedInstanceState);

			appPreference = new AppPreference(getApplicationContext());
		    String languageToLoad  = appPreference.getDefaultLanguage();
            if(Settings.getFormEnabled(this))
                appPreference.save_pin_options(true);
		    Locale locale = new Locale(languageToLoad);
		    Locale.setDefault(locale);
		    Configuration config = new Configuration();
		    config.locale = locale;
		    getBaseContext().getResources().updateConfiguration(config,
                    getBaseContext().getResources().getDisplayMetrics());
            tabletSize = getResources().getBoolean(R.bool.isTablet);
            if(!tabletSize){
                setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
            }
            appPreference.reset_auth();
            appPreference.set_bluetooth(true);

            if(User.getCount(this)  < 2 || !appPreference.getPinOptions() || !Settings.getUserEnabled(this) ||
                    Settings.getProshopLite(this)){
                Intent intent = new Intent(this, MainScreen.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(intent);
                finish();
            }

		    setContentView(R.layout.login);

            ma = new AlertDialogManager();

	        login = (Button)findViewById(R.id.btnsubmit);
			cancel = (Button)findViewById(R.id.btncancel);
			login.setOnClickListener(this);
            login.setOnKeyListener(this);
			cancel.setOnClickListener(this);

			etuser = (EditText)findViewById(R.id.etusername);
			etpass = (EditText)findViewById(R.id.etpass);
			txtv = (TextView)findViewById(R.id.versioncode);

            String version = Constants.getVersionName(this);
            txtv.setText(getString(R.string.app_name) + " " +version);

            /*ContentResolver cr = getContentResolver();
            String where = User.ID +" > 0";
            cr.delete(User.BASEURI,where,null);*/

		    ActionBar ab = getSupportActionBar();
            ab.setBackgroundDrawable(getResources().getDrawable(R.drawable.ab_background_textured_prostockactionbar));
		    ab.setDisplayShowTitleEnabled(true);
		    ab.setDisplayShowHomeEnabled(true);

            etpass.setOnKeyListener(new View.OnKeyListener() {
                @Override
                public boolean onKey(View v, int keyCode, KeyEvent event) {
                    if (event.getAction() == KeyEvent.ACTION_DOWN && event.getKeyCode() == KeyEvent.KEYCODE_ENTER) {
                        InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                        imm.hideSoftInputFromWindow(getCurrentFocus().getWindowToken(), 0);
                        process_login();

                        return false;
                    }
                    return false;
                }
            });
	    }


    @Override
    public boolean onKey(View v, int keyCode, KeyEvent event) {
        switch (v.getId()) {
            case R.id.autoquantity00:
                if (event.getAction() == KeyEvent.ACTION_DOWN && event.getKeyCode() == KeyEvent.KEYCODE_ENTER) {
                    Log.i("event", "captured");
                    InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                    imm.hideSoftInputFromWindow(getCurrentFocus().getWindowToken(), 0);
                    process_login();
                    return false;
                }
                break;
        }
        return false;
    }
		/* (non-Javadoc)
		 * @see android.view.View.OnClickListener#onClick(android.view.View)
		 */
		@Override
		public void onClick(View view) {
			switch(view.getId())
			{
			case R.id.btnsubmit: 
				process_login();
				break;
			case R.id.btncancel:  
	            finish();  
			}
		}



    private void process_login() {
        String stuser, stpass;
        stuser = etuser.getText().toString();
        stuser = stuser.trim();
        stpass = etpass.getText().toString();
        stpass = stpass.trim();

        try {
            stpass = Constants.makeSHA1Hash(stpass) ;
        } catch (Exception e) {
            e.printStackTrace();
        }
        String[] vals = {stuser,stpass} ;
        if(Validating.areSet(vals))
        {
            int create =  User.login(this, vals);
            System.out.println(" create id "+create);
            if(create >= 0)
            {
                int role = User.getUserlRole(this, create);
                appPreference.saveUserRole(role);
                appPreference.saveUserId(create);

                Intent intent = new Intent(this, MainScreen.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(intent);
                finish();
            } else {
                ma.showAlertDialog(this,getString(R.string.strlogin) ,
                        getString(R.string.strloginmsg), false);
                etpass.setText("");
            }
        } else {
            etuser.setHint(getString(R.string.strusername));
            etuser.setHintTextColor(Color.RED);
            etpass.setHint(getString(R.string.strpass));
            etpass.setHintTextColor(Color.RED);
            return ;
        }
    }


}
