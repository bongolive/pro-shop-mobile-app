/*
 * The source codes in this project are owned by Bongolive Enterprises. Copying or any kind of usage without Bongolive's approval is against the law.
 * Copyright (c) 2015.
 *
 */


package bongolive.apps.proshop.view;


import android.app.Dialog;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AutoCompleteTextView;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import java.util.ArrayList;

import bongolive.apps.proshop.R;
import bongolive.apps.proshop.controller.Constants;
import bongolive.apps.proshop.controller.TranferDetailsAdapter;
import bongolive.apps.proshop.controller.TransferItemsAdapter;
import bongolive.apps.proshop.controller.TransferingAdapter;
import bongolive.apps.proshop.controller.Validating;
import bongolive.apps.proshop.model.Products;
import bongolive.apps.proshop.model.StockIssueItems;
import bongolive.apps.proshop.model.Transfer;
import bongolive.apps.proshop.model.TransferItems;

public class TransferDetail extends AppCompatActivity{

    private boolean tabletSize = false;
    private RecyclerView mRecyclerView;
    private RecyclerView.Adapter mAdapter;
    private RecyclerView.LayoutManager mLayoutManager;
    private ActionBar actionBar;
    private EditText etfilter;
    private ArrayList arrayList;
    private RelativeLayout rl;
    private ImageView imgdone,imgnext;
    private AutoCompleteTextView bzn;
    private ActionBar ab;
    ArrayList<Transfer> mItems;
    ArrayList<TransferItems> mItemsDetail = new ArrayList<>();
    ArrayList<StockIssueItems> mItemsDetail_action = new ArrayList<>();
    FloatingActionButton actionButton;
    long pyid = 0;
    int ack = 0;
    String title;
    String action ;
    double grandbalance,grandpaid;
    /*
    * end of new strings
    */
    MenuItem menuItem;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        tabletSize = getResources().getBoolean(R.bool.isTablet);
        if (!tabletSize) {
            setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        }
        setContentView(R.layout.selling_screen);

        ab = getSupportActionBar();
        ab.setDisplayUseLogoEnabled(true);
        ab.setDisplayHomeAsUpEnabled(true);

        actionButton = (FloatingActionButton)findViewById(R.id.actionbutton);
        actionButton.setBackgroundColor(getResources().getColor(R.color.actionbar));
        actionButton.setImageResource(R.drawable.ic_action_send);
        actionButton.setVisibility(View.GONE);

        mRecyclerView = (RecyclerView) findViewById(R.id.prodct_list);
        mRecyclerView.setHasFixedSize(true);
        mRecyclerView.setItemAnimator(new DefaultItemAnimator());

        mLayoutManager = new LinearLayoutManager(this);
        mRecyclerView.setLayoutManager(mLayoutManager);

        final Intent intent = getIntent();
        pyid = intent.getExtras().getLong(Transfer.ID);

        action = intent.getAction();
        switch (action){
            case Constants.TRANSTYPE_REQUEST:
                mItemsDetail = TransferItems.getItems(this, pyid);
                ack = Transfer.getRecStatus(this, pyid);
                mAdapter = new TransferItemsAdapter(this, mItemsDetail);
                mRecyclerView.setAdapter(mAdapter);
                break;
            case Constants.TRANSTYPE_ISSUE:
                mItemsDetail_action = StockIssueItems.getItems(this, pyid);
                mAdapter = new TransferItemsAdapter(this, mItemsDetail_action, action);
                mRecyclerView.setAdapter(mAdapter);
                break;
            case Constants.TRANSTYPE_TRANSFER:
                mItemsDetail = TransferItems.getItems(this, pyid);
                mAdapter = new TransferItemsAdapter(this, mItemsDetail);
                mRecyclerView.setAdapter(mAdapter);
                ack = Transfer.getRecStatus(this, pyid);
                System.out.println(" approval "+ack);
                if(ack == 1)
                    actionButton.setVisibility(View.VISIBLE);
                break;
        }


        title = intent.getExtras().getString(Transfer.WAREHOUSEFROM);

        if(pyid == 0 )
            finish();

        if(Validating.areSet(title))
            ab.setTitle(title.toUpperCase());
        else
            ab.setTitle(getString(R.string.strtransfer));


        mRecyclerView.addOnItemTouchListener(
                new RecylerItemClick(this, new RecylerItemClick.OnItemClickListener() {
                    @Override
                    public void onItemClick(View view, int position) {
                        /*TransferItems tr = mItemsDetail.get(position);
                        transfer_details(TransferItems.getItem(TransferDetail.this, tr.getId()));*/
                    }
                })
        );

        actionButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.e("acion",action);
                if(action.equals(Constants.TRANSTYPE_TRANSFER))
                    process_ack(pyid);
            }
        });
    }

    @Override
    protected void onResume() {
        super.onResume();
        if(pyid != 0 && mRecyclerView != null && mAdapter != null){
            mAdapter.notifyDataSetChanged();
            if(action.equals(Constants.TRANSTYPE_TRANSFER)) {
                ack = Transfer.getRecStatus(this, pyid);
                if (ack == 1)
                    actionButton.setVisibility(View.VISIBLE);
                else
                    actionButton.setVisibility(View.GONE);
            }
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.audit, menu);
        MenuItem menuItem = menu.findItem(R.id.action_tick);
        menuItem.setVisible(false);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }


    private void process_ack(long id) {
        LayoutInflater infl = getLayoutInflater();
        View view = infl.inflate(R.layout.delivery, null);

        final Dialog dg = new Dialog(this, R.style.CustomDialog);
        dg.setCancelable(true);
        dg.setContentView(view);
        TextView txtt = (TextView)dg.findViewById(R.id.txttitle);
        txtt.setText(getString(R.string.strconfirm));

        actionButton = (FloatingActionButton) dg.findViewById(R.id.actionbutton);
        ArrayList<TransferItems> listarray = TransferItems.getItemsApproved(dg.getContext(),id);

        actionButton.setBackgroundColor(getResources().getColor(R.color.actionbar));
        actionButton.setImageResource(R.drawable.ic_action_send);
        ListView listView = (ListView) dg.findViewById(R.id.list);
        TransferingAdapter _adapter = new TransferingAdapter(dg.getContext(), listarray, actionButton, new
                Dialog[]{dg}, id);
        listView.setAdapter(_adapter);

        ImageView imageView = (ImageView) dg.findViewById(R.id.goback);
        imageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dg.dismiss();
            }
        });
        dg.show();
    }


    private void transfer_details(ArrayList<TransferItems> items) {
        LayoutInflater infl = getLayoutInflater();
        View view = infl.inflate(R.layout.transferitem, null);

        final Dialog dg = new Dialog(this, R.style.CustomDialog);
        dg.setCancelable(true);
        dg.setContentView(view);
        TextView txtt = (TextView)dg.findViewById(R.id.txttitle);
        TransferItems tr = items.get(0);
        String prod = tr.getProduct();
        prod = Products.getProdNameBasedSyid(dg.getContext(), prod);
        txtt.setText(prod.toUpperCase());

        actionButton = (FloatingActionButton) dg.findViewById(R.id.actionbutton);

        actionButton.setBackgroundColor(getResources().getColor(R.color.actionbar));
        actionButton.setImageResource(R.drawable.ic_action_send);
        actionButton.setVisibility(View.GONE);

        mRecyclerView = (RecyclerView)dg.findViewById(R.id.prodct_list);
        mRecyclerView.setHasFixedSize(true);
        mRecyclerView.setItemAnimator(new DefaultItemAnimator());

        mLayoutManager = new LinearLayoutManager(this);
        mRecyclerView.setLayoutManager(mLayoutManager);

        TranferDetailsAdapter _adapter = new TranferDetailsAdapter(this, items);
        mRecyclerView.setAdapter(_adapter);

        ImageView imageView = (ImageView) dg.findViewById(R.id.goback);
        imageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dg.dismiss();
            }
        });
        dg.show();
    }
}





