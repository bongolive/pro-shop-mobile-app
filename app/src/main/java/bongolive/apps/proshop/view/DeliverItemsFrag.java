/*
 * The source codes in this project are owned by Bongolive Enterprises. Copying or any kind of usage without Bongolive's approval is against the law.
 * Copyright (c) 2015.
 *
 */

package bongolive.apps.proshop.view;

import android.app.Dialog;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;

import java.util.ArrayList;

import bongolive.apps.proshop.R;
import bongolive.apps.proshop.controller.TransferingAdapter;
import bongolive.apps.proshop.model.Transfer;
import bongolive.apps.proshop.model.TransferItems;


public class DeliverItemsFrag extends Fragment
{
    View _rootView;
    FloatingActionButton actionButton;
    long pyid = 0;

    /*
    * end of new strings
    */
    MenuItem menuItem;


    /*
    * end of new strings
    */

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
    {
        _rootView = inflater.inflate(R.layout.delivery, container, false);
        return _rootView ;
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState)
    {
        super.onActivityCreated(savedInstanceState);

        actionButton = (FloatingActionButton)getActivity().findViewById(R.id.actionbutton);
        actionButton.setBackgroundColor(getResources().getColor(R.color.actionbar));
        actionButton.setImageResource(R.drawable.ic_action_send);

        pyid = getArguments().getLong(Transfer.ID);
        ArrayList<TransferItems> listarray = TransferItems.getItemsApproved(getContext(),pyid);

        Dialog dg = new Dialog(getContext());
        ListView listView = (ListView) getActivity().findViewById(R.id.list);
        TransferingAdapter _adapter = new TransferingAdapter(getContext(), listarray, actionButton, new
                Dialog[]{dg}, pyid);
        listView.setAdapter(_adapter);
    }

    @Override
    public void onResume() {
        super.onResume();

    }

}
