/*
 * The source codes in this project are owned by Bongolive Enterprises. Copying or any kind of usage without Bongolive's approval is against the law.
 * Copyright (c) 2015.
 *
 */


package bongolive.apps.proshop.view;


import android.Manifest;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.ContextWrapper;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentSender;
import android.content.pm.ActivityInfo;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Path;
import android.graphics.RectF;
import android.location.Location;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.AttributeSet;
import android.util.Log;
import android.view.ContextMenu;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.AbsListView;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.SpinnerAdapter;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.PendingResult;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.LocationSettingsRequest;
import com.google.android.gms.location.LocationSettingsResult;
import com.google.android.gms.location.LocationSettingsStates;
import com.google.android.gms.location.LocationSettingsStatusCodes;

import java.io.File;
import java.io.FileOutputStream;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.ArrayList;
import java.util.Locale;

import bongolive.apps.proshop.R;
import bongolive.apps.proshop.controller.AppPreference;
import bongolive.apps.proshop.controller.Constants;
import bongolive.apps.proshop.controller.PermissionUtils;
import bongolive.apps.proshop.controller.SalesItemAdapter;
import bongolive.apps.proshop.controller.SalesSummaryAdapter;
import bongolive.apps.proshop.controller.Validating;
import bongolive.apps.proshop.model.Customers;
import bongolive.apps.proshop.model.MultimediaContents;
import bongolive.apps.proshop.model.Products;
import bongolive.apps.proshop.model.Sales;
import bongolive.apps.proshop.model.SalesItems;
import bongolive.apps.proshop.model.Settings;
import bongolive.apps.proshop.model.StockIssue;
import bongolive.apps.proshop.model.StockIssueItems;
import bongolive.apps.proshop.model.Warehouse;

public class StockIssueActivity extends AppCompatActivity implements View.OnCreateContextMenuListener,View.OnKeyListener,
        TextWatcher,
        GoogleApiClient.ConnectionCallbacks, GoogleApiClient.OnConnectionFailedListener
        , LocationListener {

    private String customeridLocal = "";
    private String customerid = "";/*customerid captures the customer id*/

    private TextView txreportdata;
    private ListView lvitems;
    private AutoCompleteTextView edclientname;
    private AutoCompleteTextView etproduct;
    private EditText etquantity;
    private EditText etprice;
    private EditText etpartial;
    private EditText etdiscount;
    private Dialog dialogmode;
    private AppPreference appPreference;
    private boolean tabletSize = false;

    private ArrayList<String> productlist;
    private ArrayList<String> productcodelist;
    private TextView txtorderitem;
    private int  payinfull = 0;
    private boolean discountable = false;

    private String whname = "";
    private String whid = "";
    private ArrayList<String> whlistid;
    private ArrayList<SalesItems> salesItems = new ArrayList<>();
    private SalesItemAdapter adapter ;
    private LinearLayout layprod;
    private LinearLayout layqty;
    private LinearLayout layrep;
    private LinearLayout laycust;
    private ImageButton imgaddcust ;
    private View topb;
    private View lowb;
    private LinearLayout rdiscount;
    private ActionBar ab;
    private boolean business = false;
    /*
    * new strings
    */
    private String __prodcode = "";
    private String __prodname = "";
    private boolean __taxsettings = false;

    LinearLayout mContent;
    signature mSignature;
    Button mClear, mGetSign, mCancel;
    CheckBox chkconsent;
    public static String tempDir;
    public int count = 1;
    public String current = null;
    private Bitmap mBitmap;
    View mView;
    File mypath;
    private String uniqueId;
    String curr;
    String TAG = StockIssueActivity.class.getName();
    /*
    * end of new strings
    */


    /* location */
    private static final int LOCATION_PERMISSION_REQUEST = 1;
    private boolean mPermissionDenied = false;
    private GoogleApiClient mGoogleApiClient;

    private LocationRequest mLocationRequest;
    private Location currentlocation;
    private boolean LOCATION_ALLOWED;
    private static final int REQUEST_CHECK_SETTINGS = 11;


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        tabletSize = getResources().getBoolean(R.bool.isTablet);
        if (!tabletSize) {
            setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        }
        setContentView(R.layout.addsales);

        appPreference = new AppPreference(this.getApplicationContext());
        appPreference.setShowPrice(false);
        __taxsettings = Settings.getTaxEnabled(this);

        createSalesForm();

        ab = getSupportActionBar();
        ab.setTitle(getString(R.string.strissuestock));
        ab.setDisplayUseLogoEnabled(true);
        ab.setDisplayHomeAsUpEnabled(true);
        ab.setBackgroundDrawable(getResources().getDrawable(R.drawable.ab_background_textured_prostockactionbar));
        curr = appPreference.getDefaultCurrency();
        curr = curr != null ? curr : "Tsh";
        buildGoogleApiClient();
    }

    protected synchronized void buildGoogleApiClient() {
        Log.d(TAG, "startTracking");

        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) !=
                PackageManager.PERMISSION_GRANTED
                && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) !=
                PackageManager.PERMISSION_GRANTED) {
            LOCATION_ALLOWED = false;
            PermissionUtils.requestPermission(this, LOCATION_PERMISSION_REQUEST,
                    Manifest.permission.ACCESS_FINE_LOCATION, true);
        }
        if (mGoogleApiClient == null)
            mGoogleApiClient = new GoogleApiClient.Builder(this)
                    .addConnectionCallbacks(this)
                    .addOnConnectionFailedListener(this)
                    .addApi(LocationServices.API)
                    .build();

        if (!mGoogleApiClient.isConnected() || !mGoogleApiClient.isConnecting()) {
            mGoogleApiClient.connect();
        }
        mLocationRequest = LocationRequest.create();
        mLocationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);

        if(mGoogleApiClient != null)
            setup_location();
    }

    private void setup_location(){
        LocationSettingsRequest.Builder builder = new LocationSettingsRequest.Builder()
                .addLocationRequest(mLocationRequest);
        builder.setAlwaysShow(true); //this is the key ingredient

        PendingResult<LocationSettingsResult> result =
                LocationServices.SettingsApi.checkLocationSettings(mGoogleApiClient, builder.build());
        result.setResultCallback(new ResultCallback<LocationSettingsResult>() {
            @Override
            public void onResult(LocationSettingsResult result) {
                final Status status = result.getStatus();
                final LocationSettingsStates state = result.getLocationSettingsStates();

                switch (status.getStatusCode()) {
                    case LocationSettingsStatusCodes.SUCCESS:
                        //nothing proceed
                        LOCATION_ALLOWED = true;
                        break;
                    case LocationSettingsStatusCodes.RESOLUTION_REQUIRED:
                        try {
                            status.startResolutionForResult(StockIssueActivity.this, REQUEST_CHECK_SETTINGS);
                        } catch (IntentSender.SendIntentException e) {
                            e.printStackTrace();
                        }
                        break;
                    case LocationSettingsStatusCodes.SETTINGS_CHANGE_UNAVAILABLE:
                        LOCATION_ALLOWED = false;
                        break;
                }
            }
        });

    }

    private void showToast(String txt) {
        Toast.makeText(this, txt, Toast.LENGTH_LONG).show();
    }

    @Override
    public void onConnected(Bundle bundle) {
        if (ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION)
                == PackageManager.PERMISSION_GRANTED) {

            LocationServices.FusedLocationApi.requestLocationUpdates(
                    mGoogleApiClient, mLocationRequest, this);
            if(currentlocation == null)
                currentlocation = LocationServices.FusedLocationApi.getLastLocation(
                        mGoogleApiClient);
        }
    }

    @Override
    public void onConnectionSuspended(int i) {

        Log.i(TAG, "GoogleApiClient connection has been suspend");
    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {

        Log.i(TAG, "GoogleApiClient connection has failed");

    }

    @Override
    public void onLocationChanged(Location location) {
        System.out.println("connection received " + location.toString());
        currentlocation = location;
    }

    private void createSalesForm() {
        lvitems = (ListView) findViewById(R.id.orderlist);
        adapter = new SalesItemAdapter(this, salesItems);
        lvitems.setAdapter(adapter);

        lvitems.setOnScrollListener(new AbsListView.OnScrollListener() {
            private int mLastFirstVisibleItem;

            @Override
            public void onScrollStateChanged(AbsListView view, int scrollState) {

            }

            @Override
            public void onScroll(AbsListView view, int firstVisibleItem,
                                 int visibleItemCount, int totalItemCount) {

                if (mLastFirstVisibleItem < firstVisibleItem) {
                    Log.i("SCROLLING DOWN", "TRUE");
                }
                if (mLastFirstVisibleItem > firstVisibleItem) {
                    Log.i("SCROLLING UP", "TRUE");
                }
                mLastFirstVisibleItem = firstVisibleItem;

            }

        });

        lvitems.setOnCreateContextMenuListener(new View.OnCreateContextMenuListener() {

            @Override
            public void onCreateContextMenu(ContextMenu menu, View view,
                                            ContextMenu.ContextMenuInfo menuInfo) {
                getMenuInflater().inflate(R.menu.removesale, menu);
                menu.setHeaderTitle(getResources().getString(R.string.strremovesale));
                menu.setHeaderIcon(R.drawable.ic_done_all_white_24dp);
                AdapterView.AdapterContextMenuInfo info = (AdapterView.AdapterContextMenuInfo) menuInfo;
                int position = info.position;
                SalesItems list = salesItems.get(position);
                Toast.makeText(StockIssueActivity.this, "ok " + list.getPrice(), Toast.LENGTH_LONG).show();
                double sales = list.getPrice();
                String total = txreportdata.getText().toString();
                if (Validating.areSet(total)) {
                    double remained = Double.parseDouble(total);
                    double rem = remained - sales;
                    txreportdata.setText(String.valueOf(rem));
                }
                salesItems.remove(position);
                adapter.notifyDataSetChanged();
                txtorderitem.setText(getString(R.string.straddeditem) + " (" + salesItems.size() + ")");
            }
        });
        String orderstatus = " (" + salesItems.size() + ")";
        txtorderitem = (TextView) findViewById(R.id.txtlabel);
        txtorderitem.append(orderstatus);
        rdiscount = (LinearLayout) findViewById(R.id.rddiscount);

        etdiscount = (EditText) findViewById(R.id.etdiscount);

        CheckBox chkdiscount = (CheckBox) findViewById(R.id.chkdiscount);
        layprod = (LinearLayout) findViewById(R.id.layproduct);
        layqty = (LinearLayout) findViewById(R.id.lyqty);
        layrep = (LinearLayout) findViewById(R.id.layreport);
        laycust = (LinearLayout) findViewById(R.id.laycustomer);
        imgaddcust = (ImageButton)findViewById(R.id.imgaddcustmer);
        if(!Settings.getAddCustomerEnabled(this))
            imgaddcust.setVisibility(View.GONE);
        imgaddcust.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                addcustomer();
            }
        });
        topb = findViewById(R.id.topborder);
        lowb = (View) findViewById(R.id.lowerborder);

        chkdiscount.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    etdiscount.setHintTextColor(Color.GRAY);
                    discountable = true;
                    etdiscount.setVisibility(View.VISIBLE);
                }
                if (!isChecked) {
                    discountable = false;
                    etdiscount.setVisibility(View.INVISIBLE);
                }
            }
        });

        String[] businesslist = Customers.getAllCustomers(this);

        productlist = Products.getAllProducts(this);
        productcodelist = Products.getAllProductsCode(this);

        ArrayAdapter<String> adapters = new ArrayAdapter<>(this, android.R.layout.select_dialog_item, businesslist);
        ArrayAdapter<String> proadapter = new ArrayAdapter<>(this, android.R.layout.select_dialog_item, productlist);

        etproduct = (AutoCompleteTextView) findViewById(R.id.autoproduct00);
        txreportdata = (TextView) findViewById(R.id.txtreportdata);
        edclientname = (AutoCompleteTextView) findViewById(R.id.autotxtsaleclientname);


        if (!business) {
            layprod.setVisibility(View.GONE);
            layqty.setVisibility(View.GONE);
            layrep.setVisibility(View.GONE);
            rdiscount.setVisibility(View.GONE);
            topb.setVisibility(View.GONE);
            lowb.setVisibility(View.GONE);
            lvitems.setVisibility(View.GONE);
            if (tabletSize) {
                System.out.println(" tablet");
                topb.setVisibility(View.VISIBLE);
                lowb.setVisibility(View.VISIBLE);
                lvitems.setVisibility(View.VISIBLE);
                layrep.setVisibility(View.INVISIBLE);
            }
        } else {
            edclientname.setText("");
            laycust.setVisibility(View.GONE);
            layprod.setVisibility(View.VISIBLE);
            layqty.setVisibility(View.VISIBLE);
            layrep.setVisibility(View.VISIBLE);

            if (Settings.getDiscountEnabled(this))
                rdiscount.setVisibility(View.VISIBLE);
            lvitems.setVisibility(View.VISIBLE);
            topb.setVisibility(View.VISIBLE);
            lowb.setVisibility(View.VISIBLE);

            this.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
        }
        etprice = (EditText) findViewById(R.id.autoprice00);

        etprice.setVisibility(View.GONE);
        rdiscount.setVisibility(View.GONE);
        txreportdata.setVisibility(View.GONE);
        TextView txttot = (TextView) findViewById(R.id.txtreportsales);
        txttot.setText("");

        etproduct.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if (!hasFocus) {
                    if (business) {
                        String productname = etproduct.getText().toString();
                        if (TextUtils.isEmpty(productname)) {
                            etproduct.clearFocus();
//                            etquantity.clearFocus();
                            Toast.makeText(StockIssueActivity.this, getResources().getString(R.string.strfilldata), Toast
                                    .LENGTH_LONG).show();
                        }
                        if (Validating.areSet(productname)) {
                            __prodcode = Products.getProductIdUsingSku(StockIssueActivity.this, productname);
                            System.out.println(" product code is " + __prodcode);
                            double pr = Products.getUnitPrice(StockIssueActivity.this, __prodcode);
                            String sku = Products.getSku(StockIssueActivity.this, __prodcode);
                            etprice.setText(getString(R.string.strprice) + " " + pr);
                            ab.setSubtitle(getString(R.string.strsku2) + " " + sku);
                        }
                    }
//                    start = false;
                }
            }
        });
        etproduct.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                String pro = (String) parent.getItemAtPosition(position);
                etproduct.setText(pro);
                __prodname = pro;
                whid = Warehouse.getDefaultId(StockIssueActivity.this);

                int pos = productlist.indexOf(pro);
                __prodcode = productcodelist.get(pos);
                double pr = Products.getUnitPrice(StockIssueActivity.this, __prodcode);
                String sku = Products.getSku(StockIssueActivity.this, __prodcode);
                ab.setSubtitle(getString(R.string.strsku2) + " " + sku);
                etprice.setText(getString(R.string.strprice) + " " + pr);
                etproduct.clearFocus();
                /*
                InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                imm.hideSoftInputFromWindow(getCurrentFocus().getWindowToken(), 0);*/

            }
        });

        etproduct.setAdapter(proadapter);
        etproduct.setThreshold(1);


        Spinner spwarehouse = (Spinner) findViewById(R.id.spwarehouse);
        ArrayList<String> whlist = Warehouse.getAll(this);
        whlistid = Warehouse.getAllIds(this);

        SpinnerAdapter spwhadapter = new ArrayAdapter<>(this, android.R.layout.simple_list_item_1, whlist);
        spwarehouse.setAdapter(spwhadapter);

        if (Warehouse.getCount(this) > 1) {
            String wh = (String) spwhadapter.getItem(1);
            whid = whlistid.get(1);
            System.out.println("wh is " + wh);
            if (!wh.equals(null)) {
                spwarehouse.setSelection(1);
            }
        } else if (Warehouse.getCount(this) > 0) {
            String wh = (String) spwhadapter.getItem(1);
            whid = whlistid.get(1);
            System.out.println("wh is " + wh);
            if (!wh.equals(null)) {
                spwarehouse.setSelection(1);
            }
        }
        spwarehouse.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                whname = "";
                whid = "";
                if (position > 0) {
                    whname = (String) parent.getItemAtPosition(position);
                    whid = whlistid.get(position);
                    System.out.println("whname is " + whname + " whid is " + whid);
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
        spwarehouse.setVisibility(View.GONE);
        etquantity = (EditText) findViewById(R.id.autoquantity00);

        etquantity.setOnFocusChangeListener(new View.OnFocusChangeListener() {

            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if (hasFocus) {
                    System.out.println("etquantity has focus");
//                    etprice.setEnabled(true);
//                    etproduct.clearFocus();
                    etprice.setText("");
                    if (!__prodcode.isEmpty()) {
                        double pr = Products.getUnitPrice(StockIssueActivity.this, __prodcode);
                        String sku = Products.getSku(StockIssueActivity.this, __prodcode);
                        etprice.setText(getString(R.string.strprice) + " " + pr);
                        ab.setSubtitle(getString(R.string.strsku2) + " " + sku);
                    }

                }
            }
        });
        etquantity.setOnKeyListener(this);

        etprice.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if (hasFocus) {
                    process_item();
                }
            }
        });

        edclientname.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                String name = (String) parent.getItemAtPosition(position);
                edclientname.setText(name);
                customerid = String.valueOf(Customers.getCustomerid(StockIssueActivity.this, name));
                customeridLocal = String.valueOf(Customers.getCustomerlocalId(StockIssueActivity.this, name));
            }
        });

        edclientname.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if(start ==1 ){
                    System.out.println("no customer");
                }
            }

            @Override
            public void afterTextChanged(Editable s) {
                if(s.length() == 0){
//                    Toast.makeText(SalesActivity.this, getString(R.string.straddcustomer)+"?",Toast.LENGTH_SHORT).show();
                }
            }
        });

        if(Customers.getCustomerCount(this) >= 1){
            String name = businesslist[0];

            edclientname.setText(name);
            customerid = String.valueOf(Customers.getCustomerid(StockIssueActivity.this, name));
            customeridLocal = String.valueOf(Customers.getCustomerlocalId(StockIssueActivity.this, name));

            edclientname.setAdapter(adapters);
            edclientname.setThreshold(1);
            edclientname.requestFocus();
        }

        edclientname.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (Validating.areSet(edclientname.getText().toString())) {
                    edclientname.setText("");
                }
            }
        });
        etproduct.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (Validating.areSet(etproduct.getText().toString())) {
                    etproduct.setText("");
                    etprice.setText("");
                    etquantity.setText("");
                    __prodcode = "";
                    getSupportActionBar().setSubtitle("");
                }
            }
        });
    }

    private void process_item() {
        System.out.println("whid " + whid);
        if(whid.equals("") || whid.isEmpty() || whid.equals(getString(R.string.strselectwarehouse)) || whid.equals(0))
        {
            whid = Warehouse.getDefaultId(this);
        }
        System.out.println("now whid " + whid);
        String qq = etquantity.getText().toString().trim();
        if(!Constants.checkDouble(this, qq, etquantity))
            return;

        String prd = etproduct.getText().toString();
        if(!prd.isEmpty()){
            int prese = Products.isProductPresent(this, new String[]{prd});
            if(prese == 0 ){
                etproduct.setText("");
                __prodcode = "";
                __prodname = "";
                etproduct.setHint(getString(R.string.strinvalidproductwarn));
                etproduct.setHintTextColor(Color.RED);
                etproduct.requestFocus();
                return;
            }
        }

        String p = __prodcode;
        etprice.setText("");
        if (Validating.areSet(p, qq)) {
            etproduct.setHintTextColor(Color.GRAY);
            etproduct.setHint(getString(R.string.strproductname));
            int qa = Products.getAllProductQuantity(StockIssueActivity.this, p);
            int q =  new BigDecimal(qq).intValue();
            int taxmethod = Products.getTaxmethod(StockIssueActivity.this, p);
            if (qa >= q && q > 0) {
                double tx = Products.getTax(StockIssueActivity.this, p);
                double pr = Products.getUnitPrice(StockIssueActivity.this, p);
                double maxdis = Settings.getDiscountLimit(this);
                //proceed now we have quantity and product code
                //check if it is discountable
                double taxamount = 0;
                double netsaleprice = 0;
                double saleprice = 0;
                double subtotal = 0;
                long prodlocal = Products.getProdid(this, p);
                String prod = Products.getProdid(this,prodlocal);
                if(prodlocal == 0)
                    return;
                long i = 0;
                if (!salesItems.isEmpty()) {
                    i = adapter.getCount() + 1;
                } else {
                    i = 1;
                }
                if(discountable){
                    //discountable
                    String discprice = etdiscount.getText().toString().trim();
                    if(!Constants.checkDouble(this,discprice,etdiscount))
                        return;
                    if (Validating.areSet(discprice)) {

                        double pdiscount = new BigDecimal(discprice).setScale(2, BigDecimal
                                .ROUND_HALF_UP).doubleValue();
                        double prdiscount = 0;
                        double checkdis = Constants.isDiscount(new double[]{pr,pdiscount,maxdis});
                        boolean error = false;
                        if(checkdis != 0)
                        {
                            etquantity.setText("");
                            etdiscount.setText("");
                            etdiscount.setHintTextColor(Color.RED);
                            etdiscount.setHint(getString(R.string.strdiscountlimitwarn) + " " + checkdis);
                            etdiscount.requestFocus();
                            error = true;
                            return;
                        }

                        if (pdiscount > 0) {//discounted price should be less than unit price
                            if(error){
                                etdiscount.setHint(getString(R.string.strproduct));
                                etdiscount.setHintTextColor(Color.DKGRAY);
                                error = false;
                            }
                            prdiscount = new BigDecimal(pr).subtract(new BigDecimal(pdiscount)).setScale(2,
                                    RoundingMode.HALF_UP).doubleValue();
                            System.out.println("new price with discount "+prdiscount);
                            double tax = Constants.itemtax(prdiscount,tx,taxmethod,__taxsettings);

                            netsaleprice = Constants.netprice(prdiscount,tx,taxmethod, __taxsettings);
                            saleprice = Constants.saleprice(prdiscount, tax, taxmethod, __taxsettings);

                            subtotal = Constants.calculate_grandtotal(saleprice,q);
                            double netsubtotal = Constants.calculate_grandtotal(netsaleprice,q);
                            taxamount = new BigDecimal(subtotal).subtract(new BigDecimal(netsubtotal)).setScale(2,
                                    RoundingMode.HALF_UP).doubleValue();
                            System.out.println("tax method is " + taxmethod + " tax settings is " +
                                    "" + __taxsettings + " netprice is " + netsaleprice + " saleprice = " +
                                    saleprice + " subtotoal " + subtotal + " netsubtotal " + netsaleprice + " " +
                                    "taxamount " + taxamount);
                            etprice.setText(String.valueOf(subtotal));

                            if(q > 0){
                                salesItems.add(new SalesItems(prodlocal,whid,i,q,netsaleprice,saleprice,taxamount,
                                        subtotal,tx,saleprice,prod));

                                adapter = new SalesItemAdapter(StockIssueActivity.this, salesItems);
                                adapter.notifyDataSetChanged();
                                txtorderitem.setText(getString(R.string.straddeditem) + " (" + salesItems.size() + ")");
                                lvitems.setAdapter(adapter);

                                double reportdata;
                                String strfinalprice = txreportdata.getText().toString();
                                if (TextUtils.isEmpty(strfinalprice)) {
                                    reportdata = 0;
                                } else {
                                    reportdata = Double.parseDouble(strfinalprice);
                                }
                                double total = subtotal + reportdata;
                                String totals = String.valueOf(total);
                                txreportdata.setText(totals);
                                txreportdata.setText(totals);
                                __prodcode = "";
                                saleprice = 0;
                                netsaleprice = 0;
                                subtotal = 0;
                                q = 0;
                                pdiscount = 0;
                                prdiscount = 0;
                                etprice.setText("");
                                etquantity.setText("");
                                etproduct.setText("");
                                etproduct.requestFocus();
                            }
                        } else {
                            Toast.makeText(StockIssueActivity.this, getString(R.string.stramountwarn), Toast
                                    .LENGTH_LONG).show();
                            etdiscount.setText("");
                        }
                    } else {
                        Toast.makeText(StockIssueActivity.this, getString(R.string.strdiscountwarn), Toast
                                .LENGTH_LONG).show();
                        etdiscount.requestFocus();
                    }
                } else {
                    double tax = Constants.itemtax(pr,tx,taxmethod,__taxsettings);
                    netsaleprice = Constants.netprice(pr, tx, taxmethod, __taxsettings);
                    saleprice = Constants.saleprice(pr, tax, taxmethod, __taxsettings);

                    subtotal = Constants.calculate_grandtotal(saleprice,q);
                    double netsubtotal = Constants.calculate_grandtotal(netsaleprice,q);
                    taxamount = new BigDecimal(subtotal).subtract(new BigDecimal(netsubtotal)).setScale(2,
                            RoundingMode.HALF_UP).doubleValue();
                    System.out.println("tax method is "+taxmethod+" tax settings is " +
                            ""+__taxsettings + " netprice is "+netsaleprice + " saleprice = " +
                            saleprice+" subtotoal "+subtotal+" netsubtotal "+netsaleprice+ " " +
                            "taxamount "+taxamount);

                    etprice.setText(String.valueOf(subtotal));
                    if(q > 0){

                        salesItems.add(new SalesItems(prodlocal,whid,i,q,netsaleprice,saleprice,taxamount,
                                subtotal,tx,0,prod));
                        adapter = new SalesItemAdapter(StockIssueActivity.this, salesItems);
                        adapter.notifyDataSetChanged();
                        txtorderitem.setText(getString(R.string.straddeditem) + " (" + salesItems.size() + ")");
                        lvitems.setAdapter(adapter);


                        double reportdata;
                        String strfinalprice = txreportdata.getText().toString();
                        if (TextUtils.isEmpty(strfinalprice)) {
                            reportdata = 0;
                        } else {
                            reportdata = Double.parseDouble(strfinalprice);
                        }
                        double total = subtotal + reportdata;
                        String totals = String.valueOf(total);
                        txreportdata.setText(totals);
                        __prodcode = "";
                        saleprice = 0;
                        netsaleprice = 0;
                        subtotal = 0;
                        q = 0;
                        etprice.setText("");
                        etquantity.setText("");
                        etproduct.setText("");
                        etproduct.requestFocus();

                    }

                }

            } else {
                Toast.makeText(StockIssueActivity.this, p + " " + getResources().getString(R.string.strsalesquantityerror1) +
                        " " + qa + " " + getResources().getString(R.string.strsalesquantityerror2), Toast.LENGTH_LONG).show();
                etquantity.setText("");
                etquantity.setHint(qa + " " + getString(R.string.strqtymax));
                etprice.setText("");
                etquantity.requestFocus();
            }
        }

        etdiscount.setText("");
        etquantity.setText("");
        ab.setSubtitle("");
        etprice.setText("");
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.add_sales_buttons, menu);
        MenuItem additem, saveitem;
        additem = menu.findItem(R.id.action_add_item);
        saveitem = menu.findItem(R.id.action_save);

        additem.setVisible(false);
        saveitem.setVisible(true);
        return super.onCreateOptionsMenu(menu);
    };

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                Intent intent = new Intent(this, MainScreen.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(intent);
                finish();
                break;
            case R.id.action_save:
                next_screen_button();
                break;
        }
        return false;

    }

    private void last_screen(final String[] vals){
        LayoutInflater infl = getLayoutInflater();
        View view = infl.inflate(R.layout.sale_summary, null) ;

        final Dialog dialog = new Dialog(this, R.style.CustomDialog	);
        dialog.setCancelable(false);
        dialog.setContentView(view);
        TextView txttotal,txtpaid,txtpaystatus,saleref,customer;

        txttotal = (TextView)dialog.findViewById(R.id.txttotal);
        txttotal.setVisibility(View.GONE);
        txtpaid = (TextView)dialog.findViewById(R.id.txtpaid);
        txtpaid.setVisibility(View.GONE);
        txtpaystatus = (TextView)dialog.findViewById(R.id.txtpaymentstatus);
        txtpaystatus.setVisibility(View.GONE);
        saleref = (TextView)dialog.findViewById(R.id.txtsaleref);
        TextView txtsb = (TextView)dialog.findViewById(R.id.txtsubtotal);
        txtsb.setText(getString(R.string.strquantity));
        customer = (TextView)dialog.findViewById(R.id.txtcustomer);
        Button btnsaveprint = (Button) dialog.findViewById(R.id.btnSubmitPrint);

        txttotal.append(" "+vals[4]);
        txtpaid.append(" " + vals[0]);
        txtpaystatus.append(" " + vals[5]);
        final String batch = "STOCK"+Constants.generateBatch()+"-"+Constants.getDateOnly2();
        saleref.setText(getString(R.string.strtransref)+" "+batch);
        customer.append(" "+Customers.getCustomerBusinessNameLoca(dialog.getContext(), customeridLocal));

        ListView listView = (ListView)dialog.findViewById(R.id.summary_list);
        SalesSummaryAdapter adapter = new SalesSummaryAdapter(dialog.getContext(),salesItems);
        listView.setAdapter(adapter);
/*
        new String[]{__paidamount[0],comm.getText().toString(), __printable[0],
                __paymode[0],txtfinal.getText().toString(),__paystatus[0]}*/

        Button btnSave = (Button) dialog.findViewById(R.id.btnSubmit);
        ImageView gobak = (ImageView)dialog.findViewById(R.id.goback);
        gobak.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });

        btnSave.setVisibility(View.GONE);
        btnsaveprint.setText(getString(R.string.strsubmit));

        btnsaveprint.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                System.out.println(" click submit and print button ");
                if (Validating.areSet(vals)) {

                    if(Settings.getSignEnabled(dialog.getContext()))
                        process_signature(new String[]{vals[0], vals[1], "0", batch,vals[5]}, dialog);
                    else
                        save_order(new String[]{vals[0], vals[1], "0", batch, vals[5]}, null, dialog);
                }
            }
        });

        dialog.show();
    }


    private void save_order(String[] valz, String[] pass,Dialog dialog) {
        SalesItems list = null;
        if(Validating.areSet(valz))
            dialog.dismiss();

        System.out.println("commments " + valz[1]);
        if (!customeridLocal.isEmpty() || !customeridLocal.equals("0")) {
            if (!valz[0].isEmpty()) {
                String cust = String.valueOf(customerid);
                String custloc = String.valueOf(customeridLocal);
                String paid = valz[0];
                double totalsales = 0;
                double totaltax = 0;
                double totaldiscount = 0;
                int totalitems = salesItems.size();
                String batch = valz[3];
                String taxset = String.valueOf(__taxsettings);
                String printed = null;
                String paymod = valz[4];
                boolean toprint = false;
                System.out.println("PRINT is "+valz[2]);
                if(valz[2].equals("1")) {
                    printed = "1";
                    toprint = true;
                }
                if(valz[2].equals("0")) {
                    printed = "0";
                    toprint  = false;
                }
                String paystatus = null;

                if (salesItems.size() > 0) {
                    for (int i = 0; i < salesItems.size(); i++) {
                        list = salesItems.get(i);
                        totalsales += list.getSbtotal();
                        totaltax += list.getTaxmt();
                    }
                }
                double _paymnt = new BigDecimal(paid).doubleValue();
                double _tsale = new BigDecimal(totalsales).doubleValue();
                if(_paymnt == _tsale)
                    paystatus = Constants.PAY_STATUS_CMPLT;
                else
                    paystatus = Constants.PAY_STATUS_PNDG;

                String lat = "0";
                String lng = "0";
                if(currentlocation != null){
                    lat = String.valueOf(currentlocation.getLatitude());
                    lng = String.valueOf(currentlocation.getLongitude());
                } else {
                    float[] lc = appPreference.getLastKnowLocation();
                    lat = String.valueOf(lc[0]);
                    lng = String.valueOf(lc[1]);
                }
                String[] vals = {cust,paid,String.valueOf(totalsales),String.valueOf(totaltax),String.valueOf
                        (totaldiscount),String.valueOf(totalitems),batch,valz[1],taxset,printed,paystatus,custloc,
                        lat,lng,Constants.SALE_STATUS_CMPLT,Constants.SALETYPE_ISSUE};
                String[] checkvals = {cust,paid,String.valueOf(totalsales),String.valueOf(totaltax),String.valueOf
                        (totaldiscount),String.valueOf(totalitems),batch,taxset,printed,paystatus,custloc};
                int orderid = 0;
                if(Validating.areSet(checkvals)){
                    if(StockIssue.insert(this, vals, Constants.LOCAL)){
                        orderid = StockIssue.getId(StockIssueActivity.this);
                        System.out.println("orderid "+orderid);
                        if(pass != null) {
                            String[] med = {pass[0], pass[1], pass[2], batch};

                            if (Validating.areSet(med)) {
                                boolean stored = MultimediaContents.storeMultimedia(this, med);
                                if (stored) {
                                    System.out.println("signature stored ? " + stored);
                                }
                            }
                        }
                    }
                }
                final int[] insert = new int[]{0};
                if(orderid != 0){
                    for(int i = 0; i < salesItems.size(); i++){
                        list = salesItems.get(i);
                        String prodno = list.getProd();
                        String locprod = String.valueOf(list.getProdloc());
                        String qty = String.valueOf(list.getQty());
                        String netprc = String.valueOf(list.getNetprice());
                        String saleprc = String.valueOf(list.getPrice());
                        String taxam = String.valueOf(list.getTaxmt());
                        String subto = String.valueOf(list.getSbtotal());
                        String wh = list.getWh();
                        String disc = String.valueOf(list.getDiscount());
                        String taxr = String.valueOf(list.getTaxrt());
                        String order = String.valueOf(orderid);
                        String saleref = batch;
                        String[] items = {prodno,locprod,qty,netprc,saleprc,taxam,subto,wh,disc,taxr,order,
                                saleref,Constants.SALE_STATUS_CMPLT,Constants.SALETYPE_ISSUE};
                        if(Validating.areSet(items)){
                            System.out.println("locprod "+locprod);
                            if(StockIssueItems.insertItems(StockIssueActivity.this, items, Constants.LOCAL) ==1){
                                String isservice = Products.getProductType(this,locprod);
                                System.out.println("product type is "+isservice);
                                if(isservice.equals(Constants.PRODUCT_STANDARD)){
                                    int prodid = Integer.parseInt(locprod);
                                    Products.updateProductQty(this, new int[]{prodid,list.getQty(), 1});
                                }
                                insert[0] += i ;
                            }
                        }
                    }
                }

                if(toprint){
                    String app = "app.incotex.fpd";
                    if(Constants.isPackageInstalled(app,this)) {
                        int send = Constants.writeReceipt(StockIssueActivity.this, salesItems,__taxsettings,customeridLocal);
                        startActivity(getPackageManager().getLaunchIntentForPackage(app));
                        setResult(-1, new Intent());
                        Toast.makeText(this, getString(R.string.strstockissued), Toast.LENGTH_LONG).show();
                        if (send == 1) {
                            Sales.putPrintStatus(this, new int[]{orderid, send});
                            customerid = "";
                            customeridLocal = "";
                            salesItems.clear();
                            whid = null;
                            toprint = false;
                            valz = null;
                            adapter.notifyDataSetChanged();
                            lvitems.setAdapter(adapter);
                            txreportdata.setText("");
                            txtorderitem.setText("");
                            business = false;
                        }
                    } else {
                        Toast.makeText(this, getString(R.string.strstockissued), Toast.LENGTH_SHORT).show();
                        Toast.makeText(this, getString(R.string.strnoprinterapp), Toast.LENGTH_LONG).show();
                        customerid = "";
                        customeridLocal = "";
                        salesItems.clear();
                        whid = null;
                        printed = "0";
                        toprint = false;
                        valz = null;
                        adapter.notifyDataSetChanged();
                        lvitems.setAdapter(adapter);
                        txreportdata.setText("");
                        txtorderitem.setText("");
                        business = false;
                    }
                } else {
                    Toast.makeText(this, getString(R.string.strstockissued), Toast.LENGTH_LONG).show();
                    customerid = "" ;
                    customeridLocal = "";
                    salesItems.clear();
                    whid = null;
                    printed = "0";
                    toprint = false;
                    valz = null;
                    adapter.notifyDataSetChanged();
                    lvitems.setAdapter(adapter);
                    txreportdata.setText("");
                    txtorderitem.setText("");
                    business = false;
                }
            }

        }

        layprod.setVisibility(View.GONE);
        layqty.setVisibility(View.GONE);
        layrep.setVisibility(View.GONE);
        rdiscount.setVisibility(View.GONE);
        topb.setVisibility(View.GONE);
        lowb.setVisibility(View.GONE);
        if(tabletSize){
            topb.setVisibility(View.VISIBLE);
            lowb.setVisibility(View.VISIBLE);
            lvitems.setVisibility(View.VISIBLE);
            layrep.setVisibility(View.INVISIBLE);
        }
        laycust.setVisibility(View.VISIBLE);
    }

    @Override
    public boolean onKey(View v, int keyCode, KeyEvent event) {
        switch (v.getId()) {
            case R.id.autoquantity00:
                if (event.getAction() == KeyEvent.ACTION_DOWN && event.getKeyCode() == KeyEvent.KEYCODE_ENTER) {
                    Log.i("event", "captured");
                    InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                    imm.hideSoftInputFromWindow(getCurrentFocus().getWindowToken(), 0);
                    process_item();
                    return false;
                }
                break;
        }
        return false;
    }
    @Override
    public boolean onKeyUp(int keyCode, KeyEvent event) {
        if(keyCode == KeyEvent.KEYCODE_BACK)
        {
            Log.i("event", "captured");
            if(salesItems.size()>0 || !__prodcode.isEmpty()){
                AlertDialog.Builder builder = new AlertDialog.Builder(this);
                builder.setMessage(getString(R.string.strsalecancelwarn))
                        .setCancelable(false)
                        .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                salesItems.clear();
                                finish();
                            }
                        })
                        .setNegativeButton("No", new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                dialog.cancel();
                            }
                        });
                AlertDialog alert = builder.create();
                alert.show();
                return true;
            } else {
                finish();
                return true;
            }
        } else
            return super.onKeyUp(keyCode, event);
    }

    @Override
    public void beforeTextChanged(CharSequence s, int start, int count, int after) {

    }

    @Override
    public void onTextChanged(CharSequence s, int start, int before, int count) {

    }

    @Override
    public void afterTextChanged(Editable s) {
        String productname = etproduct.getText().toString();
        if (TextUtils.isEmpty(productname)) {
//            etproduct.requestFocus();
            Toast.makeText(this, getResources().getString(R.string.strfilldata), Toast.LENGTH_LONG).show();
        }
        if(Validating.areSet(productname)) {
            __prodcode = Products.getProductIdUsingSku(this, productname);
            System.out.println(" product code is " + __prodcode);
            double pr = Products.getUnitPrice(StockIssueActivity.this, __prodcode);
            String sku = Products.getSku(StockIssueActivity.this, __prodcode);
            etprice.setText(getString(R.string.strprice)+" "+pr);
            ab.setSubtitle(getString(R.string.strsku2) + " " + sku);
/*
        etproduct.clearFocus();
        etquantity.requestFocus();*/
            InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(getCurrentFocus().getWindowToken(), 0);
        }
    }


    @Override
    protected void onStart() {
        super.onStart();
    }



    @Override
    public void onResume() {
        super.onResume();

    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if(mGoogleApiClient != null && mGoogleApiClient.isConnected())
            mGoogleApiClient.disconnect();
    }

    @Override
    protected void onPause() {
        super.onPause();
    }

    @Override
    protected void onStop() {
        super.onStop();
    }

    private void next_screen_button(){
        if(!business){
            String bznm = edclientname.getText().toString().trim();
            if(Validating.areSet(bznm)){
                if(!Customers.isCustomerValid(this,bznm))
                {
                    edclientname.setText("");
                    edclientname.setHint(getString(R.string.strinvalidbusinesswarn));
                    edclientname.setHintTextColor(Color.RED);
                    edclientname.requestFocus();
                } else {
                    customerid = String.valueOf(Customers.getCustomerid(StockIssueActivity.this, bznm));
                    customeridLocal = String.valueOf(Customers.getCustomerlocalId(StockIssueActivity.this, bznm));
                    business = true;
                    edclientname.setText("");
                    laycust.setVisibility(View.GONE);
                    layprod.setVisibility(View.VISIBLE);
                    etproduct.requestFocus();
                    layqty.setVisibility(View.VISIBLE);
                    layrep.setVisibility(View.VISIBLE);

                    lvitems.setVisibility(View.VISIBLE);
                    topb.setVisibility(View.VISIBLE);
                    lowb.setVisibility(View.VISIBLE);
                }
            }
        } else {
            if( !__prodcode.isEmpty()){
                AlertDialog.Builder builder = new AlertDialog.Builder(this);
                builder.setMessage(getString(R.string.strpendingitem))
                        .setCancelable(false)
                        .setPositiveButton("OKAY", new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                dialog.dismiss();
                            }
                        });
                AlertDialog alert = builder.create();
                alert.show();
            } else {
                if(salesItems != null && salesItems.size() > 0)
                {
                    final String __notes = "N/A";
                    final String __paidamount = "0";
                    final String __printable = "0";
                    final String __paymode = "Cash";
                    final String __paystatus = "Paid";
                    double totalax = 0;
                    double totalsale = 0;
                    double subtotal = 0;
                    SalesItems list = null;
                    if (salesItems.size() > 0) {
                        for (int i = 0; i < salesItems.size(); i++) {
                            list = salesItems.get(i);
                            totalax += list.getTaxmt();
                            subtotal += list.getSbtotal();
                        }
                    }

                    String[] vals = {__paidamount,__notes, __printable,
                            __paymode,__paidamount,__paystatus};
                    last_screen(vals);
                }
            }
        }
        rdiscount.setVisibility(View.GONE);
        etprice.setVisibility(View.GONE);
        txreportdata.setVisibility(View.GONE);
    }

    public void addcustomer(){

        LayoutInflater infl = getLayoutInflater();
        View view = infl.inflate(R.layout.addcustomer, null);
        final Dialog dialog = new Dialog(this, R.style.CustomDialog);
        dialog.setCancelable(false);
        dialog.setContentView(view);
        dialog.show();
        Button sub = (Button) dialog.findViewById(R.id.btnSubmit);
        TextView txttitle = (TextView) dialog.findViewById(R.id.txttitle);
        String title = getResources().getString(R.string.straddcustomer);
        title.toUpperCase(Locale.getDefault());
        txttitle.setText(title);
        final EditText clname,shname,phone,email,address,vrn,tin ;

        ImageView goback = (ImageView) dialog.findViewById(R.id.goback);
        goback.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });

        clname = (EditText) dialog.findViewById(R.id.etclientname);
        shname = (EditText) dialog.findViewById(R.id.etshopname);
        phone = (EditText) dialog.findViewById(R.id.etclientphone);
        vrn = (EditText) dialog.findViewById(R.id.etvrn);
        tin = (EditText) dialog.findViewById(R.id.ettin);
        email = (EditText) dialog.findViewById(R.id.etclientemail);
        address = (EditText) dialog.findViewById(R.id.etclientshopaddress);


        sub.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String strclname, strshname, strclemail, strclphone, strcladdress, svrn, stin;
                strclname = clname.getText().toString();
                strshname = Validating.stripit(shname.getText().toString());
                strclemail = Validating.stripit(email.getText().toString());
                strclphone = Validating.stripit(phone.getText().toString());
                svrn = vrn.getText().toString();
                stin = tin.getText().toString();
                strcladdress = address.getText().toString();

                if (!strclname.isEmpty())
                    if (!Constants.checkInvalidChars(dialog.getContext(), strclname, clname))
                        return;
                if (!strshname.isEmpty())
                    if (!Constants.checkInvalidChars(dialog.getContext(), strshname, shname))
                        return;
                if (!svrn.isEmpty())
                    if (!Constants.checkInvalidChars(dialog.getContext(), svrn, vrn))
                        return;
                if (!stin.isEmpty())
                    if (!Constants.checkInvalidChars(dialog.getContext(), stin, tin))
                        return;
                if (!strclphone.isEmpty())
                    if (!Constants.checkInvalidChars(dialog.getContext(), strclphone, phone))
                        return;
                if (!strclemail.isEmpty())
                    if (!Constants.checkInvalidChars(dialog.getContext(), strclemail, email))
                        return;

                if (!Validating.areSet(strcladdress))
                    strcladdress = "Dar es salaam";
                if (!Validating.areSet(strclname))
                    strclname = shname.getText().toString();
                EditText[] et = new EditText[]{clname, shname, phone, email, address};
                String[] input = new String[]{strclname, strshname, strclphone, strclemail, strcladdress, svrn, stin};
                String[] inputcheck = new String[]{strclname, strshname, strclphone};

                if (Validating.areSet(inputcheck)) {
                    if (strclemail.isEmpty()) {
                        int insert = Customers.insertCustomerLocal(dialog.getContext(), input);
                        if (insert == 1) {
                            customerid = String.valueOf(Customers.getCustomerid(StockIssueActivity.this, strshname));
                            customeridLocal = String.valueOf(Customers.getCustomerlocalId(StockIssueActivity.this, strshname));
                            business = true;
                            dialog.dismiss();
                            edclientname.setText("");
                            laycust.setVisibility(View.GONE);
                            layprod.setVisibility(View.VISIBLE);
                            etproduct.requestFocus();
                            layqty.setVisibility(View.VISIBLE);
                            layrep.setVisibility(View.VISIBLE);

                            if (Settings.getDiscountEnabled(dialog.getContext()))
                                rdiscount.setVisibility(View.VISIBLE);
                            lvitems.setVisibility(View.VISIBLE);
                            topb.setVisibility(View.VISIBLE);
                            lowb.setVisibility(View.VISIBLE);
                            Toast.makeText(dialog.getContext(), getResources().getString(R.string.strinputsuccesscustomer), Toast.LENGTH_SHORT).show();
                        } else {
                            Toast.makeText(dialog.getContext(), getResources().getString(R.string.strinputfailcustomer), Toast.LENGTH_SHORT).show();
                            dialog.dismiss();
                        }
                    } else if (!strclemail.isEmpty() && Validating.isEmailValid(strclemail)) {

                        int insert = Customers.insertCustomerLocal(dialog.getContext(), input);
                        if (insert == 1) {
                            customerid = String.valueOf(Customers.getCustomerid(StockIssueActivity.this, strshname));
                            customeridLocal = String.valueOf(Customers.getCustomerlocalId(StockIssueActivity.this, strshname));
                            business = true;
                            dialog.dismiss();
                            edclientname.setText("");
                            laycust.setVisibility(View.GONE);
                            layprod.setVisibility(View.VISIBLE);
                            etproduct.requestFocus();
                            layqty.setVisibility(View.VISIBLE);
                            layrep.setVisibility(View.VISIBLE);

                            if (Settings.getDiscountEnabled(dialog.getContext()))
                                rdiscount.setVisibility(View.VISIBLE);
                            lvitems.setVisibility(View.VISIBLE);
                            topb.setVisibility(View.VISIBLE);
                            lowb.setVisibility(View.VISIBLE);
                            Toast.makeText(dialog.getContext(), getResources().getString(R.string.strinputsuccesscustomer), Toast.LENGTH_SHORT).show();
                        } else {
                            Toast.makeText(dialog.getContext(), getResources().getString(R.string.strinputfailcustomer), Toast.LENGTH_SHORT).show();
                            dialog.dismiss();
                        }
                    } else {
                        et[3].setHint(getResources().getString(R.string.strinputerror) +
                                " " + getResources().getString(R.string.strclientemail));
                        et[3].setHintTextColor(Color.RED);
                        et[3].setCompoundDrawablesWithIntrinsicBounds(getResources().getDrawable(R.drawable.ic_timer_auto_black_24dp), null, getResources().getDrawable(R.drawable.ic_error_red_24dp), null);
                    }

                } else {
                    if (!Validating.areSet(strclname)) {
                        et[0].setHint(getResources().getString(R.string.strinputerror) +
                                " " + getResources().getString(R.string.strclientname));
                        et[0].setHintTextColor(Color.RED);
                        et[0].setCompoundDrawablesWithIntrinsicBounds(getResources().getDrawable(R.drawable.ic_timer_auto_black_24dp), null, getResources().getDrawable(R.drawable.ic_error_red_24dp), null);
                    }
                    if (!Validating.areSet(strshname)) {
                        et[1].setHint(getResources().getString(R.string.strinputerror) +
                                " " + getResources().getString(R.string.strclientshopname));
                        et[1].setHintTextColor(Color.RED);

                        et[1].setCompoundDrawablesWithIntrinsicBounds(getResources().getDrawable(R.drawable.ic_account_box_black_24dp), null, getResources().getDrawable(R.drawable.ic_error_red_24dp), null);
                    }
                    if (!Validating.areSet(strclphone)) {
                        et[2].setHint(getResources().getString(R.string.strinputerror) +
                                " " + getResources().getString(R.string.strclientphone));
                        et[2].setHintTextColor(Color.RED);
                        et[2].setCompoundDrawablesWithIntrinsicBounds(getResources().getDrawable(R.drawable.ic_local_phone_black_24dp), null, getResources().getDrawable(R.drawable.ic_error_red_24dp), null);
                    }
                    if (!Validating.areSet(strcladdress)) {
                        et[4].setHint(getResources().getString(R.string.strinputerror) +
                                " " + getResources().getString(R.string.strclientaddress));
                        et[4].setHintTextColor(Color.RED);
                        et[4].setCompoundDrawablesWithIntrinsicBounds(getResources().getDrawable(R.drawable.ic_pin_drop_black_24dp), null, getResources().getDrawable(R.drawable.ic_error_red_24dp), null);
                    }
                    return;
                }
            }
        });
    }

    private void process_signature(final String[] pass, final Dialog dialog) {
        LayoutInflater infl = (LayoutInflater)getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View view = infl.inflate(R.layout.signature, null);

        final Dialog dg = new Dialog(this, R.style.CustomDialog);
        dg.setCancelable(true);
        dg.setContentView(view);


        tempDir = Environment.getExternalStorageDirectory()+"/"+Constants.MASTER_DIR+"/.temp";
        ContextWrapper cw = new ContextWrapper(dg.getContext().getApplicationContext());
        File directory = cw.getDir("tayananjoka", Context.MODE_PRIVATE);

        prepareDirectory();
        uniqueId = System.currentTimeMillis() + "_" + Math.random();
        current = uniqueId + ".png";
        mypath = new File(directory, current);

        mContent = (LinearLayout)dg. findViewById(R.id.linearLayout);
        mSignature = new signature(dg.getContext(), null);
        mSignature.setBackgroundColor(Color.WHITE);
        mContent.addView(mSignature, ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT);
        mClear = (Button) dg.findViewById(R.id.clear);
        mGetSign = (Button) dg.findViewById(R.id.getsign);
        chkconsent = (CheckBox)dg.findViewById(R.id.chkconsent);
        chkconsent.setVisibility(View.GONE);
        mGetSign.setEnabled(false);
        mCancel = (Button) dg.findViewById(R.id.cancel);
        mView = mContent;


        mClear.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                Log.v("log_tag", "Panel Cleared");
                mSignature.clear();
                mGetSign.setEnabled(false);
            }
        });

        mGetSign.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                mView.setDrawingCacheEnabled(true);
                String bitmap = mSignature.save(mView);

                if (bitmap != null) {
                        Log.v("log_tag", "uri " + bitmap);
                        String[] nm = bitmap.split("/");
                        String[] med = {bitmap,nm[nm.length-1], MultimediaContents.OPERATIONTYPEIS};

                        if(Validating.areSet(med)) {
                            save_order(pass, med, dialog);
                        }
                    if (dg.isShowing())
                        dg.dismiss();
                }
            }
        });

        mCancel.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                Log.v("log_tag", "Panel Canceled");
                dg.dismiss();
            }
        });

        dg.show();
    }


    private boolean prepareDirectory()
    {
        try
        {
            if (makedirs())
            {
                return true;
            } else {
                return false;
            }
        } catch (Exception e)
        {
            e.printStackTrace();
            Toast.makeText(this, "Could not initiate File System.. Is Sdcard mounted properly?", Toast.LENGTH_SHORT)
                    .show();
            return false;
        }
    }

    private boolean makedirs()
    {
        File tempdir = new File(tempDir);
        if (!tempdir.exists())
            tempdir.mkdirs();

        if (tempdir.isDirectory())
        {
            File[] files = tempdir.listFiles();
            for (File file : files)
            {
                if (!file.delete())
                {
                    System.out.println("Failed to delete " + file);
                }
            }
        }
        return (tempdir.isDirectory());
    }
    public class signature extends View
    {
        private static final float STROKE_WIDTH = 5f;
        private static final float HALF_STROKE_WIDTH = STROKE_WIDTH / 2;
        private Paint paint = new Paint();
        private Path path = new Path();

        private float lastTouchX;
        private float lastTouchY;
        private final RectF dirtyRect = new RectF();

        public signature(Context context, AttributeSet attrs)
        {
            super(context, attrs);
            paint.setAntiAlias(true);
            paint.setColor(Color.BLACK);
            paint.setStyle(Paint.Style.STROKE);
            paint.setStrokeJoin(Paint.Join.ROUND);
            paint.setStrokeWidth(STROKE_WIDTH);
        }

        public String save(View v)
        {
            Uri uri = null;
            Log.v("log_tag", "Width: " + v.getWidth());
            Log.v("log_tag", "Height: " + v.getHeight());
            String bitmap = null;
            if(mBitmap == null)
            {
                mBitmap =  Bitmap.createBitmap(mContent.getWidth(), mContent.getHeight(), Bitmap.Config.RGB_565);;
            }
            Canvas canvas = new Canvas(mBitmap);
            try
            {
                FileOutputStream mFileOutStream = new FileOutputStream(mypath);

                v.draw(canvas);
                mBitmap.compress(Bitmap.CompressFormat.PNG, 90, mFileOutStream);
                bitmap = saveImage(mBitmap);
            }
            catch(Exception e)
            {
                Log.v("log_tag", e.toString());
            }
            return bitmap;
        }

        public void clear()
        {
            path.reset();
            invalidate();
        }

        private String saveImage(Bitmap bitmap) {

            String stored = null;

            File sdcard = Environment.getExternalStorageDirectory() ;

            File folder = new File(sdcard.getAbsoluteFile(), Constants.MASTER_DIR);
            folder.mkdir();
            String filename = String.valueOf(System.currentTimeMillis());
            File file = new File(folder.getAbsoluteFile(), filename + ".jpg") ;
            if (file.exists())
                return stored ;

            try {
                FileOutputStream out = new FileOutputStream(file);
                bitmap.compress(Bitmap.CompressFormat.JPEG, 90, out);
                out.flush();
                out.close();
                stored = file.getAbsolutePath();
                mGetSign.setEnabled(true);
            } catch (Exception e) {
                e.printStackTrace();
            }
            return stored;
        }

        @Override
        protected void onDraw(Canvas canvas)
        {
            canvas.drawPath(path, paint);
        }

        @Override
        public boolean onTouchEvent(MotionEvent event)
        {
            float eventX = event.getX();
            float eventY = event.getY();
            mGetSign.setEnabled(true);

            switch (event.getAction())
            {
                case MotionEvent.ACTION_DOWN:
                    path.moveTo(eventX, eventY);
                    lastTouchX = eventX;
                    lastTouchY = eventY;
                    return true;

                case MotionEvent.ACTION_MOVE:

                case MotionEvent.ACTION_UP:

                    resetDirtyRect(eventX, eventY);
                    int historySize = event.getHistorySize();
                    for (int i = 0; i < historySize; i++)
                    {
                        float historicalX = event.getHistoricalX(i);
                        float historicalY = event.getHistoricalY(i);
                        expandDirtyRect(historicalX, historicalY);
                        path.lineTo(historicalX, historicalY);
                    }
                    path.lineTo(eventX, eventY);
                    break;

                default:
                    debug("Ignored touch event: " + event.toString());
                    return false;
            }

            invalidate((int) (dirtyRect.left - HALF_STROKE_WIDTH),
                    (int) (dirtyRect.top - HALF_STROKE_WIDTH),
                    (int) (dirtyRect.right + HALF_STROKE_WIDTH),
                    (int) (dirtyRect.bottom + HALF_STROKE_WIDTH));

            lastTouchX = eventX;
            lastTouchY = eventY;

            return true;
        }

        private void debug(String string){
        }

        private void expandDirtyRect(float historicalX, float historicalY)
        {
            if (historicalX < dirtyRect.left)
            {
                dirtyRect.left = historicalX;
            }
            else if (historicalX > dirtyRect.right)
            {
                dirtyRect.right = historicalX;
            }

            if (historicalY < dirtyRect.top)
            {
                dirtyRect.top = historicalY;
            }
            else if (historicalY > dirtyRect.bottom)
            {
                dirtyRect.bottom = historicalY;
            }
        }

        private void resetDirtyRect(float eventX, float eventY)
        {
            dirtyRect.left = Math.min(lastTouchX, eventX);
            dirtyRect.right = Math.max(lastTouchX, eventX);
            dirtyRect.top = Math.min(lastTouchY, eventY);
            dirtyRect.bottom = Math.max(lastTouchY, eventY);
        }
    }
}


