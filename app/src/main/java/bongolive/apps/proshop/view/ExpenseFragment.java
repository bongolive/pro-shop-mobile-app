/*
 * The source codes in this project are owned by Bongolive Enterprises. Copying or any kind of usage without Bongolive's approval is against the law.
 * Copyright (c) 2015.
 *
 */

package bongolive.apps.proshop.view;

import android.app.Dialog;
import android.content.Context;
import android.content.ContextWrapper;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Path;
import android.graphics.RectF;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.support.v4.app.Fragment;
import android.support.v7.widget.CardView;
import android.util.AttributeSet;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import java.io.File;
import java.io.FileOutputStream;
import java.util.ArrayList;

import bongolive.apps.proshop.R;
import bongolive.apps.proshop.controller.AppPreference;
import bongolive.apps.proshop.controller.Constants;
import bongolive.apps.proshop.controller.Validating;
import bongolive.apps.proshop.model.Expenses;
import bongolive.apps.proshop.model.MultimediaContents;
import bongolive.apps.proshop.model.User;


public class ExpenseFragment extends Fragment implements View.OnClickListener
{
    private static final String ARG_SECTION_NUMBER = "section_number";
    AlertDialogManager alert ;
    AppPreference appPreference;
    View _rootView;
    EditText etnote,etspname,etopname,etpayamount,etexptype ;
    TextView tetmin,tetmout,tetcain,tetcaout,tettiin,tettiout,tetnote,tetstname,tetopname,txtsubdate,txtsync ;
    TextView txtsign;
    ImageView imgsign;
    CardView cardv;
    Spinner sppay,spexpense;
    private static String signaturepath = null, _paytype = null, _extype = null;
    private String[] info = null;
    boolean tabletsize;

    /*
    *  signature */
    LinearLayout mContent;
    signature mSignature;
    Button mClear, mGetSign, mCancel;

    public static String tempDir;
    public int count = 1;
    public String current = null;
    private Bitmap mBitmap;
    View mView;
    File mypath;
    private String uniqueId;
    String FRAGMENTSOURCE = null;
    String[] paymethod;
    ArrayList<String> extype;
    ArrayAdapter exadapter;
    ArrayAdapter payadapter;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
    {
        if(getArguments().getString(Constants.REPORTS) != null ) {
            FRAGMENTSOURCE = getArguments().getString(Constants.REPORTS);
            System.out.println(" source is "+FRAGMENTSOURCE);
            if (_rootView == null) {
                tabletsize = getResources().getBoolean(R.bool.isTablet);
                _rootView = inflater.inflate(R.layout.expense_form2, container, false);
            }
        } else {
            if (_rootView == null) {
                tabletsize = getResources().getBoolean(R.bool.isTablet);
                _rootView = inflater.inflate(R.layout.expense_form, container, false);
            }
        }
        return _rootView ;
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState)
    {
        super.onActivityCreated(savedInstanceState);
        alert = new AlertDialogManager();

        appPreference = new AppPreference(getActivity());

        if(FRAGMENTSOURCE == null) {
            etexptype = (EditText) getActivity().findViewById(R.id.etexptype);
            etpayamount = (EditText) getActivity().findViewById(R.id.etamount);
            etnote = (EditText) getActivity().findViewById(R.id.etnotes);
            etspname = (EditText) getActivity().findViewById(R.id.etspname);
            etopname = (EditText) getActivity().findViewById(R.id.etopname);
            txtsign = (TextView) getActivity().findViewById(R.id.txtaddattach);
            imgsign = (ImageView) getActivity().findViewById(R.id.imgattach);
            cardv = (CardView)getActivity().findViewById(R.id.card_view_gone);
            sppay = (Spinner) getActivity().findViewById(R.id.sppaymethod);
            spexpense = (Spinner) getActivity().findViewById(R.id.spexpensetype);
            setHasOptionsMenu(true);
            txtsign.setOnClickListener(this);
            paymethod = getResources().getStringArray(R.array.strpaymethods_array);
            extype = new ArrayList<>();
            extype.add(getString(R.string.stselectexpense));
            extype.addAll(appPreference.getExpenseType());
            extype.add(getString(R.string.straddexpensetype));

            payadapter =  new ArrayAdapter<>(getContext(), android.R.layout
                    .simple_dropdown_item_1line, paymethod);
            payadapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
            exadapter =  new ArrayAdapter<>(getContext(), android.R.layout
                    .simple_dropdown_item_1line, extype);
            exadapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
            sppay.setAdapter(payadapter);
            spexpense.setAdapter(exadapter);

            sppay.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                @Override
                public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                    _paytype = (String) payadapter.getItem(position);
                }

                @Override
                public void onNothingSelected(AdapterView<?> parent) {

                }
            });
            spexpense.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                @Override
                public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                    if (position != 0 || position < extype.size() - 1) {
                        _extype = null;
                        cardv.setVisibility(View.GONE);
                        _extype = (String) exadapter.getItem(position);
                    }

                    if (position == extype.size() - 1) {
                        cardv.setVisibility(View.VISIBLE);
                        etexptype.requestFocus();
                        _extype = null;
                    }
                }

                @Override
                public void onNothingSelected(AdapterView<?> parent) {

                }
            });

        } else {
            tetmin = (TextView) getActivity().findViewById(R.id.txtmeterin);
            tetmout = (TextView) getActivity().findViewById(R.id.txtmeterout);
            tetopname = (TextView) getActivity().findViewById(R.id.txtopname);
            tetcain = (TextView) getActivity().findViewById(R.id.txtcashin);
            tetcaout = (TextView) getActivity().findViewById(R.id.txtcashout);
            tettiin = (TextView) getActivity().findViewById(R.id.txttimein);
            tettiout = (TextView) getActivity().findViewById(R.id.txttimeout);
            tetnote = (TextView) getActivity().findViewById(R.id.txtnotes);
            tetstname = (TextView) getActivity().findViewById(R.id.txtstname);
            tetopname = (TextView) getActivity().findViewById(R.id.txtopname);
            txtsubdate = (TextView) getActivity().findViewById(R.id.txtsubdate);
            txtsync = (TextView) getActivity().findViewById(R.id.txtsynced);
            imgsign = (ImageView) getActivity().findViewById(R.id.imgsign);

            info = getArguments().getStringArray("info");
            if(info != null){
                tetmin.setText(info[0]);tetmout.setText(info[1]);tetopname.setText(info[2]);tetcain.setText(info[3]);
                tetcaout.setText(info[4]);tettiin.setText(info[5]);tettiout.setText(info[6]);tetnote.setText(info[7]);
                tetstname.setText(info[8]);txtsubdate.setText(info[9]);txtsync.setText(info[10]);
                String img = info[11];
                if(Validating.areSet(img)) {
                    Bitmap bitmap = Constants.decodeImg(img, 500, 500);
                    if(bitmap != null)
                        imgsign.setImageBitmap(bitmap);
                    else
                        imgsign.setVisibility(View.GONE);
                }
            }
            setHasOptionsMenu(false);
        }

    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {

        inflater.inflate(R.menu.stationmenu, menu);

        super.onCreateOptionsMenu(menu, inflater);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_saveonly:
                addform();
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    public void addform(){
        String paytype,reference,extype,amount,note,sign,snm,creator,timestamp,op;

        op = etopname.getText().toString();
        paytype = _paytype;
        reference = "EXP/"+System.currentTimeMillis();
        extype = cardv.getVisibility() == View.GONE ? _extype : etexptype.getText().toString();

        if(cardv.getVisibility() == View.VISIBLE) {
            appPreference.storeExpenseType(extype);
        }

        amount = etpayamount.getText().toString();
        snm = etspname.getText().toString();
        timestamp = String.valueOf(System.currentTimeMillis());
        note = etnote.getText().toString();
        sign = signaturepath;
        creator = String.valueOf(appPreference.getUserId());
        String username = User.getUser(getContext(), creator);
        if(!Validating.areSet(username)) {
            alert.showAlertDialog(getContext(), getString(R.string.strerror), getString(R.string.strsesionwarn), false);
            return;
        }

        String[] vals = {reference,amount,extype,paytype,op,snm,creator,sign,note,timestamp};
        String[] check = {reference,amount,extype,paytype,timestamp,op,creator};
        if(Validating.areSet(check)){
            boolean ins = Expenses.insert(getContext(), vals, Constants.LOCAL);
            if(ins){
                String[] mult = {signaturepath, username + "_" + System.currentTimeMillis(),
                        MultimediaContents.OPERATIONTYPEE, timestamp};
                System.out.println("username "+username);
                if(Validating.areSet(mult)) {
                    if (!MultimediaContents.isMediaStored(getContext(), timestamp)) {
                        boolean stored = MultimediaContents.storeMultimedia(getContext(), mult);
                        if (stored) {
                            System.out.println("stored ? " + stored);
                        }
                    }
                }
                etexptype.setText("");etpayamount.setText("");cardv.setVisibility(View.GONE);
                etnote.setText("");etopname.setText("");
                ;etspname.setText("");imgsign.setVisibility(View.GONE);txtsign.setVisibility(View
                        .VISIBLE);
                this.extype.clear();
                this.extype.add(getString(R.string.stselectexpense));
                this.extype.addAll(appPreference.getExpenseType());
                this.extype.add(getString(R.string.straddexpensetype));
                Toast.makeText(getContext(),getString(R.string.strexpenseadded),Toast.LENGTH_LONG).show();
            }
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.txtaddattach:
                process_signature(imgsign);
                break;
        }
    }

    private void process_signature(final ImageView imgsign) {
        LayoutInflater infl = (LayoutInflater)getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View view = infl.inflate(R.layout.signature, null);

        final Dialog dg = new Dialog(getContext(), R.style.CustomDialog);
        dg.setCancelable(true);
        dg.setContentView(view);

        String tempDir = Environment.getExternalStorageDirectory()+"/Proshop_Reports/.temp";
        ContextWrapper cw = new ContextWrapper(dg.getContext().getApplicationContext());
        File directory = cw.getDir("tayananjoka", Context.MODE_PRIVATE);

        prepareDirectory();
        uniqueId = String.valueOf(System.currentTimeMillis());
        current = uniqueId + ".png";
        mypath = new File(directory, current);

        LinearLayout lconsent = (LinearLayout)dg.findViewById(R.id.layoutconsent);
        lconsent.setVisibility(View.GONE);
        mContent = (LinearLayout)dg. findViewById(R.id.linearLayout);
        mSignature = new signature(dg.getContext(), null);
        mSignature.setBackgroundColor(Color.WHITE);
        mContent.addView(mSignature, ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT);
        mClear = (Button) dg.findViewById(R.id.clear);
        mGetSign = (Button) dg.findViewById(R.id.getsign);
        mGetSign.setEnabled(false);
        mCancel = (Button) dg.findViewById(R.id.cancel);
        mView = mContent;

        mClear.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                Log.v("log_tag", "Panel Cleared");
                mSignature.clear();
                mGetSign.setEnabled(false);
            }
        });

        mGetSign.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                mView.setDrawingCacheEnabled(true);
                String bitmap = mSignature.save(mView);

                if (bitmap != null) {
                    Log.v("log_tag", "uri " + bitmap);
                    signaturepath = bitmap;
                    txtsign.setVisibility(View.GONE);
                    imgsign.setVisibility(View.VISIBLE);
                    Bitmap mbitmap = Constants.decodeImg(bitmap, 400, 300);
                    imgsign.setImageBitmap(mbitmap);
                } else {
                    imgsign.setVisibility(View.GONE);
                    txtsign.setVisibility(View.VISIBLE);
                }
                if(dg.isShowing())
                    dg.dismiss();
            }
        });

        mCancel.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                Log.v("log_tag", "Panel Canceled");
                dg.dismiss();
            }
        });

        dg.show();
    }

    private boolean prepareDirectory()
    {
        try
        {
            if (makedirs())
            {
                return true;
            } else {
                return false;
            }
        } catch (Exception e)
        {
            e.printStackTrace();
            return false;
        }
    }

    private boolean makedirs()
    {
        File tempdir = new File(tempDir);
        if (!tempdir.exists())
            tempdir.mkdirs();

        if (tempdir.isDirectory())
        {
            File[] files = tempdir.listFiles();
            for (File file : files)
            {
                if (!file.delete())
                {
                    System.out.println("Failed to delete " + file);
                }
            }
        }
        return (tempdir.isDirectory());
    }
    public class signature extends View
    {
        private static final float STROKE_WIDTH = 5f;
        private static final float HALF_STROKE_WIDTH = STROKE_WIDTH / 2;
        private Paint paint = new Paint();
        private Path path = new Path();

        private float lastTouchX;
        private float lastTouchY;
        private final RectF dirtyRect = new RectF();

        public signature(Context context, AttributeSet attrs)
        {
            super(context, attrs);
            paint.setAntiAlias(true);
            paint.setColor(Color.BLACK);
            paint.setStyle(Paint.Style.STROKE);
            paint.setStrokeJoin(Paint.Join.ROUND);
            paint.setStrokeWidth(STROKE_WIDTH);
        }

        public String save(View v)
        {
            Uri uri = null;
            Log.v("log_tag", "Width: " + v.getWidth());
            Log.v("log_tag", "Height: " + v.getHeight());
            String bitmap = null;
            if(mBitmap == null)
            {
                mBitmap =  Bitmap.createBitmap(mContent.getWidth(), mContent.getHeight(), Bitmap.Config.RGB_565);
            }
            Canvas canvas = new Canvas(mBitmap);
            try
            {
                FileOutputStream mFileOutStream = new FileOutputStream(mypath);

                v.draw(canvas);
                mBitmap.compress(Bitmap.CompressFormat.PNG, 90, mFileOutStream);
                bitmap = saveImage(mBitmap);
                mGetSign.setEnabled(true);
            }
            catch(Exception e)
            {
                Log.v("log_tag", e.toString());
            }
            return bitmap;
        }

        public void clear()
        {
            path.reset();
            invalidate();
        }

        private String saveImage(Bitmap bitmap) {

            String stored = null;

            File sdcard = Environment.getExternalStorageDirectory() ;

            File folder = new File(sdcard.getAbsoluteFile(), "Proshop_Reports");
            folder.mkdir();
            String filename = String.valueOf(System.currentTimeMillis());
            File file = new File(folder.getAbsoluteFile(), filename + ".jpg") ;
            if (file.exists())
                return stored ;

            try {
                FileOutputStream out = new FileOutputStream(file);
                bitmap.compress(Bitmap.CompressFormat.JPEG, 90, out);
                out.flush();
                out.close();
                stored = file.getAbsolutePath();
            } catch (Exception e) {
                e.printStackTrace();
            }
            return stored;
        }

        @Override
        protected void onDraw(Canvas canvas)
        {
            canvas.drawPath(path, paint);
        }

        @Override
        public boolean onTouchEvent(MotionEvent event)
        {
            float eventX = event.getX();
            float eventY = event.getY();
            mGetSign.setEnabled(true);

            switch (event.getAction())
            {
                case MotionEvent.ACTION_DOWN:
                    path.moveTo(eventX, eventY);
                    lastTouchX = eventX;
                    lastTouchY = eventY;
                    return true;

                case MotionEvent.ACTION_MOVE:

                case MotionEvent.ACTION_UP:

                    resetDirtyRect(eventX, eventY);
                    int historySize = event.getHistorySize();
                    for (int i = 0; i < historySize; i++)
                    {
                        float historicalX = event.getHistoricalX(i);
                        float historicalY = event.getHistoricalY(i);
                        expandDirtyRect(historicalX, historicalY);
                        path.lineTo(historicalX, historicalY);
                    }
                    path.lineTo(eventX, eventY);
                    break;

                default:
                    debug("Ignored touch event: " + event.toString());
                    return false;
            }

            invalidate((int) (dirtyRect.left - HALF_STROKE_WIDTH),
                    (int) (dirtyRect.top - HALF_STROKE_WIDTH),
                    (int) (dirtyRect.right + HALF_STROKE_WIDTH),
                    (int) (dirtyRect.bottom + HALF_STROKE_WIDTH));

            lastTouchX = eventX;
            lastTouchY = eventY;

            return true;
        }

        private void debug(String string){
        }

        private void expandDirtyRect(float historicalX, float historicalY)
        {
            if (historicalX < dirtyRect.left)
            {
                dirtyRect.left = historicalX;
            }
            else if (historicalX > dirtyRect.right)
            {
                dirtyRect.right = historicalX;
            }

            if (historicalY < dirtyRect.top)
            {
                dirtyRect.top = historicalY;
            }
            else if (historicalY > dirtyRect.bottom)
            {
                dirtyRect.bottom = historicalY;
            }
        }

        private void resetDirtyRect(float eventX, float eventY)
        {
            dirtyRect.left = Math.min(lastTouchX, eventX);
            dirtyRect.right = Math.max(lastTouchX, eventX);
            dirtyRect.top = Math.min(lastTouchY, eventY);
            dirtyRect.bottom = Math.max(lastTouchY, eventY);
        }
    }
}
