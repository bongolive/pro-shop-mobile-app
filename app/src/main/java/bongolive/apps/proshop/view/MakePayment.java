/*
 * The source codes in this project are owned by Bongolive Enterprises. Copying or any kind of usage without Bongolive's approval is against the law.
 * Copyright (c) 2015.
 *
 */


package bongolive.apps.proshop.view;


import android.app.Dialog;
import android.content.Context;
import android.content.ContextWrapper;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Path;
import android.graphics.RectF;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.RecyclerView;
import android.text.InputFilter;
import android.util.AttributeSet;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import java.io.File;
import java.io.FileOutputStream;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.ArrayList;

import bongolive.apps.proshop.R;
import bongolive.apps.proshop.controller.Constants;
import bongolive.apps.proshop.controller.Validating;
import bongolive.apps.proshop.model.MultimediaContents;
import bongolive.apps.proshop.model.Payments;
import bongolive.apps.proshop.model.Sales;

public class MakePayment extends AppCompatActivity implements View.OnClickListener{

    private boolean tabletSize = false;
    private RecyclerView mRecyclerView;
    private RecyclerView.Adapter mAdapter;
    private RecyclerView.LayoutManager mLayoutManager;
    private ActionBar actionBar;
    private EditText etfilter;
    private ArrayList arrayList;
    private RelativeLayout rl;
    private ImageView imgdone,imgnext;
    private AutoCompleteTextView bzn;
    private ActionBar ab;
    ArrayList<Payments> mItems;
    long pyid;
    String title;
    TextView txtsaleref,txtpayref,txtdebt,txtinfo;
    Button txtsign;
    EditText etpay,etpayee;
    Spinner etpayer;
    ImageView imgsign;
    String timestamp,saleid;

    private static String signaturepath = null;

    /*
    *  signature */
    LinearLayout mContent;
    signature mSignature;
    Button mClear, mGetSign, mCancel;

    public static String tempDir;
    public int count = 1;
    public String current = null;
    private Bitmap mBitmap;
    View mView;
    File mypath;
    private String uniqueId;
    String payer;
    String balance,orderid;
    double grandbalance,grandpaid;
    String[] methods = null;
    ArrayAdapter adapter = null;
    /*
    * end of new strings
    */

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        tabletSize = getResources().getBoolean(R.bool.isTablet);
        if (!tabletSize) {
            setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        }
        setContentView(R.layout.make_payment);

        ab = getSupportActionBar();
        ab.setDisplayUseLogoEnabled(true);
        ab.setDisplayHomeAsUpEnabled(true);

        methods = getResources().getStringArray(R.array.paymethod_array);

        Intent intent = getIntent();
        pyid = intent.getExtras().getLong(Payments.ID);
        title = intent.getExtras().getString(Sales.CUSTOMERID);
        if(pyid == 0 )
            finish();

        ab.setTitle(getString(R.string.straddpayment));

        txtinfo = (TextView)findViewById(R.id.txtstinfo);
        txtsaleref = (TextView)findViewById(R.id.txtsalerefv);
        txtpayref = (TextView)findViewById(R.id.txtpayrefv);
        txtsign = (Button)findViewById(R.id.txtaddsign);
        txtdebt = (TextView)findViewById(R.id.txtdebtv);

        etpay = (EditText)findViewById(R.id.ettopay);
        etpayee = (EditText)findViewById(R.id.etpayee);
        etpayer = (Spinner)findViewById(R.id.etpayedby);

        imgsign = (ImageView)findViewById(R.id.imgsign);

        txtsign.setOnClickListener(this);

        adapter = new ArrayAdapter<>(this, android.R.layout.simple_dropdown_item_1line, methods);
        etpayer.setAdapter(adapter);
        etpayer.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                if(position > 0){
                    payer = (String) adapter.getItem(position);
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
        String payref = Constants.generatePayref();
        txtpayref.setText(payref);

        mItems = Payments.getPaymentSummary(this, pyid);
        Payments p = mItems.get(0);
        balance = p.getPostbalance();
        orderid = p.getLocalsale();
        saleid = p.getSyssale();
        grandbalance = Sales.getDebt(this, orderid);
        grandpaid = Sales.getPaid(this, orderid);
        txtdebt.setText(String.valueOf(grandbalance));
        int l = String.valueOf(grandbalance).length();
        InputFilter[] f = new InputFilter[1];
        f[0] = new InputFilter.LengthFilter(l-2);
        etpay.setFilters(f);

        String ls = p.getLocalsale();
        timestamp = Constants.generatePayref();
        txtsaleref.setText(timestamp);
        txtinfo.setText(title);
        etpayee.setText(p.getRecby());
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.audit, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                return true;
            case R.id.action_tick:
                process_item();
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.txtaddsign:
                process_signature(imgsign);
                break;
        }
    }


    private void process_item(){
        String pay = etpay.getText().toString().trim();
        String payee =etpayee.getText().toString().trim();

        String[] vals = {pay,payee,payer};

        if(Validating.areSet(vals)) {
            double paid = new BigDecimal(pay).setScale(2, RoundingMode.HALF_UP).doubleValue();
            if(paid <= grandbalance && paid > 0) {
                double pbd = grandbalance-paid;
                double gt = paid+grandpaid;
//                Log.e("paid","paid "+paid+" debt "+grandbalance+" pbc "+pbd+" grandblance "+gt);
                String postb = String.valueOf(pbd);

                String[] va = {"0",postb,"none",Constants.getDate(),pay,orderid
                        , saleid, payer, signaturepath, Constants.PAYTYPE_RECEIVED,"0",timestamp,payee,"1"};
                if(Validating.areSet(va)) {
                    String[] mult = {signaturepath, title + "_" + System.currentTimeMillis(),
                            MultimediaContents.OPERATIONTYPEPY, timestamp};
                    boolean store = Payments.insert(this, va);
                    if(store) {
                        Sales.updatePaymentStatus(this,gt,orderid);
                        if (!MultimediaContents.isMediaStored(this, timestamp)) {
                            boolean stored = MultimediaContents.storeMultimedia(this, mult);
                            if (stored) {
                                System.out.println("stored ? " + stored);
                                finish();
                            }
                        }
                    }
                    finish();
                }
            } else {
                Toast.makeText(MakePayment.this, getString(R.string.strdebtwarn), Toast.LENGTH_LONG).show();
            }
        }
    }
    private void process_signature(final ImageView imgsign) {
        LayoutInflater infl = (LayoutInflater)getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View view = infl.inflate(R.layout.signature, null);

        final Dialog dg = new Dialog(this, R.style.CustomDialog);
        dg.setCancelable(true);
        dg.setContentView(view);

        tempDir = Environment.getExternalStorageDirectory()+"/Proshop_Reports/.temp/";
        ContextWrapper cw = new ContextWrapper(dg.getContext().getApplicationContext());
        File directory = cw.getDir("tayananjoka", Context.MODE_PRIVATE);

        prepareDirectory();
        uniqueId = String.valueOf(System.currentTimeMillis());
        current = uniqueId + ".png";
        mypath = new File(directory, current);

        LinearLayout lconsent = (LinearLayout)dg.findViewById(R.id.layoutconsent);
        lconsent.setVisibility(View.GONE);
        mContent = (LinearLayout)dg. findViewById(R.id.linearLayout);
        mSignature = new signature(dg.getContext(), null);
        mSignature.setBackgroundColor(Color.WHITE);
        mContent.addView(mSignature, ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT);
        mClear = (Button) dg.findViewById(R.id.clear);
        mGetSign = (Button) dg.findViewById(R.id.getsign);
        mGetSign.setEnabled(false);
        mCancel = (Button) dg.findViewById(R.id.cancel);
        mView = mContent;

        mClear.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                Log.v("log_tag", "Panel Cleared");
                mSignature.clear();
                mGetSign.setEnabled(false);
            }
        });

        mGetSign.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                mView.setDrawingCacheEnabled(true);
                String bitmap = mSignature.save(mView);

                if (bitmap != null) {
                    Log.v("log_tag", "uri " + bitmap);
                    signaturepath = bitmap;
                    txtsign.setVisibility(View.GONE);
                    imgsign.setVisibility(View.VISIBLE);
                    Bitmap mbitmap = Constants.decodeImg(bitmap, 400, 300);
                    imgsign.setImageBitmap(mbitmap);
                } else {
                    imgsign.setVisibility(View.GONE);
                    txtsign.setVisibility(View.VISIBLE);
                }
                if(dg.isShowing())
                    dg.dismiss();
            }
        });

        mCancel.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                Log.v("log_tag", "Panel Canceled");
                dg.dismiss();
            }
        });

        dg.show();
    }

    private boolean prepareDirectory()
    {
        try
        {
            if (makedirs())
            {
                return true;
            } else {
                return false;
            }
        } catch (Exception e)
        {
            e.printStackTrace();
            return false;
        }
    }

    private boolean makedirs()
    {
        File tempdir = new File(tempDir);
        if (!tempdir.exists())
            tempdir.mkdirs();

        if (tempdir.isDirectory())
        {
            File[] files = tempdir.listFiles();
            for (File file : files)
            {
                if (!file.delete())
                {
                    System.out.println("Failed to delete " + file);
                }
            }
        }
        return (tempdir.isDirectory());
    }
    public class signature extends View
    {
        private static final float STROKE_WIDTH = 5f;
        private static final float HALF_STROKE_WIDTH = STROKE_WIDTH / 2;
        private Paint paint = new Paint();
        private Path path = new Path();

        private float lastTouchX;
        private float lastTouchY;
        private final RectF dirtyRect = new RectF();

        public signature(Context context, AttributeSet attrs)
        {
            super(context, attrs);
            paint.setAntiAlias(true);
            paint.setColor(Color.BLACK);
            paint.setStyle(Paint.Style.STROKE);
            paint.setStrokeJoin(Paint.Join.ROUND);
            paint.setStrokeWidth(STROKE_WIDTH);
        }

        public String save(View v)
        {
            Uri uri = null;
            Log.v("log_tag", "Width: " + v.getWidth());
            Log.v("log_tag", "Height: " + v.getHeight());
            String bitmap = null;
            if(mBitmap == null)
            {
                mBitmap =  Bitmap.createBitmap(mContent.getWidth(), mContent.getHeight(), Bitmap.Config.RGB_565);
            }
            Canvas canvas = new Canvas(mBitmap);
            try
            {
                FileOutputStream mFileOutStream = new FileOutputStream(mypath);

                v.draw(canvas);
                mBitmap.compress(Bitmap.CompressFormat.PNG, 90, mFileOutStream);
                bitmap = saveImage(mBitmap);
                mGetSign.setEnabled(true);
            }
            catch(Exception e)
            {
                Log.v("log_tag", e.toString());
            }
            return bitmap;
        }

        public void clear()
        {
            path.reset();
            invalidate();
        }

        private String saveImage(Bitmap bitmap) {

            String stored = null;

            File sdcard = Environment.getExternalStorageDirectory() ;

            File folder = new File(sdcard.getAbsoluteFile(), Constants.MASTER_DIR);
            folder.mkdir();
            String filename = String.valueOf(System.currentTimeMillis());
            File file = new File(folder.getAbsoluteFile(), filename + ".jpg") ;
            if (file.exists())
                return stored ;

            try {
                FileOutputStream out = new FileOutputStream(file);
                bitmap.compress(Bitmap.CompressFormat.JPEG, 90, out);
                out.flush();
                out.close();
                stored = file.getAbsolutePath();
            } catch (Exception e) {
                e.printStackTrace();
            }
            return stored;
        }

        @Override
        protected void onDraw(Canvas canvas)
        {
            canvas.drawPath(path, paint);
        }

        @Override
        public boolean onTouchEvent(MotionEvent event)
        {
            float eventX = event.getX();
            float eventY = event.getY();
            mGetSign.setEnabled(true);

            switch (event.getAction())
            {
                case MotionEvent.ACTION_DOWN:
                    path.moveTo(eventX, eventY);
                    lastTouchX = eventX;
                    lastTouchY = eventY;
                    return true;

                case MotionEvent.ACTION_MOVE:

                case MotionEvent.ACTION_UP:

                    resetDirtyRect(eventX, eventY);
                    int historySize = event.getHistorySize();
                    for (int i = 0; i < historySize; i++)
                    {
                        float historicalX = event.getHistoricalX(i);
                        float historicalY = event.getHistoricalY(i);
                        expandDirtyRect(historicalX, historicalY);
                        path.lineTo(historicalX, historicalY);
                    }
                    path.lineTo(eventX, eventY);
                    break;

                default:
                    debug("Ignored touch event: " + event.toString());
                    return false;
            }

            invalidate((int) (dirtyRect.left - HALF_STROKE_WIDTH),
                    (int) (dirtyRect.top - HALF_STROKE_WIDTH),
                    (int) (dirtyRect.right + HALF_STROKE_WIDTH),
                    (int) (dirtyRect.bottom + HALF_STROKE_WIDTH));

            lastTouchX = eventX;
            lastTouchY = eventY;

            return true;
        }

        private void debug(String string){
        }

        private void expandDirtyRect(float historicalX, float historicalY)
        {
            if (historicalX < dirtyRect.left)
            {
                dirtyRect.left = historicalX;
            }
            else if (historicalX > dirtyRect.right)
            {
                dirtyRect.right = historicalX;
            }

            if (historicalY < dirtyRect.top)
            {
                dirtyRect.top = historicalY;
            }
            else if (historicalY > dirtyRect.bottom)
            {
                dirtyRect.bottom = historicalY;
            }
        }

        private void resetDirtyRect(float eventX, float eventY)
        {
            dirtyRect.left = Math.min(lastTouchX, eventX);
            dirtyRect.right = Math.max(lastTouchX, eventX);
            dirtyRect.top = Math.min(lastTouchY, eventY);
            dirtyRect.bottom = Math.max(lastTouchY, eventY);
        }
    }
}


