/*
 * Copyright (c) 2015.
 * Prosurvey is a property of Bongolive Enterprises Ltd. An authorized
 * copy of ideas, source codes is against the regulations
 */

package bongolive.apps.proshop.view;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebView;
import android.widget.EditText;

import bongolive.apps.proshop.R;
import bongolive.apps.proshop.controller.AppPreference;


public class UserAccount extends Fragment {
    AppPreference appPreference;
    EditText company,contactp,mobile,accounttype,accvalidity, etabout;
    String scomp,scontact,smob,sacctyp,svalid;
    boolean tabletSize = false;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
    {

        tabletSize = getResources().getBoolean(R.bool.isTablet);
        return inflater.inflate(R.layout.user_account, container, false) ;
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState)
    {
        super.onActivityCreated(savedInstanceState);
        appPreference = new AppPreference(getActivity());
//        Transfer.updateall(getContext(), "1");

        company = (EditText)getActivity().findViewById(R.id.txtaccbuz);
        contactp = (EditText)getActivity().findViewById(R.id.txtacccontact);
        mobile = (EditText)getActivity().findViewById(R.id.txtaccphone);
        accounttype = (EditText)getActivity().findViewById(R.id.txtacctype);
        accvalidity = (EditText)getActivity().findViewById(R.id.txtaccvalidity);
        String[] info = appPreference.getAccountInfo();
        int accounttype_ = appPreference.get_accounttype();
        String authkey = appPreference.getAuthkey();

        if(!tabletSize) {
            scomp = !info[0].isEmpty() ? info[0] : "Demo Company Name";
            scontact = !info[1].isEmpty() ? info[1] : "Demo Contact Person";
            smob = !info[2].isEmpty() ? info[2] : "+255 000 000 000";

            sacctyp = accounttype_ < 2 ? checkacc(accounttype_) : "Demo Account";
            svalid = !authkey.isEmpty() ? "Valid Account " : "Demo Account";

            company.setText(getString(R.string.straccountname) + " : " + scomp);
            contactp.setText(getString(R.string.strcontactp) + " : " + scontact);
            mobile.setText(getString(R.string.strclientphone) + " : " + smob);
            accounttype.setText(getString(R.string.straccounttype) + " : " + sacctyp);
            accvalidity.setText(getString(R.string.straccountvalidity) + " : " + svalid);
        } else {

            scomp = !info[0].isEmpty() ? info[0] : "Demo Company Name";
            scontact = !info[1].isEmpty() ? info[1] : "Demo Contact Person";
            smob = !info[2].isEmpty() ? info[2] : "+255 000 000 000";

            sacctyp = accounttype_ < 2 ? checkacc(accounttype_) : "Demo Account";
            svalid = !authkey.isEmpty() ? "Valid Account " : "Demo Account";

            company.setText( scomp);
            contactp.setText( scontact);
            mobile.setText(smob);
            accounttype.setText(sacctyp);
            accvalidity.setText( svalid);
        }
        WebView wv;
        wv = (WebView)getActivity().findViewById(R.id.webview);
        wv.loadUrl("file:///android_asset/about.html");
    }
    private String checkacc(int acc)
    {
        String account ;
       account =  acc == 0 ? "Normal Account" : " Premium Account";
        return account;
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {

        inflater.inflate(R.menu.dashboard, menu);
        MenuItem item = menu.findItem(R.id.action_register);
        item.setVisible(true);

        super.onCreateOptionsMenu(menu, inflater);
    }

}
