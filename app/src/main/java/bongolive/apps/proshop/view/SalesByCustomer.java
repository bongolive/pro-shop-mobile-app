/*
 * Copyright (c) 2015.
 * Prosurvey is a property of Bongolive Enterprises Ltd. An authorized
 * copy of ideas, source codes is against the regulations
 */

package bongolive.apps.proshop.view;

import android.app.Dialog;
import android.content.ContentResolver;
import android.content.Intent;
import android.database.Cursor;
import android.database.DatabaseUtils;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.text.Editable;
import android.text.Html;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;


import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import bongolive.apps.proshop.R;
import bongolive.apps.proshop.controller.AppPreference;
import bongolive.apps.proshop.controller.Constants;
import bongolive.apps.proshop.controller.DeliveringAdapter;
import bongolive.apps.proshop.controller.ExportItems;
import bongolive.apps.proshop.controller.ExportReports;
import bongolive.apps.proshop.controller.ItemListAdapterOrderItems;
import bongolive.apps.proshop.controller.ReportsAdapter;
import bongolive.apps.proshop.controller.ReportsItem;
import bongolive.apps.proshop.controller.Validating;
import bongolive.apps.proshop.model.Customers;
import bongolive.apps.proshop.model.DatabaseHandler;
import bongolive.apps.proshop.model.Delivery;
import bongolive.apps.proshop.model.Product_Categories;
import bongolive.apps.proshop.model.Products;
import bongolive.apps.proshop.model.PurchaseItems;
import bongolive.apps.proshop.model.Sales;
import bongolive.apps.proshop.model.SalesItems;
import bongolive.apps.proshop.model.Settings;
import bongolive.apps.proshop.model.Station;
import bongolive.apps.proshop.model.StationPump;
import bongolive.apps.proshop.model.Warehouse;


public class SalesByCustomer extends Fragment implements View.OnClickListener{

    TextView header,filter,txttotals,txtsubtotals,txtmiddle ;
    ReportsAdapter adapter;
    ListView lv ;
    String rec = null, cat = "", starttime = "",endtime="";
    Spinner spinner,timespinner ;
    LinearLayout lb , ltotal,lf;
    TextView txtsum,txttotalslable;
    int time;
    String[] timerange = new String[]{};
    Bundle data;
    ArrayAdapter<String> mSpinnerAdapter, mSpinnerAdapter2;
    TextView etst,etend;
    AppPreference appPreference;
    MenuItem mprint ;
    String currenttitle = null;
    boolean istablet ;
    FloatingActionButton actionButton;
    ItemListAdapterOrderItems salesitemsadapter ;
    String curr;
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
    {
        return inflater.inflate(R.layout.reports_list, container, false) ;
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState)
    {
        super.onActivityCreated(savedInstanceState);
        lv = (ListView)getActivity().findViewById(R.id.list);
        istablet = getResources().getBoolean(R.bool.isTablet);

        appPreference = new AppPreference(getActivity());
        header = (TextView)getActivity().findViewById(R.id.txtheading);
        txtsubtotals = (TextView)getActivity().findViewById(R.id.txtweek);
        txttotals = (TextView)getActivity().findViewById(R.id.txtday);
        filter = (TextView)getActivity().findViewById(R.id.txtfilter);
        spinner = (Spinner)getActivity().findViewById(R.id.filterspin);
        timespinner = (Spinner)getActivity().findViewById(R.id.timebasedspin);
        lb = (LinearLayout)getActivity().findViewById(R.id.laybottom);
        ltotal = (LinearLayout)getActivity().findViewById(R.id.laytotals);
        lf = (LinearLayout)getActivity().findViewById(R.id.laytimefilter);
        txtsum = (TextView)getActivity().findViewById(R.id.totals);
        txtmiddle = (TextView)getActivity().findViewById(R.id.middle);
        txttotalslable = (TextView)getActivity().findViewById(R.id.totalslabel);
        etst = (TextView)getActivity().findViewById(R.id.etstartdate);
        etend = (TextView)getActivity().findViewById(R.id.etendtdate);
        etst.setOnClickListener(this);
        etend.setOnClickListener(this);
        setHasOptionsMenu(true);
        curr = appPreference.getDefaultCurrency();
        curr = curr != null ? curr : "Tsh";
        int postion;
        etend.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                starttime = etst.getText().toString();
                endtime = etend.getText().toString();

                if (starttime.isEmpty() || starttime.equals(getString(R.string.strstartdate))) {
                    Toast.makeText(getActivity(), getString(R.string.strdatefilterwarn), Toast.LENGTH_LONG).show();
//                    etend.setText(getString(R.string.strstartdate));
                } else if (endtime.isEmpty() || endtime.equals(getString(R.string.strenddate))) {
                    Toast.makeText(getActivity(), getString(R.string.strdatefilterwarn), Toast.LENGTH_LONG).show();
//                    etend.setText(getString(R.string.strenddate));
                } else {

                    timerange = new String[]{starttime, endtime};

                    switch (rec) {
                        case Constants.SALESBYCUSTOMER:
                            header.setText(currenttitle + " " + getString(R.string.strfrom) + " " + starttime + " " +
                                    getString(R.string.strto) + " " + endtime);
                            adapter = new ReportsAdapter(getActivity(), salesbusiness(3), rec);
                            currenttitle = currenttitle + " " + getString(R.string.strfrom) + " " + starttime + " " +
                                    getString(R.string.strto) + " " + endtime;
                            adapter.notifyDataSetChanged();
                            lv.setAdapter(adapter);
                            break;
                        case Constants.SALESBYPRODUCT:
                            adapter = new ReportsAdapter(getActivity(), salesbyproduct(3, cat), rec);
                            header.setText(currenttitle + " " + getString(R.string.strfrom) + " " + starttime + " " +
                                    getString(R.string.strto) + " " + endtime);
                            currenttitle = currenttitle + " " + getString(R.string.strfrom) + " " + starttime + " " +
                                    getString(R.string.strto) + " " + endtime;
                            adapter.notifyDataSetChanged();
                            lv.setAdapter(adapter);
                            break;
                        case Constants.TAXBYPRODUCT:
                            adapter = new ReportsAdapter(getActivity(), taxsum(), rec);
                            header.setText(currenttitle + " " + getString(R.string.strfrom) + " " + starttime + " " +
                                    getString(R.string.strto) + " " + endtime);
                            currenttitle = currenttitle + " " + getString(R.string.strfrom) + " " + starttime + " " +
                                    getString(R.string.strto) + " " + endtime;
                            adapter.notifyDataSetChanged();
                            lv.setAdapter(adapter);
                            break;
                        case Constants.PROFITREPORT:
                            adapter = new ReportsAdapter(getActivity(), profitReport(), rec);
                            header.setText(currenttitle + " " + getString(R.string.strfrom) + " " + starttime + " " +
                                    getString(R.string.strto) + " " + endtime);
                            currenttitle = currenttitle + " " + getString(R.string.strfrom) + " " + starttime + " " +
                                    getString(R.string.strto) + " " + endtime;
                            adapter.notifyDataSetChanged();
                            lv.setAdapter(adapter);
                            break;
                    }
                }
            }
        });

        data = getArguments();

        if(data.getString(Constants.REPORTS).equals(Constants.SALESBYPRODUCT) || data.getString(Constants.REPORTS)
            .equals(Constants.SALESBYCUSTOMER)) {
            ltotal.setVisibility(View.VISIBLE);
        }
        if(data.getString(Constants.REPORTS).equals(Constants.TAXBYPRODUCT) || data.getString(Constants.REPORTS).equals
            (Constants.PROFITREPORT) ) {
            lf.setVisibility(View.VISIBLE);
            ltotal.setVisibility(View.VISIBLE);
            if(data.getString(Constants.REPORTS).equals(Constants.TAXBYPRODUCT))
                txttotalslable.setText(getString(R.string.strtotsales));
            if(data.getString(Constants.REPORTS).equals(Constants.PROFITREPORT)) {
                txtmiddle.setText(getString(R.string.strprofit));
            }
        }

        if(data.getString(Constants.REPORTS).equals(Constants.SALEREPORT)){

            lf.setVisibility(View.GONE);//for time rang filter
            if(istablet){
                LinearLayout lh = (LinearLayout)getActivity().findViewById(R.id.laysalesheader);
                lh.setVisibility(View.VISIBLE);
                TextView txti,txti1,txti2,txti3,txti4,txti5;
                txti = (TextView)getActivity().findViewById(R.id.txti);
                txti1 = (TextView)getActivity().findViewById(R.id.txti1);
                txti2 = (TextView)getActivity().findViewById(R.id.txti2);
                txti3 = (TextView)getActivity().findViewById(R.id.txti3);
                txti4 = (TextView)getActivity().findViewById(R.id.txti4);
                txti5 = (TextView)getActivity().findViewById(R.id.txti5);

                txti1.setText(getString(R.string.strtotalcost));
                txti2.setText(getString(R.string.stramountpaid));
                txti3.setText(getString(R.string.strbusiness));
                txti4.setText(getString(R.string.strorderdate));
                txti5.setText(getString(R.string.strtotaltax));
            }
            txttotals.setText(getString(R.string.strdate));
            txtsubtotals.setText(getString(R.string.strtotsales));
            LinearLayout laytit = (LinearLayout)getActivity().findViewById(R.id.lay1);
            laytit.setVisibility(View.GONE);

            header.setText(getString(R.string.strsalesheader));
            currenttitle = getString(R.string.strsalesbytax);
            filter.setText(getString(R.string.strbusiness));
            header.setVisibility(View.GONE);
            lb.setVisibility(View.GONE);
            txtsubtotals.setVisibility(View.GONE);
            txttotals.setVisibility(View.GONE);
            rec = Constants.SALEREPORT;

            lv.setClickable(true);

            adapter = new ReportsAdapter(getActivity(),getList("",""),data.getString(Constants.REPORTS));
            adapter.notifyDataSetChanged();
            lv.setAdapter(adapter);
            lv.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                @Override
                public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                    Object o = lv.getItemAtPosition(position);
                    ReportsItem list = (ReportsItem) o;
                    long i = list.getOrderid();
                    String businessname = list.getBusinessname();
                    Log.v("id", "id is " + i);
                    if (!businessname.isEmpty() && i > 0)
                        showOrders(i, businessname);
                }
            });


        } else if(data.getString(Constants.REPORTS).equals(Constants.DELIVERYHISTORY)){

            lf.setVisibility(View.GONE);//for time rang filter
            if(istablet){
                LinearLayout lh = (LinearLayout)getActivity().findViewById(R.id.laysalesheader);
                lh.setVisibility(View.VISIBLE);
                TextView txti,txti1,txti2,txti3,txti4,txti5;
                txti = (TextView)getActivity().findViewById(R.id.txti);
                txti1 = (TextView)getActivity().findViewById(R.id.txti1);
                txti2 = (TextView)getActivity().findViewById(R.id.txti2);
                txti3 = (TextView)getActivity().findViewById(R.id.txti3);
                txti4 = (TextView)getActivity().findViewById(R.id.txti4);
                txti5 = (TextView)getActivity().findViewById(R.id.txti5);
                txti1.setText(getString(R.string.strsaleref));
                txti2.setText(getString(R.string.strdeldate));
                txti3.setText(getString(R.string.strdelref));
                txti4.setText(getString(R.string.strcustomer));
                txti5.setText(getString(R.string.straddress));
                txti1.setVisibility(View.GONE);
                txti3.setVisibility(View.GONE);

            }
            txttotals.setText(getString(R.string.strdate));
            txtsubtotals.setText(getString(R.string.strtotsales));
            LinearLayout laytit = (LinearLayout)getActivity().findViewById(R.id.lay1);
            laytit.setVisibility(View.GONE);

            header.setText(getString(R.string.strsalesheader));
            currenttitle = getString(R.string.strsalesbytax);
            filter.setText(getString(R.string.strbusiness));
            header.setVisibility(View.GONE);
            lb.setVisibility(View.GONE);
            txtsubtotals.setVisibility(View.GONE);
            txttotals.setVisibility(View.GONE);
            rec = Constants.SALEREPORT;

            lv.setClickable(true);

            adapter = new ReportsAdapter(getActivity(),getDelList("", ""),data.getString(Constants.REPORTS));
            adapter.notifyDataSetChanged();
            lv.setAdapter(adapter);

            lv.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                @Override
                public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                    Object o = lv.getItemAtPosition(position);
                    ReportsItem list = (ReportsItem) o;
                    long i = list.getOrderid();
                    String businessname = list.getBusinessname();
                    Log.v("id", "id is " + i);
                    if (!businessname.isEmpty() && i > 0)
                        showDelivery(i, businessname);
                }
            });


        } else if(data.getString(Constants.REPORTS).equals(Constants.ORDERREPORT)){

            lf.setVisibility(View.GONE);//for time rang filter
            if(istablet){
                LinearLayout lh = (LinearLayout)getActivity().findViewById(R.id.laysalesheader);
                lh.setVisibility(View.VISIBLE);
                TextView txti,txti1,txti2,txti3,txti4,txti5;
                txti = (TextView)getActivity().findViewById(R.id.txti);
                txti1 = (TextView)getActivity().findViewById(R.id.txti1);
                txti2 = (TextView)getActivity().findViewById(R.id.txti2);
                txti3 = (TextView)getActivity().findViewById(R.id.txti3);
                txti4 = (TextView)getActivity().findViewById(R.id.txti4);
                txti5 = (TextView)getActivity().findViewById(R.id.txti5);
                txti1.setText(getString(R.string.strsaleref));
                txti2.setText(getString(R.string.strdeldate));
                txti3.setText(getString(R.string.strdelref));
                txti4.setText(getString(R.string.strcustomer));
                txti5.setText(getString(R.string.straddress));
                txti1.setVisibility(View.GONE);
                txti3.setVisibility(View.GONE);

            }
            txttotals.setText(getString(R.string.strdate));
            txtsubtotals.setText(getString(R.string.strtotsales));
            LinearLayout laytit = (LinearLayout)getActivity().findViewById(R.id.lay1);
            laytit.setVisibility(View.GONE);

            header.setText(getString(R.string.strsalesheader));
            currenttitle = getString(R.string.strsalesbytax);
            filter.setText(getString(R.string.strbusiness));
            header.setVisibility(View.GONE);
            lb.setVisibility(View.GONE);
            txtsubtotals.setVisibility(View.GONE);
            txttotals.setVisibility(View.GONE);
            rec = Constants.SALEREPORT;

            lv.setClickable(true);

            adapter = new ReportsAdapter(getActivity(),getOrderList("", ""),data.getString(Constants.REPORTS));
            adapter.notifyDataSetChanged();
            lv.setAdapter(adapter);
            lv.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                @Override
                public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                    Object o = lv.getItemAtPosition(position);
                    ReportsItem list = (ReportsItem) o;
                    long i = list.getOrderid();
                    String businessname = list.getBusinessname();
                    Log.v("id", "id is " + i);
                    if (!businessname.isEmpty() && i > 0)
                        showOrders(i, businessname);
                }
            });

        } else if(data.getString(Constants.REPORTS).equals(Constants.STATIONHISTORY)){
            lf.setVisibility(View.GONE);//for time rang filter
            System.out.println("station");
            if(istablet){
                LinearLayout lh = (LinearLayout)getActivity().findViewById(R.id.laysalesheader);
                lh.setVisibility(View.VISIBLE);
                TextView txti,txti1,txti2,txti3,txti4,txti5;
                txti = (TextView)getActivity().findViewById(R.id.txti);
                txti1 = (TextView)getActivity().findViewById(R.id.txti1);
                txti2 = (TextView)getActivity().findViewById(R.id.txti2);
                txti3 = (TextView)getActivity().findViewById(R.id.txti3);
                txti4 = (TextView)getActivity().findViewById(R.id.txti4);
                txti4.setVisibility(View.GONE);
                txti5 = (TextView)getActivity().findViewById(R.id.txti5);
                txti5.setVisibility(View.GONE);
                txti1.setText(getString(R.string.strstation));
                txti2.setText(getString(R.string.strstmastername));
                txti3.setText(getString(R.string.strsubdate));

            }
            txttotals.setText(getString(R.string.strdate));
            txtsubtotals.setText(getString(R.string.strtotsales));
            LinearLayout laytit = (LinearLayout)getActivity().findViewById(R.id.lay1);
            laytit.setVisibility(View.GONE);

            header.setText(getString(R.string.strsalesheader));
            currenttitle = getString(R.string.strsalesbytax);
            filter.setText(getString(R.string.strbusiness));
            header.setVisibility(View.GONE);
            lb.setVisibility(View.GONE);
            txtsubtotals.setVisibility(View.GONE);
            txttotals.setVisibility(View.GONE);
            rec = Constants.STATIONHISTORY;

            lv.setClickable(true);

            adapter = new ReportsAdapter(getActivity(),getStationList("", ""),data.getString(Constants.REPORTS));
            adapter.notifyDataSetChanged();
            lv.setAdapter(adapter);
            lv.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                @Override
                public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                    Object o = lv.getItemAtPosition(position);
                    ReportsItem list = (ReportsItem) o;
                    long i = list.getStid();
                    String min = list.getMein(), mo = list.getMeout(),opn = list.getOpname(),ci = list.getCain();
                    String co = list.getCaout(), ti = list.getTin(),to = list.getTout(),no = list.getComment();
                    String stn = Warehouse.getDefault(getContext()), subd = list.getCreated(),syn = null, upd =list.getUpdated();
                    int ack = list.getSynced(); String img = list.getPhoto();String pmp = list.getPump();
                    System.out.println(" ack "+ack);
                    if(ack == 0)
                        syn = getString(R.string.strnever);
                    if(ack == 1)
                        syn = upd ;

                    String[] info = {min,mo,opn,ci,co,ti,to,no,stn,subd,syn,img,pmp};
                    Log.v("id", "id is " + i);
                    if (i > 0)
                        showStation(info);
                }
            });
        }
         mSpinnerAdapter = new ArrayAdapter(getActivity(), R.layout.spinnerlayout, getResources().getStringArray(R.array
                 .filteroptions));
        mSpinnerAdapter.setDropDownViewResource(R.layout.filter_view);
        timespinner.setAdapter(mSpinnerAdapter);
        timespinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                rec = data.getString(Constants.REPORTS);
                switch (rec){
                    case Constants.SALESBYCUSTOMER:
                        filter.setText(getString(R.string.strcustomer));
                        txttotals.setText(getString(R.string.strtotsales));
                        txtsubtotals.setText(getString(R.string.strtotoutstanding));
                        switch (position){
                            case 0:
                                adapter = new ReportsAdapter(getActivity(),salesbusiness(1),rec);
                                position = 1;
                                header.setText(getString(R.string.strsalesbydate) + " " + getString(R.string.strweek));
                                currenttitle = getString(R.string.strsalesbydate) + " " + getString(R.string.strweek);
                                adapter.notifyDataSetChanged();
                                lv.setAdapter(adapter);
                                lf.setVisibility(View.GONE);
                                break;
                            case 1:
                                adapter = new ReportsAdapter(getActivity(),salesbusiness(2),rec);
                                position =2;
                                header.setText(getString(R.string.strsalesbydate) + " " + getString(R.string.strmonth));
                                currenttitle = getString(R.string.strsalesbydate) + " " + getString(R.string.strmonth);
                                adapter.notifyDataSetChanged();
                                lv.setAdapter(adapter);
                                lf.setVisibility(View.GONE);
                                break;
                            case 2:
                                adapter = new ReportsAdapter(getActivity(),salesbusiness(0),rec);
                                position = 0;
                                header.setText(getString(R.string.strsalesbydate) + " " + getString(R.string.strtoday));
                                currenttitle = getString(R.string.strsalesbydate) + " " + getString(R.string.strtoday);
                                adapter.notifyDataSetChanged();
                                lv.setAdapter(adapter);
                                lf.setVisibility(View.GONE);
                                break;
                            case 3:
                                lf.setVisibility(View.VISIBLE);
                                currenttitle = getString(R.string.strsalesbydate);
                                lb.setVisibility(View.GONE);
                                break;
                        }
                        break;
                    case Constants.SALESBYPRODUCT:
                        filter.setText(getString(R.string.strproduct));
                        txttotals.setText(getString(R.string.strqtysold));
                        txtsubtotals.setText(getString(R.string.strtotsales));

                        switch (position){
                            case 0:
                                adapter = new ReportsAdapter(getActivity(),salesbyproduct(1, cat),rec);
                                header.setText(getString(R.string.strsalesbyproduct) + " " + getString(R.string.strweek));
                                currenttitle = getString(R.string.strsalesbyproduct) + " " + getString(R.string.strweek);
                                adapter.notifyDataSetChanged();
                                time = 1;
                                lv.setAdapter(adapter);
                                lf.setVisibility(View.GONE);
                                break;
                            case 1:
                                adapter = new ReportsAdapter(getActivity(),salesbyproduct(2, cat),rec);
                                header.setText(getString(R.string.strsalesbyproduct) + " " + getString(R.string.strmonth));
                                currenttitle = getString(R.string.strsalesbyproduct) + " " + getString(R.string.strmonth);
                                adapter.notifyDataSetChanged();
                                time = 2;
                                lv.setAdapter(adapter);
                                lf.setVisibility(View.GONE);
                                break;
                            case 2:
                                adapter = new ReportsAdapter(getActivity(),salesbyproduct(0, cat),rec);
                                header.setText(getString(R.string.strsalesbyproduct) + " " + getString(R.string.strtoday));
                                currenttitle = getString(R.string.strsalesbyproduct) + " " + getString(R.string.strtoday);
                                adapter.notifyDataSetChanged();
                                time = 0;
                                lv.setAdapter(adapter);
                                lf.setVisibility(View.GONE);
                                break;
                            case 3:
                                lf.setVisibility(View.VISIBLE);
                                currenttitle = getString(R.string.strsalesbyproduct);
                                lb.setVisibility(View.GONE);
                                break;
                        }
                        break;
                    case Constants.TAXBYPRODUCT:
                        txttotals.setText(getString(R.string.strtotsales)+" ("+curr+")");
                        txtsubtotals.setText(getString(R.string.strtotaltax)+" ("+curr+")");
                        txtmiddle.setText(getString(R.string.strtotsales));
                        header.setText(getString(R.string.strsalesbytax));
                        currenttitle = getString(R.string.strsalesbytax);
                        filter.setText(getString(R.string.strdate));
                        lb.setVisibility(View.GONE);

                        adapter = new ReportsAdapter(getActivity(),taxsum(),rec);
                        adapter.notifyDataSetChanged();
                        lv.setAdapter(adapter);

                        break;
                    case Constants.PROFITREPORT:
                        txttotals.setText(getString(R.string.strsales)+" ("+curr+")");
                        txtsubtotals.setText(getString(R.string.strexpenses)+" ("+curr+")");

                        header.setText(getString(R.string.strtotsales));
                        header.setVisibility(View.GONE);
                        currenttitle = getString(R.string.strtotsales);

                        filter.setVisibility(View.GONE);
                        lb.setVisibility(View.GONE);

                        adapter = new ReportsAdapter(getActivity(),profitReport(),rec);
                        adapter.notifyDataSetChanged();
                        lv.setAdapter(adapter);
                        break;
                    case Constants.PRODUCTREORDER:
                        header.setText(getString(R.string.strreorder));
                        filter.setText(getString(R.string.strproduct));
                        txttotals.setText(getString(R.string.strtotalqty));
                        txtsubtotals.setText(getString(R.string.strminqty));
                        lb.setVisibility(View.GONE);
                        adapter = new ReportsAdapter(getActivity(),reorderlevel(),rec);
                        adapter.notifyDataSetChanged();
                        lv.setAdapter(adapter);
                        break;
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

             final ArrayList<String> cats = Product_Categories.getProductCatsId(getActivity());
             final ArrayList<String> catsname = Product_Categories.getProductCats(getActivity());
             cats.remove(cats.size()-1);
             catsname.remove(catsname.size()-1);

             if(data.getString(Constants.REPORTS).equals(Constants.SALESBYPRODUCT)) {
                spinner.setVisibility(View.VISIBLE);
             }
        mSpinnerAdapter2 = new ArrayAdapter<String>(getActivity(), R.layout.spinnerlayout, catsname);
        mSpinnerAdapter2.setDropDownViewResource(R.layout.filter_view);
        spinner.setAdapter(mSpinnerAdapter2);
        spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                if(position > 0){
                String catnm = catsname.get(position);
                    int pos = catsname.indexOf(catnm);
                    cat = cats.get(pos);
                } else {
                    cat = "";
                }

                adapter = new ReportsAdapter(getActivity(),salesbyproduct(time, cat),rec);
                adapter.notifyDataSetChanged();
                lv.setAdapter(adapter);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
            }
        });

    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {

        inflater.inflate(R.menu.reports, menu);

        mprint =  menu.findItem(R.id.action_print);
        if(data.getString(Constants.REPORTS).equals(Constants.SALESBYPRODUCT) || data.getString(Constants.REPORTS)
                .equals(Constants.SALESBYCUSTOMER) || data.getString(Constants.REPORTS).equals(Constants.TAXBYPRODUCT)
                || data.getString(Constants.REPORTS).equals(Constants.SALEREPORT)) {
            mprint.setVisible(true);
        }

        super.onCreateOptionsMenu(menu, inflater);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if(item.getItemId() == R.id.action_print){
            switch (rec){
                case Constants.SALESBYCUSTOMER:
                    new ExportItems(getActivity(),adapter,Constants.SALESBYCUSTOMER).execute(currenttitle);
                    break;
                case Constants.SALESBYPRODUCT:
                    new ExportItems(getActivity(),adapter,Constants.SALESBYPRODUCT).execute(currenttitle);
                    break;
                case Constants.TAXBYPRODUCT:
                    new ExportItems(getActivity(),adapter,Constants.TAXBYPRODUCT).execute(currenttitle);
                    break;
                case Constants.SALEREPORT:
                    new ExportReports(getActivity(),adapter,Constants.SALEREPORT).execute(getString(R.string.strsalesbytax));
                    break;
            }
            return  true;
        }
        return super.onOptionsItemSelected(item);
    }

    private ArrayList<ReportsItem> salesbusiness(int filter){
        ArrayList<ReportsItem>list = new ArrayList<>();

        double sumtotal = 0;
        String where = null ;
        switch (filter){
            case 0://day
                where =  Sales.TABLENAME+"."+ Sales.ORDERDATE + " >=  date('now')";
                break;
            case 1: //week
                where = Sales.TABLENAME+"."+ Sales.ORDERDATE + " >= date('now','-7 day')";
                break;
            case 2://mon
                where = Sales.ORDERDATE + " <=   date('now','start of month','1 month','-1 day') AND "+
                        Sales.ORDERDATE + " > date('now','start of month','0 month','-1 day')";
                break;
            case 3://mon
                where = " strftime('%Y-%m-%d',"+ Sales.ORDERDATE + ") BETWEEN '"+starttime+"' AND '"+endtime+"'";
                break;
        }

        String q = "SELECT SUM(" + Sales.GRANDTOTAL + ") as " + "totalsales, " +
                "SUM(" + Sales.PAYMENTAMOUNT + ") as totalpay, " +
                Customers.TABLENAME + "." + Customers.BUZINESSNAME + "  FROM " +
                Sales.TABLENAME + " LEFT JOIN " + Customers.TABLENAME + " ON " +
                Sales.TABLENAME + "." + Sales.LOCALCUSTOMER + " = " + Customers.TABLENAME+"."+Customers.ID +
                " WHERE " + where + " GROUP BY "+  Customers.BUZINESSNAME;
        final Cursor c = DatabaseHandler.getInstance(getActivity()). getReadableDatabase().rawQuery(q, null);

        if (c != null) {
            try {
                if (c.moveToFirst()) {
                    do{
                        String bz = c.getString(c.getColumnIndex(Customers.BUZINESSNAME));
                        String tp = c.getString(c.getColumnIndex("totalpay"));
                        String tc = c.getString(c.getColumnIndex("totalsales"));
                        list.add(new ReportsItem(tc,tp,bz));
                        sumtotal += new BigDecimal(tc).setScale(2,RoundingMode.HALF_UP).doubleValue();
                    } while (c.moveToNext());
                }
            } finally {
                c.close();
            }
        }
        txtsum.setText(String.valueOf(sumtotal)+ " " + curr);
        return list;
    }

    private ArrayList<ReportsItem> taxsum(){

        double sumtotal = 0, sumtax = 0;
        ArrayList<ReportsItem>list = new ArrayList<>();

        String where = null;
        if(timerange.length != 0) {
            where = " strftime('%Y-%m-%d',"+ Sales.ORDERDATE + ") BETWEEN '"+starttime+"' AND '"+endtime+"'";

        } else {
            where = Sales.ORDERDATE + " BETWEEN  strftime('%Y %m %d','now') AND " +
                    " date('now','start of month','+1 month','-1 day')";
        }


        String q = "SELECT SUM(" + Sales.TOTALTAX + ") as " + "totaltax, SUM(" + Sales.GRANDTOTAL + ") as " + "totalsales, " +
                Sales.ORDERDATE + " FROM " + Sales.TABLENAME +
                " WHERE " + where + " GROUP BY strftime('%Y-%m-%d',"+ Sales.ORDERDATE + ")" ;
        final Cursor c1 = DatabaseHandler.getInstance(getActivity()).getReadableDatabase().rawQuery(q,  null);
        if (c1 != null) {
            try {
                if (c1.moveToFirst()) {
                    do{
                        String bz = c1.getString(c1.getColumnIndex(Sales.ORDERDATE));
                        String[] bzz = bz.split(" ");
                        String tp = c1.getString(c1.getColumnIndex("totalsales"));
                        String tc = c1.getString(c1.getColumnIndex("totaltax"));
                        list.add(new ReportsItem(tc,tp,bzz[0]));
                        sumtotal += new BigDecimal(tp).setScale(2,RoundingMode.HALF_UP).doubleValue();
                        sumtax += new BigDecimal(tc).setScale(2,RoundingMode.HALF_UP).doubleValue();
                    } while (c1.moveToNext());
                }
            } finally {
                c1.close();
            }
        }
        String tsale = getString(R.string.strtotsales);
        String ttax = getString(R.string.strtotaltax);
        txttotalslable.setVisibility(View.GONE);
        txtsum.setText(ttax +" "+String.valueOf(sumtax)+ " " + curr);
        txtmiddle.setText(tsale +" "+String.valueOf(sumtotal)+ " " + curr);
        return list;
    }

    private ArrayList<ReportsItem> profitReport(){

        double revenue = 0;
        double expense = 0;
        ArrayList<ReportsItem>list = new ArrayList<>();
        String st = Sales.TABLENAME+".";
        String si = SalesItems.TABLENAME+".";
        String pi = PurchaseItems.TABLENAME+".";
        String where = null;
        if(timerange.length != 0) {
            where = st+Sales.SALE_TYPE +" = "+DatabaseUtils.sqlEscapeString(Constants.SALETYPE_SALE)+" AND strftime" +
                    "('%Y-%m-%d',"+ st+Sales.ORDERDATE + ")" + " BETWEEN " + "'"+starttime+"'" + " " + "AND " +
                    "'"+endtime+"'";
        } else {
            where =  st+Sales.SALE_TYPE +" = "+DatabaseUtils.sqlEscapeString(Constants.SALETYPE_SALE)+" AND "+st+Sales
                    .ORDERDATE + " >=  date('now') " ;
        }

        String q = "SELECT revenue, sum(expenses) as expenses FROM ( SELECT "+st+Sales.ID+", "+st+Sales.GRANDTOTAL +
                " as revenue , ("+si+SalesItems
                .PRODUCTQTY+"*" +pi+PurchaseItems.UNITPRICE+") as expenses  FROM " + Sales.TABLENAME + " LEFT JOIN "
                +SalesItems.TABLENAME +" ON "+st+Sales.ID +" = "+si+SalesItems.ORDERID +" LEFT JOIN  "+PurchaseItems
                .TABLENAME +" ON "+si+SalesItems.LOCALPRODUCTNO + " = "+ pi+PurchaseItems.PRODUCT + " WHERE " + where
                + " GROUP BY "+ si+SalesItems.ID +") as "+Sales.TABLENAME+" GROUP BY "+st+Sales.ID ;

        final Cursor c = DatabaseHandler.getInstance(getActivity()).getReadableDatabase().rawQuery(q, null);
        if (c != null) {
            try {
                String tc = "0";
                String bz = "0";
                if (c.moveToFirst()) {
                    do{
                        bz = c.getString(c.getColumnIndex("revenue"));
                        tc  = c.getString(c.getColumnIndex("expenses"));
                        list.add(new ReportsItem(tc,"",bz));
                        revenue += new BigDecimal(bz).setScale(2,RoundingMode.HALF_UP).doubleValue();
                        expense += new BigDecimal(tc).setScale(2,RoundingMode.HALF_UP).doubleValue();
                    } while (c.moveToNext());
                }
            } finally {
                c.close();
            }
        }
        String rev = getString(R.string.strtotsales);
        String exp = getString(R.string.strtotaexpenses);
        String prf = getString(R.string.strprofit);
        double prt = new BigDecimal(revenue).subtract(new BigDecimal(expense)).setScale(2,RoundingMode.HALF_UP)
                .doubleValue();
        txttotalslable.setText(rev + " " + String.valueOf(revenue) + " " + curr);
        txtsum.setText(exp +" "+String.valueOf(expense)+ " " + curr);
        txtmiddle.setText(prf +" "+String.valueOf(prt)+ " " + curr);
        return list;
    }

    private ArrayList<ReportsItem> salesbyproduct(int filter, String cat){
//        cat = Product_Categories.getLocId(cat);
        ArrayList<ReportsItem>list = new ArrayList<>();

        String where = null ;

        switch (filter){
            case 0://day
                where =  Sales.TABLENAME+"."+ Sales.ORDERDATE + " >=  date('now') " ;
                break;
            case 1: //week
                where = Sales.TABLENAME+"."+ Sales.ORDERDATE + " >= date('now','-7 day') ";
                break;
            case 2://mon
                where = Sales.TABLENAME + "." + Sales.ORDERDATE + " <=  date('now','start of month','1 month','-1 day') AND "+
                        Sales.TABLENAME + "." + Sales.ORDERDATE + " > date('now','start of month','0 month','-1 day')";
                break;
            case 3:
                where = " strftime('%Y-%m-%d',"+ Sales.ORDERDATE + ") BETWEEN '"+starttime+"' AND '"+endtime+"'";
                break;
        }

        if(Validating.areSet(cat))
            where = where.concat(" AND " + Products.TABLENAME+"."+Products.CATEGORY +" LIKE '%" + cat + "%\'" );

        double sumtotal = 0;
        String q = "SELECT SUM(" + SalesItems.TABLENAME + "." + SalesItems.SUBTOTAL + ") as subtotal, "+
                SalesItems.TABLENAME + "." + SalesItems.PRODUCTQTY + ", "+
                "SUM(" + SalesItems.TABLENAME + "." + SalesItems.PRODUCTQTY + ") as totalqty, " +
                Products.TABLENAME + "." + Products.PRODUCTNAME + "  FROM " +
                SalesItems.TABLENAME +" LEFT JOIN " + Products.TABLENAME + " ON " + SalesItems.TABLENAME + "." + SalesItems
                .LOCALPRODUCTNO + "  =  " + Products.TABLENAME + "." + Products.ID + " LEFT JOIN " + Sales
                .TABLENAME + " ON " + SalesItems.TABLENAME + "." + SalesItems
                .ORDERID + "  =  " + Sales.TABLENAME + "." + Sales.ID +
                " WHERE " + where + " GROUP BY " + Products.TABLENAME + "." + Products.PRODUCTNAME;


        final Cursor c2 = DatabaseHandler.getInstance(getActivity()). getReadableDatabase(). rawQuery(q, null);

        if (c2 != null) {
            try {
                if (c2.moveToFirst()) {
                    do{
                        String bz = c2.getString(c2.getColumnIndex(Products.PRODUCTNAME));
                        String tpr = c2.getString(c2.getColumnIndex("subtotal"));
                        String tc = c2.getString(c2.getColumnIndex("totalqty"));

                        double tot = new BigDecimal(tpr).doubleValue();
                        /*double taxamount = Constants.calculateTax(Double.parseDouble(tp),Double.parseDouble(tx));
                        double grandtotal = Constants.calculate_grandtotal(Double.parseDouble(tp), Integer.parseInt(qty));
                        if(txsetting == 1){//taxable
                             tot = grandtotal;
                        } else {
                            tot = Constants.totalsnotax(grandtotal,taxamount);
                        }*/

                        tpr = new BigDecimal(tot).toString();
                        list.add(new ReportsItem(tc,tpr,bz));
                        sumtotal += new BigDecimal(tot).doubleValue();
                    } while (c2.moveToNext());
                }
            } finally {
                c2.close();
            }
        }
        txtsum.setText(String.valueOf(sumtotal)+ " " + curr);
        return list;
    }


    private ArrayList<ReportsItem> reorderlevel(){
        ArrayList<ReportsItem>list = new ArrayList<>();

        String where = Products.ITEMS_ON_HAND +" <= "+Products.REODER + " AND "+Products.SYNCSTATUS + " != 3";
        ContentResolver cr = getActivity().getContentResolver();
        Cursor c3 = cr.query(Products.BASEURI, null, where, null, Products.PRODUCTNAME + " ASC");

        if (c3 != null) {
            try {
                if (c3.moveToFirst()) {
                    do{
                        String bz = c3.getString(c3.getColumnIndex(Products.PRODUCTNAME));
                        String tp = c3.getString(c3.getColumnIndex(Products.ITEMS_ON_HAND));
                        if(tp.isEmpty())
                            tp = "0";
                        String tc = c3.getString(c3.getColumnIndex(Products.REODER));
                        list.add(new ReportsItem(tp,tc,bz));
                    } while (c3.moveToNext());
                }
            } finally {
                c3.close();
            }
        }
        return list;
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.etstartdate:
                Constants.setDateTime(getActivity(), etst);
                break;
            case R.id.etendtdate:
                Constants.setDateTime(getActivity(), etend);
                break;
        }
    }
    public List<ReportsItem> getList(String start,String end) {
        ContentResolver cr = getActivity().getContentResolver();
        Cursor c = null;
        if(!start.isEmpty() && !end.isEmpty()){
            String where = Sales.ORDERDATE +" >= "+start + " AND "+ end +" <= "+end +" AND "+Sales.SALE_TYPE +" = " +
                    ""+DatabaseUtils.sqlEscapeString(Constants.SALETYPE_SALE);
            c= cr.query(Sales.BASEURI, null, where, null, Sales.ID + " DESC");
        } else {
            String where = Sales.SALE_TYPE +" = " +
                    ""+DatabaseUtils.sqlEscapeString(Constants.SALETYPE_SALE);
            c= cr.query(Sales.BASEURI, null, where, null, Sales.ID + " DESC");
        }
        List<ReportsItem> list = new ArrayList<ReportsItem>();
        try {
            if (c.getCount() > 0) {
                c.moveToFirst();
                do {
                    int colN = c.getColumnIndex(Sales.PAYMENTAMOUNT);
                    int colP = c.getColumnIndex(Sales.LOCALCUSTOMER);
                    int colI = c.getColumnIndex(Sales.ID);
                    int colD = c.getColumnIndex(Sales.ORDERDATE);
                    int colT = c.getColumnIndex(Sales.GRANDTOTAL);
                    int colTx = c.getColumnIndex(Sales.TOTALTAX);
                    String n = c.getString(colN);
                    String p = c.getString(colP);
                    String d = c.getString(colD);
                    String t = c.getString(colT);
                    String tx = c.getString(colTx);
                    String bname = Customers.getCustomerName(getActivity(), Long.parseLong(p));
                    long i = c.getLong(colI);
                    String business = bname.toUpperCase(Locale.getDefault());
                    String totalsales =  t;
                    String paidamount =  n;
                    String orderdate = d;
                    String totaltx = tx;
                    list.add(new ReportsItem(totalsales,paidamount,business,orderdate,totaltx,i));
                } while (c.moveToNext());
            } else {
//                list.add(new ReportsItem());
            }
        } finally {
            if (c != null) {
                c.close();
            }
        }
        return list;
    }

    public List<ReportsItem> getOrderList(String start,String end) {
        ContentResolver cr = getActivity().getContentResolver();
        Cursor c = null;
        if(!start.isEmpty() && !end.isEmpty()){
            String where = Sales.ORDERDATE +" >= "+start + " AND "+ end +" <= "+end +" AND "+Sales.SALE_TYPE +" = " +
                    ""+DatabaseUtils.sqlEscapeString(Constants.SALETYPE_ORDER);
            c= cr.query(Sales.BASEURI, null, where, null, Sales.ID + " DESC");
        } else {
            String where = Sales.SALE_TYPE +" = " +
                    DatabaseUtils.sqlEscapeString(Constants.SALETYPE_ORDER);
            c= cr.query(Sales.BASEURI, null, where, null, Sales.ID + " DESC");
        }
        List<ReportsItem> list = new ArrayList<ReportsItem>();
        try {
            if (c.getCount() > 0) {
                c.moveToFirst();
                do {
                    int colN = c.getColumnIndex(Sales.PAYMENTAMOUNT);
                    int colP = c.getColumnIndex(Sales.LOCALCUSTOMER);
                    int colI = c.getColumnIndex(Sales.ID);
                    int colD = c.getColumnIndex(Sales.ORDERDATE);
                    int colT = c.getColumnIndex(Sales.GRANDTOTAL);
                    int colTx = c.getColumnIndex(Sales.TOTALTAX);
                    String n = c.getString(colN);
                    String p = c.getString(colP);
                    String d = c.getString(colD);
                    String t = c.getString(colT);
                    String tx = c.getString(colTx);
                    String bname = Customers.getCustomerName(getActivity(), Long.parseLong(p));
                    long i = c.getLong(colI);
                    String business = bname.toUpperCase(Locale.getDefault());
                    String totalsales =  t;
                    String paidamount =  n;
                    String orderdate = d;
                    String totaltx = tx;
                    list.add(new ReportsItem(totalsales,paidamount,business,orderdate,totaltx,i));
                } while (c.moveToNext());
            } else {
//                list.add(new ReportsItem());
            }
        } finally {
            if (c != null) {
                c.close();
            }
        }
        return list;
    }


    public List<ReportsItem> getStationList(String start,String end) {
        ContentResolver cr = getActivity().getContentResolver();
        Cursor c = null;
        int userid = appPreference.getUserId();
        if(!start.isEmpty() && !end.isEmpty()){
            String where = Station.CREATEDON +" >= "+start + " AND "+ Station.CREATEDON +" <= "+end + " AND "+
                    Station.CREATEDBY + " = "+userid;
            c= cr.query(Station.BASEURI, null, where, null, Station.ID + " DESC");
        } else {
//            String where =  Station.CREATEDBY + " = "+userid;
            c= cr.query(Station.BASEURI, null, null, null, Station.ID + " DESC");
        }

        List<ReportsItem> list = new ArrayList<ReportsItem>();
        try {
            if (c.getCount() > 0) {
                c.moveToFirst();
                do {
                    long _id_ = c.getLong(c.getColumnIndex(Station.ID));
                    String user = c.getString(c.getColumnIndex(Station.CREATEDBY));
                    String op = c.getString(c.getColumnIndex(Station.OPERATOR));
                    String st = c.getString(c.getColumnIndex(Station.STATIONNAME));
                    String mi = c.getString(c.getColumnIndex(Station.METERIN));
                    String mo = c.getString(c.getColumnIndex(Station.METEROUT));
                    String ci = c.getString(c.getColumnIndex(Station.CASHIN));
                    String co = c.getString(c.getColumnIndex(Station.CASHOUT));
                    String ti = c.getString(c.getColumnIndex(Station.TIMEIN));
                    String to = c.getString(c.getColumnIndex(Station.TIMEOUT));
                    String cron = c.getString(c.getColumnIndex(Station.CREATEDON));
                    String upd = c.getString(c.getColumnIndex(Station.UPDATEDON));
                    String nt = c.getString(c.getColumnIndex(Station.COMMENT));
                    String pht = c.getString(c.getColumnIndex(Station.SIGNATURE));
                    String pmp = StationPump.getPump(getActivity(), _id_);
                    int ack = c.getInt(c.getColumnIndex(Station.ACK));

                    list.add(new ReportsItem(ack,_id_,cron,upd,nt,user,pht,to,ti,co,ci,mo,mi,op,st,pmp));
                } while (c.moveToNext());
            }
        } finally {
            if (c != null) {
                c.close();
            }
        }
        return list;
    }


    public List<ReportsItem> getDelList(String start, String end) {
        ContentResolver cr = getActivity().getContentResolver();
        Cursor c = null;
        if(!start.isEmpty() && !end.isEmpty()){
            String where = Delivery.DELIVERYDATE +" >= "+start + " AND "+ Delivery.DELIVERYDATE +" <= "+end+
                    "AND "+Delivery.DELIVERYSTATUS + " != "+ DatabaseUtils.sqlEscapeString(Constants.SALE_STATUS_PNDG);
            c= cr.query(Delivery.BASEURI, null, where, null, Delivery.ID + " DESC");
        } else {
            String where = Delivery.DELIVERYSTATUS + " != "+ DatabaseUtils.sqlEscapeString(Constants.SALE_STATUS_PNDG);
            c= cr.query(Delivery.BASEURI, null, where, null, Delivery.ID + " DESC");
        }
        List<ReportsItem> list = new ArrayList<ReportsItem>();
        try {
            if (c.getCount() > 0) {
                c.moveToFirst();
                do {
                    int colN = c.getColumnIndex(Delivery.SALE_REFERENCE);
                    int colP = c.getColumnIndex(Delivery.REFRENCENO);
                    int colI = c.getColumnIndex(Delivery.ID);
                    int colSL = c.getColumnIndex(Delivery.LOCALSALE);
                    int colD = c.getColumnIndex(Delivery.DELIVERYDATE);
                    int colPr = c.getColumnIndex(Delivery.LOCALCUSTOMER);
                    int colUp = c.getColumnIndex(Delivery.ADDRESS);
                    String sref = c.getString(colN);
                    String drefno = c.getString(colP);
                    long i = c.getLong(colI);
                    long si = c.getLong(colSL);
                    String deldate = c.getString(colD);
                    String cs = c.getString(colPr);
                    String customer = Customers.getCustomerBusinessNameLoca(getActivity(), cs);
                    String address = c.getString(colUp);
                    list.add(new ReportsItem(sref,drefno,customer,deldate,address,i));
                } while (c.moveToNext());
            } else {
//                list.add(new ReportsItem());
            }
        } finally {
            if (c != null) {
                c.close();
            }
        }
        return list;
    }


    public void showOrders(final long id, final String businessname) {
        LayoutInflater infl = getActivity().getLayoutInflater();
        View view = infl.inflate(R.layout.orderitemslist, null);

        final Dialog dialog = new Dialog(getActivity(), R.style.CustomDialog);
        dialog.setCancelable(true);
        dialog.setContentView(view);
        ListView listView = (ListView) dialog.findViewById(R.id.list);
        salesitemsadapter = new ItemListAdapterOrderItems(getActivity(),SalesItems.getItems(dialog.getContext(),id));
        listView.setAdapter(salesitemsadapter);
        TextView txt = (TextView) dialog.findViewById(R.id.txttitle);
        txt.setText(businessname);
        actionButton = (FloatingActionButton)dialog.findViewById(R.id.actionbutton);

        actionButton.setBackgroundColor(getResources().getColor(R.color.actionbar));
        actionButton.setImageResource(R.drawable.ic_printer);


        ImageView imageView = (ImageView) dialog.findViewById(R.id.goback);
        imageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });

        Button btn = (Button) dialog.findViewById(R.id.btnprint);
        int printed = Sales.getPrintStatus(dialog.getContext(), id);
        if (printed == 0) {
            btn.setVisibility(View.GONE);
            actionButton.setVisibility(View.VISIBLE);

            actionButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
//                    ArrayList<OrderItemList> listarray = SalesItems.getItemsArrayList(dialog.getContext(), id);
                    ArrayList<SalesItems> listarray = SalesItems.getSaleItems(dialog.getContext(), id);
                    if (!listarray.isEmpty()) {
                        int print = 0;

                        int orderid = Integer.parseInt(String.valueOf(id));
                        String customerid = Sales.getCustomerLocal(dialog.getContext(), String.valueOf(id));

                        Boolean isSDPresent = android.os.Environment.getExternalStorageState().equals(android.os.Environment.MEDIA_MOUNTED);
                        if(isSDPresent){
//                            dialog.dismiss();
                            int send = Constants.writeReceipt(getActivity(), listarray, Settings.getTaxEnabled
                                    (getActivity()),customerid);
//                            int send = Constants.writeReceipt(getActivity(), listarray, Settings.getTaxEnabled(getActivity()));
                            startActivity(getActivity().getPackageManager().getLaunchIntentForPackage("app.incotex.fpd"));
                            getActivity().setResult(-1, new Intent());

                            Sales.putPrintStatus(getActivity(), new int[]{orderid, send});
//                            getActivity().finish();
                        } else {
                            Toast.makeText(getActivity(),"you don't have external memory",Toast.LENGTH_LONG).show();
                        }


                    }
                }
            });
        } else {
            if(txt.getVisibility() == View.GONE)
                txt.setVisibility(View.VISIBLE);
            actionButton.setVisibility(View.GONE);
        }
        dialog.show();
    }


    public void showStation(String[] info) {
        StationFragment newFragment = new StationFragment();
        Bundle args = new Bundle();
        args.putInt(Dashboard.ARG_SECTION_NUMBER, getArguments().getInt(Dashboard.ARG_SECTION_NUMBER));
        args.putString(Constants.REPORTS, Constants.STATIONHISTORY);
        args.putStringArray("info", info);
        newFragment.setArguments(args);

        FragmentTransaction transaction = getFragmentManager().beginTransaction();
        transaction.replace(R.id.content_frame, newFragment);
        transaction.addToBackStack(null);
        transaction.commit();
    }


    public void showDelivery(final long id, final String businessname) {
        LayoutInflater infl = getActivity().getLayoutInflater();
        View view = infl.inflate(R.layout.deliverylist, null);

        final Dialog dialog = new Dialog(getActivity(), R.style.CustomDialog);
        dialog.setCancelable(true);
        dialog.setContentView(view);
        ListView listView = (ListView) dialog.findViewById(R.id.list);
        ItemListAdapterOrderItems salesitemsadapter = new ItemListAdapterOrderItems(getActivity(), SalesItems
                .getItems(dialog.getContext(), id));
        listView.setAdapter(salesitemsadapter);
        TextView txt = (TextView) dialog.findViewById(R.id.txttitle);
        txt.setText(businessname);
        actionButton = (FloatingActionButton)dialog.findViewById(R.id.actionbutton);

        actionButton.setBackgroundColor(getResources().getColor(R.color.actionbar));
        actionButton.setImageResource(R.drawable.ic_action_send);
        actionButton.setVisibility(View.GONE);

        ImageView imageView = (ImageView) dialog.findViewById(R.id.goback);
        imageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });
        TextView txtbusiness,txtselref,txtdeldate,txtnote;
        txtbusiness = (TextView)dialog.findViewById(R.id.txtdelcust);
        txtdeldate = (TextView)dialog.findViewById(R.id.txtdeldate);
        txtselref = (TextView)dialog.findViewById(R.id.txtsaleref);
        txtnote = (TextView)dialog.findViewById(R.id.txtcomment);
        ArrayList<String> delist = Delivery.deliveryDetails(dialog.getContext(), id);
        if(delist.size() >0){
            txtselref.setText(getString(R.string.strsaleref).toUpperCase(Locale.getDefault()).concat(" : "+delist.get(2)));
            txtdeldate.setText(getString(R.string.strdeldate).toUpperCase(Locale.getDefault()).concat(" : "+delist.get(1)));
            txtbusiness.setText(getString(R.string.straddress).toUpperCase(Locale.getDefault()).concat(" : "+delist.get(6)));
            txtnote.setText(getString(R.string.strnotes).toUpperCase(Locale.getDefault()).concat(" : " + Html.fromHtml(delist.get(7)))
            );
        }

        actionButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                List<SalesItems> listarray = SalesItems.getItems(dialog.getContext(), id);
                if (listarray != null && listarray.size() > 0) {
                    process_delivery(id,listarray,dialog);
                }
            }

            private void process_delivery(long id, List<SalesItems> listarray, Dialog __d) {
                LayoutInflater infl = getActivity().getLayoutInflater();
                View view = infl.inflate(R.layout.delivery, null);

                final Dialog dg = new Dialog(getActivity(), R.style.CustomDialog);
                dg.setCancelable(true);
                dg.setContentView(view);


                actionButton = (FloatingActionButton)dg.findViewById(R.id.actionbutton);

                actionButton.setBackgroundColor(getResources().getColor(R.color.actionbar));
                actionButton.setImageResource(R.drawable.ic_action_send);

                ListView listView = (ListView) dg.findViewById(R.id.list);
                DeliveringAdapter _adapter = new DeliveringAdapter(dg.getContext(),listarray,actionButton,new
                        Dialog[]{__d,dg},id);
                listView.setAdapter(_adapter);

                ImageView imageView = (ImageView) dg.findViewById(R.id.goback);
                imageView.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        dg.dismiss();
                    }
                });
                dg.show();
            }
        });

        dialog.show();
    }
}