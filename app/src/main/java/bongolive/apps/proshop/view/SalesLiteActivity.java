/*
 * The source codes in this project are owned by Bongolive Enterprises. Copying or any kind of usage without Bongolive's approval is against the law.
 * Copyright (c) 2015.
 *
 */


package bongolive.apps.proshop.view;


import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.graphics.Color;
import android.os.Build;
import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.Log;
import android.view.ContextMenu;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.AbsListView;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.SpinnerAdapter;
import android.widget.TextView;
import android.widget.Toast;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.ArrayList;
import java.util.Locale;

import bongolive.apps.proshop.R;
import bongolive.apps.proshop.controller.AppPreference;
import bongolive.apps.proshop.controller.SalesItemAdapter;
import bongolive.apps.proshop.controller.SalesSummaryAdapter;
import bongolive.apps.proshop.controller.Validating;
import bongolive.apps.proshop.model.Customers;
import bongolive.apps.proshop.model.Product_Categories;
import bongolive.apps.proshop.model.Products;
import bongolive.apps.proshop.model.Sales;
import bongolive.apps.proshop.model.SalesItems;
import bongolive.apps.proshop.model.Settings;
import bongolive.apps.proshop.model.Warehouse;
import bongolive.apps.proshop.controller.Constants;
import bongolive.apps.proshop.controller.GpsTracker;

public class SalesLiteActivity extends AppCompatActivity implements View.OnCreateContextMenuListener,View.OnKeyListener,
        TextWatcher {

    private String customeridLocal = "";
    private String customerid = "";/*customerid captures the customer id*/

    private TextView txreportdata;
    private ListView lvitems;
    private AutoCompleteTextView edclientname;
    private AutoCompleteTextView etproduct;
    private EditText etquantity;
    private EditText etprice;
    private EditText etpartial;
    private EditText etdiscount;
    private Dialog dialogmode;
    private AppPreference appPreference;
    private boolean tabletSize = false;

    private ArrayList<String> productlist;
    private ArrayList<String> productcodelist;
    private TextView txtorderitem;
    private int  payinfull = 0;
    private boolean discountable = false;

    private String whname = "";
    private String whid = "";
    private ArrayList<String> whlistid;
    private ArrayList<SalesItems> salesItems = new ArrayList<>();
    private SalesItemAdapter adapter ;
    private LinearLayout layprod;
    private LinearLayout layqty;
    private LinearLayout layrep;
    private LinearLayout laycust;
    private ImageButton imgaddcust ;
    private View topb;
    private View lowb;
    private LinearLayout rdiscount;
    private ActionBar ab;
    private boolean business = false;
    /*
    * new strings
    */
    private String __prodcode = "";
    private String __prodname = "";
    private boolean __taxsettings = false;
    private GpsTracker gpsTracker;
    String curr;
    /*
    * end of new strings
    */


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        tabletSize = getResources().getBoolean(R.bool.isTablet);
        if (!tabletSize) {
            setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        }
        setContentView(R.layout.addsales);

        appPreference = new AppPreference(this.getApplicationContext());
        appPreference.setShowPrice(true);
        __taxsettings = Settings.getTaxEnabled(this);
        gpsTracker = new GpsTracker(this);

        curr = appPreference.getDefaultCurrency();
        curr = curr != null ? curr : "Tsh";

        createSalesForm();


        ab = getSupportActionBar();
        ab.setTitle(getString(R.string.straddsale));
        ab.setDisplayUseLogoEnabled(true);
        ab.setDisplayHomeAsUpEnabled(true);
        ab.setBackgroundDrawable(getResources().getDrawable(R.drawable.ab_background_textured_prostockactionbar));
    }

    private void createSalesForm() {
        lvitems = (ListView) findViewById(R.id.orderlist);
        adapter = new SalesItemAdapter(this, salesItems);
        lvitems.setAdapter(adapter);

        lvitems.setOnScrollListener(new AbsListView.OnScrollListener() {
            private int mLastFirstVisibleItem;

            @Override
            public void onScrollStateChanged(AbsListView view, int scrollState) {

            }

            @Override
            public void onScroll(AbsListView view, int firstVisibleItem,
                                 int visibleItemCount, int totalItemCount) {

                if (mLastFirstVisibleItem < firstVisibleItem) {
                    Log.i("SCROLLING DOWN", "TRUE");
                }
                if (mLastFirstVisibleItem > firstVisibleItem) {
                    Log.i("SCROLLING UP", "TRUE");
                }
                mLastFirstVisibleItem = firstVisibleItem;

            }

        });

        lvitems.setOnCreateContextMenuListener(new View.OnCreateContextMenuListener() {

            @Override
            public void onCreateContextMenu(ContextMenu menu, View view,
                                            ContextMenu.ContextMenuInfo menuInfo) {
                getMenuInflater().inflate(R.menu.removesale, menu);
                menu.setHeaderTitle(getResources().getString(R.string.strremovesale));
                menu.setHeaderIcon(R.drawable.ic_done_all_white_24dp);
                AdapterView.AdapterContextMenuInfo info = (AdapterView.AdapterContextMenuInfo) menuInfo;
                int position = info.position;
                SalesItems list = salesItems.get(position);
                Toast.makeText(SalesLiteActivity.this, "ok " + list.getPrice(), Toast.LENGTH_LONG).show();
                double sales = list.getPrice();
                String total = txreportdata.getText().toString();
                if (Validating.areSet(total)) {
                    double remained = Double.parseDouble(total);
                    double rem = remained - sales;
                    txreportdata.setText(String.valueOf(rem));
                }
                salesItems.remove(position);
                adapter.notifyDataSetChanged();
                txtorderitem.setText(getString(R.string.straddeditem) + " (" + salesItems.size() + ")");
            }
        });
        String orderstatus = " (" + salesItems.size() + ")";
        txtorderitem = (TextView) findViewById(R.id.txtlabel);
        txtorderitem.append(orderstatus);
        rdiscount = (LinearLayout) findViewById(R.id.rddiscount);
        rdiscount.setVisibility(View.VISIBLE);

        etdiscount = (EditText) findViewById(R.id.etdiscount);
        CheckBox chkdiscount = (CheckBox) findViewById(R.id.chkdiscount);
        chkdiscount.setVisibility(View.GONE);
        etdiscount.setHint(getString(R.string.strunitprices));
        etdiscount.setVisibility(View.VISIBLE);

        layprod = (LinearLayout) findViewById(R.id.layproduct);
        layqty = (LinearLayout) findViewById(R.id.lyqty);
        layrep = (LinearLayout) findViewById(R.id.layreport);
        laycust = (LinearLayout) findViewById(R.id.laycustomer);
        imgaddcust = (ImageButton)findViewById(R.id.imgaddcustmer);
        imgaddcust.setVisibility(View.GONE);
        imgaddcust.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                addcustomer();
            }
        });
        topb = findViewById(R.id.topborder);
        lowb = (View) findViewById(R.id.lowerborder);

        String[] businesslist = Customers.getAllCustomers(this);
        productlist = Products.getAllProducts(this);
        productcodelist = Products.getAllProductsCode(this);

        ArrayAdapter<String> proadapter = new ArrayAdapter<>(this, android.R.layout.select_dialog_item, productlist);

        etproduct = (AutoCompleteTextView) findViewById(R.id.autoproduct00);
        txreportdata = (TextView) findViewById(R.id.txtreportdata);

        edclientname = (AutoCompleteTextView) findViewById(R.id.autotxtsaleclientname);

        String bznm = Customers.getDefaultCustomer(this);
        customerid = String.valueOf(Customers.getCustomerid(SalesLiteActivity.this, bznm));
        customeridLocal = String.valueOf(Customers.getCustomerlocalId(SalesLiteActivity.this, bznm));
        business = true;

        if (!business) {
            layprod.setVisibility(View.GONE);
            layqty.setVisibility(View.GONE);
            layrep.setVisibility(View.GONE);
            rdiscount.setVisibility(View.GONE);
            topb.setVisibility(View.GONE);
            lowb.setVisibility(View.GONE);
            lvitems.setVisibility(View.GONE);
            if (tabletSize) {
                System.out.println(" tablet");
                topb.setVisibility(View.VISIBLE);
                lowb.setVisibility(View.VISIBLE);
                lvitems.setVisibility(View.VISIBLE);
                layrep.setVisibility(View.INVISIBLE);
            }
        } else {
            edclientname.setText("");
            laycust.setVisibility(View.GONE);
            layprod.setVisibility(View.VISIBLE);
            layqty.setVisibility(View.VISIBLE);
            layrep.setVisibility(View.VISIBLE);

            lvitems.setVisibility(View.VISIBLE);
            topb.setVisibility(View.VISIBLE);
            lowb.setVisibility(View.VISIBLE);

            this.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
        }

        etdiscount.setOnKeyListener(this);

        etproduct.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if (!hasFocus) {
                    if (business) {
                        String productname = etproduct.getText().toString();
                        if (TextUtils.isEmpty(productname)) {
                            etproduct.clearFocus();
                            Toast.makeText(SalesLiteActivity.this, getResources().getString(R.string.strfilldata), Toast
                                    .LENGTH_LONG).show();
                        }
                        if (Validating.areSet(productname)) {
                            __prodcode = Products.getProductIdUsingSku(SalesLiteActivity.this, productname);
                            System.out.println(" product code is " + __prodcode);
                            double pr = Products.getUnitPrice(SalesLiteActivity.this, __prodcode);
                            String sku = Products.getSku(SalesLiteActivity.this, __prodcode);
                            etprice.setText(getString(R.string.strprice) + " " + pr);
                            ab.setSubtitle(getString(R.string.strsku2) + " " + sku);
                        }
                    }
//                    start = false;
                }
            }
        });
        etproduct.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                String pro = (String) parent.getItemAtPosition(position);
                etproduct.setText(pro);
                __prodname = pro;
                whid = Warehouse.getDefaultId(SalesLiteActivity.this);

                int pos = productlist.indexOf(pro);
                __prodcode = productcodelist.get(pos);
                double pr = Products.getUnitPrice(SalesLiteActivity.this, __prodcode);
                String sku = Products.getSku(SalesLiteActivity.this, __prodcode);
                ab.setSubtitle(getString(R.string.strsku2) + " " + sku);
                etprice.setText(getString(R.string.strprice) + " " + pr);
                etproduct.clearFocus();
            }
        });

        etproduct.setAdapter(proadapter);
        etproduct.setThreshold(1);


        Spinner spwarehouse = (Spinner) findViewById(R.id.spwarehouse);
        ArrayList<String> whlist = Warehouse.getAll(this);
        whlistid = Warehouse.getAllIds(this);

        SpinnerAdapter spwhadapter = new ArrayAdapter<>(this, android.R.layout.simple_list_item_1, whlist);
        spwarehouse.setAdapter(spwhadapter);

        if (Warehouse.getCount(this) > 1) {
            String wh = (String) spwhadapter.getItem(1);
            whid = whlistid.get(1);
            System.out.println("wh is " + wh);
            if (!wh.equals(null)) {
                spwarehouse.setSelection(1);
            }
        } else if (Warehouse.getCount(this) > 0) {
            String wh = (String) spwhadapter.getItem(1);
            whid = whlistid.get(1);
            System.out.println("wh is " + wh);
            if (!wh.equals(null)) {
                spwarehouse.setSelection(1);
            }
        }
        spwarehouse.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                whname = "";
                whid = "";
                if (position > 0) {
                    whname = (String) parent.getItemAtPosition(position);
                    whid = whlistid.get(position);
                    System.out.println("whname is " + whname + " whid is " + whid);
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
        spwarehouse.setVisibility(View.GONE);
        etquantity = (EditText) findViewById(R.id.autoquantity00);

        etquantity.setOnFocusChangeListener(new View.OnFocusChangeListener() {

            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if (hasFocus) {
                    if(!Validating.areSet(__prodcode)){
                        addproduct();
                    } else{
                        String prc = etdiscount.getText().toString().trim();
                        double pr = Products.getUnitPrice(SalesLiteActivity.this, __prodcode);
                        if(Validating.areSet(prc)){
                            double prn = new BigDecimal(prc).setScale(2,RoundingMode.HALF_UP).doubleValue();
                            if(pr != prn)
                                addproduct();
                        }
                    }
                }
            }
        });
        etquantity.setOnKeyListener(this);


        etprice = (EditText) findViewById(R.id.autoprice00);

        etprice.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if (hasFocus) {
                    process_item();
                }
            }
        });

        edclientname.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                String name = (String) parent.getItemAtPosition(position);
                edclientname.setText(name);
                customerid = String.valueOf(Customers.getCustomerid(SalesLiteActivity.this, name));
                customeridLocal = String.valueOf(Customers.getCustomerlocalId(SalesLiteActivity.this, name));
            }
        });

        edclientname.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if(start ==1 ){
                    System.out.println("no customer");
                }
            }

            @Override
            public void afterTextChanged(Editable s) {
                if(s.length() == 0){
//                    Toast.makeText(SalesActivity.this, getString(R.string.straddcustomer)+"?",Toast.LENGTH_SHORT).show();
                }
            }
        });

        if(Customers.getCustomerCount(this) >= 1){
            String name = businesslist[0];

            edclientname.setText(name);
            customerid = String.valueOf(Customers.getCustomerid(SalesLiteActivity.this, name));
            customeridLocal = String.valueOf(Customers.getCustomerlocalId(SalesLiteActivity.this, name));

            edclientname = (AutoCompleteTextView) findViewById(R.id.autotxtsaleclientname);
        }

        edclientname.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (Validating.areSet(edclientname.getText().toString())) {
                    edclientname.setText("");
                }
            }
        });
        etproduct.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (Validating.areSet(etproduct.getText().toString())) {
                    etproduct.setText("");
                    etprice.setText("");
                    etquantity.setText("");
                    __prodcode = "";
                    getSupportActionBar().setSubtitle("");
                }
            }
        });
    }

    private void process_item() {
        System.out.println("whid " + whid);
        if(whid.equals("") || whid.isEmpty() || whid.equals(getString(R.string.strselectwarehouse)) || whid.equals(0))
        {
            whid = Warehouse.getDefaultId(this);
        }
        System.out.println("now whid " + whid);
        String qq = etquantity.getText().toString().trim();
        if(!Constants.checkDouble(this, qq, etquantity))
            return;

        String prd = etproduct.getText().toString();
        if(!prd.isEmpty()){
            int prese = Products.isProductPresent(this, new String[]{prd});
            if(prese == 0 ){
                etproduct.setText("");
                __prodcode = "";
                __prodname = "";
                etproduct.setHint(getString(R.string.strinvalidproductwarn));
                etproduct.setHintTextColor(Color.RED);
                etproduct.requestFocus();
                return;
            }
        }

        String p = __prodcode;
        etprice.setText("");
        if (Validating.areSet(p, qq)) {
            etproduct.setHintTextColor(Color.GRAY);
            etproduct.setHint(getString(R.string.strproductname));
            int qa = Products.getAllProductQuantity(SalesLiteActivity.this, p);
            int q =  new BigDecimal(qq).intValue();
            int taxmethod = Products.getTaxmethod(SalesLiteActivity.this, p);

                double tx = Products.getTax(SalesLiteActivity.this, p);
                double pr = Products.getUnitPrice(SalesLiteActivity.this, p);
                double maxdis = Settings.getDiscountLimit(this);
                //proceed now we have quantity and product code
                //check if it is discountable
                double taxamount = 0;
                double netsaleprice = 0;
                double saleprice = 0;
                double subtotal = 0;
                long prodlocal = Products.getProdid(this, p);
                String prod = Products.getProdid(this,prodlocal);
                if(prodlocal == 0)
                    return;
                long i = 0;
                if (!salesItems.isEmpty()) {
                    i = adapter.getCount() + 1;
                } else {
                    i = 1;
                }
                    double tax = Constants.itemtax(pr,tx,taxmethod,__taxsettings);
                    netsaleprice = Constants.netprice(pr, tx, taxmethod, __taxsettings);
                    saleprice = Constants.saleprice(pr, tax, taxmethod, __taxsettings);

                    subtotal = Constants.calculate_grandtotal(saleprice,q);
                    double netsubtotal = Constants.calculate_grandtotal(netsaleprice,q);
                    taxamount = new BigDecimal(subtotal).subtract(new BigDecimal(netsubtotal)).setScale(2,
                            RoundingMode.HALF_UP).doubleValue();
                    System.out.println("tax method is "+taxmethod+" tax settings is " +
                            ""+__taxsettings + " netprice is "+netsaleprice + " saleprice = " +
                            saleprice+" subtotoal "+subtotal+" netsubtotal "+netsaleprice+ " " +
                            "taxamount "+taxamount);

                    etprice.setText(String.valueOf(subtotal));
                    if(q > 0){

                        salesItems.add(new SalesItems(prodlocal,whid,i,q,netsaleprice,saleprice,taxamount,
                                subtotal,tx,0,prod));
                        adapter = new SalesItemAdapter(SalesLiteActivity.this, salesItems);
                        adapter.notifyDataSetChanged();
                        txtorderitem.setText(getString(R.string.straddeditem) + " (" + salesItems.size() + ")");
                        lvitems.setAdapter(adapter);

                        double reportdata;
                        String strfinalprice = txreportdata.getText().toString();
                        if (TextUtils.isEmpty(strfinalprice)) {
                            reportdata = 0;
                        } else {
                            reportdata = Double.parseDouble(strfinalprice);
                        }
                        double total = subtotal + reportdata;
                        String totals = String.valueOf(total);
                        txreportdata.setText(totals);
                        __prodcode = "";
                        saleprice = 0;
                        netsaleprice = 0;
                        subtotal = 0;
                        q = 0;
                        etprice.setText("");
                        etquantity.setText("");
                        etproduct.setText("");
                        etproduct.requestFocus();

                    }

        }

        etdiscount.setText("");
        etquantity.setText("");
        ab.setSubtitle("");
        etprice.setText("");
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.add_sales_buttons, menu);
        MenuItem additem, saveitem;
        additem = menu.findItem(R.id.action_add_item);
        saveitem = menu.findItem(R.id.action_save);

        additem.setVisible(false);
        saveitem.setVisible(true);
        return super.onCreateOptionsMenu(menu);
    };

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                Intent intent = new Intent(this, MainScreen.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(intent);
                finish();
                break;
            case R.id.action_save:
                next_screen_button();
                break;
        }
        return false;

    }

    private void last_screen(final String[] vals){
        LayoutInflater infl = getLayoutInflater();
        View view = infl.inflate(R.layout.sale_summary, null) ;

        final Dialog dialog = new Dialog(this, R.style.CustomDialog	);
        dialog.setCancelable(false);
        dialog.setContentView(view);
        TextView txttotal,txtpaid,txtpaystatus,saleref,customer;

        txttotal = (TextView)dialog.findViewById(R.id.txttotal);
        txtpaid = (TextView)dialog.findViewById(R.id.txtpaid);
        txtpaystatus = (TextView)dialog.findViewById(R.id.txtpaymentstatus);
        saleref = (TextView)dialog.findViewById(R.id.txtsaleref);
        customer = (TextView)dialog.findViewById(R.id.txtcustomer);
        Button btnsaveprint = (Button) dialog.findViewById(R.id.btnSubmitPrint);

        txttotal.append(" "+vals[4]);
        txtpaid.append(" " + vals[0]);
        txtpaystatus.append(" " + vals[5]);
        final String batch = "SALE"+Constants.generateBatch()+"-"+Constants.getDateOnly2();
        saleref.append(" "+batch);
        customer.append(" "+Customers.getCustomerBusinessNameLoca(dialog.getContext(), customeridLocal));

        ListView listView = (ListView)dialog.findViewById(R.id.summary_list);
        SalesSummaryAdapter adapter = new SalesSummaryAdapter(dialog.getContext(),salesItems);
        listView.setAdapter(adapter);
/*
        new String[]{__paidamount[0],comm.getText().toString(), __printable[0],
                __paymode[0],txtfinal.getText().toString(),__paystatus[0]}*/

        Button btnSave = (Button) dialog.findViewById(R.id.btnSubmit);
        ImageView gobak = (ImageView)dialog.findViewById(R.id.goback);
        gobak.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });
        btnSave.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                System.out.println(" click submit button ");
                if (Validating.areSet(vals)) {
                    save_order(new String[]{vals[0], vals[1], "0", batch}, dialog);
                }
            }
        });

        if(!Settings.getPrintEnabled(this))
            btnsaveprint.setEnabled(false);

        btnsaveprint.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                System.out.println(" click submit and print button ");
                if (Validating.areSet(vals)) {
                    save_order(new String[]{vals[0], vals[1], "1", batch}, dialog);
                }
            }
        });


        dialog.show();
    }

    private void save_order(String[] valz, Dialog dialog) {
        SalesItems list = null;
        if(Validating.areSet(valz))
            dialog.dismiss();

        System.out.println("commments " + valz[1]);
        if (!customeridLocal.isEmpty() || !customeridLocal.equals("0")) {
            if (!valz[0].isEmpty()) {
                String cust = String.valueOf(customerid);
                String custloc = String.valueOf(customeridLocal);
                String paid = valz[0];
                double totalsales = 0;
                double totaltax = 0;
                double totaldiscount = 0;
                int totalitems = salesItems.size();
                String batch = valz[3];
                String taxset = String.valueOf(__taxsettings);
                String printed = null;
                boolean toprint = false;
                System.out.println("PRINT is "+valz[2]);
                if(valz[2].equals("1")) {
                    printed = "1";
                    toprint = true;
                }
                if(valz[2].equals("0")) {
                    printed = "0";
                    toprint  = false;
                }
                String paystatus = null;

                if (salesItems.size() > 0) {
                    for (int i = 0; i < salesItems.size(); i++) {
                        list = salesItems.get(i);
                        totalsales += list.getSbtotal();
                        totaltax += list.getTaxmt();
                    }
                }
                double _paymnt = new BigDecimal(valz[0]).doubleValue();
                double _tsale = new BigDecimal(totalsales).doubleValue();
                if(_paymnt == _tsale)
                    paystatus = Constants.PAY_STATUS_CMPLT;
                else
                    paystatus = Constants.PAY_STATUS_PNDG;

                String lat = String.valueOf(gpsTracker.getLatitude());
                String lng = String.valueOf(gpsTracker.getLongitude());


                String[] vals = {cust,paid,String.valueOf(totalsales),String.valueOf(totaltax),String.valueOf
                        (totaldiscount),String.valueOf(totalitems),batch,valz[1],taxset,printed,paystatus,custloc,
                        lat,lng,Constants.SALE_STATUS_CMPLT,Constants.SALETYPE_SALE};
                String[] checkvals = {cust,paid,String.valueOf(totalsales),String.valueOf(totaltax),String.valueOf
                        (totaldiscount),String.valueOf(totalitems),batch,taxset,printed,paystatus,custloc};
                int orderid = 0;
                if(Validating.areSet(checkvals)){
                    if(Sales.insert(this,vals,Constants.LOCAL)){
                        orderid = Sales.getId(SalesLiteActivity.this);
                        System.out.println("orderid "+orderid);
                    }
                }
                final int[] insert = new int[]{0};
                if(orderid != 0){
                    for(int i = 0; i < salesItems.size(); i++){
                        list = salesItems.get(i);
                        String prodno = list.getProd();
                        System.out.println("prod "+prodno);
                        String locprod = String.valueOf(list.getProdloc());
                        System.out.println("local prod "+locprod);
                        String qty = String.valueOf(list.getQty());
                        System.out.println("qty "+qty);
                        String netprc = String.valueOf(list.getNetprice());
                        System.out.println("netprice "+netprc);
                        String saleprc = String.valueOf(list.getPrice());
                        System.out.println("salepr "+saleprc);
                        String taxam = String.valueOf(list.getTaxmt());
                        System.out.println("taxamou "+taxam);
                        String subto = String.valueOf(list.getSbtotal());
                        System.out.println("sutot "+subto);
                        String wh = list.getWh();
                        System.out.println("wh "+wh);
                        String disc = String.valueOf(list.getDiscount());
                        System.out.println("disc "+disc);
                        String taxr = String.valueOf(list.getTaxrt());
                        System.out.println("taxrat "+taxr);
                        String order = String.valueOf(orderid);
                        String saleref = batch;
                        String[] items = {prodno,locprod,qty,netprc,saleprc,taxam,subto,wh,disc,taxr,order,
                                saleref,Constants.SALE_STATUS_CMPLT,Constants.SALETYPE_SALE};
                        if(Validating.areSet(items)){
                            System.out.println("locprod "+locprod);
                            if(SalesItems.insertItems(SalesLiteActivity.this,items,Constants.LOCAL) ==1){
                                /*String isservice = Products.getProductType(this,locprod);
                                System.out.println("product type is "+isservice);
                                if(isservice.equals(Constants.PRODUCT_STANDARD)){
                                    int prodid = Integer.parseInt(locprod);
                                    Products.updateProductQty(this, new int[]{prodid,list.getQty(), 1});
                                }*/ //disable qty deduction
                                insert[0] += i ;
                            }
                        }
                    }
                }

                if(toprint){
                    String app = "app.incotex.fpd";
                    if(Constants.isPackageInstalled(app,this)) {
                        int send = Constants.writeReceipt(SalesLiteActivity.this, salesItems,__taxsettings,customeridLocal);
                        startActivity(getPackageManager().getLaunchIntentForPackage(app));
                        setResult(-1, new Intent());
                        Toast.makeText(this, getString(R.string.strorderadded), Toast.LENGTH_LONG).show();
                        if (send == 1) {
                            Sales.putPrintStatus(this, new int[]{orderid, send});
                            salesItems.clear();
                            toprint = false;
                            valz = null;
                            adapter.notifyDataSetChanged();
                            lvitems.setAdapter(adapter);
                            txreportdata.setText("");
                            txtorderitem.setText("");
                        }
                    } else {
                        Toast.makeText(this, getString(R.string.strorderadded), Toast.LENGTH_SHORT).show();
                        Toast.makeText(this, getString(R.string.strnoprinterapp), Toast.LENGTH_LONG).show();
                        salesItems.clear();
                        printed = "0";
                        toprint = false;
                        valz = null;
                        adapter.notifyDataSetChanged();
                        lvitems.setAdapter(adapter);
                        txreportdata.setText("");
                        txtorderitem.setText("");
                    }
                } else {
                    Toast.makeText(this, getString(R.string.strorderadded), Toast.LENGTH_LONG).show();
                    salesItems.clear();
                    printed = "0";
                    toprint = false;
                    valz = null;
                    adapter.notifyDataSetChanged();
                    lvitems.setAdapter(adapter);
                    txreportdata.setText("");
                    txtorderitem.setText("");
                }
            }

        }

        laycust.setVisibility(View.GONE);
        layprod.setVisibility(View.VISIBLE);
        layqty.setVisibility(View.VISIBLE);
        layrep.setVisibility(View.VISIBLE);

        lvitems.setVisibility(View.VISIBLE);
        topb.setVisibility(View.VISIBLE);
        lowb.setVisibility(View.VISIBLE);
    }


    private void process_sale(){

        LayoutInflater infl = getLayoutInflater();
        View view = infl.inflate(R.layout.paymentmethod, null) ;
        ImageView submit;
        dialogmode = new Dialog(this, R.style.CustomDialog	);
        dialogmode.setCancelable(false);
        dialogmode.setContentView(view);
        final String[] __notes = new String[]{""};
        final String[] __paidamount = new String[]{""};
        final String[] __printable = new String[]{""};
        final String[] __paymode = new String[]{""};
        final String[] __paystatus = new String[]{""};
        final Spinner sppaymethods;
        final EditText comm ;
        String[] businesslist;
        final ArrayAdapter adapter ;
        ArrayAdapter<String> adapters;


        final TextView txtfinal = (TextView)dialogmode.findViewById(R.id.txttitle);
        TextView bznlable;
        final AutoCompleteTextView bzn;
        final double[] mfinalcost = new double[]{0};
        double totalax = 0;
        double totalsale = 0;
        double subtotal = 0;
        SalesItems list = null;
        if (salesItems.size() > 0) {
            for (int i = 0; i < salesItems.size(); i++) {
                list = salesItems.get(i);
                mfinalcost[0] += list.getSbtotal();
                totalax += list.getTaxmt();
                subtotal += list.getSbtotal();
            }
        }
        totalsale = subtotal-totalax;


        sppaymethods = (Spinner)dialogmode.findViewById(R.id.sppayoptions);

        final String[] methods = getResources().getStringArray(R.array.strpaymethods_array);
        final String[] methods2 = getResources().getStringArray(R.array.strpaymethods_array_nopost);


        if(Settings.getPayLaterEnabled(dialogmode.getContext()))
            adapter = new ArrayAdapter<>(dialogmode
                    .getContext(), android.R.layout
                    .simple_dropdown_item_1line, methods);
        else
            adapter = new ArrayAdapter<>(dialogmode.getContext(), android.R.layout
                    .simple_dropdown_item_1line, methods2);
         businesslist = Customers.getAllCustomers(this);
        adapters = new ArrayAdapter<>(this, android.R.layout.select_dialog_item, businesslist);
        bzn = (AutoCompleteTextView) dialogmode.findViewById(R.id.autotxtsaleclientname);
        bznlable = (TextView)dialogmode.findViewById(R.id.txtbusiness);
        bzn.setAdapter(adapters);
        bzn.setThreshold(1);
        bznlable.setVisibility(View.VISIBLE);
        bzn.setVisibility(View.VISIBLE);

        customerid = "0";
        customeridLocal = "0";

        bzn.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                String name = (String) parent.getItemAtPosition(position);
                bzn.setText(name);
                customerid = String.valueOf(Customers.getCustomerid(dialogmode.getContext(), name));
                customeridLocal = String.valueOf(Customers.getCustomerlocalId(dialogmode.getContext(), name));
            }
        });

        sppaymethods.setAdapter(adapter);
        sppaymethods.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                if (position == adapter.getCount() - 1 && adapter.getCount() == methods.length) {
                    etpartial.setFocusable(true);
                    etpartial.setFocusableInTouchMode(true);
                    etpartial.setText("0");
                    __paymode[0] = (String) adapter.getItem(position);
                    __paystatus[0] = __paymode[0];
                    payinfull = 1;
                    System.out.println(__paymode[0]);
                } else {
                    __paymode[0] = (String) adapter.getItem(position);
                    __paystatus[0] = "Paid";
                    System.out.println(__paymode[0]);
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        txtfinal.setText(dialogmode.getContext().getString(R.string.stramount) + " " + mfinalcost[0] + " " + curr);
        submit = (ImageView)dialogmode.findViewById(R.id.btnSubmit);


        etpartial = (EditText)dialogmode.findViewById(R.id.etpartialpay);
        etpartial.setText("" + subtotal);
        comm = (EditText)dialogmode.findViewById(R.id.etcomments);
        this.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);

        ImageView gobak = (ImageView)dialogmode.findViewById(R.id.goback);
        gobak.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                __notes[0] = "";
                __paidamount[0] = "";
                dialogmode.dismiss();
            }
        });

        __printable[0] = "1";

        submit.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View view) {
                String busn = bzn.getText().toString();
                if(Validating.areSet(busn)){
                    if(customeridLocal.equals("0")){
                        boolean added = addupdatecustomer(bzn);
                        if(added){
                            System.out.println("added and id is "+customeridLocal);
                        }
                    }
                } else {
                    String bznm = Customers.getDefaultCustomer(dialogmode.getContext());
                    customerid = String.valueOf(Customers.getCustomerid(dialogmode.getContext(), bznm));
                    customeridLocal = String.valueOf(Customers.getCustomerlocalId(dialogmode.getContext(), bznm));
                }

                String str = SalesLiteActivity.this.etpartial.getText().toString().trim();
                if (payinfull == 1)
                {
                    if (!str.isEmpty())
                    {
                        if(!Constants.checkDouble(SalesLiteActivity.this,str,etpartial))
                            return;
                        Double partialpay = new BigDecimal(str).setScale(2, RoundingMode.HALF_UP).doubleValue();
                        Double fullpay =  new BigDecimal(mfinalcost[0]).setScale(2, RoundingMode.HALF_UP).doubleValue();
                        if (partialpay <= fullpay)
                        {
                            __paidamount[0] = String.valueOf(partialpay);
//                            save_order(__paidamount[0],__notes[0],false);
                            String comments = comm.getText().toString().trim();
                            dialogmode.dismiss();
                            if(!Validating.areSet(comments))
                                comments = "no comments";
                            last_screen(new String[]{__paidamount[0],comments, __printable[0],
                                    __paymode[0],txtfinal.getText().toString(),__paystatus[0]});
                            return;
                        } else {
                            Toast.makeText(SalesLiteActivity.this.dialogmode.getContext(),
                                    SalesLiteActivity.this.getString(R.string.strwrongamount) + " " +
                                            mfinalcost[0], Toast.LENGTH_LONG).show();
                            return;
                        }
                    } else {
                        etpartial.requestFocus();
                        Toast.makeText(dialogmode.getContext(), SalesLiteActivity
                                .this.getString(R.string.strfilltheamount), Toast.LENGTH_LONG).show();
                        return;
                    }
                } else {
                    String comments = comm.getText().toString().trim();
                    dialogmode.dismiss();
                    if(!Validating.areSet(comments))
                        comments = "no comments";
                    __paidamount[0] = String.valueOf(mfinalcost[0]);
                    dialogmode.dismiss();
                    last_screen(new String[]{__paidamount[0], comments, __printable[0], __paymode[0],
                            txtfinal.getText().toString(), __paystatus[0]});
                }
            }
        });
        dialogmode.show();
    }

    private boolean addupdatecustomer(AutoCompleteTextView etclint) {
        boolean bool = false;
        String strclname,strshname,strclemail,strclphone,strcladdress,svrn,stin ;
        strclname = etclint.getText().toString().trim();
        strshname = strclname;
        strclemail = "";
        strclphone = "0008833";
        strcladdress = "Dar es salaam";
        svrn ="";
        stin = "";

        boolean businessexist = Customers.isCustomerValid(this, strclname);
        String[] input = new String[]{strclname,strshname,strclphone,strclemail,strcladdress,svrn,stin} ;
        if (!businessexist) {
            int insert = Customers.insertCustomerLocal(this,input);
            if (insert == 1) {
                customerid = String.valueOf(Customers.getCustomerid(SalesLiteActivity.this, strshname));
                customeridLocal = String.valueOf(Customers.getCustomerlocalId(SalesLiteActivity.this, strshname));
                bool = true;
            }
        } else {
            customerid = String.valueOf(Customers.getCustomerid(SalesLiteActivity.this, strshname));
            customeridLocal = String.valueOf(Customers.getCustomerlocalId(SalesLiteActivity.this, strshname));
            bool = true;
        }
        return bool;
    }

    @Override
    public boolean onKey(View v, int keyCode, KeyEvent event) {
        switch (v.getId()) {
            case R.id.autoquantity00:
                if (event.getAction() == KeyEvent.ACTION_DOWN && event.getKeyCode() == KeyEvent.KEYCODE_ENTER) {
                    Log.i("event", "captured qty");
                    InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                    imm.hideSoftInputFromWindow(getCurrentFocus().getWindowToken(), 0);
                    process_item();
                    return false;
                }
                break;
            case R.id.etdiscount:
                if (event.getAction() == KeyEvent.ACTION_DOWN && event.getKeyCode() == KeyEvent.KEYCODE_ENTER) {
                    Log.i("event", "captured unitprice");
                    boolean add = addproduct();
                    if(add) {
                        etquantity.requestFocus();
                    } else
                        etdiscount.requestFocus();
                }
                break;
        }
        return false;
    }
    @Override
    public boolean onKeyUp(int keyCode, KeyEvent event) {
        if(keyCode == KeyEvent.KEYCODE_BACK)
        {
            Log.i("event", "captured");
            if(salesItems.size()>0 || !__prodcode.isEmpty()){
                AlertDialog.Builder builder = new AlertDialog.Builder(this);
                builder.setMessage(getString(R.string.strsalecancelwarn))
                        .setCancelable(false)
                        .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                salesItems.clear();
                                startActivity(new Intent(SalesLiteActivity.this,MainScreen.class));
                            }
                        })
                        .setNegativeButton("No", new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                dialog.cancel();
                            }
                        });
                AlertDialog alert = builder.create();
                alert.show();
                return true;
            } else {
                startActivity(new Intent(this,MainScreen.class));
                finish();
                return true;
            }
        } else
            return super.onKeyUp(keyCode, event);
    }

    @Override
    public void beforeTextChanged(CharSequence s, int start, int count, int after) {

    }

    @Override
    public void onTextChanged(CharSequence s, int start, int before, int count) {

    }

    @Override
    public void afterTextChanged(Editable s) {
        String productname = etproduct.getText().toString();
        if (TextUtils.isEmpty(productname)) {
            Toast.makeText(this, getResources().getString(R.string.strfilldata), Toast.LENGTH_LONG).show();
        }
        if(Validating.areSet(productname)) {
            __prodcode = Products.getProductIdUsingSku(this, productname);
            System.out.println(" product code is " + __prodcode);
            double pr = Products.getUnitPrice(SalesLiteActivity.this, __prodcode);
            String sku = Products.getSku(SalesLiteActivity.this, __prodcode);
            etprice.setText(getString(R.string.strprice)+" "+pr);
            ab.setSubtitle(getString(R.string.strsku2) + " " + sku);
            InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(getCurrentFocus().getWindowToken(), 0);
        }
    }


    @Override
    protected void onStart() {
        super.onStart();
        turnGpsOn() ;
    }

    public void turnGpsOn(){
        int accounttype = appPreference.get_accounttype();
        if(accounttype == 1)
            if(Build.VERSION.SDK_INT > Build.VERSION_CODES.KITKAT) {
                if(!gpsTracker.isgpson()) {
                    System.out.println(" isgpson?? " + gpsTracker.isgpson());
                    Toast.makeText(this,getString(R.string.strgps),Toast.LENGTH_LONG).show();
                    Intent inte = new Intent(android.provider.Settings.ACTION_LOCATION_SOURCE_SETTINGS);
                    startActivity(inte);
                }
            } else {
                Intent intent = new Intent("android.location.GPS_ENABLED_CHANGE");
                intent.putExtra("enabled", true);
                sendBroadcast(intent);
            }
    }


    @Override
    public void onResume() {
        super.onResume();
        turnGpsOn();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if(gpsTracker.isgpson())
            gpsTracker.stopUsingGPS();
        turnGpsOf();
    }

    @Override
    protected void onPause() {
        super.onPause();
        if(gpsTracker.isgpson())
            gpsTracker.stopUsingGPS();
        turnGpsOf();
    }
    public void turnGpsOf(){
        if(Build.VERSION.SDK_INT < Build.VERSION_CODES.KITKAT) {
            Intent intent = new Intent("android.location.GPS_ENABLED_CHANGE");
            intent.putExtra("enabled", false);
            sendBroadcast(intent);
        }
    }

    @Override
    protected void onStop() {
        super.onStop();
        if(gpsTracker.isgpson())
            gpsTracker.stopUsingGPS();
        turnGpsOf();
    }

    private void next_screen_button(){
        if(!business){
            String bznm = edclientname.getText().toString().trim();
            if(Validating.areSet(bznm)){
                if(!Customers.isCustomerValid(this,bznm))
                {
                    edclientname.setText("");
                    edclientname.setHint(getString(R.string.strinvalidbusinesswarn));
                    edclientname.setHintTextColor(Color.RED);
                    edclientname.requestFocus();
                } else {
                    customerid = String.valueOf(Customers.getCustomerid(SalesLiteActivity.this, bznm));
                    customeridLocal = String.valueOf(Customers.getCustomerlocalId(SalesLiteActivity.this, bznm));
                    business = true;
                    edclientname.setText("");
                    laycust.setVisibility(View.GONE);
                    layprod.setVisibility(View.VISIBLE);
                    etproduct.requestFocus();
                    layqty.setVisibility(View.VISIBLE);
                    layrep.setVisibility(View.VISIBLE);
                    lvitems.setVisibility(View.VISIBLE);
                    topb.setVisibility(View.VISIBLE);
                    lowb.setVisibility(View.VISIBLE);
                }
            }
        } else {
            if( !__prodcode.isEmpty()){
                AlertDialog.Builder builder = new AlertDialog.Builder(this);
                builder.setMessage(getString(R.string.strpendingitem))
                        .setCancelable(false)
                        .setPositiveButton("OKAY", new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                dialog.dismiss();
                            }
                        });
                AlertDialog alert = builder.create();
                alert.show();
            } else {
                if(salesItems != null && salesItems.size() > 0)
                    process_sale();
            }
        }
    }

    public void addcustomer(){

        LayoutInflater infl = getLayoutInflater();
        View view = infl.inflate(R.layout.addcustomer, null);
        final Dialog dialog = new Dialog(this, R.style.CustomDialog);
        dialog.setCancelable(false);
        dialog.setContentView(view);
        dialog.show();
        Button sub = (Button) dialog.findViewById(R.id.btnSubmit);
        TextView txttitle = (TextView) dialog.findViewById(R.id.txttitle);
        String title = getResources().getString(R.string.straddcustomer);
        title.toUpperCase(Locale.getDefault());
        txttitle.setText(title);
        final EditText clname,shname,phone,email,address,vrn,tin ;

        ImageView goback = (ImageView) dialog.findViewById(R.id.goback);
        goback.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });

        clname = (EditText) dialog.findViewById(R.id.etclientname);
        shname = (EditText) dialog.findViewById(R.id.etshopname);
        phone = (EditText) dialog.findViewById(R.id.etclientphone);
        vrn = (EditText) dialog.findViewById(R.id.etvrn);
        tin = (EditText) dialog.findViewById(R.id.ettin);
        email = (EditText) dialog.findViewById(R.id.etclientemail);
        address = (EditText) dialog.findViewById(R.id.etclientshopaddress);


        sub.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String strclname, strshname, strclemail, strclphone, strcladdress, svrn, stin;
                strclname = clname.getText().toString();
                strshname = Validating.stripit(shname.getText().toString());
                strclemail = Validating.stripit(email.getText().toString());
                strclphone = Validating.stripit(phone.getText().toString());
                svrn = vrn.getText().toString();
                stin = tin.getText().toString();
                strcladdress = address.getText().toString();

                if (!strclname.isEmpty())
                    if (!Constants.checkInvalidChars(dialog.getContext(), strclname, clname))
                        return;
                if (!strshname.isEmpty())
                    if (!Constants.checkInvalidChars(dialog.getContext(), strshname, shname))
                        return;
                if (!svrn.isEmpty())
                    if (!Constants.checkInvalidChars(dialog.getContext(), svrn, vrn))
                        return;
                if (!stin.isEmpty())
                    if (!Constants.checkInvalidChars(dialog.getContext(), stin, tin))
                        return;
                if (!strclphone.isEmpty())
                    if (!Constants.checkInvalidChars(dialog.getContext(), strclphone, phone))
                        return;
                if (!strclemail.isEmpty())
                    if (!Constants.checkInvalidChars(dialog.getContext(), strclemail, email))
                        return;

                if (!Validating.areSet(strcladdress))
                    strcladdress = "Dar es salaam";
                if (!Validating.areSet(strclname))
                    strclname = shname.getText().toString();
                EditText[] et = new EditText[]{clname, shname, phone, email, address};
                String[] input = new String[]{strclname, strshname, strclphone, strclemail, strcladdress, svrn, stin};
                String[] inputcheck = new String[]{strclname, strshname, strclphone};

                if (Validating.areSet(inputcheck)) {
                    if (strclemail.isEmpty()) {
                        int insert = Customers.insertCustomerLocal(dialog.getContext(), input);
                        if (insert == 1) {
                            customerid = String.valueOf(Customers.getCustomerid(SalesLiteActivity.this, strshname));
                            customeridLocal = String.valueOf(Customers.getCustomerlocalId(SalesLiteActivity.this, strshname));
                            business = true;
                            dialog.dismiss();
                            edclientname.setText("");
                            laycust.setVisibility(View.GONE);
                            layprod.setVisibility(View.VISIBLE);
                            etproduct.requestFocus();
                            layqty.setVisibility(View.VISIBLE);
                            layrep.setVisibility(View.VISIBLE);

                            if (Settings.getDiscountEnabled(dialog.getContext()))
                                rdiscount.setVisibility(View.VISIBLE);
                            lvitems.setVisibility(View.VISIBLE);
                            topb.setVisibility(View.VISIBLE);
                            lowb.setVisibility(View.VISIBLE);
                            Toast.makeText(dialog.getContext(), getResources().getString(R.string.strinputsuccesscustomer), Toast.LENGTH_SHORT).show();
                        } else {
                            Toast.makeText(dialog.getContext(), getResources().getString(R.string.strinputfailcustomer), Toast.LENGTH_SHORT).show();
                            dialog.dismiss();
                        }
                    } else if (!strclemail.isEmpty() && Validating.isEmailValid(strclemail)) {

                        int insert = Customers.insertCustomerLocal(dialog.getContext(), input);
                        if (insert == 1) {
                            customerid = String.valueOf(Customers.getCustomerid(SalesLiteActivity.this, strshname));
                            customeridLocal = String.valueOf(Customers.getCustomerlocalId(SalesLiteActivity.this, strshname));
                            business = true;
                            dialog.dismiss();
                            edclientname.setText("");
                            laycust.setVisibility(View.GONE);
                            layprod.setVisibility(View.VISIBLE);
                            etproduct.requestFocus();
                            layqty.setVisibility(View.VISIBLE);
                            layrep.setVisibility(View.VISIBLE);

                            if (Settings.getDiscountEnabled(dialog.getContext()))
                                rdiscount.setVisibility(View.VISIBLE);
                            lvitems.setVisibility(View.VISIBLE);
                            topb.setVisibility(View.VISIBLE);
                            lowb.setVisibility(View.VISIBLE);
                            Toast.makeText(dialog.getContext(), getResources().getString(R.string.strinputsuccesscustomer), Toast.LENGTH_SHORT).show();
                        } else {
                            Toast.makeText(dialog.getContext(), getResources().getString(R.string.strinputfailcustomer), Toast.LENGTH_SHORT).show();
                            dialog.dismiss();
                        }
                    } else {
                        et[3].setHint(getResources().getString(R.string.strinputerror) +
                                " " + getResources().getString(R.string.strclientemail));
                        et[3].setHintTextColor(Color.RED);
                        et[3].setCompoundDrawablesWithIntrinsicBounds(getResources().getDrawable(R.drawable.ic_timer_auto_black_24dp), null, getResources().getDrawable(R.drawable.ic_error_red_24dp), null);
                    }

                } else {
                    if (!Validating.areSet(strclname)) {
                        et[0].setHint(getResources().getString(R.string.strinputerror) +
                                " " + getResources().getString(R.string.strclientname));
                        et[0].setHintTextColor(Color.RED);
                        et[0].setCompoundDrawablesWithIntrinsicBounds(getResources().getDrawable(R.drawable.ic_timer_auto_black_24dp), null, getResources().getDrawable(R.drawable.ic_error_red_24dp), null);
                    }
                    if (!Validating.areSet(strshname)) {
                        et[1].setHint(getResources().getString(R.string.strinputerror) +
                                " " + getResources().getString(R.string.strclientshopname));
                        et[1].setHintTextColor(Color.RED);

                        et[1].setCompoundDrawablesWithIntrinsicBounds(getResources().getDrawable(R.drawable.ic_account_box_black_24dp), null, getResources().getDrawable(R.drawable.ic_error_red_24dp), null);
                    }
                    if (!Validating.areSet(strclphone)) {
                        et[2].setHint(getResources().getString(R.string.strinputerror) +
                                " " + getResources().getString(R.string.strclientphone));
                        et[2].setHintTextColor(Color.RED);
                        et[2].setCompoundDrawablesWithIntrinsicBounds(getResources().getDrawable(R.drawable.ic_local_phone_black_24dp), null, getResources().getDrawable(R.drawable.ic_error_red_24dp), null);
                    }
                    if (!Validating.areSet(strcladdress)) {
                        et[4].setHint(getResources().getString(R.string.strinputerror) +
                                " " + getResources().getString(R.string.strclientaddress));
                        et[4].setHintTextColor(Color.RED);
                        et[4].setCompoundDrawablesWithIntrinsicBounds(getResources().getDrawable(R.drawable.ic_pin_drop_black_24dp), null, getResources().getDrawable(R.drawable.ic_error_red_24dp), null);
                    }
                    return;
                }
            }
        });
    }

    private boolean addproduct() {
        boolean bool = false;
        String setprodname, setreorder, setprice, settax, setsku, setdesc, stcat = "",
                stservice = null, sttaxmethod = null, prodphoto = null;
        setprodname = etproduct.getText().toString().trim();
        setreorder = "0";
        setprice = etdiscount.getText().toString().trim();
        if(!setprice.isEmpty())
            if (!Constants.checkDouble(this, setprice, etdiscount))
                return false;

        if (!Constants.checkInvalidChars(this, setprodname, etproduct))
            return false;

        setsku = String.valueOf(System.currentTimeMillis());
        setdesc = "";
        stcat = Product_Categories.getIdLite(this);
        System.out.println(" stcat id is "+stcat);
        settax = "18";
        stservice = Constants.PRODUCT_STANDARD;
        sttaxmethod = "0";
        String[] check = {setprodname, setprice, stcat};

        if (Validating.areSet(check)) {
            int prodexist = Products.isProductPresent(this, new String[]{setprodname});
            int prodexistid = Products.isProductPresentUpdating(this, setprodname);
            if (prodexist == 0) {
                String[] vals = {setprodname, setreorder, setprice, settax, setsku, setdesc, stcat,
                        stservice, "co128", sttaxmethod,prodphoto};
                int insert = Products.insertProduct(this, vals, Constants.LOCAL);
                if (insert == 1) {
                    productlist = Products.getAllProducts(this);
                    productcodelist = Products.getAllProductsCode(this);

                    ArrayAdapter<String> proadapter = new ArrayAdapter<>(this, android.R.layout.select_dialog_item, productlist);
                    proadapter.notifyDataSetChanged();
                    etproduct.setAdapter(proadapter);
                    __prodcode = Products.getProductIdUsingSku(this, setsku);
                    bool = true;
                }
            } else if(prodexistid > 0){
                String[] vals = {setprodname, setreorder, setprice, settax, setsku, setdesc, String.valueOf
                        (prodexistid),stcat, stservice, "co128", sttaxmethod,prodphoto};
                boolean insert = Products.updateProduct(this, vals, Constants.LOCAL);
                if(insert)
                    __prodcode = Products.getProductIdUsingSku(SalesLiteActivity.this, setsku);
                bool = true;
            }
            ab.setSubtitle(getString(R.string.strsku2) + " " + setsku);
            String price = getString(R.string.strprice) + " " + setprice;
            etprice.setText(price);
        }
        return bool;
    }
}


