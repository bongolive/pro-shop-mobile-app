/*
 * Copyright (c) 2015. All rights reserved. The source codes are property of bongolive
 */

package bongolive.apps.proshop.view;

import android.annotation.TargetApi;
import android.app.Activity;
import android.app.Dialog;
import android.app.SearchManager;
import android.content.ContentResolver;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.hardware.Camera;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v7.widget.SearchView;
import android.util.Log;
import android.view.ContextMenu;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.csvreader.CsvReader;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.math.BigDecimal;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import bongolive.apps.proshop.R;
import bongolive.apps.proshop.controller.AppPreference;
import bongolive.apps.proshop.controller.Constants;
import bongolive.apps.proshop.controller.ImportItems;
import bongolive.apps.proshop.controller.ItemList;
import bongolive.apps.proshop.controller.ItemListAdapter;
import bongolive.apps.proshop.controller.Validating;
import bongolive.apps.proshop.model.MultimediaContents;
import bongolive.apps.proshop.model.Product_Categories;
import bongolive.apps.proshop.model.Products;
import bongolive.apps.proshop.model.PurchaseItems;
import bongolive.apps.proshop.model.Sales;
import bongolive.apps.proshop.model.Settings;
import bongolive.apps.proshop.model.Warehouse;

public class ProductsFragment extends Fragment implements AdapterView.OnItemClickListener {
    private static final String ARG_SECTION_NUMBER = "section_number";
    AlertDialogManager alert;
    View _rootView;
    ListView lv;
    Button sub;
    Dialog dialog;
    Dialog dg;
    ItemListAdapter adapter;
    List<ItemList> list;
    private final int ADDSTOCK = 1;
    private final int GETPRODUCTSKU = 3;
    SearchManager searchManager;
    SearchView searchView;
    TextView txtbuz, txtcli, txtadd, txtcont, txtunk;
    boolean tabletsize;
    AppPreference appPreference;
    String prodphoto;
    public static int IMPORT_SOURCE = 0;
    MenuItem item_receivestock, item_importstock;

    private final int REQUEST_CODE_PICK_FILE = 2;
    private Camera mCamera = null;
    private PhotoPreview mCameraView = null;
    final int MEDIA_TYPE_IMAGE = 1;
    FloatingActionButton actionButton ;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        if (_rootView == null) {
            _rootView = inflater.inflate(R.layout.list_redesinged, container, false);
        } else {
//			((ViewGroup)_rootView.getParent()).removeView(_rootView) ;
        }
        return _rootView;
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
//        ((Dashboard) getActivity()).onSectionAttached(getArguments().getInt(ARG_SECTION_NUMBER));

        tabletsize = getResources().getBoolean(R.bool.isTablet);
        if (tabletsize) {

            txtbuz = (TextView) getActivity().findViewById(R.id.txtbusiness);
            txtcli = (TextView) getActivity().findViewById(R.id.txtclient);
            txtadd = (TextView) getActivity().findViewById(R.id.txtadddress);
            txtcont = (TextView) getActivity().findViewById(R.id.txtcontact);
            txtunk = (TextView) getActivity().findViewById(R.id.txtunkown);
            txtbuz.setText(getString(R.string.strproduct));
            txtcli.setText(getString(R.string.strunitprices));
            txtadd.setText(getString(R.string.strsku));
            txtcont.setText(getString(R.string.strquantity));
            txtunk.setText(getString(R.string.strlastsell));
        }

        lv = (ListView) getActivity().findViewById(R.id.list);

        adapter = new ItemListAdapter(getActivity(), getList(), 2);

        lv.setAdapter(adapter);
        lv.setOnItemClickListener(this);
        registerForContextMenu(lv);
        appPreference = new AppPreference(getActivity());
        setHasOptionsMenu(true);

        actionButton = (FloatingActionButton)getActivity().findViewById(R.id.addcustmer);
        actionButton.setBackgroundColor(getResources().getColor(R.color.actionbar));
        actionButton.setVisibility(View.VISIBLE);
        actionButton.setImageResource(R.drawable.ic_action_add);

        actionButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!Settings.getAddProdEnabled(getActivity())) {
                    Toast.makeText(getActivity(), getString(R.string.strnopermissionwarn), Toast.LENGTH_SHORT).show();
                } else {
                    int product = Products.getCount(getActivity());
                    int acctype = appPreference.get_accounttype();
                    if (acctype == 2 && product >= 5) {
                        Toast.makeText(getActivity(), getString(R.string.strprodwarn), Toast.LENGTH_SHORT).show();
                    } else {
                        addproduct();
                    }
                }
            }
        });
        if(list.size() < 1){

            Snackbar snackbar = Snackbar
                    .make(_rootView, getString(R.string.strnodata), Snackbar.LENGTH_INDEFINITE);
            snackbar.show();
        }
    }

    @Override
    public void onCreateContextMenu(ContextMenu menu, View v, ContextMenu.ContextMenuInfo menuInfo) {
        if (v.getId() == R.id.list) {
            ListView lv = (ListView) v;
            AdapterView.AdapterContextMenuInfo acmi = (AdapterView.AdapterContextMenuInfo) menuInfo;
            ItemList obj = (ItemList) lv.getItemAtPosition(acmi.position);

            menu.setHeaderTitle(getString(R.string.actioncenter));
            // Add all the menu options
            menu.add(Menu.NONE, Constants.CONTEXTMENU_EDIT, 0, getString(R.string.stredit));
            menu.add(Menu.NONE, Constants.CONTEXTMENU_DELETE, 1, getString(R.string.strdelete));
        }
    }

    @Override
    public boolean onContextItemSelected(MenuItem item) {
        AdapterView.AdapterContextMenuInfo info = (AdapterView.AdapterContextMenuInfo) item.getMenuInfo();
        ItemList _list = list.get(info.position);
        String name = _list.getitemname();
        long i = _list.getId();
        switch (item.getItemId()) {
            case Constants.CONTEXTMENU_EDIT:
                if (!Settings.getEditProdEnabled(getActivity())) {
                    Toast.makeText(getActivity(), getString(R.string.strnopermissionwarn), Toast.LENGTH_SHORT).show();
                } else {
                    if (i > 0)
                        showProduct(i, name, 1);
                }
                return true;
            case Constants.CONTEXTMENU_DELETE:
                if (!Settings.getEditProdEnabled(getActivity())) {
                    Toast.makeText(getActivity(), getString(R.string.strnopermissionwarn), Toast.LENGTH_SHORT).show();
                } else {
                    int acctype = appPreference.get_accounttype();

                    Products.deleteProduct(getActivity(), String.valueOf(i), acctype);
                    adapter.notifyDataSetChanged();
                    adapter = new ItemListAdapter(getActivity(), getList(), 2);
                    lv.setAdapter(adapter);
                }
                return true;
            default:
                return super.onContextItemSelected(item);
        }
    }

    private void showProduct(final long i, final String prodname, int action) {
        LayoutInflater infl = getActivity().getLayoutInflater();
        View view = infl.inflate(R.layout.addproduct, null);

        final Dialog dialog = new Dialog(getActivity(), R.style.CustomDialog);
        dialog.setCancelable(true);
        dialog.setContentView(view);

        final EditText etprodname, etreorder, etprice, etsku, etdesc, etqty, etupdate, etcat;
        final TextView txttile, lastup, qtyinstock,txtphoto;
        final ImageView imgphoto;
        Spinner ettax ;
        final String[] cats = {""};final String[] vatarray = {""};
        CheckBox chks, chktax;
        final boolean[] svc = {false};
        final boolean[] istaxinclusive = {true};
        LinearLayout layoqty;

        etprodname = (EditText) dialog.findViewById(R.id.etprodname);
        etreorder = (EditText) dialog.findViewById(R.id.etreorder);
        etprice = (EditText) dialog.findViewById(R.id.etunitprice);
        ettax = (Spinner) dialog.findViewById(R.id.ettax);
        etsku = (EditText) dialog.findViewById(R.id.etsku);
        etdesc = (EditText) dialog.findViewById(R.id.etdescr);
        txttile = (TextView) dialog.findViewById(R.id.txttitle);
        lastup = (TextView) dialog.findViewById(R.id.txtlastsell);


        txtphoto = (TextView) dialog.findViewById(R.id.txtaddphoto);
        imgphoto = (ImageView) dialog.findViewById(R.id.imgproductphoto);

        txtphoto.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                takephoto(imgphoto,txtphoto);
            }
        });

        qtyinstock = (TextView) dialog.findViewById(R.id.txtitemonhand);
        chks = (CheckBox) dialog.findViewById(R.id.chkservice);
        chktax = (CheckBox) dialog.findViewById(R.id.chktaxmethod);

        final Spinner spinner = (Spinner) dialog.findViewById(R.id.spcat);
        etcat = (EditText) dialog.findViewById(R.id.etcat);
        txttile.setText(prodname);
        etqty = (EditText) dialog.findViewById(R.id.etitemonhand);
        etupdate = (EditText) dialog.findViewById(R.id.etlastupdate);
        layoqty = (LinearLayout) dialog.findViewById(R.id.layqty);


        Button edit = (Button) dialog.findViewById(R.id.btnSubmit);
        ImageView goback = (ImageView) dialog.findViewById(R.id.goback);
        goback.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });

        String[] details = Products.getProducts(getActivity(), i);
        final ArrayList<String> vatlist = new ArrayList<>(Arrays.asList(getResources().getStringArray(R.array
                .strtaxrate)));
        final ArrayList<String> list = Product_Categories.getProductCats(dialog.getContext());
        final ArrayList<String> listid = Product_Categories.getProductCatsId(dialog.getContext());
        final ArrayList<String> vatlistval = new ArrayList<>(Arrays.asList(getResources().getStringArray(R.array
                .strtaxrateval)));

        final ArrayAdapter spinneradapter = new ArrayAdapter(dialog.getContext(), android.R.layout
                .simple_list_item_1, list);
        spinneradapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        if (list.size() > 0)
            spinner.setAdapter(spinneradapter);
        if (details[8].equals(getString(R.string.strselectcategory)))
            details[8] = "1";
        String[] selectedvalue = Product_Categories.getCatCodeAndName(dialog.getContext(), details[8]);
        System.out.println(" selected value " + selectedvalue[0]);
        if (!selectedvalue[0].equals(null)) {
            System.out.println("  not null");
            int spinerpos = spinneradapter.getPosition(selectedvalue[0]);
            spinner.setSelection(spinerpos);
        }
        spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                if (position > 0 || position < list.size() - 1)
                    cats[0] = listid.get(position);
                if (position == list.size() - 1) {
                    etcat.setVisibility(View.VISIBLE);
                }
            }
            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
        final ArrayAdapter vatadapter = new ArrayAdapter(dialog.getContext(), android.R.layout
                .simple_list_item_1, vatlist);
        vatadapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        if (vatlist.size() > 0)
            ettax.setAdapter(vatadapter);
        int k = vatlistval.indexOf(String.valueOf(details[4]));//get the position of the value in the value array
        String[] selectedvatvalue = {vatlist.get(k)}; // get the value of this position in the vat list array
        System.out.println(" k  == > "+k+"  selected values ==>   " + selectedvatvalue);
        if (!selectedvatvalue[0].equals(null)) {
            System.out.println("  not null");
            int spinerpos = vatadapter.getPosition(selectedvatvalue[0]);
            ettax.setSelection(spinerpos);
        }
        ettax.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                    vatarray[0] = vatlistval.get(position);
            }
            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
        //name,price,update,itemonhand,taxrate,reorder,description,sku
        if (action == 0) {
            //view
            etqty.setVisibility(View.VISIBLE);
            etupdate.setVisibility(View.VISIBLE);
            lastup.setVisibility(View.VISIBLE);
            qtyinstock.setVisibility(View.VISIBLE);
            etqty.setFocusable(false);
            etqty.setFocusableInTouchMode(false);
            etupdate.setFocusable(false);
            etupdate.setFocusableInTouchMode(false);
            etprodname.setFocusable(false);
            etprodname.setFocusableInTouchMode(false);
            etprice.setFocusable(false);
            etprice.setFocusableInTouchMode(false);
            ettax.setFocusable(false);
            ettax.setFocusableInTouchMode(false);
            etsku.setFocusable(false);
            etsku.setFocusableInTouchMode(false);
            etdesc.setFocusable(false);
            etdesc.setFocusableInTouchMode(false);
            etreorder.setFocusable(false);
            etreorder.setFocusableInTouchMode(false);
            etcat.setVisibility(View.VISIBLE);
            etcat.setFocusable(false);
            etcat.setFocusableInTouchMode(false);
            spinner.setVisibility(View.GONE);
            chks.setEnabled(false);
            chktax.setEnabled(false);
            txtphoto.setVisibility(View.GONE);
            imgphoto.setVisibility(View.VISIBLE);

            edit.setVisibility(View.GONE);
            if (tabletsize)
                layoqty.setVisibility(View.VISIBLE);


            etprodname.setText(details[0]);
            etprice.setText(details[1]);

            etsku.setText(details[7]);
            etdesc.setText(details[6]);
            etupdate.setText(details[2]);
            etqty.setText(details[3]);
            etreorder.setText(details[5]);
            if(Validating.areSet(details[12])){
                Bitmap bt = Constants.decodeImg(details[12],300,300);
                imgphoto.setImageBitmap(bt);
                imgphoto.setRotation(90);
            }
            String[] cat = Product_Categories.getCatCodeAndName(dialog.getContext(), details[8]);
            etcat.setText(cat[1]);
            if (details[9].equals(Constants.PRODUCT_STANDARD))
                chks.setChecked(false);
            if (details[9].equals(Constants.PRODUCT_SERVICE))
                chks.setChecked(true);
            if (details[11].equals("0"))
                chktax.setChecked(true);
            if (details[11].equals("1"))
                chktax.setChecked(false);

        } else if (action == 1) {
            edit.setVisibility(View.VISIBLE);
            etsku.setFocusable(false);
            etsku.setFocusableInTouchMode(false);
            etprodname.setText(details[0]);
            etprice.setText(details[1]);

            etsku.setText(details[7]);
            etdesc.setText(details[6]);
            if (details[9].equals(Constants.PRODUCT_STANDARD))
                chks.setChecked(false);
            if (details[9].equals(Constants.PRODUCT_SERVICE))
                chks.setChecked(true);
            if (details[11].equals("0"))
                chktax.setChecked(true);
            if (details[11].equals("1"))
                chktax.setChecked(false);

            if (list.size() == 0) {
                spinner.setVisibility(View.GONE);
                etcat.setVisibility(View.VISIBLE);
            }
            etreorder.setText(details[5]);
            if(Validating.areSet(details[12])){
                txtphoto.setVisibility(View.GONE);
                imgphoto.setVisibility(View.VISIBLE);
                Bitmap bt = Constants.decodeImg(details[12],300,300);
                imgphoto.setImageBitmap(bt);
                imgphoto.setRotation(90);
            }
        }

        chks.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked)
                    svc[0] = true;
                if (!isChecked)
                    svc[0] = false;
            }
        });
        chktax.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked)
                    istaxinclusive[0] = true;
                if (!isChecked)
                    istaxinclusive[0] = false;
            }
        });


        edit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String setprodname, setreorder, setprice, settax, setsku, setdesc, setcat = null, schkser = null,
                        schektax = null;
                setprodname = etprodname.getText().toString().trim();
                setreorder = etreorder.getText().toString().trim();
                if (!Constants.checkDouble(getActivity(), setreorder, etreorder))
                    return;
                setprice = etprice.getText().toString();
                if (!Constants.checkDouble(getActivity(), setprice, etprice))
                    return;

                if (vatarray != null && vatarray.length ==1)
                    settax = vatarray[0];
                else
                    settax = vatlistval.get(0);
                if (!Constants.checkInvalidChars(getActivity(), setprodname, etprodname))
                    return;
                setsku = etsku.getText().toString().trim();
                if (!Constants.checkInvalidChars(getActivity(), setsku, etsku))
                    return;
                setsku = etsku.getText().toString();
                setdesc = etdesc.getText().toString();
                if (etcat.getVisibility() == View.VISIBLE) {
                    String stcat1 = etcat.getText().toString();
                    if (!stcat1.isEmpty()) {
                        if (!Product_Categories.isCatPresent(dialog.getContext(), stcat1.toUpperCase()
                                .trim(), Constants.LOCAL)) {
                            String[] vals = {"0", stcat1.toUpperCase().trim()};
                            if (Product_Categories.storeWCat(dialog.getContext(), vals, Constants.LOCAL) == 1)
                                setcat = Product_Categories.getId(dialog.getContext());
                        } else {
                            Toast.makeText(dialog.getContext(), "This category exists please select it " +
                                    "from the list instead", Toast.LENGTH_SHORT).show();
                            etcat.setVisibility(View.GONE);
                            spinner.setVisibility(View.VISIBLE);
                            int spinerpos = spinneradapter.getPosition(stcat1);
                            spinner.setSelection(spinerpos);
                            return;
                        }
                    }
                }
                if (etcat.getVisibility() == View.GONE)
                    setcat = cats[0];
                if (svc[0])
                    schkser = Constants.PRODUCT_SERVICE;
                if (!svc[0])
                    schkser = Constants.PRODUCT_STANDARD;

                if (istaxinclusive[0])
                    schektax = "0";
                if (!istaxinclusive[0])
                    schektax = "1";

                String[] check = {setprodname, setreorder, setprice, settax, setcat};
                if (setdesc.isEmpty())
                    setdesc = "No description";
                String[] vals = {setprodname, setreorder, setprice, settax, setsku, setdesc, String
                        .valueOf(i), setcat, schkser, "code128", schektax,prodphoto};

                if (Validating.areSet(check)) {
                    if (setcat.equals(getString(R.string.strselectcategory)))
                        return;
                    int prodexist = Products.isProductPresent(dialog.getContext(), new String[]{setprodname});
                    int prodexistid = Products.isProductPresentUpdating(dialog.getContext(), setprodname);
                    if (prodexist == 0 || (prodexist == 1 && prodexistid == i)) {
                        boolean insert = Products.updateProduct(dialog.getContext(), vals, Constants.LOCAL);
                        if (insert) {
                            String[] mult = {prodphoto, setprodname + "_" + System.currentTimeMillis(),
                                    MultimediaContents.OPERATIONTYPEP, setsku};
                            if(Validating.areSet(mult)) {
                                if (!MultimediaContents.isMediaStored(getContext(), setsku)) {
                                    boolean stored = MultimediaContents.storeMultimedia(getContext(), mult);
                                    if (stored) {
                                        System.out.println("media stored ? " + stored);
                                    }
                                }
                            }
                            adapter = new ItemListAdapter(getActivity(), getList(), 2);
                            adapter.notifyDataSetChanged();
                            lv.setAdapter(adapter);
                            Toast.makeText(dialog.getContext(), getString(R.string
                                    .itemupdated), Toast.LENGTH_SHORT).show();
                            dialog.dismiss();
                        }
                    } else {
                        Toast.makeText(dialog.getContext(), getString(R.string
                                .strprodexists), Toast.LENGTH_SHORT).show();
                    }
                }
            }
        });

        dialog.show();
    }


    public List<ItemList> getList() {
        ContentResolver cr = getActivity().getContentResolver();
        String where = Products.SYNCSTATUS + " != 3";
        Cursor c = cr.query(Products.BASEURI, null, where, null, Products.ID + " DESC");
        list = new ArrayList<ItemList>();
        try {
            if (c.getCount() > 0) {
                c.moveToFirst();
                do {
                    int colN = c.getColumnIndex(Products.PRODUCTNAME);
                    int colP = c.getColumnIndex(Products.ITEMS_ON_HAND);
                    int colI = c.getColumnIndex(Products.ID);
                    int colD = c.getColumnIndex(Products.SKU);
                    int colPr = c.getColumnIndex(Products.UNITPRICE);
                    int colUp = c.getColumnIndex(Products.UPDATE);
                    String n = c.getString(colN);
                    String p = c.getString(colP);
                    long i = c.getLong(colI);
                    String d = c.getString(colD);
                    String pr = c.getString(colPr);
                    String upd = c.getString(colUp);
                    if (!Validating.areSet(upd))
                        upd = getString(R.string.strnever);
                    if (tabletsize) {
                        list.add(new ItemList(n.toUpperCase(Locale.getDefault()), pr, p, i,
                                d, upd));
                    } else {
                        list.add(new ItemList(getString(R.string.strproduct).toUpperCase(Locale.getDefault()) + ": " +
                                n.toUpperCase(Locale.getDefault()),
                                getString(R.string.strprice).toUpperCase(Locale.getDefault()) + ": " +
                                        pr.toUpperCase(Locale.getDefault()),
                                getString(R.string.strtotalqty).toUpperCase(Locale.getDefault()) + ": " + p, i,
                                getString(R.string.strsku).toUpperCase(Locale.getDefault()) + ": " + d,
                                getString(R.string.strlastsell).toUpperCase(Locale.getDefault()) + ": " + upd));
                    }
                } while (c.moveToNext());
            } else {
                String n = getString(R.string.strnodata).toUpperCase(Locale.getDefault());
                String p = " ";
                long i = 0;
                String d = null;
                list.add(new ItemList(n, "", p, i, d));
            }
        } finally {
            if (c != null) {
                c.close();
            }
        }
        return list;
    }

    public ArrayList<ItemList> getListOrderItem(long i) {
        ContentResolver cr = getActivity().getContentResolver();
        String where = Products.ID + " = " + i;
        Cursor c = cr.query(Sales.BASEURI, null, where, null, null);
        ArrayList<ItemList> itemLists = new ArrayList<>(c.getCount());
        if (c.moveToFirst()) {
            do {
                int colN = c.getColumnIndex(Sales.TOTALSALES);
                int colP = c.getColumnIndex(Sales.PAYMENTAMOUNT);
                int colI = c.getColumnIndex(Sales.ID);
                int colD = c.getColumnIndex(Sales.ORDERDATE);
                String n = c.getString(colN);
                String p = c.getString(colP);
                long ii = c.getLong(colI);
                String d = c.getString(colD);

                itemLists.add(new ItemList(getString(R.string.strordervalue).toUpperCase(Locale.getDefault()) + ": " +
                        n.toUpperCase(Locale.getDefault()),
                        "",
                        getString(R.string.stramountpaid).toUpperCase(Locale.getDefault()) + ": " + p, i,
                        getString(R.string.strorderdate).toUpperCase(Locale.getDefault()) + ": " + d));
            } while (c.moveToNext());
        }
        ArrayList<ItemList> itemlist = new ArrayList<>();
        return itemlist;
    }

    @TargetApi(Build.VERSION_CODES.ICE_CREAM_SANDWICH)
    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {

        inflater.inflate(R.menu.add_product, menu);

        item_importstock = menu.findItem(R.id.action_addstock_full);
        item_receivestock = menu.findItem(R.id.action_import_stock);
        String apptype = Settings.getApptypeEnabled(getActivity());
        if (apptype.equals(Constants.APPTYPE_SALE)) {
            item_importstock.setVisible(false);
            item_receivestock.setVisible(false);
        }

        searchManager = (SearchManager) getActivity().getSystemService(Context.SEARCH_SERVICE);

        searchView = (SearchView) menu.findItem(R.id.action_search).getActionView();
        searchView.setSearchableInfo(searchManager.getSearchableInfo(getActivity().getComponentName()));
        super.onCreateOptionsMenu(menu, inflater);
    }

    @TargetApi(Build.VERSION_CODES.ICE_CREAM_SANDWICH)
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_addproduct:
                if (!Settings.getAddProdEnabled(getActivity())) {
                    Toast.makeText(getActivity(), getString(R.string.strnopermissionwarn), Toast.LENGTH_SHORT).show();
                } else {
                    int product = Products.getCount(getActivity());
                     int acctype = appPreference.get_accounttype();
                    if (acctype == 2 && product >= 5) {
                        Toast.makeText(getActivity(), getString(R.string.strprodwarn), Toast.LENGTH_SHORT).show();
                    } else {
                        addproduct();
                    }
                }
                return true;
            case R.id.action_addstock_full:
                if (!Settings.getPuchItemEnabled(getActivity())) {
                    Toast.makeText(getActivity(), getString(R.string.strnopermissionwarn), Toast.LENGTH_SHORT).show();
                } else {
                    int prod = Products.getCount(getActivity());
                    if (prod > 0) {
                        Intent intent = new Intent(getActivity(), StockActivity.class);
                        startActivityForResult(intent, ADDSTOCK);
                        return true;
                    } else {
                        Toast.makeText(getActivity(), getString(R.string.strnoprod), Toast.LENGTH_SHORT).show();
                    }
                }
                break;
            case R.id.action_search:
                getActivity().onSearchRequested();
                return true;
            case R.id.action_import_products:
                if (!Settings.getAddProdEnabled(getActivity())) {
                    Toast.makeText(getActivity(), getString(R.string.strnopermissionwarn), Toast.LENGTH_SHORT).show();
                } else {
                    IMPORT_SOURCE = 1;
                    Intent intent = new Intent("SELECT_FILE_ACTION", null, getActivity(), PickFile.class);
                    intent.putExtra("showCannotRead", false);
                    startActivityForResult(intent, REQUEST_CODE_PICK_FILE);
                }
                break;
            case R.id.action_import_stock:
                if (!Settings.getPuchItemEnabled(getActivity())) {
                    Toast.makeText(getActivity(), getString(R.string.strnopermissionwarn), Toast.LENGTH_SHORT).show();
                } else {
                    IMPORT_SOURCE = 2;
                    Intent inte = new Intent("SELECT_FILE_ACTION", null, getActivity(), PickFile.class);
                    inte.putExtra("showCannotRead", false);
                    startActivityForResult(inte, REQUEST_CODE_PICK_FILE);
                }
                break;
        }


        return super.onOptionsItemSelected(item);
    }

    private  void takephoto(final ImageView img, final TextView txtp) {
        LayoutInflater infl = getActivity().getLayoutInflater();
        View view = infl.inflate(R.layout.camerascreen, null);
        dg = new Dialog(getActivity(), R.style.CustomDialog);
        dg.setCancelable(false);
        dg.setContentView(view);
        dg.show();//prodname,reorder,price,tax,desc,sku

        try {
            mCamera = Camera.open();//you can use open(int) to use different cameras
        } catch (Exception e) {
            Log.d("ERROR", "Failed to get camera: " + e.getMessage());
        }

        if (mCamera != null) {
            mCameraView = new PhotoPreview(dg.getContext(), mCamera);//create a SurfaceView to show camera data
            FrameLayout camera_view = (FrameLayout) dg.findViewById(R.id.camera_view);
            camera_view.addView(mCameraView);//add the SurfaceView to the layout
        }

        ImageButton imgClose = (ImageButton) dg.findViewById(R.id.imgClose);
        ImageButton imgSave = (ImageButton) dg.findViewById(R.id.imgTake);
        imgClose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Log.v("log_tag", "Panel Canceled");
                dg.dismiss();
            }

        });


        imgSave.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Camera.PictureCallback mPicture = new Camera.PictureCallback() {

                    @Override
                    public void onPictureTaken(byte[] data, Camera camera) {

                        File pictureFile = getOutputMediaFile(MEDIA_TYPE_IMAGE);
                        if (pictureFile == null) {
                            return;
                        }

                        try {
                            FileOutputStream fos = new FileOutputStream(pictureFile);
                            fos.write(data);
                            fos.close();
                        } catch (FileNotFoundException e) {
                        } catch (IOException e) {
                        }
                        if (pictureFile != null) {
                            String path = pictureFile.getAbsolutePath().toString();
                            System.out.println(path);
                            //return/save image here
                            prodphoto = path;
                            Bitmap bitmap = Constants.decodeImg(prodphoto,300,300);
                            img.setImageBitmap(bitmap);
                            img.setVisibility(View.VISIBLE);
                            txtp.setVisibility(View.GONE);
                            img.setRotation(90);
                            dg.dismiss();
                        }
                    }

                    private File getOutputMediaFile(int type) {
                        File sdcard = Environment.getExternalStorageDirectory();

                        File folder = new File(sdcard.getAbsoluteFile(), "proregister");
                        if (!folder.exists())
                            folder.mkdir();
                        String filename = String.valueOf(System.currentTimeMillis());
                        File file = new File(folder.getAbsoluteFile(), filename + ".jpg");

                        return file;
                    }
                };

                mCamera.takePicture(null, null, mPicture);
            }

        });

    }


    @TargetApi(Build.VERSION_CODES.ICE_CREAM_SANDWICH)
    private void addproduct() {
        LayoutInflater infl = getActivity().getLayoutInflater();
        View view = infl.inflate(R.layout.addproduct, null);
        final String[] category = {""}; final String[] vatarray = {""};
        dialog = new Dialog(getActivity(), R.style.CustomDialog);
        dialog.setCancelable(false);
        dialog.setContentView(view);
        dialog.show();//prodname,reorder,price,tax,desc,sku
        final EditText etprodname, etreorder, etprice, etsku, etdesc, etcat;
        final Spinner ettax = (Spinner)dialog.findViewById(R.id.ettax);
        final CheckBox chekservice, chktax;
        final TextView txttile,txtphoto;
        final ImageView imgphoto;
        final boolean[] isservice = {false};
        final boolean[] istaxinclusive = {true};
        final Spinner spinner = (Spinner) dialog.findViewById(R.id.spcat);
        etprodname = (EditText) dialog.findViewById(R.id.etprodname);
        etreorder = (EditText) dialog.findViewById(R.id.etreorder);
        etprice = (EditText) dialog.findViewById(R.id.etunitprice);

        etsku = (EditText) dialog.findViewById(R.id.etsku);
        etdesc = (EditText) dialog.findViewById(R.id.etdescr);
        etcat = (EditText) dialog.findViewById(R.id.etcat);
        txttile = (TextView) dialog.findViewById(R.id.txttitle);
        chekservice = (CheckBox) dialog.findViewById(R.id.chkservice);
        chktax = (CheckBox) dialog.findViewById(R.id.chktaxmethod);
        txttile.setText(getString(R.string.straddproduct));
        txttile.setAllCaps(true);


        txtphoto = (TextView) dialog.findViewById(R.id.txtaddphoto);
        imgphoto = (ImageView) dialog.findViewById(R.id.imgproductphoto);

        txtphoto.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                takephoto(imgphoto,txtphoto);
            }
        });

        final ArrayList<String> vatlist = new ArrayList<>(Arrays.asList(getResources().getStringArray(R.array
                .strtaxrate)));
        final ArrayList<String> list = Product_Categories.getProductCats(dialog.getContext());
        final ArrayList<String> listid = Product_Categories.getProductCatsId(dialog.getContext());
        final ArrayList<String> vatlistval = new ArrayList<>(Arrays.asList(getResources().getStringArray(R.array
                .strtaxrateval)));

        chekservice.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked)
                    isservice[0] = true;
                if (!isChecked)
                    isservice[0] = false;
            }
        });

        chktax.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked)
                    istaxinclusive[0] = true;
                if (!isChecked)
                    istaxinclusive[0] = false;
            }
        });


        final ArrayAdapter spinneradapter = new ArrayAdapter(dialog.getContext(), android.R.layout
                .simple_list_item_1, list);
        spinneradapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinner.setAdapter(spinneradapter);
        spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                if (position != 0 || position < list.size() - 1)
                    category[0] = listid.get(position);

                if (position == list.size() - 1) {
                    etcat.setVisibility(View.VISIBLE);
                    spinner.setVisibility(View.GONE);
                    etcat.requestFocus();
                    category[0] = "0";
                }
                System.out.println("cat is " + category[0]);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        final ArrayAdapter vatadapter = new ArrayAdapter(dialog.getContext(), android.R.layout
                .simple_list_item_1, vatlist);
        vatadapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        ettax.setAdapter(vatadapter);
        ettax.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                    vatarray[0] = vatlistval.get(position);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        ImageView goback = (ImageView) dialog.findViewById(R.id.goback);
        goback.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });
        sub = (Button) dialog.findViewById(R.id.btnSubmit);
        sub.setVisibility(View.VISIBLE);


        sub.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                submit();
            }

            private void submit() {
                String setprodname, setreorder, setprice, settax, setsku, setdesc, stcat = "",
                        stservice = null, sttaxmethod = null;
                setprodname = etprodname.getText().toString().trim();
                setreorder = etreorder.getText().toString().trim();
                if(!setreorder.isEmpty())
                    if (!Constants.checkDouble(getActivity(), setreorder, etreorder))
                        return;
                setprice = etprice.getText().toString().trim();
                if(!setprice.isEmpty())
                    if (!Constants.checkDouble(getActivity(), setprice, etprice))
                        return;

                if (!Constants.checkInvalidChars(getActivity(), setprodname, etprodname))
                    return;
                setsku = etsku.getText().toString().trim();
                if(!setsku.isEmpty())
                    if (!Constants.checkInvalidChars(getActivity(), setsku, etsku))
                        return;
                setdesc = etdesc.getText().toString();
                if (etcat.getVisibility() == View.VISIBLE) {
                    String stcat1 = etcat.getText().toString();
                    if (!stcat1.isEmpty()) {
                        if (!Product_Categories.isCatPresent(dialog.getContext(), stcat1.toUpperCase()
                                .trim(), Constants.LOCAL)) {
                            String[] vals = {"0", stcat1.toUpperCase().trim()};
                            if (Product_Categories.storeWCat(dialog.getContext(), vals, Constants.LOCAL) == 1)
                                stcat = Product_Categories.getId(dialog.getContext());
                        } else {
                            Toast.makeText(dialog.getContext(), "This category exists please select it " +
                                    "from the list instead", Toast.LENGTH_SHORT).show();
                            etcat.setVisibility(View.GONE);
                            spinner.setVisibility(View.VISIBLE);
                            int spinerpos = spinneradapter.getPosition(stcat1);
                            spinner.setSelection(spinerpos);
                            return;
                        }
                    }
                }
                if (etcat.getVisibility() == View.GONE)
                    stcat = category[0];
                if (vatarray != null && vatarray.length ==1)
                    settax = vatarray[0];
                else
                    settax = vatlistval.get(0);
                if (isservice[0])
                    stservice = Constants.PRODUCT_SERVICE;
                if (!isservice[0])
                    stservice = Constants.PRODUCT_STANDARD;
                if (istaxinclusive[0])
                    sttaxmethod = "0";
                if (!istaxinclusive[0])
                    sttaxmethod = "1";
                setsku.replace(" ", "").trim();

                if (stcat.equals(getString(R.string.strselectcategory))) {
                    if (spinner.getVisibility() == View.VISIBLE)
                        spinner.requestFocus();
                    else
                        etcat.requestFocus();
                    return;
                }
                String[] check = {setprodname, setprice, stcat};

                if (setdesc.isEmpty())
                    setdesc = "No description";
                           /* for(int i = 0; i< vals.length; i++){
                                if(i != 0 || i != 5 || i != 6){
                                    vals[i].trim();
                                }
                                vals[i].replace("'","\'");
                            }*/
                Log.v("cat", "cat is " + stcat);
                if (Validating.areSet(check)) {
                    int prodskuuniq = 0;
                    int prodexist = Products.isProductPresent(dialog.getContext(), new String[]{setprodname});
                    if (setsku.isEmpty()) {
                        prodskuuniq = 0;
                        setsku = String.valueOf(System.currentTimeMillis());
                    } else {
                        prodskuuniq = Products.isProductPresent(dialog.getContext(), new
                                String[]{setprodname, " ", setsku});
                    }
                    if (prodskuuniq == 1) {
                        etsku.setHint(getString(R.string.strskuwarn));
                        etsku.setText("");
                        etsku.setHintTextColor(Color.RED);
                        etsku.requestFocus();
                        return;
                    }
                    if (prodexist == 0) {
                        if(setreorder.isEmpty())
                            setreorder = "20";
                        if(settax.isEmpty())
                            settax = "18";
                        String[] vals = {setprodname, setreorder, setprice, settax, setsku, setdesc, stcat,
                                stservice, "co128", sttaxmethod,prodphoto};
                        System.out.println("sku is " + setsku);
                        int insert = Products.insertProduct(dialog.getContext(), vals, Constants.LOCAL);
                        if (insert == 1) {
                            String[] mult = {prodphoto, setprodname + "_" + System.currentTimeMillis(),
                                    MultimediaContents.OPERATIONTYPEP, setsku};
                            if(Validating.areSet(mult)) {
                                if (!MultimediaContents.isMediaStored(getContext(), setsku)) {
                                    boolean stored = MultimediaContents.storeMultimedia(getContext(), mult);
                                    if (stored) {
                                        System.out.println("media stored ? " + stored);
                                    }
                                }
                            }
                            adapter = new ItemListAdapter(getActivity(), getList(), 2);
                            adapter.notifyDataSetChanged();
                            lv.setAdapter(adapter);
                            Toast.makeText(dialog.getContext(), getString(R.string
                                    .strproductadd), Toast.LENGTH_SHORT).show();
                            dialog.dismiss();
                        }
                    } else {
                        Toast.makeText(dialog.getContext(), getString(R.string
                                .strprodexists), Toast.LENGTH_SHORT).show();
                    }
                }
            }
        });
    }


    @Override
    public void onItemClick(AdapterView<?> parent, View view, final int position, final long id) {
        ItemList _list = list.get(position);
        String name = _list.getitemname();
        long i = _list.getId();
        if (i > 0)
            showProduct(i, name, 0);
//        if (i > 0)
//            showProduct(i, name, 1);
    }

    @Override
    public void onActivityResult(int reqCode, int resCode, Intent data) {
        getActivity();
        switch (reqCode) {
            case ADDSTOCK:
                if (resCode == Activity.RESULT_OK) {
                    adapter.notifyDataSetChanged();
                    adapter = new ItemListAdapter(getActivity(), getList(), 2);
                    lv.setAdapter(adapter);
                    Toast.makeText(getActivity(), getString(R.string.strstockadded), Toast
                            .LENGTH_SHORT)
                            .show();
                } else if (resCode == Activity.RESULT_CANCELED) {
                    Toast.makeText(getActivity(), getString(R.string.strstockcancl), Toast
                            .LENGTH_SHORT).show();
                }
                break;
            case REQUEST_CODE_PICK_FILE:
                System.out.println("import " + IMPORT_SOURCE);
                if (resCode == Activity.RESULT_OK) {
                    final String str1 = data.getStringExtra(PickFile.returnFileParameter);
                    appPreference.saveDbPath(str1);
                    Log.v("filepath", "filepath path is " + str1);
                    String[] arrayOfString = str1.split("/");
                    final String str2 = arrayOfString[(-1 + arrayOfString.length)];

                    if (Constants.validateCsv(str2)) {
                        if (IMPORT_SOURCE == 1){
                            /*new Thread(){
                                public void run(){
                                    getActivity().runOnUiThread(new Runnable() {
                                        @Override
                                        public void run() {
                                            import_products(new String[]{str2, str1});
                                        }
                                    });
                                }
                            }.start();*/
                            new ImportItems(getActivity(),adapter,lv,Constants.SEARCHSOURCE_PRODUCTS).execute(new String[]{str2, str1});
                        }

                        if(IMPORT_SOURCE == 2){
                            new Thread()
                            {
                                public void run()
                                {
                                    getActivity().runOnUiThread(new Runnable()
                                    {
                                        public void run()
                                        {
                                            import_items(new String[]{str2, str1});
                                        }
                                    });
                                }
                            }.start();
                        }

                    } else {
                        Toast.makeText(getActivity(), getString(R.string.strfilepickerformatwarn),
                                Toast.LENGTH_SHORT).show();
                    }
                } else {
                    Toast.makeText(
                            getActivity(),
                            getString(R.string.strfilepickerwarn),
                            Toast.LENGTH_SHORT).show();
                }
                IMPORT_SOURCE = 0;
                break;
            case GETPRODUCTSKU:

                break;
        }
    }


    private void import_items(String[] params){

        CsvReader reader = null;
        try {
            reader = new CsvReader(params[1]);
            boolean readh = reader.readHeaders();

            String batch = Constants.generateBatch();
            while (reader.readRecord()) {
                if (readh) {
                    if(Warehouse.getCount(getActivity()) == 0){
                        String[] vals = {"1","DEMO_WH","DEMO WAREHOUSE",""};
                        Warehouse.storeWh(getActivity(), vals);
                    }
                    reader.getSkipEmptyRecords();
                    reader.getTrimWhitespace();
                    String[] titles = reader.getHeaders();
                    String prn = "",prdid = "", prqty = "", prunitpurc = "",pexpir = "", whid = "";
                    for (String s : titles) {
                        System.out.println(s);
                        if (s.equals(getActivity().getString(R.string.strproductname)))
                            prn = reader.get(getActivity().getString(R.string.strproductname));
                        if (s.equals(getActivity().getString(R.string.strpurchaseprice)))
                            prunitpurc = reader.get(getActivity().getString(R.string.strpurchaseprice));
                        if (s.equals(getActivity().getString(R.string.strquantity)))
                            prqty = reader.get(getActivity().getString(R.string.strquantity));
                        if (s.equals(getActivity().getString(R.string.strexpirydt)))
                            pexpir = reader.get(getActivity().getString(R.string.strexpirydt));
                    }

                    if(!prn.isEmpty())
                        if(!Constants.checkInvalidChars(prn)) {
                            Toast.makeText(getActivity(),getString(R.string.strinvalidcharacterwarn)+" "+prn,Toast
                                    .LENGTH_SHORT).show();
                            reader.skipLine();
                        }

                    if(!prn.isEmpty()){
                        String prc = prn.replace(" ","").trim().toUpperCase();
                        int proid = Products.getLocalProdid(getActivity(), prc);
                        prdid = String.valueOf(proid);
                    }

                    if(prdid.equals(0))
                        Toast.makeText(getActivity(),getString(R.string.strinvalidproductwarn)+" "+prn,Toast
                                .LENGTH_SHORT).show();

                    if(!pexpir.isEmpty())
                        if(!Constants.checkInvalidChars(pexpir)) {
                            Toast.makeText(getActivity(),getString(R.string.strinvalidcharacterwarn)+" "+pexpir,Toast
                                    .LENGTH_SHORT).show();
                            reader.skipLine();
                        }
                    ArrayList<String> whids = Warehouse.getAllIds(getActivity());
                    if(Warehouse.getCount(getActivity()) ==1){
                        whid = whids.get(1);
                        System.out.println("wh is "+whid);
                    }
                    SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd");
                    try {
                        if(!pexpir.isEmpty()) {
                            Date expd = df.parse(pexpir);
                            pexpir = String.valueOf(expd);
                        }
                    } catch (ParseException e) {
                        e.printStackTrace();
                    }

                    if(!prqty.isEmpty())
                        if(!Constants.checkInteger(prqty)){
                            Toast.makeText(getActivity(),getString(R.string.numberwarn)+" "+prqty,Toast
                                    .LENGTH_SHORT).show();
                            reader.skipLine();
                        }
                    if(!prqty.isEmpty())
                        if(!Constants.checkIntegerStrictly(prqty)){
                            Toast.makeText(getActivity(),getString(R.string.numberwarn)+" "+prqty,Toast
                                    .LENGTH_SHORT).show();
                            reader.skipLine();
                        }
                    if(!prunitpurc.isEmpty())
                        if(!Constants.checkIntegerStrictly(prunitpurc)){
                            Toast.makeText(getActivity(),getString(R.string.numberwarn)+" "+prunitpurc,Toast
                                    .LENGTH_SHORT).show();
                            reader.skipLine();
                        }
                    if(!prunitpurc.isEmpty())
                        if(!Constants.checkInteger(prunitpurc)){
                            Toast.makeText(getActivity(),getString(R.string.numberwarn)+" "+prunitpurc,Toast
                                    .LENGTH_SHORT).show();
                            reader.skipLine();
                        }

                    String[] check =  new String[]{prdid, batch,whid,prqty};
                    if(Validating.areSet(check) && !whid.equals("0") && !prdid.equals("0")){
                        if(pexpir.isEmpty())
                            pexpir = "2030-05-05";
                        if(prunitpurc.isEmpty())
                            prunitpurc = "0";

                        String[] val =  new String[]{prdid, batch,whid,prqty,pexpir,prunitpurc};
                        boolean insert = PurchaseItems.insert(getActivity(), val, Constants.LOCAL);

                        if (insert) {
                            Products.updateProductQty(getActivity(), new int[]{new BigDecimal(prdid).intValue(),
                                    new BigDecimal(prqty).intValue(), 0});
                            Toast.makeText(getActivity(),getString(R.string.strqtyupdated)+" "+prn,Toast
                                    .LENGTH_SHORT).show();
                            adapter = new ItemListAdapter(getActivity(), getList(), 2);
                            adapter.notifyDataSetChanged();
                            lv.setAdapter(adapter);
                        }
                    } else {

                        if (prn.isEmpty())
                            Toast.makeText(getActivity(),getString(R.string.strprodnamewarn)+" "+prn,Toast
                                    .LENGTH_SHORT).show();
                        if (prqty.isEmpty())
                            Toast.makeText(getActivity(),getString(R.string.strqtywarn)+" "+prqty,Toast
                                    .LENGTH_SHORT).show();
                    }


                } else {
                    Toast.makeText(getActivity(),getString(R.string.strcolumnwarn),Toast
                            .LENGTH_SHORT).show();
                }
            }
            reader.close();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    private void import_products(String[] params){

        CsvReader reader = null;
        try {
            reader = new CsvReader(params[1]);
            boolean readh = reader.readHeaders();

            while (reader.readRecord()) {
                if (readh) {
                    reader.getSkipEmptyRecords();
                    reader.getTrimWhitespace();
                    String[] titles = reader.getHeaders();
                    String pnm = "", punitpr = "", pcat = "", ptype = "", ptxr = "", ptaxmeth = "", psku = "",
                            pdes = "", preord = "";

                    for (String s: titles) {
                        if (s.equals(getString(R.string.strproductname)))
                            pnm = reader.get(getString(R.string.strproductname));
                        if (s.equals(getString(R.string.strunitprices)))
                            punitpr = reader.get(getString(R.string.strunitprices));
                        if (s.equals(getString(R.string.strcategory)))
                            pcat = reader.get(getString(R.string.strcategory));
                        if (s.equals(getString(R.string.strproducttype)))
                            ptype = reader.get(getString(R.string.strproducttype));
                        if (s.equals(getString(R.string.strtaxrate)))
                            ptxr = reader.get(getString(R.string.strtaxrate));
                        if (s.equals(getString(R.string.strtaxmethod)))
                            ptaxmeth = reader.get(getString(R.string.strtaxmethod));
                        if (s.equals(getString(R.string.strsku)))
                            psku = reader.get(getString(R.string.strsku));
                        if (s.equals(getString(R.string.strprodesc)))
                            pdes = reader.get(getString(R.string.strprodesc));
                        if (s.equals(getString(R.string.strreoderval)))
                            preord = reader.get(getString(R.string.strreoderval));
                    }

                    if (!pnm.isEmpty())
                        if (!Constants.checkInvalidChars(pnm)) {
                            Toast.makeText(getActivity(),getString(R.string.strinvalidcharacterwarn)+" "+pnm,Toast
                                    .LENGTH_SHORT).show();
                         reader.skipRecord();
                            System.out.println(reader.skipLine());
                            System.out.println(reader.skipRecord());
                        }
                    if (!psku.isEmpty())
                        if (!Constants.checkInvalidChars(psku)) {
                            Toast.makeText(getActivity(),getString(R.string.strinvalidcharacterwarn)+" "+psku,Toast
                                    .LENGTH_SHORT).show();
                            reader.skipLine();
                        }
                    if (!pcat.isEmpty())
                        if (!Constants.checkInvalidChars(pcat)) {
                            Toast.makeText(getActivity(),getString(R.string.strinvalidcharacterwarn)+" "+pcat,Toast
                                    .LENGTH_SHORT).show();
                            reader.skipLine();
                        }
                    if (!ptype.isEmpty())
                        if (!Constants.checkInvalidChars(ptype)) {
                            Toast.makeText(getActivity(),getString(R.string.strinvalidcharacterwarn)+" "+ptype,Toast
                                    .LENGTH_SHORT).show();
                            reader.skipLine();
                        }
                    if (!ptaxmeth.isEmpty())
                        if (!Constants.checkInvalidChars(ptaxmeth)) {
                            Toast.makeText(getActivity(),getString(R.string.strinvalidcharacterwarn)+" "+ptaxmeth,Toast
                                    .LENGTH_SHORT).show();
                            reader.skipLine();
                        }
                    if (!preord.isEmpty())
                        if (!Constants.checkInteger(preord)) {
                            Toast.makeText(getActivity(),getString(R.string.strinvalidcharacterwarn)+" "+preord,Toast
                                    .LENGTH_SHORT).show();
                            reader.skipLine();
                        }
                    if (!preord.isEmpty())
                        if (!Constants.checkIntegerStrictly(preord)) {
                            Toast.makeText(getActivity(),getString(R.string.numberwarn)+" "+preord,Toast
                                    .LENGTH_SHORT).show();
                            reader.skipLine();
                        }
                    if (!punitpr.isEmpty())
                        if (!Constants.checkInteger(punitpr)) {
                            Toast.makeText(getActivity(),getString(R.string.numberwarn)+" "+punitpr,Toast
                                    .LENGTH_SHORT).show();
                            reader.skipLine();
                        }
                    if (!punitpr.isEmpty())
                        if (!Constants.checkIntegerStrictly(punitpr)) {
                            Toast.makeText(getActivity(),getString(R.string.numberwarn)+" "+punitpr,Toast
                                    .LENGTH_SHORT).show();
                            reader.skipLine();
                        }
                    if (!ptxr.isEmpty())
                        if (!Constants.checkInteger(ptxr)) {
                            Toast.makeText(getActivity(),getString(R.string.numberwarn)+" "+ptxr,Toast
                                    .LENGTH_SHORT).show();
                            reader.skipLine();
                        }
                    if (!ptxr.isEmpty())
                        if (!Constants.checkIntegerStrictly(ptxr)) {
                            Toast.makeText(getActivity(),getString(R.string.numberwarn)+" "+ptxr,Toast
                                    .LENGTH_SHORT).show();
                            reader.skipLine();
                        }

                    System.out.println("hit here");
                    String[] pcheck = {pnm, punitpr};
                    if (Validating.areSet(pcheck)) {//if necessary fields are not null
                        System.out.println("prod and unit price entered");
                        if (ptxr.isEmpty())
                            ptxr = "18";
                        if (ptaxmeth.isEmpty())
                            ptaxmeth = "0";
                        if (pcat.isEmpty())
                            pcat = "General";
                        if (ptype.isEmpty())
                            ptype = Constants.PRODUCT_STANDARD;
                        if (preord.isEmpty())
                            preord = "50";
                        if (!ptaxmeth.equals(Constants.TAXMETHOD_EXC) || !ptaxmeth.equals(Constants
                                .TAXMETHOD_INCL))
                            ptaxmeth = "0";
                        if (!ptype.equals(Constants.PRODUCT_SERVICE) || !ptype.equals(Constants
                                .PRODUCT_STANDARD))
                            ptype = Constants.PRODUCT_STANDARD;
                        String stcat = "0";
                        if (!Product_Categories.isCatPresent(getActivity(), pcat.toUpperCase().trim(), Constants.LOCAL)) {
                            String[] vals = {"0", pcat.toUpperCase().trim()};
                            if (Product_Categories.storeWCat(getActivity(), vals, Constants.LOCAL) == 1)
                                stcat = Product_Categories.getId(getActivity());
                            System.out.println("cat id for " + pcat + " " + stcat);
                        } else {
                            stcat = Product_Categories.getLocId(getActivity(), pcat);
                        }
                        String prodcd = pnm.toUpperCase();
                        prodcd = prodcd.replace(" ", "");

                        int prodskuuniq = 0;

                        psku.replace(" ", "").trim();
                        if (psku.isEmpty()) {
                            prodskuuniq = 0;
                            psku = String.valueOf(System.currentTimeMillis());
                        } else {
                            prodskuuniq = Products.isProductPresent(getActivity(), new
                                    String[]{prodcd, " ", psku});
                        }
                        if (prodskuuniq == 1) {
                            Toast.makeText(getActivity(),getString(R.string.strskuwarn)+" "+pnm,Toast
                                    .LENGTH_SHORT).show();
                        }

                        int prodexist = Products.isProductPresent(getActivity(), new String[]{prodcd});
                        int prodid = Products.getProdid(getActivity(), prodcd);
                        System.out.println(" prodesists " + prodexist + " stcat " + stcat + " prodid " + prodid);
                        if(stcat.equals(0)) {
                            Toast.makeText(getActivity(), getString(R.string.strcatsavewarn) + " " + pcat, Toast
                                    .LENGTH_SHORT).show();
                            reader.skipLine();
                        }

                        if (!stcat.equals("0"))
                            if (prodexist == 0) {
                                String[] prod = {pnm, preord, punitpr, ptxr, psku, pdes, stcat, ptype, "code128",
                                        ptaxmeth,""};
                                if (Products.insertProduct(getActivity(), prod, Constants.LOCAL) == 1){

                                    Toast.makeText(getActivity(),getString(R.string.strproductadded)+" "+pnm,Toast
                                            .LENGTH_SHORT).show();
                                    adapter = new ItemListAdapter(getActivity(), getList(), 2);
                                    adapter.notifyDataSetChanged();
                                    lv.setAdapter(adapter);

                                }
                            } else {
                                if (prodid != 0) {
                                    String[] v = {pnm, preord, punitpr, ptxr, psku, pdes, String.valueOf(prodid),
                                            stcat, ptype, "code128", ptaxmeth,""};
                                    if (Products.updateProduct(getActivity(), v, Constants.LOCAL))
                                    {

                                        Toast.makeText(getActivity(),getString(R.string.strproductupdated)+" "+pnm,Toast
                                                .LENGTH_SHORT).show();
                                        adapter = new ItemListAdapter(getActivity(), getList(), 2);
                                        adapter.notifyDataSetChanged();
                                        lv.setAdapter(adapter);
                                    }
                                }
                            }
                    } else {
                        if (pnm.isEmpty())
                            Toast.makeText(getActivity(),getString(R.string.strprodnamewarn)+" "+pnm,Toast
                                    .LENGTH_SHORT).show();
                    }


                } else {
                    Toast.makeText(getActivity(),getString(R.string.strcolumnwarn),Toast
                            .LENGTH_SHORT).show();
                }
            }
            reader.close();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

    }
}
