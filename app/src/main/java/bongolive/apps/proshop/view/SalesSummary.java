package bongolive.apps.proshop.view;

import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.ViewPager;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.ArrayList;

import bongolive.apps.proshop.R;
import bongolive.apps.proshop.controller.AppPreference;
import bongolive.apps.proshop.controller.SalesSummaryProductsAdapter;
import bongolive.apps.proshop.controller.Validating;
import bongolive.apps.proshop.model.Customers;
import bongolive.apps.proshop.model.Pending_Items;
import bongolive.apps.proshop.model.Products;
import bongolive.apps.proshop.model.Sales;
import bongolive.apps.proshop.model.SalesItems;
import bongolive.apps.proshop.model.Settings;
import bongolive.apps.proshop.controller.Constants;
import bongolive.apps.proshop.controller.GpsTracker;

public class SalesSummary extends AppCompatActivity implements ActionBar.TabListener {

    private ActionBar actionBar;
    static MenuItem msell,mprint;
    /**
     * The {@link android.support.v4.view.PagerAdapter} that will provide
     * fragments for each of the sections. We use a
     * {@link FragmentPagerAdapter} derivative, which will keep every
     * loaded fragment in memory. If this becomes too memory intensive, it
     * may be best to switch to a
     * {@link android.support.v13.app.FragmentStatePagerAdapter}.
     */
    private SectionsPagerAdapter mSectionsPagerAdapter;

    /**
     * The {@link ViewPager} that will host the section contents.
     */
    private ViewPager mViewPager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.sales_summary);

        actionBar = getSupportActionBar();

        // Create the adapter that will return a fragment for each of the three
        // primary sections of the activity.
        mSectionsPagerAdapter = new SectionsPagerAdapter(getSupportFragmentManager());

        // Set up the ViewPager with the sections adapter.
        mViewPager = (ViewPager) findViewById(R.id.container);
        mViewPager.setAdapter(mSectionsPagerAdapter);


        // Set up the action bar.

        actionBar.setNavigationMode(ActionBar.NAVIGATION_MODE_TABS);

        // When swiping between different sections, select the corresponding
        // tab. We can also use ActionBar.Tab#select() to do this if we have
        // a reference to the Tab.
        mViewPager.setOnPageChangeListener(new ViewPager.SimpleOnPageChangeListener() {
            @Override
            public void onPageSelected(int position) {
                actionBar.setSelectedNavigationItem(position);
            }
        });

        // For each of the sections in the app, add a tab to the action bar.
        for (int i = 0; i < mSectionsPagerAdapter.getCount(); i++) {
            // Create a tab with text corresponding to the page title defined by
            // the adapter. Also specify this Activity object, which implements
            // the TabListener interface, as the callback (listener) for when
            // this tab is selected.
            actionBar.addTab(
                    actionBar.newTab()
                            .setText(mSectionsPagerAdapter.getPageTitle(i))
                            .setTabListener(this));
        }

    }



    @Override
    public void onTabSelected(ActionBar.Tab tab, FragmentTransaction fragmentTransaction) {
        // When the given tab is selected, switch to the corresponding page in
        // the ViewPager.
        mViewPager.setCurrentItem(tab.getPosition());
    }

    @Override
    public void onTabUnselected(ActionBar.Tab tab, FragmentTransaction fragmentTransaction) {
    }

    @Override
    public void onTabReselected(ActionBar.Tab tab, FragmentTransaction fragmentTransaction) {
    }

    /**
     * A {@link FragmentPagerAdapter} that returns a fragment corresponding to
     * one of the sections/tabs/pages.
     */
    public class SectionsPagerAdapter extends FragmentPagerAdapter {

        public SectionsPagerAdapter(FragmentManager fm) {
            super(fm);
        }

        @Override
        public Fragment getItem(int position) {
            return PlaceholderFragment.newInstance(position + 1);
        }

        @Override
        public int getCount() {
            return 2;
        }

        @Override
        public CharSequence getPageTitle(int position) {
            switch (position) {
                case 0:
                    return getString(R.string.title_activity_sales_summary);
                case 1:
                    return getString(R.string.stritems);
            }
            return null;
        }
    }

    /**
     * A placeholder fragment containing a simple view
     */
    public static class PlaceholderFragment extends Fragment {
        private static final String ARG_SECTION_NUMBER = "section_number";
        private static int ACTIVE = 0;
        private RecyclerView mRecyclerView;
        private RecyclerView.Adapter mAdapter;
        private RecyclerView.LayoutManager mLayoutManager;
        private RelativeLayout rl;

        ArrayList<Pending_Items> lst = new ArrayList<>();
        private AppPreference appPreference;

        private Spinner sppaymethods;
        private EditText comm,etpartial,etdiscount ;
        private String paymode,mtaxtot,mfinalcost,msubtotal,buz;
        private boolean payinfull = false;
        private TextView txtdiscount,txttaxable,txttaxamo,txtqty,txttot,txtgrandtot,txttitle;

        private int qty = 0, items = 0;
        private double subtotal = 0;
        private double txttotal = 0;
        private double gtotal = 0;
        private static MenuItem sell,print;
        private GpsTracker gpsTracker;


        /* this is the final insert variables */
        String cust ;
        String custloc ;
        String paid  ;
        double totalsales = 0;
        double totaltax = 0;
        double totaldiscount = 0;
        double paidamount = 0;
        int totalitems = 0;
        String batch ;
        boolean taxset ;
        String printed ;
        String paystatus = null;
        String lat ;
        String lng ;
        String note;
        /*end of final insert variables*/

        /**
         * Returns a new instance of this fragment for the given section
         * number.
         */
        public static PlaceholderFragment newInstance(int sectionNumber) {
            PlaceholderFragment fragment = new PlaceholderFragment();
            Bundle args = new Bundle();
            args.putInt(ARG_SECTION_NUMBER, sectionNumber);
            fragment.setArguments(args);
            return fragment;
        }


        public PlaceholderFragment() {
        }

        @Override
        public View onCreateView(LayoutInflater inflater, ViewGroup container,
                                 Bundle savedInstanceState) {
            Bundle bundle = getArguments();
             ACTIVE = bundle.getInt(ARG_SECTION_NUMBER);
            View rootView = null;
            if(ACTIVE == 1)
                rootView = inflater.inflate(R.layout.single_summary, container, false);
            else if(ACTIVE == 2)
                rootView = inflater.inflate(R.layout.selling_screen, container, false);

            return rootView;
        }

        @Override
        public void onActivityCreated(Bundle savedInstanceState) {
            super.onActivityCreated(savedInstanceState);
            lst = Pending_Items.getItems(getActivity(), Constants.SALETYPE_SALE);
            setHasOptionsMenu(true);
            gpsTracker = new GpsTracker(getContext());
            lat = String.valueOf(gpsTracker.getLatitude()); // done
            lng = String.valueOf(gpsTracker.getLongitude());// done
            appPreference = new AppPreference(getContext());
            if(ACTIVE == 1){
                sppaymethods = (Spinner)getActivity().findViewById(R.id.sppayoptions);
                comm = (EditText)getActivity().findViewById(R.id.etnotes);
                etdiscount = (EditText)getActivity().findViewById(R.id.txtdiscount_valued);
                etpartial = (EditText)getActivity().findViewById(R.id.etpartialpay);

                txtdiscount = (TextView)getActivity().findViewById(R.id.txttotald_value);
                txttaxable = (TextView)getActivity().findViewById(R.id.txttax_value);
                txttaxamo = (TextView)getActivity().findViewById(R.id.txttaxamount_value);
                txtqty = (TextView)getActivity().findViewById(R.id.txtqty_value);
                txttot = (TextView)getActivity().findViewById(R.id.txtamount_value);
                txtgrandtot = (TextView)getActivity().findViewById(R.id.txttotal_value);
                txttitle = (TextView)getActivity().findViewById(R.id.txtcustomer);

                long custid = appPreference.getBuz();
                custloc = String.valueOf(custid);// done
                cust = Customers.getSysCustidBasedOnLocalId(getContext(), custloc);// done
                if(custid != 0)
                    buz = Customers.getCustomerName(getContext(), custid);

                txttitle.setText(buz);
                qty = lst.size();

                for(int i = 0; i < lst.size(); i++){
                    Pending_Items pt = lst.get(i);
                    items += pt.getQty();
                    totaltax += pt.getTaxmt();
                    gtotal += pt.getSbtotal();
                }

                batch = "SALE"+Constants.generateBatch()+"-"+Constants.getDateOnly2();
                taxset = Settings.getTaxEnabled(getContext()); // done

                totalitems = items; // done
                totalsales = gtotal;// done

                setAssignments();

                etdiscount.setOnKeyListener(new View.OnKeyListener() {
                    @Override
                    public boolean onKey(View v, int keyCode, KeyEvent event) {
                        if (event.getAction() == KeyEvent.ACTION_DOWN && event.getKeyCode() == KeyEvent.KEYCODE_ENTER) {
                            String prc = etdiscount.getText().toString();
                            if (Validating.areSet(prc)) {
                                double maxdiscount = Settings.getDiscountLimit(getContext());

                                double pr = new BigDecimal(prc).setScale(2, RoundingMode.HALF_UP).doubleValue();
                                double discount = 0;
                                if (pr <= maxdiscount) {
                                    discount = new BigDecimal(pr).divide(new BigDecimal(100)).setScale(2, RoundingMode.HALF_UP)
                                            .doubleValue();
                                    BigDecimal dis = new BigDecimal(totalsales).multiply(new BigDecimal(discount));
                                    totalsales = new BigDecimal(totalsales).subtract(dis).setScale(2, RoundingMode.HALF_UP)
                                            .doubleValue();
                                    paidamount = totalsales;
                                    totaldiscount = dis.setScale(2, RoundingMode.HALF_UP).doubleValue();
                                    System.out.println(" sales is " + totalsales + " discount " + totaldiscount);
                                    setAssignments();
                                } else {
                                    Toast.makeText(getContext(), getString(R.string.stramountwarn), Toast
                                            .LENGTH_LONG).show();
                                    etdiscount.setText("");
                                    etdiscount.requestFocus();
                                }
                            }
                            return false;
                        }
                        return false;
                    }
                });

                final String[] methods = getResources().getStringArray(R.array.strpaymethods_array);
                final String[] methods2 = getResources().getStringArray(R.array.strpaymethods_array_nopost);

                final ArrayAdapter adapter ;

                if(Settings.getPayLaterEnabled(getContext()))
                    adapter = new ArrayAdapter<>(getContext(), android.R.layout
                            .simple_dropdown_item_1line, methods);
                else
                    adapter = new ArrayAdapter<>(getContext(), android.R.layout
                            .simple_dropdown_item_1line, methods2);

                sppaymethods.setAdapter(adapter);
                sppaymethods.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                    @Override
                    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                        if (position == adapter.getCount() - 1 && adapter.getCount() == methods.length) {
                            etpartial.setFocusable(true);
                            etpartial.setFocusableInTouchMode(true);
                            etpartial.setText("0");
                            paymode = (String) adapter.getItem(position);
                            paystatus = paymode;
                            payinfull = true;
                        } else {
                            paymode = (String) adapter.getItem(position);
                            paystatus = "Paid";
                            paidamount = new BigDecimal(mfinalcost).setScale(2, RoundingMode.HALF_UP).doubleValue();
                            setAssignments();
                        }
                    }

                    @Override
                    public void onNothingSelected(AdapterView<?> parent) {

                    }
                });

                etpartial.setOnKeyListener(new View.OnKeyListener() {
                    @Override
                    public boolean onKey(View v, int keyCode, KeyEvent event) {
                        if (event.getAction() == KeyEvent.ACTION_DOWN && event.getKeyCode() == KeyEvent.KEYCODE_ENTER) {
                            String str = etpartial.getText().toString().trim();
                            if (payinfull) {
                                if (!str.isEmpty()) {
                                    if (!Constants.checkDouble(getContext(), str, etpartial))
                                        return true;
                                    Double partialpay = new BigDecimal(str).setScale(2, RoundingMode.HALF_UP).doubleValue();
                                    Double fullpay = new BigDecimal(mfinalcost).setScale(2, RoundingMode.HALF_UP).doubleValue();
                                    if (partialpay <= fullpay) {
                                        paid = String.valueOf(partialpay);
                                        paidamount = partialpay;
                                        setAssignments();
                                    } else {
                                        Toast.makeText(getContext(),
                                                getString(R.string.strwrongamount) + " " +
                                                        mfinalcost, Toast.LENGTH_LONG).show();
                                    }
                                } else {
                                    etpartial.requestFocus();
                                    Toast.makeText(getContext(), getString(R.string.strfilltheamount), Toast.LENGTH_LONG).show();
                                }
                            } else {
                                paid = mfinalcost;
                                paidamount = new BigDecimal(mfinalcost).setScale(2, RoundingMode.HALF_UP).doubleValue();
                                setAssignments();
                            }
                        }
                        return false;
                    }
                });

                comm.setOnKeyListener(new View.OnKeyListener() {
                    @Override
                    public boolean onKey(View v, int keyCode, KeyEvent event) {
                        String com = comm.getText().toString();
                        if(Validating.areSet(com))
                            note = com; // done
                        return false;
                    }
                });

            } else if(ACTIVE ==2 ){
                mRecyclerView = (RecyclerView)getActivity().findViewById(R.id.prodct_list);
                mRecyclerView.setHasFixedSize(false);

                mLayoutManager = new LinearLayoutManager(getActivity());
                mRecyclerView.setLayoutManager(mLayoutManager);
                rl = (RelativeLayout)getActivity().findViewById(R.id.rltotals);

                if(lst.size() > 0) {
                    mAdapter = new SalesSummaryProductsAdapter(getActivity(), lst, rl, Constants
                            .SALETYPE_SALE);
                    mRecyclerView.setAdapter(mAdapter);
                }

            }
        }

        @Override
        public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
            super.onCreateOptionsMenu(menu, inflater);
            inflater.inflate(R.menu.sales_summary, menu);

            sell = menu.findItem(R.id.action_saveonly);
            print = menu.findItem(R.id.action_saveprint);
            sell.setVisible(true);
            print.setVisible(true);

            sell.setOnMenuItemClickListener(new MenuItem.OnMenuItemClickListener() {
                @Override
                public boolean onMenuItemClick(MenuItem item) {
                    printed = "0";
                    process_sale(false);
                    return false;
                }
            });

            print.setOnMenuItemClickListener(new MenuItem.OnMenuItemClickListener() {
                @Override
                public boolean onMenuItemClick(MenuItem item) {
                    printed = "1";
                    process_sale(true);
                    return false;
                }
            });

        }


        private void process_sale(boolean bool){
            if(paidamount == totalsales)
                paystatus = Constants.PAY_STATUS_CMPLT;
            else
                paystatus = Constants.PAY_STATUS_PNDG;
            paid = String.valueOf(paidamount);
            String taxset_ = String.valueOf(taxset);

            String[] vals = {cust,paid,String.valueOf(totalsales),String.valueOf(totaltax),String.valueOf
                    (totaldiscount),String.valueOf(totalitems),batch,note,taxset_,printed,paystatus,custloc,
                    lat,lng,Constants.SALE_STATUS_CMPLT,Constants.SALETYPE_SALE};
            String[] checkvals = {cust,paid,String.valueOf(totalsales),String.valueOf(totaltax),String.valueOf
                    (totaldiscount),String.valueOf(totalitems),batch,taxset_,printed,paystatus,custloc};
            int orderid = 0;
            if(Validating.areSet(checkvals)){
                if(Sales.insert(getContext(), vals, Constants.LOCAL)){
                    orderid = Sales.getId(getContext());
                    System.out.println("orderid "+orderid);
                }

                if(orderid > 0){
                    final int[] insert = new int[]{0};
                    if(orderid != 0){
                        for(int i = 0; i < lst.size(); i++){
                            Pending_Items list = lst.get(i);
                            String prodno = list.getProd();
                            String locprod = String.valueOf(list.getProdloc());
                            String qty = String.valueOf(list.getQty());
                            String netprc = String.valueOf(list.getNetprice());
                            String saleprc = String.valueOf(list.getPrice());
                            String taxam = String.valueOf(list.getTaxmt());
                            String subto = String.valueOf(list.getSbtotal());
                            String wh = list.getWh();
                            String disc = String.valueOf(list.getDiscount());
                            String taxr = String.valueOf(list.getTaxrt());
                            String order = String.valueOf(orderid);
                            String saleref = batch;

                            String[] items = {prodno,locprod,qty,netprc,saleprc,taxam,subto,wh,disc,taxr,order,
                                    saleref,Constants.SALE_STATUS_CMPLT,Constants.SALETYPE_SALE};
                            if(Validating.areSet(items)){
                                if(SalesItems.insertItems(getContext(), items, Constants.LOCAL) ==1){
                                    String isservice = Products.getProductType(getContext(), locprod);
                                    if(isservice.equals(Constants.PRODUCT_STANDARD)){
                                        int prodid = Integer.parseInt(locprod);
                                        Products.updateProductQty(getContext(), new int[]{prodid,list.getQty(), 1});
                                    }
                                    insert[0] += i ;
                                    Toast.makeText(getContext(), getString(R.string.strorderadded), Toast.LENGTH_SHORT).show();
                                }
                            }
                        }
                    }

                }
                if(bool) {
                    String app = Constants.PREINTERAPP;
                    if (Constants.isPackageInstalled(app, getContext())) {
                        int send = Constants.writeReceiptPending(getContext(), lst, taxset, custloc);
                        startAct();
                        startActivity(getActivity().getPackageManager().getLaunchIntentForPackage(app));
                    } else {
                        Toast.makeText(getContext(), getString(R.string.strnoprinterapp), Toast.LENGTH_LONG).show();
                        startAct();
                    }
                } else
                    startAct();
            }
        }

        @Override
        public void onStart() {
            super.onStart();
            turnGpsOn() ;
        }

        private void startAct(){
            Pending_Items.purgeItems(getContext(), Constants.SALETYPE_SALE);
            appPreference.clearCustomer();
            Intent intent = new Intent(getActivity(), Dashboard.class);
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            startActivity(intent);
            getActivity().finish();
        }

        public void turnGpsOn(){
            int accounttype = appPreference.get_accounttype();
            if(accounttype == 1)
                if(Build.VERSION.SDK_INT > Build.VERSION_CODES.KITKAT) {
                    if(!gpsTracker.isgpson()) {
                        System.out.println(" isgpson?? " + gpsTracker.isgpson());
                        Toast.makeText(getContext(),getString(R.string.strgps),Toast.LENGTH_LONG).show();
                        Intent inte = new Intent(android.provider.Settings.ACTION_LOCATION_SOURCE_SETTINGS);
                        startActivity(inte);
                    }
                } else {
                    Intent intent = new Intent("android.location.GPS_ENABLED_CHANGE");
                    intent.putExtra("enabled", true);
                    getActivity().sendBroadcast(intent);
                }
        }


        @Override
        public void onResume() {
            super.onResume();
            turnGpsOn();
        }

        @Override
        public void onDestroy() {
            super.onDestroy();
            if(gpsTracker.isgpson())
                gpsTracker.stopUsingGPS();
            turnGpsOf();
        }

        @Override
        public void onPause() {
            super.onPause();
            if(gpsTracker.isgpson())
                gpsTracker.stopUsingGPS();
            turnGpsOf();
        }
        public void turnGpsOf(){
            if(Build.VERSION.SDK_INT < Build.VERSION_CODES.KITKAT) {
                Intent intent = new Intent("android.location.GPS_ENABLED_CHANGE");
                intent.putExtra("enabled", false);
                getActivity().sendBroadcast(intent);
            }
        }

        @Override
        public void onStop() {
            super.onStop();
            if(gpsTracker.isgpson())
                gpsTracker.stopUsingGPS();
            turnGpsOf();
        }
        private void setAssignments(){

            totaltax = txttotal ; // done
            subtotal = new BigDecimal(totalsales).subtract(new BigDecimal(txttotal)).doubleValue();

            System.out.println("total cost "+totalsales);
            msubtotal = new BigDecimal(subtotal).setScale(2,RoundingMode.HALF_UP).toString();

            mfinalcost = new BigDecimal(totalsales).setScale(2,RoundingMode.HALF_UP).toString();
            mtaxtot = new BigDecimal(txttotal).setScale(2, RoundingMode.HALF_UP).toString();

                txtqty.setText(String.valueOf(qty)+"("+items+")");
                txttaxable.setText(msubtotal);
                txttot.setText(msubtotal);
                txtgrandtot.setText(mfinalcost);
                txttaxamo.setText(mtaxtot);
                etpartial.setText(mfinalcost);

                if(!Settings.getDiscountEnabled(getContext())){
                    etdiscount.setFocusable(false);
                    etdiscount.setFocusableInTouchMode(false);
                    txtdiscount.setText("0");
                    etdiscount.setHint(getString(R.string.strdiscountenabledwarn));
                } else
                    txtdiscount.setText(String.valueOf(totaldiscount));
        }
    }
}
