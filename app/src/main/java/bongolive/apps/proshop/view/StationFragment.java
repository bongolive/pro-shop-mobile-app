/*
 * The source codes in this project are owned by Bongolive Enterprises. Copying or any kind of usage without Bongolive's approval is against the law.
 * Copyright (c) 2015.
 *
 */

package bongolive.apps.proshop.view;

import android.app.Dialog;
import android.content.Context;
import android.content.ContextWrapper;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Path;
import android.graphics.RectF;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.support.v4.app.Fragment;
import android.util.AttributeSet;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import java.io.File;
import java.io.FileOutputStream;
import java.util.ArrayList;

import bongolive.apps.proshop.R;
import bongolive.apps.proshop.controller.Validating;
import bongolive.apps.proshop.controller.AppPreference;
import bongolive.apps.proshop.controller.Constants;
import bongolive.apps.proshop.model.MultimediaContents;
import bongolive.apps.proshop.model.Station;
import bongolive.apps.proshop.model.StationPump;
import bongolive.apps.proshop.model.User;
import bongolive.apps.proshop.model.Warehouse;


public class StationFragment extends Fragment implements View.OnClickListener
{
    private static final String ARG_SECTION_NUMBER = "section_number";
    AlertDialogManager alert ;
    AppPreference appPreference;
    View _rootView;
    EditText etmin,etmout,etcain,etcaout,ettiin,ettiout,etnote,etstname,etopname,etpump ;
    TextView tetmin,tetmout,tetcain,tetcaout,tettiin,tettiout,tetnote,tetstname,tetopname,txtsubdate,
            txtsync,txtstpname ;
    TextView txtsign;
    ImageView imgsign;
    Spinner sppump;
    ArrayAdapter<String> padapter;
    private static String signaturepath = null;
    private String[] info = null;
    boolean tabletsize;
    ArrayList pumps;
    /*
    *  signature */
    LinearLayout mContent;
    signature mSignature;
    Button mClear, mGetSign, mCancel;

    public static String tempDir;
    public int count = 1;
    public String current = null;
    private Bitmap mBitmap;
    View mView;
    File mypath;
    private String uniqueId, pumpname;
    String FRAGMENTSOURCE = null;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
    {
        if(getArguments().getString(Constants.REPORTS) != null ) {
            FRAGMENTSOURCE = getArguments().getString(Constants.REPORTS);
            System.out.println(" source is "+FRAGMENTSOURCE);
            if (_rootView == null) {
                tabletsize = getResources().getBoolean(R.bool.isTablet);
                _rootView = inflater.inflate(R.layout.station_form2, container, false);
            }
        } else {
            if (_rootView == null) {
                tabletsize = getResources().getBoolean(R.bool.isTablet);
                _rootView = inflater.inflate(R.layout.station_form, container, false);
            }
        }
        return _rootView ;
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState)
    {
        super.onActivityCreated(savedInstanceState);
        alert = new AlertDialogManager();

        appPreference = new AppPreference(getActivity());

        if(FRAGMENTSOURCE == null) {
            etmin = (EditText) getActivity().findViewById(R.id.etmeterin);
            etmout = (EditText) getActivity().findViewById(R.id.etmeterout);
            etcain = (EditText) getActivity().findViewById(R.id.etcashin);
            etcaout = (EditText) getActivity().findViewById(R.id.etcashout);
            ettiin = (EditText) getActivity().findViewById(R.id.ettimein);
            ettiout = (EditText) getActivity().findViewById(R.id.ettimeout);
            etnote = (EditText) getActivity().findViewById(R.id.etnotes);
            etstname = (EditText) getActivity().findViewById(R.id.etstname);
            etstname.setText(Warehouse.getDefault(getContext()));
            etopname = (EditText) getActivity().findViewById(R.id.etopname);
            txtsign = (TextView) getActivity().findViewById(R.id.txtaddsign);
            imgsign = (ImageView) getActivity().findViewById(R.id.imgsign);
            sppump = (Spinner) getActivity().findViewById(R.id.sppump);
            etpump = (EditText) getActivity().findViewById(R.id.etpump);
            setHasOptionsMenu(true);
            ettiin.setOnClickListener(this);
            ettiout.setOnClickListener(this);
            txtsign.setOnClickListener(this);
            pumps = StationPump.getItems(getContext());
            padapter = new ArrayAdapter<String>(getContext(),android.R.layout.simple_list_item_1,pumps);
            padapter.setDropDownViewResource(android.R.layout.simple_dropdown_item_1line);
            sppump.setAdapter(padapter);
            sppump.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                @Override
                public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                    if (position != 0 || position < pumps.size() - 1)
                        pumpname = pumps.get(position).toString();

                    if (position == pumps.size() - 1) {
                        etpump.setVisibility(View.VISIBLE);
                        sppump.setVisibility(View.GONE);
                        etpump.requestFocus();
                    }
                    System.out.println("cat is " + pumpname);
                }

                @Override
                public void onNothingSelected(AdapterView<?> parent) {

                }
            });
        } else {
            tetmin = (TextView) getActivity().findViewById(R.id.txtmeterin);
            tetmout = (TextView) getActivity().findViewById(R.id.txtmeterout);
            tetopname = (TextView) getActivity().findViewById(R.id.txtopname);
            tetcain = (TextView) getActivity().findViewById(R.id.txtcashin);
            tetcaout = (TextView) getActivity().findViewById(R.id.txtcashout);
            tettiin = (TextView) getActivity().findViewById(R.id.txttimein);
            tettiout = (TextView) getActivity().findViewById(R.id.txttimeout);
            tetnote = (TextView) getActivity().findViewById(R.id.txttnotes);
            tetstname = (TextView) getActivity().findViewById(R.id.txtstname);
            tetopname = (TextView) getActivity().findViewById(R.id.txtopname);
            txtstpname = (TextView) getActivity().findViewById(R.id.txtstpname);
            txtsubdate = (TextView) getActivity().findViewById(R.id.txtsubdate);
            txtsync = (TextView) getActivity().findViewById(R.id.txtsynced);
            imgsign = (ImageView) getActivity().findViewById(R.id.imgsign);

            info = getArguments().getStringArray("info");
            System.out.println(info.toString());
            for(String inf: info){
                System.out.println(inf);
            }
            if(info != null){
                tetmin.setText(info[0]);tetmout.setText(info[1]);tetopname.setText(info[2]);tetcain.setText(info[3]);
                tetcaout.setText(info[4]);
                tettiin.setText(info[5]);
                tettiout.setText(info[6]);
                tetnote.setText(info[7]);
                tetstname.setText(info[8]);txtsubdate.setText(info[9]);txtsync.setText(info[10]);
                String img = info[11];
                txtstpname.setText(info[12]);
                if(Validating.areSet(img)) {
                    Bitmap bitmap = Constants.decodeImg(img, 300, 300);
                    if(bitmap != null)
                        imgsign.setImageBitmap(bitmap);
                    else
                        imgsign.setVisibility(View.GONE);
                }
            }
            setHasOptionsMenu(false);
        }

    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {

        inflater.inflate(R.menu.stationmenu, menu);

        super.onCreateOptionsMenu(menu, inflater);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_saveonly:
                addform();
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    public void addform(){
        String min,mout,cin,cout,tin,tout,note,sign,snm,creator,timestamp,op;
        op = etopname.getText().toString();
        min = etmin.getText().toString();
        mout = etmout.getText().toString();
        cin = etcain.getText().toString();
        cout = etcaout.getText().toString();
        tin = ettiin.getText().toString();
        tout = ettiout.getText().toString();
        snm = etstname.getText().toString();
        timestamp = String.valueOf(System.currentTimeMillis());
        note = etnote.getText().toString();
        sign = signaturepath;
        if(etpump.getVisibility() == View.VISIBLE)
            pumpname = etpump.getText().toString().trim();
        if(pumpname == null)
        {
            Toast.makeText(getContext(),getString(R.string.strselectpump),Toast.LENGTH_LONG).show();
            return;
        }
        creator = String.valueOf(appPreference.getUserId());
        String username = User.getUser(getContext(), creator);
        if(!Validating.areSet(username)) {
            alert.showAlertDialog(getContext(),getString(R.string.strerror),getString(R.string.strsesionwarn),false);
            etmin.setText("");etmout.setText("");etcaout.setText("");etcain.setText("");ettiout.setText("");
            etnote.setText("");
            ettiin.setText("");etstname.setText("");imgsign.setVisibility(View.GONE);txtsign.setVisibility(View.VISIBLE);
            return;
        }

        String[] vals = {min,mout,tout,tin,cin,cout,snm,creator,sign,note,timestamp,op};
        String[] check = {min,mout,tout,tin,cin,cout,snm,sign,timestamp,op};
        if(Validating.areSet(check)){
            boolean ins = Station.insert(getContext(),vals,Constants.LOCAL);
            if(ins){
                int id = Station.getId(getContext());
                if(id > 0){
                    StationPump.insert(getContext(),new String[]{"0",pumpname,String.valueOf(id)},Constants.LOCAL);
                }
                String[] mult = {signaturepath, username + "_" + System.currentTimeMillis(),
                        MultimediaContents.OPERATIONTYPES, timestamp};
                System.out.println("username "+username);
                if(!MultimediaContents.isMediaStored(getContext(),timestamp)) {
                    boolean stored = MultimediaContents.storeMultimedia(getContext(), mult);
                    if (stored) {
                        System.out.println("stored ? " + stored);
                    }
                }
                etmin.setText("");etmout.setText("");etcaout.setText("");etcain.setText("");ettiout.setText("");
                etnote.setText("");
                ettiin.setText("");etstname.setText("");imgsign.setVisibility(View.GONE);txtsign.setVisibility(View.VISIBLE);
                Toast.makeText(getContext(),getString(R.string.strformadded),Toast.LENGTH_LONG).show();
            }
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.ettimein:
                Constants.setDateTimeSeconds(getContext(), ettiin);
                break;
            case R.id.ettimeout:
                Constants.setDateTimeSeconds(getContext(), ettiout);
                break;
            case R.id.txtaddsign:
                process_signature(imgsign);
                break;
        }
    }

    private void process_signature(final ImageView imgsign) {
        LayoutInflater infl = (LayoutInflater)getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View view = infl.inflate(R.layout.signature, null);

        final Dialog dg = new Dialog(getContext(), R.style.CustomDialog);
        dg.setCancelable(true);
        dg.setContentView(view);

        tempDir = Environment.getExternalStorageDirectory()+"/Proshop_Reports/.temp";
        ContextWrapper cw = new ContextWrapper(dg.getContext().getApplicationContext());
        File directory = cw.getDir("tayananjoka", Context.MODE_PRIVATE);

        prepareDirectory();
        uniqueId = String.valueOf(System.currentTimeMillis());
        current = uniqueId + ".png";
        mypath = new File(directory, current);

        LinearLayout lconsent = (LinearLayout)dg.findViewById(R.id.layoutconsent);
        lconsent.setVisibility(View.GONE);
        mContent = (LinearLayout)dg. findViewById(R.id.linearLayout);
        mSignature = new signature(dg.getContext(), null);
        mSignature.setBackgroundColor(Color.WHITE);
        mContent.addView(mSignature, ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT);
        mClear = (Button) dg.findViewById(R.id.clear);
        mGetSign = (Button) dg.findViewById(R.id.getsign);
        mGetSign.setEnabled(false);
        mCancel = (Button) dg.findViewById(R.id.cancel);
        mView = mContent;

        mClear.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                Log.v("log_tag", "Panel Cleared");
                mSignature.clear();
                mGetSign.setEnabled(false);
            }
        });

        mGetSign.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                mView.setDrawingCacheEnabled(true);
                String bitmap = mSignature.save(mView);

                if (bitmap != null) {
                    Log.v("log_tag", "uri " + bitmap);
                    signaturepath = bitmap;
                    txtsign.setVisibility(View.GONE);
                    imgsign.setVisibility(View.VISIBLE);
                    Bitmap mbitmap = Constants.decodeImg(bitmap, 400, 300);
                    imgsign.setImageBitmap(mbitmap);
                } else {
                    imgsign.setVisibility(View.GONE);
                    txtsign.setVisibility(View.VISIBLE);
                }
                if(dg.isShowing())
                    dg.dismiss();
            }
        });

        mCancel.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                Log.v("log_tag", "Panel Canceled");
                dg.dismiss();
            }
        });

        dg.show();
    }

    private boolean prepareDirectory()
    {
        try
        {
            if (makedirs())
            {
                return true;
            } else {
                return false;
            }
        } catch (Exception e)
        {
            e.printStackTrace();
            return false;
        }
    }

    private boolean makedirs()
    {
        File tempdir = new File(tempDir);
        if (!tempdir.exists())
            tempdir.mkdirs();

        if (tempdir.isDirectory())
        {
            File[] files = tempdir.listFiles();
            for (File file : files)
            {
                if (!file.delete())
                {
                    System.out.println("Failed to delete " + file);
                }
            }
        }
        return (tempdir.isDirectory());
    }
    public class signature extends View
    {
        private static final float STROKE_WIDTH = 5f;
        private static final float HALF_STROKE_WIDTH = STROKE_WIDTH / 2;
        private Paint paint = new Paint();
        private Path path = new Path();

        private float lastTouchX;
        private float lastTouchY;
        private final RectF dirtyRect = new RectF();

        public signature(Context context, AttributeSet attrs)
        {
            super(context, attrs);
            paint.setAntiAlias(true);
            paint.setColor(Color.BLACK);
            paint.setStyle(Paint.Style.STROKE);
            paint.setStrokeJoin(Paint.Join.ROUND);
            paint.setStrokeWidth(STROKE_WIDTH);
        }

        public String save(View v)
        {
            Uri uri = null;
            Log.v("log_tag", "Width: " + v.getWidth());
            Log.v("log_tag", "Height: " + v.getHeight());
            String bitmap = null;
            if(mBitmap == null)
            {
                mBitmap =  Bitmap.createBitmap(mContent.getWidth(), mContent.getHeight(), Bitmap.Config.RGB_565);
            }
            Canvas canvas = new Canvas(mBitmap);
            try
            {
                FileOutputStream mFileOutStream = new FileOutputStream(mypath);

                v.draw(canvas);
                mBitmap.compress(Bitmap.CompressFormat.PNG, 90, mFileOutStream);
                bitmap = saveImage(mBitmap);
                mGetSign.setEnabled(true);
            }
            catch(Exception e)
            {
                Log.v("log_tag", e.toString());
            }
            return bitmap;
        }

        public void clear()
        {
            path.reset();
            invalidate();
        }

        private String saveImage(Bitmap bitmap) {

            String stored = null;

            File sdcard = Environment.getExternalStorageDirectory() ;

            File folder = new File(sdcard.getAbsoluteFile(), "Proshop_Reports");
            folder.mkdir();
            String filename = String.valueOf(System.currentTimeMillis());
            File file = new File(folder.getAbsoluteFile(), filename + ".jpg") ;
            if (file.exists())
                return stored ;

            try {
                FileOutputStream out = new FileOutputStream(file);
                bitmap.compress(Bitmap.CompressFormat.JPEG, 90, out);
                out.flush();
                out.close();
                stored = file.getAbsolutePath();
            } catch (Exception e) {
                e.printStackTrace();
            }
            return stored;
        }

        @Override
        protected void onDraw(Canvas canvas)
        {
            canvas.drawPath(path, paint);
        }

        @Override
        public boolean onTouchEvent(MotionEvent event)
        {
            float eventX = event.getX();
            float eventY = event.getY();
            mGetSign.setEnabled(true);

            switch (event.getAction())
            {
                case MotionEvent.ACTION_DOWN:
                    path.moveTo(eventX, eventY);
                    lastTouchX = eventX;
                    lastTouchY = eventY;
                    return true;

                case MotionEvent.ACTION_MOVE:

                case MotionEvent.ACTION_UP:

                    resetDirtyRect(eventX, eventY);
                    int historySize = event.getHistorySize();
                    for (int i = 0; i < historySize; i++)
                    {
                        float historicalX = event.getHistoricalX(i);
                        float historicalY = event.getHistoricalY(i);
                        expandDirtyRect(historicalX, historicalY);
                        path.lineTo(historicalX, historicalY);
                    }
                    path.lineTo(eventX, eventY);
                    break;

                default:
                    debug("Ignored touch event: " + event.toString());
                    return false;
            }

            invalidate((int) (dirtyRect.left - HALF_STROKE_WIDTH),
                    (int) (dirtyRect.top - HALF_STROKE_WIDTH),
                    (int) (dirtyRect.right + HALF_STROKE_WIDTH),
                    (int) (dirtyRect.bottom + HALF_STROKE_WIDTH));

            lastTouchX = eventX;
            lastTouchY = eventY;

            return true;
        }

        private void debug(String string){
        }

        private void expandDirtyRect(float historicalX, float historicalY)
        {
            if (historicalX < dirtyRect.left)
            {
                dirtyRect.left = historicalX;
            }
            else if (historicalX > dirtyRect.right)
            {
                dirtyRect.right = historicalX;
            }

            if (historicalY < dirtyRect.top)
            {
                dirtyRect.top = historicalY;
            }
            else if (historicalY > dirtyRect.bottom)
            {
                dirtyRect.bottom = historicalY;
            }
        }

        private void resetDirtyRect(float eventX, float eventY)
        {
            dirtyRect.left = Math.min(lastTouchX, eventX);
            dirtyRect.right = Math.max(lastTouchX, eventX);
            dirtyRect.top = Math.min(lastTouchY, eventY);
            dirtyRect.bottom = Math.max(lastTouchY, eventY);
        }
    }
}
