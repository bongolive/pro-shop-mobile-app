/*
 * The source codes in this project are owned by Bongolive Enterprises. Copying or any kind of usage without Bongolive's approval is against the law.
 * Copyright (c) 2015.
 *
 */

package bongolive.apps.proshop.view;

import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.ActionBar;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AutoCompleteTextView;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;

import java.util.ArrayList;

import bongolive.apps.proshop.R;
import bongolive.apps.proshop.controller.Constants;
import bongolive.apps.proshop.controller.TransferItemsAdapter;
import bongolive.apps.proshop.model.StockIssueItems;
import bongolive.apps.proshop.model.Transfer;
import bongolive.apps.proshop.model.TransferItems;


public class TransferDetailFrag extends Fragment
{
    View _rootView;
    private boolean tabletSize;
    private RecyclerView mRecyclerView;
    private RecyclerView.Adapter mAdapter;
    private RecyclerView.LayoutManager mLayoutManager;
    private ActionBar actionBar;
    private EditText etfilter;
    private ArrayList arrayList;
    private RelativeLayout rl;
    private ImageView imgdone,imgnext;
    private AutoCompleteTextView bzn;
    private Toolbar ab;
    ArrayList<TransferItems> mItemsDetail = new ArrayList<>();
    ArrayList<StockIssueItems> mItemsDetail_action = new ArrayList<>();
    FloatingActionButton actionButton;
    long pyid = 0;
    int ack = 0;
    String title;
    String action ;
    double grandbalance,grandpaid;
    /*
    * end of new strings
    */
    MenuItem menuItem;


    /*
    * end of new strings
    */

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
    {
        _rootView = inflater.inflate(R.layout.selling_screen, container, false);
        tabletSize = getResources().getBoolean(R.bool.isTablet);
        return _rootView ;
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState)
    {
        super.onActivityCreated(savedInstanceState);

        actionButton = (FloatingActionButton)getActivity().findViewById(R.id.actionbutton);
        actionButton.setBackgroundColor(getResources().getColor(R.color.actionbar));
        actionButton.setImageResource(R.drawable.ic_action_send);
        actionButton.setVisibility(View.GONE);

        mRecyclerView = (RecyclerView) getActivity().findViewById(R.id.prodct_list);
        mRecyclerView.setHasFixedSize(true);
        mRecyclerView.setItemAnimator(new DefaultItemAnimator());

        mLayoutManager = new LinearLayoutManager(getContext());
        mRecyclerView.setLayoutManager(mLayoutManager);

        ab = ((MainScreen)this.getActivity()).getmToolbar();

        pyid = getArguments().getLong(Transfer.ID);
//        ack = Transfer.getRecStatus(getContext(), pyid);
        action = getArguments().getString(Constants.SOURCE);

        switch (action){
            case Constants.TRANSTYPE_REQUEST:
                mItemsDetail = TransferItems.getItems(getContext(), pyid);
                ack = Transfer.getRecStatus(getContext(), pyid);
                mAdapter = new TransferItemsAdapter(getActivity(), mItemsDetail);
                mRecyclerView.setAdapter(mAdapter);
                break;
            case Constants.TRANSTYPE_ISSUE:
                title = getArguments().getString(Transfer.WAREHOUSEFROM);
                ab.setTitle(title);
                mItemsDetail_action = StockIssueItems.getItems(getContext(), pyid);
                mAdapter = new TransferItemsAdapter(getActivity(), mItemsDetail_action, action);
                mRecyclerView.setAdapter(mAdapter);
                break;
            case Constants.TRANSTYPE_TRANSFER:
                mItemsDetail = TransferItems.getItems(getContext(), pyid);
                mAdapter = new TransferItemsAdapter(getActivity(), mItemsDetail);
                mRecyclerView.setAdapter(mAdapter);
                ack = Transfer.getRecStatus(getContext(), pyid);
                System.out.println(" approval "+ack);
                if(ack == 1)
                    actionButton.setVisibility(View.VISIBLE);
                break;
        }

        mRecyclerView.addOnItemTouchListener(
                new RecylerItemClick(getContext(), new RecylerItemClick.OnItemClickListener() {
                    @Override
                    public void onItemClick(View view, int position) {
//                        TransferItems tr = mItemsDetail.get(position);
//                        transfer_details(TransferItems.getItem(getContext(), tr.getId()));
                    }
                })
        );

        actionButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showStation();
            }
        });
        setHasOptionsMenu(true);

    }

    public void showStation() {

            DeliverItemsFrag newFragment = new DeliverItemsFrag();
            Bundle args = new Bundle();
            args.putLong(Transfer.ID, pyid);
            newFragment.setArguments(args);

            FragmentTransaction transaction = getFragmentManager().beginTransaction();
            transaction.replace(R.id.content_frame, newFragment);
            transaction.addToBackStack(String.valueOf(pyid));
            transaction.commit();
    }

    @Override
    public void onResume() {
        super.onResume();
        if(pyid != 0 && mRecyclerView != null && mAdapter != null) {
            mAdapter.notifyDataSetChanged();
            mAdapter.notifyDataSetChanged();
            if(action.equals(Constants.TRANSTYPE_TRANSFER)) {
                ack = Transfer.getRecStatus(getContext(), pyid);
                if (ack == 1)
                    actionButton.setVisibility(View.VISIBLE);
                else
                    actionButton.setVisibility(View.GONE);
            }
        }
    }
}
