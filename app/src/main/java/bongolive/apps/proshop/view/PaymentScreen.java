/*
 * The source codes in this project are owned by Bongolive Enterprises. Copying or any kind of usage without Bongolive's approval is against the law.
 * Copyright (c) 2015.
 *
 */


package bongolive.apps.proshop.view;


import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.MenuItem;
import android.view.View;
import android.widget.AutoCompleteTextView;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;

import java.util.ArrayList;

import bongolive.apps.proshop.R;
import bongolive.apps.proshop.controller.PaymentSplitAdapter;
import bongolive.apps.proshop.model.Customers;
import bongolive.apps.proshop.model.Payments;
import bongolive.apps.proshop.model.Sales;

public class PaymentScreen extends AppCompatActivity{

    private boolean tabletSize = false;
    private RecyclerView mRecyclerView;
    private RecyclerView.Adapter mAdapter;
    private RecyclerView.LayoutManager mLayoutManager;
    private ActionBar actionBar;
    private EditText etfilter;
    private ArrayList arrayList;
    private RelativeLayout rl;
    private ImageView imgdone,imgnext;
    private AutoCompleteTextView bzn;
    private ActionBar ab;
    ArrayList<Payments> mItems;
    /*
    * end of new strings
    */

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        tabletSize = getResources().getBoolean(R.bool.isTablet);
        if (!tabletSize) {
            setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        }
        setContentView(R.layout.selling_screen);

        ab = getSupportActionBar();
        ab.setDisplayUseLogoEnabled(true);
        ab.setDisplayHomeAsUpEnabled(true);

        mRecyclerView = (RecyclerView) findViewById(R.id.prodct_list);
        mRecyclerView.setHasFixedSize(true);
        mRecyclerView.setItemAnimator(new DefaultItemAnimator());

        mLayoutManager = new LinearLayoutManager(this);
        mRecyclerView.setLayoutManager(mLayoutManager);
        mItems = Payments.getPaymentItems(this);
        if(mItems.size() < 1){
            Snackbar snackbar = Snackbar
                    .make(mRecyclerView, getString(R.string.strnodata), Snackbar.LENGTH_INDEFINITE);

            snackbar.show();
        }
        mAdapter = new PaymentSplitAdapter(this, mItems);
        mRecyclerView.setAdapter(mAdapter);

        mRecyclerView.addOnItemTouchListener(
                new RecylerItemClick(this, new RecylerItemClick.OnItemClickListener() {
                    @Override
                    public void onItemClick(View view, int position) {
                        Payments py = mItems.get(position);
                        String localsale = py.getLocalsale();
                        boolean haschild = Payments.anyChild(PaymentScreen.this, localsale);
                        if((py.getNodetype() == 0 && !haschild) || py.getNodetype() == 1) {
                            long id = py.getId();
                            String custmerid = Sales.getCustomerLocal(PaymentScreen.this, localsale);
                            String customer = Customers.getCustomerBusinessNameLoca(PaymentScreen.this, custmerid);
                            Intent intent = new Intent(PaymentScreen.this, PaymentDetail.class);
                            intent.putExtra(Payments.ID, id);
                            intent.putExtra(Sales.CUSTOMERID, customer);
                            startActivity(intent);
                        }
                    }
                })
        );

    }

    @Override
    protected void onResume() {
        super.onResume();
        if(mAdapter != null)
            mAdapter.notifyDataSetChanged();
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }
}


