/*
 * Copyright (c) 2015. All rights reserved. The source codes are property of bongolive
 */

package bongolive.apps.proshop.view;


import android.content.Context;
import android.os.AsyncTask;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.TextView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import bongolive.apps.proshop.R;
import bongolive.apps.proshop.controller.AppPreference;
import bongolive.apps.proshop.controller.Validating;
import bongolive.apps.proshop.model.PurchaseItems;
import bongolive.apps.proshop.model.SalesItems;
import bongolive.apps.proshop.model.Settings;
import bongolive.apps.proshop.model.Warehouse;
import bongolive.apps.proshop.controller.Constants;
import bongolive.apps.proshop.controller.MCrypt;

/**
 * Created by nasznjoka on 5/18/15.
 */
public class DeviceRegistration extends AsyncTask<String, Integer, Integer> {
    private static final String TAG = DeviceRegistration.class.getName();
    Context context;
    ProgressBar bar;
    TextView txt;
    AppPreference appPreference;
    Button btn;
    boolean werehousesuccess  = false;
    boolean settingssuccess = false;

    public DeviceRegistration(Context context, ProgressBar pbar, TextView textView, Button btnstart) {
        this.context = context;
        this.bar = pbar;
        this.txt = textView;
        appPreference = new AppPreference(context);
        btn = btnstart;
    }

    @Override
    protected void onProgressUpdate(Integer... values) {
        super.onProgressUpdate(values);
        if (this.bar != null) {
            bar.setProgress(values[0]);
            txt.setText("saving settings");
            bar.setMax(values[1]);
        }
    }

    @Override
    protected void onPreExecute() {
        super.onPreExecute();
        bar.setIndeterminate(false);
        txt.setText("processing");
    }

    @Override
    protected Integer doInBackground(String... params) {
        Log.v("action", params[0]);
        int[] ret = {0};
        MCrypt mCrypt = new MCrypt();
        if (params[0].equals(Constants.TAGREGISTRATION)) {
            //register the device now
            try {
                JSONObject jsonObject = Settings.registerDevice(context);
                if (jsonObject != null) {
                    if (jsonObject.has(Constants.SUCCESS)) {

                        String obj = "{\"success\":\"1\",\"wh_list\":[{\"wh_id\":\"1\",\"wh_code\":\"WW1\"," +
                                "\"wh_name\":\"Sample Wharehouse\",\"wh_address\":\"\"}]}";
//                        JSONObject jb = new JSONObject(obj);
                        if (jsonObject.has(Warehouse.GETARRAYNAME)) {
                            JSONArray custarray = jsonObject.getJSONArray(Warehouse.GETARRAYNAME);
                            if(custarray.length() > 0) {
                                for (int i = 0; i < custarray.length(); i++) {
                                    JSONObject js = custarray.getJSONObject(i);
                                    String whid = js.getString(Warehouse.WH_ID);
                                    String cd = js.getString(Warehouse.WH_CODE);
                                    String nm = js.getString(Warehouse.WH_NAME);
                                    String ad = js.getString(Warehouse.WH_ADDRESS);
                                    String status = Constants.WH_ACTIVE;
                                    String[] vals = {whid, cd, nm, ad,status};
                                    String[] chk = {whid, cd, nm};
                                    if (Validating.areSet(chk)) {
                                        if (!Warehouse.isWhPresent(context, whid)) {
                                            if (Warehouse.storeWh(context, vals) == 1)
                                                werehousesuccess = true;
                                            System.out.println("ware house inserted " + nm);
                                        } else {
                                            if (Warehouse.updateWh(context, vals) == 1) {
                                                werehousesuccess = true;
                                                boolean supd = SalesItems.updateWh(context,whid);
                                                boolean pupd = PurchaseItems.updateWh(context, whid);
                                                System.out.println("the sale update "+supd+" purchase update "+pupd);
                                            }
                                            System.out.println("ware house updated ");
                                        }
                                    } else {
                                        //store // email on incomplete data
                                        ret[0] = 0;
                                    }
                                }
                            } else {
                                System.out.println("array length "+custarray.length());
                            }
                        } else {

                        }

                        if (jsonObject.has(Settings.ARRAYNAME)) {

                            JSONObject j = jsonObject.getJSONObject(Settings.ARRAYNAME);
                            String si = j.getString(Settings.SYSID);
                            String tx = j.getString(Settings.VAT);
                            String ds = j.getString(Settings.DISCOUNT);
                            String tt = j.getString(Settings.TRACKTIME);
                            String pt = j.getString(Settings.PRINT);
                            String ex = j.getString(Settings.EXPORTDB);
                            String im = j.getString(Settings.IMPORTDB);
                            String pe = j.getString(Settings.PRODUCT_EDIT);
                            String ce = j.getString(Settings.CUSTOMER_EDIT);
                            String sr = j.getString(Settings.STOCK_RECEIVE);
                            String ps = j.getString(Settings.PRODUCT_SHARING);
                            String pa = j.getString(Settings.PRODUCT_ADD);
                            String ca = j.getString(Settings.CUSTOMER_ADD);
                            String dl = j.getString(Settings.DISCOUNT_LIMIT);
                            String ap = j.getString(Settings.APPTYPE);
                            String pl = j.getString(Settings.PAYLATER);
                            String us = j.getString(Settings.USERS);
                            String del =j.getString(Settings.DELIVERY);
                            String ord =j.getString(Settings.ORDERS);
                            String lt = j.getString(Settings.PROSHOPLITE);
                            String f = j.getString(Settings.STATION_FORM);
                            String e = j.getString(Settings.EXPENSES);
                            String a = j.getString(Settings.AUDIT);
                            String p = j.getString(Settings.PAYMENT);
                            String s = j.getString(Settings.STOCK);
                            String sig = j.getString(Settings.SIGNATURE);
                            String sl = j.getString(Settings.SALES);
                            String rp = j.getString(Settings.REPORTS);
                            String pd = j.getString(Settings.PRODUCTS);
                            String ct = j.getString(Settings.CUSTOMERS);

                            String[] settings = {si, tx, ds, tt, pt, ex, im, pe, ce, sr, ps, pa, ca, dl, ap,pl,us,del,
                                    ord,lt,f,e,a,p,s,sig,sl,rp,pd,ct};

                            if (Validating.areSet(settings)) {
                                if (!Settings.isSettingsExist(context, si)) {
                                    System.out.println("settings is not existing");
                                    if (Settings.insert(context, settings))
                                        settingssuccess = true;
                                } else {
                                    System.out.println("settings exists");
                                    if (Settings.update(context, settings))
                                        settingssuccess = true;
                                }
                            } else {
                                ret[0] = 0;
                            }

                        }

                        if(werehousesuccess)
                            Warehouse.sendAckJson(context);
                        if(settingssuccess)
                            Settings.sendAckJson(context);


                        String token = jsonObject.getString(Constants.AUTHTOKENSERVER);
                        String account = jsonObject.getString(Constants.DEVICEACCOUNTTYPESERVER);
                        String co = jsonObject.getString("business_name");
                        String pe = jsonObject.getString("contact_person");
                        String po = jsonObject.getString("mobile");

                        String[] vals = {token, account,co,pe,po};
                        if (Validating.areSet(vals)) {
                            if (vals[1].equals(Constants.ACCOUNT_TYPE_NORMAL)) {
                                appPreference.save_accounttype(0);
                            } else if (vals[1].equals(Constants.ACCOUNT_TYPE_PREMIUM)) {
                                appPreference.save_accounttype(1);
                            }
                            appPreference.save_auth(vals[0]);
                            appPreference.save_buss(vals[2]);
                            appPreference.save_contactp(vals[3]);
                            appPreference.save_phone(vals[4]);
                            ret[0] = 1;
                        }
                    }
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
        } else {
            //validate the token now
            try {

                JSONObject jsonObject = Settings.validateToken(context);
                if (jsonObject != null) {
                    if (jsonObject.has(Constants.SUCCESS)) {
                        ret[0] = 1;
                    } else {
                        if (jsonObject.has(Constants.DEVICEACCOUNTTYPESERVER)) {
                            String account = jsonObject.getString(Constants.DEVICEACCOUNTTYPESERVER);
                            if (!account.isEmpty()) {
                                int acctype = appPreference.get_accounttype();
                                String currentac = null;
                                if (acctype == 0)
                                    currentac = Constants.ACCOUNT_TYPE_NORMAL;
                                if (acctype == 1)
                                    currentac = Constants.ACCOUNT_TYPE_PREMIUM;
                                if (!currentac.equals(account))
                                    ret[0] = 2;
                            }
                        }
                    }
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
        return ret[0];
    }

    protected void onPostExecute(final Integer ret) {
        super.onPostExecute(ret);
        btn.setVisibility(View.VISIBLE);
        bar.setProgress(100);
        if (ret == 1) {
            txt.setText(context.getString(R.string.strvalidtkenmsg));
            btn.setText(context.getString(R.string.strstart));
        } else if (ret == 2) {
            new DeviceRegistration(context, bar, txt, btn).execute(Constants.TAGREGISTRATION);
        } else {
            txt.setText(context.getString(R.string.strtokenerrormsg));
            btn.setText(context.getString(R.string.strclose));
        }
    }


}
