/*
 * The source codes in this project are owned by Bongolive Enterprises. Copying or any kind of usage without Bongolive's approval is against the law.
 * Copyright (c) 2015.
 *
 */

package bongolive.apps.proshop.view;

import android.accounts.Account;
import android.app.Dialog;
import android.app.SearchManager;
import android.content.ContentResolver;
import android.content.Context;
import android.database.Cursor;
import android.graphics.Color;
import android.net.Uri;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.support.v7.widget.SearchView;
import android.view.ContextMenu;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;


import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import bongolive.apps.proshop.R;
import bongolive.apps.proshop.controller.AppPreference;
import bongolive.apps.proshop.controller.Constants;
import bongolive.apps.proshop.controller.ItemList;
import bongolive.apps.proshop.controller.ItemListAdapter;
import bongolive.apps.proshop.controller.Validating;
import bongolive.apps.proshop.model.ContentProviderApi;
import bongolive.apps.proshop.model.Customers;
import bongolive.apps.proshop.model.Sales;
import bongolive.apps.proshop.model.Settings;


public class CustomersFragment extends Fragment implements OnItemClickListener,OnClickListener
{
    private static final String ARG_SECTION_NUMBER = "section_number";
    AlertDialogManager alert ;
    View _rootView;
    List<ItemList> list;
    ListView lv ;
    Button sub ;
    Dialog dialog ;
    ItemListAdapter adapter ;
    EditText clname,shname,phone,email,address,vrn,tin ;
    ArrayList<String> deletearray;
    SearchManager searchManager;
    SearchView searchView;
    TextView txtbuz,txtcli,txtadd,txtcont,txtit4;
    boolean tabletsize;
    FloatingActionButton actionButton ;
    AlertDialogManager alertDialogManager ;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
    {
        if(_rootView == null)
        {

            tabletsize = getResources().getBoolean(R.bool.isTablet);
            _rootView = inflater.inflate(R.layout.list_redesinged, container, false);
        } else {
//			((ViewGroup)_rootView.getParent()).removeView(_rootView) ;
        }
        return _rootView ;
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState)
    {
        super.onActivityCreated(savedInstanceState);
//        ((Dashboard)getActivity()).onSectionAttached(getArguments().getInt(ARG_SECTION_NUMBER));
        alert = new AlertDialogManager();

        if(tabletsize) {

            txtbuz = (TextView) getActivity().findViewById(R.id.txtbusiness);
            txtcli = (TextView) getActivity().findViewById(R.id.txtclient);
            txtadd = (TextView) getActivity().findViewById(R.id.txtadddress);
            txtcont = (TextView) getActivity().findViewById(R.id.txtcontact);
            txtit4 = (TextView) getActivity().findViewById(R.id.txtunkown);
            txtbuz.setText(getString(R.string.strbusiness));
            txtcli.setText(getString(R.string.strclientname));
            txtadd.setText(getString(R.string.straddress));
            txtcont.setText(getString(R.string.strclientphone));
            txtit4.setText(getString(R.string.strclientemail));
            txtadd.setVisibility(View.GONE);
            txtit4.setVisibility(View.GONE);
            alertDialogManager = new AlertDialogManager();
        }

        lv = (ListView)getActivity().findViewById(R.id.list);
        lv.setItemsCanFocus(false);

        adapter = new ItemListAdapter(getActivity(), getList(), 1);
        lv.setAdapter(adapter);
        lv.setOnItemClickListener(this);
        registerForContextMenu(lv);
        actionButton = (FloatingActionButton)getActivity().findViewById(R.id.addcustmer);
        actionButton.setBackgroundColor(getResources().getColor(R.color.actionbar));
        actionButton.setVisibility(View.VISIBLE);
        actionButton.setImageResource(R.drawable.ic_action_add);
        AppPreference appPreference = new AppPreference(getActivity());

        final int acctype = appPreference.get_accounttype();
        actionButton.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!Settings.getAddCustomerEnabled(getActivity())) {
                    Toast.makeText(getActivity(), getString(R.string.strnopermissionwarn), Toast.LENGTH_SHORT).show();
                    return;
                } else if(acctype == 0 && Customers.getCustomerCount(getActivity()) > 5){
                    Toast.makeText(getActivity(), getString(R.string.strcustwarn), Toast.LENGTH_SHORT).show();
                    return;
                }
                addcustomer();
            }
        });

        setHasOptionsMenu(true);
    }


    /* (non-Javadoc)
     * @see android.widget.AdapterView.OnItemClickListener#onItemClick(android.widget.AdapterView, android.view.View, int, long)
     */
    @Override
    public void onItemClick(AdapterView<?> parent, View view, final int position, final long id) {
        ItemList _list = list.get(position);
        String name = _list.getitemname();
        long i = _list.getId();

        Uri uri = Uri.withAppendedPath(Customers.BASEURI, String.valueOf(id));
        if (i > 0)
//                    showCustomer(i, name, 1);
            showCustomer(i, name, 0);
    }
    @Override
    public void onCreateContextMenu(ContextMenu menu, View v, ContextMenu.ContextMenuInfo menuInfo) {
        if (v.getId() == R.id.list) {
            ListView lv = (ListView) v;
            AdapterView.AdapterContextMenuInfo acmi = (AdapterView.AdapterContextMenuInfo) menuInfo;
            ItemList obj = (ItemList) lv.getItemAtPosition(acmi.position);

            menu.setHeaderTitle(getString(R.string.actioncenter));
            // Add all the menu options
            menu.add(Menu.NONE, Constants.CONTEXTMENU_EDIT, 0, getString(R.string.stredit));
            menu.add(Menu.NONE, Constants.CONTEXTMENU_DELETE, 1, getString(R.string.strdelete));
        }
    }

    @Override
    public boolean onContextItemSelected(MenuItem item) {
        AdapterView.AdapterContextMenuInfo info = (AdapterView.AdapterContextMenuInfo) item.getMenuInfo();
        ItemList _list = list.get(info.position);
        String name = _list.getitemname();
        long i = _list.getId();
        switch (item.getItemId()) {
            case Constants.CONTEXTMENU_EDIT:
                if(!Settings.getEditCustEnabled(getActivity()))
                {
                    Toast.makeText(getActivity(),getString(R.string.strnopermissionwarn),Toast.LENGTH_LONG).show();
                } else {
                    if (i > 0)
                        showCustomer(i, name, 1);
                }
                return true;
            case Constants.CONTEXTMENU_DELETE:
                if(!Settings.getEditCustEnabled(getActivity()))
                {
                    Toast.makeText(getActivity(),getString(R.string.strnopermissionwarn),Toast.LENGTH_LONG).show();
                } else {
                    Customers.deleteCustomer(getActivity(), String.valueOf(i));
                    adapter.notifyDataSetChanged();
                    adapter = new ItemListAdapter(getActivity(), getList(), 2);
                    lv.setAdapter(adapter);
                }
                return true;
            default:
                return super.onContextItemSelected(item);
        }
    }
    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {

        inflater.inflate(R.menu.addcustomer, menu);

        searchManager =  (SearchManager) getActivity().getSystemService(Context.SEARCH_SERVICE);
        searchView = (SearchView)menu.findItem(R.id.action_search).getActionView();
        searchView.setQueryHint(getString(R.string.strcustomersearchhint));
        searchView.setSearchableInfo(searchManager.getSearchableInfo(getActivity().getComponentName()));


        super.onCreateOptionsMenu(menu, inflater);
    }

    public List<ItemList> getList(){
        ContentResolver cr = getActivity().getContentResolver() ;
        String where = Customers.SYNCSTATUS + " != 3";
        Cursor c = cr.query(Customers.BASEURI, null, where, null, Customers.ID + " DESC");
        list = new ArrayList<ItemList>();
        try{
            if(c.getCount() > 0)
            {
                c.moveToFirst() ;
                do{
                    int colN = c.getColumnIndex(Customers.BUZINESSNAME);
                    int colP = c.getColumnIndex(Customers.MOBILE);
                    int colI = c.getColumnIndex(Customers.ID);
                    int colD = c.getColumnIndex(Customers.STREET);
                    int colPN = c.getColumnIndex(Customers.CUSTOMERNAME);
                    int colEM = c.getColumnIndex(Customers.CUSTOMERNAME);
                    String n = c.getString(colN);
                    String p = c.getString(colP);
                    String pn = c.getString(colPN);

                    long i = c.getLong(colI) ;
                    String d = c.getString(colD);
                    String em = c.getString(colD);
                    //String[] detailsc = Customers.getCustomer(getActivity(), colI);
                    if(tabletsize){
                        list.add(new ItemList(n.toUpperCase(Locale.getDefault()), pn.toUpperCase(Locale.getDefault()), p, i,
                                d));
                    } else {
                        list.add(new ItemList(getString(R.string.strbusiness).toUpperCase(Locale.getDefault()) + ": " +
                                n.toUpperCase(Locale.getDefault()),
                                getString(R.string.strclientname).toUpperCase(Locale.getDefault()) + ": " +
                                        pn.toUpperCase(Locale.getDefault()),
                                getString(R.string.strcontact).toUpperCase(Locale.getDefault()) + ": " + p, i,
                                getString(R.string.straddress).toUpperCase(Locale.getDefault()) + ": " + d));
                    }
                } while (c.moveToNext()) ;
            } else {
                String n = getString(R.string.strnodata).toUpperCase(Locale.getDefault()) ;
                String p = " " ;
                long i = 0 ;
                String d = null ;
                list.add(new ItemList(n,"", p, i,d));
                lv.setAdapter(adapter);
            }
        }finally{
            if(c != null) {
                c.close();
            }
        }
        return list ;
    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_addcustomer:
                if(!Settings.getAddCustomerEnabled(getActivity()))
                {
                    Toast.makeText(getActivity(),getString(R.string.strnopermissionwarn),Toast.LENGTH_LONG).show();
                } else {
                    addcustomer();
                }
                break;
            case R.id.action_search:
                getActivity().onSearchRequested();
                break;
        }

        return super.onOptionsItemSelected(item);
    }

    public void addcustomer(){

        LayoutInflater infl = getActivity().getLayoutInflater();
        View view = infl.inflate(R.layout.addcustomer, null);
        dialog = new Dialog(getActivity(), R.style.CustomDialog);
        dialog.setCancelable(false);
        dialog.setContentView(view);
        dialog.show();
        sub = (Button) dialog.findViewById(R.id.btnSubmit);
        TextView txttitle = (TextView) dialog.findViewById(R.id.txttitle);
        String title = getResources().getString(R.string.straddcustomer);
        title.toUpperCase(Locale.getDefault());
        txttitle.setText(title);

        actionButton = (FloatingActionButton)dialog.findViewById(R.id.action_addcustomer);
        actionButton.setBackgroundColor(getResources().getColor(R.color.actionbar));
        actionButton.setVisibility(View.VISIBLE);
        actionButton.setImageResource(R.drawable.ic_action_add);

        ImageView goback = (ImageView) dialog.findViewById(R.id.goback);
        goback.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });

        clname = (EditText) dialog.findViewById(R.id.etclientname);
        shname = (EditText) dialog.findViewById(R.id.etshopname);
        phone = (EditText) dialog.findViewById(R.id.etclientphone);
        vrn = (EditText) dialog.findViewById(R.id.etvrn);
        tin = (EditText) dialog.findViewById(R.id.ettin);
        email = (EditText) dialog.findViewById(R.id.etclientemail);
        address = (EditText) dialog.findViewById(R.id.etclientshopaddress);
        sub.setOnClickListener(this);
        actionButton.setOnClickListener(this);
    }
    /* (non-Javadoc)
     * @see android.view.View.OnClickListener#onClick(android.view.View)
     */
    @Override
    public void onClick(View v) {
        switch(v.getId())
        {
            case R.id.btnSubmit:
                String strclname,strshname,strclemail,strclphone,strcladdress,svrn,stin ;

                strclname = clname.getText().toString();
                strshname = Validating.stripit(shname.getText().toString());
                strclemail = Validating.stripit(email.getText().toString());
                strclphone = Validating.stripit(phone.getText().toString());
                svrn = vrn.getText().toString();
                stin = tin.getText().toString();
                strcladdress = address.getText().toString();

                if(!strclname.isEmpty())
                    if(!Constants.checkInvalidChars(getActivity(), strclname, clname))
                        return;
                if(!strshname.isEmpty())
                    if(!Constants.checkInvalidChars(getActivity(), strshname, shname))
                        return;
                if(!svrn.isEmpty())
                    if(!Constants.checkInvalidChars(getActivity(), svrn, vrn))
                        return;
                if(!stin.isEmpty())
                    if(!Constants.checkInvalidChars(getActivity(), stin, tin))
                        return;
                if(!strclphone.isEmpty())
                    if(!Constants.checkInvalidChars(getActivity(), strclphone, phone))
                        return;
                if(!strclemail.isEmpty())
                    if(!Constants.checkInvalidChars(getActivity(), strclemail, email))
                        return;

                if(!Validating.areSet(strcladdress))
                    strcladdress = "Dar es salaam";
                if(!Validating.areSet(strclname))
                    strclname = shname.getText().toString();
                EditText[] et = new EditText[]{clname,shname,phone,email,address} ;
                String[] input = new String[]{strclname,strshname,strclphone,strclemail,strcladdress,svrn,stin} ;
                String[] inputcheck = new String[]{strclname,strshname,strclphone} ;

                if(Validating.areSet(inputcheck)){
                    if(strclemail.isEmpty()) {
                        int insert = Customers.insertCustomerLocal(getActivity(), input);
                        adapter.notifyDataSetChanged();
                        if (insert == 1) {
                            adapter = new ItemListAdapter(getActivity(), getList(), 1);
                            adapter.notifyDataSetChanged();
                            lv.setAdapter(adapter);
                            Toast.makeText(getActivity(), getResources().getString(R.string.strinputsuccesscustomer), Toast.LENGTH_SHORT).show();
                            dialog.dismiss();
                        } else {
                            Toast.makeText(getActivity(), getResources().getString(R.string.strinputfailcustomer), Toast.LENGTH_SHORT).show();
                            dialog.dismiss();
                        }
                    } else if (!strclemail.isEmpty() && Validating.isEmailValid(strclemail)){

                        int insert = Customers.insertCustomerLocal(getActivity(), input);
                        if (insert == 1) {
                            Toast.makeText(getActivity(), getResources().getString(R.string.strinputsuccesscustomer), Toast.LENGTH_SHORT).show();
                            adapter.notifyDataSetChanged();
                            adapter = new ItemListAdapter(getActivity(), getList(), 1);
                            lv.setAdapter(adapter);
                            dialog.dismiss();
                        } else {
                            Toast.makeText(getActivity(), getResources().getString(R.string.strinputfailcustomer), Toast.LENGTH_SHORT).show();
                            dialog.dismiss();
                        }
                    } else {
                        et[3].setHint(getResources().getString(R.string.strinputerror)+
                                " "+ getResources().getString(R.string.strclientemail));
                        et[3].setHintTextColor(Color.RED) ;
                        et[3].setCompoundDrawablesWithIntrinsicBounds(getResources().getDrawable(R.drawable.ic_timer_auto_black_24dp), null, getResources().getDrawable(R.drawable.ic_error_red_24dp), null);}

                } else {
                    if(!Validating.areSet(strclname)){
                        et[0].setHint(getResources().getString(R.string.strinputerror) +
                                " "+ getResources().getString(R.string.strclientname));
                        et[0].setHintTextColor(Color.RED) ;
                        et[0].setCompoundDrawablesWithIntrinsicBounds(getResources().getDrawable(R.drawable.ic_timer_auto_black_24dp), null, getResources().getDrawable(R.drawable.ic_error_red_24dp), null);
                    }
                    if(!Validating.areSet(strshname)){
                        et[1].setHint(getResources().getString(R.string.strinputerror)+
                                " "+ getResources().getString(R.string.strclientshopname));
                        et[1].setHintTextColor(Color.RED) ;

                        et[1].setCompoundDrawablesWithIntrinsicBounds(getResources().getDrawable(R.drawable.ic_account_box_black_24dp), null, getResources().getDrawable(R.drawable.ic_error_red_24dp), null);}
                    if(!Validating.areSet(strclphone)){
                        et[2].setHint(getResources().getString(R.string.strinputerror)+
                                " "+ getResources().getString(R.string.strclientphone));
                        et[2].setHintTextColor(Color.RED) ;
                        et[2].setCompoundDrawablesWithIntrinsicBounds(getResources().getDrawable(R.drawable.ic_local_phone_black_24dp), null, getResources().getDrawable(R.drawable.ic_error_red_24dp), null);}
                    if(!Validating.areSet(strcladdress)){
                        et[4].setHint(getResources().getString(R.string.strinputerror)+
                                " "+ getResources().getString(R.string.strclientaddress));
                        et[4].setHintTextColor(Color.RED) ;
                        et[4].setCompoundDrawablesWithIntrinsicBounds(getResources().getDrawable(R.drawable.ic_pin_drop_black_24dp), null, getResources().getDrawable(R.drawable.ic_error_red_24dp), null);}
                    return;
                }
                break;

        }
    }

    private void showCustomer(final long i, final String businessname, int action){
        LayoutInflater infl = getActivity().getLayoutInflater();
        View view = infl.inflate(R.layout.addcustomer, null) ;

        dialog = new Dialog(getActivity(), R.style.CustomDialog	);
        dialog.setCancelable(true);
        dialog.setContentView(view);

        clname = (EditText)dialog.findViewById(R.id.etclientname);
        shname = (EditText)dialog.findViewById(R.id.etshopname);
        phone = (EditText)dialog.findViewById(R.id.etclientphone);
        email = (EditText)dialog.findViewById(R.id.etclientemail);
        address = (EditText)dialog.findViewById(R.id.etclientshopaddress) ;
        vrn = (EditText) dialog.findViewById(R.id.etvrn);
        tin = (EditText) dialog.findViewById(R.id.ettin);
        TextView txttitle = (TextView)dialog.findViewById(R.id.txttitle);
        txttitle.setText(businessname);

        ImageView goback = (ImageView)dialog.findViewById(R.id.goback);
        goback.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });

        Button edit = (Button)dialog.findViewById(R.id.btnSubmit);
        edit.setVisibility(View.GONE);

        if(action == 0){
            clname.setFocusable(false);
            shname.setFocusable(false);
            email.setFocusable(false);
            phone.setFocusable(false);
            address.setFocusable(false);
            vrn.setFocusable(false);
            tin.setFocusable(false);
            clname.setFocusableInTouchMode(false);
            shname.setFocusableInTouchMode(false);
            phone.setFocusableInTouchMode(false);
            address.setFocusableInTouchMode(false);
            vrn.setFocusableInTouchMode(false);
            tin.setFocusableInTouchMode(false);
            email.setFocusableInTouchMode(false);
        } else
            actionButton.setVisibility(View.VISIBLE);

        final String[] details = Customers.getCustomer(getActivity(), i);
        clname.setText(details[0]);
        shname.setText(details[1]);
        email.setText(details[2]);
        phone.setText(details[3]);
        address.setText(details[4]);
        vrn.setText(details[5]);
        tin.setText(details[6]);

        actionButton.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                String cl = clname.getText().toString();
                String sh  = shname.getText().toString();
                String em = email.getText().toString();
                String ph = phone.getText().toString();
                String add = address.getText().toString();
                String svrn = vrn.getText().toString();
                String stin = tin.getText().toString();
                if(!cl.isEmpty())
                    if(!Constants.checkInvalidChars(getActivity(), cl, clname))
                        return;
                if(!sh.isEmpty())
                    if(!Constants.checkInvalidChars(getActivity(), sh, shname))
                        return;
                if(!svrn.isEmpty())
                    if(!Constants.checkInvalidChars(getActivity(), svrn, vrn))
                        return;
                if(!stin.isEmpty())
                    if(!Constants.checkInvalidChars(getActivity(), stin, tin))
                        return;
                if(!ph.isEmpty())
                    if(!Constants.checkInvalidChars(getActivity(), ph, phone))
                        return;
                if(!em.isEmpty())
                    if(!Constants.checkInvalidChars(getActivity(), em, email))
                        return;
                if(!add.isEmpty())
                    if(!Constants.checkInvalidChars(getActivity(), add, address))
                        return;

                int userid = Customers.getCustomerlocalId(dialog.getContext(), details[1]);
                String[] vals = {cl,sh,ph,em,add,String.valueOf(userid),svrn,stin};
                String[] vals2 = {cl,sh,ph,add};
                if(Validating.areSet(vals2)){
                    int update = Customers.updateCustomerDetails(dialog.getContext(), vals);
                    if(update > 0){
                        Toast.makeText(dialog.getContext(), getString(R.string.itemupdated),Toast.LENGTH_SHORT).show();
                        dialog.dismiss();
                    }
                }
            }

        });
        dialog.show();
    }


    private List<ItemList> getCustomerHistory(long i) {
        ContentResolver cr = dialog.getContext().getContentResolver();
        String where = Sales.CUSTOMERID + " = " + i;
        Cursor c = cr.query(Sales.BASEURI,null,where,null,null);
        ArrayList<ItemList> itemLists = new ArrayList<>(c.getCount());
        if(c.moveToFirst()){
            do{
                int colN = c.getColumnIndex(Sales.TOTALSALES);
                int colP = c.getColumnIndex(Sales.PAYMENTAMOUNT);
                int colI = c.getColumnIndex(Sales.ID);
                int colD = c.getColumnIndex(Sales.ORDERDATE);
                String n = c.getString(colN);
                String p = c.getString(colP);
                long ii = c.getLong(colI) ;
                String d = c.getString(colD);

                itemLists.add(new ItemList(getString(R.string.strordervalue).toUpperCase(Locale.getDefault())+ ": "+
                        n.toUpperCase(Locale.getDefault()),
                        "",
                        getString(R.string.stramountpaid).toUpperCase(Locale.getDefault())+ ": "+p, i,
                        getString(R.string.strorderdate).toUpperCase(Locale.getDefault())+ ": "+d));
            } while (c.moveToNext());
        }
        return itemLists;
    }

    public void ondemandsync()
    {
        Account mAccount = mAccount = Constants.createSyncAccount(getActivity());
        Bundle settingsBundle = new Bundle() ;
        settingsBundle.putBoolean(ContentResolver.SYNC_EXTRAS_MANUAL, true);
        settingsBundle.putBoolean(ContentResolver.SYNC_EXTRAS_EXPEDITED, true);
        ContentResolver.requestSync(mAccount, ContentProviderApi.AUTHORITY, settingsBundle);
    }
/* todo
 * add search functionality
 */
}
