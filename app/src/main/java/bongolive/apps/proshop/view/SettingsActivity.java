/*
 * The source codes in this project are owned by Bongolive Enterprises. Copying or any kind of usage without Bongolive's approval is against the law.
 * Copyright (c) 2015.
 *
 */

package bongolive.apps.proshop.view;

import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.ActivityInfo;
import android.os.Bundle;
import android.preference.CheckBoxPreference;
import android.preference.EditTextPreference;
import android.preference.ListPreference;
import android.preference.Preference;
import android.preference.PreferenceActivity;
import android.util.Log;
import android.view.KeyEvent;

import bongolive.apps.proshop.R;
import bongolive.apps.proshop.controller.AppPreference;
import bongolive.apps.proshop.model.ExportDatabase;
import bongolive.apps.proshop.model.Settings;
import bongolive.apps.proshop.model.User;
import bongolive.apps.proshop.controller.Constants;

public class SettingsActivity extends PreferenceActivity implements SharedPreferences
.OnSharedPreferenceChangeListener {
    ListPreference listsync, listlang, listcurrency;
    CheckBoxPreference vatpref, printrecptpref, discountpref, postpaid, register,reqpin,bluetooth,preforders;
    AppPreference appPreference;
    CheckBoxPreference litepref,prefstock;
    CheckBoxPreference dbimportpref, addprod, addcust, editprod, editcust, exportdata, prefuser,prefdelivery;
    EditTextPreference etdiscontvalue;
    boolean tabletSize = false;

    @Override
    public void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);

        tabletSize = getResources().getBoolean(R.bool.isTablet);
        if (!tabletSize) {
            setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        }
        addPreferencesFromResource(R.xml.pref_general);
        appPreference = new AppPreference(getApplicationContext());
        listlang = (ListPreference) findPreference(Constants.PREFLANG);
        listsync = (ListPreference) findPreference(Constants.PREFSYNC);
        listcurrency = (ListPreference) findPreference(Constants.PREFCURRENCY);
        vatpref = (CheckBoxPreference) findPreference(Constants.PREFTAX);
        printrecptpref = (CheckBoxPreference) findPreference(Constants.PREFRECEIPT);
        discountpref = (CheckBoxPreference) findPreference(Constants.PREFDISCOUNT);
        exportdata = (CheckBoxPreference) findPreference(Constants.PREF_EXPORT_KEY);
        dbimportpref = (CheckBoxPreference) findPreference(Constants.PREF_IMPORT_KEY);
        addcust = (CheckBoxPreference) findPreference(Constants.PREFADDCUST);
        editcust = (CheckBoxPreference) findPreference(Constants.PREFEDITCUST);
        addprod = (CheckBoxPreference) findPreference(Constants.PREFADDPROD);
        editprod = (CheckBoxPreference) findPreference(Constants.PREFEDITPRD);
        postpaid = (CheckBoxPreference) findPreference(Constants.PREFPOSTPAID);
        etdiscontvalue = (EditTextPreference) findPreference(Constants.PREFLIMITPRICE);
        register = (CheckBoxPreference) findPreference(Constants.PREFREGISTER);
        reqpin = (CheckBoxPreference)findPreference(Constants.PREFPIN);
        bluetooth = (CheckBoxPreference)findPreference(Constants.PREFBLUETOOTH);
        prefuser = (CheckBoxPreference)findPreference(Constants.PREFUSER);
        prefdelivery = (CheckBoxPreference)findPreference(Constants.PREFDELIVERY);
        preforders = (CheckBoxPreference)findPreference(Constants.PREFORDER);
        litepref = (CheckBoxPreference)findPreference(Settings.PROSHOPLITE);
        prefstock = (CheckBoxPreference)findPreference(Settings.STOCK_RECEIVE);
        /* true */
        if (Settings.getEditProdEnabled(this))
            editprod.setChecked(true);
        if (Settings.getEditCustEnabled(this))
            editcust.setChecked(true);
        if (Settings.getAddCustomerEnabled(this))
            addcust.setChecked(true);
        if (Settings.getAddProdEnabled(this))
            addprod.setChecked(true);
        if (Settings.getPrintEnabled(this))
            printrecptpref.setChecked(true);
        if (Settings.getExportEnabled(this))
            exportdata.setSelectable(true);
        if (Settings.getImportEnabled(this))
            dbimportpref.setSelectable(true);
        if (Settings.getTaxEnabled(this))
            vatpref.setChecked(true);
        if (Settings.getDiscountEnabled(this))
            discountpref.setChecked(true);
        if (Settings.getPayLaterEnabled(this))
            postpaid.setChecked(true);
        if (Settings.getUserEnabled(this))
            prefuser.setChecked(true);
        if (Settings.getDeliveryEnabled(this))
            prefdelivery.setChecked(true);
        if (Settings.getOrderEnabled(this))
            preforders.setChecked(true);
        if (Settings.getProshopLite(this))
            litepref.setChecked(true);
        if (Settings.getPuchItemEnabled(this))
            prefstock.setChecked(true);
        /* false */
        if (!Settings.getEditProdEnabled(this))
            editprod.setChecked(false);
        if (!Settings.getEditCustEnabled(this))
            editcust.setChecked(false);
        if (!Settings.getAddCustomerEnabled(this))
            addcust.setChecked(false);
        if (!Settings.getAddProdEnabled(this))
            addprod.setChecked(false);
        if (!Settings.getPrintEnabled(this))
            printrecptpref.setChecked(false);
        if (!Settings.getExportEnabled(this))
            exportdata.setSelectable(false);
        if (!Settings.getImportEnabled(this))
            dbimportpref.setSelectable(false);
        if (!Settings.getTaxEnabled(this))
            vatpref.setChecked(false);
        if (!Settings.getDiscountEnabled(this))
            discountpref.setChecked(false);
        if (!Settings.getPayLaterEnabled(this))
            postpaid.setChecked(false);
        if (!Settings.getUserEnabled(this))
            prefuser.setChecked(false);
        if (!Settings.getDeliveryEnabled(this))
            prefdelivery.setChecked(false);
        if (!Settings.getOrderEnabled(this))
            preforders.setChecked(false);
        if (!Settings.getProshopLite(this))
            litepref.setChecked(false);
        if (!Settings.getPuchItemEnabled(this))
            prefstock.setChecked(false);


        final String apptype = Settings.getApptypeEnabled(this);
        int accounttype = appPreference.get_accounttype();
        System.out.println("account type " + accounttype);
        if (accounttype == 2) {
            editprod.setSelectable(true);
            editcust.setSelectable(true);
            addcust.setSelectable(true);
            addprod.setSelectable(true);
            printrecptpref.setSelectable(true);
            exportdata.setSelectable(true);
            dbimportpref.setSelectable(true);
            vatpref.setSelectable(true);
            discountpref.setSelectable(true);
            postpaid.setSelectable(true);
            etdiscontvalue.setSelectable(true);
            prefuser.setSelectable(true);
            prefdelivery.setSelectable(true);
            preforders.setSelectable(true);
            litepref.setSelectable(true);
            prefstock.setSelectable(true);
        } else {
            if (apptype.equals(Constants.APPTYPE_SHOP)) {
                if(appPreference.getUserRole() != 0) {
                    editprod.setSelectable(true);
                    editcust.setSelectable(true);
                    addcust.setSelectable(true);
                    addprod.setSelectable(true);
                    printrecptpref.setSelectable(true);
                    exportdata.setSelectable(true);
                    dbimportpref.setSelectable(true);
                    vatpref.setSelectable(true);
                    discountpref.setSelectable(true);
                    postpaid.setSelectable(true);
                    etdiscontvalue.setSelectable(true);
                    prefuser.setEnabled(false);
                    prefdelivery.setEnabled(false);
                    preforders.setEnabled(false);
                    litepref.setSelectable(true);
                    litepref.setEnabled(true);
                    prefstock.setSelectable(true);
                    prefstock.setEnabled(true);
                } else {
                    listcurrency.setSelectable(false);
                    listsync.setSelectable(false);
                    prefdelivery.setEnabled(false);
                    preforders.setEnabled(false);
                    litepref.setEnabled(false);
                }
            }
        }


        editprod.setOnPreferenceClickListener(new Preference.OnPreferenceClickListener() {
            @Override
            public boolean onPreferenceClick(Preference preference) {
                if (editprod.isChecked())
                    Settings.updateVal(SettingsActivity.this, new String[]{Settings.PRODUCT_EDIT, "1"});
                if (!editprod.isChecked())
                    Settings.updateVal(SettingsActivity.this, new String[]{Settings.PRODUCT_EDIT, "0"});
                return false;
            }
        });
        editcust.setOnPreferenceClickListener(new Preference.OnPreferenceClickListener() {
            @Override
            public boolean onPreferenceClick(Preference preference) {
                if (editcust.isChecked())
                    Settings.updateVal(SettingsActivity.this, new String[]{Settings.CUSTOMER_EDIT, "1"});
                if (!editcust.isChecked())
                    Settings.updateVal(SettingsActivity.this, new String[]{Settings.CUSTOMER_EDIT, "0"});
                return false;
            }
        });
        addcust.setOnPreferenceClickListener(new Preference.OnPreferenceClickListener() {
            @Override
            public boolean onPreferenceClick(Preference preference) {
                if (addcust.isChecked())
                    Settings.updateVal(SettingsActivity.this, new String[]{Settings.CUSTOMER_ADD, "1"});
                if (!addcust.isChecked())
                    Settings.updateVal(SettingsActivity.this, new String[]{Settings.CUSTOMER_ADD, "0"});
                return false;
            }
        });
        addprod.setOnPreferenceClickListener(new Preference.OnPreferenceClickListener() {
            @Override
            public boolean onPreferenceClick(Preference preference) {
                if (addprod.isChecked())
                    Settings.updateVal(SettingsActivity.this, new String[]{Settings.PRODUCT_ADD, "1"});
                if (!addprod.isChecked())
                    Settings.updateVal(SettingsActivity.this, new String[]{Settings.PRODUCT_ADD, "0"});
                return false;
            }
        });
        printrecptpref.setOnPreferenceClickListener(new Preference.OnPreferenceClickListener() {
            @Override
            public boolean onPreferenceClick(Preference preference) {
                if (printrecptpref.isChecked())
                    Settings.updateVal(SettingsActivity.this, new String[]{Settings.PRINT, "1"});
                if (!printrecptpref.isChecked())
                    Settings.updateVal(SettingsActivity.this, new String[]{Settings.PRINT, "0"});
                return false;
            }
        });
        vatpref.setOnPreferenceClickListener(new Preference.OnPreferenceClickListener() {
            @Override
            public boolean onPreferenceClick(Preference preference) {
                if (vatpref.isChecked())
                    Settings.updateVal(SettingsActivity.this, new String[]{Settings.VAT, "1"});
                if (!vatpref.isChecked())
                    Settings.updateVal(SettingsActivity.this, new String[]{Settings.VAT, "0"});
                return false;
            }
        });
        discountpref.setOnPreferenceClickListener(new Preference.OnPreferenceClickListener() {
            @Override
            public boolean onPreferenceClick(Preference preference) {
                if (discountpref.isChecked())
                    Settings.updateVal(SettingsActivity.this, new String[]{Settings.DISCOUNT, "1"});
                if (!discountpref.isChecked())
                    Settings.updateVal(SettingsActivity.this, new String[]{Settings.DISCOUNT, "0"});
                return false;
            }
        });
        postpaid.setOnPreferenceClickListener(new Preference.OnPreferenceClickListener() {
            @Override
            public boolean onPreferenceClick(Preference preference) {
                if (postpaid.isChecked())
                    Settings.updateVal(SettingsActivity.this, new String[]{Settings.PAYLATER, "1"});
                if (!postpaid.isChecked())
                    Settings.updateVal(SettingsActivity.this, new String[]{Settings.PAYLATER, "0"});
                return false;
            }
        });

        preforders.setOnPreferenceClickListener(new Preference.OnPreferenceClickListener() {
            @Override
            public boolean onPreferenceClick(Preference preference) {
                if (preforders.isChecked())
                    Settings.updateVal(SettingsActivity.this, new String[]{Settings.ORDERS, "1"});
                if (!preforders.isChecked())
                    Settings.updateVal(SettingsActivity.this, new String[]{Settings.ORDERS, "0"});
                return false;
            }
        });

        prefuser.setOnPreferenceClickListener(new Preference.OnPreferenceClickListener() {
            @Override
            public boolean onPreferenceClick(Preference preference) {
                if (prefuser.isChecked())
                    Settings.updateVal(SettingsActivity.this, new String[]{Settings.USERS, "1"});
                if (!prefuser.isChecked())
                    Settings.updateVal(SettingsActivity.this, new String[]{Settings.USERS, "0"});
                return false;
            }
        });

        litepref.setOnPreferenceClickListener(new Preference.OnPreferenceClickListener() {
            @Override
            public boolean onPreferenceClick(Preference preference) {
                if (litepref.isChecked())
                    Settings.updateVal(SettingsActivity.this, new String[]{Settings.PROSHOPLITE, "1"});
                if (!litepref.isChecked())
                    Settings.updateVal(SettingsActivity.this, new String[]{Settings.PROSHOPLITE, "0"});
                return false;
            }
        });

        prefstock.setOnPreferenceClickListener(new Preference.OnPreferenceClickListener() {
            @Override
            public boolean onPreferenceClick(Preference preference) {
                if (prefstock.isChecked())
                    Settings.updateVal(SettingsActivity.this, new String[]{Settings.STOCK_RECEIVE, "1"});
                if (!prefstock.isChecked())
                    Settings.updateVal(SettingsActivity.this, new String[]{Settings.STOCK_RECEIVE, "0"});
                return false;
            }
        });

        prefdelivery.setOnPreferenceClickListener(new Preference.OnPreferenceClickListener() {
            @Override
            public boolean onPreferenceClick(Preference preference) {
                if (prefdelivery.isChecked())
                    Settings.updateVal(SettingsActivity.this, new String[]{Settings.DELIVERY, "1"});
                if (!prefdelivery.isChecked())
                    Settings.updateVal(SettingsActivity.this, new String[]{Settings.DELIVERY, "0"});
                return false;
            }
        });


        etdiscontvalue.setOnPreferenceChangeListener(new Preference.OnPreferenceChangeListener() {
            @Override
            public boolean onPreferenceChange(Preference preference, Object newValue) {
                return false;
            }
        });


        etdiscontvalue.setTitle(getString(R.string.pref_title_discountlimit) + " " + Settings.getDiscountLimit(this));

        exportdata.setOnPreferenceClickListener(new Preference.OnPreferenceClickListener() {
            @Override
            public boolean onPreferenceClick(Preference preference) {
                if (exportdata.isChecked()) {
//					Dialog d = new Dialog(getApplicationContext());
                    ExportDatabase ex = new ExportDatabase(SettingsActivity.this, exportdata);
                    ex.execute();
                }
                return false;
            }
        });


        register.setOnPreferenceClickListener(new Preference.OnPreferenceClickListener() {
            @Override
            public boolean onPreferenceClick(Preference preference) {
                if (register.isChecked()) {
                    appPreference.clear_authentications();
                }
                register.setChecked(false);
                return false;
            }
        });

        if(User.getCount(this) == 1){
            reqpin.setEnabled(false);
            reqpin.setSelectable(false);
        }

        reqpin.setOnPreferenceClickListener(new Preference.OnPreferenceClickListener() {
            @Override
            public boolean onPreferenceClick(Preference preference) {
                if(reqpin.isChecked()){
                    appPreference.save_pin_options(true);
                } else {
                    appPreference.save_pin_options(false);
                }
                return false;
            }
        });

        bluetooth.setOnPreferenceClickListener(new Preference.OnPreferenceClickListener() {
            @Override
            public boolean onPreferenceClick(Preference preference) {
                if(bluetooth.isChecked()){
                    appPreference.set_bluetooth(true);
                } else {
                    appPreference.set_bluetooth(false);
                }
                return false;
            }
        });

        setListPreferenceData(listlang);
        setListPreferenceDataCurrency(listcurrency);
        setListPreferenceDataSync(listsync);
        listlang.setOnPreferenceClickListener(new Preference.OnPreferenceClickListener() {
            @Override
            public boolean onPreferenceClick(Preference preference) {
                setListPreferenceData(listlang);
                return false;
            }
        });
        listcurrency.setOnPreferenceClickListener(new Preference.OnPreferenceClickListener() {
            @Override
            public boolean onPreferenceClick(Preference preference) {
                setListPreferenceDataCurrency(listcurrency);
                return false;
            }
        });
        listsync.setOnPreferenceClickListener(new Preference.OnPreferenceClickListener() {
            @Override
            public boolean onPreferenceClick(Preference preference) {
                setListPreferenceDataSync(listsync);
                return false;
            }
        });
    }

    @Override
    public void onSharedPreferenceChanged(SharedPreferences sharedPreferences, String key) {
        if (key.equals(Constants.PREFLANG)) {
            Preference langpref = findPreference(key);
            langpref.setSummary(sharedPreferences.getString(key, ""));
            System.out.println(" restarted ");
            restartActivity();
        }
        if (key.equals(Constants.PREFSYNC)) {
            Preference syncpref = findPreference(key);
            syncpref.setSummary(sharedPreferences.getString(key, ""));
        }
        if (key.equals(Constants.PREFCURRENCY)) {
            Preference currepref = findPreference(key);
            currepref.setSummary(sharedPreferences.getString(key, ""));
        }
    }

    protected static void setListPreferenceData(ListPreference lp) {

        lp.setEntries(R.array.pref_default_language_titles);
        lp.setEntryValues(R.array.pref_default_language_values);
        lp.setDefaultValue(0);
    }

    protected static void setListPreferenceDataCurrency(ListPreference lp) {

        lp.setEntries(R.array.pref_default_currency_titles);
        lp.setEntryValues(R.array.pref_default_currency_values);
        lp.setDefaultValue(1);
    }

    protected static void setListPreferenceDataSync(ListPreference lp) {

        lp.setEntries(R.array.pref_sync_frequency_titles);
        lp.setEntryValues(R.array.pref_sync_frequency_values);
        lp.setDefaultValue(2);
    }

    private void restartActivity() {
        System.out.println(" restarted ");
        Intent intent = getIntent();
        finish();
        startActivity(intent);
    }

    @Override
    public boolean onKeyUp(int keyCode, KeyEvent event) {
        if (keyCode == KeyEvent.KEYCODE_BACK) {
            Log.i("event", "captured");
            startActivity(new Intent(this, MainScreen.class));
            finish();
            return true;
        } else
            return super.onKeyUp(keyCode, event);

    }

}