/*
 * The source codes in getContext() project are owned by Bongolive Enterprises. Copying or any kind of usage without Bongolive's approval is against the law.
 * Copyright (c) 2015.
 *
 */

package bongolive.apps.proshop.view;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.Log;
import android.view.ContextMenu;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.SpinnerAdapter;
import android.widget.TextView;
import android.widget.Toast;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.ArrayList;
import java.util.Locale;

import bongolive.apps.proshop.R;
import bongolive.apps.proshop.controller.AppPreference;
import bongolive.apps.proshop.controller.Constants;
import bongolive.apps.proshop.controller.StockItemListAdapter;
import bongolive.apps.proshop.controller.Validating;
import bongolive.apps.proshop.model.Products;
import bongolive.apps.proshop.model.PurchaseItems;
import bongolive.apps.proshop.model.Transfer;
import bongolive.apps.proshop.model.TransferItems;
import bongolive.apps.proshop.model.Warehouse;


public class TransferRequestFrag extends Fragment implements AdapterView.OnItemClickListener,
        TextWatcher,View.OnCreateContextMenuListener
{
    View _rootView;
    Button btnSave,btnSavePrint;

    ListView lvitems ;
    AutoCompleteTextView edclientname,etproduct ;
    EditText etquantity,etprice,etexpirydate ;
    Spinner spwarehouse;
    SpinnerAdapter spwhadapter;
    ArrayList<String> whlist, whlistid;

    TextView txtsku,txtprice  ;
    AppPreference appPreference;

    ArrayAdapter<String> proadapter ;
    StockItemListAdapter adapter;
    String[] businesslist ;
    ArrayList<String> productlist,productcodelist,paystatus;

    static int __stockquantity ;
    int receipt = 0, payinfull = 0, quantitytype = 0;
    String orderstatus = null;
    static String _prodname = "" ;
    boolean tabletSize = false;
    Button btnsaveorder,btnadditem;
    String whname = "",whid = "";

    String __prodn = "",__prdcode = "";
    private ArrayList<StockItemList> marraylist = new ArrayList<StockItemList>();

    /*
    * end of new strings
    */
    MenuItem menuItem;


    /*
    * end of new strings
    */

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
    {
        _rootView = inflater.inflate(R.layout.addstock_full, container, false);
        return _rootView ;
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState)
    {
        super.onActivityCreated(savedInstanceState);

        appPreference = new AppPreference(getContext());

        createSalesForm();

        setHasOptionsMenu(true);
    }


    private void showToast(String txt) {
        Toast.makeText(getContext(), txt, Toast.LENGTH_LONG).show();
    }
    private void createSalesForm() {
        lvitems = (ListView) getActivity().findViewById(R.id.stocklist);
        adapter = new StockItemListAdapter(getContext(), marraylist);
        lvitems.setAdapter(adapter);
        lvitems.setOnCreateContextMenuListener(new View.OnCreateContextMenuListener() {

            @Override
            public void onCreateContextMenu(ContextMenu menu, View view,
                                            ContextMenu.ContextMenuInfo menuInfo) {
                getActivity().getMenuInflater().inflate(R.menu.removesale, menu);
                menu.setHeaderTitle(getResources().getString(R.string.strremovesale));
                menu.setHeaderIcon(R.drawable.ic_done_all_white_24dp);
                AdapterView.AdapterContextMenuInfo info = (AdapterView.AdapterContextMenuInfo) menuInfo;
                int position = info.position;
                StockItemList list = marraylist.get(position);
                showToast("ok " + list.getPrice());
                double purchaseprice = list.getPrice();

                marraylist.remove(position);
                adapter.notifyDataSetChanged();

            }
        });
        orderstatus = " (" + marraylist.size() + ")";

        productlist = Products.getAllProductsSyncd(getContext());
        productcodelist = Products.getAllProductsCodeSyncd(getContext());

        proadapter = new ArrayAdapter<String>(getContext(), android.R.layout.simple_list_item_1,
                productlist);

        etproduct = (AutoCompleteTextView) getActivity().findViewById(R.id.etstockprodname);
        etproduct.addTextChangedListener(this);
        etproduct.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                String pro = (String) parent.getItemAtPosition(position);
                etproduct.setText(pro);
                _prodname = pro;
                __prodn = pro;

                int pos = productlist.indexOf(pro);
                __prdcode = productcodelist.get(pos);
            }
        });
//            etproduct.setOnFocusChangeListener(this);
        etproduct.setAdapter(proadapter);
        etproduct.setThreshold(1);
        spwarehouse = (Spinner) getActivity().findViewById(R.id.spwarehouse);
        whlist = Warehouse.getAll(getContext());
        whlistid = Warehouse.getAllIds(getContext());

        spwhadapter = new ArrayAdapter<String>(getContext(), android.R.layout.simple_list_item_1, whlist);
        spwarehouse.setAdapter(spwhadapter);

        if (Warehouse.getCount(getContext()) == 1) {
            String wh = (String) spwhadapter.getItem(1);
            System.out.println("wh is " + wh);
            if (!wh.equals(null)) {
                spwarehouse.setSelection(1);
            }
        }
        spwarehouse.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                whname = "";
                whid = "";
                if (position > 0) {
                    whname = (String) parent.getItemAtPosition(position);
                    whid = whlistid.get(position);
                    System.out.println("whname is " + whname + " whid is " + whid);
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        etquantity = (EditText)  getActivity().findViewById(R.id.etstockqty);
        etexpirydate = (EditText)  getActivity().findViewById(R.id.etstockexpirydate);
        etexpirydate.setVisibility(View.GONE);

        etquantity.setOnFocusChangeListener(new View.OnFocusChangeListener() {

            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if (hasFocus) {

                    etprice.setEnabled(true);
                }
            }
        });
        etprice = (EditText)  getActivity().findViewById(R.id.etpurchaseprice);
        etprice.setVisibility(View.GONE);
        String lang = appPreference.getDefaultCurrency();
        lang = lang != null ? lang : "Tsh" ;
        if (lang.equals("Tshs") || lang.isEmpty() || lang.equals("-1")) {
            etprice.setCompoundDrawablesWithIntrinsicBounds(R.drawable.ic_attach_money_black_24dptz,0,0,0);
        } else {
            etprice.setCompoundDrawablesWithIntrinsicBounds(R.drawable.ic_attach_money_black_24dp,0,0,0);
        }

            btnsaveorder = (Button)  getActivity().findViewById(R.id.btnsaveorder);
            btnsaveorder.setText(getString(R.string.strsubmitrequest));
            btnadditem = (Button)  getActivity().findViewById(R.id.btnadditem);

            btnadditem.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    add_item();
                }
            });

            btnsaveorder.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    saveOrder();
                }
            });


        etquantity.setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                if (event.getAction() == KeyEvent.ACTION_DOWN && event.getKeyCode() == KeyEvent.KEYCODE_ENTER) {
                    Log.i("event", "captured");
                    InputMethodManager imm = (InputMethodManager)  getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
                    imm.hideSoftInputFromWindow( getActivity().getCurrentFocus().getWindowToken(), 0);
                    add_item();
                    return false;
                }
                return false;
            }
        });
    }

    @Override
    public void onResume() {
        super.onResume();

    }

/*

    @Override
    public boolean onKeyUp(int keyCode, KeyEvent event) {
        if(keyCode == KeyEvent.KEYCODE_BACK)
        {
            Log.i("event", "captured");
            if(marraylist.size()>0 || !__prdcode.isEmpty()){
                AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
                builder.setMessage(getString(R.string.strsalecancelwarn))
                        .setCancelable(false)
                        .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                marraylist.clear();
                                startActivity(new Intent(getContext(),MainScreen.class));
                            }
                        })
                        .setNegativeButton("No", new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                dialog.cancel();
                            }
                        });
                AlertDialog alert = builder.create();
                alert.show();
                return true;
            } else {
                startActivity(new Intent(getContext(),MainScreen.class));
                return true;
            }
        } else
            return super.onKeyUp(keyCode, event);
    }
*/

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

        if(view.getId() == R.id.autotxtsaleclientname){
            String name = (String) parent.getItemAtPosition(position);
            edclientname.setText(name);

            edclientname.clearFocus();
            etproduct.requestFocus();
        }
    }

    @Override
    public void beforeTextChanged(CharSequence s, int start, int count, int after) {

    }

    @Override
    public void onTextChanged(CharSequence s, int start, int before, int count) {

    }

    @Override
    public void afterTextChanged(Editable s) {
        String productname = etproduct.getText().toString();
        if (TextUtils.isEmpty(productname)) {
            etproduct.requestFocus();
            showToast(getString(R.string.strfilldata));
        }
        if(Validating.areSet(productname))
            __prdcode = Products.getProductIdUsingSku(getContext(),productname);
        System.out.println(" product code is "+__prdcode);
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater = getActivity().getMenuInflater();
        inflater.inflate(R.menu.add_sales_buttons, menu);

        MenuItem additem,saveitem;
        additem = (MenuItem)menu.findItem(R.id.action_add_item);
        additem.setVisible(false);
        saveitem = (MenuItem)menu.findItem(R.id.action_save);

        additem.setVisible(false);
        saveitem.setVisible(false);
        super.onCreateOptionsMenu(menu, inflater);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item)
    {
        switch(item.getItemId())
        {
            case R.id.action_save:
                saveOrder();
                break;
            case R.id.action_add_item:
                add_item();
                break;
        }

        return false;

    }

    private void add_item() {
        String p = __prdcode;
        String suserq = etquantity.getText().toString().trim();
        if(!Constants.checkDouble(getContext(), suserq, etquantity))
            return;
        int prodid = Products.getLocalProdid(getContext(), p);
        if(prodid == 0)
            return;
        String price = PurchaseItems.getUnitprice(getContext(), String.valueOf(prodid));

        String expiryd = "0000-00-00";
        String[] check = new String[]{p,suserq,price} ;
        String[] va = new String[]{p,suserq,price,expiryd} ;
        if(Validating.areSet(check)){
            if(new BigDecimal(suserq).intValue() > 0 && new BigDecimal(price).doubleValue() > 0) {

                double grandtotal = new BigDecimal(va[2]).setScale(2, RoundingMode.HALF_UP).doubleValue();
                int proid = Products.getLocalProdid(getContext(), va[0]);
                String exdate = va[3];
                if(exdate.isEmpty())
                    exdate = "2030-12-31" ;

                long i = 0;
                if (!marraylist.isEmpty() && marraylist != null) {
                    i = adapter.getCount() + 1;
                } else {
                    i = 1;
                }

                if (!va[1].isEmpty() && grandtotal > 0 ) {
                    marraylist.add(new StockItemList(p, new BigDecimal(va[1]).intValue(), getString(R
                            .string.strlabelindex) + " " + i,
                            grandtotal, proid, exdate));
                }

                adapter = new StockItemListAdapter(getContext(), marraylist);
                adapter.notifyDataSetChanged();

                lvitems.setAdapter(adapter);
                showToast(p.toUpperCase(Locale.getDefault()) + " " + getString(R.string.straddedtocart));
                etproduct.setText("");
                etquantity.setText("");
                etprice.setText("");
                etexpirydate.setText("");
            } else {
                showToast(getString(R.string.strzeroqty));
            }
        } else {
            showToast(getString(R.string.straddproduct));
        }
        __prdcode = "";
        __prodn = "";
    }

    private void saveOrder() {
        if (whid.isEmpty())
            return;
        if (marraylist.size() != 0) {
            StockItemList list = null;

            boolean insert = false, inserttransfer = false;
            double gtotal = 0;

            for (int i = 0; i < marraylist.size(); i++){
                list = marraylist.get(i);
                gtotal += new BigDecimal(list.getPrice()).multiply(new BigDecimal(list.getQuantity()))
                        .setScale(2,RoundingMode.HALF_UP).doubleValue();
            }
            String note = "N/A";
            String total = String.valueOf(gtotal);
            String ttax = "0",status_ = Constants.TRANSTYPE_REQUEST;
            String wh = whid;
            String batch = "CAT/"+Constants.generateBatch();
            String systrnaid = "0",ack_ = "0",whf = "0",media = " ";
            String transidl = "0";
            String rec = "2";
            String[] transfer = {note,total,ttax,status_,total,wh,batch,systrnaid,ack_,whf,media,rec};
            if(Validating.areSet(transfer)){
                if(Transfer.insert(getContext(), transfer)){
                    transidl = Transfer.getId(getContext());
                    inserttransfer = true;
                }
            }

            list = null;
            if(inserttransfer && !transidl.equals("0"))
                for (int i = 0; i < marraylist.size(); i++) {
                    list = marraylist.get(i);
                    String status = "0", transitemid = "0";
                    String quantity = String.valueOf(list.getQuantity());
                    String expirydate = list.getExpirydate();
                    String sprice = String.valueOf(list.getPrice());
                    String productid = Products.getSysProdidBasedOnLocalId(getContext(), String.valueOf(list.getProdid()));
                    String transid = "0", ack = "0";
                    String timestamp = String.valueOf(System.currentTimeMillis());
                    String[] transferitem = new String[]{status, transitemid, transidl, quantity, expirydate
                            , sprice, productid, transid, timestamp,ack,quantity,ack};

                    insert = TransferItems.insert(getContext(), transferitem);

                    if (insert) {
                    /*Products.updateProductQty(this, new int[]{list.getProdid(), list
                            .getQuantity(), 0});*/
                        // we wont update the quantity until the status shows approved
                    }
                }
            if (insert) {
                showToast(getString(R.string.strrqtsubmitted));
                marraylist.clear();
                adapter.notifyDataSetChanged();
            }
        }
    }


}
