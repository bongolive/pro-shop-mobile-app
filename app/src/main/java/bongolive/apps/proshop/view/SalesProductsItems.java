/*
 * Copyright (c) 2015.
 * Prosurvey is a property of Bongolive Enterprises Ltd. An authorized
 * copy of ideas, source codes is against the regulations
 */

package bongolive.apps.proshop.view;

import java.io.Serializable;

/**
 * @author nasznjoka
 * 
 * Oct 9, 2014
 *
 */
public class SalesProductsItems implements Serializable{
	String product, sku;
	int quantity;
    long prodid;
	double unit, subtotal;

	public SalesProductsItems(String product, int quantity, double unit, double subtotal,String sku, long id) {
		this.product = product;
		this.quantity = quantity;
		this.unit = unit;
		this.subtotal = subtotal;
        this.prodid = id;
		this.sku = sku;
	}

	public SalesProductsItems() {

	}

	public String getProduct() {
		return product;
	}
	public String getSku() {
		return sku;
	}

	public int getQuantity() {
		return quantity;
	}

	public double getUnit() {
		return unit;
	}

	public double getSubtotal() {
		return subtotal;
	}
    public long getProdid(){
        return prodid;
    }
}
