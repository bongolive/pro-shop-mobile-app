/*
 * The source codes in this project are owned by Bongolive Enterprises. Copying or any kind of usage without Bongolive's approval is against the law.
 * Copyright (c) 2015.
 *
 */


package bongolive.apps.proshop.view;


import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AutoCompleteTextView;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;

import java.util.ArrayList;

import bongolive.apps.proshop.R;
import bongolive.apps.proshop.controller.PaymentsDetailsAdapter;
import bongolive.apps.proshop.controller.Validating;
import bongolive.apps.proshop.model.Payments;
import bongolive.apps.proshop.model.Sales;

public class PaymentDetail extends AppCompatActivity{

    private boolean tabletSize = false;
    private RecyclerView mRecyclerView;
    private RecyclerView.Adapter mAdapter;
    private RecyclerView.LayoutManager mLayoutManager;
    private ActionBar actionBar;
    private EditText etfilter;
    private ArrayList arrayList;
    private RelativeLayout rl;
    private ImageView imgdone,imgnext;
    private AutoCompleteTextView bzn;
    private ActionBar ab;
    ArrayList<Payments> mItems;
    FloatingActionButton actionButton;
    String orderid;
    long pyid = 0;
    String title;
    double grandbalance,grandpaid;
    /*
    * end of new strings
    */
    MenuItem menuItem;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        tabletSize = getResources().getBoolean(R.bool.isTablet);
        if (!tabletSize) {
            setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        }
        setContentView(R.layout.selling_screen);

        ab = getSupportActionBar();
        ab.setDisplayUseLogoEnabled(true);
        ab.setDisplayHomeAsUpEnabled(true);

        final Intent intent = getIntent();
        pyid = intent.getExtras().getLong(Payments.ID);
        title = intent.getExtras().getString(Sales.CUSTOMERID);
        if(pyid == 0 )
            finish();

        if(Validating.areSet(title))
            ab.setTitle(title.toUpperCase());
        else
            ab.setTitle(getString(R.string.strpayment));

        actionButton = (FloatingActionButton)findViewById(R.id.actionbutton);
        actionButton.setVisibility(View.GONE);
//        actionButton.setBackgroundColor(getResources().getColor(R.color.actionbar));
//        actionButton.setImageResource(R.drawable.ic_pay);

        mRecyclerView = (RecyclerView) findViewById(R.id.prodct_list);
        mRecyclerView.setHasFixedSize(true);
        mRecyclerView.setItemAnimator(new DefaultItemAnimator());

        mLayoutManager = new LinearLayoutManager(this);
        mRecyclerView.setLayoutManager(mLayoutManager);
        mItems = Payments.getPaymentSummary(this, pyid);
        Payments p = mItems.get(0);

        orderid = p.getLocalsale();
        grandbalance = Sales.getDebt(this, orderid);
        System.out.println(" grand blance "+grandbalance);

        mAdapter = new PaymentsDetailsAdapter(this, mItems);
        mRecyclerView.setAdapter(mAdapter);

        actionButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent1 = new Intent(PaymentDetail.this, MakePayment.class);
                intent1.putExtra(Sales.CUSTOMERID, title);
                intent1.putExtra(Payments.ID, pyid);
                startActivity(intent1);
            }
        });
    }

    @Override
    protected void onResume() {
        super.onResume();
        if(pyid != 0 && mRecyclerView != null && mAdapter != null){
            mAdapter.notifyDataSetChanged();
        }
        if(orderid != null){
            grandbalance = Sales.getDebt(this, orderid);

            if(grandbalance > 0){
                if(menuItem != null)
                    menuItem.setVisible(true);
            } else
            if(menuItem != null)
                menuItem.setVisible(false);
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.audit, menu);
        menuItem = menu.findItem(R.id.action_tick);
        menuItem.setIcon(R.drawable.ic_action_send);

        grandbalance = Sales.getDebt(this, orderid);

        if(grandbalance > 0){
            menuItem.setVisible(true);
        } else
            menuItem.setVisible(false);

        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                return true;
            case R.id.action_tick:
                Intent intent1 = new Intent(PaymentDetail.this, MakePayment.class);
                intent1.putExtra(Sales.CUSTOMERID, title);
                intent1.putExtra(Payments.ID, pyid);
                startActivity(intent1);
                break;
        }
        return super.onOptionsItemSelected(item);
    }
}


