/*
 * The source codes in this project are owned by Bongolive Enterprises. Copying or any kind of usage without Bongolive's approval is against the law.
 * Copyright (c) 2015.
 *
 */

package bongolive.apps.proshop.view;

import android.app.Dialog;
import android.app.SearchManager;
import android.content.ContentResolver;
import android.database.Cursor;
import android.database.DatabaseUtils;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.support.v7.widget.SearchView;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import bongolive.apps.proshop.R;
import bongolive.apps.proshop.controller.AppPreference;
import bongolive.apps.proshop.controller.Constants;
import bongolive.apps.proshop.controller.DeliveringAdapter;
import bongolive.apps.proshop.controller.ItemList;
import bongolive.apps.proshop.controller.ItemListAdapter;
import bongolive.apps.proshop.controller.ItemListAdapterOrderItems;
import bongolive.apps.proshop.model.Customers;
import bongolive.apps.proshop.model.Delivery;
import bongolive.apps.proshop.model.Sales;
import bongolive.apps.proshop.model.SalesItems;

public class DeliveriesFragment extends Fragment implements AdapterView.OnItemClickListener {
    private static final String ARG_SECTION_NUMBER = "section_number";
    AlertDialogManager alert;
    View _rootView;
    ListView lv;
    Button sub;
    Dialog dialog;
    ItemListAdapter adapter;
    List<ItemList> list;
    private final int ADDSTOCK = 1;
    private final int GETPRODUCTSKU = 3;
    SearchManager searchManager;
    SearchView searchView;
    TextView txtbuz, txtcli, txtadd, txtcont, txtunk;
    boolean tabletsize;
    AppPreference appPreference;
    String productcode;
    public static int IMPORT_SOURCE = 0;
    MenuItem item_receivestock, item_importstock;

    private final int REQUEST_CODE_PICK_DIR = 1;
    private final int REQUEST_CODE_PICK_FILE = 2;
    FloatingActionButton actionButton;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        if (_rootView == null) {
            _rootView = inflater.inflate(R.layout.list_redesinged, container, false);
        } else {
//			((ViewGroup)_rootView.getParent()).removeView(_rootView) ;
        }
        return _rootView;
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
//        ((Dashboard) getActivity()).onSectionAttached(getArguments().getInt(ARG_SECTION_NUMBER));

        
        tabletsize = getResources().getBoolean(R.bool.isTablet);
        if (tabletsize) {

            txtbuz = (TextView) getActivity().findViewById(R.id.txtbusiness);
            txtcli = (TextView) getActivity().findViewById(R.id.txtclient);
            txtadd = (TextView) getActivity().findViewById(R.id.txtadddress);
            txtcont = (TextView) getActivity().findViewById(R.id.txtcontact);
            txtunk = (TextView) getActivity().findViewById(R.id.txtunkown);
            txtbuz.setText(getString(R.string.strsaleref));
            txtcli.setText(getString(R.string.strdeldate));
            txtadd.setText(getString(R.string.strdelref));
            txtcont.setText(getString(R.string.strcustomer));
            txtunk.setText(getString(R.string.straddress));
            txtbuz.setVisibility(View.GONE);
            txtadd.setVisibility(View.GONE);
        }

        lv = (ListView) getActivity().findViewById(R.id.list);

        adapter = new ItemListAdapter(getActivity(), getDelList(), 3);
        lv.setAdapter(adapter);
        lv.setOnItemClickListener(this);
        registerForContextMenu(lv);
        appPreference = new AppPreference(getActivity());
        setHasOptionsMenu(true);
    }


    public List<ItemList> getDelList() {
        ContentResolver cr = getActivity().getContentResolver();
        String where = Delivery.DELIVERYSTATUS + " = "+ DatabaseUtils.sqlEscapeString(Constants.SALE_STATUS_PNDG);
        Cursor c = cr.query(Delivery.BASEURI, null, where, null, Delivery.DELIVERYDATE + " DESC");
        list = new ArrayList<ItemList>();
        try {
            if (c.getCount() > 0) {
                c.moveToFirst();
                do {
                    int colN = c.getColumnIndex(Delivery.SALE_REFERENCE);
                    int colP = c.getColumnIndex(Delivery.REFRENCENO);
                    int colI = c.getColumnIndex(Delivery.ID);
                    int colSL = c.getColumnIndex(Delivery.LOCALSALE);
                    int colD = c.getColumnIndex(Delivery.DELIVERYDATE);
                    int colPr = c.getColumnIndex(Delivery.LOCALCUSTOMER);
                    int colUp = c.getColumnIndex(Delivery.ADDRESS);
                    String sref = c.getString(colN);
                    String drefno = c.getString(colP);
                    long i = c.getLong(colI);
                    long si = c.getLong(colSL);
                    String deldate = c.getString(colD);
                    String cs = c.getString(colPr);
                    String customer = Customers.getCustomerBusinessNameLoca(getActivity(), cs);
                    String address = c.getString(colUp);

                    if (tabletsize) {
                        list.add(new ItemList(sref.toUpperCase(Locale.getDefault()), customer, drefno, i,
                                deldate, address,si));
                    } else {
                        list.add(new ItemList(getString(R.string.strsaleref).toUpperCase(Locale.getDefault()) + ": "
                                + sref,
                                getString(R.string.strcustomer).toUpperCase(Locale.getDefault()) + ": " +
                                        customer.toUpperCase(Locale.getDefault()),
                                getString(R.string.strdelref).toUpperCase(Locale.getDefault()) + ": " + drefno, i,
                                getString(R.string.strdeldate).toUpperCase(Locale.getDefault()) + ": " + deldate,
                                getString(R.string.straddress).toUpperCase(Locale.getDefault()) + ": " + address,si));
                    }
                } while (c.moveToNext());
            } else {
                String n = getString(R.string.strnodata).toUpperCase(Locale.getDefault());
                String p = " ";
                long i = 0;
                String d = null;
                list.add(new ItemList(n, n, p, 0, d,d,i));
            }
        } finally {
            if (c != null) {
                c.close();
            }
        }
        return list;
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, final int position, final long id) {
        ItemList _list = list.get(position);
        String name = _list.getBuss();
        long i = _list.getSaleid();
        if (i > 0){
            showDelivery(i, name);
            List<SalesItems> listarray = SalesItems.getItems(getActivity(), i);
            boolean isSalePending = Sales.isSalePending(getActivity(), i);
            if(isSalePending){
                for(int k = 0; k < listarray.size(); k++){
                    SalesItems items = listarray.get(k);
                    String[] v = new String[]{String.valueOf(items.getId()),String.valueOf(items.getQty())};
                    boolean rollbk = SalesItems.rollQtyBack(getActivity(),v);
                    System.out.println("rolled back qty ? "+rollbk);
                }
            }
        }
    }



    public void showDelivery(final long id, final String businessname) {
        LayoutInflater infl = getActivity().getLayoutInflater();
        View view = infl.inflate(R.layout.deliverylist, null);

        final Dialog dialog = new Dialog(getActivity(), R.style.CustomDialog);
        dialog.setCancelable(true);
        dialog.setContentView(view);
        ListView listView = (ListView) dialog.findViewById(R.id.list);
        ItemListAdapterOrderItems salesitemsadapter = new ItemListAdapterOrderItems(getActivity(), SalesItems
                .getItems(dialog.getContext(), id));
        listView.setAdapter(salesitemsadapter);
        TextView txt = (TextView) dialog.findViewById(R.id.txttitle);
        txt.setText(businessname);
        actionButton = (FloatingActionButton)dialog.findViewById(R.id.actionbutton);

        actionButton.setBackgroundColor(getResources().getColor(R.color.actionbar));
        actionButton.setImageResource(R.drawable.ic_action_send);

        ImageView imageView = (ImageView) dialog.findViewById(R.id.goback);
        imageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });
        TextView txtbusiness,txtselref,txtdeldate,txtnote;
        txtbusiness = (TextView)dialog.findViewById(R.id.txtdelcust);
        txtdeldate = (TextView)dialog.findViewById(R.id.txtdeldate);
        txtselref = (TextView)dialog.findViewById(R.id.txtsaleref);
        txtnote = (TextView)dialog.findViewById(R.id.txtcomment);
        ArrayList<String> delist = Delivery.deliveryDetails(dialog.getContext(), id);
        if(delist.size() >0){
            txtselref.setText(getString(R.string.strsaleref).toUpperCase(Locale.getDefault()).concat(" : "+delist.get(2)));
            txtdeldate.setText(getString(R.string.strdeldate).toUpperCase(Locale.getDefault()).concat(" : "+delist.get(1)));
            txtbusiness.setText(getString(R.string.straddress).toUpperCase(Locale.getDefault()).concat(" : "+delist.get(6)));
            txtnote.setText(getString(R.string.strnotes).toUpperCase(Locale.getDefault()).concat(" : "+ Html.fromHtml(delist.get(7)))
            );
        }

            actionButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    List<SalesItems> listarray = SalesItems.getItems(dialog.getContext(), id);
                    if (listarray != null && listarray.size() > 0) {
                        process_delivery(id,listarray,dialog);
                    }
                }

                private void process_delivery(long id, List<SalesItems> listarray, Dialog __d) {
                    LayoutInflater infl = getActivity().getLayoutInflater();
                    View view = infl.inflate(R.layout.delivery, null);

                    final Dialog dg = new Dialog(getActivity(), R.style.CustomDialog);
                    dg.setCancelable(true);
                    dg.setContentView(view);

                    actionButton = (FloatingActionButton)dg.findViewById(R.id.actionbutton);

                    actionButton.setBackgroundColor(getResources().getColor(R.color.actionbar));
                    actionButton.setImageResource(R.drawable.ic_action_send);
                    ListView listView = (ListView) dg.findViewById(R.id.list);
                    DeliveringAdapter _adapter = new DeliveringAdapter(dg.getContext(),listarray,actionButton,new
                            Dialog[]{__d,dg},id);
                    listView.setAdapter(_adapter);

                    ImageView imageView = (ImageView) dg.findViewById(R.id.goback);
                    imageView.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            dg.dismiss();
                        }
                    });
                    dg.show();
                }
            });

        dialog.show();
    }

}
