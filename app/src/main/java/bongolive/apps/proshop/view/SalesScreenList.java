/*
 * The source codes in this project are owned by Bongolive Enterprises. Copying or any kind of usage without Bongolive's approval is against the law.
 * Copyright (c) 2015.
 *
 */


package bongolive.apps.proshop.view;


import android.content.ContentResolver;
import android.content.Context;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.database.Cursor;
import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.AutoCompleteTextView;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.RelativeLayout;

import java.util.ArrayList;

import bongolive.apps.proshop.R;
import bongolive.apps.proshop.controller.Constants;
import bongolive.apps.proshop.controller.SalesAdapter;
import bongolive.apps.proshop.model.Products;

public class SalesScreenList extends AppCompatActivity{

    private boolean tabletSize = false;
    private ListView mRecyclerView;
    private SalesAdapter mAdapter;
    private RecyclerView.LayoutManager mLayoutManager;
    private ActionBar actionBar;
    private EditText etfilter;
    private ArrayList arrayList;
    private RelativeLayout rl;
    private ImageView imgdone,imgnext;
    private AutoCompleteTextView bzn;
    /*
    * end of new strings
    */

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        tabletSize = getResources().getBoolean(R.bool.isTablet);
        if (!tabletSize) {
            setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        }
        setContentView(R.layout.selling_screen_list);
        actionBar = getSupportActionBar();
        assert actionBar != null;
        actionBar.setDisplayShowCustomEnabled(true);
        actionBar.setDisplayShowTitleEnabled(false);

        mRecyclerView = (ListView)findViewById(R.id.prodct_list);
        bzn = (AutoCompleteTextView) findViewById(R.id.autotxtsaleclientname);

        LayoutInflater inflater = (LayoutInflater)this.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View v = inflater.inflate(R.layout.filter_product,null);
        actionBar.setCustomView(v);

        etfilter = (EditText)v.findViewById(R.id.etproductfilter);
        imgdone = (ImageView)v.findViewById(R.id.imgdone);
        imgnext = (ImageView)v.findViewById(R.id.imgnext);
        rl = (RelativeLayout)findViewById(R.id.rltotals);
        etfilter.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                etfilter.setText("");
            }
        });
        imgdone.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(SalesScreenList.this, Dashboard.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(intent);
                finish();
            }
        });

        mAdapter = new SalesAdapter(this,getlist(), imgnext, Constants.SALETYPE_SALE,rl);
        mRecyclerView.setAdapter(mAdapter);
    }

    public ArrayList<SalesProductsItems> getlist(){
        ArrayList<SalesProductsItems> list = new ArrayList<>();
        ContentResolver cr = getContentResolver();
//        String where = Products.REODER +" = 1";
        Cursor c = cr.query(Products.BASEURI,null,null,null,null);
        try{
                if(c.moveToFirst()){
                    do {
                        String prodname = c.getString(c.getColumnIndex(Products.PRODUCTNAME));
                        long id = c.getLong(c.getColumnIndex(Products.ID));
                        double unit = c.getDouble(c.getColumnIndex(Products.UNITPRICE));
                        int qty = 0;
                        String sku = c.getString(c.getColumnIndex(Products.SKU));
                        list.add(new SalesProductsItems(prodname,qty,unit,unit,sku,id));
                    } while (c.moveToNext());
                    rl.setVisibility(View.VISIBLE);
                }
        } finally {
            if(c != null){
                c.close();
            }
        }
        return list;
    }

}


