package bongolive.apps.proshop.view;

import android.accounts.Account;
import android.app.AlarmManager;
import android.app.Dialog;
import android.app.PendingIntent;
import android.bluetooth.BluetoothAdapter;
import android.content.ContentResolver;
import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.content.res.Configuration;
import android.graphics.Color;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.text.Html;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ExpandableListView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.Locale;
import java.util.concurrent.TimeUnit;

import bongolive.apps.proshop.R;
import bongolive.apps.proshop.controller.AppPreference;
import bongolive.apps.proshop.controller.ChildItem;
import bongolive.apps.proshop.controller.Constants;
import bongolive.apps.proshop.controller.GpsTracker;
import bongolive.apps.proshop.controller.GroupItem;
import bongolive.apps.proshop.controller.LocationTrigger;
import bongolive.apps.proshop.controller.MCrypt;
import bongolive.apps.proshop.controller.Menu_Adapter;
import bongolive.apps.proshop.controller.Validating;
import bongolive.apps.proshop.model.ContentProviderApi;
import bongolive.apps.proshop.model.Customers;
import bongolive.apps.proshop.model.Product_Categories;
import bongolive.apps.proshop.model.SalesItems;
import bongolive.apps.proshop.model.Settings;
import bongolive.apps.proshop.model.Warehouse;


public class Dashboard extends AppCompatActivity implements ExpandableListView.OnChildClickListener, ExpandableListView
        .OnGroupClickListener {

	private ExpandableListView drawerList;
	RelativeLayout mDrawerPane;
	private ActionBarDrawerToggle mDrawerToggle;
	private DrawerLayout mDrawerLayout;
	private Menu_Adapter ExpAdapter;
	private ArrayList<GroupItem> ExpListItems;
	ArrayList<String> group_keys_list = new ArrayList<>();
	ArrayList<String> reports_items_list = new ArrayList<>();
	private static final int REQUEST_ENABLE_BT = 2;
	public static final String ARG_SECTION_NUMBER = "section_number";
	private static final int REQUEST_PRINT = 1;
	private static final int REQUEST_GPS = 3;
	private static  int CONNECTIONSTATUS = 0;
	Account mAccount;
	AlertDialogManager alert;
	Uri customer ;

	AppPreference appPreference  ;
	GpsTracker gpsTracker;
	BluetoothAdapter mBtAdapter;
	int accounttype;
    CharSequence mTitle;
	Button start;
	ProgressBar progressBar;
	TextView txtsyncsta;
	private boolean firstrun;
	String extra = null;
    int userrole = 0;

	boolean tabletSize = false;
	ActionBar actionBar ;
    MenuItem registeritem ;
	boolean showusers = false, showdelive = false, showorders = false, lite = false, audit = false, station = false;
	boolean xpense = false, payment = false;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		appPreference = new AppPreference(getApplicationContext());
		String languageToLoad = appPreference.getDefaultLanguage();

		alert = new AlertDialogManager();
		Locale locale = new Locale(languageToLoad);
		Locale.setDefault(locale);
		Configuration config = new Configuration();
		config.locale = locale;

		getBaseContext().getResources().updateConfiguration(config,
				getBaseContext().getResources().getDisplayMetrics());

		tabletSize = getResources().getBoolean(R.bool.isTablet);
		if(!tabletSize){
			setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
		}

		setContentView(R.layout.expandable_menu);

		String apptype = Settings.getApptypeEnabled(this);

		TextView tv = (TextView)findViewById(R.id.version);
		String version = Constants.getVersionName(this);
        tv.setText(apptype+ " :  "+version);

		extra = Constants.SEARCHSOURCE_MAIN;

		actionBar = getSupportActionBar();
		actionBar.setBackgroundDrawable(getResources().getDrawable(R.drawable.ab_background_textured_prostockactionbar));

        gpsTracker = new GpsTracker(this);

		mAccount = Constants.createSyncAccount(this);
		mBtAdapter = BluetoothAdapter.getDefaultAdapter();

		Fragment fragment = new Home();
		showusers = Settings.getUserEnabled(this);
		showdelive = Settings.getDeliveryEnabled(this);
		showorders = Settings.getOrderEnabled(this);
		lite = Settings.getProshopLite(this);
		station = Settings.getFormEnabled(this);
		audit = Settings.getAuditEnabled(this);
        userrole = appPreference.getUserRole();
        xpense = Settings.getExEnabled(this);
        payment = Settings.getPaymentEnabled(this);

		FragmentManager fragmentManager = getSupportFragmentManager();
		Bundle bundle = new Bundle();
        bundle.putString(Constants.SEARCHSOURCE, Constants.SEARCHSOURCE_HOME);
		fragment.setArguments(bundle);
		fragmentManager.beginTransaction()
				.replace(R.id.content_frame, fragment)
				.commit();

		if(tabletSize){
			getSupportActionBar().setDisplayHomeAsUpEnabled(true);
			getSupportActionBar().setLogo(R.drawable.ic_drawer);
		}


        if(!tabletSize)
		getSupportActionBar().setDisplayHomeAsUpEnabled(true);

		getSupportActionBar().setBackgroundDrawable(getResources().
				getDrawable(R.drawable.ab_background_textured_prostockactionbar));

        accounttype = appPreference.get_accounttype();
		if(accounttype == 1){

			int tracktime = Settings.getTrackTime(this);
			if(tracktime > 0) {
                scheduleAlarm();
            }

            String time = appPreference.getDefaultSync();
            if(!Validating.areSet(time))
            {
                time = getResources().getStringArray(R.array.pref_sync_frequency_values)[0];
            }
            int t = Integer.parseInt(time);

            periodicadapersync(t);
        }
        if(!appPreference.getBluetoothoption()) {
            String address = appPreference.getAddress();
            if (mBtAdapter != null) {
                if (!mBtAdapter.isEnabled()) {
                    getAddress();
                } else {
                    if (address.isEmpty())
                        getAddress();
                }
            } else {
                getAddress();
                Log.v("bluetoothaddress", " address is " + address);
            }
        }

		initDrawer();

		if(Customers.getCustomerCount(this) == 0){
			Customers.createDefault(this);
		}

		String[] vals = {"1","DEFAULT_WH","SAMPLE WAREHOUSE","NO ADDRESS",Constants.WH_DEMO};
        if(Warehouse.getCount(this) == 0)
	    	Warehouse.storeWh(this, vals);
        Constants.createsuper(this);
        if(!Product_Categories.isCatPresent(this,"GENERAL",Constants.LOCAL))
            Product_Categories.createDefault(this);

	}

    private void check_firstrun() {
        firstrun = appPreference.check_firstrun();
        if (firstrun) {
            validate(Constants.TAGREGISTRATION);
            appPreference.set_firstrun();
            appPreference.save_last_sync(Constants.getDateOnly2());
        } else {
            String auth = appPreference.getAuthkey();
            String authtyping = null;
            if(!auth.isEmpty()){

                String im = Constants.getIMEINO(this);
                im = im.concat("|");
                int acctype = appPreference.get_accounttype();
                if(acctype == 0)
                    authtyping = Constants.ACCOUNT_TYPE_NORMAL;
                if(acctype == 1)
                    authtyping = Constants.ACCOUNT_TYPE_PREMIUM;

                MCrypt mCrypt = new MCrypt();
                try {
                    String crypted = new String(mCrypt.decrypt(auth));
                    if(im.concat(authtyping).equals(crypted)){
                        if(acctype == 1){
                            String time = appPreference.getDefaultSync();
							if(!Validating.areSet(time))
							{
								time = getResources().getStringArray(R.array.pref_sync_frequency_values)[0];
							}
                            int t = Integer.parseInt(time);
                            if ( Settings.getSettingsCount(this) < 1) {
                                Toast.makeText(this, getString(R.string.strsyncsstartstatus), Toast
                                        .LENGTH_SHORT).show();
                                ondemandsync();
                            } else {
                                periodicadapersync(t);
                            }
                        } else {
                            Toast.makeText(this,
                                    getString(R.string.normalaccountmsg),
                                    Toast.LENGTH_SHORT).show();
                        }
                    } else {
                        validate("validate");
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            } else {
                validate(Constants.TAGREGISTRATION);
            }
        }
    }

    public void getAddress(){
        Intent selectDevice = new Intent(this, UniversalBluetoothInstance.class);
        startActivityForResult(selectDevice, REQUEST_PRINT);
	}

	private void initDrawer() {
		mDrawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout);

		drawerList = (ExpandableListView) findViewById(R.id.left_drawer);


		mDrawerPane = (RelativeLayout) findViewById(R.id.drawerPane);

		mDrawerToggle = new ActionBarDrawerToggle(this, mDrawerLayout, R.string.navigation_drawer_open, R.string
				.navigation_drawer_close) {
			@Override
			public void onDrawerOpened(View drawerView) {
				super.onDrawerOpened(drawerView);

				invalidateOptionsMenu();
			}

			@Override
			public void onDrawerClosed(View drawerView) {
				super.onDrawerClosed(drawerView);
				invalidateOptionsMenu();
			}
		};

        mTitle = getTitle();

        if(!tabletSize)
		mDrawerLayout.setDrawerListener(mDrawerToggle);

		ExpListItems = SetStandardGroups();
		ExpAdapter = new Menu_Adapter(this, ExpListItems);
		drawerList.setAdapter(ExpAdapter);
		drawerList.setOnChildClickListener(this);
		drawerList.setOnGroupClickListener(this);
	}

    public void wh(){
        ContentResolver cr = getContentResolver();
        ContentValues cv = new ContentValues();
        cv.put(SalesItems.WH, Warehouse.getDefaultId(this));
        cr.update(SalesItems.BASEURI, cv, null, null);
    }

	@Override
	protected void onPostCreate(Bundle savedInstanceState) {
		super.onPostCreate(savedInstanceState);
		mDrawerToggle.syncState();
	}

	public ArrayList<GroupItem> SetStandardGroups() {
        String apptype = Settings.getApptypeEnabled(this);

        group_keys_list.add(getString(R.string.title_section1));
        if(station){
            group_keys_list.add(getString(R.string.title_section12));
            group_keys_list.add(getString(R.string.title_section5));
            group_keys_list.add(getString(R.string.title_section6));
            group_keys_list.add(getString(R.string.title_section7));
        } else if(lite){
            group_keys_list.add(getString(R.string.title_section2));
            group_keys_list.add(getString(R.string.title_section6));
            group_keys_list.add(getString(R.string.title_section7));
        } else {
            group_keys_list.add(getString(R.string.title_section2));
            group_keys_list.add(getString(R.string.title_section15));
            if (apptype.equals(Constants.APPTYPE_SALE) && showdelive) {
                group_keys_list.add(getString(R.string.title_section10));
            }
            if (apptype.equals(Constants.APPTYPE_SALE) && showorders) {
                group_keys_list.add(getString(R.string.title_section11));
            }
            if (apptype.equals(Constants.APPTYPE_SALE) && audit) {
                group_keys_list.add(getString(R.string.title_section13));
            }
            if(apptype.equals(Constants.APPTYPE_SALE) && xpense)
                group_keys_list.add(getString(R.string.title_section14));

            if (!lite) {
                group_keys_list.add(getString(R.string.title_section3));
                group_keys_list.add(getString(R.string.title_section4));
            }


            if (showusers)
                group_keys_list.add(getString(R.string.title_section5));

            group_keys_list.add(getString(R.string.title_section6));
            group_keys_list.add(getString(R.string.title_section7));
        }

        if(apptype.equals(Constants.APPTYPE_SALE)) {
            if(station){
                reports_items_list.add(getString(R.string.reporttitle11));
            } else {
                if (showusers) {
                    if (userrole == 1) {
                        reports_items_list.add(getString(R.string.reporttitle6));
                        reports_items_list.add(getString(R.string.reporttitle3));
                        reports_items_list.add(getString(R.string.reporttitle7));
                        reports_items_list.add(getString(R.string.reporttitle4));
                        reports_items_list.add(getString(R.string.reporttitle9));
                        reports_items_list.add(getString(R.string.reporttitle10));
                    } else {
                        reports_items_list.add(getString(R.string.reporttitle6));
                        reports_items_list.add(getString(R.string.reporttitle4));
                    }
                } else {
                    reports_items_list.add(getString(R.string.reporttitle6));
                    reports_items_list.add(getString(R.string.reporttitle4));
                }
            }
        } else {
            if(!lite){
                reports_items_list.add(getString(R.string.reporttitle6));
                reports_items_list.add(getString(R.string.reporttitle1));
                reports_items_list.add(getString(R.string.reporttitle2));
                reports_items_list.add(getString(R.string.reporttitle8));
                reports_items_list.add(getString(R.string.reporttitle5));
                reports_items_list.add(getString(R.string.reporttitle4));
            } else {
                reports_items_list.add(getString(R.string.reporttitle6));
                reports_items_list.add(getString(R.string.reporttitle1));
                reports_items_list.add(getString(R.string.reporttitle2));
                reports_items_list.add(getString(R.string.reporttitle5));
                reports_items_list.add(getString(R.string.reporttitle4));
            }
        }

        ArrayList<GroupItem> list = new ArrayList<GroupItem>();

        ArrayList<ChildItem> ch_list;

        int size = reports_items_list.size();
        int j = 0;

        for (int i = 0; i < group_keys_list.size(); i++) {
            GroupItem gru = new GroupItem();
            gru.setName(group_keys_list.get(i));
            ch_list = new ArrayList<ChildItem>();
                    if (i == group_keys_list.size() - 2) {
                        for (; j < size; j++) {
                            ChildItem ch = new ChildItem();
                            ch.setName("   " + reports_items_list.get(j));
                            ch_list.add(ch);
                        }
                        gru.setItems(ch_list);
                    }
            list.add(gru);
        }
        return list;
    }
	public boolean onPrepareOptionsMenu(Menu menu) {
		// If the nav drawer is open, hide action items related to the content view
//        boolean drawerOpen = mDrawerLayout.isDrawerOpen(mDrawerList);
//        menu.findItem(R.id.action_settings).setVisible(!drawerOpen);
		return super.onPrepareOptionsMenu(menu);
	}

	@Override
	public boolean onChildClick(ExpandableListView parent, View v, int groupPosition, int childPosition, long id)
	{
		selectChild(childPosition);
		return true;
	}

	private void selectChild(int position) {
		Fragment fragment = new SalesByCustomer();
        String apptype = Settings.getApptypeEnabled(this);
		FragmentManager fragmentManager = getSupportFragmentManager();
		Bundle bundle = new Bundle();
		switch (position){
			case 1:
                    if (apptype.equals(Constants.APPTYPE_SALE)) {
                        if (showdelive && showusers && userrole == 1 )
                            bundle.putString(Constants.REPORTS, Constants.DELIVERYHISTORY);
                        else
                            bundle.putString(Constants.REPORTS, Constants.PRODUCTREORDER);
                    } else {
                        bundle.putString(Constants.REPORTS, Constants.SALESBYCUSTOMER);
                    }

				break;
			case 2:
                if(apptype.equals(Constants.APPTYPE_SALE)) {
                    if (showdelive && showusers && userrole == 1)
                        bundle.putString(Constants.REPORTS, Constants.ORDERREPORT);
                } else
                    bundle.putString(Constants.REPORTS, Constants.SALESBYPRODUCT);
				break;
			case 3:
                if(station){

                } else {
                    if (apptype.equals(Constants.APPTYPE_SHOP)) {
                        if (!lite)
                            bundle.putString(Constants.REPORTS, Constants.PROFITREPORT);
                        else
                            bundle.putString(Constants.REPORTS, Constants.TAXBYPRODUCT);
                    } else
                        bundle.putString(Constants.REPORTS, Constants.TAXBYPRODUCT);
                }
				break;
			case 4:
                if(apptype.equals(Constants.APPTYPE_SHOP)) {
                    if(!lite)
                        bundle.putString(Constants.REPORTS, Constants.TAXBYPRODUCT);
                    else
                        bundle.putString(Constants.REPORTS, Constants.PRODUCTREORDER);
                }
                else
                    bundle.putString(Constants.REPORTS, Constants.PRODUCTREORDER);
				break;
			case 5:
                    bundle.putString(Constants.REPORTS, Constants.PRODUCTREORDER);
				break;
			case 0:
                if(station)
                    bundle.putString(Constants.REPORTS, Constants.STATIONHISTORY);
                else
                    bundle.putString(Constants.REPORTS, Constants.SALEREPORT);
				break;
		}
        fragment.setArguments(bundle);
        fragmentManager.beginTransaction()
                .replace(R.id.content_frame, fragment)
                .commit();
        drawerList.setItemChecked(position, true);
        setTitle(getString(R.string.strreportheader));
        if(!tabletSize)
            mDrawerLayout.closeDrawer(mDrawerPane);

	}

    private void showToast(String txt) {
        Toast.makeText(this, txt, Toast.LENGTH_LONG).show();
    }

    @Override
	public boolean onGroupClick(ExpandableListView parent, View v, int groupPosition, long id) {

        if(station || lite){
            if (groupPosition != group_keys_list.size() - 2) {
                selectItemFromDrawer(groupPosition);
            }
        } else {
            if (appPreference.getUserRole() == 0) {
                selectItemFromDrawer(groupPosition);
            } else {
                if (groupPosition != group_keys_list.size() - 2) {
                    selectItemFromDrawer(groupPosition);
                }
            }
        }
		return false;
	}

	private void selectItemFromDrawer(int position) {
		int newposition = position+1 ;
		FragmentManager fragmentManager = getSupportFragmentManager();
        Fragment fm = new Fragment();
		Bundle args = new Bundle() ;
        String apptype = Settings.getApptypeEnabled(this);
                switch (newposition) {
                    case 1:
                        extra = Constants.SEARCHSOURCE_HOME;
                        fm = new Home();
                        break;
                    case 2:
                        if(station) {/*
                            extra = Constants.SEARCHSOURCE_STATION;
                            fm = new StationFragment();*/

                            fm = null;
                            Intent pitent = new Intent(this,MainScreen.class);
                            startActivity(pitent);
                        }
                        else {
                            if ( lite) {
                                fm = null;
                                Intent saleintent = new Intent(this, MainScreen.class);
//                                Intent saleintent = new Intent(this, SalesLiteActivity.class);
                                startActivity(saleintent);
                            } else {
                                fm = null;
                                Intent saleintent = new Intent(this, MainScreen.class);
                                startActivity(saleintent);
                            }
                        }
                        break;
                    case 3:
                        if(station){
                                fm = new UserManagement();
                        } else {
                            fm = null;
                            Intent pitent = new Intent(this,MainScreen.class);
                            startActivity(pitent);
                        }
                        break;
                    case 4:
                            if (showdelive) {
                                extra = Constants.SEARCHSOURCE_PRODUCTS;
                                fm = new DeliveriesFragment();
                            } else if (showorders) {
                                fm = null;
                                Intent orderintent = new Intent(this, OrdersActivity.class);
                                startActivity(orderintent);
                            } else if (audit) {
                                fm = null;
                                Intent orderintent = new Intent(this, AuditInit.class);
                                startActivity(orderintent);
                            } else if (xpense) {
                                fm = new ExpenseFragment();
                            } else {
                                extra = Constants.SEARCHSOURCE_CUSTOMERS;
                                fm = new CustomersFragment();
                            }
                        break;
                    case 5:
                        if(station){
                            System.out.println(" report here ");
                        } else if (lite) {
                           fm = new UserAccount();
                         }else {
                            if (showdelive) {
                                if (showorders) {
                                    fm = null;
                                    Intent orderintent = new Intent(this, OrdersActivity.class);
                                    startActivity(orderintent);
                                } else  if(audit){
                                        /*fm = new AuditingFrag();*/
                                    fm = null;
                                    Intent orderintent = new Intent(this, AuditInit.class);
                                    startActivity(orderintent);
                                    } else if (xpense){
                                        fm = new ExpenseFragment();
                                    } else {
                                        extra = Constants.SEARCHSOURCE_CUSTOMERS;
                                        fm = new CustomersFragment();
                                    }
                            } else {//no delivery
                                if (showorders) {
                                    if (audit) {
                                        fm = null;
                                        Intent orderintent = new Intent(this, AuditInit.class);
                                        startActivity(orderintent);
                                    } else if (xpense)
                                        fm = new ExpenseFragment();
                                    else {
                                        extra = Constants.SEARCHSOURCE_CUSTOMERS;
                                        fm = new CustomersFragment();
                                    }
                                } else if (audit){
                                    if (xpense) {
                                        fm = new ExpenseFragment();
                                    } else {
                                        extra = Constants.SEARCHSOURCE_CUSTOMERS;
                                        fm = new CustomersFragment();
                                    }
                            } else {
                                    extra = Constants.SEARCHSOURCE_PRODUCTS;
                                    fm = new ProductsFragment();
                                }
                            }
                        }
                        break;
                    case 6:
                        if(station){
                            fm = new UserAccount();
                        } else if (!lite)
                        if(showdelive) {
                            if (showorders) {
                                if (audit) {
                                    fm = null;
                                    Intent orderintent = new Intent(this, AuditInit.class);
                                    startActivity(orderintent);
                                } else if (xpense) {
                                    fm = new ExpenseFragment();
                                } else {
                                    extra = Constants.SEARCHSOURCE_CUSTOMERS;
                                    fm = new CustomersFragment();
                                }
                            } else {
                                if (audit) {
                                    fm = new ExpenseFragment();
                                } else if (xpense) {
                                    extra = Constants.SEARCHSOURCE_CUSTOMERS;
                                    fm = new CustomersFragment();
                                } else {
                                    extra = Constants.SEARCHSOURCE_PRODUCTS;
                                    fm = new ProductsFragment();
                                }
                            }
                        } else {//no delivery
                            if (showorders) {//no orders as well
                                extra = Constants.SEARCHSOURCE_PRODUCTS;
                                fm = new ProductsFragment();
                            } else if (audit ){
                                extra = Constants.SEARCHSOURCE_PRODUCTS;
                                fm = new ProductsFragment();
                            } else if (showusers){
                                fm = new UserManagement();
                            }
                        }
                        break;
                    case 7:
                        if(!station || !lite)
                        if(showdelive) {
                            if (showorders) {
                                if (audit) {
                                    if (xpense)
                                        fm = new ExpenseFragment();
                                    else {
                                        extra = Constants.SEARCHSOURCE_CUSTOMERS;
                                        fm = new CustomersFragment();
                                    }
                                } else {
                                    if (xpense) {
                                        extra = Constants.SEARCHSOURCE_CUSTOMERS;
                                        fm = new CustomersFragment();
                                    } else {
                                        extra = Constants.SEARCHSOURCE_PRODUCTS;
                                        fm = new ProductsFragment();
                                    }
                                }
                            } else {
                                if (audit) {
                                    if (xpense) {
                                        extra = Constants.SEARCHSOURCE_CUSTOMERS;
                                        fm = new CustomersFragment();
                                    } else {
                                        extra = Constants.SEARCHSOURCE_PRODUCTS;
                                        fm = new ProductsFragment();
                                    }
                                } else {
                                    if (xpense) {
                                        extra = Constants.SEARCHSOURCE_CUSTOMERS;
                                        fm = new CustomersFragment();
                                    } else {
                                        extra = Constants.SEARCHSOURCE_PRODUCTS;
                                        fm = new ProductsFragment();
                                    }
                                }
                            }
                        } else {//no delivery
                            if (showorders) {//no orders as well
                                    extra = Constants.SEARCHSOURCE_PRODUCTS;
                                    fm = new ProductsFragment();
                            } else if (audit){
                                if(xpense)
                                {
                                    extra = Constants.SEARCHSOURCE_PRODUCTS;
                                    fm = new ProductsFragment();
                                } else {
                                    extra = Constants.SEARCHSOURCE_PRODUCTS;
                                    fm = new ProductsFragment();
                                }
                            } else if (showusers){
                                fm = new UserManagement();
                            } else
                                fm = new UserAccount();
                        }
                        break;
                    case 8:
                        if(!station || !lite)
                        if(showdelive){
                            if(showorders) {
                                if (audit) {
                                    if (xpense) {
                                        extra = Constants.SEARCHSOURCE_CUSTOMERS;
                                        fm = new CustomersFragment();
                                    } else {
                                        extra = Constants.SEARCHSOURCE_PRODUCTS;
                                        fm = new ProductsFragment();
                                    }
                                } else if (xpense) {
                                    extra = Constants.SEARCHSOURCE_PRODUCTS;
                                    fm = new ProductsFragment();
                                } else {
                                    if(showusers)
                                        fm = new UserManagement();
                                    else
                                        fm = new UserAccount();
                                }
                            } else {
                                if (audit) {
                                    if (xpense) {
                                        extra = Constants.SEARCHSOURCE_CUSTOMERS;
                                        fm = new CustomersFragment();
                                    } else {
                                        extra = Constants.SEARCHSOURCE_PRODUCTS;
                                        fm = new ProductsFragment();
                                    }
                                } else if (xpense) {
                                    extra = Constants.SEARCHSOURCE_PRODUCTS;
                                    fm = new ProductsFragment();
                                } else {
                                    if(showusers)
                                        fm = new UserManagement();
                                    else
                                        fm = new UserAccount();
                                }
                            }
                        } else {
                            fm = new UserAccount();
                        }
                        break;
                    case 9:
                        if(!station || !lite)
                        if(showdelive) {
                            if (showorders) {
                                if (audit) {
                                    if (xpense) {
                                        extra = Constants.SEARCHSOURCE_PRODUCTS;
                                        fm = new ProductsFragment();
                                    } else {
                                        if (showusers)
                                            fm = new UserManagement();
                                    }
                                } else if (xpense) {
                                    if (showusers)
                                        fm = new UserManagement();
                                } else
                                    fm = new UserAccount();
                            } else {
                                if (showusers)
                                    fm = new UserManagement();
                                else
                                    fm = new UserAccount();
                            }
                        }  else {
                            if (showusers)
                                fm = new UserManagement();
                            else
                                fm = new UserAccount();
                        }
                        break;
                    case 10:
                        if(!station || !lite)
                        if(showdelive){
                            if(showorders)
                                if(audit)
                                    if (xpense) {
                                        if (showusers)
                                            fm = new UserManagement();
                                    } else {
                                        fm = new UserAccount();
                                    }
                        }
                        break;
                    case 11:
                        if(!station || !lite)
                        if(showdelive){
                            if(showorders)
                                if(audit)
                                    if (xpense)
                                        if(!showusers)
                                            fm = new UserAccount();
                        }
                        break;
                    case 12:
                        if(!station || !lite)
                        if(showdelive){
                            if(showorders)
                                if(audit)
                                    if (xpense)
                                        if(showusers)
                                            fm = new UserAccount();
                        }
                        break;
                }

        args.putInt(ARG_SECTION_NUMBER, newposition);
        if(fm != null){
            fm.setArguments(args);
            fragmentManager.beginTransaction().
                    replace(R.id.content_frame, fm).
                    commit();
        }
            drawerList.setItemChecked(position, true);

            setTitle(group_keys_list.get(position));
            if (!tabletSize)
                mDrawerLayout.closeDrawer(mDrawerPane);

	}
	/*
     * running the sync adapter periodically
     */
	public void periodicadapersync(int time)
	{
        ContentResolver.addPeriodicSync(
                mAccount,
                ContentProviderApi.AUTHORITY,
                Bundle.EMPTY, time) ;
	}
	/*
     * running the sync adapter on demand
     */
	public void ondemandsync()
	{
		Bundle settingsBundle = new Bundle() ;
		settingsBundle.putBoolean(ContentResolver.SYNC_EXTRAS_MANUAL, true);
		settingsBundle.putBoolean(ContentResolver.SYNC_EXTRAS_EXPEDITED, true);
        ContentResolver.requestSync(mAccount, ContentProviderApi.AUTHORITY, settingsBundle);
	}

	@Override
	protected void onStart() {
		super.onStart();
		setnavdrawer();
        turnGpsOn() ;
	}

	public void turnGpsOn(){
        if(accounttype == 1)
            if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
                System.out.println(" gps "+gpsTracker.isgpson());
                if(!gpsTracker.isgpson()) {
                    Toast.makeText(this,getString(R.string.strgps),Toast.LENGTH_LONG).show();
                    Intent inte = new Intent(android.provider.Settings.ACTION_LOCATION_SOURCE_SETTINGS);
                    startActivity(inte);
                }
            } else {
                Intent intent = new Intent("android.location.GPS_ENABLED_CHANGE");
                intent.putExtra("enabled", true);
                sendBroadcast(intent);
            }
	}


	@Override
	public void onResume() {
        super.onResume();
		setnavdrawer();
        turnGpsOn();
	}

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if(gpsTracker.isgpson())
            gpsTracker.stopUsingGPS();
        turnGpsOf();
    }

    @Override
	protected void onPause() {
		super.onPause();
        if(gpsTracker.isgpson())
			gpsTracker.stopUsingGPS();
        turnGpsOf();
	}
    public void turnGpsOf(){
        if(Build.VERSION.SDK_INT < Build.VERSION_CODES.KITKAT) {
            Intent intent = new Intent("android.location.GPS_ENABLED_CHANGE");
            intent.putExtra("enabled", false);
            sendBroadcast(intent);
        }
    }

	@Override
	protected void onStop() {
		super.onStop();
        if(gpsTracker.isgpson())
			gpsTracker.stopUsingGPS();
        turnGpsOf();
	}


	public void scheduleAlarm() {
		Intent intent = new Intent(getApplicationContext(), LocationTrigger.class);
		final PendingIntent pIntent = PendingIntent.getBroadcast(this, LocationTrigger.REQUEST_CODE,
				intent, PendingIntent.FLAG_UPDATE_CURRENT);
		long firstMillis = System.currentTimeMillis();
		long intervalMillis = 0;
		int minutesintervar = Settings.getTrackTime(this);
		if(minutesintervar == 0){
			intervalMillis = Integer.parseInt(Constants.DEFAULTTRACKINTERVAL);
		} else {
			String interval = String.valueOf(minutesintervar);
			long ii = Long.valueOf(interval);
			intervalMillis = TimeUnit.MINUTES.toMillis(ii);
		}
		if(intervalMillis != 0) {
			AlarmManager alarm = (AlarmManager) this.getSystemService(Context.ALARM_SERVICE);
            alarm.setInexactRepeating(AlarmManager.RTC_WAKEUP, firstMillis, intervalMillis, pIntent);
		}
	}
	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		super.onActivityResult(requestCode, resultCode, data);
		switch (requestCode) {
			case REQUEST_PRINT: {
				if (resultCode == RESULT_OK) {
					Toast.makeText(this, "Device connectted address "+appPreference.getAddress(),Toast.LENGTH_LONG).show();
				} else {
					Toast.makeText(this, "Device not connectted",Toast.LENGTH_LONG).show();
				}
				break;
			}
			case REQUEST_ENABLE_BT: {
				if (!mBtAdapter.isEnabled()) {
					Intent enableBtIntent = new Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE);
					startActivityForResult(enableBtIntent, REQUEST_ENABLE_BT);
				}
				break;
			}
		}
	}

	private void validate(String source){
		LayoutInflater infl = getLayoutInflater();
		View view = infl.inflate(R.layout.startscreen, null) ;

		final Dialog dialog = new Dialog(this, R.style.CustomDialog	);
		dialog.setCancelable(false);
		dialog.setContentView(view);

		progressBar = (ProgressBar)dialog.findViewById(R.id.progressbar);
		txtsyncsta = (TextView)dialog.findViewById(R.id.txtbody);
		start = (Button)dialog.findViewById(R.id.btnstart);
		progressBar.setVisibility(View.VISIBLE);
		txtsyncsta.setVisibility(View.VISIBLE);

		boolean network = Constants.isNotConnected(this);
		if(!network) {
            Toast.makeText(this, getString(R.string.strinternetwarn), Toast.LENGTH_LONG).show();
            return;
        }
		new DeviceRegistration(dialog.getContext(),progressBar,txtsyncsta,start).execute(source);
		start.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				String auth = appPreference.getAuthkey();
				String authtyping = null;
				if (!auth.isEmpty()) {

					String im = Constants.getIMEINO(dialog.getContext());
					im = im.concat("|");
					int acctype = appPreference.get_accounttype();
					if (acctype == 0)
						authtyping = Constants.ACCOUNT_TYPE_NORMAL;
					if (acctype == 1)
						authtyping = Constants.ACCOUNT_TYPE_PREMIUM;

					MCrypt mCrypt = new MCrypt();
					try {
						String crypted = new String(mCrypt.decrypt(auth));
						if (im.concat(authtyping).equals(crypted)) {
                            registeritem.setVisible(false);
							dialog.dismiss();
						} else {
							dialog.dismiss();
                            appPreference.clear_authentications();
							invalidaccount();
						}
					} catch (Exception e) {
						e.printStackTrace();
					}
				} else {
					invalidaccount();
                    appPreference.clear_authentications();
					dialog.dismiss();
				}
			}
		});
		dialog.show();
	}

	private void invalidaccount(){
		LayoutInflater infl = getLayoutInflater();
		View view = infl.inflate(R.layout.startscreen, null) ;

		final Dialog dialog = new Dialog(this, R.style.CustomDialog	);
		dialog.setCancelable(false);
		dialog.setContentView(view);

		progressBar = (ProgressBar)dialog.findViewById(R.id.progressbar);
		TextView txthead = (TextView)dialog.findViewById(R.id.txtheader);
		start = (Button)dialog.findViewById(R.id.btnstart);
		txthead.setText(getString(R.string.strtokenerror));
		txthead.append("\n\n");
		txthead.append(getString(R.string.strtokenerrormsg));

		Runnable runnable = new Runnable() {
			@Override
			public void run() {
				appPreference.clear_authentications();
				dialog.dismiss();
				System.exit(0);
			}
		};
		Handler handler = new Handler();
        handler.postDelayed(runnable, 15000);

		start.setVisibility(View.VISIBLE);
        start.setText(getString(R.string.strclose));
		start.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				appPreference.clear_authentications();
				dialog.dismiss();
				System.exit(0);
			}
		});
		dialog.show();
	}
	@Override
	public void startActivity(Intent intent) {
		if (Intent.ACTION_SEARCH.equals(intent.getAction())) {
			intent.putExtra(Constants.SEARCHSOURCE, extra);
		}

		super.startActivity(intent);
	}

	private void setnavdrawer(){
		if(tabletSize){
			mDrawerLayout.setDrawerLockMode(DrawerLayout.LOCK_MODE_LOCKED_OPEN);
            mDrawerLayout.setScrimColor(Color.TRANSPARENT);
		}
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
        if(!mDrawerLayout.isDrawerOpen(mDrawerPane) || tabletSize) {
            getMenuInflater().inflate(R.menu.dashboard, menu);
            String address = appPreference.getAddress();
            if (address.isEmpty()) {
                menu.findItem(R.id.action_pair).setTitle(getString(R.string.strscanpair));
                CONNECTIONSTATUS = 1;
            } else {
                menu.findItem(R.id.action_pair).setTitle(getString(R.string.strclearpair));
                CONNECTIONSTATUS = 2;
            }
            String auth = appPreference.getAuthkey();
			if(auth.isEmpty()){
                registeritem = menu.findItem(R.id.action_register);
                registeritem.setVisible(true);
                if(extra.equals(Constants.SEARCHSOURCE_MAIN) || extra.equals(Constants.SEARCHSOURCE_HOME)){
                    registeritem.setShowAsAction(MenuItem.SHOW_AS_ACTION_ALWAYS);
                }
            }
            return true;
        }
		return super.onCreateOptionsMenu(menu);
	}

    public void restoreActionBar() {

        actionBar.setNavigationMode(ActionBar.NAVIGATION_MODE_STANDARD);
        actionBar.setDisplayShowTitleEnabled(true);
        actionBar.setTitle(Html.fromHtml("<font color=#fff>"+mTitle+"</font>"));
    }

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		int id = item.getItemId();
		switch(id){
			case R.id.action_settings:
				Intent intent = new Intent(this, SettingsActivity.class);
				intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
				startActivity(intent);
                finish();
				return true;
			case R.id.action_syncnow:
				ondemandsync();
				break;
			case R.id.action_pair:
				if(CONNECTIONSTATUS == 2){
					appPreference.clearAddress();
					Log.v("ADDRESS"," address cleared now it is "+appPreference.getAddress());
				} else if (CONNECTIONSTATUS == 1) {
					getAddress();
				}
				break;
			case R.id.action_register:
                check_firstrun();
				break;
		}
        if(!tabletSize)
		if (mDrawerToggle.onOptionsItemSelected(item)) {
			return true;
		}

		return super.onOptionsItemSelected(item);
	}

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        mDrawerToggle.onConfigurationChanged(newConfig);
    }
}