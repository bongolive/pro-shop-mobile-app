/*
 * The source codes in this project are owned by Bongolive Enterprises. Copying or any kind of usage without Bongolive's approval is against the law.
 * Copyright (c) 2015.
 *
 */

package bongolive.apps.proshop.view;

import android.content.Context;
import android.content.ContextWrapper;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Path;
import android.graphics.RectF;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.ActionBar;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.text.InputFilter;
import android.util.AttributeSet;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import java.io.File;
import java.io.FileOutputStream;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.ArrayList;

import bongolive.apps.proshop.R;
import bongolive.apps.proshop.controller.AppPreference;
import bongolive.apps.proshop.controller.Constants;
import bongolive.apps.proshop.controller.PaymentsDetailsAdapter;
import bongolive.apps.proshop.controller.Validating;
import bongolive.apps.proshop.model.MultimediaContents;
import bongolive.apps.proshop.model.Payments;
import bongolive.apps.proshop.model.Sales;
import bongolive.apps.proshop.model.Transfer;


public class PaymentDetailFrag extends Fragment
{
    View _rootView;
    private RecyclerView mRecyclerView;
    private RecyclerView.Adapter mAdapter;
    private RecyclerView.LayoutManager mLayoutManager;
    private ActionBar actionBar;
    private EditText etfilter;
    private ArrayList arrayList;
    private RelativeLayout rl;
    private ImageView imgdone,imgnext;
    private AutoCompleteTextView bzn;
    ArrayList<Payments> mItems;
    FloatingActionButton actionButton;
    String orderid;
    long pyid = 0;
    String title;
    Toolbar ab;
    double grandbalance,grandpaid;
    /*
    * end of new strings
    */
    MenuItem menuItem;


    public PaymentDetailFrag(){

    }
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
    {
        _rootView = inflater.inflate(R.layout.selling_screen, container, false);
        return _rootView ;
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState)
    {
        super.onActivityCreated(savedInstanceState);
        setHasOptionsMenu(true);

        ab = ((MainScreen)this.getActivity()).getmToolbar();

        pyid = getArguments().getLong(Payments.ID);
        title = getArguments().getString(Sales.CUSTOMERID);

        if(Validating.areSet(title))
            ab.setTitle(title.toUpperCase());
        else
            ab.setTitle(getString(R.string.strpayment));

        actionButton = (FloatingActionButton)getActivity().findViewById(R.id.actionbutton);
        actionButton.setVisibility(View.GONE);
//        actionButton.setBackgroundColor(getResources().getColor(R.color.actionbar));
//        actionButton.setImageResource(R.drawable.ic_pay);

        mRecyclerView = (RecyclerView) getActivity().findViewById(R.id.prodct_list);
        mRecyclerView.setHasFixedSize(true);
        mRecyclerView.setItemAnimator(new DefaultItemAnimator());

        mLayoutManager = new LinearLayoutManager(getContext());
        mRecyclerView.setLayoutManager(mLayoutManager);
        mItems = Payments.getPaymentSummary(getContext(), pyid);
        Payments p = mItems.get(0);

        orderid = p.getLocalsale();
        grandbalance = Sales.getDebt(getContext(), orderid);
        System.out.println(" grand blance "+grandbalance);

        mAdapter = new PaymentsDetailsAdapter(getActivity(), mItems);
        mRecyclerView.setAdapter(mAdapter);

        actionButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

            }
        });
    }
    
    @Override
    public void onResume() {
        super.onResume();
        if(pyid != 0 && mRecyclerView != null && mAdapter != null){
            mAdapter.notifyDataSetChanged();
        }
        if(orderid != null){
            grandbalance = Sales.getDebt(getContext(), orderid);

            if(grandbalance > 0){
                if(menuItem != null)
                    menuItem.setVisible(true);
            } else
            if(menuItem != null)
                menuItem.setVisible(false);
        }
    }


    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        getActivity().getMenuInflater().inflate(R.menu.audit, menu);
        menuItem = menu.findItem(R.id.action_tick);
        menuItem.setIcon(R.drawable.ic_action_send);

        MenuItem syn = menu.findItem(R.id.action_syncnow);
        MenuItem pair = menu.findItem(R.id.action_pair);
        MenuItem sett = menu.findItem(R.id.action_settings);

        grandbalance = Sales.getDebt(getContext(), orderid);
        if(grandbalance > 0){
            menuItem.setVisible(true);
            syn.setVisible(false);
            pair.setVisible(false);
            sett.setVisible(false);

        } else {
            menuItem.setVisible(false);
            syn.setVisible(true);
            pair.setVisible(false);
            sett.setVisible(false);
        }

        super.onCreateOptionsMenu(menu,inflater);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_tick:
                makePaymentMethod(pyid,title);
        }
        return super.onOptionsItemSelected(item);
    }


    public static class MakePayments extends Fragment implements View.OnClickListener{
        Toolbar tbar;
        AppPreference appPreference;
        ArrayList<Payments> mItems;
        long pyid;
        String title;
        TextView txtsaleref,txtpayref,txtdebt,txtinfo;
        public Button txtsign;
        EditText etpay,etpayee;
        Spinner etpayer;
        public ImageView imgsign;
        String timestamp,saleid;
        String payer;
        String balance,orderid;
        double grandbalance,grandpaid;
        String[] methods = null;
        ArrayAdapter adapter = null;

        public String signaturepath = null;



        public MakePayments(){

        }
        @Override
        public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
        {
            View view = inflater.inflate(R.layout.make_payment, container, false);
            return view ;
        }

        @Override
        public void onActivityCreated(Bundle savedInstanceState)
        {
            super.onActivityCreated(savedInstanceState);
            appPreference = new AppPreference(getContext());

            tbar = ((MainScreen)this.getActivity()).getmToolbar();
//            tbar.setNavigationIcon(R.drawable.ic_arrow_back_white_36dp);
            tbar.setNavigationOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    SalesFrag newFragment = new SalesFrag();
                    FragmentTransaction transaction = getFragmentManager().beginTransaction();
                    transaction.replace(R.id.content_frame, newFragment);
                    tbar.setNavigationIcon(null);
                    transaction.commit();
                }
            });
            setHasOptionsMenu(true);


            methods = getResources().getStringArray(R.array.paymethod_array);

            pyid = getArguments().getLong(Payments.ID);
            title = getArguments().getString(Sales.CUSTOMERID);
            String img = getArguments().getString(Payments.ATTACHEMENT);

            tbar.setTitle(getString(R.string.straddpayment));

            txtinfo = (TextView)getActivity().findViewById(R.id.txtstinfo);
            txtsaleref = (TextView)getActivity().findViewById(R.id.txtsalerefv);
            txtpayref = (TextView)getActivity().findViewById(R.id.txtpayrefv);
            txtsign = (Button)getActivity().findViewById(R.id.txtaddsign);
            txtdebt = (TextView)getActivity().findViewById(R.id.txtdebtv);

            etpay = (EditText)getActivity().findViewById(R.id.ettopay);
            etpayee = (EditText)getActivity().findViewById(R.id.etpayee);
            etpayer = (Spinner)getActivity().findViewById(R.id.etpayedby);

            imgsign = (ImageView)getActivity().findViewById(R.id.imgsign);

            txtsign.setOnClickListener(this);

            adapter = new ArrayAdapter<>(getContext(), android.R.layout.simple_dropdown_item_1line, methods);
            etpayer.setAdapter(adapter);
            etpayer.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                @Override
                public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                    if(position > 0){
                        payer = (String) adapter.getItem(position);
                    }
                }

                @Override
                public void onNothingSelected(AdapterView<?> parent) {

                }
            });
            String payref = Constants.generatePayref();
            txtpayref.setText(payref);

            mItems = Payments.getPaymentSummary(getContext(), pyid);
            Payments p = mItems.get(0);
            balance = p.getPostbalance();
            orderid = p.getLocalsale();
            saleid = p.getSyssale();
            grandbalance = Sales.getDebt(getContext(), orderid);
            grandpaid = Sales.getPaid(getContext(), orderid);
            txtdebt.setText(String.valueOf(grandbalance));
            int l = String.valueOf(grandbalance).length();
            InputFilter[] f = new InputFilter[1];
            f[0] = new InputFilter.LengthFilter(l-2);
            etpay.setFilters(f);

            String ls = p.getLocalsale();
            timestamp = Constants.generatePayref();
            txtsaleref.setText(timestamp);
            txtinfo.setText(title);
            etpayee.setText(p.getRecby());

            if(img != null){
                signaturepath = img;
                txtsign.setVisibility(View.GONE);
                imgsign.setVisibility(View.VISIBLE);
                Bitmap mbitmap = Constants.decodeImg(signaturepath, 400, 300);
                imgsign.setImageBitmap(mbitmap);
            }
        }

        @Override
        public void onClick(View v) {
            switch (v.getId()){
                case R.id.txtaddsign:
                    signature(pyid,title);
                    break;
            }
        }

        @Override
        public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
            getActivity().getMenuInflater().inflate(R.menu.audit, menu);

            MenuItem menuItem = menu.findItem(R.id.action_tick);
            menuItem.setIcon(R.drawable.ic_action_send);

            MenuItem syn = menu.findItem(R.id.action_syncnow);
            MenuItem pair = menu.findItem(R.id.action_pair);
            MenuItem sett = menu.findItem(R.id.action_settings);
            syn.setVisible(false);
            pair.setVisible(false);
            sett.setVisible(false);

            super.onCreateOptionsMenu(menu,inflater);
        }


        @Override
        public boolean onOptionsItemSelected(MenuItem item) {
            switch (item.getItemId()) {
                case R.id.action_tick:
                    process_item();
                    break;
            }
            return super.onOptionsItemSelected(item);
        }
        private void process_item(){
            String pay = etpay.getText().toString().trim();
            String payee =etpayee.getText().toString().trim();

            String[] vals = {pay,payee,payer};

            if(Validating.areSet(vals)) {
                double paid = new BigDecimal(pay).setScale(2, RoundingMode.HALF_UP).doubleValue();
                if(paid <= grandbalance && paid > 0) {
                    double pbd = grandbalance-paid;
                    double gt = paid+grandpaid;
//                Log.e("paid","paid "+paid+" debt "+grandbalance+" pbc "+pbd+" grandblance "+gt);
                    String postb = String.valueOf(pbd);

                    String[] va = {"0",postb,"none",Constants.getDate(),pay,orderid
                            , saleid, payer, signaturepath, Constants.PAYTYPE_RECEIVED,"0",timestamp,payee,"1"};
                    if(Validating.areSet(va)) {
                        String[] mult = {signaturepath, title + "_" + System.currentTimeMillis(),
                                MultimediaContents.OPERATIONTYPEPY, timestamp};
                        boolean store = Payments.insert(getContext(), va);
                        if(store) {
                            Sales.updatePaymentStatus(getContext(),gt,orderid);
                            if (!MultimediaContents.isMediaStored(getContext(), timestamp)) {
                                boolean stored = MultimediaContents.storeMultimedia(getContext(), mult);
                                if (stored) {
                                    System.out.println("stored ? " + stored);

                                    getFragmentManager().popBackStack(null, FragmentManager.POP_BACK_STACK_INCLUSIVE);
                                    PaymentDetailFrag newFragment = new PaymentDetailFrag();
                                    Bundle args = new Bundle();
                                    args.putLong(Transfer.ID, pyid);
                                    args.putString(Sales.CUSTOMERID, title);
                                    newFragment.setArguments(args);
                                    FragmentTransaction transaction = getFragmentManager().beginTransaction();
                                    transaction.replace(R.id.content_frame, newFragment);
                                    transaction.addToBackStack(null);
                                    transaction.commit();
                                }
                            }
                        }
                    }
                } else {
                    Toast.makeText(getContext(), getString(R.string.strdebtwarn), Toast.LENGTH_LONG).show();
                }
            }
        }


        public void signature(long id, String customer) {
            SignHere newFragment = new SignHere();
            Bundle args = new Bundle();
            args.putLong(Transfer.ID, id);
            args.putString(Sales.CUSTOMERID, customer);
            newFragment.setArguments(args);
            FragmentTransaction transaction = getFragmentManager().beginTransaction();
            transaction.replace(R.id.content_frame, newFragment);
            transaction.addToBackStack(null);
            transaction.commit();
        }
    }
    public static class SignHere extends Fragment{
        Toolbar tbar;

        LinearLayout mContent;
        SignHere.signature mSignature;
        Button mClear, mGetSign, mCancel;
        CheckBox chkconsent;
        public String tempDir;
        public int count = 1;
        public String current = null;
        private Bitmap mBitmap;
        View mView;
        File mypath;
        private String uniqueId;

    /*
    * end of new strings
    */

        public SignHere(){

        }
        @Override
        public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
        {
            View view = inflater.inflate(R.layout.signature, container, false);
            return view ;
        }

        @Override
        public void onActivityCreated(Bundle savedInstanceState)
        {
            super.onActivityCreated(savedInstanceState);

            setHasOptionsMenu(false);
            tbar = ((MainScreen)this.getActivity()).getmToolbar();
            tbar.setNavigationIcon(null);

            tempDir = Environment.getExternalStorageDirectory()+"/"+Constants.MASTER_DIR+"/.temp";
            ContextWrapper cw = new ContextWrapper(getContext().getApplicationContext());
            File directory = cw.getDir("tayananjoka", Context.MODE_PRIVATE);

            prepareDirectory();
            uniqueId = System.currentTimeMillis() + "_" + Math.random();
            current = uniqueId + ".png";
            mypath = new File(directory, current);

            mContent = (LinearLayout)getActivity(). findViewById(R.id.linearLayout);
            mSignature = new signature(getContext(), null);
            mSignature.setBackgroundColor(Color.WHITE);
            mContent.addView(mSignature, ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT);
            mClear = (Button) getActivity().findViewById(R.id.clear);
            mGetSign = (Button) getActivity().findViewById(R.id.getsign);
            chkconsent = (CheckBox)getActivity().findViewById(R.id.chkconsent);
            chkconsent.setVisibility(View.GONE);
            mGetSign.setEnabled(false);
            mCancel = (Button) getActivity().findViewById(R.id.cancel);
            mView = mContent;

            mClear.setOnClickListener(new View.OnClickListener() {
                public void onClick(View v) {
                    Log.v("log_tag", "Panel Cleared");
                    mSignature.clear();
                    mGetSign.setEnabled(false);
                }
            });

            mGetSign.setOnClickListener(new View.OnClickListener() {
                public void onClick(View v) {
                    mView.setDrawingCacheEnabled(true);
                    String bitmap = mSignature.save(mView);

                    if (bitmap != null) {
                        Log.v("log_tag", "uri " + bitmap);
                        long id = getArguments().getLong(Payments.ID);
                        String cu = getArguments().getString(Sales.CUSTOMERID);
                        prevstep(id, cu, bitmap);

                    }
                }
            });

            mCancel.setOnClickListener(new View.OnClickListener() {
                public void onClick(View v) {
                    Log.v("log_tag", "Panel Canceled");
                    prevstep(getArguments().getLong(Payments.ID),getArguments().getString(Sales.CUSTOMERID),null);
                }
            });

        }

        @Override
        public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
            inflater = getActivity().getMenuInflater();

            MenuItem syn = menu.findItem(R.id.action_syncnow);
            MenuItem pair = menu.findItem(R.id.action_pair);
            MenuItem sett = menu.findItem(R.id.action_settings);

            syn.setVisible(false);
            pair.setVisible(false);
            sett.setVisible(false);

            super.onCreateOptionsMenu(menu, inflater);
        };

        @Override
        public boolean onOptionsItemSelected(MenuItem item) {
            return false;

        }

        public void prevstep(long id, String customer,String img) {

            MakePayments newFragment = new MakePayments();
            Bundle args = new Bundle();
            args.putLong(Payments.ID, id);
            args.putString(Sales.CUSTOMERID, customer);
            args.putString(Payments.ATTACHEMENT, img);
            newFragment.setArguments(args);
            FragmentTransaction transaction = getFragmentManager().beginTransaction();
            transaction.replace(R.id.content_frame, newFragment);
            transaction.addToBackStack(null);
            transaction.commit();
        }

        private boolean prepareDirectory()
        {
            try
            {
                if (makedirs())
                {
                    return true;
                } else {
                    return false;
                }
            } catch (Exception e)
            {
                e.printStackTrace();
//                showToast(getString(R.string.strnofilesystem));
                return false;
            }
        }

        private boolean makedirs()
        {
            File tempdir = new File(tempDir);
            if (!tempdir.exists())
                tempdir.mkdirs();

            if (tempdir.isDirectory())
            {
                File[] files = tempdir.listFiles();
                for (File file : files)
                {
                    if (!file.delete())
                    {
                        System.out.println("Failed to delete " + file);
                    }
                }
            }
            return (tempdir.isDirectory());
        }
        public class signature extends View
        {
            private static final float STROKE_WIDTH = 5f;
            private static final float HALF_STROKE_WIDTH = STROKE_WIDTH / 2;
            private Paint paint = new Paint();
            private Path path = new Path();

            private float lastTouchX;
            private float lastTouchY;
            private final RectF dirtyRect = new RectF();

            public signature(Context context, AttributeSet attrs)
            {
                super(context, attrs);
                paint.setAntiAlias(true);
                paint.setColor(Color.BLACK);
                paint.setStyle(Paint.Style.STROKE);
                paint.setStrokeJoin(Paint.Join.ROUND);
                paint.setStrokeWidth(STROKE_WIDTH);
            }

            public String save(View v)
            {
                Uri uri = null;
                Log.v("log_tag", "Width: " + v.getWidth());
                Log.v("log_tag", "Height: " + v.getHeight());
                String bitmap = null;
                if(mBitmap == null)
                {
                    mBitmap =  Bitmap.createBitmap(mContent.getWidth(), mContent.getHeight(), Bitmap.Config.RGB_565);;
                }
                Canvas canvas = new Canvas(mBitmap);
                try
                {
                    FileOutputStream mFileOutStream = new FileOutputStream(mypath);

                    v.draw(canvas);
                    mBitmap.compress(Bitmap.CompressFormat.PNG, 90, mFileOutStream);
                    bitmap = saveImage(mBitmap);
                }
                catch(Exception e)
                {
                    Log.v("log_tag", e.toString());
                }
                return bitmap;
            }

            public void clear()
            {
                path.reset();
                invalidate();
            }

            private String saveImage(Bitmap bitmap) {

                String stored = null;

                File sdcard = Environment.getExternalStorageDirectory() ;

                File folder = new File(sdcard.getAbsoluteFile(), Constants.MASTER_DIR);
                folder.mkdir();
                String filename = String.valueOf(System.currentTimeMillis());
                File file = new File(folder.getAbsoluteFile(), filename + ".jpg") ;
                if (file.exists())
                    return stored ;

                try {
                    FileOutputStream out = new FileOutputStream(file);
                    bitmap.compress(Bitmap.CompressFormat.JPEG, 90, out);
                    out.flush();
                    out.close();
                    stored = file.getAbsolutePath();
                    mGetSign.setEnabled(true);
                } catch (Exception e) {
                    e.printStackTrace();
                }
                return stored;
            }

            @Override
            protected void onDraw(Canvas canvas)
            {
                canvas.drawPath(path, paint);
            }

            @Override
            public boolean onTouchEvent(MotionEvent event)
            {
                float eventX = event.getX();
                float eventY = event.getY();
                mGetSign.setEnabled(true);

                switch (event.getAction())
                {
                    case MotionEvent.ACTION_DOWN:
                        path.moveTo(eventX, eventY);
                        lastTouchX = eventX;
                        lastTouchY = eventY;
                        return true;

                    case MotionEvent.ACTION_MOVE:

                    case MotionEvent.ACTION_UP:

                        resetDirtyRect(eventX, eventY);
                        int historySize = event.getHistorySize();
                        for (int i = 0; i < historySize; i++)
                        {
                            float historicalX = event.getHistoricalX(i);
                            float historicalY = event.getHistoricalY(i);
                            expandDirtyRect(historicalX, historicalY);
                            path.lineTo(historicalX, historicalY);
                        }
                        path.lineTo(eventX, eventY);
                        break;

                    default:
                        debug("Ignored touch event: " + event.toString());
                        return false;
                }

                invalidate((int) (dirtyRect.left - HALF_STROKE_WIDTH),
                        (int) (dirtyRect.top - HALF_STROKE_WIDTH),
                        (int) (dirtyRect.right + HALF_STROKE_WIDTH),
                        (int) (dirtyRect.bottom + HALF_STROKE_WIDTH));

                lastTouchX = eventX;
                lastTouchY = eventY;

                return true;
            }

            private void debug(String string){
            }

            private void expandDirtyRect(float historicalX, float historicalY)
            {
                if (historicalX < dirtyRect.left)
                {
                    dirtyRect.left = historicalX;
                }
                else if (historicalX > dirtyRect.right)
                {
                    dirtyRect.right = historicalX;
                }

                if (historicalY < dirtyRect.top)
                {
                    dirtyRect.top = historicalY;
                }
                else if (historicalY > dirtyRect.bottom)
                {
                    dirtyRect.bottom = historicalY;
                }
            }

            private void resetDirtyRect(float eventX, float eventY)
            {
                dirtyRect.left = Math.min(lastTouchX, eventX);
                dirtyRect.right = Math.max(lastTouchX, eventX);
                dirtyRect.top = Math.min(lastTouchY, eventY);
                dirtyRect.bottom = Math.max(lastTouchY, eventY);
            }
        }
    }
    public void makePaymentMethod(long id, String customer) {

        MakePayments newFragment = new MakePayments();
        Bundle args = new Bundle();
        args.putLong(Transfer.ID, id);
        args.putString(Sales.CUSTOMERID, customer);
        args.putString(Payments.ATTACHEMENT,null);
        newFragment.setArguments(args);
        FragmentTransaction transaction = getFragmentManager().beginTransaction();
        transaction.replace(R.id.content_frame, newFragment);
        transaction.addToBackStack(null);
        transaction.commit();
    }
}
