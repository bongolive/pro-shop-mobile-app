/*
 * The source codes in this project are owned by Bongolive Enterprises. Copying or any kind of usage without Bongolive's approval is against the law.
 * Copyright (c) 2015.
 *
 */

package bongolive.apps.proshop.view;

import android.accounts.Account;
import android.app.AlarmManager;
import android.app.Dialog;
import android.app.PendingIntent;
import android.bluetooth.BluetoothAdapter;
import android.content.ContentResolver;
import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.content.res.Configuration;
import android.graphics.Color;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBarActivity;
import android.support.v7.app.ActionBarDrawerToggle;
import android.text.Html;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ExpandableListView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.Locale;
import java.util.concurrent.TimeUnit;

import bongolive.apps.proshop.controller.ChildItem;
import bongolive.apps.proshop.controller.GroupItem;
import bongolive.apps.proshop.controller.Menu_Adapter;
import bongolive.apps.proshop.R;
import bongolive.apps.proshop.controller.AppPreference;
import bongolive.apps.proshop.model.ContentProviderApi;
import bongolive.apps.proshop.model.PurchaseItems;
import bongolive.apps.proshop.model.SalesItems;
import bongolive.apps.proshop.model.Settings;
import bongolive.apps.proshop.model.Warehouse;
import bongolive.apps.proshop.controller.Constants;
import bongolive.apps.proshop.controller.GpsTracker;
import bongolive.apps.proshop.controller.LocationTrigger;
import bongolive.apps.proshop.controller.MCrypt;


public class Dashboardbkup extends ActionBarActivity implements ExpandableListView.OnChildClickListener, ExpandableListView.OnGroupClickListener {

	private ExpandableListView drawerList;
	RelativeLayout mDrawerPane;
	private ActionBarDrawerToggle mDrawerToggle;
	private DrawerLayout mDrawerLayout;
	private Menu_Adapter ExpAdapter;
	private ArrayList<GroupItem> ExpListItems;
	String group_keys[] = null;

	String reports_items[] = null;
	private static final int REQUEST_ENABLE_BT = 2;
	private static final String ARG_SECTION_NUMBER = "section_number";
	private static final int REQUEST_PRINT = 1;
	private static  int CONNECTIONSTATUS = 0;
	Account mAccount;
	AlertDialogManager alert;
	Uri customer ;

	AppPreference appPreference  ;
	GpsTracker gpsTracker;
	BluetoothAdapter mBtAdapter;
	int accounttype;
    CharSequence mTitle;
	Button start;
	ProgressBar progressBar;
	TextView txtsyncsta;
	private boolean firstrun;
	String extra = null;

	boolean tabletSize = false;
	ActionBar actionBar ;
    MenuItem registeritem ;
	boolean showusers = false;


	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		appPreference = new AppPreference(getApplicationContext());
		appPreference.saveVatChoice(1);
        appPreference.saveDiscountStatus(1);
        appPreference.savePrintReceiptChoice(1);
		String languageToLoad = appPreference.getDefaultLanguage();

		alert = new AlertDialogManager();
		Locale locale = new Locale(languageToLoad);
		Locale.setDefault(locale);
		Configuration config = new Configuration();
		config.locale = locale;

		getBaseContext().getResources().updateConfiguration(config,
				getBaseContext().getResources().getDisplayMetrics());

		tabletSize = getResources().getBoolean(R.bool.isTablet);
		if(!tabletSize){
			setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
		}

		setContentView(R.layout.expandable_menu);

//        appPreference.clear_authentications();
//        wh();

		String apptype = Settings.getApptypeEnabled(this);

		TextView tv = (TextView)findViewById(R.id.version);
		String version = Constants.getVersionName(this);
        tv.setText(apptype+ " :  "+version);

		extra = Constants.SEARCHSOURCE_MAIN;

		actionBar = getSupportActionBar();
		actionBar.setBackgroundDrawable(getResources().getDrawable(R.drawable.ab_background_textured_prostockactionbar));

        gpsTracker = new GpsTracker(this);

		mAccount = Constants.createSyncAccount(this);
		mBtAdapter = BluetoothAdapter.getDefaultAdapter();

		Fragment fragment = new Home();
		showusers = Settings.getUserEnabled(this);


		FragmentManager fragmentManager = getSupportFragmentManager();
		Bundle bundle = new Bundle();
        bundle.putString(Constants.SEARCHSOURCE,Constants.SEARCHSOURCE_HOME);
		fragment.setArguments(bundle);
		fragmentManager.beginTransaction()
				.replace(R.id.content_frame, fragment)
				.commit();

		if(tabletSize){
			getSupportActionBar().setDisplayHomeAsUpEnabled(true);
			getSupportActionBar().setLogo(R.drawable.ic_drawer);
//			getSupportActionBar().setDisplayShowHomeEnabled(true);
		}

        if(!tabletSize)
		getSupportActionBar().setDisplayHomeAsUpEnabled(true);

		getSupportActionBar().setBackgroundDrawable(getResources().
				getDrawable(R.drawable.ab_background_textured_prostockactionbar));

        accounttype = appPreference.get_accounttype();

		System.out.println("acc type is "+accounttype);
		if(accounttype == 1){

			int tracktime = Settings.getTrackTime(this);
            System.out.println("track time is "+tracktime);
			if(tracktime > 0) {
                System.out.println("allow scheduled sync");
                scheduleAlarm();
            }

        }
        if(!appPreference.getBluetoothoption()) {
            String address = appPreference.getAddress();
            if (mBtAdapter != null) {
                if (!mBtAdapter.isEnabled()) {
                    getAddress();
                } else {
                    if (address.isEmpty())
                        getAddress();
                }
            } else {
                getAddress();
                Log.v("bluetoothaddress", " address is " + address);
            }
        }

		initDrawer();
//        Customers.confirmdeleteCustomer(this, "0");
		/*if(Customers.getCustomerCount(this) == 0){
			String fname = getString(R.string.strdefcustname);
			String bname = getString(R.string.strdefbussname);
			String mobile = "0000-000000";
			String area = "Agrey St Kkoo Dar es salaam";
			String email = "default@email.com";
			String[] vals = {fname,bname,mobile,email,area};
			Customers.insertCustomerLocal(this, vals);
		}*/
		String[] vals = {"1","SMPLE_CAT","TEGETA WAREHOUSE",""};
  /*      if(Warehouse.getCount(this) == 0)
		Warehouse.storeWh(this, vals);
*/
        Constants.createsuper(this);
	}

    private void check_firstrun() {
        firstrun = appPreference.check_firstrun();
        if (firstrun) {
            validate(Constants.TAGREGISTRATION);
            appPreference.set_firstrun();
            appPreference.save_last_sync(Constants.getDateOnly2());
        } else {
            String auth = appPreference.getAuthkey();
            String authtyping = null;
            if(!auth.isEmpty()){

                String im = Constants.getIMEINO(this);
                im = im.concat("|");
                int acctype = appPreference.get_accounttype();
                if(acctype == 0)
                    authtyping = Constants.ACCOUNT_TYPE_NORMAL;
                if(acctype == 1)
                    authtyping = Constants.ACCOUNT_TYPE_PREMIUM;

                MCrypt mCrypt = new MCrypt();
                try {
                    String crypted = new String(mCrypt.decrypt(auth));
                    if(im.concat(authtyping).equals(crypted)){
                        if(acctype == 1){
                            String time = appPreference.getDefaultSync();
							if(time.isEmpty())
							{
								time = getResources().getStringArray(R.array.pref_sync_frequency_values)[0];
							}
                            int t = Integer.parseInt(time);
                            if (PurchaseItems.getCount(this) < 1 || Settings.getSettingsCount(this) < 1) {
                                Toast.makeText(this, getString(R.string.strsyncsstartstatus), Toast
                                        .LENGTH_SHORT)
                                        .show();
                                ondemandsync();
                            } else {
                                periodicadapersync(t);
                            }
                        } else {
                            Toast.makeText(this,
                                    getString(R.string.normalaccountmsg),
                                    Toast.LENGTH_SHORT).show();
                        }
                    } else {
                        validate("validate");
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            } else {
                validate(Constants.TAGREGISTRATION);
            }
        }
    }

    public void getAddress(){
        Intent selectDevice = new Intent(this, UniversalBluetoothInstance.class);
        startActivityForResult(selectDevice, REQUEST_PRINT);
	}

	private void initDrawer() {
		mDrawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout);

		drawerList = (ExpandableListView) findViewById(R.id.left_drawer);


		mDrawerPane = (RelativeLayout) findViewById(R.id.drawerPane);

		mDrawerToggle = new ActionBarDrawerToggle(this, mDrawerLayout, R.string.navigation_drawer_open, R.string
				.navigation_drawer_close) {
			@Override
			public void onDrawerOpened(View drawerView) {
				super.onDrawerOpened(drawerView);

				invalidateOptionsMenu();
			}

			@Override
			public void onDrawerClosed(View drawerView) {
				super.onDrawerClosed(drawerView);
				invalidateOptionsMenu();
			}
		};

        mTitle = getTitle();

        if(!tabletSize)
		mDrawerLayout.setDrawerListener(mDrawerToggle);

		ExpListItems = SetStandardGroups();
		ExpAdapter = new Menu_Adapter(this, ExpListItems);
		drawerList.setAdapter(ExpAdapter);
		drawerList.setOnChildClickListener(this);
		drawerList.setOnGroupClickListener(this);

	}


    public void wh(){
        ContentResolver cr = getContentResolver();
        ContentValues cv = new ContentValues();
        cv.put(SalesItems.WH, Warehouse.getDefaultId(this));
        cr.update(SalesItems.BASEURI, cv, null, null);
    }

	@Override
	protected void onPostCreate(Bundle savedInstanceState) {
		super.onPostCreate(savedInstanceState);
		mDrawerToggle.syncState();
	}

	public ArrayList<GroupItem> SetStandardGroups() {
        String apptype = Settings.getApptypeEnabled(this);
        if(apptype.equals(Constants.APPTYPE_SALE)) {
            if(showusers) {
                if (appPreference.getUserRole() == 0) {
                    group_keys = new String[]{
                            getString(R.string.title_section1),
                            getString(R.string.title_section2),
                            getString(R.string.title_section10),
                            getString(R.string.title_section3),
                            getString(R.string.title_section4),
                            getString(R.string.title_section7)
                    };
                } else {
                    group_keys = new String[]{
                            getString(R.string.title_section1),
                            getString(R.string.title_section2),
                            getString(R.string.title_section10),
                            getString(R.string.title_section3),
                            getString(R.string.title_section4),
                            getString(R.string.title_section5),
                            getString(R.string.title_section6),
                            getString(R.string.title_section7)
                    };
                }
            } else {
                group_keys = new String[]{
                        getString(R.string.title_section1),
                        getString(R.string.title_section2),
                        getString(R.string.title_section10),
                        getString(R.string.title_section3),
                        getString(R.string.title_section4),
                        getString(R.string.title_section6),
                        getString(R.string.title_section7)
                };
            }

            reports_items = new String[]{

                    getString(R.string.reporttitle6),
                    getString(R.string.reporttitle1),
                    getString(R.string.reporttitle2),
                    getString(R.string.reporttitle5),
                    getString(R.string.reporttitle4)
            };
            ArrayList<GroupItem> list = new ArrayList<GroupItem>();

            ArrayList<ChildItem> ch_list;

            int size = reports_items.length;
            int j = 0;

            for (int i = 0; i < group_keys.length; i++) {
                GroupItem gru = new GroupItem();
                gru.setName(group_keys[i]);
                ch_list = new ArrayList<ChildItem>();
                if (appPreference.getUserRole() == 0) {

                } else {
                    if (i == group_keys.length - 2) {
                        for (; j < size; j++) {
                            ChildItem ch = new ChildItem();
                            ch.setName("   " + reports_items[j]);
                            ch_list.add(ch);
                        }
                        gru.setItems(ch_list);
                    }
                }

                list.add(gru);

            }
            return list;
        } else {

            if (appPreference.getUserRole() == 0) {
                group_keys = new String[]{
                        getString(R.string.title_section1),
                        getString(R.string.title_section2),
                        getString(R.string.title_section10),
                        getString(R.string.title_section3),
                        getString(R.string.title_section4),
                        getString(R.string.title_section7)
                };
            } else {
                group_keys = new String[]{
                        getString(R.string.title_section1),
                        getString(R.string.title_section2),
                        getString(R.string.title_section10),
                        getString(R.string.title_section3),
                        getString(R.string.title_section4),
                        getString(R.string.title_section5),
                        getString(R.string.title_section6),
                        getString(R.string.title_section7)
                };
            }

            reports_items = new String[]{

                    getString(R.string.reporttitle6),
                    getString(R.string.reporttitle1),
                    getString(R.string.reporttitle2),
                    getString(R.string.reporttitle5),
                    getString(R.string.reporttitle4)
            };
            ArrayList<GroupItem> list = new ArrayList<GroupItem>();

            ArrayList<ChildItem> ch_list;

            int size = reports_items.length;
            int j = 0;

            for (int i = 0; i < group_keys.length; i++) {
                GroupItem gru = new GroupItem();
                gru.setName(group_keys[i]);
                ch_list = new ArrayList<ChildItem>();
                if (appPreference.getUserRole() == 0) {

                } else {
                    if (i == group_keys.length - 2) {
                        for (; j < size; j++) {
                            ChildItem ch = new ChildItem();
                            ch.setName("   " + reports_items[j]);
                            ch_list.add(ch);
                        }
                        gru.setItems(ch_list);
                    }
                }

                list.add(gru);

            }
            return list;
        }
	}


	public boolean onPrepareOptionsMenu(Menu menu) {
		// If the nav drawer is open, hide action items related to the content view
//        boolean drawerOpen = mDrawerLayout.isDrawerOpen(mDrawerList);
//        menu.findItem(R.id.action_settings).setVisible(!drawerOpen);
		return super.onPrepareOptionsMenu(menu);
	}

	@Override
	public boolean onChildClick(ExpandableListView parent, View v, int groupPosition, int childPosition, long id)
	{
		selectChild(childPosition);
		return true;
	}

	private void selectChild(int position) {
		Fragment fragment = new SalesByCustomer();

		FragmentManager fragmentManager = getSupportFragmentManager();
		Bundle bundle = new Bundle();
		switch (position){
			case 1:
				bundle.putString(Constants.REPORTS, Constants.SALESBYCUSTOMER);
				fragment.setArguments(bundle);
				fragmentManager.beginTransaction()
						.replace(R.id.content_frame, fragment)
						.commit();
				break;
			case 2:
				bundle.putString(Constants.REPORTS, Constants.SALESBYPRODUCT);
				fragment.setArguments(bundle);
				fragmentManager.beginTransaction()
						.replace(R.id.content_frame, fragment)
						.commit();
				drawerList.setItemChecked(position, true);
				break;
			case 3:
				bundle.putString(Constants.REPORTS, Constants.TAXBYPRODUCT);
				fragment.setArguments(bundle);
				fragmentManager.beginTransaction()
						.replace(R.id.content_frame, fragment)
						.commit();
				drawerList.setItemChecked(position, true);
				break;
			case 4:
				bundle.putString(Constants.REPORTS, Constants.PRODUCTREORDER);
				fragment.setArguments(bundle);
				fragmentManager.beginTransaction()
						.replace(R.id.content_frame, fragment)
						.commit();
				drawerList.setItemChecked(position, true);
				break;
			case 0:
				bundle.putString(Constants.REPORTS, Constants.SALEREPORT);
				fragment.setArguments(bundle);
				fragmentManager.beginTransaction()
						.replace(R.id.content_frame, fragment)
						.commit();
				drawerList.setItemChecked(position, true);
				break;
		}

        setTitle(getString(R.string.strreportheader));
        if(!tabletSize)
            mDrawerLayout.closeDrawer(mDrawerPane);

	}
	@Override
	public boolean onGroupClick(ExpandableListView parent, View v, int groupPosition, long id) {

        System.out.println(appPreference.getUserRole());
        if(appPreference.getUserRole() == 0) {
                selectItemFromDrawer(groupPosition);
        } else {
            if (groupPosition != group_keys.length - 2) {
                selectItemFromDrawer(groupPosition);
            }
        }
		return false;
	}


	private void selectItemFromDrawer(int position) {
		int newposition = position+1 ;
        System.out.println(" pos "+newposition);
		FragmentManager fragmentManager = getSupportFragmentManager();
		Bundle args = new Bundle() ;
        String apptype = Settings.getApptypeEnabled(this);
        if(apptype.equals(Constants.APPTYPE_SALE)) {
            if(showusers) {
                if (appPreference.getUserRole() != 0)
                    switch (newposition) {
                        case 1:
                            extra = Constants.SEARCHSOURCE_HOME;
                            Home bb = new Home();
                            args.putInt(ARG_SECTION_NUMBER, newposition);
                            bb.setArguments(args);
                            fragmentManager.beginTransaction().
                                    replace(R.id.content_frame, bb).
                                    commit();
                            break;
                        case 2:
                            Intent orderintent = new Intent(this, OrdersActivity.class);
                            startActivity(orderintent);
                            break;
                        case 3:
                            extra = Constants.SEARCHSOURCE_PRODUCTS;
                            DeliveriesFragment dd = new DeliveriesFragment();
                            args.putInt(ARG_SECTION_NUMBER, newposition);
                            dd.setArguments(args);
                            fragmentManager.beginTransaction().
                                    replace(R.id.content_frame, dd).
                                    commit();
                            break;
                        case 4:
                            extra = Constants.SEARCHSOURCE_CUSTOMERS;
                            CustomersFragment cc = new CustomersFragment();
                            args.putInt(ARG_SECTION_NUMBER, newposition);
                            cc.setArguments(args);
                            fragmentManager.beginTransaction().
                                    replace(R.id.content_frame, cc).
                                    commit();
                            break;
                        case 5:
                            extra = Constants.SEARCHSOURCE_PRODUCTS;
                            ProductsFragment pp = new ProductsFragment();
                            args.putInt(ARG_SECTION_NUMBER, newposition);
                            pp.setArguments(args);
                            fragmentManager.beginTransaction().
                                    replace(R.id.content_frame, pp).
                                    commit();
                            break;
                        case 6:
                            UserManagement umng = new UserManagement();
                            args.putInt(ARG_SECTION_NUMBER, newposition);
                            umng.setArguments(args);
                            fragmentManager.beginTransaction().
                                    replace(R.id.content_frame, umng).
                                    commit();
                            break;
                        case 8:
                            UserAccount sfr = new UserAccount();
                            args.putInt(ARG_SECTION_NUMBER, newposition);
                            sfr.setArguments(args);
                            fragmentManager.beginTransaction().
                                    replace(R.id.content_frame, sfr).
                                    commit();
                            break;
                    }
                else
                    switch (newposition) {
                        case 1:
                            extra = Constants.SEARCHSOURCE_HOME;
                            Home bb = new Home();
                            args.putInt(ARG_SECTION_NUMBER, newposition);
                            bb.setArguments(args);
                            fragmentManager.beginTransaction().
                                    replace(R.id.content_frame, bb).
                                    commit();
                            break;
                        case 2:
                            Intent orderintent = new Intent(this, OrdersActivity.class);
                            startActivity(orderintent);
                            break;
                        case 3:
                            extra = Constants.SEARCHSOURCE_PRODUCTS;
                            DeliveriesFragment dd = new DeliveriesFragment();
                            args.putInt(ARG_SECTION_NUMBER, newposition);
                            dd.setArguments(args);
                            fragmentManager.beginTransaction().
                                    replace(R.id.content_frame, dd).
                                    commit();
                            break;
                        case 4:
                            extra = Constants.SEARCHSOURCE_CUSTOMERS;
                            CustomersFragment cc = new CustomersFragment();
                            args.putInt(ARG_SECTION_NUMBER, newposition);
                            cc.setArguments(args);
                            fragmentManager.beginTransaction().
                                    replace(R.id.content_frame, cc).
                                    commit();
                            break;
                        case 5:
                            extra = Constants.SEARCHSOURCE_PRODUCTS;
                            ProductsFragment pp = new ProductsFragment();
                            args.putInt(ARG_SECTION_NUMBER, newposition);
                            pp.setArguments(args);
                            fragmentManager.beginTransaction().
                                    replace(R.id.content_frame, pp).
                                    commit();
                            break;
                        case 7:
                            UserAccount sfr = new UserAccount();
                            args.putInt(ARG_SECTION_NUMBER, newposition);
                            sfr.setArguments(args);
                            fragmentManager.beginTransaction().
                                    replace(R.id.content_frame, sfr).
                                    commit();
                            break;
                    }

                drawerList.setItemChecked(position, true);

                setTitle(group_keys[position]);
                if (!tabletSize)
                    mDrawerLayout.closeDrawer(mDrawerPane);
            } else {
                System.out.println("here ap type is prosale and showusers is no");
                switch (newposition) {
                    case 1:
                        extra = Constants.SEARCHSOURCE_HOME;
                        Home bb = new Home();
                        args.putInt(ARG_SECTION_NUMBER, newposition);
                        bb.setArguments(args);
                        fragmentManager.beginTransaction().
                                replace(R.id.content_frame, bb).
                                commit();
                        break;
                    case 2:
                        Intent orderintent = new Intent(this, OrdersActivity.class);
                        startActivity(orderintent);
                        break;
                    case 3:
                        extra = Constants.SEARCHSOURCE_PRODUCTS;
                        DeliveriesFragment dd = new DeliveriesFragment();
                        args.putInt(ARG_SECTION_NUMBER, newposition);
                        dd.setArguments(args);
                        fragmentManager.beginTransaction().
                                replace(R.id.content_frame, dd).
                                commit();
                        break;
                    case 4:
                        extra = Constants.SEARCHSOURCE_CUSTOMERS;
                        CustomersFragment cc = new CustomersFragment();
                        args.putInt(ARG_SECTION_NUMBER, newposition);
                        cc.setArguments(args);
                        fragmentManager.beginTransaction().
                                replace(R.id.content_frame, cc).
                                commit();
                        break;
                    case 5:
                        extra = Constants.SEARCHSOURCE_PRODUCTS;
                        ProductsFragment pp = new ProductsFragment();
                        args.putInt(ARG_SECTION_NUMBER, newposition);
                        pp.setArguments(args);
                        fragmentManager.beginTransaction().
                                replace(R.id.content_frame, pp).
                                commit();
                        break;
                    case 8:
                        UserManagement umng = new UserManagement();
                        args.putInt(ARG_SECTION_NUMBER, newposition);
                        umng.setArguments(args);
                        fragmentManager.beginTransaction().
                                replace(R.id.content_frame, umng).
                                commit();
                        break;
                    case 7:
                        UserAccount sfr = new UserAccount();
                        args.putInt(ARG_SECTION_NUMBER, newposition);
                        sfr.setArguments(args);
                        fragmentManager.beginTransaction().
                                replace(R.id.content_frame, sfr).
                                commit();
                        break;
                }
                drawerList.setItemChecked(position, true);

                setTitle(group_keys[position]);
                if (!tabletSize)
                    mDrawerLayout.closeDrawer(mDrawerPane);
            }
        } else {

            if (appPreference.getUserRole() != 0)
                switch (newposition) {
                    case 1:
                        extra = Constants.SEARCHSOURCE_HOME;
                        Home bb = new Home();
                        args.putInt(ARG_SECTION_NUMBER, newposition);
                        bb.setArguments(args);
                        fragmentManager.beginTransaction().
                                replace(R.id.content_frame, bb).
                                commit();
                        break;
                    case 2:
                        Intent orderintent = new Intent(this, OrdersActivity.class);
                        startActivity(orderintent);
                        break;
                    case 3:
                        extra = Constants.SEARCHSOURCE_PRODUCTS;
                        DeliveriesFragment dd = new DeliveriesFragment();
                        args.putInt(ARG_SECTION_NUMBER, newposition);
                        dd.setArguments(args);
                        fragmentManager.beginTransaction().
                                replace(R.id.content_frame, dd).
                                commit();
                        break;
                    case 4:
                        extra = Constants.SEARCHSOURCE_CUSTOMERS;
                        CustomersFragment cc = new CustomersFragment();
                        args.putInt(ARG_SECTION_NUMBER, newposition);
                        cc.setArguments(args);
                        fragmentManager.beginTransaction().
                                replace(R.id.content_frame, cc).
                                commit();
                        break;
                    case 5:
                        extra = Constants.SEARCHSOURCE_PRODUCTS;
                        ProductsFragment pp = new ProductsFragment();
                        args.putInt(ARG_SECTION_NUMBER, newposition);
                        pp.setArguments(args);
                        fragmentManager.beginTransaction().
                                replace(R.id.content_frame, pp).
                                commit();
                        break;
                    case 6:
                        UserManagement umng = new UserManagement();
                        args.putInt(ARG_SECTION_NUMBER, newposition);
                        umng.setArguments(args);
                        fragmentManager.beginTransaction().
                                replace(R.id.content_frame, umng).
                                commit();
                        break;
                    case 7:
                        UserAccount sfr = new UserAccount();
                        args.putInt(ARG_SECTION_NUMBER, newposition);
                        sfr.setArguments(args);
                        fragmentManager.beginTransaction().
                                replace(R.id.content_frame, sfr).
                                commit();
                        break;
                }
            else
                switch (newposition) {
                    case 1:
                        extra = Constants.SEARCHSOURCE_HOME;
                        Home bb = new Home();
                        args.putInt(ARG_SECTION_NUMBER, newposition);
                        bb.setArguments(args);
                        fragmentManager.beginTransaction().
                                replace(R.id.content_frame, bb).
                                commit();
                        break;
                    case 2:
                        Intent orderintent = new Intent(this, OrdersActivity.class);
                        startActivity(orderintent);
                        break;
                    case 3:
                        extra = Constants.SEARCHSOURCE_PRODUCTS;
                        DeliveriesFragment dd = new DeliveriesFragment();
                        args.putInt(ARG_SECTION_NUMBER, newposition);
                        dd.setArguments(args);
                        fragmentManager.beginTransaction().
                                replace(R.id.content_frame, dd).
                                commit();
                        break;
                    case 4:
                        extra = Constants.SEARCHSOURCE_CUSTOMERS;
                        CustomersFragment cc = new CustomersFragment();
                        args.putInt(ARG_SECTION_NUMBER, newposition);
                        cc.setArguments(args);
                        fragmentManager.beginTransaction().
                                replace(R.id.content_frame, cc).
                                commit();
                        break;
                    case 5:
                        UserManagement umng = new UserManagement();
                        args.putInt(ARG_SECTION_NUMBER, newposition);
                        umng.setArguments(args);
                        fragmentManager.beginTransaction().
                                replace(R.id.content_frame, umng).
                                commit();
                        break;
                    case 7:
                        UserAccount sfr = new UserAccount();
                        args.putInt(ARG_SECTION_NUMBER, newposition);
                        sfr.setArguments(args);
                        fragmentManager.beginTransaction().
                                replace(R.id.content_frame, sfr).
                                commit();
                        break;
                }

            drawerList.setItemChecked(position, true);

            setTitle(group_keys[position]);
            if (!tabletSize)
                mDrawerLayout.closeDrawer(mDrawerPane);
        }
	}


	/*
     * running the sync adapter periodically
     */
	public void periodicadapersync(int time)
	{
        ContentResolver.addPeriodicSync(
                mAccount,
                ContentProviderApi.AUTHORITY,
                Bundle.EMPTY, time) ;
	}
	/*
     * running the sync adapter on demand
     */
	public void ondemandsync()
	{
		System.out.println("ondemand sync");
		Bundle settingsBundle = new Bundle() ;
		settingsBundle.putBoolean(ContentResolver.SYNC_EXTRAS_MANUAL, true);
		settingsBundle.putBoolean(ContentResolver.SYNC_EXTRAS_EXPEDITED, true);
        ContentResolver.requestSync(mAccount, ContentProviderApi.AUTHORITY, settingsBundle);
//        SyncInfo info = ContentResolver.getCurrentSync();
//        System.out.println("current sync "+info);
	}

	@Override
	protected void onStart() {
		super.onStart();
		setnavdrawer();

			turnGpsOn() ;
	}

	public void turnGpsOn(){
//        if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
           /* Intent inte = new Intent(android.provider.Settings.ACTION_LOCATION_SOURCE_SETTINGS);
            startActivity(inte);*/
       /* } else {
            Intent intent = new Intent("android.location.GPS_ENABLED_CHANGE");
            intent.putExtra("enabled", true);
            sendBroadcast(intent);
        }*/
	}
	public void turnGpsOf(){
/*
		Intent intent = new Intent("android.location.GPS_ENABLED_CHANGE");
		intent.putExtra("enabled", false);
		sendBroadcast(intent);*/
	}

	@Override
	public void onResume() {
        super.onResume();
		setnavdrawer();
        System.out.println("account " + accounttype);
		if(accounttype == 1 && !gpsTracker.isgpson())
			turnGpsOn();
	}

	@Override
	protected void onPause() {
		super.onPause();

			gpsTracker.stopUsingGPS();
	}
	@Override
	protected void onStop() {
		super.onStop();

			gpsTracker.stopUsingGPS();
//			turnGpsOf();

	}


	public void scheduleAlarm() {
		Intent intent = new Intent(getApplicationContext(), LocationTrigger.class);
		final PendingIntent pIntent = PendingIntent.getBroadcast(this, LocationTrigger.REQUEST_CODE,
				intent, PendingIntent.FLAG_UPDATE_CURRENT);
		long firstMillis = System.currentTimeMillis();
		long intervalMillis = 0;
		int minutesintervar = Settings.getTrackTime(this);
		if(minutesintervar == 0){
			intervalMillis = Integer.parseInt(Constants.DEFAULTTRACKINTERVAL);
		} else {
			String interval = String.valueOf(minutesintervar);
			long ii = Long.valueOf(interval);
			intervalMillis = TimeUnit.MINUTES.toMillis(ii);
		}
		if(intervalMillis != 0) {
			AlarmManager alarm = (AlarmManager) this.getSystemService(Context.ALARM_SERVICE);
            alarm.setInexactRepeating(AlarmManager.RTC_WAKEUP, firstMillis, intervalMillis, pIntent);
		}
	}
	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		super.onActivityResult(requestCode, resultCode, data);
		switch (requestCode) {
			case REQUEST_PRINT: {
				if (resultCode == RESULT_OK) {
					Toast.makeText(this, "Device connectted address "+appPreference.getAddress(),Toast.LENGTH_LONG).show();
				} else {
					Toast.makeText(this, "Device not connectted",Toast.LENGTH_LONG).show();
				}
				break;
			}
			case REQUEST_ENABLE_BT: {
				if (!mBtAdapter.isEnabled()) {
					Intent enableBtIntent = new Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE);
					startActivityForResult(enableBtIntent, REQUEST_ENABLE_BT);
				}
				break;
			}
		}
	}

	private void validate(String source){
		LayoutInflater infl = getLayoutInflater();
		View view = infl.inflate(R.layout.startscreen, null) ;

		final Dialog dialog = new Dialog(this, R.style.CustomDialog	);
		dialog.setCancelable(false);
		dialog.setContentView(view);

		progressBar = (ProgressBar)dialog.findViewById(R.id.progressbar);
		txtsyncsta = (TextView)dialog.findViewById(R.id.txtbody);
		start = (Button)dialog.findViewById(R.id.btnstart);
		progressBar.setVisibility(View.VISIBLE);
		txtsyncsta.setVisibility(View.VISIBLE);

		boolean network = Constants.isNotConnected(this);
        System.out.println("network "+network);
		if(!network) {
            Toast.makeText(this, getString(R.string.strinternetwarn), Toast.LENGTH_LONG).show();
            return;
        }
		new DeviceRegistration(dialog.getContext(),progressBar,txtsyncsta,start).execute(source);
		start.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				String auth = appPreference.getAuthkey();
				String authtyping = null;
				if (!auth.isEmpty()) {

					String im = Constants.getIMEINO(dialog.getContext());
					im = im.concat("|");
					int acctype = appPreference.get_accounttype();
					if (acctype == 0)
						authtyping = Constants.ACCOUNT_TYPE_NORMAL;
					if (acctype == 1)
						authtyping = Constants.ACCOUNT_TYPE_PREMIUM;

					MCrypt mCrypt = new MCrypt();
					try {
						String crypted = new String(mCrypt.decrypt(auth));
						if (im.concat(authtyping).equals(crypted)) {
							dialog.dismiss();
						} else {
							dialog.dismiss();
                            appPreference.clear_authentications();
							invalidaccount();
						}
					} catch (Exception e) {
						e.printStackTrace();
					}
				} else {
					invalidaccount();
                    appPreference.clear_authentications();
					dialog.dismiss();
				}
			}
		});
		dialog.show();
	}

	private void invalidaccount(){
		LayoutInflater infl = getLayoutInflater();
		View view = infl.inflate(R.layout.startscreen, null) ;

		final Dialog dialog = new Dialog(this, R.style.CustomDialog	);
		dialog.setCancelable(false);
		dialog.setContentView(view);

		progressBar = (ProgressBar)dialog.findViewById(R.id.progressbar);
		TextView txthead = (TextView)dialog.findViewById(R.id.txtheader);
		start = (Button)dialog.findViewById(R.id.btnstart);
		txthead.setText(getString(R.string.strtokenerror));
		txthead.append("\n\n");
		txthead.append(getString(R.string.strtokenerrormsg));

		Runnable runnable = new Runnable() {
			@Override
			public void run() {
				appPreference.clear_authentications();
				dialog.dismiss();
				System.exit(0);
			}
		};
		Handler handler = new Handler();
        handler.postDelayed(runnable, 15000);

		start.setVisibility(View.VISIBLE);
        start.setText(getString(R.string.strclose));
		start.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				appPreference.clear_authentications();
				dialog.dismiss();
				System.exit(0);
			}
		});
		dialog.show();
	}
	@Override
	public void startActivity(Intent intent) {
		if (Intent.ACTION_SEARCH.equals(intent.getAction())) {
			intent.putExtra(Constants.SEARCHSOURCE, extra);
		}
		super.startActivity(intent);
	}

	private void setnavdrawer(){
		if(tabletSize){
			mDrawerLayout.setDrawerLockMode(DrawerLayout.LOCK_MODE_LOCKED_OPEN);
            mDrawerLayout.setScrimColor(Color.TRANSPARENT);
		}
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
        if(!mDrawerLayout.isDrawerOpen(mDrawerPane) || tabletSize) {
            getMenuInflater().inflate(R.menu.dashboard, menu);
            String address = appPreference.getAddress();
            if (address.isEmpty()) {
                menu.findItem(R.id.action_pair).setTitle(getString(R.string.strscanpair));
                CONNECTIONSTATUS = 1;
            } else {
                menu.findItem(R.id.action_pair).setTitle(getString(R.string.strclearpair));
                CONNECTIONSTATUS = 2;
            }
            String auth = appPreference.getAuthkey();
			if(auth.isEmpty()){
                registeritem = menu.findItem(R.id.action_register);
                registeritem.setVisible(true);
                if(extra.equals(Constants.SEARCHSOURCE_MAIN) || extra.equals(Constants.SEARCHSOURCE_HOME)){
                    registeritem.setShowAsAction(MenuItem.SHOW_AS_ACTION_ALWAYS);
                }
            }

//        restoreActionBar();
            return true;
        }
		return super.onCreateOptionsMenu(menu);
	}

    public void restoreActionBar() {

        actionBar.setNavigationMode(ActionBar.NAVIGATION_MODE_STANDARD);
        actionBar.setDisplayShowTitleEnabled(true);
        actionBar.setTitle( Html.fromHtml("<font color=#ffffff>" + mTitle + " </font>") );
        actionBar.setBackgroundDrawable(getResources().getDrawable(R.drawable.ab_background_textured_prostockactionbar));
    }

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		int id = item.getItemId();
		switch(id){
			case R.id.action_settings:
				Intent intent = new Intent(this, SettingsActivity.class);
				intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
				startActivity(intent);
                finish();
				return true;
			case R.id.action_syncnow:
				ondemandsync();
				/*
			Intent in = new Intent(this, UploadPhoto.class);
			startActivity(in);*/
				break;
			case R.id.action_pair:
				if(CONNECTIONSTATUS == 2){
					appPreference.clearAddress();
					Log.v("ADDRESS"," address cleared now it is "+appPreference.getAddress());
				} else if (CONNECTIONSTATUS == 1) {
					getAddress();
				}
				break;
			case R.id.action_register:
                check_firstrun();
				break;
		}
        if(!tabletSize)
		if (mDrawerToggle.onOptionsItemSelected(item)) {
			return true;
		}

		return super.onOptionsItemSelected(item);
	}

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        mDrawerToggle.onConfigurationChanged(newConfig);
    }

}