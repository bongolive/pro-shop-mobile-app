/*
 * Copyright (c) 2015. All rights reserved. The source codes are property of bongolive
 */

package bongolive.apps.proshop.controller;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseExpandableListAdapter;
import android.widget.TextView;

import java.util.ArrayList;

import bongolive.apps.proshop.R;

/**
 * Created by nasznjoka on 5/29/15.
 */
public class Menu_Adapter extends BaseExpandableListAdapter {
    AppPreference appPreference ;
    private Context context;
    private ArrayList<GroupItem> groups;

    public Menu_Adapter(Context context, ArrayList<GroupItem> groups) {
        this.context = context;
        this.groups = groups;
        appPreference = new AppPreference(context);
    }

    @Override
    public Object getChild(int groupPosition, int childPosition) {
        ArrayList<ChildItem> chList = groups.get(groupPosition).getItems();
        return chList.get(childPosition);
    }

    @Override
    public long getChildId(int groupPosition, int childPosition) {
        return childPosition;
    }

    @Override
    public View getChildView(int groupPosition, int childPosition,
                             boolean isLastChild, View convertView, ViewGroup parent) {

        ChildItem child = (ChildItem) getChild(groupPosition, childPosition);
        if (convertView == null) {
            LayoutInflater infalInflater = (LayoutInflater) context
                    .getSystemService(context.LAYOUT_INFLATER_SERVICE);
            convertView = infalInflater.inflate(R.layout.child, null);
        }
        TextView tv = (TextView) convertView.findViewById(R.id.child_name);
//        ImageView iv = (ImageView) convertView.findViewById(R.id.flag);

        tv.setText(child.getName().toString());
//        iv.setImageResource(child.getImage());

        return convertView;
    }

    @Override
    public int getChildrenCount(int groupPosition) {
        if(groupPosition == groups.size()-2) {
            ArrayList<ChildItem> chList = groups.get(groupPosition).getItems();

            return chList.size();
        }
        return 0;
    }

    @Override
    public Object getGroup(int groupPosition) {
        return groups.get(groupPosition);
    }

    @Override
    public int getGroupCount() {
        return groups.size();
    }

    @Override
    public long getGroupId(int groupPosition) {
        return groupPosition;
    }

    @Override
    public View getGroupView(int groupPosition, boolean isExpanded,
                             View convertView, ViewGroup parent) {
        GroupItem group = (GroupItem) getGroup(groupPosition);
        if (convertView == null) {
            LayoutInflater inf = (LayoutInflater) context
                    .getSystemService(context.LAYOUT_INFLATER_SERVICE);
            convertView = inf.inflate(R.layout.group, null);
        }
        TextView tv = (TextView) convertView.findViewById(R.id.group_name);
        tv.setText(group.getName());
        return convertView;
    }

    @Override
    public boolean hasStableIds() {
        return true;
    }

    @Override
    public boolean isChildSelectable(int groupPosition, int childPosition) {
        if(groupPosition == groups.size()-2){
            return true;
        }
        return false;
    }

}
