/*
 * Copyright (c) 2015.
 * Prosurvey is a property of Bongolive Enterprises Ltd. An authorized
 * copy of ideas, source codes is against the regulations
 */

package bongolive.apps.proshop.controller;

import android.app.IntentService;
import android.content.Intent;
import android.os.Build;
import android.util.Log;
import android.widget.Toast;

import bongolive.apps.proshop.R;
import bongolive.apps.proshop.model.Tracking;

/**
 * An {@link IntentService} subclass for handling asynchronous task requests in
 * a service on a separate handler thread.
 * <p/>
 * TODO: Customize class - update intent actions, extra parameters and static
 * helper methods.
 */
public class MyLocationServices extends IntentService {
GpsTracker gpsTracker;

    public MyLocationServices() {
        super("LocationServices");
    }


    @Override
    protected void onHandleIntent(Intent intent) {
        turnGpsOn();

        Log.v("GPSON", "GPS IS TURNED ON");
        gpsTracker = new GpsTracker(getApplicationContext());
        AppPreference app = new AppPreference(getApplicationContext());
        double lat = gpsTracker.getLatitude();
        double lng = gpsTracker.getLongitude();
        double[] vals = {lat,lng};
        float[] val = {new Float(lat),new Float(lng)};
        float[] lastloc = app.getLastKnowLocation();
        if(lastloc[0] != val[0] || lastloc[1] != val[1]) {
            if (!vals.equals(0)) {
                app.saveLastLocation(val);
                Tracking.storeLocation(getApplicationContext(), vals);
            }
            Log.v("GPSOF", "GPS IS TURNED OF");
        } else {
            System.out.println("the last location " + lastloc[0] + "," + lastloc[1] + " has not changed so ignore new loc " +
                    +val[0] + "," + val[1]);
        }
        gpsTracker.stopUsingGPS();
        turnGpsOf();
    }

    public void turnGpsOn(){

            if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
                System.out.println(" gps "+gpsTracker.isgpson());
                if(!gpsTracker.isgpson()) {
                    Toast.makeText(this, getString(R.string.strgps), Toast.LENGTH_LONG).show();
                    Intent inte = new Intent(android.provider.Settings.ACTION_LOCATION_SOURCE_SETTINGS);
                    startActivity(inte);
                }
            } else {
                Intent intent = new Intent("android.location.GPS_ENABLED_CHANGE");
                intent.putExtra("enabled", true);
                sendBroadcast(intent);
            }
    }
    public void turnGpsOf(){
        if(Build.VERSION.SDK_INT < Build.VERSION_CODES.KITKAT) {
            Intent intent = new Intent("android.location.GPS_ENABLED_CHANGE");
            intent.putExtra("enabled", false);
            sendBroadcast(intent);
        }
    }
}
