package bongolive.apps.proshop.controller;

import android.app.Activity;
import android.content.Context;
import android.graphics.drawable.Drawable;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import bongolive.apps.proshop.R;
import bongolive.apps.proshop.model.Customers;
import bongolive.apps.proshop.model.Payments;
import bongolive.apps.proshop.model.Sales;

/**
 * Created by anandbose on 09/06/15.
 */
public class PaymentSplitAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    public static final int HEADER = 0;
    public static final int CHILD = 1;

    private List<Payments> data;
    Activity context;
    AppPreference appPreference;
    String currency = null;
    static OnItemClickListener clickListener ;

    public PaymentSplitAdapter(Activity mcontext, ArrayList<Payments> myDataset){
        this.data = myDataset;
        this.context = mcontext;
        this.appPreference = new AppPreference(context);
        this.currency = appPreference.getDefaultCurrency();
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int type) {
        View view = null;
        switch (type) {
            case HEADER:
                LayoutInflater inflater = (LayoutInflater) parent.getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                view = inflater.inflate(R.layout.payment_screen_parent, parent, false);
                ListHeaderViewHolder header = new ListHeaderViewHolder(view);
                return header;
            case CHILD:
                LayoutInflater inflater1 = (LayoutInflater) parent.getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                view = inflater1.inflate(R.layout.payment_screen_child, parent, false);
                ListChildViewHolder child = new ListChildViewHolder(view);
                return child;
        }
        return null;
    }

    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        final Payments cl = data.get(position);
        switch (cl.getNodetype()) {
            case HEADER:
                final ListHeaderViewHolder h = (ListHeaderViewHolder) holder;
                h.refferalItem = cl;

                currency = appPreference.getDefaultCurrency() ;
                currency = currency != null ? currency : "Tsh";
                String locsaleid = cl.getLocalsale();

                final boolean haschild = Payments.anyChild(context, locsaleid);
                String saleref = Sales.getSaleRef(context, locsaleid);
                double deb = Sales.getDebt(context, locsaleid);

                String paydate = Sales.getSaleDate(context, locsaleid);
                paydate = Constants.getFormatedDate(paydate);
                String balance = String.valueOf(deb);
                String amount = Sales.getPayerableAmount(context,locsaleid);

                String due = context.getString(R.string.strdebt);
                String pd = context.getString(R.string.strsaledate);
                String srf = context.getString(R.string.strsaleref);
                String bl = context.getString(R.string.strpayableamount);

                String custmerid = Sales.getCustomerLocal(context, locsaleid);
                String customer = Customers.getCustomerBusinessNameLoca(context, custmerid);

                h.txtdebt.setText(due+" : " +balance+" " +currency);
                h.txtcust.setText(customer.toUpperCase());

                h.txtdate.setText(pd+" : " +paydate );
                h.txtsaleref.setText(srf + " : " + saleref);
                h.txtpaid.setText(bl+" : " +amount+" " +currency);

                changeBg(h,new BigDecimal(balance).intValue());
                final Drawable minus = context.getResources().getDrawable(R.drawable.circle_minus);
                final Drawable plus = context.getResources().getDrawable(R.drawable.circle_plus);
                if(haschild)
                if (cl.invisibleChildren == null) {
                    h.txtdebt.setCompoundDrawablesWithIntrinsicBounds(null, null, minus, null);
                } else {
                    h.txtdebt.setCompoundDrawablesWithIntrinsicBounds(null, null, plus, null);
                }
                h.layview.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if (haschild) {
                            if (cl.invisibleChildren == null) {
                                cl.invisibleChildren = new ArrayList<Payments>();
                                int count = 0;
                                int pos = data.indexOf(h.refferalItem);
                                while (data.size() > pos + 1 && data.get(pos + 1).getNodetype() == CHILD) {
                                    cl.invisibleChildren.add(data.remove(pos + 1));
                                    count++;
                                }
                                notifyItemRangeRemoved(pos + 1, count);
                                h.txtdebt.setCompoundDrawablesWithIntrinsicBounds(null, null, plus, null);
                            } else {
                                int pos = data.indexOf(h.refferalItem);
                                int index = pos + 1;
                                for (Payments i : cl.invisibleChildren) {
                                    data.add(index, i);
                                    index++;
                                }
                                notifyItemRangeInserted(pos + 1, index - pos - 1);
                                h.txtdebt.setCompoundDrawablesWithIntrinsicBounds(null, null, minus, null);
                                cl.invisibleChildren = null;
                            }
                        }
                    }
                });
                break;
            case CHILD:

                Payments c = data.get(position);
                final ListChildViewHolder hh = (ListChildViewHolder) holder;

                locsaleid = c.getLocalsale();
                saleref = Sales.getSaleRef(context, locsaleid);
                deb = Sales.getDebt(context, locsaleid);
                amount = c.getAmount();

                paydate = c.getPaydate();
                paydate = Constants.getFormatedDate(paydate);
                balance = cl.getPostbalance();

                bl = context.getString(R.string.strpaid);
                due = context.getString(R.string.strdebt);
                pd = context.getString(R.string.strpaydate);
                srf = context.getString(R.string.strsaleref);

                custmerid = Sales.getCustomerLocal(context, locsaleid);
                customer = Customers.getCustomerBusinessNameLoca(context, custmerid);

                hh.txtdebt.setText(due+" : " +balance+" " +currency);
                hh.txtcust.setText(customer.toUpperCase());

                hh.txtdate.setText(pd+" : " +paydate );
                hh.txtsaleref.setText(srf+" : " +saleref );
                hh.txtpaid.setText(bl+" : " +amount+" " +currency);

                break;
        }
    }

    @Override
    public int getItemViewType(int position) {
        return data.get(position).getNodetype();
    }

    @Override
    public int getItemCount() {
        return data.size();
    }

    private static class ListHeaderViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
//        public ImageView btn_expand_toggle;
        public Payments refferalItem;
        private LinearLayout layview;
        private TextView txtcust,txtpaid,txtdebt,txtdate,txtsaleref ;

        public ListHeaderViewHolder(View v) {
            super(v);
            layview = (LinearLayout)v.findViewById(R.id.view_lay);
            txtcust = (TextView)v.findViewById(R.id.txtcustomer);
            txtdate = (TextView)v.findViewById(R.id.txtsaledate);
            txtpaid = (TextView)v.findViewById(R.id.txtpaid);
            txtdebt = (TextView)v.findViewById(R.id.txtunpaid);
            txtsaleref = (TextView)v.findViewById(R.id.txtsaleref);
        }
        @Override
        public void onClick(View v) {
            try {
                clickListener.onItemClick(v, getLayoutPosition());
            } catch (NullPointerException e){

            }
        }
    }
    private static class ListChildViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {         public Payments refferalItem;
        private LinearLayout layview;
        private TextView txtcust,txtpaid,txtdebt,txtdate,txtsaleref ;

        public ListChildViewHolder(View v) {
            super(v);
            layview = (LinearLayout)v.findViewById(R.id.view_lay);
            txtcust = (TextView)v.findViewById(R.id.txtcustomer);
            txtdate = (TextView)v.findViewById(R.id.txtsaledate);
            txtpaid = (TextView)v.findViewById(R.id.txtpaid);
            txtdebt = (TextView)v.findViewById(R.id.txtunpaid);
            txtsaleref = (TextView)v.findViewById(R.id.txtsaleref);
        }
        @Override
        public void onClick(View v) {
            try {
                clickListener.onItemClick(v, getLayoutPosition());
            } catch (NullPointerException e){

            }
        }
    }

    public interface OnItemClickListener {
        public void onItemClick(View view, int position);
    }

    public void SetOnItemClickListener(final OnItemClickListener itemClickListener) {
        clickListener = itemClickListener;
    }

    private void changeBg(ListHeaderViewHolder h,int qty){
        if(qty == 0){
            h.txtdebt.setTextColor(context.getResources().getColor(R.color.clrproduct));
        } else {
            h.txtdebt.setTextColor(context.getResources().getColor(R.color.clrpending));
        }
    }
}
