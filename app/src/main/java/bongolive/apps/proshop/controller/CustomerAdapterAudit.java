package bongolive.apps.proshop.controller;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import java.util.ArrayList;

/**
 * Created by nasznjoka on 11/24/15.
 */
public class CustomerAdapterAudit extends ArrayAdapter<String> {
    long audit;

    public CustomerAdapterAudit(Context context, ArrayList<String> objects) {
        super(context,0, objects);
    }
    @Override
        public View getView(int position, View convertView, ViewGroup parent) {
        TextView text = (TextView) convertView;

        if (text == null) {
            text = (TextView) LayoutInflater.from(getContext()).inflate(android.R.layout.simple_spinner_dropdown_item, parent, false);
        }
        String prod = getItem(position);
        text.setText(prod);

        return text;
    }

        @Override
        public View getDropDownView(int position, View convertView, ViewGroup parent) {
            TextView text = (TextView) convertView;

            if (text == null) {
                text = (TextView) LayoutInflater.from(getContext()).inflate(android.R.layout.simple_spinner_dropdown_item,  parent, false);
            }

            String prod = getItem(position);
            text.setText(prod);

            return text;
        }
}
