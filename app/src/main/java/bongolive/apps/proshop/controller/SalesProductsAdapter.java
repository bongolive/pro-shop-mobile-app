/*
 * The source codes in this project are owned by Bongolive Enterprises. Copying or any kind of usage without Bongolive's approval is against the law.
 * Copyright (c) 2015.
 *
 */

package bongolive.apps.proshop.controller;


import android.app.Activity;
import android.content.Intent;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

import bongolive.apps.proshop.R;
import bongolive.apps.proshop.model.Customers;
import bongolive.apps.proshop.model.Pending_Items;
import bongolive.apps.proshop.model.Products;
import bongolive.apps.proshop.model.Settings;
import bongolive.apps.proshop.model.Warehouse;
import bongolive.apps.proshop.view.Dashboard;
import bongolive.apps.proshop.view.SalesProductsItems;
import bongolive.apps.proshop.view.SalesSummary;
import bongolive.apps.proshop.view.SingeItem;

/**
 * Created by nasznjoka on 5/18/15.
 */

public class SalesProductsAdapter extends RecyclerView.Adapter<SalesProductsAdapter.Vh>
        implements TextWatcher,Filterable,View.OnClickListener,AdapterView.OnItemClickListener {

    public List<SalesProductsItems> mItems = new ArrayList<SalesProductsItems>();
    public static List<SalesProductsItems> filteredIt = new ArrayList<SalesProductsItems>();
    RecyclerView mRecyclerView;
    Activity context;
    AppPreference appPreference;
    private EditText etfilter;
    private TextView txtt,txti;
    private ImageView imgrmv,imgnext;
    private RelativeLayout rl ;
    private int items = 0;
    private double gtotal = 0;
    private ArrayList<Pending_Items> pitems = new ArrayList<>();
    String prodname = null;
    String currency = null;
    private String saletype;
    final int[] qty = {0};
    final double[] unit = {0} ;
    final double[] subtotal = {0};

    final String[] s_qty = {"0"};
    final String[] s_subtotal = {"0"};
    String s_unit = "";
    private boolean __taxsettings = false;
    private String[] businesslist;
    private ArrayAdapter<String> custadapters;
    private AutoCompleteTextView bzn;
    private String customeridLocal = "0";
    private String customerid = "0";
    protected static final int ADD_STATUS_FULL = 3;
    protected static final int ADD_STATUS_GONE = 0;
    protected static final int ADD_STATUS_SELECTED = 2;
    protected static final int ADD_STATUS_UNSELECTED = 1;
    private static final int STATE_REGULAR_CELL = 2;
    private static int STATE_SECTIONED_CELL ;


    @Override
    public void beforeTextChanged(CharSequence s, int start, int count, int after) {

    }

    @Override
    public void onTextChanged(CharSequence s, int start, int before, int count) {
        getFilter().filter(s.toString());
    }

    @Override
    public void afterTextChanged(Editable s) {
    }

    @Override
    public Filter getFilter() {
        return new ProductsFilter(this,mItems);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.imgrmvtotal:
                appPreference = new AppPreference(context);
                items = 0;
                gtotal = 0;
                pitems.clear();
                Pending_Items.purgeItems(context, Constants.SALETYPE_SALE);
                Intent intent = new Intent(context,Dashboard.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                context.startActivity(intent);
                context.finish();
                break;
            case R.id.imgnext:
                if(customeridLocal.equals("0") && bzn.getVisibility() == View.GONE){
                    bzn.setVisibility(View.VISIBLE);
                    return;
                } else {
                    String busn = bzn.getText().toString();
                    if (Validating.areSet(busn)) {
                        if (customeridLocal.equals("0")) {
                            boolean added = addupdatecustomer(bzn);
                            if (added) {
                                System.out.println("added and id is " + customeridLocal);
                            }
                            if(!customeridLocal.equals("0")){
                                appPreference.saveCustomer(Long.valueOf(customeridLocal));
                                Intent intent1 = new Intent(context, SalesSummary.class);
                                context.startActivity(intent1);
                            }
                        }
                    } else {
                        String bznm = Customers.getDefaultCustomer(context);
                        customerid = String.valueOf(Customers.getCustomerid(context, bznm));
                        customeridLocal = String.valueOf(Customers.getCustomerlocalId(context, bznm));
                        if(!customeridLocal.equals("0")){
                            appPreference.saveCustomer(Long.valueOf(customeridLocal));
                            Intent intent1 = new Intent(context, SalesSummary.class);
                            context.startActivity(intent1);
                        }
                    }
                }
                break;
        }
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        String name = (String) parent.getItemAtPosition(position);
        bzn.setText(name);
        customerid = String.valueOf(Customers.getCustomerid(context, name));
        customeridLocal = String.valueOf(Customers.getCustomerlocalId(context, name));
    }

    public static class Vh extends RecyclerView.ViewHolder {
        private LinearLayout layactions;
        private LinearLayout layview;
        private Button imgadd,imgminus;
        private TextView txtproduct,txtsubtotal,txtunit,txtqty ;

        public Vh(View v) {
            super(v);
            layactions = (LinearLayout)v.findViewById(R.id.actions_lay);
            layview = (LinearLayout)v.findViewById(R.id.view_lay);
            imgadd = (Button)v.findViewById(R.id.action_plus);
            imgminus = (Button)v.findViewById(R.id.action_minus);
            txtqty = (TextView)v.findViewById(R.id.action_view);
            txtproduct = (TextView)v.findViewById(R.id.action_product);
            txtsubtotal = (TextView)v.findViewById(R.id.action_total);
            txtunit = (TextView)v.findViewById(R.id.action_unit);
        }
    }

    public SalesProductsAdapter(Activity mcontext, ArrayList<SalesProductsItems> myDataset,
                                RecyclerView rv, EditText filter,RelativeLayout rl_,String source,
                                ImageView nxt,AutoCompleteTextView bzn) {
        mItems = myDataset;
        filteredIt = mItems;
        context = mcontext;
        mRecyclerView = rv;
        etfilter = filter;
        etfilter.addTextChangedListener(this);
        rl = rl_;
        txtt = (TextView)rl.getChildAt(0);
        txti = (TextView)rl.getChildAt(1);
        imgrmv = (ImageView)rl.getChildAt(2);
        imgnext = nxt;
        imgrmv.setOnClickListener(this);
        imgnext.setOnClickListener(this);
        pitems = Pending_Items.getItems(context, source);
        saletype = source;
        appPreference = new AppPreference(context);
        __taxsettings = Settings.getTaxEnabled(context);
        currency = appPreference.getDefaultCurrency();
        businesslist = Customers.getAllCustomers(context);
        custadapters = new ArrayAdapter<>(context, android.R.layout.select_dialog_item, businesslist);
        this.bzn = bzn;
        bzn.setAdapter(custadapters);
        bzn.setThreshold(1);
        bzn.setOnItemClickListener(this);
    }

    // Create new views (invoked by the layout manager)
    @Override
    public Vh onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.selling_screen_sellitems, parent, false);

        Vh vh = new Vh(v);
        return vh;
    }

    @Override
    public void onBindViewHolder(final Vh holder, final int position) {
        final SalesProductsItems cl = filteredIt.get(position);
        if(pitems != null && pitems.size() > 0){
            for(int j = 0; j < pitems.size(); j++){
                Pending_Items _pitems = pitems.get(j);
                long proid = cl.getProdid();
                long _proid = _pitems.getProdloc();
                if(proid == _proid){
                    prodname = cl.getProduct();
                    qty[0] = new BigDecimal(_pitems.getQty()).intValue();
                    unit[0] = _pitems.getPrice();
                } else {
                    prodname = cl.getProduct();
                    qty[0] = new BigDecimal(cl.getQuantity()).intValue();
                    unit[0] = cl.getUnit();
                }
            }
        } else {
            prodname = cl.getProduct();
            qty[0] = new BigDecimal(cl.getQuantity()).intValue();
            unit[0] = cl.getUnit();
        }


        items = pitems.size();
        if(items > 0)
            imgnext.setVisibility(View.VISIBLE);
        else
            imgnext.setVisibility(View.GONE);

        holder.txtproduct.setText(prodname);
        updateViews(qty[0], holder);

        if(qty[0] > 0){
            System.out.println(" qty is " + s_qty);
            holder.imgminus.setVisibility(View.GONE);
            holder.imgadd.setVisibility(View.GONE);
            holder.txtqty.setText(s_qty[0]);
            holder.txtunit.setText(s_unit);
            holder.txtsubtotal.setText(s_subtotal[0]);
        } else {
            holder.imgminus.setVisibility(View.GONE);
            holder.txtqty.setVisibility(View.GONE);
            holder.txtunit.setText(s_unit);
            holder.txtsubtotal.setVisibility(View.GONE);
        }

        checkItemPos(position);
        holder.imgadd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                qty[0] += 1;
                if(Products.checkQty(context,cl.getProdid(),qty[0])) {
                    if (qty[0] == 1) {
                        s_qty[0] = String.valueOf(qty[0]);

                        holder.txtqty.setText(s_qty[0]);
                        holder.txtqty.setVisibility(View.VISIBLE);
                        holder.imgminus.setVisibility(View.VISIBLE);

                        process_item(cl, holder,position);
                        changeBg(holder,qty[0]);

                        holder.txtsubtotal.setVisibility(View.VISIBLE);
                    } else {
                        s_qty[0] = String.valueOf(qty[0]);
                        holder.txtqty.setText(s_qty[0]);
                        process_item(cl, holder,position);
                        holder.txtsubtotal.setVisibility(View.VISIBLE);
                    }
                } else {
                    Toast.makeText(context,cl.getProduct() + " " + context.getResources().getString(R.string
                            .strsalesquantityerror1) +
                            " " + Products.getItemOnHand(context,String.valueOf(cl.getProdid())) + " " + context.getResources()
                            .getString(R.string.strsalesquantityerror2), Toast.LENGTH_LONG).show();
                }
            }
        });

        holder.layview.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(holder.imgminus.getVisibility() == View.VISIBLE){
                    holder.imgminus.setVisibility(View.GONE);
                    if(qty[0] == 0) {
                        holder.imgadd.setVisibility(View.VISIBLE);
                        holder.txtqty.setVisibility(View.GONE);
                    }
                    else {
                        holder.txtqty.setVisibility(View.VISIBLE);
                        holder.imgadd.setVisibility(View.GONE);
                        holder.txtqty.setText(s_qty[0]);
                    }
                } else {
                    Intent intent = new Intent(context,SingeItem.class);
                    intent.putExtra(Constants.SOURCE,Constants.SALETYPE_SALE);
                    intent.putExtra(Products.PRODUCTNAME,cl.getProdid());
                    intent.putExtra(Products.ID, position);
                    context.startActivity(intent);
                }
            }
        });

        holder.txtqty.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(holder.imgadd.getVisibility() == View.GONE){
                    holder.txtqty.setText(s_qty[0]);
                    holder.imgadd.setVisibility(View.VISIBLE);
                    holder.imgminus.setVisibility(View.VISIBLE);
                }
            }
        });

    }

    // Return the size of your dataset (invoked by the layout manager)
    @Override
    public int getItemCount() {
        return filteredIt.size();
    }

    @Override
    public long getItemId(int arg0){
        return 0;
    }

    private void process_item(SalesProductsItems cl,Vh holder, int pos) {
        String locprod = String.valueOf(cl.getProdid());
        String prodno = Products.getSysProdidBasedOnLocalId(context, locprod);
        String whid = Warehouse.getDefaultId(context);

        int q =  new BigDecimal(qty[0]).intValue();
        int taxmethod = Products.getTaxmethod(context, cl.getProdid());
        double tx = Products.getTax(context, cl.getProdid());
        double pr = Products.getUnitPrice(context, cl.getProdid());
        double maxdis = Settings.getDiscountLimit(context);
        double taxamount = 0;
        double netsaleprice = 0;
        double saleprice = 0;
        double subtotal = 0;
        long prodlocal = cl.getProdid();

        long i = 0;
        double tax = Constants.itemtax(pr, tx, taxmethod, __taxsettings);
        netsaleprice = Constants.netprice(pr, tx, taxmethod, __taxsettings);
        saleprice = Constants.saleprice(pr, tax, taxmethod, __taxsettings);

        subtotal = Constants.calculate_grandtotal(saleprice, q);
        double netsubtotal = Constants.calculate_grandtotal(netsaleprice, q);
        taxamount = new BigDecimal(subtotal).subtract(new BigDecimal(netsubtotal)).setScale(2,
                RoundingMode.HALF_UP).doubleValue();

        String spr = String.valueOf(saleprice);
        String nspr = String.valueOf(netsaleprice);
        String txa = String.valueOf(taxamount);
        String stxr = String.valueOf(tx);

        String[] vals = {prodno,locprod,s_qty[0],nspr,spr,txa,String.valueOf(subtotal),whid,"0",stxr,Constants
                .SALETYPE_SALE,String.valueOf(pos)};
        if(Validating.areSet(vals)){
            if(!Pending_Items.isProdIdExist(context,new String[]{vals[1],vals[10]})){
                boolean insert = Pending_Items.insertItems(context,vals);
                pitems = Pending_Items.getItems(context,Constants.SALETYPE_SALE);
                items = pitems.size();
                updateViews(qty[0],holder);
            } else {
                boolean insert = Pending_Items.updateItems(context, vals);
                pitems = Pending_Items.getItems(context,Constants.SALETYPE_SALE);
                items = pitems.size();
                updateViews(qty[0],holder);
            }
        }
    }
    private static class ProductsFilter extends Filter {

        private final SalesProductsAdapter adapter;

        private final List<SalesProductsItems> originalList;

        private final List<SalesProductsItems> filteredList;

        private ProductsFilter(SalesProductsAdapter adapter, List<SalesProductsItems> originalList) {
            super();
            this.adapter = adapter;
            this.originalList = new LinkedList<>(originalList);
            this.filteredList = new ArrayList<>();
        }

        @Override
        protected FilterResults performFiltering(CharSequence constraint) {
            filteredList.clear();
            final FilterResults results = new FilterResults();

            if (constraint == null || constraint.length() == 0)
                filteredList.addAll(originalList);
            else {
                final String filterPattern = constraint.toString().toLowerCase().trim();

                for (final SalesProductsItems it : originalList) {

                    if (it.getProduct().toLowerCase().contains(filterPattern))
                        filteredList.add(it);
                    else if (it.getSku().matches(constraint.toString()))
                        filteredList.add(it);
                }
            }

            results.values = filteredList;
            results.count = filteredList.size();
            return results;
        }


        @Override
        protected void publishResults(CharSequence constraint, FilterResults results) {
            adapter.mItems = originalList;
            if(results.count > 0) {
                filteredIt.clear();
                filteredIt.addAll((ArrayList<SalesProductsItems>) results.values);
                adapter.notifyDataSetChanged();
            } else {
                filteredIt.clear();
                filteredIt.addAll(originalList);
                adapter.notifyDataSetChanged();
            }
        }
    }
    private void checkItemPos(int pos){
        LinearLayoutManager layoutManager = ((LinearLayoutManager)mRecyclerView.getLayoutManager());
        int firstVisiblePosition = layoutManager.findFirstVisibleItemPosition();
        int lastvisible = layoutManager.findLastVisibleItemPosition();
        System.out.println(" first visible item "+lastvisible);
    }

    private void updateViews(int qt, Vh holder){
        double un = unit[0];
        double st = new BigDecimal(qt).multiply(new BigDecimal(un)).doubleValue();
        s_qty[0] = String.valueOf(qt);
        s_subtotal[0] =  new BigDecimal(st).setScale(2, RoundingMode.HALF_UP).toString();

        s_subtotal[0] = currency + " " + new BigDecimal(subtotal[0]).setScale(2, RoundingMode.HALF_UP).toString();
        s_unit = currency+" "+new BigDecimal(unit[0]).setScale(2, RoundingMode.HALF_UP).toString();

        holder.txtsubtotal.setText(s_subtotal[0]);
        holder.txtunit.setText(s_unit);
        holder.txtsubtotal.setVisibility(View.VISIBLE);
        if(qt > 0)
            imgnext.setVisibility(View.VISIBLE);
        else
            imgnext.setVisibility(View.GONE);

        gtotal += subtotal[0];
        txtt.setText(currency + " " + new BigDecimal(gtotal).setScale(2, RoundingMode.HALF_UP).toString());
        txti.setText("/ " + items + " " + context.getString(R.string.stritems));
    }

    private void changeBg(Vh h,int qty){
        if(qty > 0){
            h.imgadd.setBackgroundColor(context.getResources().getColor(R.color.clrproduct));
            h.imgadd.setTextColor(context.getResources().getColor(R.color.white));
        } else {
            h.imgadd.setBackgroundColor(context.getResources().getColor(R.color.gray_back));
            h.imgadd.setTextColor(context.getResources().getColor(R.color.clrproduct));
        }
    }

    private boolean addupdatecustomer(AutoCompleteTextView etclint) {
        boolean bool = false;
        String strclname,strshname,strclemail,strclphone,strcladdress,svrn,stin ;
        strclname = etclint.getText().toString().trim();
        strshname = strclname;
        strclemail = "";
        strclphone = "0008833";
        strcladdress = "Dar es salaam";
        svrn ="";
        stin = "";

        boolean businessexist = Customers.isCustomerValid(context, strclname);
        String[] input = new String[]{strclname,strshname,strclphone,strclemail,strcladdress,svrn,stin} ;
        if (!businessexist) {
            int insert = Customers.insertCustomerLocal(context,input);
            if (insert == 1) {
                customerid = String.valueOf(Customers.getCustomerid(context, strshname));
                customeridLocal = String.valueOf(Customers.getCustomerlocalId(context, strshname));
                bool = true;
            }
        } else {
            customerid = String.valueOf(Customers.getCustomerid(context, strshname));
            customeridLocal = String.valueOf(Customers.getCustomerlocalId(context, strshname));
            bool = true;
        }
        return bool;
    }
    public void setSelected(int position) {
        STATE_SECTIONED_CELL = position;
    }
}