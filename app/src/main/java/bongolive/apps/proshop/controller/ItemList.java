/*
 * Copyright (c) 2015. All rights reserved. The source codes are property of bongolive
 */

package bongolive.apps.proshop.controller;

/**
 * @author nasznjoka
 *         <p/>
 *         Oct 9, 2014
 */
public class ItemList {
    String itemname;
    String itemname1;
    String itemname2;
    String itemname3;
    String itemname4;
    long id;

    long auditid;
    String title,desc,date;
    long bus;


    public ItemList(long id, long auditid, String title, String desc, String date, long bus ) {
        this.id = id;
        this.auditid = auditid;
        this.title = title;
        this.desc = desc;
        this.date = date;
        this.bus = bus;
    }

    public ItemList(long id,String cust) {
        this.id = id;
        this.title = cust;
    }

    public String getDate() {
        return date;
    }

    public String getDesc() {
        return desc;
    }

    public long getBus() {
        return bus;
    }
    public String getTitle() {
        return title;
    }

    public long getAuditid() {
        return auditid;
    }

    String buss,saleref,delref,deldate,deladd;
    long delid,saleid;

    /*public ItemList(String n, String p, long i, String a){
        itemname = n;
        itemname1  = p ;
        itemname2 = a ;
        id = i ;
    }*/
    public ItemList(String n, String pn, String p, long i, String a) {
        itemname = n;
        itemname3 = pn;
        itemname1 = p;
        itemname2 = a;
        id = i;
    }

    public ItemList(String n, String pn, String p, String a, long i) {
        itemname = n;
        itemname1 = pn;
        itemname2 = p;
        itemname3 = a;
        id = i;
    }

    public ItemList(String n, String pn, String p, long i, String a, String ex,long si) {
        saleref = n;
        buss = pn;
        delref = p;
        deldate = a;
        id = i;
        deladd = ex;
        saleid = si;
    }
    public ItemList(String n, String pn, String p, long i, String a, String ex) {
        itemname = n;
        itemname3 = pn;
        itemname1 = p;
        itemname2 = a;
        id = i;
        itemname4 = ex;
    }

    public String getBuss() {
        return buss;
    }

    public String getSaleref() {
        return saleref;
    }

    public String getDelref() {
        return delref;
    }

    public String getDeldate() {
        return deldate;
    }

    public String getDeladd() {
        return deladd;
    }

    public long getDelid() {
        return delid;
    }

    public long getSaleid() {
        return saleid;
    }

    public String getitemname() {
        return itemname;
    }

    public String getitemname1() {
        return itemname1;
    }

    public String getitemname3() {
        return itemname3;
    }

    public String getitemname4() {
        return itemname4;
    }

    public long getId() {
        return id;
    }

    public String getitemname2() {
        return itemname2;
    }

}
