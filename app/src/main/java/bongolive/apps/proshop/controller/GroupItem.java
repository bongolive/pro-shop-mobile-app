/*
 * Copyright (c) 2015. All rights reserved. The source codes are property of bongolive
 */

package bongolive.apps.proshop.controller;

import java.util.ArrayList;

/**
 * @author nasznjoka
 *         <p/>
 *         Oct 9, 2014
 */
public class GroupItem {

        private String Name;
        private ArrayList<ChildItem> Items;

        public String getName() {
            return Name;
        }

        public void setName(String name) {
            this.Name = name;
        }

        public ArrayList<ChildItem> getItems() {
            return Items;
        }

        public void setItems(ArrayList<ChildItem> Items) {
            this.Items = Items;
        }

    }