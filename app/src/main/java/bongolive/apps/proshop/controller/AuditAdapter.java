/*
 * The source codes in this project are owned by Bongolive Enterprises. Copying or any kind of usage without Bongolive's approval is against the law.
 * Copyright (c) 2015.
 *
 */

package bongolive.apps.proshop.controller;


import android.app.Activity;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import bongolive.apps.proshop.R;
import bongolive.apps.proshop.model.Audit;

/**
 * Created by nasznjoka on 5/18/15.
 */

public class AuditAdapter extends RecyclerView.Adapter<AuditAdapter.Vh> {

    public List<Audit> mItems = new ArrayList<Audit>();
    Activity context;
    AppPreference appPreference;
    static OnItemClickListener clickListener ;
    private boolean tabletSize ;

    public static class Vh extends RecyclerView.ViewHolder implements View.OnClickListener {
        private LinearLayout layview;
        private TextView txtcust,txtpaid,txtdebt,txtdate ;

        public Vh(View v) {
            super(v);
            layview = (LinearLayout)v.findViewById(R.id.view_lay);
            txtcust = (TextView)v.findViewById(R.id.txtcustomer);
            txtcust.setTextSize(15);
            txtdate = (TextView)v.findViewById(R.id.txtsaledate);
            txtdate.setVisibility(View.GONE);
            txtpaid = (TextView)v.findViewById(R.id.txtpaid);
            txtdebt = (TextView)v.findViewById(R.id.txtunpaid);
        }

        @Override
        public void onClick(View v) {
            try {
                clickListener.onItemClick(v, getLayoutPosition());
            } catch (NullPointerException e){

            }
        }
    }

    public AuditAdapter(Activity mcontext, ArrayList<Audit> myDataset, boolean tabletSize){
        mItems = myDataset;
        context = mcontext;
        appPreference = new AppPreference(context);
        this.tabletSize = tabletSize;
    }

    // Create new views (invoked by the layout manager)
    @Override
    public Vh onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.payment_screen_items, parent, false);

        Vh vh = new Vh(v);
        return vh;
    }

    @Override
    public void onBindViewHolder(final Vh holder, final int position) {
        final Audit cl = mItems.get(position);
        String title = cl.getName();
        String date = cl.getDate();
        date = Constants.getFormatedDate(date);
        String desc = cl.getDesc();

        String tt = context.getString(R.string.straudittitle);
        String due = context.getString(R.string.strcreated);
        String ds = context.getString(R.string.strprodesc);
        if(tabletSize){
            holder.txtdebt.setText(desc);
            holder.txtcust.setText(title.toUpperCase());
            holder.txtpaid.setText(date);
        } else {
            holder.txtdebt.setText(ds + " : " + desc);
            holder.txtcust.setText(tt.toUpperCase() + " : " + title.toUpperCase());
            holder.txtpaid.setText(due + " : " + date);
        }
        changeBg(holder);

    }

    // Return the size of your dataset (invoked by the layout manager)
    @Override
    public int getItemCount() {
        return mItems.size();
    }

    @Override
    public long getItemId(int arg0){
        return 0;
    }

    private void changeBg(Vh h){
            h.txtcust.setTextColor(context.getResources().getColor(R.color.clrproduct));
    }

    public interface OnItemClickListener {
        public void onItemClick(View view, int position);
    }

    public void SetOnItemClickListener(final OnItemClickListener itemClickListener) {
        this.clickListener = itemClickListener;
    }
}