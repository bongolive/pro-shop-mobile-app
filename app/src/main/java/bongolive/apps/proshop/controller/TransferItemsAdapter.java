/*
 * The source codes in this project are owned by Bongolive Enterprises. Copying or any kind of usage without Bongolive's approval is against the law.
 * Copyright (c) 2015.
 *
 */

package bongolive.apps.proshop.controller;


import android.app.Activity;
import android.graphics.Color;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.ArrayList;
import java.util.List;

import bongolive.apps.proshop.R;
import bongolive.apps.proshop.model.Products;
import bongolive.apps.proshop.model.StockIssueItems;
import bongolive.apps.proshop.model.TransferItems;

/**
 * Created by nasznjoka on 5/18/15.
 */

public class TransferItemsAdapter extends RecyclerView.Adapter<TransferItemsAdapter.Vh> {

    public List<TransferItems> mItems = new ArrayList<TransferItems>();
    public List<StockIssueItems> mItems_action = new ArrayList<StockIssueItems>();
    Activity context;
    AppPreference appPreference;
    String currency = null;
    static OnItemClickListener clickListener ;
    String action;

    public static class Vh extends RecyclerView.ViewHolder implements View.OnClickListener {
        private LinearLayout layview;
        private TextView txtproduct,txtgtotal,txtdate,txtstatus,txtqty ;

        public Vh(View v) {
            super(v);
            layview = (LinearLayout)v.findViewById(R.id.view_lay);
            txtproduct = (TextView)v.findViewById(R.id.txtproductorwh);
            txtstatus = (TextView)v.findViewById(R.id.txtstatus);
            txtgtotal = (TextView)v.findViewById(R.id.txtgrandtotal);
            txtqty = (TextView)v.findViewById(R.id.txtsaleref);
            txtdate = (TextView)v.findViewById(R.id.txttransdate);
            txtdate.setVisibility(View.GONE);
        }

        @Override
        public void onClick(View v) {
            try {
                clickListener.onItemClick(v, getLayoutPosition());
            } catch (NullPointerException e){

            }
        }
    }

    public TransferItemsAdapter(Activity mcontext, ArrayList<TransferItems> myDataset){
        mItems = myDataset;
        context = mcontext;
        appPreference = new AppPreference(context);
        currency = appPreference.getDefaultCurrency();
        this.action = null;
    }

    public TransferItemsAdapter(Activity mcontext, ArrayList<StockIssueItems> myDataset, String action){
        mItems_action = myDataset;
        context = mcontext;
        appPreference = new AppPreference(context);
        currency = appPreference.getDefaultCurrency();
        this.action = action;
    }

    // Create new views (invoked by the layout manager)
    @Override
    public Vh onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.transfer_adapter_screen, parent, false);

        Vh vh = new Vh(v);
        return vh;
    }
    //product,status,quantity,sum
    @Override
    public void onBindViewHolder(final Vh holder, final int position) {
        if(action != null) {
            final StockIssueItems cl = mItems_action.get(position);
            currency = appPreference.getDefaultCurrency();
            currency = currency != null ? currency : "Tsh";
            long saleid = cl.getOrderid();
            long locsaleid = cl.getId();
            int qty = cl.getQty();
            int ack = cl.getAck();
            long prod = cl.getProdloc();
            String product = Products.getProdName(context,String.valueOf(prod));

            String status = null;

            if (ack == 0) {
                status = context.getString(R.string.strnotsent);
                holder.txtstatus.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.ic_rejected, 0);
                holder.txtstatus.setTextColor(Color.parseColor("#fb0000"));
            } else if (ack == 1) {
                status = context.getString(R.string.strsynced);
                holder.txtstatus.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.ic_done, 0);
                holder.txtstatus.setTextColor(Color.parseColor("#669900"));
            }

            String bl = context.getString(R.string.strtotalvals);
            String pd = context.getString(R.string.strquantity);
            String srf = context.getString(R.string.strstatus);

            holder.txtproduct.setText(product);
            holder.txtqty.setText(pd + " : " + qty);
            holder.txtstatus.setText(srf + " : " + status);
            holder.txtgtotal.setVisibility(View.GONE);
        } else {
            final TransferItems cl = mItems.get(position);
            currency = appPreference.getDefaultCurrency();
            currency = currency != null ? currency : "Tsh";
            String saleid = cl.getTransidl();
            long locsaleid = cl.getId();
            String qty = cl.getQtyverified();
            String amount = new BigDecimal(cl.getQtyverified()).multiply(new BigDecimal(cl.getUnit()))
                    .setScale(2, RoundingMode.HALF_UP).toString();
            String ack = cl.getStatus();
            int ack1 = cl.getAck();
            String status = null;

            if (ack.equals("2")) {
                status = context.getString(R.string.strrejected);
                holder.txtstatus.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.ic_rejected, 0);
                holder.txtstatus.setTextColor(Color.parseColor("#fb0000"));
            } else if (ack.equals("1")) {
                status = context.getString(R.string.strapproved);
                holder.txtstatus.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.ic_done, 0);
                holder.txtstatus.setTextColor(Color.parseColor("#669900"));
            } else if (ack.equals("0")) {
                if (ack1 == 0)
                    status = context.getString(R.string.strnotsent);
                else
                    status = context.getString(R.string.strwaitingapproval);
                holder.txtstatus.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.color.clrpending, 0);
                holder.txtstatus.setTextColor(Color.parseColor("#FA7306"));
            }

            String prod = cl.getProduct();
            prod = Products.getProdNameBasedSyid(context, prod);

            String bl = context.getString(R.string.strtotalvals);
            String pd = context.getString(R.string.strquantity);
            String srf = context.getString(R.string.strstatus);
            holder.txtproduct.setText(prod);

            holder.txtqty.setText(pd + " : " + qty);
            holder.txtstatus.setText(srf + " : " + status);
            holder.txtgtotal.setText(bl + " : " + amount + " " + currency);
        }
    }

    // Return the size of your dataset (invoked by the layout manager)
    @Override
    public int getItemCount() {
        if(action == null)
            return mItems.size();
        else
            return mItems_action.size();
    }

    @Override
    public long getItemId(int arg0){
        return 0;
    }

    private void changeBg(Vh h,int qty){
        if(qty == 0){
            h.txtdate.setTextColor(context.getResources().getColor(R.color.clrproduct));
        } else {
            h.txtdate.setTextColor(context.getResources().getColor(R.color.clrpending));
        }
    }

    public interface OnItemClickListener {
        public void onItemClick(View view, int position);
    }

    public void SetOnItemClickListener(final OnItemClickListener itemClickListener) {
        this.clickListener = itemClickListener;
    }
}