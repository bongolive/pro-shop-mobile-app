/*
 * Copyright (c) 2015.
 * Prosurvey is a property of Bongolive Enterprises Ltd. An authorized
 * copy of ideas, source codes is against the regulations
 */

package bongolive.apps.proshop.controller;

import android.accounts.Account;
import android.accounts.AccountManager;
import android.app.Activity;
import android.app.DatePickerDialog;
import android.app.TimePickerDialog;
import android.content.ContentResolver;
import android.content.Context;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.Rect;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Build;
import android.os.Environment;
import android.telephony.TelephonyManager;
import android.text.InputType;
import android.util.Base64;
import android.util.DisplayMetrics;
import android.util.Log;
import android.util.Xml;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.AutoCompleteTextView;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.TimePicker;

import org.xmlpull.v1.XmlSerializer;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.StringWriter;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.concurrent.TimeUnit;
import java.util.regex.Pattern;

import bongolive.apps.proshop.R;
import bongolive.apps.proshop.model.ContentProviderApi;
import bongolive.apps.proshop.model.Customers;
import bongolive.apps.proshop.model.Pending_Items;
import bongolive.apps.proshop.model.Products;
import bongolive.apps.proshop.model.SalesItems;
import bongolive.apps.proshop.model.Settings;
import bongolive.apps.proshop.model.User;

public class Constants {

    /**
     * Account type string.
     */
    public static final String ACCOUNT_TYPE = "proshop.bongolive.co.tz";
    public static final String DEVICE_ACCOUNTTYPE = "account_type";
    public static final String ACCOUNT = "ProShop";
//        	public static final String SERVER = "http://sales.pro.co.tz/api/service";//production
//    public static final String SERVER = "http://192.168.1.32/probaselocal/api/service";//developmentn
    public static final String SERVER = "http://www.pro.co.tz/probase/api/service";//developmentn
//        	public static final String SERVERAUTHENTICITY = "http://sales.pro.co.tz/api/service/authenticate";
//    public static final String SERVERAUTHENTICITY = "http://192.168.1.32/probaselocal/api/service/authenticate";//development
    public static final String SERVERAUTHENTICITY = "http://www.pro.co.tz/probase/api/service/authenticate";//development
    public static final String SYNCTRACK = "http://pro.co.tz/probase/sync.php";//sync tracker
    public static final String AUTHTOKEN = "auth_key";
    public static final String AUTHTOKENSERVER = "authentication_key";
    public static final String DEVICEACCOUNTTYPESERVER = "account";
    public static final String FIRSTRUN = "first_run";
    public static final String TAGREGISTRATION = "register_device";
    public static final String AUTHENTICATETAG = "authenticate";
    public static final String ACCOUNT_TYPE_NORMAL = "normal";
    public static final String ACCOUNT_TYPE_PREMIUM = "premium";
    public static final String LASTSYNC = "last_sync";
    public static final String FRESHSYNC = "current_sync";
    public static final String TAGSSENDPRODUCT = "upload_products";
    public static final String TAGSRECEIVEPRODUCT = "receive_products";
    public static final String SEARCHSOURCE = "search_initilizer";
    public static final String SEARCHSOURCE_PURCHASEITEMS = "purchase_items";
    public static final String SEARCHSOURCE_PRODUCTS = "products_fragment";
    public static final String SEARCHSOURCE_CUSTOMERS = "customers_fragment";
    public static final String SEARCHSOURCE_ORDERS = "orders_fragment";
    public static final String SEARCHSOURCE_MAIN = "dashboard";
    public static final String SEARCHSOURCE_HOME = "home_fragment";
    public static final String SEARCHSOURCE_STATION = "station_fragment";
    public static final String SEARCHSOURCE_AUDI = "audit_fragment";
    public static final String SALESBYCUSTOMER = "sales_customer";
    public static final String SALESBYPRODUCT = "sales_product";
    public static final String DELIVERYHISTORY = "delivery_report";
    public static final String STATIONHISTORY = "station_report";
    public static final String TAXBYPRODUCT = "tax_report";
    public static final String PRODUCTREORDER = "reorder_level";
    public static final String ERRORCODE = "code";
    public static final String REPORTS = "reports";
    public static final String MASTER_DIR = "Proshop_Reports";
    public static final String STOCKLEVEL = "stock_movement";
    public static final String BUSINESSNAME = "account_company_name";
    public static final String CONTACTP = "account_company_contactp";
    public static final String PHONE = "account_company_phone";
    public static final String KEY_DBPATH = "database_path";
    public static final String KEY_DB = "database_name";
    public static final CharSequence PREF_EXPORT_KEY = "pref_export_key";
    public static final CharSequence PREF_IMPORT_KEY = "pref_import_extenal_key";
    public static final String PREFPRODUCTSKU = "product_sku";
    public static final String SALEREPORT = "sales_report";
    public static final String ORDERREPORT = "order_report";
    public static final String PROFITREPORT = "profit_report";
    public static final String LOCAL = "local";
    public static final String REMOTE = "remote";
    public static final String TAGGETITEMS = "restore_sales_items";
    public static final String TAGSSENDSALEITEM = "upload_sale_items";
    public static final String TAGSSENDRETURNITEM = "upload_return_items";
    public static final String TAGSGETSALEITEM = "receive_sale_items";
    public static final String TAGSGETAUDIT = "receive_audit";
    public static final String TAGSSENDAUDIT = "upload_audit";
    public static final String TAGSGETXPENSE = "receive_expense";
    public static final String TAGSSENDXPENSE = "upload_expense";
    public static final String TAGSSENDPURCHASES = "upload_purchase_items";
    public static final String TAGSSENDTRANSFER = "upload_transfer_items";
    public static final String TAGSGETTRANSFER = "receive_transfer_items";
    public static final String TAGSGETPURCHASES = "receive_purchase_items";
    public static final String USERROLE = "user_role";
    public static final String USERID = "user_id";
    public static final String PREFBLUETOOTH = "bluetooth_enable_key";
    public static final String CLIENT = "CLIENT";
    public static final String TAGSRECEIVEDELIVERY = "receive_delivery";
    public static final String TAGSSENDDELIVERY = "upload_delivery";
    public static final String PREFLASTLAT = "last_known_lat";
    public static final String PREFLASTLNG = "last_known_lng";
    public static final String TAGSTOCKONCE = "assign_products";
    public static final String TAGSTOCKUPDATE = "assign_products";
    public static final String TAGSSENDSALES = "upload_sales";
    public static final String TAGSSENDSTOCKISUED = "upload_issued_stock";
    public static final String TAGSSENDSTOCKITEMSISSUED = "upload_issued_stock_items";
    public static final String TAGSSENDFORM = "upload_station";
    public static final String TAGSGETDFORM = "receive_station";
    public static final String TAGSRECEIVESALES = "receive_sales";
    public static final String TAGSITEM = "upload_sales_items";
    public static final String ACKITEM = "item";
    public static final String TAGSENDCUSTOMER = "upload_customers";
    public static final String TAGSENDPRODUCT = "upload_product";
    public static final String TAGSENDSTOCK = "upload_stock";
    public static final String TAGGETCUSTOMER = "assign_customers";
    public static final String TAGGETWH = "receive_warehouse";
    public static final String TAGGETCAT = "receive_product_cats";
    public static final String TAGSENDCAT = "upload_product_cats";
    public static final String TAGSENDPAYMENT = "upload_payment";
    public static final String TAGSETTINGS = "receive_account_settings";
    public static final String TAGLOCATIONUPDATES = "device_tracking";
    public static final String TAGMULTIMEDIA = "multi_media_data";
    public static final String TAGMULTIMEDIA_GET = "get_multi_media_data";
    public static final String DEFAULTTRACKINTERVAL = "5400000";
    public static final String SUCCESS = "success";
    public static final String FAILURE = "failure";
    public static final String MULT_IMAGE = "image";
    public static final String MULT_SOUND = "sound";
    public static final String MULT_VIDEO = "video";
    public static final String ACKVALUE = "1";
    public static final String ACK = "ack";
    public static final String TAGACK = "acktag";
    public static final String IMEI = "imei";
    public static final String ACKREFERENCE = "ref";
    public static final String ACKKEY = "key";
    public static final String PREFCURRENCY = "default_currency_key";
    public static final String PREFSYNC = "sync_frequency";
    public static final String PREFLANG = "default_language";
    public static final String PREFDISCOUNT = "enable_discount_key";
    public static final String PREFTAX = "include_vat_key";
    public static final String PREFADDPROD = "addprod_key";
    public static final String PREFEDITPRD = "editprod_key";
    public static final String PREFPOSTPAID = "postpaid_key";
    public static final String PREFADDCUST = "addcust_key";
    public static final String PREFREGISTER = "show_reregister_key";
    public static final String PREFDELIVERY = "include_delivery_key";
    public static final String PREFIMPORTSTOCK = "stockimport_key";
    public static final String PREFEDITCUST = "editcust_key";
    public static final String PREFUSER = "include_user_key";
    public static final String PREFRECEIPT = "print_recpt_key";
    public static final String PREFPIN = "pref_require_pin";
    public static final String PREFORDER = "include_order_key";
    public static final String PREFLIMITPRICE = "discount_limit_value";
    public static final String PREFADDRESS = "address";
    public static final String PREFALLOWPRINTWITHOUTSTANDINGPAYMENT = "print_recpt_with_nopayment";
    public static final String RCP = "RCP";
    public static final String TRANSACTION = "TRANSACTIONS";
    public static final String PAYMENTS = "PAYMENTS";
    public static final String TRN = "TRN";
    public static final String PMT = "PMT";//PAYMENTS
    public static final String TAX_GROUP = "TAX_GROUP";
    public static final String NAME = "NAME";
    public static final String SPRICE = "SPRICE";
    public static final String QTY = "QTY";
    public static final String MEAS_UNIT = "MEAS_UNIT";
    public static final String PAY_TYPE = "PAY_TYPE";
    public static final String PAY_AMOUNT = "PAY_AMOUNT";
    public static final String PDISCOUNT = "PDISCOUNT";
    public static final String NDISCOUNT = "NDISCOUNT";
    public static final String PMARKUP = "PMARKUP";
    public static final String CLIENT_DATA = "CLIENT_DATA";
    public static final String PAY_STATUS_CMPLT = "paid";
    public static final String PAY_STATUS_PNDG = "partial";
    public static final String SALE_STATUS_CMPLT = "completed";
    public static final String SALE_STATUS_PNDG = "pending";
    public static final String SALE_STATUS_PART = "partial";
    public static final String SALE_STATUS_REJT = "rejected";
    public static final String SALE_STATUS_ORDER = "order";
    public static final String PRODUCT_STANDARD = "standard";
    public static final String SALETYPE_SALE = "sale";
    public static final String SALETYPE_ISSUE = "issue";
    public static final String SALETYPE_ORDER = "orders";
    public static final String PAYTYPE_RECEIVED = "received";
    public static final String PAYTYPE_PENDING = "pending";
    public static final String WH_DEMO = "demo";
    public static final String WH_ACTIVE = "active";
    public static final String WH_INACTIVE = "inactive";
    public static final String SOURCE_WEB = "web";
    public static final String SOURCE_APP = "device";
    public static final String PRODUCT_SERVICE = "service";
    public static final String TAXMETHOD_INCL = "inclusive";
    public static final String TAXMETHOD_EXC = "exclusive";
    public static final String APPTYPE_SHOP = "proshop";
    public static final String APPTYPE_SALE = "prosale";
    public static final String TRANSTYPE_REQUEST= "request";
    public static final String TRANSTYPE_TRANSFER = "transfer";
    public static final String TRANSTYPE_ISSUE = "issue";
    public static final int CONTEXTMENU_EDIT = 1;
    public static final int CONTEXTMENU_DELETE = 2;


    public final static String REQUESTING_LOCATION_UPDATES_KEY = "requesting-location-updates-key";
    public final static String LOCATION_KEY = "location-key";
    public final static String LAST_UPDATED_TIME_STRING_KEY = "last-updated-time-string-key";

    public static final String ADDRESS = "ADDRESS";
    public static final String CLIENTID = "ID";
    public static final long MIN_DISTANCE_CHANGE_FOR_UPDATES = 10; // 10 meters

    // The minimum time between updates in milliseconds
    public static final long MIN_TIME_BW_UPDATES = 1000 * 60 * 1; // 1 minute
    public static final String ITEMLIST = "itemsList";
    public static final String SOURCE = "source";
    public static final String PREINTERAPP = "app.incotex.fpd";
    public static final String SHOWPRICE = "show_price";


    /**
     * Authtoken type string.
     */
    private static String TAG = Constants.class.getName();

    public static final long UPDATE_INTERVAL_IN_MILLISECONDS(Context context) {
        long intervalMillis = 0;
        int minutesintervar = Settings.getTrackTime(context);
        if (minutesintervar == 0) {
            intervalMillis = Integer.parseInt(Constants.DEFAULTTRACKINTERVAL);
        } else {
            String interval = String.valueOf(minutesintervar);
            long ii = Long.valueOf(interval);
            intervalMillis = TimeUnit.MINUTES.toMillis(ii);
        }
        return intervalMillis;
    }

    public static final long SYNC_PER_USER(Context context) {
        long sync = 0;
        try {
            sync = Integer.parseInt(Constants.getIMEINO(context).substring(0, 8)) / 2;
            return sync;
        } catch (NumberFormatException e) {
            e.toString();
        }
        return 0;
    }

    public static Account createSyncAccount(Context context) {
        Account newaccount = new Account(ACCOUNT, ACCOUNT_TYPE);
        AccountManager mAccountManager = (AccountManager) context.getSystemService(Context.ACCOUNT_SERVICE);

        if (mAccountManager.addAccountExplicitly(newaccount, null, null)) {
            ContentResolver.setSyncAutomatically(newaccount, ContentProviderApi.AUTHORITY, true);
            Log.v(TAG, "PROSHOP ACCOUNT IS GOOD TO GO");
        } else {
            Log.v(TAG, "PROSHOP MOBILE  ACCOUNT FAILED TO BE SET UP");
        }
        return newaccount;

    }

    public static final String getIMEINO(Context ctx) {
        TelephonyManager tManager = (TelephonyManager) ctx
                .getSystemService(Context.TELEPHONY_SERVICE);
        String imeiid = tManager.getDeviceId();
        if (!Validating.areSet(imeiid) || imeiid.equals("unknown")) {
            imeiid = getDeviceId();
        }
        return imeiid;
    }

    public static boolean isNotConnected(Context ctx) {
        try {
            ConnectivityManager cm = (ConnectivityManager) ctx.getSystemService(Context.CONNECTIVITY_SERVICE);
            NetworkInfo netInfo = cm.getActiveNetworkInfo();

            if (netInfo != null && netInfo.isConnected()) {
                return true;
            } else {
                return false;
            }
        } catch (Exception e) {
            return false;
        }
    }

    public static final String getDate() {
        Date update = null;
        String returnstring = null;
        try {
            update = new Date();

            String[] formats = new String[]{
                    "yyyy-MM-dd HH:mm"
            };

            for (String df : formats) {
                SimpleDateFormat sdf = new SimpleDateFormat(df, Locale.getDefault());

                return sdf.format(update);
            }
        } finally {

        }
        return returnstring;
    }

    public static final String getDate(String format) {
        Date update = null;
        String returnstring = null;
        try {
            update = new Date();

            String[] formats = new String[]{
                    format
            };

            for (String df : formats) {
                SimpleDateFormat sdf = new SimpleDateFormat(df, Locale.getDefault());

                return sdf.format(update);
            }
        } finally {

        }
        return returnstring;
    }

    public static final String getFormatedDate(String format) {
        Date update = null;
        String returnstring = null;
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
        try {

            update = dateFormat.parse(format);

            String[] formats = new String[]{
                    "yyyy-MM-dd"
            };

            for (String df : formats) {
                SimpleDateFormat sdf = new SimpleDateFormat(df, Locale.getDefault());

                return sdf.format(update);
            }
        } catch (ParseException e) {
            e.printStackTrace();
        } finally {

        }
        return returnstring;
    }

    public static final String getDateOnly() {
        Date update = null;
        String returnstring = null;
        try {
            update = new Date();

            String[] formats = new String[]{
                    "MM-dd HH:mm"
            };

            for (String df : formats) {
                SimpleDateFormat sdf = new SimpleDateFormat(df, Locale.getDefault());

                return sdf.format(update);
            }
        } finally {

        }
        return returnstring;
    }

    public static final String getDateOnly2() {
        Date update = null;
        String returnstring = null;
        try {
            update = new Date();

            String[] formats = new String[]{
                    "MM-dd"
            };

            for (String df : formats) {
                SimpleDateFormat sdf = new SimpleDateFormat(df, Locale.getDefault());

                return sdf.format(update);
            }
        } finally {

        }
        return returnstring;
    }

    public static final String getYearDateMonth() {
        Date update = null;
        String returnstring = null;
        try {
            update = new Date();

            String[] formats = new String[]{
                    "yyyy-MM-dd"
            };

            for (String df : formats) {
                SimpleDateFormat sdf = new SimpleDateFormat(df, Locale.getDefault());

                return sdf.format(update);
            }
        } finally {

        }
        return returnstring;
    }


    public static final String getDateRange() {

        Calendar c = Calendar.getInstance();   // this takes current date
        c.set(Calendar.DAY_OF_MONTH, 1);
        System.out.println(c.getTime());

        Date update = c.getTime();
        String returnstring = null;
        try {

            String[] formats = new String[]{
                    "yyyy-MM-dd"
            };

            for (String df : formats) {
                SimpleDateFormat sdf = new SimpleDateFormat(df, Locale.getDefault());

                return sdf.format(update);
            }
        } finally {

        }
        return returnstring;
    }

    public static final String compareDateWeek() {
        Date update = null;
        String returnstring = null;
        try {
            update = new Date();

            Calendar calendar = Calendar.getInstance();
            calendar.setTime(update);
            calendar.add(Calendar.DAY_OF_MONTH, -7);
            Date newDate = calendar.getTime();

            String[] formats = new String[]{
                    "yyyy-MM-dd HH:mm"
            };

            for (String df : formats) {
                SimpleDateFormat sdf = new SimpleDateFormat(df, Locale.getDefault());

                return sdf.format(newDate);
            }
        } finally {

        }
        return returnstring;
    }

    public static final String compareDateMonth() {
        Date update = null;
        String returnstring = null;
        try {
            update = new Date();

            Calendar calendar = Calendar.getInstance();
            calendar.setTime(update);
            calendar.add(Calendar.DAY_OF_MONTH, -30);
            Date newDate = calendar.getTime();

            String[] formats = new String[]{
                    "yyyy-MM"
            };

            for (String df : formats) {
                SimpleDateFormat sdf = new SimpleDateFormat(df, Locale.getDefault());

                return sdf.format(newDate);
            }
        } finally {

        }
        return returnstring;
    }


    public static void LogException(Exception ex) {
        Log.d(TAG + " Exception",
                TAG + "Exception -- > " + ex.getMessage() + "\n");
        ex.printStackTrace();
    }

    public static final String getPriceAfterBeforeTaxs(double price, double tax, int taxyes) {
        double finalprice = 0;
        switch (taxyes) {
            case 1:
                finalprice = price + price * (tax / 100);

                return String.valueOf(finalprice);
            case 0:
                return String.valueOf(price);
        }
        return null;
    }

    public static double calculate_grandtotal(double price, int qty) {
        double ret = 0;
        //no tax
        BigDecimal outpt = new BigDecimal(price).multiply(new BigDecimal(qty));//price*qty

        ret = outpt.setScale(2, RoundingMode.HALF_UP).doubleValue();

        return ret;
    }

    public static double calculate_unit_tax(double taxrate, double price) {
        double pri = 0;
        double tax = new BigDecimal(taxrate).divide(new BigDecimal(100)).setScale(2, BigDecimal.ROUND_HALF_UP)
                .doubleValue();
        pri = new BigDecimal(price).multiply(new BigDecimal(tax)).setScale(2, RoundingMode.HALF_UP).doubleValue();
        return pri;
    }

    public static double calculate_unit_netsaleprice(double price, double taxrate, int taxmethod, boolean taxsettings) {
        double ret = 0;
        if (taxsettings) {
            if (taxmethod == 0) {//no tax tax inclusive
                // this was the old implimentation where if tax is inclusive
                double tot = new BigDecimal(price).subtract(new BigDecimal(taxrate)).doubleValue();
                ret = new BigDecimal(tot).setScale(2, RoundingMode.HALF_UP).doubleValue();
            } else {
                double tot = new BigDecimal(price).doubleValue();
                ret = new BigDecimal(tot).setScale(2, RoundingMode.HALF_UP).doubleValue();
            }
        } else {
            if (taxmethod == 0) {//tax inclusive
            /* old implementation for now net will be the same as it was be it its inclusive or exclusive
                double tot = new BigDecimal(price).subtract(new BigDecimal(taxrate)).doubleValue();
                ret = new BigDecimal(tot).setScale(2, RoundingMode.HALF_UP).doubleValue();*/
                double tot = new BigDecimal(price).doubleValue();
                ret = new BigDecimal(tot).setScale(2, RoundingMode.HALF_UP).doubleValue();
            } else {
                double tot = new BigDecimal(price).doubleValue();
                ret = new BigDecimal(tot).setScale(2, RoundingMode.HALF_UP).doubleValue();
            }
        }
        return ret;
    }

    public static double netprice(double price, double taxrate, int taxmethod, boolean settings) {
        double toreturn = 0;
        if (settings) {
            if (taxmethod == 0) {//tax inclusive
                BigDecimal tx = new BigDecimal(taxrate).divide(new BigDecimal(100), 2, RoundingMode.HALF_UP);
                BigDecimal _tax = tx.add(new BigDecimal(1));
                BigDecimal _pri = new BigDecimal(price);
                System.out.println("for net price _tax is " + _tax + " _price " + _pri);
                toreturn = _pri.divide(_tax, 2, BigDecimal.ROUND_HALF_UP).doubleValue();
            } else {
                toreturn = price;
            }
        } else {
            if (taxmethod == 0) {//tax inclusive
                toreturn = new BigDecimal(price).setScale(2, RoundingMode.HALF_UP).doubleValue();
            } else {
                toreturn = new BigDecimal(price).setScale(2, RoundingMode.HALF_UP).doubleValue();
                ;
            }
        }
        return toreturn;
    }

    public static double saleprice(double price, double taxitem, int taxmethod, boolean settings) {
        double toreturn = 0;
        if (settings) {
            if (taxmethod == 0) {//tax inclusive
                BigDecimal _tax = new BigDecimal(0).add(new BigDecimal(1));
                BigDecimal _pri = new BigDecimal(price);
                toreturn = _pri.divide(_tax).setScale(2, RoundingMode.HALF_UP).doubleValue();
            } else {
                System.out.println("hit here");
                toreturn = new BigDecimal(price).add(new BigDecimal(taxitem)).setScale(2, RoundingMode.HALF_UP).doubleValue();
            }
        } else {
            if (taxmethod == 0) {//tax inclusive
                toreturn = new BigDecimal(price).setScale(2, RoundingMode.HALF_UP).doubleValue();
            } else {
                toreturn = new BigDecimal(price).setScale(2, RoundingMode.HALF_UP).doubleValue();
            }
        }
        return toreturn;
    }

    public static double itemtax(double price, double taxrate, int taxmethod, boolean settings) {
        double toreturn = 0;
        if (settings) {
            if (taxmethod == 0) {
                double _netprice = netprice(price, taxrate, taxmethod, settings);
                toreturn = new BigDecimal(price).subtract(new BigDecimal(_netprice)).setScale(2, RoundingMode.HALF_UP)
                        .doubleValue();
            } else {
                BigDecimal tx = new BigDecimal(taxrate).divide(new BigDecimal(100), 2, BigDecimal.ROUND_HALF_UP);
                double _saleprice = new BigDecimal(price).multiply(tx).setScale(2, RoundingMode.HALF_UP).doubleValue();
                System.out.println(" itemtax " + _saleprice);
                toreturn = _saleprice;
            }
        } else {
            toreturn = 0;
        }
        return toreturn;
    }

    public static double getTax(double netprice, double nettax) {
        double toreturn = 0;
        toreturn = new BigDecimal(netprice).multiply(new BigDecimal(nettax)).setScale(2, RoundingMode.HALF_UP)
                .doubleValue();
        return toreturn;
    }

    public static double calculate_unit_saleprice(double price, double taxrate, int taxmethod, boolean taxsettings) {
        double ret = 0;
        if (taxsettings) {
            if (taxmethod == 0) {//no tax tax inclusive
                ret = new BigDecimal(price).setScale(2, RoundingMode.HALF_UP).doubleValue();
            } else {
                double tot = new BigDecimal(price).doubleValue();
                ret = new BigDecimal(tot).add(new BigDecimal(taxrate)).setScale(2, RoundingMode.HALF_UP).doubleValue();
            }
        } else {
            if (taxmethod == 0) {//tax inclusive
                /*double tot = new BigDecimal(price).subtract(new BigDecimal(taxrate)).doubleValue();
                ret = new BigDecimal(tot).setScale(2, RoundingMode.HALF_UP).doubleValue();*/
                double tot = new BigDecimal(price).doubleValue();
                ret = new BigDecimal(tot).setScale(2, RoundingMode.HALF_UP).doubleValue();
            } else {
                double tot = new BigDecimal(price).doubleValue();
                ret = new BigDecimal(tot).setScale(2, RoundingMode.HALF_UP).doubleValue();
            }
        }
        return ret;
    }

    public static double calculate_grandtotal(double price, int qty, double tx) {
        double ret = 0;
        //no tax
        tx = new BigDecimal(tx).divide(new BigDecimal(100)).doubleValue();
        BigDecimal total = new BigDecimal(price).multiply(new BigDecimal(qty));//price*qty
        BigDecimal taxamount = total.multiply(new BigDecimal(tx));
        BigDecimal outpt = total.add(taxamount);

        ret = outpt.setScale(2, RoundingMode.HALF_UP).doubleValue();

        return ret;
    }

    public static double calculate_grandtotal_inclusive(double price, int qty, double tx) {
        double ret = 0;
        //no tax
        tx = new BigDecimal(tx).divide(new BigDecimal(100)).doubleValue();
        BigDecimal total = new BigDecimal(price).multiply(new BigDecimal(qty));//price*qty
        BigDecimal taxamount = total.multiply(new BigDecimal(tx));
        BigDecimal outpt = total.subtract(taxamount);

        ret = outpt.setScale(2, RoundingMode.HALF_UP).doubleValue();

        return ret;
    }

    public static boolean validateCsv(String myString) {
        if (myString.length() > 3) {
            String format = myString.substring(myString.length() - 3);
            if (format.equals("csv")) {
                return true;
            }
        }
        return false;
    }

    public static double calculateTotalNoTax(double totals, double taxrate) {

        BigDecimal output = new BigDecimal(0);
        BigDecimal taxamount = new BigDecimal(calculateTax(totals, taxrate));
        if (taxrate == 0) {
            output = new BigDecimal(totals);
        } else if (taxrate > 0) {
            output = new BigDecimal(totals).subtract(taxamount);
        }
        return output.setScale(2, RoundingMode.HALF_UP).doubleValue();
    }

    public static double[] isDiscountLegit(double[] price) {
        //price 0 unit price 1 discount 2 disc %
        BigDecimal unit = new BigDecimal(price[0]);
        BigDecimal actual = new BigDecimal(price[1]);
        BigDecimal maxdiscrate = new BigDecimal(price[2]).divide(new BigDecimal(100)).setScale(2, RoundingMode.HALF_UP);
        BigDecimal maxdisc = unit.multiply(maxdiscrate);
        BigDecimal diff = unit.subtract(actual);
        double difference = diff.setScale(2, RoundingMode.HALF_UP).doubleValue();
        double discounted = maxdisc.setScale(2, RoundingMode.HALF_UP).doubleValue();
        System.out.println("price " + price[0] + " disc " + price[1] + " disrate " + price[2]);
        System.out.println("maxdis " + discounted + " diff " + difference);
        if (difference <= discounted)
            return new double[]{difference, discounted};

        return new double[]{unit.subtract(maxdisc).doubleValue()};
    }

    public static double isDiscount(double[] price) {
        //price 0 unit price 1 discount 2 disc %
        BigDecimal unit = new BigDecimal(price[0]); // unit price
        System.out.println("unit " + unit);
        BigDecimal actual = new BigDecimal(price[1]); // the discount
        System.out.println("actual " + actual);
        BigDecimal maxdiscrate = new BigDecimal(price[2]).setScale(2, RoundingMode.HALF_UP);
        System.out.println("max rate " + maxdiscrate);
        BigDecimal maxdisc = unit.multiply(maxdiscrate).divide(new BigDecimal(100));
        System.out.println("max discount " + maxdisc);
        double max = maxdisc.setScale(2, RoundingMode.HALF_UP).doubleValue();
        double discount = actual.setScale(2, RoundingMode.HALF_UP).doubleValue();
        System.out.println("actual max  " + max);

        if (max < discount && discount > 0)
            return max;

        return 0;
    }

    public static double calculateTax(double price, double taxrate) {
        //this will return tax only
        BigDecimal rate = new BigDecimal(taxrate).divide(new BigDecimal(100));//price*qty
        BigDecimal output = new BigDecimal(price).multiply(rate);

        return output.setScale(2, RoundingMode.HALF_UP).doubleValue();
    }

    public static double totalsnotax(double price, double taxamount) {//calculate the total cost with no tax
        BigDecimal output = new BigDecimal(price).subtract(new BigDecimal(taxamount));
        return output.setScale(2, RoundingMode.HALF_UP).doubleValue();
    }

    public static int exportPrintableFile(Context context) {
        final String xmlData = "rcpREQ";
        try {
            File file = new File(Environment.getExternalStorageDirectory().getPath() + "/Download/" + xmlData + ".xml");

            FileOutputStream fileOutputStream1 = new FileOutputStream(file);
            XmlSerializer serializer = Xml.newSerializer();
            StringWriter stringWriter = new StringWriter();
            serializer.setOutput(stringWriter);
            serializer.startDocument("UTF-8", true);
            serializer.startTag(null, RCP);
            serializer.startTag(null, TRANSACTION);
            serializer.startTag(null, TRN);
            serializer.startTag(null, TAX_GROUP);
            serializer.text("1");
            serializer.endTag(null, TAX_GROUP);
            serializer.startTag(null, NAME);
            serializer.text("Korosho");
            serializer.endTag(null, NAME);
            serializer.startTag(null, SPRICE);
            serializer.text("25.00");
            serializer.endTag(null, SPRICE);
            serializer.startTag(null, QTY);
            serializer.text("9");
            serializer.endTag(null, QTY);
            serializer.endTag(null, TRN);

            serializer.startTag(null, TRN);
            serializer.startTag(null, TAX_GROUP);
            serializer.text("1");
            serializer.endTag(null, TAX_GROUP);
            serializer.startTag(null, NAME);
            serializer.text("Karanga");
            serializer.endTag(null, NAME);
            serializer.startTag(null, SPRICE);
            serializer.text("15.00");
            serializer.endTag(null, SPRICE);
            serializer.startTag(null, QTY);
            serializer.text("3");
            serializer.endTag(null, QTY);
            serializer.startTag(null, PDISCOUNT);
            serializer.text("5.00");
            serializer.endTag(null, PDISCOUNT);
            serializer.endTag(null, TRN);

            serializer.endTag(null, TRANSACTION);
            serializer.startTag(null, PAYMENTS);
            serializer.startTag(null, PMT);
            serializer.startTag(null, PAY_TYPE);
            serializer.text("0");
            serializer.endTag(null, PAY_TYPE);
            serializer.startTag(null, PAY_AMOUNT);
            serializer.text("160.00");
            serializer.endTag(null, PAY_AMOUNT);
            serializer.endTag(null, PMT);
            serializer.endTag(null, PAYMENTS);
            serializer.endTag(null, RCP);
            serializer.endDocument();
            serializer.flush();

            String datawriter = stringWriter.toString();
            fileOutputStream1.write(datawriter.getBytes());
            fileOutputStream1.close();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return 0;
    }

    public static int writeReceipt(Context context, List<SalesItems> itemLists, boolean settings, String customer) {
        boolean tablet = context.getResources().getBoolean(R.bool.isTablet);
        System.out.println("tablet ? " + tablet);
        String vrn = Customers.getVrn(context, customer);
        String tin = Customers.getTin(context, customer);
        System.out.println("tin " + tin + " vrn " + vrn + " customer is " + customer);
        if (!tablet) {
            System.out.println("it is phone? " + tablet);
            final String xmlData = "rcpREQ";

            try {
                File file = new File(Environment.getExternalStorageDirectory().getPath() + "/Download/" + xmlData + ".xml");
                System.out.println(file.getAbsolutePath());
                File sdcard = Environment.getExternalStorageDirectory();
                File folder = new File(sdcard.getAbsoluteFile(), "Proshop_Reports");

                if (!folder.exists()) {
                    folder.mkdirs();
                }

                String j = Constants.getDateOnly();
                File file2 = new File(folder.getAbsoluteFile(), j + ".xml");
                System.out.println(file2.getAbsolutePath());
                FileOutputStream fileOutputStream1 = new FileOutputStream(file);
                FileOutputStream fileOutputStream2 = new FileOutputStream(file2);
                XmlSerializer serializer = Xml.newSerializer();
                StringWriter stringWriter = new StringWriter();
                serializer.setOutput(stringWriter);

                serializer.startDocument("UTF-8", true);
                serializer.startTag(null, RCP);

               /* serializer.startTag(null, CLIENT);
                serializer.startTag(null, NAME);
                serializer.text("DAR GROUP");
                serializer.endTag(null, NAME);
                serializer.startTag(null,ADDRESS);
                serializer.text("Kariakoo Congo");
                serializer.endTag(null, ADDRESS);
                serializer.endTag(null, CLIENT);
               */
                serializer.startTag(null, TRANSACTION);
                double itemSales = 0;
                for (SalesItems orderItemList : itemLists) {
                    serializer.startTag(null, TRN);
                    if (settings && orderItemList.getTaxitem() > 0) {
                        System.out.println("tax group 1");
                        serializer.startTag(null, TAX_GROUP);
                        serializer.text("1");
                        serializer.endTag(null, TAX_GROUP);
                    } else {
                        System.out.println("tax group d");
                        serializer.startTag(null, TAX_GROUP);
                        serializer.text("3");
                        serializer.endTag(null, TAX_GROUP);
                    }

                    serializer.startTag(null, NAME);
                    String prodname = null;
                    long prodid = orderItemList.getProdloc();
                    int taxmethod = Products.getTaxmethod(context, prodid);
                    prodname = Products.getProdName(context, String.valueOf(prodid));
                    serializer.text(prodname);
                    System.out.println("production " + prodname);
                    serializer.endTag(null, NAME);
                    serializer.startTag(null, SPRICE);
                    double price = orderItemList.getPrice();
                    double discountpr = orderItemList.getDiscount();

                    if (discountpr != 0)
                        price = discountpr;

                    System.out.println("price " + price + " netprice " + orderItemList.getNetprice() + " discount " + discountpr);
                    serializer.text(String.valueOf(price));
                    serializer.endTag(null, SPRICE);
                    serializer.startTag(null, QTY);
                    serializer.text(String.valueOf(orderItemList.getQty()));
                    serializer.endTag(null, QTY);
                    double singlesales = orderItemList.getSbtotal();//without taxes
                    itemSales += singlesales;
                    serializer.endTag(null, TRN);
                }
                serializer.endTag(null, TRANSACTION);
                serializer.endTag(null, RCP);
                serializer.endDocument();
                String datawriter = stringWriter.toString();
                fileOutputStream1.write(datawriter.getBytes());
                fileOutputStream2.write(datawriter.getBytes());
                fileOutputStream1.close();
                fileOutputStream2.close();
                return 1;
            } catch (FileNotFoundException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }
        } else {
            System.out.println("it is tablet? " + tablet);
            final String xmlData = "rcpREQ";

            try {
                File file = new File(Environment.getExternalStorageDirectory().getPath() + "/Download/" + xmlData + ".xml");
                System.out.println(file.getAbsolutePath());
                FileOutputStream fileOutputStream1 = new FileOutputStream(file);

                XmlSerializer serializer = Xml.newSerializer();
                StringWriter stringWriter = new StringWriter();
                serializer.setOutput(stringWriter);

                serializer.startDocument("UTF-8", true);
                serializer.startTag(null, RCP);
                serializer.startTag(null, TRANSACTION);
                double itemSales = 0;
                for (SalesItems orderItemList : itemLists) {
                    serializer.startTag(null, TRN);
                    if (settings && orderItemList.getTaxitem() > 0) {
                        System.out.println("tax group 1");
                        serializer.startTag(null, TAX_GROUP);
                        serializer.text("1");
                        serializer.endTag(null, TAX_GROUP);
                    } else {
                        System.out.println("tax group d");
                        serializer.startTag(null, TAX_GROUP);
                        serializer.text("3");
                        serializer.endTag(null, TAX_GROUP);
                    }
                    serializer.startTag(null, NAME);
                    String prodname = null;
                    long prodid = orderItemList.getProdloc();
                    int taxmethod = Products.getTaxmethod(context, prodid);
                    prodname = Products.getProdName(context, String.valueOf(prodid));
                    serializer.text(prodname);
                    System.out.println("production " + prodname);
                    serializer.endTag(null, NAME);
                    serializer.startTag(null, SPRICE);
                    double price = orderItemList.getPrice();
                    double discountpr = orderItemList.getDiscount();

                    if (discountpr != 0)
                        price = discountpr;

                    System.out.println("price " + price + " netprice " + orderItemList.getNetprice() + " discount " + discountpr);
                    serializer.text(String.valueOf(price));
                    serializer.endTag(null, SPRICE);
                    serializer.startTag(null, QTY);
                    serializer.text(String.valueOf(orderItemList.getQty()));
                    serializer.endTag(null, QTY);
                    double singlesales = orderItemList.getSbtotal();//without taxes
                    itemSales += singlesales;
                    serializer.endTag(null, TRN);
                }
                serializer.endTag(null, TRANSACTION);
                serializer.endTag(null, RCP);
                serializer.endDocument();
                String datawriter = stringWriter.toString();
                fileOutputStream1.write(datawriter.getBytes());

                fileOutputStream1.close();
                return 1;
            } catch (FileNotFoundException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        return 0;
    }

    public static int writeReceiptPending(Context context, ArrayList<Pending_Items> itemLists, boolean settings, String
            customer) {
        boolean tablet = context.getResources().getBoolean(R.bool.isTablet);
        System.out.println("tablet ? " + tablet);
        String vrn = Customers.getVrn(context, customer);
        String tin = Customers.getTin(context, customer);
        System.out.println("tin " + tin + " vrn " + vrn + " customer is " + customer);
        if (!tablet) {
            System.out.println("it is phone? " + tablet);
            final String xmlData = "rcpREQ";

            try {
                File file = new File(Environment.getExternalStorageDirectory().getPath() + "/Download/" + xmlData + ".xml");
                System.out.println(file.getAbsolutePath());
                File sdcard = Environment.getExternalStorageDirectory();
                File folder = new File(sdcard.getAbsoluteFile(), "Proshop_Reports");

                if (!folder.exists()) {
                    folder.mkdirs();
                }

                String j = Constants.getDateOnly();
                File file2 = new File(folder.getAbsoluteFile(), j + ".xml");
                System.out.println(file2.getAbsolutePath());
                FileOutputStream fileOutputStream1 = new FileOutputStream(file);
                FileOutputStream fileOutputStream2 = new FileOutputStream(file2);
                XmlSerializer serializer = Xml.newSerializer();
                StringWriter stringWriter = new StringWriter();
                serializer.setOutput(stringWriter);

                serializer.startDocument("UTF-8", true);
                serializer.startTag(null, RCP);

               /* serializer.startTag(null, CLIENT);
                serializer.startTag(null, NAME);
                serializer.text("DAR GROUP");
                serializer.endTag(null, NAME);
                serializer.startTag(null,ADDRESS);
                serializer.text("Kariakoo Congo");
                serializer.endTag(null, ADDRESS);
                serializer.endTag(null, CLIENT);
               */
                serializer.startTag(null, TRANSACTION);
                double itemSales = 0;
                for (Pending_Items orderItemList : itemLists) {
                    serializer.startTag(null, TRN);
                    if (settings && orderItemList.getTaxitem() > 0) {
                        System.out.println("tax group 1");
                        serializer.startTag(null, TAX_GROUP);
                        serializer.text("1");
                        serializer.endTag(null, TAX_GROUP);
                    } else {
                        System.out.println("tax group d");
                        serializer.startTag(null, TAX_GROUP);
                        serializer.text("3");
                        serializer.endTag(null, TAX_GROUP);
                    }

                    serializer.startTag(null, NAME);
                    String prodname = null;
                    long prodid = orderItemList.getProdloc();
                    int taxmethod = Products.getTaxmethod(context, prodid);
                    prodname = Products.getProdName(context, String.valueOf(prodid));
                    serializer.text(prodname);
                    System.out.println("production " + prodname);
                    serializer.endTag(null, NAME);
                    serializer.startTag(null, SPRICE);
                    double price = orderItemList.getPrice();
                    double discountpr = orderItemList.getDiscount();

                    if (discountpr != 0)
                        price = discountpr;

                    System.out.println("price " + price + " netprice " + orderItemList.getNetprice() + " discount " + discountpr);
                    serializer.text(String.valueOf(price));
                    serializer.endTag(null, SPRICE);
                    serializer.startTag(null, QTY);
                    serializer.text(String.valueOf(orderItemList.getQty()));
                    serializer.endTag(null, QTY);
                    double singlesales = orderItemList.getSbtotal();//without taxes
                    itemSales += singlesales;
                    serializer.endTag(null, TRN);
                }
                serializer.endTag(null, TRANSACTION);
                serializer.endTag(null, RCP);
                serializer.endDocument();
                String datawriter = stringWriter.toString();
                fileOutputStream1.write(datawriter.getBytes());
                fileOutputStream2.write(datawriter.getBytes());
                fileOutputStream1.close();
                fileOutputStream2.close();
                return 1;
            } catch (FileNotFoundException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }
        } else {
            System.out.println("it is tablet? " + tablet);
            final String xmlData = "rcpREQ";

            try {
                File file = new File(Environment.getExternalStorageDirectory().getPath() + "/Download/" + xmlData + ".xml");
                System.out.println(file.getAbsolutePath());
                FileOutputStream fileOutputStream1 = new FileOutputStream(file);

                XmlSerializer serializer = Xml.newSerializer();
                StringWriter stringWriter = new StringWriter();
                serializer.setOutput(stringWriter);

                serializer.startDocument("UTF-8", true);
                serializer.startTag(null, RCP);
                serializer.startTag(null, TRANSACTION);
                double itemSales = 0;
                for (Pending_Items orderItemList : itemLists) {
                    serializer.startTag(null, TRN);
                    if (settings && orderItemList.getTaxitem() > 0) {
                        System.out.println("tax group 1");
                        serializer.startTag(null, TAX_GROUP);
                        serializer.text("1");
                        serializer.endTag(null, TAX_GROUP);
                    } else {
                        System.out.println("tax group d");
                        serializer.startTag(null, TAX_GROUP);
                        serializer.text("3");
                        serializer.endTag(null, TAX_GROUP);
                    }
                    serializer.startTag(null, NAME);
                    String prodname = null;
                    long prodid = orderItemList.getProdloc();
                    int taxmethod = Products.getTaxmethod(context, prodid);
                    prodname = Products.getProdName(context, String.valueOf(prodid));
                    serializer.text(prodname);
                    System.out.println("production " + prodname);
                    serializer.endTag(null, NAME);
                    serializer.startTag(null, SPRICE);
                    double price = orderItemList.getPrice();
                    double discountpr = orderItemList.getDiscount();

                    if (discountpr != 0)
                        price = discountpr;

                    System.out.println("price " + price + " netprice " + orderItemList.getNetprice() + " discount " + discountpr);
                    serializer.text(String.valueOf(price));
                    serializer.endTag(null, SPRICE);
                    serializer.startTag(null, QTY);
                    serializer.text(String.valueOf(orderItemList.getQty()));
                    serializer.endTag(null, QTY);
                    double singlesales = orderItemList.getSbtotal();//without taxes
                    itemSales += singlesales;
                    serializer.endTag(null, TRN);
                }
                serializer.endTag(null, TRANSACTION);
                serializer.endTag(null, RCP);
                serializer.endDocument();
                String datawriter = stringWriter.toString();
                fileOutputStream1.write(datawriter.getBytes());

                fileOutputStream1.close();
                return 1;
            } catch (FileNotFoundException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        return 0;
    }

    public static int writeXmls(List<OrderItemList> itemLists) {
        final String xmlData = "rcpREQ";
        try {
            File file = new File(Environment.getExternalStorageDirectory().getPath() + "/Download/" + xmlData + ".xml");
            File sdcard = Environment.getExternalStorageDirectory();
            File folder = new File(sdcard.getAbsoluteFile(), "Proshop_Reports");

            if (!folder.exists()) {
                folder.mkdirs();
            }

            String j = Constants.getDateOnly();
            File file2 = new File(folder.getAbsoluteFile(), j + ".xml");

            FileOutputStream fileOutputStream1 = new FileOutputStream(file);
            FileOutputStream fileOutputStream2 = new FileOutputStream(file2);
            XmlSerializer serializer = Xml.newSerializer();
            StringWriter stringWriter = new StringWriter();
            serializer.setOutput(stringWriter);

            serializer.startDocument("UTF-8", true);
            serializer.startTag(null, RCP);
            serializer.startTag(null, TRANSACTION);
            double itemSales = 0;
            for (OrderItemList orderItemList : itemLists) {
                serializer.startTag(null, TRN);
                serializer.startTag(null, TAX_GROUP);
                serializer.text("1");
                serializer.endTag(null, TAX_GROUP);
                serializer.startTag(null, NAME);
                serializer.text(orderItemList.getProductname());
                System.out.println("production " + orderItemList.getProductname());
                serializer.endTag(null, NAME);
                serializer.startTag(null, SPRICE);
                double price = orderItemList.getUnitprice();
                double discountpr = orderItemList.getDiscountunitprice();
                if (discountpr != 0)
                    price = discountpr;
                serializer.text(String.valueOf(price));
                serializer.endTag(null, SPRICE);
                serializer.startTag(null, QTY);
                serializer.text(String.valueOf(orderItemList.getQuantity()));
                serializer.endTag(null, QTY);
                double singlesales = orderItemList.getGrandtotal();//without taxes
                itemSales += singlesales;
                serializer.endTag(null, TRN);
            }
            serializer.endTag(null, TRANSACTION);
            serializer.endTag(null, RCP);
            serializer.endDocument();
            String datawriter = stringWriter.toString();
            fileOutputStream1.write(datawriter.getBytes());
            fileOutputStream2.write(datawriter.getBytes());
            fileOutputStream1.close();
            fileOutputStream2.close();
            return 1;
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return 0;
    }


    public static final String getbase64photo(String imagpath) {
        String mCurrentPhotoPath = null;
        File f = new File(imagpath);

        if(!f.exists())
                return null;

        Bitmap bm = decodeFile(imagpath);
        ByteArrayOutputStream bao = new ByteArrayOutputStream();
        bm.compress(Bitmap.CompressFormat.JPEG, 90, bao);
        byte[] ba = bao.toByteArray();
        mCurrentPhotoPath = Base64.encodeToString(ba, Base64.DEFAULT);
        return mCurrentPhotoPath;
    }

    public static Bitmap decodeFile(String filePath) {

        // Decode image size
        BitmapFactory.Options o = new BitmapFactory.Options();
        o.inJustDecodeBounds = true;
        BitmapFactory.decodeFile(filePath, o);

        // The new size we want to scale to
        final int REQUIRED_SIZE = 1024;

        // Find the correct scale value. It should be the power of 2.
        int width_tmp = o.outWidth, height_tmp = o.outHeight;
        int scale = 1;
        while (true) {
            if (width_tmp < REQUIRED_SIZE && height_tmp < REQUIRED_SIZE)
                break;
            width_tmp /= 2;
            height_tmp /= 2;
            scale *= 2;
        }

        // Decode with inSampleSize
        BitmapFactory.Options o2 = new BitmapFactory.Options();
        o2.inSampleSize = scale;
        Bitmap bitmap = BitmapFactory.decodeFile(filePath, o2);

        return bitmap;
    }


    public static final String getBase64Vid(final String url) {
        try {
            File file = new File(url);
            byte[] bFile = new byte[(int) file.length()];
            Log.v("filelength", " " + file.length());
            FileInputStream inputStream = new FileInputStream(url);
            inputStream.read(bFile);
            inputStream.close();
//        Log.v("filecontent", Base64.encodeToString(bFile, Base64.NO_PADDING));
//        return Base64.encodeb(bFile, Base64.DEFAULT);
            return bongolive.apps.proshop.controller.Base64.encodeBytes(bFile);
        } catch (IOException e) {
            Log.e(TAG, Log.getStackTraceString(e));
        }
        return null;
    }

    public static final String makeSHA1Hash(String input) throws NoSuchAlgorithmException {
        MessageDigest md = MessageDigest.getInstance("SHA1");
        md.reset();
        byte[] buffer = input.getBytes();
        md.update(buffer);
        byte[] digest = md.digest();

        String hexStr = "";
        for (int i = 0; i < digest.length; i++) {
            hexStr += Integer.toString((digest[i] & 0xff) + 0x100, 16).substring(1);
        }

        return hexStr;
    }


    public static final String convertBase64toBitmap(Context context, String base64) {
        try {
            byte[] imageAsBytes = Base64.decode(base64.getBytes(), Base64.DEFAULT);
            Bitmap bitmap = BitmapFactory.decodeByteArray(imageAsBytes, 0, imageAsBytes.length);

            File sdcard = Environment.getExternalStorageDirectory();

            File folder = new File(sdcard.getAbsoluteFile(), Constants.MASTER_DIR);

            String filename = String.valueOf(System.currentTimeMillis());

            File file = null;

            try {
                file = new File(folder.getAbsoluteFile(), filename + ".jpg");
                ByteArrayOutputStream bos = new ByteArrayOutputStream();
                bitmap.compress(Bitmap.CompressFormat.JPEG, 0 /*ignored for PNG*/, bos);
                byte[] bitmapdata = bos.toByteArray();
                FileOutputStream fos = new FileOutputStream(file);
                fos.write(bitmapdata);
                fos.flush();
                fos.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
            String path = file.getAbsolutePath();
            return path;
        } catch (IllegalArgumentException e) {
            e.printStackTrace();
        }

        return null;
    }


    public static final String generateBatch() {
        String batch = null;

        long number = (long) Math.floor(Math.random() * 9000000000L) + 1000000000L;
        batch = String.valueOf(number);
        return batch;
    }

    public static final String generatePayref() {
        String dt = getDate("yyyy/MM");

        String batch = "IPAY/" + dt + "/" + System.currentTimeMillis();
        return batch;
    }

    public static void setDateTimeField(Context context, final EditText fromDateEtxt) {

        Calendar newCalendar = Calendar.getInstance();
        final SimpleDateFormat dateFormatter;
        dateFormatter = new SimpleDateFormat("yyyy-MM-dd", Locale.US);
        DatePickerDialog fromDatePickerDialog = new DatePickerDialog(context, new DatePickerDialog.OnDateSetListener() {

            public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                Calendar newDate = Calendar.getInstance();
                newDate.set(year, monthOfYear, dayOfMonth);
                fromDateEtxt.setText(dateFormatter.format(newDate.getTime()));

            }

        }, newCalendar.get(Calendar.YEAR), newCalendar.get(Calendar.MONTH), newCalendar.get(Calendar.DAY_OF_MONTH));
        fromDatePickerDialog.show();
    }


    public static void setDateTime(Context context, final TextView fromDateEtxt) {

        Calendar newCalendar = Calendar.getInstance();
        final SimpleDateFormat dateFormatter;
        dateFormatter = new SimpleDateFormat("yyyy-MM-dd", Locale.US);
        DatePickerDialog fromDatePickerDialog = new DatePickerDialog(context, new DatePickerDialog.OnDateSetListener() {

            public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                Calendar newDate = Calendar.getInstance();
                newDate.set(year, monthOfYear, dayOfMonth);
                fromDateEtxt.setText(dateFormatter.format(newDate.getTime()));

            }

        }, newCalendar.get(Calendar.YEAR), newCalendar.get(Calendar.MONTH), newCalendar.get(Calendar.DAY_OF_MONTH));
        fromDatePickerDialog.show();
    }


    public static void setDateTimeSeconds(Context context, final EditText fromDateEtxt) {
        Calendar newCalendar = Calendar.getInstance();
        final SimpleDateFormat dateFormatter;
        dateFormatter = new SimpleDateFormat("yyyy-MM-dd HH:mm", Locale.getDefault());
        TimePickerDialog pickerDialog = new TimePickerDialog(context, new TimePickerDialog.OnTimeSetListener() {
            @Override
            public void onTimeSet(TimePicker view, int hourOfDay, int minute) {
                Calendar newDate = Calendar.getInstance();
                String[] dt = getYearDateMonth().split("-");
                int year = Integer.parseInt(dt[0]);
                int monthOfYear = Integer.parseInt(dt[1]);
                int dayOfMonth = Integer.parseInt(dt[2]);
                newDate.set(year, monthOfYear, dayOfMonth, hourOfDay, minute);
                fromDateEtxt.setText(dateFormatter.format(newDate.getTime()));

            }
        }, newCalendar.get(Calendar.HOUR_OF_DAY), newCalendar.get(Calendar.MINUTE), true);

        pickerDialog.show();
    }


    public static final String getVersionName(Context context) {
        try {
            String versionName = context.getPackageManager()
                    .getPackageInfo(context.getPackageName(), 0).versionName;
            return versionName;
        } catch (PackageManager.NameNotFoundException ne) {
            ne.printStackTrace();
        }
        return "0";
    }

    public static final String getReportName(String report) {
        File sdcard = Environment.getExternalStorageDirectory();

        File folder = new File(sdcard.getAbsoluteFile(), "Proshop_Reports");

        if (!folder.exists()) {
            folder.mkdirs();
        }


        File file = new File(folder.getAbsoluteFile(), report + " on " + getDate() + ".csv");
        System.out.println("filename " + file);
        return file.toString();
    }


    public static boolean checkInteger(String string) {

        for (int i = 0; i < string.length(); i++) {
            if (Character.isDigit(string.charAt(i))) {
                System.out.println(" this is digit " + string.charAt(i));
                return true;
            } else {
                System.out.println(" this is not digit " + string.charAt(i));
                return false;
            }
        }
        return false;
    }

    public static boolean checkIntegerStrictly(String string) {

        for (int i = 0; i < string.length(); i++) {
            Pattern validator = Pattern.compile("[;#&()?{}@$^*/%=\\-\\|\\+\\\\\"<>]");
            CharSequence sequence = new String(string);
            if (!validator.matcher(sequence).find()) {
                System.out.println(" characters not found " + sequence);
                return true;
            }
        }
        return false;
    }

    public static final String d_val(String v) {
        return new BigDecimal(v).setScale(2, RoundingMode.HALF_UP).toString();
    }

    public static boolean checkDouble(Context context, String string, EditText et) {
        for (int i = 0; i < string.length(); i++) {
            if (Character.isDigit(string.charAt(i))) {
                return true;
            } else {
                et.setText("");
                et.requestFocus();
                et.setHint(context.getString(R.string.numberwarn));
                et.setHintTextColor(Color.RED);
                return false;
            }
        }
        return false;
    }

    public static boolean checkInvalidChars(Context context, String string, EditText et) {

        for (int i = 0; i < string.length(); i++) {
            Pattern validator = Pattern.compile("[;#*/%=\\|\\+\\\\\"<>]");
            CharSequence sequence = new String(string);
            if (!validator.matcher(sequence).find()) {
                System.out.println(" not found " + validator.matcher(sequence));
                return true;
            } else {
                System.out.println(" found " + validator.matcher(sequence));
                et.setText("");
                et.requestFocus();
                et.setHint(context.getString(R.string.strinvalidcharacterwarn));
                et.setHintTextColor(Color.RED);
                return false; // etc: write all characters you need.
            }
        }
        return false;
    }


    public static Bitmap decodeImg(String path, int reqWidth, int reqHeight) {
        File file = new File(path);
        if (file.exists()) {
            final BitmapFactory.Options options = new BitmapFactory.Options();
            options.inJustDecodeBounds = true;
            BitmapFactory.decodeFile(path, options);

            options.inSampleSize = calculateInSampleSize(options, reqWidth, reqHeight);

            options.inJustDecodeBounds = false;
            return BitmapFactory.decodeFile(path, options);
        } else
            return null;
    }


    public static int calculateInSampleSize(
            BitmapFactory.Options options, int reqWidth, int reqHeight) {
        // Raw height and width of image
        final int height = options.outHeight;
        final int width = options.outWidth;
        int inSampleSize = 1;

        if (height > reqHeight || width > reqWidth) {

            final int halfHeight = height / 2;
            final int halfWidth = width / 2;

            // Calculate the largest inSampleSize value that is a power of 2 and keeps both
            // height and width larger than the requested height and width.
            while ((halfHeight / inSampleSize) > reqHeight
                    && (halfWidth / inSampleSize) > reqWidth) {
                inSampleSize *= 2;
            }
        }

        return inSampleSize;
    }


    public static boolean checkInvalidChars(Context context, String string, AutoCompleteTextView et) {

        for (int i = 0; i < string.length(); i++) {
            Pattern validator = Pattern.compile("[;#*/%=\\|\\+\\\\\"<>]");
            CharSequence sequence = new String(string);
            if (!validator.matcher(sequence).find()) {
                System.out.println(" not found " + validator.matcher(sequence));
                return true;
            } else {
                System.out.println(" found " + validator.matcher(sequence));
                et.setText("");
                et.requestFocus();
                et.setHint(context.getString(R.string.strinvalidcharacterwarn));
                et.setHintTextColor(Color.RED);
                return false; // etc: write all characters you need.
            }
        }
        return false;
    }

    public static boolean checkInvalidChars(String string) {

        for (int i = 0; i < string.length(); i++) {
            Pattern validator = Pattern.compile("[;#*/%=\\|\\+\\\\\"<>]");
            CharSequence sequence = new String(string);
            if (!validator.matcher(sequence).find()) {
                System.out.println(" not found " + validator.matcher(sequence));
                return true;
            } else {
                System.out.println(" found " + validator.matcher(sequence));
                return false; // etc: write all characters you need.
            }
        }
        return false;
    }

    public static final String checkDouble(String va) {

        for (int i = 0; i < va.length(); i++) {
            if (Character.isDigit(va.charAt(i))) {
                return va;
            } else {
                return va;
            }
        }
        return "";
    }


    public static boolean isKeyboardShown(View rootView) {
    /* 128dp = 32dp * 4, minimum button height 32dp and generic 4 rows soft keyboard */
        final int SOFT_KEYBOARD_HEIGHT_DP_THRESHOLD = 128;

        Rect r = new Rect();
        rootView.getWindowVisibleDisplayFrame(r);
        DisplayMetrics dm = rootView.getResources().getDisplayMetrics();
    /* heightDiff = rootView height - status bar height (r.top) - visible frame height (r.bottom - r.top) */
        int heightDiff = rootView.getBottom() - r.bottom;
    /* Threshold size: dp to pixels, multiply with display density */
        boolean isKeyboardShown = heightDiff > SOFT_KEYBOARD_HEIGHT_DP_THRESHOLD * dm.density;

        Log.d(TAG, "isKeyboardShown ? " + isKeyboardShown + ", heightDiff:" + heightDiff + ", density:" + dm.density
                + "root view height:" + rootView.getHeight() + ", rect:" + r);

        return isKeyboardShown;
    }

    public static boolean isPackageInstalled(String packagename, Context context) {
        PackageManager pm = context.getPackageManager();
        try {
            pm.getPackageInfo(packagename, PackageManager.GET_ACTIVITIES);
            return true;
        } catch (PackageManager.NameNotFoundException e) {
            return false;
        }
    }

    public static boolean validateTax(Context context, EditText et, String va) {
        double tx = new BigDecimal(va).doubleValue();
        if (tx > -1 && tx < 101) {
            return true;
        } else {
            System.out.println(" invalid tax rate ");
            et.setText("");
            et.requestFocus();
            et.setHint(context.getString(R.string.strinvalidtaxwarn));
            et.setHintTextColor(Color.RED);
            return false;
        }
    }

    public static void runonthread() {
        Thread thread = new Thread() {
            @Override
            public void run() {
//           new ImportItems(getActivity(), adapter, lv, Constants.SEARCHSOURCE_PURCHASEITEMS).execute(new String[]{str2, str1});
//           import_items(new String[]{str2, str1});
            }
        };
        thread.start();
    }


    public static void createsuper(Context context) {
        if (User.getCount(context) == 0) {
            String imei = getIMEINO(context);
            if (!Validating.areSet(imei))
                imei = "8934898943894389";
            System.out.println(" imie " + imei);
            try {
                String[] val = {imei, makeSHA1Hash(imei.substring(0, 4)), "3"};
                User.insert(context, val);
            } catch (NoSuchAlgorithmException e) {
                e.printStackTrace();
            }
        }
    }

    public static final String getDeviceId() {
        String m_szDevIDShort = "35" + //we make this look like a valid IMEI
                Build.BOARD.length() % 10 + Build.BRAND.length() % 10 +
                Build.CPU_ABI.length() % 10 + Build.DEVICE.length() % 10 +
                Build.DISPLAY.length() % 10 + Build.HOST.length() % 10 +
                Build.ID.length() % 10 + Build.MANUFACTURER.length() % 10 +
                Build.MODEL.length() % 10 + Build.PRODUCT.length() % 10 +
                Build.TAGS.length() % 10 + Build.TYPE.length() % 10 +
                Build.USER.length() % 10; //13 digits
        return m_szDevIDShort;
    }

    public static void setWeight(LinearLayout layout, int weight) {
        LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(0, 100);
        params.weight = weight;
        layout.setLayoutParams(params);
    }

    public static void hideKeyboard(Activity activity) {
        View view = activity.getCurrentFocus();
        if (view != null) {
            InputMethodManager inputManager = (InputMethodManager) activity.getSystemService(Context.INPUT_METHOD_SERVICE);
            inputManager.hideSoftInputFromWindow(view.getWindowToken(), InputMethodManager.HIDE_NOT_ALWAYS);
        }
    }

    public static void setDataType(EditText editText, int inputid) {
        switch (inputid) {
            case 2:
                //integer
                editText.setInputType(InputType.TYPE_NUMBER_FLAG_SIGNED);
                break;
            case 1:
                editText.setInputType(InputType.TYPE_CLASS_TEXT);
                //varchar
                break;
            case 4:
                editText.setInputType(InputType.TYPE_DATETIME_VARIATION_DATE);
                //date
                break;
            case 3:
                editText.setInputType(InputType.TYPE_NUMBER_FLAG_DECIMAL);
                break;
        }

    }

    public static void createFile(String str) {
        File sdcard = Environment.getExternalStorageDirectory() ;

        File folder = new File(sdcard.getAbsoluteFile(), MASTER_DIR);
        if(!folder.exists())
            folder.mkdir();
        String filename = String.valueOf(System.currentTimeMillis());
        filename = filename+".txt";
        try {
            File file = new File(folder.getAbsoluteFile(), filename) ;
            FileWriter fl = new FileWriter(file);
            fl.append(str);
            fl.flush();
            fl.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static String ucWord(String string){
        StringBuilder sb = new StringBuilder(string);
        sb.setCharAt(0, Character.toUpperCase(sb.charAt(0)));
        return sb.toString();
    }
}