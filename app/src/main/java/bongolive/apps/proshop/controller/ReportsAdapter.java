/*
 * Copyright (c) 2015. All rights reserved. The source codes are property of bongolive
 */

package bongolive.apps.proshop.controller;

import android.content.Context;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.ArrayList;
import java.util.List;

import bongolive.apps.proshop.R;

public class ReportsAdapter extends BaseAdapter  {
    String action;
    private LayoutInflater mInflater;
    private List<ReportsItem> mItems = new ArrayList<ReportsItem>();
    Context context;
    AppPreference appPreference;
    boolean istablet = false;
    public ReportsAdapter() {

    }

    public ReportsAdapter(Context contexts, List<ReportsItem> items, String source) {
        mInflater = LayoutInflater.from(contexts);
        context = contexts;
        mItems = items;
        action = source;
        istablet = context.getResources().getBoolean(R.bool.isTablet);
    }

    /* (non-Javadoc)
     * @see android.widget.Adapter#getCount()
     */
    @Override
    public int getCount() {
        return mItems.size();
    }

    /* (non-Javadoc)
     * @see android.widget.Adapter#getItem(int)
     */
    @Override
    public Object getItem(int position) {
        return mItems.get(position);
    }

    /* (non-Javadoc)
     * @see android.widget.Adapter#getItemId(int)
     */
    @Override
    public long getItemId(int arg0) {
        return 0;
    }

    /* (non-Javadoc)
     * @see android.widget.Adapter#getView(int, android.view.View, android.view.ViewGroup)
     */
    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ItemViewHolder holder;
        if(action.equals(Constants.SALEREPORT) || action.equals(Constants.DELIVERYHISTORY)  || action.equals
                (Constants.ORDERREPORT) || action.equals(Constants.STATIONHISTORY) ){
            if (convertView == null) {
                convertView = mInflater.inflate(R.layout.sales_report, null);
                holder = new ItemViewHolder();
                holder.txtitem1 = (TextView) convertView.findViewById(R.id.txtitem1);
                holder.txtitem2 = (TextView) convertView.findViewById(R.id.txtitem2);
                holder.txtitem3 = (TextView) convertView.findViewById(R.id.txtitem3);
                holder.txtitem4 = (TextView) convertView.findViewById(R.id.txtitem4);
                holder.txtitem5 = (TextView) convertView.findViewById(R.id.txtitem5);
                holder.txti = (TextView) convertView.findViewById(R.id.txtindex);
                convertView.setTag(holder);
            } else {
                holder = (ItemViewHolder) convertView.getTag();
            }
        } else {
            if (convertView == null) {
                convertView = mInflater.inflate(R.layout.reports_items, null);
                holder = new ItemViewHolder();
                holder.txtd = (TextView) convertView.findViewById(R.id.txtdayval);
                holder.txtd.setSingleLine(false);
                holder.txtw = (TextView) convertView.findViewById(R.id.txtweekval);
                holder.txtw.setSingleLine(false);
                holder.txth = (TextView) convertView.findViewById(R.id.txtfilter);
                holder.txth.setSingleLine(false);
                holder.txti = (TextView) convertView.findViewById(R.id.txtindexval);

                convertView.setTag(holder);
            } else {
                holder = (ItemViewHolder) convertView.getTag();
            }
        }

        appPreference = new AppPreference(context);
        String cry = appPreference.getDefaultCurrency();
        cry = cry != null ? cry : "Tsh";
        ReportsItem cl = mItems.get(position);

        String tsale = cl.getTotalsales();
        String tpaid = cl.getTotalpaymet();
        String bus = cl.getBusinessname();
        switch (action){
            case Constants.SALESBYCUSTOMER:// sales order by customer
                holder.txtd.setText(new BigDecimal(tsale).setScale(2,RoundingMode.HALF_UP).doubleValue()+ " " + cry);
                double db = Double.parseDouble(tsale);
                double tp = Double.parseDouble(tpaid);
                double diff = new BigDecimal(db).subtract(new BigDecimal(tp)).setScale(2,RoundingMode.HALF_UP)
                    .doubleValue();
                holder.txtw.setText(String.valueOf(diff) + " " + cry);
                holder.txth.setText(bus);
                holder.txti.setText(String.valueOf(position + 1));
                break;
            case Constants.SALESBYPRODUCT:// sales order by product
                holder.txtd.setText(tsale );
                holder.txtw.setText(tpaid + " " + cry);
                holder.txth.setText(bus);
                holder.txti.setText(String.valueOf(position + 1));
                break;
            case Constants.TAXBYPRODUCT:// sales order by product
                holder.txtd.setText(tpaid);
                holder.txth.setText(bus);
                holder.txtw.setText(tsale);
                holder.txti.setText(String.valueOf(position + 1));
                break;
            case Constants.PROFITREPORT:// sales order by product
                holder.txtd.setText(tsale);
                holder.txtw.setText(tpaid + " " + cry);
                holder.txth.setText(bus);
                holder.txtw.setVisibility(View.GONE);
                holder.txti.setText(String.valueOf(position + 1));
                break;
            case Constants.PRODUCTREORDER:// sales order by product
                holder.txtd.setText(tsale);
                holder.txtw.setText(tpaid);
                holder.txth.setText(bus);
                holder.txti.setText(String.valueOf(position + 1));
                break;
            case Constants.SALEREPORT:// sales order by product

                if(istablet){
                    holder.txti.setText(String.valueOf(position + 1));
                    holder.txtitem1.setText( new BigDecimal(tsale).setScale(2, RoundingMode.HALF_UP).doubleValue() + " " + cry);
                    holder.txtitem2.setText(new BigDecimal(tpaid).setScale(2,RoundingMode.HALF_UP).doubleValue() + " " + cry);
                    holder.txtitem3.setText(bus);
                    holder.txtitem4.setText( cl.getOrderdate());
                    holder.txtitem5.setText(new BigDecimal(cl.getTax()).setScale(2, RoundingMode.HALF_UP).doubleValue() + " " + cry);
                } else {
                    holder.txti.setText(String.valueOf(position + 1));
                    holder.txtitem1.setText(context.getString(R.string.strtotsales) + " " + new BigDecimal(tsale).setScale
                            (2, RoundingMode.HALF_UP).doubleValue() + " " + cry);
                    holder.txtitem2.setText(context.getString(R.string.stramountpaid) + " " + new BigDecimal(tpaid).setScale(2,
                            RoundingMode.HALF_UP).doubleValue() + " " + cry);
                    holder.txtitem3.setText(bus);
                    holder.txtitem4.setText(context.getString(R.string.strorderdate) + " " + cl.getOrderdate());
                    holder.txtitem5.setText(context.getString(R.string.strtotaltax) + " " + new BigDecimal(cl.getTax()).setScale(2,
                            RoundingMode.HALF_UP).doubleValue() + " " + cry);
                }
                break;
            case Constants.DELIVERYHISTORY:// sales order by product
                holder.txtitem5.setSingleLine(true);
                holder.txtitem1.setVisibility(View.GONE);
                holder.txtitem2.setVisibility(View.GONE);
                if(istablet){
                    holder.txti.setText(String.valueOf(position + 1));
                    holder.txtitem1.setText(tsale);
                    holder.txtitem2.setText(tpaid);
                    holder.txtitem3.setText(bus);
                    holder.txtitem4.setText(cl.getOrderdate());
                    holder.txtitem5.setText(cl.getTax());
                } else {
                    holder.txti.setText(String.valueOf(position + 1));
                    holder.txtitem1.setText(context.getString(R.string.strsaleref) + " " + tsale);
                    holder.txtitem2.setText(context.getString(R.string.strdelref) + " " + tpaid);
                    holder.txtitem3.setText(context.getString(R.string.strcustomer) + " " + bus);
                    holder.txtitem4.setText(context.getString(R.string.strdeldate) + " " + cl.getOrderdate());
                    holder.txtitem5.setText(context.getString(R.string.straddress) + " " + Html.fromHtml(cl.getTax()));
                }
                break;
            case Constants.ORDERREPORT:// sales order by product
                holder.txtitem5.setSingleLine(true);
                holder.txtitem1.setVisibility(View.GONE);
                holder.txtitem2.setVisibility(View.GONE);
                if(istablet){
                    holder.txti.setText(String.valueOf(position + 1));
                    holder.txtitem1.setText(tsale);
                    holder.txtitem2.setText(tpaid);
                    holder.txtitem3.setText(bus);
                    holder.txtitem4.setText(cl.getOrderdate());
                    holder.txtitem5.setText(cl.getTax());
                } else {
                    holder.txti.setText(String.valueOf(position + 1));
                    holder.txtitem1.setText(context.getString(R.string.strsaleref) + " " + tsale);
                    holder.txtitem2.setText(context.getString(R.string.strdelref) + " " + tpaid);
                    holder.txtitem3.setText(context.getString(R.string.strcustomer) + " " + bus);
                    holder.txtitem4.setText(context.getString(R.string.strdeldate) + " " + cl.getOrderdate());
                    holder.txtitem5.setText(context.getString(R.string.straddress) + " " + Html.fromHtml(cl.getTax()));
                }
            case Constants.STATIONHISTORY:// sales order by product
                holder.txtitem5.setSingleLine(true);
                holder.txtitem1.setVisibility(View.GONE);
                holder.txtitem2.setVisibility(View.GONE);
                if(istablet){
                    holder.txti.setText(String.valueOf(position + 1));
                    holder.txtitem1.setText(cl.getStname());
                    holder.txtitem2.setText(cl.getOpname());
                    holder.txtitem3.setText(cl.getCreated());
                    holder.txtitem4.setText(cl.getOrderdate());
                    holder.txtitem5.setText(cl.getTax());
                } else {
                    holder.txti.setText(String.valueOf(position + 1));
                    holder.txtitem3.setText(context.getString(R.string.strstation) + " " + cl.getStname());
                    holder.txtitem4.setText(context.getString(R.string.strstmastername) + " " + cl.getOpname());
                    holder.txtitem5.setText(context.getString(R.string.strsubdate) + " " + cl.getCreated());
                }
                break;
        }
        return convertView;
    }

    static class ItemViewHolder {
        TextView txtd;
        TextView txtw;
        TextView txth;
        TextView txti;

        TextView txtitem1;
        TextView txtitem2;
        TextView txtitem3;
        TextView txtitem4;
        TextView txtitem5;
    }

}
