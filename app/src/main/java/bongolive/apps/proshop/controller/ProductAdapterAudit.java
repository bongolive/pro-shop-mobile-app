package bongolive.apps.proshop.controller;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import java.util.ArrayList;

import bongolive.apps.proshop.R;
import bongolive.apps.proshop.model.AuditResponses;

/**
 * Created by nasznjoka on 11/24/15.
 */
public class ProductAdapterAudit extends ArrayAdapter<String> {
    long[] extras;
    ArrayList prodids ;

    public ProductAdapterAudit(Context context, ArrayList<String> objects, ArrayList<Long> proids, long[] extras) {
        super(context,0, objects);
        this.extras = extras;
        this.prodids = proids;
    }
    @Override
        public View getView(int position, View convertView, ViewGroup parent) {
        TextView text = (TextView) convertView;

        if (text == null) {
            text = (TextView) LayoutInflater.from(getContext()).inflate(android.R.layout.simple_spinner_dropdown_item, parent, false);
        }
        String prod = getItem(position);
        String prodid = prodids.get(position).toString();
//        System.out.print("prod id "+prodid);
        long prodl = Long.parseLong(prodids.get(position).toString());

        boolean dne = AuditResponses.getCustResponse(getContext(), new long[]{extras[0], prodl, extras[1]});
//        System.out.println(" done ?? "+dne);
        if(dne)
            text.setCompoundDrawablesWithIntrinsicBounds(0,0, R.drawable.ic_action_tick_green,0);

        text.setText(prod);

        return text;
    }

        @Override
        public View getDropDownView(int position, View convertView, ViewGroup parent) {
            TextView text = (TextView) convertView;

            if (text == null) {
                text = (TextView) LayoutInflater.from(getContext()).inflate(android.R.layout.simple_spinner_dropdown_item,  parent, false);
            }

            String prod = getItem(position);
            long prodl = Long.parseLong(prodids.get(position).toString());

            boolean dne = AuditResponses.getCustResponse(getContext(), new long[]{extras[0], prodl, extras[1]});
            if(dne)
                text.setCompoundDrawablesWithIntrinsicBounds(0,0, R.drawable.ic_action_tick_green,0);

            text.setText(prod);

            return text;
        }
}
