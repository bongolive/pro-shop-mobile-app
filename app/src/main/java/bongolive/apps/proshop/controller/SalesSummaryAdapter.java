/*
 * The source codes in this project are owned by Bongolive Enterprises. Copying or any kind of usage without Bongolive's approval is against the law.
 * Copyright (c) 2015. 
 *
 */

package bongolive.apps.proshop.controller;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import bongolive.apps.proshop.R;
import bongolive.apps.proshop.model.Products;
import bongolive.apps.proshop.model.SalesItems;

public class SalesSummaryAdapter extends BaseAdapter {
private LayoutInflater mInflater ;
AppPreference appPreference ;
Context _context ;
private List<SalesItems> mItems = new ArrayList<>() ;
public SalesSummaryAdapter(Context context, List<SalesItems> items)
{
	mInflater = LayoutInflater.from(context);
	mItems = items ;
    this._context = context;
}  
	/* (non-Javadoc)
	 * @see android.widget.Adapter#getCount()
	 */
	@Override
	public int getCount() { 
		return mItems.size() ;
	}
	/* (non-Javadoc)
	 * @see android.widget.Adapter#getItem(int)
	 */
	@Override
	public Object getItem(int position) { 
		return mItems.get(position);
	}
	/* (non-Javadoc)
	 * @see android.widget.Adapter#getItemId(int)
	 */
	@Override
	public long getItemId(int arg0) { 
		return 0;
	}
	/* (non-Javadoc)
	 * @see android.widget.Adapter#getView(int, android.view.View, android.view.ViewGroup)
	 */
	@Override
	public View getView(int position, View convertView, ViewGroup parent) { 
		ItemViewHolder holder ;
	
		if(convertView == null)
		{
			convertView = mInflater.inflate(R.layout.sales_summary_items, null);
			holder = new ItemViewHolder() ;
			holder.txtprodname = (TextView)convertView.findViewById(R.id.txtprodname);
			holder.txtquantity = (TextView)convertView.findViewById(R.id.txtProQty);
			holder.txtsales = (TextView)convertView.findViewById(R.id.txtsales);
			holder.txtsubtotal = (TextView)convertView.findViewById(R.id.txtsubtotal);
			holder.txttax = (TextView)convertView.findViewById(R.id.txttax);
			convertView.setTag(holder);
		} else {
			holder = (ItemViewHolder)convertView.getTag();
		}
        int newsize = position+1;
		SalesItems cl = mItems.get(position);
		appPreference = new AppPreference(_context.getApplicationContext());
		String prodnm = Products.getProdName(_context, String.valueOf(cl.getProdloc()));
		String prodlable = _context.getString(R.string.strproduct);
		String qtylable = _context.getString(R.string.strquantity);
//		String subtt = _context.getString(R.string.strsubtotal);
		String taxlb = _context.getString(R.string.strtotaltax);
		String prc = _context.getString(R.string.strunitprices);

		if(!appPreference.showPricing()){
			holder.txtquantity.setVisibility(View.GONE);
			holder.txtsales.setVisibility(View.GONE);
			holder.txttax.setVisibility(View.GONE);
			holder.txtsubtotal.setText(String.valueOf(cl.getQty()));
			holder.txtprodname.setText(newsize + " : " + prodnm);
		} else {
			holder.txtprodname.setText(newsize + " : " + prodnm);
			holder.txtquantity.setText(qtylable + " : " + cl.getQty());
			holder.txtsales.setText(prc + " : " + cl.getPrice());
			holder.txtsubtotal.setText(" " + cl.getSbtotal());
			holder.txttax.setText(taxlb + " : " + cl.getTaxmt());

		}
		return convertView ;
	} 
	static class ItemViewHolder {
		TextView txtprodname ;
		TextView txtquantity ; 
		TextView txtsales ;
		TextView txtsubtotal;
		TextView txttax;
	}

}