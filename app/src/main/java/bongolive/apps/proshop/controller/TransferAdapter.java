/*
 * The source codes in this project are owned by Bongolive Enterprises. Copying or any kind of usage without Bongolive's approval is against the law.
 * Copyright (c) 2015.
 *
 */

package bongolive.apps.proshop.controller;


import android.app.Activity;
import android.graphics.Color;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import bongolive.apps.proshop.R;
import bongolive.apps.proshop.model.Customers;
import bongolive.apps.proshop.model.StockIssue;
import bongolive.apps.proshop.model.Transfer;

/**
 * Created by nasznjoka on 5/18/15.
 */

public class TransferAdapter extends RecyclerView.Adapter<TransferAdapter.Vh> {

    public List<Transfer> mItems = new ArrayList<Transfer>();
    public List<StockIssue> mItems_action = new ArrayList<StockIssue>();
    Activity context;
    AppPreference appPreference;
    String currency = null;
    private String customeridLocal = "0";
    private String customerid = "0";
    static OnItemClickListener clickListener ;
    String action = null;

    public static class Vh extends RecyclerView.ViewHolder implements View.OnClickListener {
        private LinearLayout layview;
        private TextView txtwh,txtgtotal,txtdate,txtstatus,txtsaleref ;

        public Vh(View v) {
            super(v);
            layview = (LinearLayout)v.findViewById(R.id.view_lay);
            txtwh = (TextView)v.findViewById(R.id.txtproductorwh);
            txtstatus = (TextView)v.findViewById(R.id.txtstatus);
            txtgtotal = (TextView)v.findViewById(R.id.txtgrandtotal);
            txtdate = (TextView)v.findViewById(R.id.txttransdate);
            txtsaleref = (TextView)v.findViewById(R.id.txtsaleref);
        }

        @Override
        public void onClick(View v) {
            try {
                clickListener.onItemClick(v, getLayoutPosition());
            } catch (NullPointerException e){

            }
        }
    }

    public TransferAdapter(Activity mcontext, ArrayList<Transfer> myDataset){
        mItems = myDataset;
        context = mcontext;
        appPreference = new AppPreference(context);
        currency = appPreference.getDefaultCurrency();
        action = null;
    }
    public TransferAdapter(Activity mcontext, ArrayList<StockIssue> myDataset, String action){
        mItems_action = myDataset;
        context = mcontext;
        appPreference = new AppPreference(context);
        currency = appPreference.getDefaultCurrency();
        this.action = action;
    }

    // Create new views (invoked by the layout manager)
    @Override
    public Vh onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.transfer_adapter_screen, parent, false);

        Vh vh = new Vh(v);
        return vh;
    }

    @Override
    public void onBindViewHolder(final Vh holder, final int position) {
        if(action == null) {
            final Transfer cl = mItems.get(position);
            currency = appPreference.getDefaultCurrency();
            currency = currency != null ? currency : "Tsh";
            String saleid = cl.getTransid();
            long locsaleid = cl.getId();
            String saleref = cl.getBatch();
            String amount = cl.getGtotal();
            String paydate = cl.getDate();
            String wh = cl.getWarehousefrom();
            paydate = Constants.getFormatedDate(paydate);

            String bl = context.getString(R.string.strtotalvals);
            String due = context.getString(R.string.strfromwh);
            String pd = context.getString(R.string.strtransdate);
            String srf = context.getString(R.string.strtransref);
            String sts = context.getString(R.string.strstatus);

            int approval = cl.getApproval();
            int ack = cl.getAck();
            String status = null;

            if (approval == 2) {
                if (ack == 0)
                    status = context.getString(R.string.strnotsent);
                else
                    status = context.getString(R.string.strwaitingapproval);

                holder.txtstatus.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.color.clrred, 0);
                holder.txtstatus.setTextColor(Color.parseColor("#FA7306"));
            } else if (approval == 1) {
                status = context.getString(R.string.strapproved);
                holder.txtstatus.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.color.clrpending, 0);
                holder.txtstatus.setTextColor(Color.parseColor("#FA7306"));
            } else if (approval == 3) {
                status = context.getString(R.string.strdelivered);
                holder.txtstatus.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.ic_done, 0);
                holder.txtstatus.setTextColor(Color.parseColor("#669900"));
            }

            holder.txtdate.setText(pd + " : " + paydate);
            holder.txtwh.setText(due + " : " + wh);
            holder.txtstatus.setText(sts + " : " + status);
            holder.txtsaleref.setText(srf + " : " + saleref);
            holder.txtgtotal.setText(bl + " : " + amount + " " + currency);
        } else {
            final StockIssue cl = mItems_action.get(position);
            currency = appPreference.getDefaultCurrency();
            currency = currency != null ? currency : "Tsh";

            long locsaleid = cl.getId();
            String saleref = cl.getSaleref();
            String amount = String.valueOf(cl.getTotalsales());
            String paydate = cl.getDate();
            String cu = cl.getCustlocal();
            String customer = Customers.getCustomerBusinessNameLoca(context, cu);
            paydate = Constants.getFormatedDate(paydate);

            String bl = context.getString(R.string.strtotalvals);
            String custnm = context.getString(R.string.strcustomer);
            String pd = context.getString(R.string.strsaledate);
            String srf = context.getString(R.string.strtransref);
            String sts = context.getString(R.string.strstatus);

            int ack = cl.getAck();
            String status = null;

            if (ack == 0) {
                status = context.getString(R.string.strnotsent);
                holder.txtstatus.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.color.clrred, 0);
                holder.txtstatus.setTextColor(Color.parseColor("#FA7306"));
            } else if (ack == 1) {
                status = context.getString(R.string.strsynced);
                holder.txtstatus.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.color.clrpending, 0);
                holder.txtstatus.setTextColor(Color.parseColor("#669900"));
            }

            holder.txtdate.setText(pd + " : " + paydate);
            holder.txtwh.setText(custnm + " : " + customer);
            holder.txtstatus.setText(sts + " : " + status);
            holder.txtsaleref.setText(srf + " : " + saleref);
            holder.txtgtotal.setVisibility(View.GONE);
        }
    }

    // Return the size of your dataset (invoked by the layout manager)
    @Override
    public int getItemCount() {
        if(action == null)
            return mItems.size();
        else
            return mItems_action.size();
    }

    @Override
    public long getItemId(int arg0){
        return 0;
    }

    private void changeBg(Vh h,int qty){
        if(qty == 0){
            h.txtdate.setTextColor(context.getResources().getColor(R.color.clrproduct));
        } else {
            h.txtdate.setTextColor(context.getResources().getColor(R.color.clrpending));
        }
    }

    public interface OnItemClickListener {
        public void onItemClick(View view, int position);
    }

    public void SetOnItemClickListener(final OnItemClickListener itemClickListener) {
        this.clickListener = itemClickListener;
    }

}