/*
 * Copyright (c) 2015.
 * Prosurvey is a property of Bongolive Enterprises Ltd. An authorized
 * copy of ideas, source codes is against the regulations
 */

package bongolive.apps.proshop.controller;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.text.TextUtils;
import android.util.Log;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Iterator;

import bongolive.apps.proshop.R;
import bongolive.apps.proshop.model.Customers;
import bongolive.apps.proshop.model.Expenses;
import bongolive.apps.proshop.model.SalesItems;

/**
 * Created by nasznjoka on 1/2/2015.
 */
public class AppPreference {
	private SharedPreferences _sharedPrefs,_settingsPrefs;
	private SharedPreferences.Editor _prefsEditor;
    Context mContext ;

    public AppPreference(Context context) {
        this._sharedPrefs = context.getSharedPreferences(Constants.CLIENT_DATA, Activity.MODE_MULTI_PROCESS);
        PreferenceManager.setDefaultValues(context, R.xml.pref_general, false);
        this._settingsPrefs = PreferenceManager.getDefaultSharedPreferences(context);
        this._prefsEditor = _sharedPrefs.edit();
        mContext = context;
	}

	public int getReceiptStatu() {
		return _sharedPrefs.getInt(Constants.PREFRECEIPT, 0);
	}
	public int getVatStatus() {
		return _sharedPrefs.getInt(Constants.PREFTAX, 0);
	}
    public int getDiscountStatus(){
        return  _sharedPrefs.getInt(Constants.PREFDISCOUNT, 0);
    }
    public String getDefaultLanguage()
    {
        return _settingsPrefs.getString(Constants.PREFLANG, "");
    }
    public String getDefaultSync()
    {
        return _settingsPrefs.getString(Constants.PREFSYNC, "");
    }
    public String getDefaultCurrency()
    {
        return _settingsPrefs.getString(Constants.PREFCURRENCY, null);
    }


    public int getPrintWithNoPayment()
    {
        return _sharedPrefs.getInt(Constants.PREFALLOWPRINTWITHOUTSTANDINGPAYMENT, 0);
    }

    public boolean check_firstrun()
    {
        return _sharedPrefs.getBoolean(Constants.FIRSTRUN, true);
    }
    public boolean showPricing()
    {
        return _sharedPrefs.getBoolean(Constants.SHOWPRICE, true);
    }

    public int get_accounttype()
    {
        return _sharedPrefs.getInt(Constants.DEVICE_ACCOUNTTYPE, 2);
    }

    public String getAddress()
    {
        return _sharedPrefs.getString("address", "");
    }

    public long getBuz()
    {
        return _sharedPrefs.getLong(Customers.BUZINESSNAME, 0);
    }

    public void storeList(ArrayList<SalesItems> adapter){

        /*_prefsEditor.putString(Constants.ITEMLIST,jsonArray.toString());
        _prefsEditor.commit();*/
    }
    public ArrayList<HashMap<String,String>> getList(){
        ArrayList<HashMap<String, String>> adapters = null;
        String json = _sharedPrefs.getString(Constants.ITEMLIST, "");
        try {
            JSONArray jsonArray = new JSONArray(json);
            System.out.println("jsonarray count "+jsonArray.length());
            for(int i = 0; i < jsonArray.length(); i++){
                HashMap<String, String> item = new HashMap<>();
                String obj = (String)jsonArray.get(i);
                JSONObject job = new JSONObject(obj);
                Iterator<String> it = job.keys();
                while (it.hasNext()){
                    String ky = it.next();
                    item.put(ky,(String)job.get(ky));
                }
                adapters.add(item);
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
        System.out.println("jsonarray count "+adapters.size());
        return adapters;
    }
    public String[] getAccountInfo(){
        String[] info = {
           _sharedPrefs.getString(Constants.BUSINESSNAME, ""),
           _sharedPrefs.getString(Constants.CONTACTP, ""),
           _sharedPrefs.getString(Constants.PHONE,"")
        };
        return info;
    }


    public String getLastSync()
    {
        return _sharedPrefs.getString(Constants.LASTSYNC, "");
    }

    public String getAuthkey()
    {
        return _sharedPrefs.getString(Constants.AUTHTOKEN, "");
    }
    public float[] getLastKnowLocation()
    {
        float lat = _sharedPrefs.getFloat(Constants.PREFLASTLAT, Float.valueOf("-6.8368537"));
        float lng = _sharedPrefs.getFloat(Constants.PREFLASTLAT, Float.valueOf("39.2519426"));
        return new float[]{lat,lng};
    }
    public String getProductCode()
    {
        return _sharedPrefs.getString(Constants.PREFPRODUCTSKU, "");
    }

	public void saveVatChoice(int id) {
		_prefsEditor.putInt(Constants.PREFTAX, id);
		_prefsEditor.commit();
	}
	public void saveLastLocation(float[] vals) {
		_prefsEditor.putFloat(Constants.PREFLASTLAT, vals[0]);
		_prefsEditor.putFloat(Constants.PREFLASTLNG, vals[1]);
		_prefsEditor.commit();
	}

	public void saveProductCode(String id) {
		_prefsEditor.putString(Constants.PREFPRODUCTSKU, id);
		_prefsEditor.commit();
	}
    public void saveDiscountStatus(int id) {
        _prefsEditor.putInt(Constants.PREFDISCOUNT, id);
        _prefsEditor.commit();
    }
    public void setShowPrice(boolean id) {
        _prefsEditor.putBoolean(Constants.SHOWPRICE, id);
        _prefsEditor.commit();
    }
    public void removeShowPrice() {
        _prefsEditor.remove(Constants.SHOWPRICE);
        _prefsEditor.commit();
    }
    public void savePrintWithNoPaymentStatus(int id) {
        _prefsEditor.putInt(Constants.PREFALLOWPRINTWITHOUTSTANDINGPAYMENT, id);
        _prefsEditor.commit();
    }
    public void setDefaultCurrency(String id){
        _prefsEditor.putString(Constants.PREFCURRENCY, id);
        _prefsEditor.commit();
    }
    public void clearAddress(){
        _prefsEditor.remove(Constants.PREFADDRESS);
        _prefsEditor.commit();
    }
    public void clearCustomer(){
        _prefsEditor.remove(Customers.BUZINESSNAME);
        _prefsEditor.commit();
    }
    public void saveCustomer(long cus){
        _prefsEditor.putLong(Customers.BUZINESSNAME, cus);
        _prefsEditor.commit();
    }
    public void saveAddress(String address){
        _prefsEditor.putString(Constants.PREFADDRESS, address);
        _prefsEditor.commit();
    }

    public void savePrintReceiptChoice(int id) {
        _prefsEditor.putInt(Constants.PREFRECEIPT, id);
        _prefsEditor.commit();
    }


    public boolean getBluetoothoption()
    {
        return this._sharedPrefs.getBoolean(Constants.PREFBLUETOOTH, false);
    }

    public boolean getPinOptions()
    {
        return this._sharedPrefs.getBoolean(Constants.PREFPIN, false);
    }

    public String getDbnm()
    {
        return this._sharedPrefs.getString(Constants.KEY_DB, "");
    }

    public String getDbpath()
    {
        return this._sharedPrefs.getString(Constants.KEY_DBPATH, "");
    }


    public void reset_auth()
    {
        this._sharedPrefs.edit().remove(Constants.USERID);
        this._sharedPrefs.edit().remove(Constants.USERROLE).apply();
        Log.v("PATHRESETTING", "PATH is reset " + getDbnm());
    }

    public void reset_path()
    {
        this._sharedPrefs.edit().remove(Constants.KEY_DBPATH).commit();
        Log.v("PATHRESETTING", "PATH is reset " + getDbpath());
    }
    public void save_auth(String vals){
        _prefsEditor.putString(Constants.AUTHTOKEN, vals);
        _prefsEditor.commit();
    }

    public void storeExpenseType(String type){
        ArrayList<String> arrayList = getExpenseType();
        arrayList.add(type);
        this._prefsEditor.putString(Expenses.EXPENSETYPE, TextUtils.join(",", arrayList));
        this._prefsEditor.commit();
    }

    public ArrayList<String> getExpenseType(){
        String exlist = this._sharedPrefs.getString(Expenses.EXPENSETYPE, null);
        ArrayList<String> ex = new ArrayList<>();
        if(Validating.areSet(exlist)) {
            String[] lst = TextUtils.split(exlist, ",");
            ex = new ArrayList<>(Arrays.asList(lst));
        }
        return ex;
    }
    public void saveDbPath(String paramString)
    {
        this._prefsEditor.putString(Constants.KEY_DBPATH, paramString);
        this._prefsEditor.commit();
    }

    public void save_pin_options(boolean paramString)
    {
        this._prefsEditor.putBoolean(Constants.PREFPIN, paramString);
        this._prefsEditor.commit();
    }


    public void save_buss(String vals){
        _prefsEditor.putString(Constants.BUSINESSNAME, vals);
        _prefsEditor.commit();
    }


    public void save_contactp(String vals){
        _prefsEditor.putString(Constants.CONTACTP, vals);
        _prefsEditor.commit();
    }


    public void save_phone(String vals){
        _prefsEditor.putString(Constants.PHONE, vals);
        _prefsEditor.commit();
    }

    public void save_accounttype(int vals){
        _prefsEditor.putInt(Constants.DEVICE_ACCOUNTTYPE, vals);
        _prefsEditor.commit();
    }

    public void clear_authentications(){
        _prefsEditor.remove(Constants.DEVICE_ACCOUNTTYPE);
        _prefsEditor.remove(Constants.AUTHTOKEN);
        _prefsEditor.remove(Constants.BUSINESSNAME);
        _prefsEditor.remove(Constants.CONTACTP);
        _prefsEditor.remove(Constants.PHONE);
        _prefsEditor.commit();
    }

    public void set_bluetooth(boolean key){
        _prefsEditor.putBoolean(Constants.PREFBLUETOOTH, key);
        _prefsEditor.commit();
    }

    public void set_firstrun(){
        _prefsEditor.putBoolean(Constants.FIRSTRUN, false);
        _prefsEditor.commit();
    }

    public void save_last_sync(String date){
        _prefsEditor.putString(Constants.LASTSYNC, date);
        _prefsEditor.commit();
    }

    public int getUserId() {
        return _sharedPrefs.getInt(Constants.USERID, 0);
    }

    public int getUserRole() {
        return _sharedPrefs.getInt(Constants.USERROLE, 100);
    }

    public void saveUserRole(int role) {
        _prefsEditor.putInt(Constants.USERROLE, role);
        _prefsEditor.commit();
    }


    public void saveUserId(int id) {
        _prefsEditor.putInt(Constants.USERID, id);
        _prefsEditor.commit();
    }

}
