/*
 * The source codes in this project are owned by Bongolive Enterprises. Copying or any kind of usage without Bongolive's approval is against the law.
 * Copyright (c) 2015.
 *
 */

package bongolive.apps.proshop.controller;


import android.app.Activity;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.ArrayList;
import java.util.List;

import bongolive.apps.proshop.R;
import bongolive.apps.proshop.model.Pending_Items;
import bongolive.apps.proshop.model.Products;
import bongolive.apps.proshop.model.Settings;
import bongolive.apps.proshop.model.Warehouse;
import bongolive.apps.proshop.view.SalesProductsItems;
import bongolive.apps.proshop.view.SingeItem;

/**
 * Created by nasznjoka on 5/18/15.
 */

public class SalesAdapter extends BaseAdapter {

    public List<SalesProductsItems> mItems = new ArrayList<SalesProductsItems>();
    public static List<SalesProductsItems> filteredIt = new ArrayList<SalesProductsItems>();
    Activity context;
    AppPreference appPreference;
    private EditText etfilter;
    private TextView txtt,txti;
    private ImageView imgrmv,imgnext;
    private RelativeLayout rl ;
    private int items = 0;
    private double gtotal = 0;
    private ArrayList<Pending_Items> pitems = new ArrayList<>();
    String prodname = null;
    String currency = null;
    private String saletype;
    final int[] qty = {0};
    final double[] unit = {0} ;
    final double[] subtotal = {0};

    final String[] s_qty = {"0"};
    final String[] s_subtotal = {"0"};
    String s_unit = "";
    private boolean __taxsettings = false;
    private String[] businesslist;
    private ArrayAdapter<String> custadapters;
    private AutoCompleteTextView bzn;
    private String customeridLocal = "0";
    private String customerid = "0";
    protected static final int ADD_STATUS_FULL = 3;
    protected static final int ADD_STATUS_GONE = 0;
    protected static final int ADD_STATUS_SELECTED = 2;
    protected static final int ADD_STATUS_UNSELECTED = 1;
    private static final int STATE_REGULAR_CELL = 2;
    private static final int STATE_SECTIONED_CELL = 1;

    public static class Vh  {
        private LinearLayout layactions;
        private LinearLayout layview;
        private Button imgadd,imgminus;
        private TextView txtproduct,txtsubtotal,txtunit,txtqty ;

        public Vh(View v) {
            layactions = (LinearLayout)v.findViewById(R.id.actions_lay);
            layview = (LinearLayout)v.findViewById(R.id.view_lay);
            imgadd = (Button)v.findViewById(R.id.action_plus);
            imgminus = (Button)v.findViewById(R.id.action_minus);
            txtqty = (TextView)v.findViewById(R.id.action_view);
            txtproduct = (TextView)v.findViewById(R.id.action_product);
            txtsubtotal = (TextView)v.findViewById(R.id.action_total);
            txtunit = (TextView)v.findViewById(R.id.action_unit);
        }
    }

    public SalesAdapter(Activity mcontext, ArrayList<SalesProductsItems> myDataset, ImageView nxt,String source,
                        RelativeLayout rl_) {
        mItems = myDataset;
        context = mcontext;
        rl = rl_;
        txtt = (TextView)rl.getChildAt(0);
        txti = (TextView)rl.getChildAt(1);
        imgrmv = (ImageView)rl.getChildAt(2);
        imgnext = nxt;
        pitems = Pending_Items.getItems(context, source);
        saletype = source;
        appPreference = new AppPreference(context);
        __taxsettings = Settings.getTaxEnabled(context);
        currency = appPreference.getDefaultCurrency() ;
        currency = currency != null ? currency : "Tsh";
    }

    @Override
    public int getCount() {
        return mItems.size();
    }

    @Override
    public Object getItem(int position) {
        return mItems.get(position);
    }

    @Override
    public long getItemId(int arg0){
        return 0;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        final Vh vh ;
        if(convertView == null){
            convertView = LayoutInflater.from(context).inflate(R.layout.selling_screen_sellitems,null);
            vh = new Vh(convertView);
            convertView.setTag(vh);
        } else {
            vh = (Vh)convertView.getTag();
        }

        final SalesProductsItems cl = mItems.get(position);

//        System.out.println("chidl is "+ch.getVisibility());
        if(pitems != null && pitems.size() > 0){
            for(int j = 0; j < pitems.size(); j++){
                Pending_Items _pitems = pitems.get(j);
                long proid = cl.getProdid();
                long _proid = _pitems.getProdloc();
                if(proid == _proid){
                    prodname = cl.getProduct();
                    qty[0] = new BigDecimal(_pitems.getQty()).intValue();
                    unit[0] = _pitems.getPrice();
                } else {
                    prodname = cl.getProduct();
                    qty[0] = new BigDecimal(cl.getQuantity()).intValue();
                    unit[0] = cl.getUnit();
                }
            }
        } else {
            prodname = cl.getProduct();
            qty[0] = new BigDecimal(cl.getQuantity()).intValue();
            unit[0] = cl.getUnit();
        }
        items = pitems.size();
        if(items > 0)
            imgnext.setVisibility(View.VISIBLE);
        else
            imgnext.setVisibility(View.GONE);

        vh.txtproduct.setText(prodname);
        updateViews(qty[0], vh);

        if(qty[0] > 0){
            System.out.println(" qty is " + s_qty);
            vh.imgminus.setVisibility(View.GONE);
            vh.imgadd.setVisibility(View.GONE);
            vh.txtqty.setText(s_qty[0]);
            vh.txtunit.setText(s_unit);
            vh.txtsubtotal.setText(s_subtotal[0]);
        } else {
            vh.imgminus.setVisibility(View.GONE);
            vh.txtqty.setVisibility(View.GONE);
            vh.txtunit.setText(s_unit);
            vh.txtsubtotal.setVisibility(View.GONE);
        }


        vh.imgadd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                qty[0] += 1;
                if(Products.checkQty(context, cl.getProdid(), qty[0])) {
                    if (qty[0] == 1) {
                        s_qty[0] = String.valueOf(qty[0]);

                        vh.txtqty.setText(s_qty[0]);
                        vh.txtqty.setVisibility(View.VISIBLE);
                        vh.imgminus.setVisibility(View.VISIBLE);

                        process_item(cl, vh, position);

                        vh.txtsubtotal.setVisibility(View.VISIBLE);
                    } else {
                        s_qty[0] = String.valueOf(qty[0]);
                        vh.txtqty.setText(s_qty[0]);
                        process_item(cl, vh,position);
                        vh.txtsubtotal.setVisibility(View.VISIBLE);
                    }
                } else {
                    Toast.makeText(context, cl.getProduct() + " " + context.getResources().getString(R.string
                            .strsalesquantityerror1) +
                            " " + Products.getItemOnHand(context, String.valueOf(cl.getProdid())) + " " + context.getResources()
                            .getString(R.string.strsalesquantityerror2), Toast.LENGTH_LONG).show();
                }
            }
        });

        vh.layview.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (vh.imgminus.getVisibility() == View.VISIBLE) {
                    vh.imgminus.setVisibility(View.GONE);
                    if (qty[0] == 0) {
                        vh.imgadd.setVisibility(View.VISIBLE);
                        vh.txtqty.setVisibility(View.GONE);
                    } else {
                        vh.txtqty.setVisibility(View.VISIBLE);
                        vh.imgadd.setVisibility(View.GONE);
                        vh.txtqty.setText(s_qty[0]);
                    }
                } else {
                    Intent intent = new Intent(context, SingeItem.class);
                    intent.putExtra(Constants.SOURCE, Constants.SALETYPE_SALE);
                    intent.putExtra(Products.PRODUCTNAME, cl.getProdid());
                    intent.putExtra(Products.ID, position);
                    context.startActivity(intent);
                }
            }
        });

        vh.txtqty.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (vh.imgadd.getVisibility() == View.GONE) {
                    vh.txtqty.setText(s_qty[0]);
                    vh.imgadd.setVisibility(View.VISIBLE);
                    vh.imgminus.setVisibility(View.VISIBLE);
                }
            }
        });

        return convertView;
    }

    private void updateViews(int qt, Vh vh){
        double un = unit[0];
        double st = new BigDecimal(qt).multiply(new BigDecimal(un)).doubleValue();
        s_qty[0] = String.valueOf(qt);
        s_subtotal[0] =  new BigDecimal(st).setScale(2, RoundingMode.HALF_UP).toString();

        s_subtotal[0] = currency + " " + new BigDecimal(subtotal[0]).setScale(2, RoundingMode.HALF_UP).toString();
        s_unit = currency+" "+new BigDecimal(unit[0]).setScale(2, RoundingMode.HALF_UP).toString();

        vh.txtsubtotal.setText(s_subtotal[0]);
        vh.txtunit.setText(s_unit);
        vh.txtsubtotal.setVisibility(View.VISIBLE);
        if(qt > 0)
            imgnext.setVisibility(View.VISIBLE);
        else
            imgnext.setVisibility(View.GONE);

        gtotal += subtotal[0];
        txtt.setText(currency + " " + new BigDecimal(gtotal).setScale(2, RoundingMode.HALF_UP).toString());
        txti.setText("/ " + items + " " + context.getString(R.string.stritems));
    }

    private void process_item(SalesProductsItems cl,Vh holder, int pos) {
        String locprod = String.valueOf(cl.getProdid());
        String prodno = Products.getSysProdidBasedOnLocalId(context, locprod);
        String whid = Warehouse.getDefaultId(context);

        int q =  new BigDecimal(qty[0]).intValue();
        int taxmethod = Products.getTaxmethod(context, cl.getProdid());
        double tx = Products.getTax(context, cl.getProdid());
        double pr = Products.getUnitPrice(context, cl.getProdid());
        double maxdis = Settings.getDiscountLimit(context);
        double taxamount = 0;
        double netsaleprice = 0;
        double saleprice = 0;
        double subtotal = 0;
        long prodlocal = cl.getProdid();

        long i = 0;
        double tax = Constants.itemtax(pr, tx, taxmethod, __taxsettings);
        netsaleprice = Constants.netprice(pr, tx, taxmethod, __taxsettings);
        saleprice = Constants.saleprice(pr, tax, taxmethod, __taxsettings);

        subtotal = Constants.calculate_grandtotal(saleprice, q);
        double netsubtotal = Constants.calculate_grandtotal(netsaleprice, q);
        taxamount = new BigDecimal(subtotal).subtract(new BigDecimal(netsubtotal)).setScale(2,
                RoundingMode.HALF_UP).doubleValue();

        String spr = String.valueOf(saleprice);
        String nspr = String.valueOf(netsaleprice);
        String txa = String.valueOf(taxamount);
        String stxr = String.valueOf(tx);

        String[] vals = {prodno,locprod,s_qty[0],nspr,spr,txa,String.valueOf(subtotal),whid,"0",stxr,Constants
                .SALETYPE_SALE,String.valueOf(pos)};
        if(Validating.areSet(vals)){
            if(!Pending_Items.isProdIdExist(context,new String[]{vals[1],vals[10]})){
                boolean insert = Pending_Items.insertItems(context,vals);
                pitems = Pending_Items.getItems(context,Constants.SALETYPE_SALE);
                items = pitems.size();
                updateViews(qty[0],holder);
            } else {
                boolean insert = Pending_Items.updateItems(context, vals);
                pitems = Pending_Items.getItems(context,Constants.SALETYPE_SALE);
                items = pitems.size();
                updateViews(qty[0],holder);
            }
        }
    }

}