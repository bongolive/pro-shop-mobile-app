/*
 * The source codes in this project are owned by Bongolive Enterprises. Copying or any kind of usage without Bongolive's approval is against the law.
 * Copyright (c) 2015.
 *
 */

package bongolive.apps.proshop.controller;


import android.app.Activity;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.ArrayList;
import java.util.List;

import bongolive.apps.proshop.R;
import bongolive.apps.proshop.model.Pending_Items;
import bongolive.apps.proshop.model.Products;
import bongolive.apps.proshop.model.Settings;

/**
 * Created by nasznjoka on 5/18/15.
 */

public class SalesSummaryProductsAdapter extends RecyclerView.Adapter<SalesSummaryProductsAdapter.Vh>
        implements View.OnClickListener {

    public List<Pending_Items> mItems = new ArrayList<Pending_Items>();
    Activity context;
    AppPreference appPreference;
    private TextView txtt,txti;
    private RelativeLayout rl ;
    private int items = 0;
    private double gtotal = 0;
    String prodname = null;
    String currency = null;
    private String saletype;
    final int[] qty = {0};
    final double[] unit = {0} ;
    final double[] subtotal = {0};

    final String[] s_qty = {"0"};
    final String[] s_subtotal = {"0"};
    String s_unit = "";
    private boolean __taxsettings = false;

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.imgnext:
        }
    }

    public static class Vh extends RecyclerView.ViewHolder {
        private LinearLayout layactions;
        private LinearLayout layview;
        private Button imgadd,imgminus;
        private TextView txtproduct,txtsubtotal,txtunit,txtqty ;

        public Vh(View v) {
            super(v);
            layactions = (LinearLayout)v.findViewById(R.id.actions_lay);
            layview = (LinearLayout)v.findViewById(R.id.view_lay);
            imgadd = (Button)v.findViewById(R.id.action_plus);
            imgadd.setVisibility(View.GONE);
            imgminus = (Button)v.findViewById(R.id.action_minus);
            imgminus.setVisibility(View.GONE);
            txtqty = (TextView)v.findViewById(R.id.action_view);
            txtproduct = (TextView)v.findViewById(R.id.action_product);
            txtsubtotal = (TextView)v.findViewById(R.id.action_total);
            txtunit = (TextView)v.findViewById(R.id.action_unit);
        }
    }

    public SalesSummaryProductsAdapter(Activity mcontext, ArrayList<Pending_Items> myDataset,
                                       RelativeLayout rl_, String source) {
        mItems = myDataset;
        context = mcontext;
        rl = rl_;
        txtt = (TextView)rl.getChildAt(0);
        txti = (TextView)rl.getChildAt(1);
        saletype = source;
        appPreference = new AppPreference(context);
        __taxsettings = Settings.getTaxEnabled(context);
        currency = appPreference.getDefaultCurrency();
        currency = currency != null ? currency : "Tsh";
    }

    // Create new views (invoked by the layout manager)
    @Override
    public Vh onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.selling_screen_sellitems, parent, false);

        Vh vh = new Vh(v);
        return vh;
    }

    @Override
    public void onBindViewHolder(final Vh holder, final int position) {
        final Pending_Items cl = mItems.get(position);

        prodname = Products.getProdName(context,String.valueOf(cl.getProdloc()));
        qty[0] = new BigDecimal(cl.getQty()).intValue();
        unit[0] = cl.getPrice();
        subtotal[0] = cl.getSbtotal();

        items = mItems.size();

        holder.txtproduct.setText(prodname);

        updateViews(qty[0], holder);

    }

    // Return the size of your dataset (invoked by the layout manager)
    @Override
    public int getItemCount() {
        return mItems.size();
    }

    @Override
    public long getItemId(int arg0){
        return 0;
    }

    private void updateViews(int qt, Vh holder){
        s_qty[0] = String.valueOf(qt);
        s_subtotal[0] = currency + " " + new BigDecimal(subtotal[0]).setScale(2, RoundingMode.HALF_UP).toString();
        s_unit = currency+" "+new BigDecimal(unit[0]).setScale(2, RoundingMode.HALF_UP).toString();

        holder.txtsubtotal.setText(s_subtotal[0]);
        holder.txtunit.setText(s_unit);
        holder.txtsubtotal.setVisibility(View.VISIBLE);
        holder.txtqty.setText(s_qty[0]);

        gtotal += subtotal[0];
        txtt.setText(currency + " " + new BigDecimal(gtotal).setScale(2, RoundingMode.HALF_UP).toString());
        txti.setText("/ " + items + " " + context.getString(R.string.stritems));
    }
}