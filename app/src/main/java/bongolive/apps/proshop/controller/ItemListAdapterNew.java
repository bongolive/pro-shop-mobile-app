/*
 * Copyright (c) 2015. All rights reserved. The source codes are property of bongolive
 */

package bongolive.apps.proshop.controller;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import bongolive.apps.proshop.R;

public class ItemListAdapterNew extends BaseAdapter {
    int action;
    private LayoutInflater mInflater;
    private List<ItemList> mItems = new ArrayList<ItemList>();
    Context _context ;
    MenuItem[] menuitems;
    ItemViewHolder holder;
    ItemList cl;
    public ItemListAdapterNew() {

    }

    public ItemListAdapterNew(Context context, List<ItemList> items, int source, MenuItem[] menuItems) {
        mInflater = LayoutInflater.from(context);
        mItems = items;
        action = source;
        menuitems = menuItems;
        _context = context;
    }

    /* (non-Javadoc)
     * @see android.widget.Adapter#getCount()
     */
    @Override
    public int getCount() {
        return mItems.size();
    }

    /* (non-Javadoc)
     * @see android.widget.Adapter#getItem(int)
     */
    @Override
    public Object getItem(int position) {
        return mItems.get(position);
    }

    /* (non-Javadoc)
     * @see android.widget.Adapter#getItemId(int)
     */
    @Override
    public long getItemId(int arg0) {
        return 0;
    }

    /* (non-Javadoc)
     */
    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        if (convertView == null) {
            convertView = mInflater.inflate(R.layout.list_item, null);
            holder = new ItemViewHolder();
            holder.txtindex = (TextView) convertView.findViewById(R.id.txtindex);
            holder.txtName = (TextView) convertView.findViewById(R.id.txtitem);
            holder.txtName1 = (TextView) convertView.findViewById(R.id.txtitem1);
            holder.txtName2 = (TextView) convertView.findViewById(R.id.txtitem2);
            holder.txtName3 = (TextView) convertView.findViewById(R.id.txtitemextra);
            holder.txtName4 = (TextView) convertView.findViewById(R.id.txtitem4);
            convertView.setTag(holder);
        } else {
            holder = (ItemViewHolder) convertView.getTag();
        }
        cl = mItems.get(position);

        holder.txtindex.setText(String.valueOf(position+1));
        holder.txtName.setText(cl.getitemname());
        holder.txtName1.setText(cl.getitemname1());
        holder.txtName2.setText(cl.getitemname2());
        String item3 = cl.getitemname3();
        if (Validating.areSet(item3)) {
            holder.txtName3.setVisibility(View.VISIBLE);
            holder.txtName3.setText(item3);
        }
//		if(!cl.getitemname4().isEmpty()){
        holder.txtName4.setVisibility(View.VISIBLE);
        holder.txtName4.setText(cl.getitemname4());
//		}

        long id = cl.getId();


        return convertView;
    }





    static class ItemViewHolder {
        TextView txtindex;
        TextView txtName;
        TextView txtName1;
        TextView txtName2;
        TextView txtName3;
        TextView txtName4;
    }

}
