/*
 * The source codes in this project are owned by Bongolive Enterprises. Copying or any kind of usage without Bongolive's approval is against the law.
 * Copyright (c) 2015.
 *
 */

package bongolive.apps.proshop.controller;


import android.app.ProgressDialog;
import android.content.Context;
import android.os.AsyncTask;
import android.widget.ListView;
import android.widget.Toast;

import com.csvreader.CsvReader;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.math.BigDecimal;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

import bongolive.apps.proshop.R;
import bongolive.apps.proshop.model.Product_Categories;
import bongolive.apps.proshop.model.Products;
import bongolive.apps.proshop.model.PurchaseItems;
import bongolive.apps.proshop.model.Warehouse;
import bongolive.apps.proshop.view.AlertDialogManager;

/**
 * Created by nasznjoka on 5/18/15.
 */
public class ImportItems extends AsyncTask<String, Integer, ArrayList<String>> {
    private static final String TAG = ImportItems.class.getName();
    Context context;
    AppPreference appPreference;
    ItemListAdapter adapter;
    String source;
    ProgressDialog pDialog;
    ArrayList<String> errormsg = new ArrayList<>();
    ListView listView;

    public ImportItems(Context context, ItemListAdapter ad, ListView liv, String s) {
        this.context = context;
        adapter = ad;
        source = s;
        appPreference = new AppPreference(context);
        listView = liv;
    }

    @Override
    protected void onProgressUpdate(Integer... values) {
        super.onProgressUpdate(values);
        if (this.pDialog != null) {
            pDialog.setProgress(values[0]);
            pDialog.setMax(values[1]);
        }
    }
    @Override
    protected void onPreExecute() {
        super.onPreExecute();

        pDialog = new ProgressDialog(context);
        pDialog.setMessage(context.getString(R.string.strwait));
        pDialog.setIndeterminate(true);
        pDialog.setCancelable(false);
        pDialog.show();
    }

    @Override
    protected ArrayList<String> doInBackground(String... params) {
        ArrayList<String> ret = new ArrayList<>();
        CsvReader reader = null;
        switch (source){
            case Constants.SEARCHSOURCE_PRODUCTS:
                try {
                    reader = new CsvReader(params[1]);
                    boolean readh = reader.readHeaders();

                    while (reader.readRecord()) {
                        if (readh) {
                            reader.getSkipEmptyRecords();
                            reader.getTrimWhitespace();
                            String[] titles = reader.getHeaders();
                            String pnm = "", punitpr = "", pcat = "", ptype = "", ptxr = "", ptaxmeth = "", psku = "",
                                    pdes = "", preord = "";
                            for (String s : titles) {
                                System.out.println(s);
                                if (s.equals(context.getString(R.string.strproductname)))
                                    pnm = reader.get(context.getString(R.string.strproductname));
                                if (s.equals(context.getString(R.string.strunitprices)))
                                    punitpr = reader.get(context.getString(R.string.strunitprices));
                                if (s.equals(context.getString(R.string.strcategory)))
                                    pcat = reader.get(context.getString(R.string.strcategory));
                                if (s.equals(context.getString(R.string.strproducttype)))
                                    ptype = reader.get(context.getString(R.string.strproducttype));
                                if (s.equals(context.getString(R.string.strtaxrate)))
                                    ptxr = reader.get(context.getString(R.string.strtaxrate));
                                if (s.equals(context.getString(R.string.strtaxmethod)))
                                    ptaxmeth = reader.get(context.getString(R.string.strtaxmethod));
                                if (s.equals(context.getString(R.string.strsku)))
                                    psku = reader.get(context.getString(R.string.strsku));
                                if (s.equals(context.getString(R.string.strprodesc)))
                                    pdes = reader.get(context.getString(R.string.strprodesc));
                                if (s.equals(context.getString(R.string.strreoderval)))
                                    preord = reader.get(context.getString(R.string.strreoderval));
                            }
                            System.out.println("product is "+pnm+" count "+reader.getCurrentRecord());

//                            publishProgress(reader.getCurrentRecord(), 1000);

                            if (!pnm.isEmpty())
                                if (!Constants.checkInvalidChars(pnm)) {
                                    errormsg.add(context.getString(R.string.strinvalidcharacterwarn) + " " + pnm);
                                    continue;
                                }
                            if (!psku.isEmpty())
                                if (!Constants.checkInvalidChars(psku)) {
                                    errormsg.add(context.getString(R.string.strinvalidcharacterwarn) + " " + psku);
                                    continue;
                                }
                            if (!pcat.isEmpty())
                                if (!Constants.checkInvalidChars(pcat)) {
                                    errormsg.add(context.getString(R.string.strinvalidcharacterwarn) + " " + pcat);
                                    continue;
                                }
                            if (!ptype.isEmpty())
                                if (!Constants.checkInvalidChars(ptype)) {
                                    errormsg.add(context.getString(R.string.strinvalidcharacterwarn) + " " + ptype);
                                    continue;
                                }
                            if (!ptaxmeth.isEmpty())
                                if (!Constants.checkInvalidChars(ptaxmeth)) {
                                    errormsg.add(context.getString(R.string.strinvalidcharacterwarn) + " " + ptaxmeth);
                                    continue;
                                }
                            if (!preord.isEmpty())
                                if (!Constants.checkInteger(preord)) {
                                    errormsg.add(context.getString(R.string.numberwarn) + " " + preord);
                                    continue;
                                }
                            if (!preord.isEmpty())
                                if (!Constants.checkIntegerStrictly(preord)) {
                                    errormsg.add(context.getString(R.string.numberwarn) + " " + preord);
                                    continue;
                                }
                            if (!punitpr.isEmpty())
                                if (!Constants.checkInteger(punitpr)) {
                                    errormsg.add(context.getString(R.string.numberwarn) + " " + punitpr);
                                    continue;
                                }
                            if (!punitpr.isEmpty())
                                if (!Constants.checkIntegerStrictly(punitpr)) {
                                    errormsg.add(context.getString(R.string.numberwarn) + " " + punitpr);
                                    continue;
                                }
                            if (!ptxr.isEmpty())
                                if (!Constants.checkInteger(ptxr)) {
                                    errormsg.add(context.getString(R.string.numberwarn) + " " + ptxr);
                                    continue;
                                }
                            if (!ptxr.isEmpty())
                                if (!Constants.checkIntegerStrictly(ptxr)) {
                                    errormsg.add(context.getString(R.string.numberwarn) + " " + ptxr);
                                    continue;
                                }

                            System.out.println("hit here");
                            String[] pcheck = {pnm, punitpr};
                            if (Validating.areSet(pcheck)) {//if necessary fields are not null
                                System.out.println("prod and unit price entered");
                                if (ptxr.isEmpty())
                                    ptxr = "18";
                                if (ptaxmeth.isEmpty())
                                    ptaxmeth = "0";
                                if (pcat.isEmpty())
                                    pcat = "General";
                                if (ptype.isEmpty())
                                    ptype = Constants.PRODUCT_STANDARD;
                                if (preord.isEmpty())
                                    preord = "50";
                                if (!ptaxmeth.equals(Constants.TAXMETHOD_EXC) || !ptaxmeth.equals(Constants
                                        .TAXMETHOD_INCL))
                                    ptaxmeth = "0";
                                if (!ptype.equals(Constants.PRODUCT_SERVICE) || !ptype.equals(Constants
                                        .PRODUCT_STANDARD))
                                    ptype = Constants.PRODUCT_STANDARD;
                                String stcat = "0";
                                if (!Product_Categories.isCatPresent(context, pcat.toUpperCase().trim(), Constants.LOCAL)) {
                                    String[] vals = {"0", pcat.toUpperCase().trim()};
                                    if (Product_Categories.storeWCat(context, vals, Constants.LOCAL) == 1)
                                        stcat = Product_Categories.getId(context);
                                    System.out.println("cat id for " + pcat + " " + stcat);
                                } else {
                                    stcat = Product_Categories.getLocId(context, pcat);
                                }
                                String prodcd = pnm.toUpperCase();
                                prodcd = prodcd.replace(" ", "");

                                int prodskuuniq = 0;

                                psku.replace(" ", "").trim();
                                if (psku.isEmpty()) {
                                    prodskuuniq = 0;
                                    psku = String.valueOf(System.currentTimeMillis());
                                } else {
                                    prodskuuniq = Products.isProductPresent(context, new
                                            String[]{prodcd, " ", psku});
                                }
                                if (prodskuuniq == 1) {
                                    errormsg.add(context.getString(R.string.strskuwarn));
                                }

                                int prodexist = Products.isProductPresent(context, new String[]{prodcd});
                                int prodid = Products.getProdid(context, prodcd);
                                System.out.println(" prodesists " + prodexist + " stcat " + stcat + " prodid " + prodid);
                                if (!stcat.equals("0"))
                                    if (prodexist == 0) {
                                        String[] prod = {pnm, preord, punitpr, ptxr, psku, pdes, stcat, ptype, "code128", ptaxmeth};
                                        if (Products.insertProduct(context, prod, Constants.LOCAL) == 1)
                                            ret.add(pnm + " inserted");
                                    } else {
                                        if (prodid != 0) {
                                            String[] v = {pnm, preord, punitpr, ptxr, psku, pdes, String.valueOf(prodid),
                                                    stcat, ptype, "code128", ptaxmeth};
                                            if (Products.updateProduct(context, v, Constants.LOCAL))
                                                ret.add(pnm + " updated");
                                        }
                                    }
                            } else {
                                if (pnm.isEmpty())
                                    errormsg.add(context.getString(R.string.strprodnamewarn));
                            }


                        } else {
                            System.out.println("could not read headers  " + readh);
                        }
                    }
                reader.close();
                } catch (FileNotFoundException e) {
                    e.printStackTrace();
                } catch (IOException e) {
                    e.printStackTrace();
                }
                break;
            case Constants.SEARCHSOURCE_PURCHASEITEMS:
                try {
                    reader = new CsvReader(params[1]);
                    boolean readh = reader.readHeaders();

                    String batch = Constants.generateBatch();
                    while (reader.readRecord()) {
                        if (readh) {
                            if(Warehouse.getCount(context) == 0){
                                String[] vals = {"1","DEMO_WH","DEMO WAREHOUSE",""};
                                Warehouse.storeWh(context, vals);
                            }
                            reader.getSkipEmptyRecords();
                            reader.getTrimWhitespace();
                            String[] titles = reader.getHeaders();
                            String prn = "",prdid = "", prqty = "", prunitpurc = "",pexpir = "", whid = "";
                            for (String s : titles) {
                                System.out.println(s);
                                if (s.equals(context.getString(R.string.strproductname)))
                                    prn = reader.get(context.getString(R.string.strproductname));
                                if (s.equals(context.getString(R.string.strpurchaseprice)))
                                    prunitpurc = reader.get(context.getString(R.string.strpurchaseprice));
                                if (s.equals(context.getString(R.string.strquantity)))
                                    prqty = reader.get(context.getString(R.string.strquantity));
                                if (s.equals(context.getString(R.string.strexpirydt)))
                                    pexpir = reader.get(context.getString(R.string.strexpirydt));
                            }

                            if(!prn.isEmpty())
                            if(!Constants.checkInvalidChars(prn)) {
                                errormsg.add(context.getString(R.string.strinvalidcharacterwarn)+" "+prn);
                                continue;
                            }
                            if(!pexpir.isEmpty())
                            if(!Constants.checkInvalidChars(pexpir)) {
                                errormsg.add(context.getString(R.string.strinvalidcharacterwarn)+ " " + pexpir);
                                continue;
                            }
                            ArrayList<String> whids = Warehouse.getAllIds(context);
                            if(Warehouse.getCount(context) ==1){
                                whid = whids.get(1);
                                System.out.println("wh is "+whid);
                            }
                            SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd");
                            try {
                                if(!pexpir.isEmpty()) {
                                    Date expd = df.parse(pexpir);
                                    pexpir = String.valueOf(expd);
                                }
                            } catch (ParseException e) {
                                e.printStackTrace();
                            }
                            if(!prn.isEmpty()){
                                String prc = prn.replace(" ","").trim().toUpperCase();
                                int proid = Products.getLocalProdid(context, prc);
                                prdid = String.valueOf(proid);
                            }
                            if(!prqty.isEmpty())
                            if(!Constants.checkInteger(prqty)){
                                errormsg.add(context.getString(R.string.numberwarn)+ " " + prqty);
                                continue;
                            }
                            if(!prqty.isEmpty())
                            if(!Constants.checkIntegerStrictly(prqty)){
                                errormsg.add(context.getString(R.string.numberwarn)+ " " + prqty);
                                continue;
                            }
                            if(!prunitpurc.isEmpty())
                            if(!Constants.checkIntegerStrictly(prunitpurc)){
                                errormsg.add(context.getString(R.string.numberwarn)+ " " + prunitpurc);
                                continue;
                            }
                            if(!prunitpurc.isEmpty())
                            if(!Constants.checkInteger(prunitpurc)){
                                errormsg.add(context.getString(R.string.numberwarn)+ " " + prunitpurc);
                                continue;
                            }

                            String[] check =  new String[]{prdid, batch,whid,prqty};
                            if(Validating.areSet(check) && !whid.equals("0") && !prdid.equals("0")){
                                if(pexpir.isEmpty())
                                    pexpir = "2030-05-05";
                                if(prunitpurc.isEmpty())
                                    prunitpurc = "0";

                                String[] val =  new String[]{prdid, batch,whid,prqty,pexpir,prunitpurc};
                                boolean insert = PurchaseItems.insert(context, val, Constants.LOCAL);

                                if (insert) {
                                    Products.updateProductQty(context, new int[]{new BigDecimal(prdid).intValue(),
                                            new BigDecimal(prqty).intValue(), 0});
                                    ret.add(prn + "quantity updated "+prqty);
                                }
                            } else {

                                if (prn.isEmpty() || prdid.equals("0"))
                                    errormsg.add(context.getString(R.string.strprodnamewarn)+" "+prn);
                                if (prqty.isEmpty())
                                    errormsg.add(context.getString(R.string.strqtywarn)+" "+prqty);
                            }


                            } else {
                            System.out.println("could not read headers  " + readh);
                            errormsg.add("could not read column titles");
                        }
                    }
                    reader.close();
                } catch (FileNotFoundException e) {
                    e.printStackTrace();
                } catch (IOException e) {
                    e.printStackTrace();
                }
                break;
        }
        return ret;
    }

    protected void onPostExecute(ArrayList<String> ret) {
        super.onPostExecute(ret);

        pDialog.setProgress(100);
        if(ret.size()> 0)
        {
            for (int i = 0; i< ret.size(); i++){
                Toast.makeText(context,ret.get(i),Toast.LENGTH_LONG).show();
            }

            adapter.notifyDataSetChanged();
            listView.setAdapter(adapter);
            if(pDialog.isShowing())
                pDialog.dismiss();
        } else {
            String err = context.getString(R.string.strnoupdateorinsertwarn);
            if(errormsg.size() > 0){
                for(int i = 0; i < errormsg.size(); i++) {
                    err += " \n"+ errormsg.get(i);
//                    Toast.makeText(context, errormsg.get(i), Toast.LENGTH_LONG).show();
                }
            }
            if (pDialog.isShowing())
                pDialog.dismiss();
            AlertDialogManager al = new AlertDialogManager();
            al.showAlertDialog(context, context.getString(R.string.strimportwarn), err, false);
//            Toast.makeText(context,context.getString(R.string.strnoupdateorinsertwarn),Toast.LENGTH_LONG).show();

        }

    }
}
