/*
 * Copyright (c) 2015.
 * Prosurvey is a property of Bongolive Enterprises Ltd. An authorized
 * copy of ideas, source codes is against the regulations
 */

package bongolive.apps.proshop.controller;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import bongolive.apps.proshop.R;
import bongolive.apps.proshop.model.Products;
import bongolive.apps.proshop.model.SalesItems;

public class ItemListAdapterOrderItems extends BaseAdapter {
private LayoutInflater mInflater ;
Context _activity ;
AppPreference appPreference;
private List<SalesItems> mItems = new ArrayList<SalesItems>() ;
public ItemListAdapterOrderItems(Context context, List<SalesItems> items)
{
	mInflater = LayoutInflater.from(context);
	mItems = items ;
    _activity = context;
}
	/* (non-Javadoc)
	 * @see android.widget.Adapter#getCount()
	 */
	@Override
	public int getCount() { 
		return mItems.size() ;
	}
	/* (non-Javadoc)
	 * @see android.widget.Adapter#getItem(int)
	 */
	@Override
	public Object getItem(int position) { 
		return mItems.get(position);
	}
	/* (non-Javadoc)
	 * @see android.widget.Adapter#getItemId(int)
	 */
	@Override
	public long getItemId(int arg0) { 
		return 0;
	}
	/* (non-Javadoc)
	 * @see android.widget.Adapter#getView(int, android.view.View, android.view.ViewGroup)
	 */
	@Override
	public View getView(int position, View convertView, ViewGroup parent) { 
		ItemViewHolder holder ;
		if(convertView == null)
		{
			convertView = mInflater.inflate(R.layout.orderitem, null);
			holder = new ItemViewHolder() ;
			holder.txtprd = (TextView)convertView.findViewById(R.id.txtProdNo);
			holder.txtqty = (TextView)convertView.findViewById(R.id.txtProQty);
			holder.txtsaleprice = (TextView)convertView.findViewById(R.id.txtSalePrice);
            holder.txttaxrt = (TextView)convertView.findViewById(R.id.txtTaxRate);
            holder.txtsyncstat = (TextView)convertView.findViewById(R.id.txtSyncStatus);
            holder.txtamount = (TextView)convertView.findViewById(R.id.txtamountpaid);
            holder.txtsubtotal = (TextView)convertView.findViewById(R.id.txtsubtotal);
            holder.txttotaltax = (TextView)convertView.findViewById(R.id.txttaxamount);
            convertView.setTag(holder);
		} else {
			holder = (ItemViewHolder)convertView.getTag();
		}
        SalesItems cl = mItems.get(position);
        appPreference = new AppPreference(_activity);
        String currency = appPreference.getDefaultCurrency();
		currency = currency != null ? currency : "Tsh";
        String prodname = Products.getProdName(_activity, String.valueOf(cl.getProdloc()));
		holder.txtprd.setText(prodname);
		holder.txtqty.setText(_activity.getString(R.string.stritemssold)+" "+cl.getQty());
		holder.txtsaleprice.setText(_activity.getString(R.string.strperitem)+" "+cl.getPrice()+" "+currency);
        holder.txttaxrt.setText(_activity.getString(R.string.strtaxrate)+" "+cl.getTaxrt());
		holder.txttotaltax.setText(_activity.getString(R.string.strtotaltax) +" "+cl.getTaxmt()+" "+currency);
		holder.txtamount.setText(_activity.getString(R.string.strtaxitem) +" "+cl.getTaxitem()+" "+currency);
		holder.txtsubtotal.setText(_activity.getString(R.string.strsubtotal) +" "+cl.getSbtotal()+" "+currency);

        int sst = cl.getAck(); // sync status

        if(sst == 1){
            holder.txtsyncstat.setText(_activity.getString(R.string.stritemsynced));
        } else {
            holder.txtsyncstat.setText(_activity.getString(R.string.stritemunsynced));

        }
        long id = cl.getId();


		return convertView ;
	} 
	static class ItemViewHolder {
		TextView txtprd ;
		TextView txtqty ;
		TextView txtsaleprice ;
        TextView txttaxrt ;
        TextView txtsyncstat ;
		TextView txtsubtotal;
		TextView txttotaltax;
		TextView txtamount;
    }
  
}
