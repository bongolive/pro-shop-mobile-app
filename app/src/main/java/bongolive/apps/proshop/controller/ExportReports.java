/*
 * The source codes in this project are owned by Bongolive Enterprises. Copying or any kind of usage without Bongolive's approval is against the law.
 * Copyright (c) 2015.
 *
 */

package bongolive.apps.proshop.controller;


import android.app.ProgressDialog;
import android.content.Context;
import android.os.AsyncTask;
import android.widget.Toast;

import com.opencsv.CSVWriter;

import java.io.FileWriter;
import java.io.IOException;
import java.math.BigDecimal;
import java.math.RoundingMode;

import bongolive.apps.proshop.R;

/**
 * Created by nasznjoka on 5/18/15.
 */
public class ExportReports extends AsyncTask<String, Integer, String> {
    private static final String TAG = ExportReports.class.getName();
    Context context;
    AppPreference appPreference;
    String source;
    ProgressDialog pDialog;
    private ReportsAdapter adapter ;

    public ExportReports(Context con, ReportsAdapter ad, String s) {
        context = con;
        source = s;
        adapter = ad;
        appPreference = new AppPreference(con);
    }

    @Override
    protected void onPreExecute() {
        super.onPreExecute();

        pDialog = new ProgressDialog(context);
        pDialog.setMessage(context.getString(R.string.strwait));
        pDialog.setCancelable(false);
        pDialog.show();
    }


    @Override
    protected String doInBackground(String... params) {
        CSVWriter writer = null;

        String filename = Constants.getReportName(source);
        String currency = appPreference.getDefaultCurrency() ;
        currency = currency != null ? currency : "Tsh";
        switch (source){
            case Constants.SALESBYPRODUCT:
                try {
                    writer =  new CSVWriter(new FileWriter(filename),'\t');
                    writer.writeNext(params);
                    String head  = context.getString(R.string.strproduct)+"#"+
                            context.getString(R.string.strtotalqty)+"#"+
                            context.getString(R.string.strtotsales);
                    String[] header = head.split("#");
                    writer.writeNext(header);
                    for(int i = 0; i < adapter.getCount(); i++) {
                        ReportsItem cl = (ReportsItem)adapter.getItem(i);
                        String va = cl.getBusinessname()+"#"+cl.getTotalsales()
                                +"#"+cl.getTotalpaymet()+ " " + currency;

                        String[] entries = va.split("#");
                        System.out.println("items are "+va);
                        writer.writeNext(entries);
                    }

                    writer.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
                break;
            case Constants.SALESBYCUSTOMER:
                try {
                    writer =  new CSVWriter(new FileWriter(filename),'\t');
                    writer.writeNext(params);
                    String head  = context.getString(R.string.strbusiness)+"#"+
                            context.getString(R.string.strtotsales)+"#"+
                            context.getString(R.string.strtotoutstanding);
                    String[] header = head.split("#");
                    writer.writeNext(header);
                    for(int i = 0; i < adapter.getCount(); i++) {
                        ReportsItem cl = (ReportsItem)adapter.getItem(i);
                        double db = Double.parseDouble(cl.getTotalsales());
                        double tp = Double.parseDouble(cl.getTotalpaymet());
                        double diff = new BigDecimal(db).subtract(new BigDecimal(tp)).setScale(2, RoundingMode.HALF_UP)
                                .doubleValue();
                        String va = cl.getBusinessname()+"#"+
                                cl.getTotalsales()+ " " + currency+"#"+
                                diff+ " " + currency;

                        String[] entries = va.split("#");
                        System.out.println("items are "+va);
                        writer.writeNext(entries);
                    }

                    writer.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
                break;
            case Constants.TAXBYPRODUCT:
                try {
                    writer =  new CSVWriter(new FileWriter(filename),'\t');
                    writer.writeNext(params);
                    String head  = context.getString(R.string.strdate)+"#"+
                            context.getString(R.string.strtotaltax);
                    String[] header = head.split("#");
                    writer.writeNext(header);
                    for(int i = 0; i < adapter.getCount(); i++) {
                        ReportsItem cl = (ReportsItem)adapter.getItem(i);
                        String va = cl.getBusinessname()+"#"+
                                cl.getTotalsales()+ " " + currency;

                        String[] entries = va.split("#");
                        System.out.println("items are "+va);
                        writer.writeNext(entries);
                    }

                    writer.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
                break;
            case Constants.SALEREPORT:
                try{
                    writer = new CSVWriter(new FileWriter(filename),'\t');
                    writer.writeNext(params);
                    String head =context.getString(R.string.strtotsales)+" ("+currency+") #"+context.getString(R.string
                            .stramountpaid)+" "+currency
                            +"#"+context.getString(R.string.strbusiness)+"#"+context.getString(R.string.strorderdate)
                            +"#"+context.getString(R.string.strtotaltax)+" ("+currency+")";
                    String[] header = head.split("#");
                    writer.writeNext(header);
                    for(int i =0; i< adapter.getCount(); i++){
                        ReportsItem cl = (ReportsItem)adapter.getItem(i);
                        String va = new BigDecimal(cl.getTotalsales()).setScale(2,
                                RoundingMode.HALF_UP).doubleValue()+"#"+new BigDecimal(cl.getTotalpaymet()).setScale(2,
                                RoundingMode.HALF_UP).doubleValue()+"#"+
                                cl.getBusinessname()+"#"+cl.getOrderdate()+"#"+new BigDecimal(cl.getTax()).setScale(2,
                                RoundingMode.HALF_UP).doubleValue();
                        String[] entries  = va.split("#");
                        System.out.println("items are "+va);
                        writer.writeNext(entries);
                    }
                    writer.close();
                } catch (IOException e){
                    e.printStackTrace();
                }
        }
        return filename;
    }

    protected void onPostExecute(String ret) {
        super.onPostExecute(ret);
        Toast.makeText(context, context.getString(R.string.strexportsuccess)+" Proshop_Reports",Toast.LENGTH_LONG)
                .show();
            if(pDialog.isShowing())
                pDialog.dismiss();
    }
}
