/*
 * The source codes in this project are owned by Bongolive Enterprises. Copying or any kind of usage without Bongolive's approval is against the law.
 * Copyright (c) 2015. 
 *
 */

package bongolive.apps.proshop.controller;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import bongolive.apps.proshop.R;
import bongolive.apps.proshop.model.Products;
import bongolive.apps.proshop.model.SalesItems;

public class SalesItemAdapter extends BaseAdapter {
private LayoutInflater mInflater ;
AppPreference appPreference ;
Context _context ;
private List<SalesItems> mItems = new ArrayList<>() ;
public SalesItemAdapter(Context context, List<SalesItems> items)
{
	mInflater = LayoutInflater.from(context);
	mItems = items ;
    this._context = context;
}  
	/* (non-Javadoc)
	 * @see android.widget.Adapter#getCount()
	 */
	@Override
	public int getCount() { 
		return mItems.size() ;
	}
	/* (non-Javadoc)
	 * @see android.widget.Adapter#getItem(int)
	 */
	@Override
	public Object getItem(int position) { 
		return mItems.get(position);
	}
	/* (non-Javadoc)
	 * @see android.widget.Adapter#getItemId(int)
	 */
	@Override
	public long getItemId(int arg0) { 
		return 0;
	}
	/* (non-Javadoc)
	 * @see android.widget.Adapter#getView(int, android.view.View, android.view.ViewGroup)
	 */
	@Override
	public View getView(int position, View convertView, ViewGroup parent) { 
		ItemViewHolder holder ;
	
		if(convertView == null)
		{
			convertView = mInflater.inflate(R.layout.salesitemrow, null);
			holder = new ItemViewHolder() ;
			holder.txtprodname = (TextView)convertView.findViewById(R.id.txtprodname);
			holder.txtquantity = (TextView)convertView.findViewById(R.id.txtProQty);
			holder.txtprice = (TextView)convertView.findViewById(R.id.txtprice);
			holder.txtindex = (TextView)convertView.findViewById(R.id.txtlabelindex); 
			holder.txtprodid = (TextView)convertView.findViewById(R.id.autoproid);
			holder.txtsubtotal = (TextView)convertView.findViewById(R.id.txtsubtotal);
			holder.txttax = (TextView)convertView.findViewById(R.id.txttax);
			holder.txtpr = (TextView)convertView.findViewById(R.id.txtdiscount);
			convertView.setTag(holder);
		} else {
			holder = (ItemViewHolder)convertView.getTag();
		}
		appPreference = new AppPreference(_context.getApplicationContext());
		SalesItems cl = mItems.get(position);
		String prodnm = Products.getProdName(_context, String.valueOf(cl.getProdloc()));
		String prodlable = _context.getString(R.string.strproduct);
		String qtylable = _context.getString(R.string.strquantity);
		String subtt = _context.getString(R.string.strsubtotal);
		String taxlb = _context.getString(R.string.strtotaltax);
		String prc = _context.getString(R.string.strunitprices);

		if(!appPreference.showPricing()){
			holder.txtprodname.setVisibility(View.GONE);
			holder.txtsubtotal.setVisibility(View.GONE);
			holder.txttax.setVisibility(View.GONE);
			holder.txtquantity.setText(String.valueOf(cl.getId()) + "  " + prodlable + " : " + prodnm);
			holder.txtprice.setText(qtylable + " : " + cl.getQty());
		} else {
			holder.txtprodname.setText(String.valueOf(cl.getId()) + "  " + prodlable + " : " + prodnm);
			holder.txtquantity.setText(qtylable + " : " + cl.getQty());
			if (cl.getDiscount() != 0)
				holder.txtprice.setText(prc + " : " + cl.getDiscount());
			if (cl.getDiscount() == 0)
				holder.txtprice.setText(prc + " : " + cl.getPrice());

			holder.txtindex.setVisibility(View.GONE);
			holder.txtindex.setText(String.valueOf(cl.getId()));
			holder.txtprodid.setText(String.valueOf(cl.getProdloc()));
			holder.txtsubtotal.setText(subtt + " : " + cl.getSbtotal());
			holder.txttax.setText(taxlb + " : " + cl.getTaxmt());
			holder.txtpr.setText(String.valueOf(cl.getPrice()));

		}

        /*String lang = appPreference.getDefaultCurrency();
        if(lang.equals("Tshs") || lang.isEmpty() || lang.equals("-1")){
            holder.txtprice.setCompoundDrawablesWithIntrinsicBounds(R.drawable.ic_attach_money_black_24dptz,0,0,0);
        } else{
            holder.txtprice.setCompoundDrawablesWithIntrinsicBounds(R.drawable.ic_attach_money_black_24dp,0,0,0);
        }*/
		return convertView ;
	} 
	static class ItemViewHolder {
		TextView txtprodname ;
		TextView txtquantity ; 
		TextView txtprice ; 
		TextView txtindex;
		TextView txtprodid;
		TextView txtsubtotal;
		TextView txttax;
		TextView txtpr ;
	}

}