/*
 * Copyright (c) 2015. All rights reserved. The source codes are property of bongolive
 */

package bongolive.apps.proshop.controller;

/**
 * @author nasznjoka
 *         <p/>
 *         Oct 9, 2014
 */
public class ChildItem {

    private String Name;
    private int Image;

    public String getName() {
        return Name;
    }

    public void setName(String Name) {
        this.Name = Name;
    }

    public int getImage() {
        return Image;
    }

    public void setImage(int Image) {
        this.Image = Image;
    }


}