/*
 * Copyright (c) 2015. All rights reserved. The source codes are property of bongolive
 */

package bongolive.apps.proshop.controller;

/**
 * @author nasznjoka
 *         <p/>
 *         Oct 9, 2014
 */
public class ReportsItem {
    String totalsales,totalsalesdy,totalsaleswk,totalsalesmo, totalpaymet, orderdate, tax; //order
    long orderid, stockid, custid, orderitemid;//general
    String batch, stockdate, stokqty, purchaseprice, expiredate;//stock
    String unitprice, reorder, unittax, lastupdate, productname;      //product
    String customername,businessname,mobile,area;

    String stname,opname,mein,meout,cain,caout,tin,tout,photo,user,comment,updated,created,pump;
    long stid;
    int synced;

    public ReportsItem(){

    }

    public String getTotalsales() {
        return totalsales;
    }

    public String getTotalsalesdy() {
        return totalsalesdy;
    }

    public String getTotalsaleswk() {
        return totalsaleswk;
    }

    public String getTotalsalesmo() {
        return totalsalesmo;
    }

    public String getTotalpaymet() {
        return totalpaymet;
    }

    public String getOrderdate() {
        return orderdate;
    }

    public String getTax() {
        return tax;
    }

    public long getOrderid() {
        return orderid;
    }

    public long getStockid() {
        return stockid;
    }

    public long getCustid() {
        return custid;
    }

    public long getOrderitemid() {
        return orderitemid;
    }

    public String getBatch() {
        return batch;
    }

    public String getStockdate() {
        return stockdate;
    }

    public String getStokqty() {
        return stokqty;
    }

    public String getPurchaseprice() {
        return purchaseprice;
    }

    public String getExpiredate() {
        return expiredate;
    }

    public String getUnitprice() {
        return unitprice;
    }

    public String getReorder() {
        return reorder;
    }

    public String getUnittax() {
        return unittax;
    }

    public String getLastupdate() {
        return lastupdate;
    }

    public String getProductname() {
        return productname;
    }

    public String getCustomername() {
        return customername;
    }

    public String getBusinessname() {
        return businessname;
    }

    public String getMobile() {
        return mobile;
    }

    public String getArea() {
        return area;
    }

    public String getStname() {
        return stname;
    }

    public String getOpname() {
        return opname;
    }

    public String getMein() {
        return mein;
    }

    public String getMeout() {
        return meout;
    }

    public String getCain() {
        return cain;
    }

    public String getCaout() {
        return caout;
    }

    public String getTin() {
        return tin;
    }

    public String getTout() {
        return tout;
    }

    public String getPhoto() {
        return photo;
    }

    public String getUser() {
        return user;
    }

    public String getComment() {
        return comment;
    }

    public int getSynced() {
        return synced;
    }

    public String getCreated() {
        return created;
    }

    public String getUpdated() {
        return updated;
    }

    public String getPump() {
        return pump;
    }

    public long getStid() {
        return stid;
    }

    //sales by customer
    public ReportsItem(String _totalsales, String _totalpaymet, String _businessname){
        totalsales = _totalsales;
        totalpaymet = _totalpaymet;
        businessname = _businessname;
    }
    //sales by customer
    public ReportsItem(String _totalsales, String _totalpaymet, String _businessname, String _orderdt, String
            totaltx, long id){
        totalsales = _totalsales;
        totalpaymet = _totalpaymet;
        businessname = _businessname;
        orderdate = _orderdt;
        tax = totaltx;
        orderid = id;
    }
    //sales by customer station form

    public ReportsItem(int synced, long stid, String created, String updated, String comment, String user, String photo, String tout, String tin, String caout, String cain, String meout, String mein, String opname, String stname, String pump) {
        this.synced = synced;
        this.stid = stid;
        this.created = created;
        this.updated = updated;
        this.comment = comment;
        this.user = user;
        this.photo = photo;
        this.tout = tout;
        this.tin = tin;
        this.caout = caout;
        this.cain = cain;
        this.meout = meout;
        this.mein = mein;
        this.opname = opname;
        this.stname = stname;
        this.pump = pump;
    }
}
