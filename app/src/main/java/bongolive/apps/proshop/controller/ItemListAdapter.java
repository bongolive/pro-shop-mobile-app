/*
 * Copyright (c) 2015. All rights reserved. The source codes are property of bongolive
 */

package bongolive.apps.proshop.controller;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import bongolive.apps.proshop.R;

public class ItemListAdapter extends BaseAdapter  {
    int action;
    private LayoutInflater mInflater;
    boolean tabletsize ;
    Context _context;
    private long customer;
    private List<ItemList> mItems = new ArrayList<ItemList>();

    public ItemListAdapter() {

    }

    public ItemListAdapter(Context context, List<ItemList> items, int source) {
        mInflater = LayoutInflater.from(context);
        mItems = items;
        _context = context;
        action = source;
        tabletsize = context.getResources().getBoolean(R.bool.isTablet);

    }
    public ItemListAdapter(Context context, List<ItemList> items, int source, long customer) {
        mInflater = LayoutInflater.from(context);
        mItems = items;
        _context = context;
        action = source;
        tabletsize = context.getResources().getBoolean(R.bool.isTablet);
        this.customer = customer;
    }

    /* (non-Javadoc)
     * @see android.widget.Adapter#getCount()
     */
    @Override
    public int getCount() {
        return mItems.size();
    }

    /* (non-Javadoc)
     * @see android.widget.Adapter#getItem(int)
     */
    @Override
    public Object getItem(int position) {
        return mItems.get(position);
    }

    /* (non-Javadoc)
     * @see android.widget.Adapter#getItemId(int)
     */
    @Override
    public long getItemId(int arg0) {
        return 0;
    }

    /* (non-Javadoc)
     * @see android.widget.Adapter#getView(int, android.view.View, android.view.ViewGroup)
     */
    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ItemViewHolder holder;
        if (convertView == null) {
            convertView = mInflater.inflate(R.layout.list_item, null);
            holder = new ItemViewHolder();
            holder.txtName = (TextView) convertView.findViewById(R.id.txtitem);
            holder.txtindex = (TextView) convertView.findViewById(R.id.txtindex);
            holder.txtName1 = (TextView) convertView.findViewById(R.id.txtitem1);
            holder.txtName2 = (TextView) convertView.findViewById(R.id.txtitem2);
            holder.txtName3 = (TextView) convertView.findViewById(R.id.txtitemextra);
            holder.txtName4 = (TextView) convertView.findViewById(R.id.txtitem4);
            convertView.setTag(holder);
        } else {
            holder = (ItemViewHolder) convertView.getTag();
        }
        ItemList cl = mItems.get(position);

        if(action == 3){
            holder.txtName.setTextSize(13);
            holder.txtName1.setTextSize(13);
            holder.txtName2.setTextSize(13);
            holder.txtName3.setTextSize(13);
            holder.txtName4.setTextSize(13);
            holder.txtName.setVisibility(View.GONE);
            holder.txtName2.setVisibility(View.GONE);
            if (tabletsize) {
                holder.txtName.setText(cl.getSaleref());
                holder.txtName1.setText(cl.getBuss());
                holder.txtName2.setText(cl.getDelref());
                holder.txtindex.setText(String.valueOf(position + 1));
                String item3 = cl.getDeldate();
                holder.txtName3.setVisibility(View.VISIBLE);
                holder.txtName3.setText(item3);
                holder.txtName4.setText(cl.getDeladd());


                long id = cl.getSaleid();

            } else {
                holder.txtName.setText(cl.getSaleref());
                holder.txtName1.setText(cl.getBuss());
                holder.txtName2.setText(cl.getDelref());
                holder.txtindex.setText(String.valueOf(position + 1));
                String item3 = cl.getDeldate();
                    holder.txtName3.setVisibility(View.VISIBLE);
                    holder.txtName3.setText(item3);

                holder.txtName4.setVisibility(View.VISIBLE);
                holder.txtName4.setText(cl.getDeladd());

                long id = cl.getSaleid();
            }
        } else if (action == 4) {
            if (tabletsize) {
                holder.txtName.setTextSize(13);
                holder.txtName1.setTextSize(13);
                holder.txtName2.setTextSize(13);
                holder.txtName3.setTextSize(13);
                holder.txtName4.setTextSize(13);
                holder.txtName.setText(cl.getTitle());
                holder.txtName1.setText(cl.getDesc());
                holder.txtName2.setText(cl.getDate());
                holder.txtindex.setText(String.valueOf(position + 1));
                holder.txtName3.setVisibility(View.GONE);
                holder.txtName4.setVisibility(View.GONE);

                long id = cl.getId();

            } else {
                holder.txtName.setText(_context.getString(R.string.straudittitle) + " " +cl.getTitle());
                holder.txtName1.setText(_context.getString(R.string.strcreated) + " " +cl.getDate());
                holder.txtName2.setText(_context.getString(R.string.strprodesc) + " " +cl.getDesc());
                long bz = cl.getBus();
                if(bz == customer)
                    holder.txtName.setCompoundDrawablesWithIntrinsicBounds(0,0,R.drawable.ic_action_tick,0);

                holder.txtindex.setText(String.valueOf(position + 1));
                holder.txtName3.setVisibility(View.GONE);
                holder.txtName4.setVisibility(View.GONE);
                long id = cl.getId();
            }
        } else if (action == 5) {
            if (tabletsize) {
                holder.txtName.setTextSize(13);
                holder.txtName.setText(cl.getTitle());
                holder.txtName1.setVisibility(View.GONE);
                holder.txtName2.setVisibility(View.GONE);
                holder.txtindex.setVisibility(View.GONE);
                holder.txtName3.setVisibility(View.GONE);
                holder.txtName4.setVisibility(View.GONE);

                long id = cl.getId();

            } else {
                holder.txtName.setText(cl.getTitle());
                holder.txtindex.setText(String.valueOf(position + 1));
                float ht = _context.getResources().getDimension(R.dimen.list_item_height);
                int height = new BigDecimal(ht).intValue();
                holder.txtName.setMinHeight(height);
                holder.txtName.setTextSize(16);
                holder.txtName1.setVisibility(View.GONE);
                holder.txtName2.setVisibility(View.GONE);
                holder.txtindex.setVisibility(View.GONE);
                holder.txtName3.setVisibility(View.GONE);
                holder.txtName4.setVisibility(View.GONE);
                long id = cl.getId();
            }
        }else {
            if (tabletsize) {
                holder.txtName.setTextSize(13);
                holder.txtName1.setTextSize(13);
                holder.txtName2.setTextSize(13);
                holder.txtName3.setTextSize(13);
                holder.txtName4.setTextSize(13);
                holder.txtName.setText(cl.getitemname());
                holder.txtName1.setText(cl.getitemname1());
                holder.txtName2.setText(cl.getitemname2());
                holder.txtindex.setText(String.valueOf(position + 1));
                String item3 = cl.getitemname3();
                if (Validating.areSet(item3)) {
                    holder.txtName3.setVisibility(View.VISIBLE);
                    holder.txtName3.setText(item3);
                }

                if (action == 1)
                    holder.txtName2.setVisibility(View.GONE);
//		if(!cl.getitemname4().isEmpty()){
                if (action == 0 || action == 1)
                    holder.txtName4.setVisibility(View.GONE);

                if (action == 100) {
                    holder.txtName2.setVisibility(View.GONE);
                    holder.txtName4.setVisibility(View.GONE);
                }

                holder.txtName4.setText(cl.getitemname4());
//		}

                long id = cl.getId();

            } else {
                holder.txtName.setText(cl.getitemname());
                holder.txtName1.setText(cl.getitemname1());
                holder.txtName2.setText(cl.getitemname2());
                holder.txtindex.setText(String.valueOf(position + 1));
                String item3 = cl.getitemname3();
                if (Validating.areSet(item3)) {
                    holder.txtName3.setVisibility(View.VISIBLE);
                    holder.txtName3.setText(item3);
                }

                holder.txtName4.setVisibility(View.VISIBLE);
                holder.txtName4.setText(cl.getitemname4());


                if (action == 100) {
                    holder.txtName3.setVisibility(View.GONE);
                    holder.txtName4.setVisibility(View.GONE);
                }

                long id = cl.getId();
            }
        }
        return convertView;
    }

    static class ItemViewHolder {
        TextView txtName;
        TextView txtindex;
        TextView txtName1;
        TextView txtName2;
        TextView txtName3;
        TextView txtName4;
    }

}
