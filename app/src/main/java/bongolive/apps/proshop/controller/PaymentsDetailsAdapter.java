/*
 * The source codes in this project are owned by Bongolive Enterprises. Copying or any kind of usage without Bongolive's approval is against the law.
 * Copyright (c) 2015.
 *
 */

package bongolive.apps.proshop.controller;


import android.app.Activity;
import android.graphics.Bitmap;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import bongolive.apps.proshop.R;
import bongolive.apps.proshop.model.Payments;
import bongolive.apps.proshop.model.Sales;

/**
 * Created by nasznjoka on 5/18/15.
 */

public class PaymentsDetailsAdapter extends RecyclerView.Adapter<PaymentsDetailsAdapter.Vh> {

    public List<Payments> mItems = new ArrayList<Payments>();
    Activity context;
    AppPreference appPreference;
    String currency = null;

    public static class Vh extends RecyclerView.ViewHolder {
        private LinearLayout layview;
        private TextView txtpaid,txtpostpaid,txtdebt,txtpaydt,txtnote,txtsync,
        txtsaleref,txtpayref,txtpaytype;
        ImageView txtattach;

        public Vh(View v) {
            super(v);
            layview = (LinearLayout)v.findViewById(R.id.view_lay);
            txtpaid = (TextView)v.findViewById(R.id.txtpaid);
            txtpostpaid = (TextView)v.findViewById(R.id.txtpostpaid);
            txtdebt = (TextView)v.findViewById(R.id.txtdebt);
            txtpaydt = (TextView)v.findViewById(R.id.txtpaydate);
            txtnote = (TextView)v.findViewById(R.id.txtnotes);
            txtattach = (ImageView)v.findViewById(R.id.txtattach);
            txtsync = (TextView)v.findViewById(R.id.txtsynced);
            txtsaleref = (TextView)v.findViewById(R.id.txtsaleref);
            txtpayref = (TextView)v.findViewById(R.id.txtpayref);
            txtpaytype = (TextView)v.findViewById(R.id.txtpaytype);
        }
    }

    public PaymentsDetailsAdapter(Activity mcontext, ArrayList<Payments> myDataset){
        mItems = myDataset;
        context = mcontext;
        appPreference = new AppPreference(context);
        currency = appPreference.getDefaultCurrency() ;
        currency = currency != null ? currency : "Tsh";
    }

    // Create new views (invoked by the layout manager)
    @Override
    public Vh onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.payment_detail, parent, false);

        Vh vh = new Vh(v);
        return vh;
    }

    @Override
    public void onBindViewHolder(final Vh holder, final int position) {
        final Payments cl = mItems.get(position);
        currency = appPreference.getDefaultCurrency() ;
        currency = currency != null ? currency : "Tsh";
        String locsaleid = cl.getLocalsale();
        double deb = Sales.getDebt(context, locsaleid);

        String amount = cl.getAmount();
        String paydate = cl.getPaydate();
        paydate = Constants.getFormatedDate(paydate);
        String balance = String.valueOf(deb);
//        String balance = cl.getPostbalance();
        String saleref = Sales.getSaleRef(context, locsaleid);
        String postp = cl.getPostpaid();
        String ack = cl.getAck();
        String attach = cl.getAttachment();
        String payref = cl.getRefer();
        String type = cl.getType();
        String note = cl.getComment();

        if(ack.equals("1"))
            ack = context.getString(R.string.stryes);
        else
            ack = context.getString(R.string.strno);

        holder.txtpaid.setText(amount);
        holder.txtpostpaid.setText(postp);
        holder.txtdebt.setText(balance);
        holder.txtpaydt.setText(paydate);
        holder.txtnote.setText(note);
        if(attach != null) {
            Bitmap bitmap = Constants.decodeImg(attach, 300, 200);
            if (bitmap != null)
                holder.txtattach.setImageBitmap(bitmap);
        }
        holder.txtsync.setText(ack);
        holder.txtsaleref.setText(saleref);
        holder.txtpayref.setText(payref);
        holder.txtpaytype.setText(type);
    }

    // Return the size of your dataset (invoked by the layout manager)
    @Override
    public int getItemCount() {
        return mItems.size();
    }

    @Override
    public long getItemId(int arg0){
        return 0;
    }

}