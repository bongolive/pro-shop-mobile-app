/*
 * Copyright (c) 2015.
 * Prosurvey is a property of Bongolive Enterprises Ltd. An authorized
 * copy of ideas, source codes is against the regulations
 */

package bongolive.apps.proshop.controller;

import android.Manifest;
import android.app.Service;
import android.content.Intent;
import android.content.IntentSender;
import android.content.pm.PackageManager;
import android.location.Location;
import android.os.Bundle;
import android.os.IBinder;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.PendingResult;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.LocationSettingsRequest;
import com.google.android.gms.location.LocationSettingsResult;
import com.google.android.gms.location.LocationSettingsStates;
import com.google.android.gms.location.LocationSettingsStatusCodes;

import bongolive.apps.proshop.model.Settings;
import bongolive.apps.proshop.model.Tracking;

/**
 * Created by nasznjoka on 3/19/15.
 */


public class LocationUpdate extends Service implements
        GoogleApiClient.ConnectionCallbacks, GoogleApiClient.OnConnectionFailedListener
        , LocationListener {

    private final AppCompatActivity mContext;
    private static final String TAG = LocationUpdate.class.getName();
    AppPreference appPreference;
    private boolean currentlyProcessingLocation = false;
    /* location */
    private static final int LOCATION_PERMISSION_REQUEST = 1;
    private boolean mPermissionDenied = false;
    private GoogleApiClient mGoogleApiClient;

    private LocationRequest mLocationRequest;
    private Location currentlocation;
    private boolean LOCATION_ALLOWED;
    private static final int REQUEST_CHECK_SETTINGS = 11;
    private String[] loc = null;


    public LocationUpdate(AppCompatActivity context) {
        this.mContext = context;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        appPreference = new AppPreference(mContext);
    }


    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {

        if (!currentlyProcessingLocation) {
            currentlyProcessingLocation = true;
            buildGoogleApiClient();
        }

        return START_NOT_STICKY;
    }

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    /**
     * Builds a GoogleApiClient. Uses {@code #addApi} to request the LocationServices API.
     */
    protected synchronized void buildGoogleApiClient() {
        Log.d(TAG, "startTracking");

        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) !=
                PackageManager.PERMISSION_GRANTED
                && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) !=
                PackageManager.PERMISSION_GRANTED) {
            LOCATION_ALLOWED = false;
            PermissionUtils.requestPermission(mContext, LOCATION_PERMISSION_REQUEST,
                    Manifest.permission.ACCESS_FINE_LOCATION, true);
        }
        if (mGoogleApiClient == null)
            mGoogleApiClient = new GoogleApiClient.Builder(this)
                    .addConnectionCallbacks(this)
                    .addOnConnectionFailedListener(this)
                    .addApi(LocationServices.API)
                    .build();

        if (!mGoogleApiClient.isConnected() || !mGoogleApiClient.isConnecting()) {
            mGoogleApiClient.connect();
        }
        mLocationRequest = LocationRequest.create();
        mLocationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
        int tracktime = Settings.getTrackTime(this);
        if (tracktime > 0) {
            mLocationRequest.setFastestInterval(tracktime);

            LocationServices.FusedLocationApi.requestLocationUpdates(
                    mGoogleApiClient, mLocationRequest, this);
        } else
            stopLocationUpdates();
        if(mGoogleApiClient != null)
            setup_location();
    }

    private void setup_location(){
        LocationSettingsRequest.Builder builder = new LocationSettingsRequest.Builder()
                .addLocationRequest(mLocationRequest);
        builder.setAlwaysShow(true); //this is the key ingredient

        PendingResult<LocationSettingsResult> result =
                LocationServices.SettingsApi.checkLocationSettings(mGoogleApiClient, builder.build());
        result.setResultCallback(new ResultCallback<LocationSettingsResult>() {
            @Override
            public void onResult(LocationSettingsResult result) {
                final Status status = result.getStatus();
                final LocationSettingsStates state = result.getLocationSettingsStates();

                switch (status.getStatusCode()) {
                    case LocationSettingsStatusCodes.SUCCESS:
                        //nothing proceed
                        LOCATION_ALLOWED = true;
                        break;
                    case LocationSettingsStatusCodes.RESOLUTION_REQUIRED:
                        try {
                            status.startResolutionForResult(mContext, REQUEST_CHECK_SETTINGS);
                        } catch (IntentSender.SendIntentException e) {
                            e.printStackTrace();
                        }
                        break;
                    case LocationSettingsStatusCodes.SETTINGS_CHANGE_UNAVAILABLE:
                        LOCATION_ALLOWED = false;
                        break;
                }
            }
        });

    }

    @Override
    public void onConnected(Bundle bundle) {
        if (ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION)
                == PackageManager.PERMISSION_GRANTED) {

            LocationServices.FusedLocationApi.requestLocationUpdates(
                    mGoogleApiClient, mLocationRequest, this);
            if(currentlocation == null)
                currentlocation = LocationServices.FusedLocationApi.getLastLocation(
                    mGoogleApiClient);
        }
    }

    @Override
    public void onConnectionSuspended(int i) {

        Log.i(TAG, "GoogleApiClient connection has been suspend");
    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {

        Log.i(TAG, "GoogleApiClient connection has failed");

        stopLocationUpdates();
        stopSelf();
    }

    @Override
    public void onLocationChanged(Location location) {
        System.out.println("connection received " + location.toString());
        currentlocation = location;
        if (location.getAccuracy() < 500.0f) {
            try {
                double lat = currentlocation.getLatitude();
                double lng = currentlocation.getLongitude();

                double[] vals = {lat,lng};
                float[] val = {Float.valueOf(String.valueOf(lat)),Float.valueOf(String.valueOf(lng))};
                float[] lastloc = appPreference.getLastKnowLocation();

                if(lastloc[0] != val[0] || lastloc[1] != val[1]) {
                    if (!vals.equals(0)) {
                        appPreference.saveLastLocation(val);
                        Tracking.storeLocation(getApplicationContext(), vals);
                    }
                } else {
                    System.out.println("the last location " + lastloc[0] + "," + lastloc[1] + " has not changed so ignore new loc " +
                            +val[0] + "," + val[1]);
                }
                stopLocationUpdates();
            } catch (NullPointerException e){
                e.printStackTrace();
            }
        }
    }

    private void stopLocationUpdates() {
        if (mGoogleApiClient != null && mGoogleApiClient.isConnected()) {
            mGoogleApiClient.disconnect();
        }
    }

}
