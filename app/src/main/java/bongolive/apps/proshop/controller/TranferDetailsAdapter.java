/*
 * The source codes in this project are owned by Bongolive Enterprises. Copying or any kind of usage without Bongolive's approval is against the law.
 * Copyright (c) 2015.
 *
 */

package bongolive.apps.proshop.controller;


import android.app.Activity;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.ArrayList;
import java.util.List;

import bongolive.apps.proshop.R;
import bongolive.apps.proshop.model.Products;
import bongolive.apps.proshop.model.Transfer;
import bongolive.apps.proshop.model.TransferItems;

/**
 * Created by nasznjoka on 5/18/15.
 */

public class TranferDetailsAdapter extends RecyclerView.Adapter<TranferDetailsAdapter.Vh> {

    public List<TransferItems> mItems = new ArrayList<TransferItems>();
    Activity context;
    AppPreference appPreference;
    String currency = null;

    public static class Vh extends RecyclerView.ViewHolder {
        private LinearLayout layview;
        private TextView txtpaid,txtpostpaid,txtdebt,txtpaydt,txtnote,txtsync,
        txtsaleref,txtpayref,txtpaytype;
        ImageView txtattach;

        public Vh(View v) {
            super(v);
            layview = (LinearLayout)v.findViewById(R.id.view_lay);
            txtpaid = (TextView)v.findViewById(R.id.txtpaid);
            txtpostpaid = (TextView)v.findViewById(R.id.txtpostpaid);
            txtdebt = (TextView)v.findViewById(R.id.txtdebt);
            txtpaydt = (TextView)v.findViewById(R.id.txtpaydate);
            txtnote = (TextView)v.findViewById(R.id.txtnotes);
            txtattach = (ImageView)v.findViewById(R.id.txtattach);
            txtsync = (TextView)v.findViewById(R.id.txtsynced);
            txtsaleref = (TextView)v.findViewById(R.id.txtsaleref);
            txtpayref = (TextView)v.findViewById(R.id.txtpayref);
            txtpaytype = (TextView)v.findViewById(R.id.txtpaytype);
            txtattach.setVisibility(View.GONE);
        }
    }

    public TranferDetailsAdapter(Activity mcontext, ArrayList<TransferItems> myDataset){
        mItems = myDataset;
        context = mcontext;
        appPreference = new AppPreference(context);
        currency = appPreference.getDefaultCurrency();
    }

    // Create new views (invoked by the layout manager)
    @Override
    public Vh onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.transfer_detail, parent, false);

        Vh vh = new Vh(v);
        return vh;
    }

    @Override
    public void onBindViewHolder(final Vh holder, final int position) {
        final TransferItems cl = mItems.get(position);
        currency = appPreference.getDefaultCurrency() ;
        currency = currency != null ? currency : "Tsh";
        String locsaleid = cl.getTransidl();
        String prod = cl.getProduct();
        prod = Products.getProdNameBasedSyid(context, prod);

        String amount = new BigDecimal(cl.getQtyverified()).multiply(new BigDecimal(cl.getUnit()))
                .setScale(2, RoundingMode.HALF_UP).toString();
        String paydate = cl.getDate();
        paydate = Constants.getFormatedDate(paydate);
        String qtydlvd = cl.getQtyverified();
//        String balance = cl.getPostbalance();
        String saleref = Transfer.getRefNo(context, locsaleid);
        String qtyrqstd = cl.getQty();
        String unitc = cl.getUnit();
        String ack = cl.getStatus();
        String type = cl.getTimestamp();
        String status = null;

        if(ack.equals("0"))
            status = context.getString(R.string.strnotapproved);
        else if(ack.equals("1"))
            status = context.getString(R.string.strapproved);
        else if(ack.equals("2"))
            status = context.getString(R.string.strdisapproved);

        holder.txtpaydt.setText(prod);
        holder.txtpaid.setText(paydate);
        holder.txtpostpaid.setText(qtyrqstd);
        holder.txtdebt.setText(qtydlvd);
        holder.txtpayref.setText(unitc+" "+currency);
        holder.txtsaleref.setText(amount+" "+currency);
        holder.txtsync.setText(status);
        holder.txtpaytype.setText(type);
    }

    // Return the size of your dataset (invoked by the layout manager)
    @Override
    public int getItemCount() {
        return mItems.size();
    }

    @Override
    public long getItemId(int arg0){
        return 0;
    }

}