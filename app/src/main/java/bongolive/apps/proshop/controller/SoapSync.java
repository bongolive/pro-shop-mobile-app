/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package bongolive.apps.proshop.controller;

import android.util.Log;

import org.json.JSONObject;
import org.ksoap2.SoapEnvelope;
import org.ksoap2.serialization.SoapObject;
import org.ksoap2.serialization.SoapSerializationEnvelope;
import org.xmlpull.v1.XmlPullParserException;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;

/**
 *
 * @author nasznjoka
 */
public class SoapSync {
    private static final String CONTENTTYPE = "Content-Type";
    private static final String JSON = "application/json; charset=utf-8";
    private static final String SOAP = "application/soap+xml; charset=utf-8";
    static String response = null;
    static JSONObject jObj = null;
    static InputStream is ;
    static String json = "";
    private final String POSTMETHOD ="POST";
    private static final String METHOD_NAME = "TopGoalScorers";
    private static final String SOAP_ACTION = "http://footballpool.dataaccess.eu/data/TopGoalScorers&quot";
    private static final String NAMESPACE = "http://footballpool.dataaccess.eu";
    private static final String _url_ = "http://footballpool.dataaccess.eu/data/info.wso?WSDL&quot";
//you can get these values from the wsdl file^

    public SoapSync() {

    }

    public String getJson(String url,JSONObject params){
        try {
            URL _url = new URL(url);
            HttpURLConnection urlConn =(HttpURLConnection)_url.openConnection();
            urlConn.setRequestMethod(POSTMETHOD);
            urlConn.setRequestProperty(CONTENTTYPE, JSON);
            urlConn.setRequestProperty("Accept", "application/json");
            urlConn.setDoOutput(true);
            urlConn.connect();

            BufferedWriter writer = new BufferedWriter(new OutputStreamWriter(urlConn.getOutputStream()));
            writer.write(params.toString());
            writer.flush();
            writer.close();

            if(urlConn.getResponseCode() == HttpURLConnection.HTTP_OK){
                is = urlConn.getInputStream();
            } else {
                is = urlConn.getErrorStream();
            }

        } catch (MalformedURLException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

        //return response;
        try {
            BufferedReader reader = new BufferedReader(new InputStreamReader(
                    is, "UTF-8"), 8);
            StringBuilder sb = new StringBuilder();
            String line = null;
            while ((line = reader.readLine()) != null) {
                sb.append(line + "\n");
            }
            is.close();
            response = sb.toString();
            Log.e("JSON", response);
        } catch (Exception e) {
            Log.e("Buffer Error", "Error converting result " + e.toString());
        }

        return response ;

    }


    public SoapObject soap(String METHOD_NAME, String SOAP_ACTION, String NAMESPACE) throws IOException, XmlPullParserException {
        SoapObject request = new SoapObject(NAMESPACE, METHOD_NAME); //set up request
        request.addProperty("iTopN", "5"); //variable name, value. I got the variable name, from the wsdl file!
        SoapSerializationEnvelope envelope = new SoapSerializationEnvelope(SoapEnvelope.VER11); //put all required data into a soap envelope
        envelope.setOutputSoapObject(request);  //prepare request
        URL url = new URL(_url_);
        HttpURLConnection urlcon = (HttpURLConnection)url.openConnection();
        urlcon.setDoOutput(true);
        urlcon.setDoInput(true);
        urlcon.setAllowUserInteraction(true);
        urlcon.setRequestMethod(POSTMETHOD);
        urlcon.setRequestProperty(CONTENTTYPE, SOAP);
        urlcon.addRequestProperty(SOAP_ACTION, String.valueOf(envelope));
//        AndroidHttpTransport httpTransport = new AndroidHttpTransport(URL);
//
//        httpTransport.debug = true;  //this is optional, use it if you don't want to use a packet sniffer to check what the sent message was (httpTransport.requestDump)
//        httpTransport.call(SOAP_ACTION, envelope); //send request
        SoapObject result=(SoapObject)envelope.getResponse(); //get response
        return result;
    }

}
