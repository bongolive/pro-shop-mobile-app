/*
 * Copyright (c) 2015.
 * Prosurvey is a property of Bongolive Enterprises Ltd. An authorized
 * copy of ideas, source codes is against the regulations
 */

/**
 * 
 */
package bongolive.apps.proshop.controller;

import android.accounts.Account;
import android.annotation.TargetApi;
import android.content.AbstractThreadedSyncAdapter;
import android.content.ContentProviderClient;
import android.content.ContentResolver;
import android.content.Context;
import android.content.SyncResult;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;

import bongolive.apps.proshop.model.Audit;
import bongolive.apps.proshop.model.AuditResponses;
import bongolive.apps.proshop.model.Customers;
import bongolive.apps.proshop.model.Delivery;
import bongolive.apps.proshop.model.Expenses;
import bongolive.apps.proshop.model.MultimediaContents;
import bongolive.apps.proshop.model.Payments;
import bongolive.apps.proshop.model.Product_Categories;
import bongolive.apps.proshop.model.Products;
import bongolive.apps.proshop.model.PurchaseItems;
import bongolive.apps.proshop.model.Sales;
import bongolive.apps.proshop.model.SalesItems;
import bongolive.apps.proshop.model.Settings;
import bongolive.apps.proshop.model.Station;
import bongolive.apps.proshop.model.StockIssue;
import bongolive.apps.proshop.model.Tracking;
import bongolive.apps.proshop.model.Transfer;
import bongolive.apps.proshop.model.Warehouse;

/**
 * @author nasznjoka
 * 
 * Oct 8, 2014
 *
 *
 */
@TargetApi(Build.VERSION_CODES.HONEYCOMB)
public class SyncAdapterMaster extends AbstractThreadedSyncAdapter
{
	
	private static final String TAG = "SyncAdapter";
	ContentResolver mContentResolver ;
    AppPreference preference;
	/**
	 * @param context
	 * @param autoInitialize
	 */
	public SyncAdapterMaster(Context context, boolean autoInitialize) {
		super(context, autoInitialize);
		mContentResolver = context.getContentResolver() ;
	}
	
	//android 3.0 and above compatibility
	public SyncAdapterMaster(Context context, boolean autoInitialize, boolean allowParallelSync)
	{
		super(context, autoInitialize, allowParallelSync);
		mContentResolver = context.getContentResolver() ;
	}

	/* (non-Javadoc)
	 * @see android.content.AbstractThreadedSyncAdapter#onPerformSync(android.accounts.Account, android.os.Bundle, java.lang.String, android.content.ContentProviderClient, android.content.SyncResult)
	 */
	@Override
	public void onPerformSync(Account account, Bundle extras, String authority,
			ContentProviderClient provider, SyncResult syncResult) {
        preference = new AppPreference(getContext());
        int accounttype = preference.get_accounttype();
        System.out.println("account type "+accounttype);
        boolean receive = Settings.getPuchItemEnabled(getContext());
        /*try {
            Settings.checkSync(getContext());
        } catch (JSONException e) {
            e.printStackTrace();
        }*/
        if(accounttype == 1) {
		/*
		 * This is where all the methods for uploading and download are done
		 */
            Log.d(TAG, "*********** start of *********   onPerformSync for account[" + account.name + "]");

            try {
                Log.d(TAG, "GET CUSTOMERS processing");
                Customers.getCustomersFromServer(getContext());
            } catch (Exception e) {
                e.printStackTrace();
            }

            try {
                Log.v(TAG, "product categores processing");
                Product_Categories.getCatfromServer(getContext());
            } catch (Exception e) {
                e.printStackTrace();
            }

            try {
                Log.v(TAG, "product processing");
                Products.processInformation(getContext());
            } catch (Exception e) {
                e.printStackTrace();
            }

            try {
                Log.d(TAG, "SEND CUSTOMERS processing");
                Customers.sendCustomersToServer(getContext());
            } catch (Exception e) {
                e.printStackTrace();
            }

            try {
                Log.d(TAG, "WH processing");
                Warehouse.getWhfromServer(getContext());
            } catch (Exception e) {
                e.printStackTrace();
            }


            if(receive) {

                try {
                    Log.d(TAG, "Purchase Items processing receive is "+receive);
                    PurchaseItems.saveStock(getContext());
                } catch (Exception e) {
                    e.printStackTrace();
                }


                try {
                    Log.d(TAG, "order processing");

                    Sales.processInformation(getContext());
                } catch (Exception e) {
                    e.printStackTrace();
                }


                try {
                    Log.v(TAG, "orderItems processing");
                    SalesItems.processInformation(getContext());
                } catch (Exception e) {
                    e.printStackTrace();
                }


            } else {

                try {
                    Log.d(TAG, "order processing ");

                    Sales.processInformation(getContext());
                } catch (Exception e) {
                    e.printStackTrace();
                }


                try {
                    Log.v(TAG, "orderItems processing");
                    SalesItems.processInformation(getContext());
                } catch (Exception e) {
                    e.printStackTrace();
                }

                try {
                    Log.d(TAG, "Purchase Items processing receive is " + receive);
                    PurchaseItems.saveStock(getContext());
                } catch (Exception e) {
                    e.printStackTrace();
                }

            }

            try {
                Log.d(TAG, "TRACKER processing");
                Tracking.processInformation(getContext());
            } catch (Exception e) {
                e.printStackTrace();
            }

            if(Settings.getDeliveryEnabled(getContext()))
            try {
                Log.d(TAG, "Delivery processing");
                Delivery.processInformation(getContext());
            } catch (Exception e) {
                e.printStackTrace();
            }

            if(Settings.getStockEnabled(getContext()))
            try {
                Log.d(TAG, "Stock Transfer processing");
                Transfer.process(getContext());
            } catch (Exception e) {
                e.printStackTrace();
            }


            if(Settings.getStockEnabled(getContext()))
            try {
                Log.d(TAG, "Stock ISSUE processing");
                StockIssue.process(getContext());
            } catch (Exception e) {
                e.printStackTrace();
            }

            try {
                Log.d(TAG, "SETTINGS processing");
                Settings.processSettings(getContext());
            } catch (Exception e) {
                e.printStackTrace();
            }
            if(Settings.getFormEnabled(getContext()))
            try {
                Log.d(TAG, "STATION processing");
                Station.processInformation(getContext());
            } catch (Exception e) {
                e.printStackTrace();
            }
            if(Settings.getAuditEnabled(getContext()))
            try {
                Log.d(TAG, "AUDIT processing");
                Audit.processInformation(getContext());
            } catch (Exception e) {
                e.printStackTrace();
            }
            if(Settings.getAuditEnabled(getContext()))
            try {
                Log.d(TAG, "AUDITRESPONSE processing");
                AuditResponses.processInformation(getContext());
            } catch (Exception e) {
                e.printStackTrace();
            }
            if(Settings.getExEnabled(getContext()))
            try {
                Log.d(TAG, "EXPENSE processing");
                Expenses.processInformation(getContext());
            } catch (Exception e) {
                e.printStackTrace();
            }

            try {
                Log.d(TAG, "PAYMENT processing");
                Payments.processInformation(getContext());
            } catch (Exception e) {
                e.printStackTrace();
            }


            try {
                Log.v(TAG, "multimedia processing");
                MultimediaContents.processInformation(getContext());
            } catch (Exception e) {
                e.printStackTrace();
            }


            Log.d(TAG, "*********** end of *********   onPerformSync for account[" + account.name + "]");

//            syncResult.delayUntil = t;
        }
//        preference.save_last_sync("06-15");
        String date = Constants.getDateOnly2();
        String lastsyc = preference.getLastSync();
        System.out.println("last sync "+lastsyc);

        if(Validating.areSet(lastsyc)) {
            String[] start = lastsyc.split("-");
            String[] end = date.split("-");
            int st = Integer.parseInt(start[0]);
            int et = Integer.parseInt(end[0]);
            int st1 = Integer.parseInt(start[1]);
            int et1 = Integer.parseInt(end[1]);
            if (et > st) {
                if (et1 + (30 - st1) >= 30) {
                    System.out.println("its more than a month validate the account");
                    Settings.perform_account_validation(getContext());
                }
            } else if (et == st) {
                if (et1 - st1 >= 29) {
                    System.out.println("its more than a month validate the account");
                    Settings.perform_account_validation(getContext());
                }
            } else {
                System.out.println("it certainly not more than a moth");
            }
        }
	}
 
}
