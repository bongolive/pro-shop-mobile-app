/*
 * The source codes in this project are owned by Bongolive Enterprises. Copying or any kind of usage without Bongolive's approval is against the law.
 * Copyright (c) 2015.
 *
 */

package bongolive.apps.proshop.controller;

import android.app.Dialog;
import android.content.Context;
import android.content.ContextWrapper;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Path;
import android.graphics.RectF;
import android.net.Uri;
import android.os.Environment;
import android.support.design.widget.FloatingActionButton;
import android.util.AttributeSet;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import java.io.File;
import java.io.FileOutputStream;
import java.util.ArrayList;
import java.util.List;

import bongolive.apps.proshop.R;
import bongolive.apps.proshop.model.Delivery;
import bongolive.apps.proshop.model.MultimediaContents;
import bongolive.apps.proshop.model.Products;
import bongolive.apps.proshop.model.Sales;
import bongolive.apps.proshop.model.SalesItems;

public class DeliveringAdapter extends BaseAdapter {
        private LayoutInflater mInflater;
        AppPreference appPreference;
        Context _context;
        FloatingActionButton actionButton;
        Dialog[] _d;
        ArrayList<Long> updatedids = new ArrayList<>();
        long _id , sysid;
        private List<SalesItems> mItems = new ArrayList<SalesItems>();

        LinearLayout mContent;
        signature mSignature;
        Button mClear, mGetSign, mCancel;
        CheckBox chkconsent;
        public static String tempDir;
        public int count = 1;
        public String current = null;
        private Bitmap mBitmap;
        View mView;
        File mypath;
        private String uniqueId;


        public DeliveringAdapter(Context context, List<SalesItems> items, FloatingActionButton ab, Dialog[] d,long id) {
            mInflater = LayoutInflater.from(context);
            mItems = items;
            this._context = context;
            this.actionButton = ab;
            _d = d;
            _id = id;
        }

        /* (non-Javadoc)
         * @see android.widget.Adapter#getCount()
         */
        @Override
        public int getCount() {
            return mItems.size();
        }

        /* (non-Javadoc)
         * @see android.widget.Adapter#getItem(int)
         */
        @Override
        public Object getItem(int position) {
            return mItems.get(position);
        }

        /* (non-Javadoc)
         * @see android.widget.Adapter#getItemId(int)
         */
        @Override
        public long getItemId(int arg0) {
            return 0;
        }

        /* (non-Javadoc)
         * @see android.widget.Adapter#getView(int, android.view.View, android.view.ViewGroup)
         */
        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            final ItemViewHolder holder;

            if (convertView == null) {
                convertView = mInflater.inflate(R.layout.deliveryitems, null);
                holder = new ItemViewHolder();
                holder.txtprodname = (TextView) convertView.findViewById(R.id.txtdelprod);
                holder.txttodelqty = (TextView) convertView.findViewById(R.id.txtdelqty);
                holder.etdelqty = (EditText) convertView.findViewById(R.id.etdelqty);
                holder.chkaccept = (CheckBox) convertView.findViewById(R.id.chkdel);

                final SalesItems items = mItems.get(position);
                long prod = items.getProdloc();
                sysid = items.getOrderid();

                final int prodqty = items.getQty();
                final int prodqtyd = items.getQtydel();
                long saleid = items.getOrderid();
                String prodnm = Products.getProdName(_context, String.valueOf(prod));
                System.out.println("prod id is " + prod + " prodname " + prodnm);
                holder.txtprodname.setText(prodnm);
                holder.txttodelqty.setText(String.valueOf(prodqty));
                holder.etdelqty.setText(String.valueOf(prodqtyd));

                holder.chkaccept.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                    @Override
                    public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                        if (isChecked) {
                            holder.etdelqty.setText(String.valueOf(prodqtyd));
                            holder.etdelqty.setFocusable(false);
                            holder.etdelqty.setFocusableInTouchMode(false);
                        } else {
                            holder.etdelqty.setText(String.valueOf(prodqtyd));
                            holder.etdelqty.setFocusable(true);
                            holder.etdelqty.setFocusableInTouchMode(true);
                            holder.etdelqty.requestFocus();
                        }
                    }
                });
                holder.etdelqty.setOnFocusChangeListener(new View.OnFocusChangeListener() {
                    @Override
                    public void onFocusChange(View v, boolean hasFocus) {
                        if(hasFocus){
                            ((InputMethodManager) _context.getSystemService(Context.INPUT_METHOD_SERVICE))
                                    .toggleSoftInput
                                            (InputMethodManager.SHOW_FORCED, InputMethodManager.HIDE_IMPLICIT_ONLY);
                        }
                    }
                });

                holder.etdelqty.setOnKeyListener(new View.OnKeyListener() {
                    @Override
                    public boolean onKey(View v, int keyCode, KeyEvent event) {
                        if(event.getAction() == KeyEvent.ACTION_DOWN && event.getKeyCode() == KeyEvent.KEYCODE_ENTER ){
                            System.out.println("event collected");
                            String qnt = holder.etdelqty.getText().toString();
                            if(Validating.areSet(qnt)) {
                                int qty = Integer.parseInt(qnt);
                                if (qty < prodqtyd) {
                                    String[] vals = new String[]{String.valueOf(items.getId()), qnt, Constants
                                            .SALE_STATUS_REJT};
                                    boolean upd = SalesItems.updateItem(_context,vals);
                                    if(upd){
                                        updatedids.add(items.getId());
                                        System.out.println("id list size is " + updatedids.size());
                                        holder.chkaccept.setChecked(true);
                                        holder.etdelqty.setText(qnt);
                                    }
                                    ((InputMethodManager) _context.getSystemService(Context.INPUT_METHOD_SERVICE))
                                            .hideSoftInputFromWindow(holder.etdelqty.getWindowToken(), 0);
                                } else if (qty == prodqty) {
                                    ((InputMethodManager) _context.getSystemService(Context.INPUT_METHOD_SERVICE))
                                            .hideSoftInputFromWindow(holder.etdelqty.getWindowToken(), 0);
                                    holder.chkaccept.setChecked(true);
                                } else if(qty > prodqty){
                                    Toast.makeText(_context,_context.getString(R.string.strqtywarn),Toast
                                            .LENGTH_SHORT).show();
                                    holder.etdelqty.setText(qnt);
                                }
                            }
                        }
                        return false;
                    }
                });
                convertView.setTag(holder);
            } else {
                holder = (ItemViewHolder) convertView.getTag();
            }
            actionButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    process_signature();
                }
            });
            return convertView;
        }

        static class ItemViewHolder {
            TextView txtprodname;
            TextView txttodelqty;
            EditText etdelqty;
            CheckBox chkaccept;
        }

        private void process_signature() {
            LayoutInflater infl = (LayoutInflater)_context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            View view = infl.inflate(R.layout.signature, null);

            final Dialog dg = new Dialog(_context, R.style.CustomDialog);
            dg.setCancelable(true);
            dg.setContentView(view);


            tempDir = Environment.getExternalStorageDirectory()+"/Proshop_Reports/.temp";
            ContextWrapper cw = new ContextWrapper(dg.getContext().getApplicationContext());
            File directory = cw.getDir("tayananjoka", Context.MODE_PRIVATE);

            prepareDirectory();
            uniqueId = System.currentTimeMillis() + "_" + Math.random();
            current = uniqueId + ".png";
            mypath = new File(directory, current);

            mContent = (LinearLayout)dg. findViewById(R.id.linearLayout);
            mSignature = new signature(dg.getContext(), null);
            mSignature.setBackgroundColor(Color.WHITE);
            mContent.addView(mSignature, ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT);
            mClear = (Button) dg.findViewById(R.id.clear);
            mGetSign = (Button) dg.findViewById(R.id.getsign);
            chkconsent = (CheckBox)dg.findViewById(R.id.chkconsent);
            mGetSign.setEnabled(false);
            mCancel = (Button) dg.findViewById(R.id.cancel);
            mView = mContent;
            final boolean[] accepted = {false};


            chkconsent.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                @Override
                public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                    if(isChecked){
                        accepted[0] = true;
                    } else {
                        accepted[0] = false;
                    }
                }
            });

            mClear.setOnClickListener(new View.OnClickListener() {
                public void onClick(View v) {
                    Log.v("log_tag", "Panel Cleared");
                    mSignature.clear();
                    mGetSign.setEnabled(false);
                }
            });

            mGetSign.setOnClickListener(new View.OnClickListener() {
                public void onClick(View v) {
                    mView.setDrawingCacheEnabled(true);
                    String bitmap = mSignature.save(mView);

                    if (bitmap != null) {
                        if(accepted[0]) {
                            Log.v("log_tag", "uri " + bitmap);
                            String[] nm = bitmap.split("/");

                            String doref = Delivery.getSaleReference(dg.getContext(), _id);
                            System.out.println("ref is "+doref+" _id "+_id);
                            String[] med = {bitmap,nm[nm.length-1],MultimediaContents.OPERATIONTYPED,doref};

                            if(Validating.areSet(med)) {
                                System.out.println(nm[nm.length - 1]);
                               boolean stored = MultimediaContents.storeMultimedia(dg.getContext(), med);
                                if(stored){
                                    System.out.println("stored ? "+stored);
                                }
                            }
                            //path,name,type,foreign

                            Toast.makeText(dg.getContext(), dg.getContext().getString(R.string.strdelsuccess), Toast
                                    .LENGTH_SHORT).show();
                            String status ;
                            boolean chkstat = SalesItems.checkReject(dg.getContext(),_id,0);
                            boolean chkstat1 = SalesItems.checkReject(dg.getContext(),_id,1);

                            boolean setitems = SalesItems.updateAllForReturn(dg.getContext(),_id);
                            System.out.println("all items reset ack to 2 ? "+setitems);

                            if(chkstat)
                                status = Constants.SALE_STATUS_PART;
                            else
                                status = Constants.SALE_STATUS_CMPLT;

                            if(!chkstat1)
                                status = Constants.SALE_STATUS_REJT;

                            String[] vals = {doref,status};
                            boolean upd = Delivery.updateDelivery(dg.getContext(), vals);

                            if(upd) {
                                boolean upd2 = Sales.updateSaleStatus(dg.getContext(), _id);
                                System.out.println("sale updated? "+upd2);
                                /*Intent intent = new Intent(dg.getContext(), PrintingScreen.class);
                                intent.putExtra("info", _id);
                                dg.getContext().startActivity(intent);*/

                                if (_d[0].isShowing())
                                    _d[0].dismiss();
                                if (_d[1].isShowing())
                                    _d[1].dismiss();
                                if (dg.isShowing())
                                    dg.dismiss();
                            } else {
                                //restore all the previsous items state
                                boolean rollbck = SalesItems.updateItemRollBack(dg.getContext(),_id);
                                System.out.println(" sale items rolled back? "+rollbck);
                            }
                            //print
                        } else {
                            Toast.makeText(dg.getContext(),dg.getContext().getString(R.string.strconsentwarn),Toast
                                    .LENGTH_SHORT).show();
                        }
                    }
                }
            });

            mCancel.setOnClickListener(new View.OnClickListener() {
                public void onClick(View v) {
                    Log.v("log_tag", "Panel Canceled");
                    dg.dismiss();
                }
            });

            dg.show();
        }



        private boolean prepareDirectory()
        {
            try
            {
                if (makedirs())
                {
                    return true;
                } else {
                    return false;
                }
            } catch (Exception e)
            {
                e.printStackTrace();
                Toast.makeText(_context, "Could not initiate File System.. Is Sdcard mounted properly?", Toast.LENGTH_SHORT)
                        .show();
                return false;
            }
        }

        private boolean makedirs()
        {
            File tempdir = new File(tempDir);
            if (!tempdir.exists())
                tempdir.mkdirs();

            if (tempdir.isDirectory())
            {
                File[] files = tempdir.listFiles();
                for (File file : files)
                {
                    if (!file.delete())
                    {
                        System.out.println("Failed to delete " + file);
                    }
                }
            }
            return (tempdir.isDirectory());
        }
        public class signature extends View
        {
            private static final float STROKE_WIDTH = 5f;
            private static final float HALF_STROKE_WIDTH = STROKE_WIDTH / 2;
            private Paint paint = new Paint();
            private Path path = new Path();

            private float lastTouchX;
            private float lastTouchY;
            private final RectF dirtyRect = new RectF();

            public signature(Context context, AttributeSet attrs)
            {
                super(context, attrs);
                paint.setAntiAlias(true);
                paint.setColor(Color.BLACK);
                paint.setStyle(Paint.Style.STROKE);
                paint.setStrokeJoin(Paint.Join.ROUND);
                paint.setStrokeWidth(STROKE_WIDTH);
            }

            public String save(View v)
            {
                Uri uri = null;
                Log.v("log_tag", "Width: " + v.getWidth());
                Log.v("log_tag", "Height: " + v.getHeight());
                String bitmap = null;
                if(mBitmap == null)
                {
                    mBitmap =  Bitmap.createBitmap(mContent.getWidth(), mContent.getHeight(), Bitmap.Config.RGB_565);;
                }
                Canvas canvas = new Canvas(mBitmap);
                try
                {
                    FileOutputStream mFileOutStream = new FileOutputStream(mypath);

                    v.draw(canvas);
                    mBitmap.compress(Bitmap.CompressFormat.PNG, 90, mFileOutStream);
                    bitmap = saveImage(mBitmap);
                }
                catch(Exception e)
                {
                    Log.v("log_tag", e.toString());
                }
                return bitmap;
            }

            public void clear()
            {
                path.reset();
                invalidate();
            }

            private String saveImage(Bitmap bitmap) {

                String stored = null;

                File sdcard = Environment.getExternalStorageDirectory() ;

                File folder = new File(sdcard.getAbsoluteFile(), Constants.MASTER_DIR);
                folder.mkdir();
                String filename = String.valueOf(System.currentTimeMillis());
                File file = new File(folder.getAbsoluteFile(), filename + ".jpg") ;
                if (file.exists())
                    return stored ;

                try {
                    FileOutputStream out = new FileOutputStream(file);
                    bitmap.compress(Bitmap.CompressFormat.JPEG, 90, out);
                    out.flush();
                    out.close();
                    stored = file.getAbsolutePath();
                } catch (Exception e) {
                    e.printStackTrace();
                }
                return stored;
            }

            @Override
            protected void onDraw(Canvas canvas)
            {
                canvas.drawPath(path, paint);
            }

            @Override
            public boolean onTouchEvent(MotionEvent event)
            {
                float eventX = event.getX();
                float eventY = event.getY();
                mGetSign.setEnabled(true);

                switch (event.getAction())
                {
                    case MotionEvent.ACTION_DOWN:
                        path.moveTo(eventX, eventY);
                        lastTouchX = eventX;
                        lastTouchY = eventY;
                        return true;

                    case MotionEvent.ACTION_MOVE:

                    case MotionEvent.ACTION_UP:

                        resetDirtyRect(eventX, eventY);
                        int historySize = event.getHistorySize();
                        for (int i = 0; i < historySize; i++)
                        {
                            float historicalX = event.getHistoricalX(i);
                            float historicalY = event.getHistoricalY(i);
                            expandDirtyRect(historicalX, historicalY);
                            path.lineTo(historicalX, historicalY);
                        }
                        path.lineTo(eventX, eventY);
                        break;

                    default:
                        debug("Ignored touch event: " + event.toString());
                        return false;
                }

                invalidate((int) (dirtyRect.left - HALF_STROKE_WIDTH),
                        (int) (dirtyRect.top - HALF_STROKE_WIDTH),
                        (int) (dirtyRect.right + HALF_STROKE_WIDTH),
                        (int) (dirtyRect.bottom + HALF_STROKE_WIDTH));

                lastTouchX = eventX;
                lastTouchY = eventY;

                return true;
            }

            private void debug(String string){
            }

            private void expandDirtyRect(float historicalX, float historicalY)
            {
                if (historicalX < dirtyRect.left)
                {
                    dirtyRect.left = historicalX;
                }
                else if (historicalX > dirtyRect.right)
                {
                    dirtyRect.right = historicalX;
                }

                if (historicalY < dirtyRect.top)
                {
                    dirtyRect.top = historicalY;
                }
                else if (historicalY > dirtyRect.bottom)
                {
                    dirtyRect.bottom = historicalY;
                }
            }

            private void resetDirtyRect(float eventX, float eventY)
            {
                dirtyRect.left = Math.min(lastTouchX, eventX);
                dirtyRect.right = Math.max(lastTouchX, eventX);
                dirtyRect.top = Math.min(lastTouchY, eventY);
                dirtyRect.bottom = Math.max(lastTouchY, eventY);
            }
        }
    }