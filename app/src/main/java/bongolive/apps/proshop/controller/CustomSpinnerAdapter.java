package bongolive.apps.proshop.controller;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import java.util.ArrayList;

import bongolive.apps.proshop.R;
import bongolive.apps.proshop.model.AuditResponses;
import bongolive.apps.proshop.model.Products;

/**
 * Created by nasznjoka on 11/24/15.
 */
public class CustomSpinnerAdapter extends ArrayAdapter<AuditResponses> {
    long[] extras;
    Context context;

    public CustomSpinnerAdapter(Context context, ArrayList<AuditResponses> objects, long[] extras) {
        super(context,0, objects);
        this.extras = extras;
        this.context = context;
    }
    @Override
        public View getView(int position, View convertView, ViewGroup parent) {
        TextView text = (TextView) convertView;

        if (text == null) {
            text = (TextView) LayoutInflater.from(getContext()).inflate(android.R.layout.simple_spinner_dropdown_item, parent, false);
        }
        AuditResponses au = getItem(position);

        String prodl = au.getProdl();
        long prdl = Long.parseLong(prodl);
        String prd = Products.getProdName(context, prodl);
        boolean dne = AuditResponses.getCustResponse(getContext(), new long[]{extras[0], prdl, extras[1]});
        if(dne)
            text.setCompoundDrawablesWithIntrinsicBounds(0,0, R.drawable.ic_action_tick,0);

        text.setText(prd);

        return text;
    }

        @Override
        public View getDropDownView(int position, View convertView, ViewGroup parent) {
            TextView text = (TextView) convertView;

            if (text == null) {
                text = (TextView) LayoutInflater.from(getContext()).inflate(android.R.layout.simple_spinner_dropdown_item,  parent, false);
            }

            AuditResponses au = getItem(position);
            String prodl = au.getProdl();
            long prdl = Long.parseLong(prodl);
            String prd = Products.getProdName(context, prodl);
            boolean dne = AuditResponses.getCustResponse(getContext(), new long[]{extras[0], prdl, extras[1]});
            if(dne)
                text.setCompoundDrawablesWithIntrinsicBounds(0,0, R.drawable.ic_action_tick,0);

            text.setText(prd);

            return text;
        }
}
